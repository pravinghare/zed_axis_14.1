﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using DataProcessingBlocks;
using System.Globalization;
using EDI.Constant;

namespace OnlineDataProcessingsImportClass
{
  public class ImportOnlineVendorCreditEntry
    {
       private static ImportOnlineVendorCreditEntry m_ImportOnlineVendorCreditEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineVendorCreditEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import vendor Credit class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineVendorCreditEntry GetInstance()
        {
            if (m_ImportOnlineVendorCreditEntry == null)
                m_ImportOnlineVendorCreditEntry = new ImportOnlineVendorCreditEntry();
            return m_ImportOnlineVendorCreditEntry;
        }
        /// setting values to vendor credit data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntryCollection ImportOnlineVendorCreditData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntryCollection coll = new OnlineVendorCreditQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
          
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                  
                    string datevalue = string.Empty;
                   
                    //Employee Validation
                    OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntry vendorCredit = new OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntry();
                    if (dt.Columns.Contains("DocNumber"))
                    {
                        vendorCredit = coll.FindVendorCreditNumberEntry(dr["DocNumber"].ToString());

                        if (vendorCredit == null)
                        {

                            #region if vendorCredit is null

                            vendorCredit = new OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntry();

                            if (dt.Columns.Contains("DocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["DocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["DocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorCredit.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorCredit.DocNumber = dr["DocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            vendorCredit.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorCredit.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of PrivateNote
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    vendorCredit.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    vendorCredit.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                vendorCredit.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            vendorCredit.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }
                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        vendorCredit.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorCredit.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorCredit.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            vendorCredit.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorCredit.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                            if (dt.Columns.Contains("DepartmentRef"))
                            {
                                #region Validations of DepartmentRef
                                if (dr["DepartmentRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepartmentRef"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (DepartmentRef.Name != null)
                            {
                                vendorCredit.DepartmentRef.Add(DepartmentRef);
                            }

                            OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemRef"))
                            {
                                #region Validations of LineItemRef
                                if (dr["LineItemRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineItemRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemQty"))
                            {
                                #region Validations for LineItemQty
                                if (dr["LineItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemUnitPrice"))
                            {
                                #region Validations for LineItemUnitPrice
                                if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemTaxCodeRef"))
                            {
                                #region Validations of LineItemTaxCodeRef
                                if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomerRef"))
                            {
                                #region Validations of LineCustomerRef
                                if (dr["LineCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of LineBillableStatus
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ItemRef.Name != null || ItemRef.SKU != null)
                            {
                                ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (ClassRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                            }
                            if (CustomerRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                            }
                            if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                            {
                                Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                            }

                            OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                            OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();

                            if (dt.Columns.Contains("LineAccountDescription"))
                            {
                                #region Validations of LineAccountDescription
                                if (dr["LineAccountDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountDescription"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineAccountAmount"))
                            {
                                #region Validations for LineAccountAmount
                                if (dr["LineAccountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                            if (dt.Columns.Contains("LineAccountCustomerRef"))
                            {
                                #region Validations of LineAccountCustomerRef
                                if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CustomerRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                            }

                            OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                            if (dt.Columns.Contains("LineAccountClassRef"))
                            {
                                #region Validations of LineAccountClassRef
                                if (dr["LineAccountClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountClassRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ClassRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                            }

                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (AccountRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            }
                            ////Improvemnt:: 493
                            //if (dt.Columns.Contains("LineAcctNum"))
                            //{
                            //    #region Validations of LineAccountRef
                            //    if (dr["LineAcctNum"].ToString() != string.Empty)
                            //    {
                            //        if (dr["LineAcctNum"].ToString().Length > 15)
                            //        {
                            //            if (isIgnoreAll == false)
                            //            {
                            //                string strMessages = "This LineAcctNum (" + dr["LineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            //                if (Convert.ToString(result) == "Cancel")
                            //                {
                            //                    continue;
                            //                }
                            //                if (Convert.ToString(result) == "No")
                            //                {
                            //                    return null;
                            //                }
                            //                if (Convert.ToString(result) == "Ignore")
                            //                {
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString().Substring(0, 15);
                            //                }
                            //                if (Convert.ToString(result) == "Abort")
                            //                {
                            //                    isIgnoreAll = true;
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //                }
                            //            }
                            //            else
                            //            {
                            //                AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //        }
                            //    }
                            //    #endregion
                            //}

                            //if (AccountRef.AcctNum != null)
                            //{
                            //    AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            //}

                            if (dt.Columns.Contains("LineAccountBillableStatus"))
                            {
                                #region Validations of LineAccountBillableStatus
                                if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                            {
                                #region Validations of LineAccountTaxCodeRef
                                if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TaxCodeRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                            }

                            if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                                AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);

                         
                            if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                vendorCredit.Line.Add(AccountBasedExpenseLine);
                                vendorCredit.Line.Add(Line);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                            {
                                vendorCredit.Line.Add(AccountBasedExpenseLine);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                vendorCredit.Line.Add(Line);
                            }
                            else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                            {
                                vendorCredit.Line.Add(Line);
                                vendorCredit.Line.Add(AccountBasedExpenseLine);
                            }

                            //Bug;;586
                            OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                            OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                            OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                            if (dt.Columns.Contains("TxnTaxCodeRefName"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }

                            //

                            OnlineEntities.VendorRef VendorRef = new OnlineEntities.VendorRef();
                            if (dt.Columns.Contains("VendorRef"))
                            {
                                #region Validations of VendorRef
                                if (dr["VendorRef"].ToString() != string.Empty)
                                {
                                    if (dr["VendorRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorRef (" + dr["VendorRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorRef.Name = dr["VendorRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorRef.Name = dr["VendorRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorRef.Name = dr["VendorRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorRef.Name = dr["VendorRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (VendorRef.Name != null)
                            {
                                vendorCredit.VendorRef.Add(VendorRef);
                            }

                            OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                            if (dt.Columns.Contains("APAccountRef"))
                            {
                                #region Validations of APAccountRef
                                if (dr["APAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["APAccountRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This APAccountRef (" + dr["APAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                APAccountRef.Name = dr["APAccountRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                APAccountRef.Name = dr["APAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            APAccountRef.Name = dr["APAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (APAccountRef.Name != null)
                            {
                                vendorCredit.APAccountRef.Add(APAccountRef);
                            }
                            ////Improvement::493
                            //if (dt.Columns.Contains("APAcctNum"))
                            //{
                            //    #region Validations of APAccountRef
                            //    if (dr["APAcctNum"].ToString() != string.Empty)
                            //    {
                            //        if (dr["APAcctNum"].ToString().Length > 15)
                            //        {
                            //            if (isIgnoreAll == false)
                            //            {
                            //                string strMessages = "This APAcctNum (" + dr["APAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            //                if (Convert.ToString(result) == "Cancel")
                            //                {
                            //                    continue;
                            //                }
                            //                if (Convert.ToString(result) == "No")
                            //                {
                            //                    return null;
                            //                }
                            //                if (Convert.ToString(result) == "Ignore")
                            //                {
                            //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString().Substring(0, 15);
                            //                }
                            //                if (Convert.ToString(result) == "Abort")
                            //                {
                            //                    isIgnoreAll = true;
                            //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                            //                }
                            //            }
                            //            else
                            //            {
                            //                APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                            //        }
                            //    }
                            //    #endregion
                            //}

                            //if (APAccountRef.AcctNum != null)
                            //{
                            //    vendorCredit.APAccountRef.Add(APAccountRef);
                            //}

                            if (dt.Columns.Contains("GlobalTaxCalculation"))
                            {
                                #region Validations of GlobalTaxCalculation
                                if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                                {
                                    if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TransactionLocationType"))
                            {
                                #region Validations of TransactionLocationType
                                if (dr["TransactionLocationType"].ToString() != string.Empty)
                                {
                                    if (dr["TransactionLocationType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            #endregion

                            coll.Add(vendorCredit);
                        }
                        else
                        {
                            #region if vendorCredit is not null

                            OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemRef"))
                            {
                                #region Validations of LineItemRef
                                if (dr["LineItemRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineItemRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemQty"))
                            {
                                #region Validations for LineItemQty
                                if (dr["LineItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemUnitPrice"))
                            {
                                #region Validations for LineItemUnitPrice
                                if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemTaxCodeRef"))
                            {
                                #region Validations of LineItemTaxCodeRef
                                if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineCustomerRef"))
                            {
                                #region Validations of LineCustomerRef
                                if (dr["LineCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of LineBillableStatus
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ItemRef.Name != null || ItemRef.SKU != null)
                            {
                                ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (ClassRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                            }
                            if (CustomerRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                            }
                            if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                            {
                                Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                            }

                            OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                            OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();
                            if (dt.Columns.Contains("LineAccountDescription"))
                            {
                                #region Validations of LineAccountDescription
                                if (dr["LineAccountDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountDescription"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineAccountAmount"))
                            {
                                #region Validations for LineAccountAmount
                                if (dr["LineAccountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                            if (dt.Columns.Contains("LineAccountCustomerRef"))
                            {
                                #region Validations of LineAccountCustomerRef
                                if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CustomerRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                            }

                            OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                            if (dt.Columns.Contains("LineAccountClassRef"))
                            {
                                #region Validations of LineAccountClassRef
                                if (dr["LineAccountClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountClassRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ClassRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                            }

                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (AccountRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            }

                            ////Improvemnt:: 493
                            //if (dt.Columns.Contains("LineAcctNum"))
                            //{
                            //    #region Validations of LineAccountRef
                            //    if (dr["LineAcctNum"].ToString() != string.Empty)
                            //    {
                            //        if (dr["LineAcctNum"].ToString().Length > 15)
                            //        {
                            //            if (isIgnoreAll == false)
                            //            {
                            //                string strMessages = "This LineAcctNum (" + dr["LineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            //                if (Convert.ToString(result) == "Cancel")
                            //                {
                            //                    continue;
                            //                }
                            //                if (Convert.ToString(result) == "No")
                            //                {
                            //                    return null;
                            //                }
                            //                if (Convert.ToString(result) == "Ignore")
                            //                {
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString().Substring(0, 15);
                            //                }
                            //                if (Convert.ToString(result) == "Abort")
                            //                {
                            //                    isIgnoreAll = true;
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //                }
                            //            }
                            //            else
                            //            {
                            //                AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //        }
                            //    }
                            //    #endregion
                            //}

                            //if (AccountRef.AcctNum != null)
                            //{
                            //    AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            //}

                            if (dt.Columns.Contains("LineAccountBillableStatus"))
                            {
                                #region Validations of LineAccountBillableStatus
                                if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                            {
                                #region Validations of LineAccountTaxCodeRef
                                if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Bug;;586
                            OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                            OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                            OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                            if (dt.Columns.Contains("TxnTaxCodeRefName"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }

                            //

                            if (TaxCodeRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                            }

                            if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                                AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);
                            

                            if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                vendorCredit.Line.Add(AccountBasedExpenseLine);
                                vendorCredit.Line.Add(Line);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                            {
                                vendorCredit.Line.Add(AccountBasedExpenseLine);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                vendorCredit.Line.Add(Line);
                            }
                            else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                            {
                                vendorCredit.Line.Add(Line);
                                vendorCredit.Line.Add(AccountBasedExpenseLine);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        vendorCredit = new OnlineVendorCreditQBEntry();

                        #region if vendorCredit is not null

                        vendorCredit = new OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntry();                       

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of PrivateNote
                            DateTime SODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorCredit.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorCredit.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            vendorCredit.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        vendorCredit.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }
                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    vendorCredit.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            vendorCredit.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vendorCredit.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorCredit.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorCredit.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                        if (dt.Columns.Contains("DepartmentRef"))
                        {
                            #region Validations of DepartmentRef
                            if (dr["DepartmentRef"].ToString() != string.Empty)
                            {
                                if (dr["DepartmentRef"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                else
                                {
                                    DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (DepartmentRef.Name != null)
                        {
                            vendorCredit.DepartmentRef.Add(DepartmentRef);
                        }

                        OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                        OnlineEntities.Line Line = new OnlineEntities.Line();
                        OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                        OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                        OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                        OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                        if (dt.Columns.Contains("LineDescription"))
                        {
                            #region Validations of Associate
                            if (dr["LineDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineDescription"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineDescription = dr["LineDescription"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineAmount"))
                        {
                            #region Validations for LineAmount
                            if (dr["LineAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemRef"))
                        {
                            #region Validations of LineItemRef
                            if (dr["LineItemRef"].ToString() != string.Empty)
                            {
                                if (dr["LineItemRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.Name = dr["LineItemRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        //bug 486
                        if (dt.Columns.Contains("SKU"))
                        {
                            #region Validations of SKU
                            if (dr["SKU"].ToString() != string.Empty)
                            {
                                if (dr["SKU"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.SKU = dr["SKU"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineClassRef"))
                        {
                            #region Validations of LineClassRef
                            if (dr["LineClassRef"].ToString() != string.Empty)
                            {
                                if (dr["LineClassRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef.Name = dr["LineClassRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemQty"))
                        {
                            #region Validations for LineItemQty
                            if (dr["LineItemQty"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemUnitPrice"))
                        {
                            #region Validations for LineItemUnitPrice
                            if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemTaxCodeRef"))
                        {
                            #region Validations of LineItemTaxCodeRef
                            if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineCustomerRef"))
                        {
                            #region Validations of LineCustomerRef
                            if (dr["LineCustomerRef"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomerRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineBillableStatus"))
                        {
                            #region Validations of LineBillableStatus
                            if (dr["LineBillableStatus"].ToString() != string.Empty)
                            {
                                if (dr["LineBillableStatus"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ItemRef.Name != null || ItemRef.SKU != null)
                        {
                            ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                        }
                        if (TaxCodeRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                        }
                        if (ClassRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                        }
                        if (CustomerRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                        }
                        if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                        {
                            Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                        }

                        OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                        OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();

                        if (dt.Columns.Contains("LineAccountDescription"))
                        {
                            #region Validations of LineAccountDescription
                            if (dr["LineAccountDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountDescription"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineAccountAmount"))
                        {
                            #region Validations for LineAccountAmount
                            if (dr["LineAccountAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                        if (dt.Columns.Contains("LineAccountCustomerRef"))
                        {
                            #region Validations of LineAccountCustomerRef
                            if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (CustomerRef1.Name != null)
                        {
                            AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                        }

                        OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                        if (dt.Columns.Contains("LineAccountClassRef"))
                        {
                            #region Validations of LineAccountClassRef
                            if (dr["LineAccountClassRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountClassRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ClassRef1.Name != null)
                        {
                            AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                        }

                        OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                        if (dt.Columns.Contains("LineAccountRef"))
                        {
                            #region Validations of LineAccountRef
                            if (dr["LineAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountRef.Name = dr["LineAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (AccountRef.Name != null)
                        {
                            AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                        }

                        ////Improvemnt:: 493
                        //if (dt.Columns.Contains("LineAcctNum"))
                        //{
                        //    #region Validations of LineAccountRef
                        //    if (dr["LineAcctNum"].ToString() != string.Empty)
                        //    {
                        //        if (dr["LineAcctNum"].ToString().Length > 15)
                        //        {
                        //            if (isIgnoreAll == false)
                        //            {
                        //                string strMessages = "This LineAcctNum (" + dr["LineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                        //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                        //                if (Convert.ToString(result) == "Cancel")
                        //                {
                        //                    continue;
                        //                }
                        //                if (Convert.ToString(result) == "No")
                        //                {
                        //                    return null;
                        //                }
                        //                if (Convert.ToString(result) == "Ignore")
                        //                {
                        //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString().Substring(0, 15);
                        //                }
                        //                if (Convert.ToString(result) == "Abort")
                        //                {
                        //                    isIgnoreAll = true;
                        //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                        //            }
                        //        }
                        //        else
                        //        {
                        //            AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                        //        }
                        //    }
                        //    #endregion
                        //}

                        //if (AccountRef.AcctNum != null)
                        //{
                        //    AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                        //}

                        if (dt.Columns.Contains("LineAccountBillableStatus"))
                        {
                            #region Validations of LineAccountBillableStatus
                            if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                        if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                        {
                            #region Validations of LineAccountTaxCodeRef
                            if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (TaxCodeRef1.Name != null)
                        {
                            AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                        }

                        if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                            AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);

                      
                        if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            vendorCredit.Line.Add(AccountBasedExpenseLine);
                            vendorCredit.Line.Add(Line);
                        }
                        else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                        {
                            vendorCredit.Line.Add(AccountBasedExpenseLine);
                        }
                        else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            vendorCredit.Line.Add(Line);
                        }
                        else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                        {
                            vendorCredit.Line.Add(Line);
                            vendorCredit.Line.Add(AccountBasedExpenseLine);
                        }

                        //Bug;;586
                        OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                        OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                        OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                        OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                        if (dt.Columns.Contains("TxnTaxCodeRefName"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                            {
                                if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (TxnTaxCodeRef.Name != null)
                        {
                            TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                        }

                        //

                        OnlineEntities.VendorRef VendorRef = new OnlineEntities.VendorRef();
                        if (dt.Columns.Contains("VendorRef"))
                        {
                            #region Validations of VendorRef
                            if (dr["VendorRef"].ToString() != string.Empty)
                            {
                                if (dr["VendorRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorRef (" + dr["VendorRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorRef.Name = dr["VendorRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorRef.Name = dr["VendorRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorRef.Name = dr["VendorRef"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorRef.Name = dr["VendorRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (VendorRef.Name != null)
                        {
                            vendorCredit.VendorRef.Add(VendorRef);
                        }

                        OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                        if (dt.Columns.Contains("APAccountRef"))
                        {
                            #region Validations of APAccountRef
                            if (dr["APAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["APAccountRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This APAccountRef (" + dr["APAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            APAccountRef.Name = dr["APAccountRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            APAccountRef.Name = dr["APAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    APAccountRef.Name = dr["APAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (APAccountRef.Name != null)
                        {
                            vendorCredit.APAccountRef.Add(APAccountRef);
                        }

                        ////Improvement::493
                        //if (dt.Columns.Contains("APAcctNum"))
                        //{
                        //    #region Validations of APAccountRef
                        //    if (dr["APAcctNum"].ToString() != string.Empty)
                        //    {
                        //        if (dr["APAcctNum"].ToString().Length > 15)
                        //        {
                        //            if (isIgnoreAll == false)
                        //            {
                        //                string strMessages = "This APAcctNum (" + dr["APAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                        //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                        //                if (Convert.ToString(result) == "Cancel")
                        //                {
                        //                    continue;
                        //                }
                        //                if (Convert.ToString(result) == "No")
                        //                {
                        //                    return null;
                        //                }
                        //                if (Convert.ToString(result) == "Ignore")
                        //                {
                        //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString().Substring(0, 15);
                        //                }
                        //                if (Convert.ToString(result) == "Abort")
                        //                {
                        //                    isIgnoreAll = true;
                        //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                        //            }
                        //        }
                        //        else
                        //        {
                        //            APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                        //        }
                        //    }
                        //    #endregion
                        //}

                        //if (APAccountRef.AcctNum != null)
                        //{
                        //    vendorCredit.APAccountRef.Add(APAccountRef);
                        //}

                        if (dt.Columns.Contains("GlobalTaxCalculation"))
                        {
                            #region Validations of GlobalTaxCalculation
                            if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                            {
                                if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorCredit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TransactionLocationType"))
                        {
                            #region Validations of TransactionLocationType
                            if (dr["TransactionLocationType"].ToString() != string.Empty)
                            {
                                if (dr["TransactionLocationType"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorCredit.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                }
                            }
                            #endregion
                        }

                        #endregion

                        coll.Add(vendorCredit);
                    }

                }
                else
                {
                    return null;
                }

            }
            #endregion
            
            #endregion

            return coll;

        }
    }
}
