﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class OnlineInvoiceQBEntry
    {
        #region member
        private string m_DocNumber;
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private string m_PrivateNote;
        private string m_TxnDate;
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.CustomerRef> m_CustomerRef = new Collection<OnlineEntities.CustomerRef>();
        private string m_CustomerMemo;
        private Collection<BillAddr> m_BillAddr = new Collection<BillAddr>();
        private Collection<ShipAddr> m_ShipAddr = new Collection<ShipAddr>();
        private Collection<OnlineEntities.SalesTermRef> m_SalesTermRef = new Collection<OnlineEntities.SalesTermRef>();
        private Collection<OnlineEntities.ShipMethodRef> m_ShipMethodRef = new Collection<OnlineEntities.ShipMethodRef>();
        private string m_TrackingNum;
        private string m_DueDate;
        private string m_ShipDate;
        private string m_ApplyTaxAfterDiscount;
        private string m_PrintStatus;
        private string m_EmailStatus;
        private string m_Deposit;
        private Collection<OnlineEntities.DepositToAccountRef> m_DepositToAccountRef = new Collection<OnlineEntities.DepositToAccountRef>();
        private Collection<OnlineEntities.CustomField> m_CustomField = new Collection<OnlineEntities.CustomField>();
        //Issue No.417
        private Collection<CurrencyRef> m_CurrencyRef = new Collection<CurrencyRef>();
        private string m_ExchangeRate;
        private string m_GlobalTaxCalculation;
        //P Axis 13.2 : issue 712
        private string m_AllowOnlineACHPayment;
        private string m_AllowOnlineCreditCardPayment;

        private string m_HomeTotalAmt;
        //606
        private string m_PrimaryPhone;
        private string m_BillEmail;

        // Issue 556
        private string m_BillEmailCc;
        private string m_BillEmailBcc;

        private string m_PaymentRefNum;
        private string m_TransactionLocationType;
        //594
        private bool m_isAppend;

        #endregion

        #region properties
        
        public Collection<OnlineEntities.CustomField> CustomField
        {
            get { return m_CustomField; }
            set { m_CustomField = value; }
        }
        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }
        public Collection<OnlineEntities.SalesTermRef> SalesTermRef
        {
            get { return m_SalesTermRef; }
            set { m_SalesTermRef = value; }
        }
        public Collection<OnlineEntities.ShipMethodRef> ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }

        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }
        public string BillEmail
        {
            get { return m_BillEmail; }
            set { m_BillEmail = value; }
        }
        //issue 556
        #region issue 556  BillEmailCc BillEmailBcc
        public string BillEmailCc
        {
            get { return m_BillEmailCc; }
            set { m_BillEmailCc = value; }
        }

        public string BillEmailBcc
        {
            get { return m_BillEmailBcc; }
            set { m_BillEmailBcc = value; }
        }
        #endregion
        public Collection<OnlineEntities.DepositToAccountRef> DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
            set { m_DepositToAccountRef = value; }
        }

        public string Deposit
        {
            get { return m_Deposit; }
            set { m_Deposit = value; }
        }

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }


        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }


        public string CustomerMemo
        {
            get { return m_CustomerMemo; }
            set { m_CustomerMemo = value; }
        }


        public string TrackingNum
        {
            get { return m_TrackingNum; }
            set { m_TrackingNum = value; }
        }

        public string PrintStatus
        {
            get { return m_PrintStatus; }
            set { m_PrintStatus = value; }
        }

        //606
        public string PrimaryPhone
        {
            get { return m_PrimaryPhone; }
            set { m_PrimaryPhone = value; }
        }

        public string EmailStatus
        {
            get { return m_EmailStatus; }
            set { m_EmailStatus = value; }
        }


        public string PaymentRefNum
        {
            get { return m_PaymentRefNum; }
            set { m_PaymentRefNum = value; }
        }

        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }

        public string ApplyTaxAfterDiscount
        {
            get { return m_ApplyTaxAfterDiscount; }
            set { m_ApplyTaxAfterDiscount = value; }
        }

        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }

        //P Axis 13.2 : issue 712
        public string AllowOnlineACHPayment
        {
            get { return m_AllowOnlineACHPayment; }
            set { m_AllowOnlineACHPayment = value; }
        }
        public string AllowOnlineCreditCardPayment
        {
            get { return m_AllowOnlineCreditCardPayment; }
            set { m_AllowOnlineCreditCardPayment = value; }
        }

        public string HomeTotalAmt
        {
            get { return m_HomeTotalAmt; }
            set { m_HomeTotalAmt = value; }
        }
        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        public Collection<OnlineEntities.CustomerRef> CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public Collection<BillAddr> BillAddr
        {
            get { return m_BillAddr; }
            set { m_BillAddr = value; }
        }


        public Collection<ShipAddr> ShipAddr
        {
            get { return m_ShipAddr; }
            set { m_ShipAddr = value; }
        }

        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }
        }
        
        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public Collection<CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        //
        #endregion

        #region public method


        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        /// <summary>
        /// Creating new Invoice transaction in quickbook online.
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOInvoice(OnlineDataProcessingsImportClass.OnlineInvoiceQBEntry coll)
        {

            DataService dataService = null;
            Intuit.Ipp.Data.Invoice InvoiceAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.Invoice addInvoice = new Intuit.Ipp.Data.Invoice();
            OnlineInvoiceQBEntry Invoice = new OnlineInvoiceQBEntry();
            Invoice = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;

            //bug 514
            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            int DiscountLineNum = 1;
            try
            {
                #region Add Invoice



                if (Invoice.DocNumber != null)
                {
                    addInvoice.DocNumber = Invoice.DocNumber;
                }
                else { addInvoice.AutoDocNumber = true; addInvoice.AutoDocNumberSpecified = true; }

                if (Invoice.TxnDate != null)
                {
                    try
                    {
                        addInvoice.TxnDate = Convert.ToDateTime(Invoice.TxnDate);
                        addInvoice.TxnDateSpecified = true;
                    }
                    catch { }
                }

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in Invoice.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addInvoice.CustomField = custom_online;
                #endregion

                #region GlobalTaxCalculation

                if (Invoice.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addInvoice.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addInvoice.GlobalTaxCalculationSpecified = true;
                        }
                        else if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addInvoice.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addInvoice.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addInvoice.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addInvoice.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion

                // Axis 742
                if (Invoice.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(Invoice.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addInvoice.TransactionLocationType = value;
                    }
                    else
                    {
                        addInvoice.TransactionLocationType = Invoice.TransactionLocationType;
                    }
                }

                //P Axis 13.2 : issue 712
                if (Invoice.AllowOnlineACHPayment != null)
                {
                    addInvoice.AllowOnlineACHPayment = Convert.ToBoolean(Invoice.AllowOnlineACHPayment);
                    addInvoice.AllowOnlineACHPaymentSpecified = Convert.ToBoolean(Invoice.AllowOnlineACHPayment);
                }
                if (Invoice.AllowOnlineCreditCardPayment != null)
                {
                    addInvoice.AllowOnlineCreditCardPayment = Convert.ToBoolean(Invoice.AllowOnlineCreditCardPayment);
                    addInvoice.AllowOnlineCreditCardPaymentSpecified = Convert.ToBoolean(Invoice.AllowOnlineCreditCardPayment);
                }

                // find department 
                foreach (var i in Invoice.DepartmentRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataDepartment = await CommonUtilities.GetDepartmentExistsInOnlineQBO(i.Name.Trim());
                        //Axis 621 end
                        foreach (var item in dataDepartment)
                        {
                            id = item.Id;
                        }
                        addInvoice.DepartmentRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }

                addInvoice.PrivateNote = Invoice.PrivateNote;

                foreach (var l in Invoice.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        //}
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine != null)
                                {

                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                //bug 486
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;

                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {

                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }


                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch
                                    {
                                    }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (addInvoice.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (array_count == 0)
                                                {
                                                    Array_taxcodeName[0] = taxCoderef.Name;
                                                    array_count++;
                                                    Array_taxAmount[0] = Line.Amount;
                                                    Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                                }
                                                else
                                                {
                                                    int i = 0;
                                                    for (i = 0; i < array_count; i++)
                                                    {
                                                        if (taxCoderef.Name == Array_taxcodeName[i])
                                                        {
                                                            taxFlag = false;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            taxFlag = true;

                                                        }

                                                    }
                                                    if (taxFlag == false)
                                                    {
                                                        if (taxCoderef.Name == Array_taxcodeName[i])
                                                        {
                                                            Array_taxAmount[i] += Line.Amount;
                                                            Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        array_count = array_count + 1;
                                                        Array.Resize(ref Array_taxcodeName, array_count);
                                                        Array.Resize(ref Array_taxAmount, array_count);
                                                        Array.Resize(ref Array_AmountWithTax, array_count);
                                                        Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                        Array_taxAmount[array_count - 1] = Line.Amount;
                                                        Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                        taxFlag = false;
                                                    }
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;

                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {

                                linecount++;
                                Line = new Intuit.Ipp.Data.Line();

                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                    Line.LineNum = DiscountLineNum.ToString();
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }

                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                                DiscountLineNum++;
                            }
                        }

                        addInvoice.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }


                                //bug 486
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }

                                }

                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        try
                                                        {
                                                            if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                            {
                                                                foreach (var taxRateRef in dataTaxcode)
                                                                {
                                                                    if (taxRateRef.SalesTaxRateList.TaxRateDetail != null)
                                                                    {
                                                                        foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                        {
                                                                            taxRateRefName = taxRate.TaxRateRef.name;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex) { MessageBox.Show(ex.ToString()); }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (addInvoice.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (array_count == 0)
                                                {
                                                    Array_taxcodeName[0] = taxCoderef.Name;
                                                    array_count++;
                                                    Array_taxAmount[0] = Line.Amount;
                                                    Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                                }
                                                else
                                                {
                                                    int i = 0;
                                                    for (i = 0; i < array_count; i++)
                                                    {
                                                        if (taxCoderef.Name == Array_taxcodeName[i])
                                                        {
                                                            taxFlag = false;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            taxFlag = true;

                                                        }

                                                    }
                                                    if (taxFlag == false)
                                                    {
                                                        if (taxCoderef.Name == Array_taxcodeName[i])
                                                        {
                                                            Array_taxAmount[i] += Line.Amount;
                                                            Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        array_count = array_count + 1;
                                                        Array.Resize(ref Array_taxcodeName, array_count);
                                                        Array.Resize(ref Array_taxAmount, array_count);
                                                        Array.Resize(ref Array_AmountWithTax, array_count);
                                                        Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                        Array_taxAmount[array_count - 1] = Line.Amount;
                                                        Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                        taxFlag = false;
                                                    }
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            Line = new Intuit.Ipp.Data.Line();
                            linecount++;
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                    Line.LineNum = DiscountLineNum.ToString();
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }

                                }

                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                                DiscountLineNum++;
                            }
                        }

                        addInvoice.Line = AddLine(lines_online, Line, linecount);

                        #endregion
                    }
                }



                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addInvoice.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && Invoice.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addInvoice.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }

                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {

                    //Issue ;; 586
                    foreach (var txntaxCode in Invoice.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addInvoice.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                #endregion

                foreach (var customerRef in Invoice.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }

                        addInvoice.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }
                }

                if (Invoice.CustomerMemo != null)
                {
                    addInvoice.CustomerMemo = new MemoRef()
                    {
                        Value = Invoice.CustomerMemo
                    };
                }

                PhysicalAddress billAddr = new PhysicalAddress();
                if (Invoice.BillAddr.Count > 0)
                {
                    foreach (var billaddress in Invoice.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addInvoice.BillAddr = billAddr;
                }


                PhysicalAddress shipAddr = new PhysicalAddress();
                if (Invoice.ShipAddr.Count > 0)
                {

                    foreach (var shipAddress in Invoice.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addInvoice.ShipAddr = shipAddr;
                }

                if (Invoice.SalesTermRef.Count > 0)
                {
                    foreach (var className in Invoice.SalesTermRef)
                    {
                        string classValue = string.Empty;
                        if (className.Name != null)
                        {
                            var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(className.Name.Trim());
                            foreach (var data in dataClass)
                            {
                                classValue = data.Id;
                            }
                            addInvoice.SalesTermRef = new ReferenceType()
                            {
                                name = className.Name,
                                Value = classValue
                            };
                        }
                    }
                }

                if (Invoice.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in Invoice.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addInvoice.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (Invoice.DueDate != null)
                {
                    try
                    {
                        addInvoice.DueDate = Convert.ToDateTime(Invoice.DueDate);
                        addInvoice.DueDateSpecified = true;
                    }
                    catch
                    { }
                }
                if (Invoice.ShipDate != null)
                {
                    try
                    {
                        addInvoice.ShipDate = Convert.ToDateTime(Invoice.ShipDate);
                        addInvoice.ShipDateSpecified = true;
                    }
                    catch { }
                }




                addInvoice.TrackingNum = Invoice.TrackingNum;
                //Issue No. 417
                if (Invoice.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in Invoice.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addInvoice.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (Invoice.ExchangeRate != null)
                {
                    addInvoice.ExchangeRate = Convert.ToDecimal(Invoice.ExchangeRate);
                    addInvoice.ExchangeRateSpecified = true;
                }
                //

                if (Invoice.PrintStatus != null)
                {
                    if (Invoice.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addInvoice.PrintStatus = PrintStatusEnum.NotSet;
                        addInvoice.PrintStatusSpecified = true;
                    }
                    else if (Invoice.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addInvoice.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addInvoice.PrintStatusSpecified = true;
                    }
                    else if (Invoice.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addInvoice.PrintStatus = PrintStatusEnum.PrintComplete;
                        addInvoice.PrintStatusSpecified = true;
                    }
                }
                if (Invoice.EmailStatus != null)
                {
                    if (Invoice.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addInvoice.EmailStatus = EmailStatusEnum.NotSet;
                        addInvoice.EmailStatusSpecified = true;
                    }
                    else if (Invoice.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addInvoice.EmailStatus = EmailStatusEnum.EmailSent;
                        addInvoice.EmailStatusSpecified = true;
                    }
                    else if (Invoice.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addInvoice.EmailStatus = EmailStatusEnum.NeedToSend;
                        addInvoice.EmailStatusSpecified = true;
                    }
                }

                //606
                //Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                //if (Invoice.PrimaryPhone.Count > 0)
                //{
                //    foreach (var primaryPhone in Invoice.PrimaryPhone)
                //    {
                //        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                //    }
                //    addInvoice.PrimaryPhone = primaryPhoneNo;
                //}

                if (Invoice.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = Invoice.BillEmail;
                    addInvoice.BillEmail = EmailAddress;
                }

                // Issue 556
                if (Invoice.BillEmailCc != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = Invoice.BillEmailCc;
                    addInvoice.BillEmailCc = EmailAddress;
                }

                if (Invoice.BillEmailBcc != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = Invoice.BillEmailBcc;
                    addInvoice.BillEmailBcc = EmailAddress;
                }


                if (Invoice.ApplyTaxAfterDiscount != null)
                {
                    addInvoice.ApplyTaxAfterDiscount = Convert.ToBoolean(Invoice.ApplyTaxAfterDiscount);
                    addInvoice.ApplyTaxAfterDiscountSpecified = true;
                }

                foreach (var accounts in Invoice.DepositToAccountRef)
                {
                    if (accounts.Name != null && accounts.Name != string.Empty && accounts.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accounts.Name);
                        addInvoice.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }

                }
                if (Invoice.Deposit != null)
                {
                    addInvoice.Deposit = Convert.ToDecimal(Invoice.Deposit);
                    addInvoice.DepositSpecified = true;
                }

                #endregion
                InvoiceAdded = new Intuit.Ipp.Data.Invoice();
                InvoiceAdded = dataService.Add<Intuit.Ipp.Data.Invoice>(addInvoice);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (InvoiceAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = InvoiceAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = InvoiceAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        /// <summary>
        /// updating Invoice entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="InvoiceId"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOInvoice(OnlineDataProcessingsImportClass.OnlineInvoiceQBEntry coll, string InvoiceId, string syncToken, Intuit.Ipp.Data.Invoice PreviousData = null)
        {

            DataService dataService = null;
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.Invoice InvoiceAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            Intuit.Ipp.Data.Invoice addInvoice = new Intuit.Ipp.Data.Invoice();
            OnlineInvoiceQBEntry Invoice = new OnlineInvoiceQBEntry();
            Invoice = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];

            //bug 514
            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag
            int DiscountLineNum = 1;
            try
            {
                #region Add Invoice
                //594
                if (Invoice.isAppend)
                {
                    addInvoice = PreviousData; 
                   
                    if (addInvoice.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addInvoice.Line.Length;
                        lines_online = addInvoice.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                    addInvoice.sparse = true;
                    addInvoice.sparseSpecified = true;
                }

                addInvoice.Id = InvoiceId;
                addInvoice.SyncToken = syncToken;

                if (Invoice.DocNumber != null)
                {
                    addInvoice.DocNumber = Invoice.DocNumber;
                }
                //else { addInvoice.AutoDocNumber = true; addInvoice.AutoDocNumberSpecified = true; }

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in Invoice.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addInvoice.CustomField = custom_online;
                #endregion


                if (Invoice.TxnDate != null)
                {
                    try
                    {
                        addInvoice.TxnDate = Convert.ToDateTime(Invoice.TxnDate);
                        addInvoice.TxnDateSpecified = true;
                    }
                    catch { }
                }

                // Axis 742
                if (Invoice.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(Invoice.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addInvoice.TransactionLocationType = value;
                    }
                    else
                    {
                        addInvoice.TransactionLocationType = Invoice.TransactionLocationType;
                    }
                }

                //P Axis 13.2 : issue 712
                if (Invoice.AllowOnlineACHPayment != null)
                {
                    addInvoice.AllowOnlineACHPayment = Convert.ToBoolean(Invoice.AllowOnlineACHPayment);
                    addInvoice.AllowOnlineACHPaymentSpecified = Convert.ToBoolean(Invoice.AllowOnlineACHPayment);
                }
                if (Invoice.AllowOnlineCreditCardPayment != null)
                {
                    addInvoice.AllowOnlineCreditCardPayment = Convert.ToBoolean(Invoice.AllowOnlineCreditCardPayment);
                    addInvoice.AllowOnlineCreditCardPaymentSpecified = Convert.ToBoolean(Invoice.AllowOnlineCreditCardPayment);
                }

                // find department 
                foreach (var i in Invoice.DepartmentRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataDepartment = await CommonUtilities.GetDepartmentExistsInOnlineQBO(i.Name.Trim());
                        foreach (var item in dataDepartment)
                        {
                            id = item.Id;
                        }
                        addInvoice.DepartmentRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }

                addInvoice.PrivateNote = Invoice.PrivateNote;
                #region GlobalTaxCalculation

                if (Invoice.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addInvoice.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addInvoice.GlobalTaxCalculationSpecified = true;
                        }
                        else if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addInvoice.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addInvoice.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addInvoice.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addInvoice.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion

                foreach (var l in Invoice.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        //}
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine != null)
                                {

                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                //bug 486
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;

                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }


                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch
                                    {
                                    }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }
                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;

                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {

                                linecount++;
                                Line = new Intuit.Ipp.Data.Line();

                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                    Line.LineNum = DiscountLineNum.ToString();
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }

                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                                DiscountLineNum++;
                            }
                        }

                        addInvoice.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }


                                //bug 486
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }

                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        try
                                                        {
                                                            if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                            {
                                                                foreach (var taxRateRef in dataTaxcode)
                                                                {
                                                                    if (taxRateRef.SalesTaxRateList.TaxRateDetail != null)
                                                                    {
                                                                        foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                        {
                                                                            taxRateRefName = taxRate.TaxRateRef.name;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex) { MessageBox.Show(ex.ToString()); }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (Invoice.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            Line = new Intuit.Ipp.Data.Line();
                            linecount++;
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                    Line.LineNum = DiscountLineNum.ToString();
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }

                                }

                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                                DiscountLineNum++;
                            }
                        }

                        addInvoice.Line = AddLine(lines_online, Line, linecount);

                        #endregion
                    }
                }

                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addInvoice.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && Invoice.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;
                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;
                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addInvoice.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }

                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    //Issue ;; 586
                    foreach (var txntaxCode in Invoice.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addInvoice.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                #endregion


                foreach (var customerRef in Invoice.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }

                        addInvoice.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }

                }


                if (Invoice.CustomerMemo != null)
                {
                    addInvoice.CustomerMemo = new MemoRef()
                    {
                        Value = Invoice.CustomerMemo
                    };
                }

                PhysicalAddress billAddr = new PhysicalAddress();
                if (Invoice.BillAddr.Count > 0)
                {
                    foreach (var billaddress in Invoice.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addInvoice.BillAddr = billAddr;
                }



                PhysicalAddress shipAddr = new PhysicalAddress();
                if (Invoice.ShipAddr.Count > 0)
                {

                    foreach (var shipAddress in Invoice.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addInvoice.ShipAddr = shipAddr;
                }

                if (Invoice.SalesTermRef.Count > 0)
                {
                    foreach (var className in Invoice.SalesTermRef)
                    {
                        string classValue = string.Empty;
                        if (className.Name != null)
                        {
                            var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(className.Name.Trim());
                            foreach (var data in dataClass)
                            {
                                classValue = data.Id;
                            }
                            addInvoice.SalesTermRef = new ReferenceType()
                            {
                                name = className.Name,
                                Value = classValue
                            };
                        }
                    }
                }

                if (Invoice.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in Invoice.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addInvoice.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (Invoice.DueDate != null)
                {
                    try
                    {
                        addInvoice.DueDate = Convert.ToDateTime(Invoice.DueDate);
                        addInvoice.DueDateSpecified = true;
                    }
                    catch { }
                }
                if (Invoice.ShipDate != null)
                {
                    try
                    {
                        addInvoice.ShipDate = Convert.ToDateTime(Invoice.ShipDate);
                        addInvoice.ShipDateSpecified = true;
                    }
                    catch { }
                }



                addInvoice.TrackingNum = Invoice.TrackingNum;

                //Issue No. 417
                if (Invoice.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in Invoice.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addInvoice.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (Invoice.ExchangeRate != null)
                {
                    addInvoice.ExchangeRate = Convert.ToDecimal(Invoice.ExchangeRate);
                    addInvoice.ExchangeRateSpecified = true;
                }

                //
                if (Invoice.PrintStatus != null)
                {
                    if (Invoice.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addInvoice.PrintStatus = PrintStatusEnum.NotSet;
                        addInvoice.PrintStatusSpecified = true;
                    }
                    else if (Invoice.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addInvoice.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addInvoice.PrintStatusSpecified = true;
                    }
                    else if (Invoice.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addInvoice.PrintStatus = PrintStatusEnum.PrintComplete;
                        addInvoice.PrintStatusSpecified = true;
                    }
                }
                if (Invoice.EmailStatus != null)
                {
                    if (Invoice.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addInvoice.EmailStatus = EmailStatusEnum.NotSet;
                        addInvoice.EmailStatusSpecified = true;
                    }
                    else if (Invoice.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addInvoice.EmailStatus = EmailStatusEnum.EmailSent;
                        addInvoice.EmailStatusSpecified = true;
                    }
                    else if (Invoice.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addInvoice.EmailStatus = EmailStatusEnum.NeedToSend;
                        addInvoice.EmailStatusSpecified = true;
                    }
                }
                if (Invoice.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = Invoice.BillEmail;
                    addInvoice.BillEmail = EmailAddress;
                }

                // Issue 556
                if (Invoice.BillEmailCc != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = Invoice.BillEmailCc;
                    addInvoice.BillEmailCc = EmailAddress;
                }

                if (Invoice.BillEmailBcc != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = Invoice.BillEmailBcc;
                    addInvoice.BillEmailBcc = EmailAddress;
                }


                if (Invoice.ApplyTaxAfterDiscount != null)
                {
                    addInvoice.ApplyTaxAfterDiscount = Convert.ToBoolean(Invoice.ApplyTaxAfterDiscount);
                    addInvoice.ApplyTaxAfterDiscountSpecified = true;
                }

                foreach (var accounts in Invoice.DepositToAccountRef)
                {
                    QueryService<Account> customerQueryService = new QueryService<Account>(serviceContext);

                    string accountvalue = string.Empty;
                    if (accounts.Name != null && accounts.Name != string.Empty && accounts.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accounts.Name);
                        addInvoice.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }

                }
                if (Invoice.Deposit != null)
                {
                    addInvoice.Deposit = Convert.ToDecimal(Invoice.Deposit);
                    addInvoice.DepositSpecified = true;
                }

                #endregion
                InvoiceAdded = new Intuit.Ipp.Data.Invoice();
                InvoiceAdded = dataService.Update<Intuit.Ipp.Data.Invoice>(addInvoice);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (InvoiceAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = InvoiceAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = InvoiceAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }
        #endregion
    }
    /// <summary>
    /// checking doc number is present or not in quickbook online for Invoice.
    /// </summary>
    public class OnlineInvoiceQBEntryCollection : Collection<OnlineInvoiceQBEntry>
    {
        public OnlineInvoiceQBEntry FindInvoiceNumberEntry(string docNumber)
        {
            foreach (OnlineInvoiceQBEntry item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }


}
