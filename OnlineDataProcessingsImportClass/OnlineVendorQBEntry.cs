﻿using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Collections.ObjectModel;
using System;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class OnlineVendorQBEntry
    {
        
        #region member
        private string m_Id;
        
        //private string m_SyncTocken;
       
        private string m_Title;
       
        private string m_GivenName;
        private string m_MiddleName;
        private string m_FamilyName;
        private string m_Suffix;
        private string m_DisplayName;
        private string m_CompanyName;
        private string m_PrintOnCheckName;
        private string m_Active;
        private Collection<OnlineEntities.TelephoneNumber> m_PrimaryPhone = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.TelephoneNumber> m_AlternatePhone = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.TelephoneNumber> m_Mobile = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.TelephoneNumber> m_Fax = new Collection<OnlineEntities.TelephoneNumber>();

        private Collection<OnlineEntities.EmailAddressEmployee> m_PrimaryEmailAddr = new Collection<OnlineEntities.EmailAddressEmployee>();
        private Collection<OnlineEntities.EmailAddressEmployee> m_WebAddr = new Collection<OnlineEntities.EmailAddressEmployee>();
       
        private Collection<OnlineEntities.BillAddr> m_PrimaryAddr = new Collection<OnlineEntities.BillAddr>();
        
        //private string m_OtherContactInfo;
        private Collection<OnlineEntities.TelephoneNumber> m_OtherContactInfo = new Collection<OnlineEntities.TelephoneNumber>();
        //private Collection<OnlineEntities.OtherContact> m_OtherContactInfo = new Collection<OnlineEntities.OtherContact>();
        private string m_TaxIdentifier;
        //private string m_TermRef;
        private Collection<OnlineEntities.CustomerRef> m_TermRef = new Collection<OnlineEntities.CustomerRef>();
        private string m_Balance;
        private string m_AcctNum;
        private string m_Vendor1099;
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        private string m_TaxReportingBasis;
        private Collection<OnlineEntities.APAccountRef> m_APAccountRef = new Collection<OnlineEntities.APAccountRef>();

        //594
        private bool m_isAppend;

        #endregion

        #region Constructor
        public OnlineVendorQBEntry()
        {
        }
        #endregion

        #region properties

        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        //public string SyncToken
        //{
        //    get { return m_SyncTocken; }
        //    set { m_SyncTocken = value; }
        //}
       
        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }

        public string GivenName
        {
            get { return m_GivenName; }
            set { m_GivenName = value; }
        }
       
        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }

        public string FamilyName
        {
            get { return m_FamilyName; }
            set { m_FamilyName = value; }
        }

        public string Suffix
        {
            get { return m_Suffix; }
            set { m_Suffix = value; }
        }
        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }
        public string PrintOnCheckName
        {
            get { return m_PrintOnCheckName; }
            set { m_PrintOnCheckName = value; }
        }
        public string Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> PrimaryPhone
        {
            get { return m_PrimaryPhone; }
            set { m_PrimaryPhone = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> AlternatePhone
        {
            get { return m_AlternatePhone; }
            set { m_AlternatePhone = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> Mobile
        {
            get { return m_Mobile; }
            set { m_Mobile = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }

        public Collection<OnlineEntities.EmailAddressEmployee> PrimaryEmailAddr
        {
            get { return m_PrimaryEmailAddr; }
            set { m_PrimaryEmailAddr = value; }
        }
        public Collection<OnlineEntities.EmailAddressEmployee> WebAddr
        {
            get { return m_WebAddr; }
            set { m_WebAddr = value; }
        }
        public Collection<OnlineEntities.BillAddr> BillAddr
        {
            get { return m_PrimaryAddr; }
            set { m_PrimaryAddr = value; }
        }

        public Collection<OnlineEntities.TelephoneNumber> OtherContactInfo
        {
            get { return m_OtherContactInfo; }
            set { m_OtherContactInfo = value; }
        }

        //public Collection<OnlineEntities.OtherContact> OtherContact
        //{
        //    get { return m_OtherContactInfo; }
        //    set { m_OtherContactInfo = value; }
        //}

        //public string OtherContact
        //{
        //    get { return m_OtherContactInfo; }
        //    set { m_OtherContactInfo = value; }
        //}

        public string TaxIdentifier
        {
            get { return m_TaxIdentifier; }
            set { m_TaxIdentifier = value; }
        }
        public Collection<OnlineEntities.CustomerRef> TermRef
        {
            get { return m_TermRef; }
            set { m_TermRef = value; }
        }
        public string Balance
        {
            get { return m_Balance; }
            set { m_Balance = value; }
        }
        public string AcctNum
        {
            get { return m_AcctNum; }
            set { m_AcctNum = value; }
        }
        public string Vendor1099
        {
            get { return m_Vendor1099; }
            set { m_Vendor1099 = value; }
        }
        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public string TaxReportingBasis
        {
            get { return m_TaxReportingBasis; }
            set { m_TaxReportingBasis = value; }
        }
        public Collection<OnlineEntities.APAccountRef> APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        //#region  Public Methods
        // static method for resize array for Line tag  or taxline tag.
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
       

        ///create new transaction record .
        public async System.Threading.Tasks.Task<bool> CreateQBOVendor(OnlineDataProcessingsImportClass.OnlineVendorQBEntry coll)
        {
           
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.Vendor addedVendor = new Intuit.Ipp.Data.Vendor();
            Intuit.Ipp.Data.Vendor VendorAdded = new Intuit.Ipp.Data.Vendor();
            OnlineVendorQBEntry vendor = new OnlineVendorQBEntry();
            vendor = coll;
            try
            {
                #region Add Vendor

                //594
                if (vendor.isAppend == true)
                {
                    addedVendor.sparse = true;
                }

                //if (vendor.Id != null)
                //{
                //    addedVendor.Id = vendor.Id;
                //}

                //if (vendor.SyncToken != null)
                //{
                //    addedVendor.SyncToken = vendor.SyncToken;
                //}

                var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(vendor.DisplayName.Trim());
               
                if (dataVendor.Count > 0)
                {
                    foreach (var data in dataVendor)
                    {
                        addedVendor.Id = data.Id;
                        //addedVendor.SyncToken = data.SyncToken;
                    }
                }

                if (vendor.Title != null)
                {
                    addedVendor.Title = vendor.Title;
                }

                if (vendor.GivenName != null)
                {
                    addedVendor.GivenName = vendor.GivenName;
                }
                if (vendor.MiddleName != null)
                {
                    addedVendor.MiddleName = vendor.MiddleName;
                }
               
                if (vendor.FamilyName != null)
                {
                    addedVendor.FamilyName = vendor.FamilyName;
                }
                if (vendor.Suffix != null)
                {
                    addedVendor.Suffix = vendor.Suffix;
                }
                if (vendor.DisplayName != null)
                {
                    addedVendor.DisplayName = vendor.DisplayName;
                }

                if (vendor.CompanyName != null)
                {
                    addedVendor.CompanyName = vendor.CompanyName;
                }

                if (vendor.PrintOnCheckName != null)
                {
                    addedVendor.CompanyName = vendor.PrintOnCheckName;
                }

               
                if (vendor.Active != null)
                {
                    addedVendor.Active = Convert.ToBoolean(vendor.Active);
                    addedVendor.ActiveSpecified = true;
                }
                Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.PrimaryPhone.Count > 0)
                {
                    foreach (var primaryPhone in vendor.PrimaryPhone)
                    {
                        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                    }
                    addedVendor.PrimaryPhone = primaryPhoneNo;
                }

                Intuit.Ipp.Data.TelephoneNumber altNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.AlternatePhone.Count > 0)
                {
                    foreach (var altPhn in vendor.AlternatePhone)
                    {
                        altNo.FreeFormNumber = altPhn.AlternatePhone;
                    }
                    addedVendor.AlternatePhone = altNo;
                }
                Intuit.Ipp.Data.TelephoneNumber mob = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.Mobile.Count > 0)
                {
                    foreach (var mobNo in vendor.Mobile)
                    {
                        mob.FreeFormNumber = mobNo.Mobile;
                    }
                    addedVendor.Mobile = mob;
                }
                Intuit.Ipp.Data.TelephoneNumber fax = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.Fax.Count > 0)
                {
                    foreach (var f in vendor.Fax)
                    {
                        fax.FreeFormNumber = f.Fax;
                    }
                    addedVendor.Fax = fax;
                }

                Intuit.Ipp.Data.EmailAddress emailAddress = new Intuit.Ipp.Data.EmailAddress();
                if (vendor.PrimaryEmailAddr.Count > 0)
                {
                    foreach (var emailAddr in vendor.PrimaryEmailAddr)
                    {
                        emailAddress.Address = emailAddr.EmailAddress;
                    }
                    addedVendor.PrimaryEmailAddr = emailAddress;
                }

                Intuit.Ipp.Data.WebSiteAddress webaddr = new Intuit.Ipp.Data.WebSiteAddress();
                if (vendor.WebAddr.Count > 0)
                {
                    foreach (var emailAddr in vendor.WebAddr)
                    {
                        webaddr.URI = emailAddr.WebAddress;
                    }
                    addedVendor.WebAddr = webaddr;
                }

                PhysicalAddress primaryAddr = new PhysicalAddress();
                if (vendor.BillAddr.Count > 0)
                {
                    foreach (var address in vendor.BillAddr)
                    {
                        primaryAddr.Line1 = address.BillLine1;
                        primaryAddr.Line2 = address.BillLine2;
                        primaryAddr.Line3 = address.BillLine3;
                        primaryAddr.City = address.BillCity;
                        primaryAddr.Country = address.BillCountry;
                        primaryAddr.CountrySubDivisionCode = address.BillCountrySubDivisionCode;
                        primaryAddr.PostalCode = address.BillPostalCode;
                        primaryAddr.Note = address.BillNote;
                        primaryAddr.Line4 = address.BillLine4;
                        primaryAddr.Line5 = address.BillLine5;
                        primaryAddr.Lat = address.BillAddrLat;
                        primaryAddr.Long = address.BillAddrLong;
                    }

                    addedVendor.BillAddr = primaryAddr;
                }


                //Intuit.Ipp.Data.TelephoneNumber otherCnt = new Intuit.Ipp.Data.TelephoneNumber();
                //added
                if (vendor.OtherContactInfo.Count > 0)
                {
                    int i = 0;
                    Intuit.Ipp.Data.ContactInfo[] otherCnt1 = new Intuit.Ipp.Data.ContactInfo[(vendor.OtherContactInfo.Count)];

                    foreach (var altPhn in vendor.OtherContactInfo)
                    {

                        ContactInfo cntinfo = new ContactInfo();

                        cntinfo.AnyIntuitObject = altPhn.OtherContactInfo;
                        
                                               
                        //otherCnt1[i].AnyIntuitObject = altPhn.OtherContactInfo;

                        //otherCnt1[i].AnyIntuitObject = altPhn.OtherContactInfo;
                        //addedVendor.OtherContactInfo.;
                        //otherCnt1[i].AnyIntuitObject = vendor.OtherContactInfo;

                        //otherCnt1[i].AnyIntuitObject = otherCnt1[i];                     
                        //otherCnt1[i].AnyIntuitObject(OtherContact);

                        cntinfo.Type = ContactTypeEnum.TelephoneNumber;
                        cntinfo.TypeSpecified = true;

                        //otherCnt1[i].Type = ContactTypeEnum.TelephoneNumber;
                        //otherCnt1[i].TypeSpecified = true;
                        //i++;
                    }

                    //if (vendor.OtherContactInfo != null)
                    //{
                    //    otherCnt1[i].AnyIntuitObject = vendor.OtherContactInfo;

                    //}

                    addedVendor.OtherContactInfo = otherCnt1;
                }
                //added
                if (vendor.TaxIdentifier != null)
                {
                    addedVendor.TaxIdentifier = vendor.TaxIdentifier;
                }
                //added
                foreach (var TermRef in vendor.TermRef)
                {
                    string classValue = string.Empty;
                   
                    if (TermRef.Name != null)
                    {
                        if (TermRef.Name.Contains("'"))
                        {
                            TermRef.Name = TermRef.Name.Replace("'", "\\'");
                        }

                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(TermRef.Name.Trim());

                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedVendor.TermRef = new ReferenceType()
                        {
                            name = TermRef.Name,
                            Value = classValue
                        };
                    }
                }
                //added
                if (vendor.Balance != null)
                {
                    addedVendor.Balance = Convert.ToDecimal(vendor.Balance);
                    addedVendor.BalanceSpecified = true;
                }
                //added
                //AccountClass AccountClass = new AccountClass();
                if (vendor.AcctNum != null)
                {
                    addedVendor.AcctNum = vendor.AcctNum;
                }
                //added
                if (vendor.Vendor1099 != null)
                {
                    addedVendor.Vendor1099 = Convert.ToBoolean(vendor.Vendor1099);
                    addedVendor.Vendor1099Specified = true;
                }

                
                if (vendor.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in vendor.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedVendor.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #region find APAccountRef
                //QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                //string accountNumber = string.Empty;
                //string accountName = string.Empty;
                //foreach (var acc in vendor.APAccountRef)
                //{
                //    accountName = acc.Name;
                //    //accountNumber = acc.AcctNum;
                //}
                //string accountvalue = string.Empty;
                //if (accountName != string.Empty && accountName != null && accountName != "")
                //{
                //    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                //    addedVendor.APAccountRef = new ReferenceType()
                //    {
                //        name = accDetails.name,
                //        Value = accDetails.Value
                //    };
                //}
                foreach (var APAccountRef in vendor.APAccountRef)
                {
                    if (APAccountRef.Name != null && APAccountRef.Name != string.Empty && APAccountRef.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(APAccountRef.Name);
                        addedVendor.APAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }

                }


               #endregion
                //#endregion
                VendorAdded = new Intuit.Ipp.Data.Vendor();
                VendorAdded = dataService.Add<Intuit.Ipp.Data.Vendor>(addedVendor);
                #endregion
            }

            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (VendorAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = VendorAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = VendorAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        public async System.Threading.Tasks.Task<bool> UpdateQBOVendor(OnlineDataProcessingsImportClass.OnlineVendorQBEntry coll, string Id, string syncToken, Intuit.Ipp.Data.Vendor PreviousData = null)
        {
            
            DataService dataService = null;
            Intuit.Ipp.Data.Vendor VendorAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.Vendor addedVendor = new Intuit.Ipp.Data.Vendor();
            OnlineVendorQBEntry vendor = new OnlineVendorQBEntry();
            vendor = coll;

            try
            {
                #region Add Vendor
                if (vendor.isAppend)
                {
                    addedVendor = PreviousData;
                    addedVendor.sparse = true;
                    addedVendor.sparseSpecified = true;
                }
                addedVendor.Id = Id;
                addedVendor.SyncToken = syncToken;
              
                if (vendor.Title != null)
                {
                    addedVendor.Title = vendor.Title;
                }

                if (vendor.GivenName != null)
                {
                    addedVendor.GivenName = vendor.GivenName;
                }
                if (vendor.MiddleName != null)
                {
                    addedVendor.MiddleName = vendor.MiddleName;
                }

                if (vendor.FamilyName != null)
                {
                    addedVendor.FamilyName = vendor.FamilyName;
                }
                if (vendor.Suffix != null)
                {
                    addedVendor.Suffix = vendor.Suffix;
                }
                if (vendor.DisplayName != null)
                {
                    addedVendor.DisplayName = vendor.DisplayName;
                }

                if (vendor.CompanyName != null)
                {
                    addedVendor.CompanyName = vendor.CompanyName;
                }

                if (vendor.PrintOnCheckName != null)
                {
                    addedVendor.CompanyName = vendor.PrintOnCheckName;
                }


                if (vendor.Active != null)
                {
                    addedVendor.Active = Convert.ToBoolean(vendor.Active);
                    addedVendor.ActiveSpecified = true;
                }
                Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.PrimaryPhone.Count > 0)
                {
                    foreach (var primaryPhone in vendor.PrimaryPhone)
                    {
                        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                    }
                    addedVendor.PrimaryPhone = primaryPhoneNo;
                }

                Intuit.Ipp.Data.TelephoneNumber altNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.AlternatePhone.Count > 0)
                {
                    foreach (var altPhn in vendor.AlternatePhone)
                    {
                        altNo.FreeFormNumber = altPhn.AlternatePhone;
                    }
                    addedVendor.AlternatePhone = altNo;
                }
                Intuit.Ipp.Data.TelephoneNumber mob = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.Mobile.Count > 0)
                {
                    foreach (var mobNo in vendor.Mobile)
                    {
                        mob.FreeFormNumber = mobNo.Mobile;
                    }
                    addedVendor.Mobile = mob;
                }
                Intuit.Ipp.Data.TelephoneNumber fax = new Intuit.Ipp.Data.TelephoneNumber();
                if (vendor.Fax.Count > 0)
                {
                    foreach (var f in vendor.Fax)
                    {
                        fax.FreeFormNumber = f.Fax;
                    }
                    addedVendor.Fax = fax;
                }

                Intuit.Ipp.Data.EmailAddress emailAddress = new Intuit.Ipp.Data.EmailAddress();
                if (vendor.PrimaryEmailAddr.Count > 0)
                {
                    foreach (var emailAddr in vendor.PrimaryEmailAddr)
                    {
                        emailAddress.Address = emailAddr.EmailAddress;
                    }
                    addedVendor.PrimaryEmailAddr = emailAddress;
                }

                Intuit.Ipp.Data.WebSiteAddress webaddr = new Intuit.Ipp.Data.WebSiteAddress();
                if (vendor.WebAddr.Count > 0)
                {
                    foreach (var emailAddr in vendor.WebAddr)
                    {
                        webaddr.URI = emailAddr.WebAddress;
                    }
                    addedVendor.WebAddr = webaddr;
                }

                PhysicalAddress primaryAddr = new PhysicalAddress();
                if (vendor.BillAddr.Count > 0)
                {
                    foreach (var address in vendor.BillAddr)
                    {
                        primaryAddr.Line1 = address.BillLine1;
                        primaryAddr.Line2 = address.BillLine2;
                        primaryAddr.Line3 = address.BillLine3;
                        primaryAddr.City = address.BillCity;
                        primaryAddr.Country = address.BillCountry;
                        primaryAddr.CountrySubDivisionCode = address.BillCountrySubDivisionCode;
                        primaryAddr.PostalCode = address.BillPostalCode;
                        primaryAddr.Note = address.BillNote;
                        primaryAddr.Line4 = address.BillLine4;
                        primaryAddr.Line5 = address.BillLine5;
                        primaryAddr.Lat = address.BillAddrLat;
                        primaryAddr.Long = address.BillAddrLong;
                    }

                    addedVendor.BillAddr = primaryAddr;
                }

                //added
                if (vendor.OtherContactInfo.Count > 0)
                {
                    int i = 0;
                    Intuit.Ipp.Data.ContactInfo[] otherCnt1 = new Intuit.Ipp.Data.ContactInfo[(vendor.OtherContactInfo.Count)];

                    foreach (var altPhn in vendor.OtherContactInfo)
                    {
                        otherCnt1[i].AnyIntuitObject = altPhn.OtherContactInfo;
                        otherCnt1[i].Type = ContactTypeEnum.TelephoneNumber;
                        otherCnt1[i].TypeSpecified = true;
                        i++;
                    }
                    addedVendor.OtherContactInfo = otherCnt1;
                }
                //added
                if (vendor.TaxIdentifier != null)
                {
                    addedVendor.TaxIdentifier = vendor.TaxIdentifier;
                }
                //added
                foreach (var TermRef in vendor.TermRef)
                {
                    string classValue = string.Empty;
                    if (TermRef.Name != null)
                    {
                        if (TermRef.Name.Contains("'"))
                        {
                            TermRef.Name = TermRef.Name.Replace("'", "\\'");
                        }
                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(TermRef.Name.Trim());

                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedVendor.TermRef = new ReferenceType()
                        {
                            name = TermRef.Name,
                            Value = classValue
                        };
                    }
                }
                //added
                if (vendor.Balance != null)
                {
                    addedVendor.Balance = Convert.ToDecimal(vendor.Balance);
                    addedVendor.BalanceSpecified = true;
                }
                //added
                //AccountClass AccountClass = new AccountClass();
                if (vendor.AcctNum != null)
                {
                    addedVendor.AcctNum = vendor.AcctNum;
                }
                //added
                if (vendor.Vendor1099 != null)
                {
                    addedVendor.Vendor1099 = Convert.ToBoolean(vendor.Vendor1099);
                    addedVendor.Vendor1099Specified = true;
                }

                
                if (vendor.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in vendor.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedVendor.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #region find APAccountRef
                //QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                //string accountNumber = string.Empty;
                //string accountName = string.Empty;
                //foreach (var acc in vendor.APAccountRef)
                //{
                //    accountName = acc.Name;
                //    //accountNumber = acc.AcctNum;
                //}
                //string accountvalue = string.Empty;
                //if (accountName != string.Empty && accountName != null && accountName != "")
                //{
                //    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                //    addedVendor.APAccountRef = new ReferenceType()
                //    {
                //        name = accDetails.name,
                //        Value = accDetails.Value
                //    };
                //}
                foreach (var APAccountRef in vendor.APAccountRef)
                {
                    if (APAccountRef.Name != null && APAccountRef.Name != string.Empty && APAccountRef.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(APAccountRef.Name);
                        addedVendor.APAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }

                }
                #endregion
                //#endregion
                VendorAdded = new Intuit.Ipp.Data.Vendor();
                VendorAdded = dataService.Update<Intuit.Ipp.Data.Vendor>(addedVendor);

                #endregion
                
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (VendorAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = VendorAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = VendorAdded.SyncToken;
                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }
    }

    public class OnlineVendorQBEntryCollection : Collection<OnlineVendorQBEntry>
    {
             
    }

}
