﻿
using System;
using System.Text;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public  class ItemClass
    {

      #region member
      private string m_Name;
      private string m_Description;
      private string m_Active;
      private string m_Taxable;
      private string m_FullyQualifiedName;
      //P Axis 13.1 : issue 629
      private string m_SKU;

      private string m_SalesTaxIncluded;
      private string m_UnitPrice;
      private string m_RatePercent;
      private string m_Type;   
      private Collection<OnlineEntities.IncomeAccountRef> m_IncomeAccountRef = new Collection<IncomeAccountRef>();
      private string m_PurchaseDesc;
      private string m_PurchaseTaxIncluded;
      private string m_PurchaseCost;
      private Collection<OnlineEntities.ExpenseAccountRef> m_ExpenseAccountRef = new Collection<ExpenseAccountRef>();
      private Collection<OnlineEntities.AssetAccountRef> m_AssetAccountRef = new Collection<AssetAccountRef>();
      private string m_TrackQtyOnHand;
      private string m_QtyOnHand;
      private Collection<OnlineEntities.SalesTaxCodeRef> m_SalesTaxCodeRef = new Collection<SalesTaxCodeRef>();
      private Collection<OnlineEntities.PurchaseTaxCodeRef> m_PurchaseTaxCodeRef = new Collection<PurchaseTaxCodeRef>();
      private string m_InvStartDate;
      private Collection<OnlineEntities.ParentRef> m_ParentRef = new Collection<OnlineEntities.ParentRef>();
      private string m_Level;
      //594
      private bool m_isAppend;
      #endregion

      #region Constructor
      public ItemClass()
      {
      
      }
      #endregion

      #region Properties
      public string Name
      {
          get {return m_Name;}
          set { m_Name = value; }
      }
      public string Description
      {
          get { return m_Description; }
          set { m_Description = value; }
      }
      public string Active
      {
          get { return m_Active; }
          set { m_Active = value; }
      }
      public string FullyQualifiedName
      {
          get { return m_FullyQualifiedName;}
          set { m_FullyQualifiedName = value; }
      }

        //P Axis 13.1 : issue 629
        public string SKU
        {
            get { return m_SKU; }
            set { m_SKU = value; }
        }
        public string Taxable
      {
          get { return m_Taxable; }
          set { m_Taxable = value; }
      }
      public string SalesTaxIncluded
      {
          get { return m_SalesTaxIncluded; }
          set { m_SalesTaxIncluded = value; }
      }
      public string UnitPrice
      {
          get { return m_UnitPrice; }
          set { m_UnitPrice = value; }
      }
      public string RatePercent
      {
          get { return m_RatePercent; }
          set { m_RatePercent = value; }
      }
      public string Type
      {
          get { return m_Type; }
          set { m_Type = value; }
      }
     
      public Collection<OnlineEntities.IncomeAccountRef> IncomeAccountRef
      {
          get { return m_IncomeAccountRef; }
          set { m_IncomeAccountRef = value; }
      }
      public string PurchaseDesc
      {
          get { return m_PurchaseDesc; }
          set { m_PurchaseDesc = value; }
      }
      public string PurchaseTaxIncluded
      {
          get { return m_PurchaseTaxIncluded; }
          set { m_PurchaseTaxIncluded = value; }
      }
      public string PurchaseCost
      {
          get { return m_PurchaseCost; }
          set { m_PurchaseCost = value; }
      }
      public Collection<OnlineEntities.ExpenseAccountRef> ExpenseAccountRef
      {
          get { return m_ExpenseAccountRef; }
          set { m_ExpenseAccountRef = value; }
      }

      public Collection<OnlineEntities.AssetAccountRef> AssetAccountRef
      {
          get { return m_AssetAccountRef; }
          set { m_AssetAccountRef = value; }
      }
      public string TrackQtyOnHand
      {
          get { return m_TrackQtyOnHand; }
          set { m_TrackQtyOnHand = value; }
      }
      public string QtyOnHand
      {
          get { return m_QtyOnHand; }
          set { m_QtyOnHand = value; }
      }
      public Collection<OnlineEntities.SalesTaxCodeRef> SalesTaxCodeRef
      {
          get { return m_SalesTaxCodeRef; }
          set { m_SalesTaxCodeRef = value; }
      }

      public Collection<OnlineEntities.PurchaseTaxCodeRef> PurchaseTaxCodeRef
      {
          get { return m_PurchaseTaxCodeRef; }
          set { m_PurchaseTaxCodeRef = value; }
      }

      public Collection<OnlineEntities.ParentRef> ParentRef
      {
          get { return m_ParentRef; }
          set { m_ParentRef = value; }
      }
      public string Level
      {
          get { return m_Level; }
          set { m_Level = value; }
      }
      public string InvStartDate
      {
          get { return m_InvStartDate; }
          set { m_InvStartDate = value; }
      }

      //594
      public bool isAppend
      {
          get { return m_isAppend; }
          set { m_isAppend = value; }
      }

        #endregion

      #region  Public Methods

        /// <summary>

        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        /// static method for resize array for Line tag  or taxline tag.
      public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
      {
          Array.Resize(ref lines, linecount);
          lines[linecount - 1] = line;
          return lines;
      }
        /// Creating new Item transaction in quickbook online. 
      public async System.Threading.Tasks.Task<bool> CreateQBOItem(OnlineDataProcessingsImportClass.ItemClass coll)
      {

          DataService dataService = null;
          ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
          dataService = new DataService(serviceContext);

          Intuit.Ipp.Data.Item itemAdded = null;
          Intuit.Ipp.Data.Item addedItem = new Intuit.Ipp.Data.Item();
          ItemClass ItemClass = new ItemClass();
     
          ItemClass = coll;
          try
          {
                #region Add Item
              var dataItem1 = await CommonUtilities.GetItemNameExistsInOnlineQBO(ItemClass.Name.Trim());
               
              if (dataItem1.Count > 0)
              {

                  foreach (var data in dataItem1)
                  {
                      addedItem.Id = data.Id;
                      addedItem.SyncToken = data.SyncToken;
                  }
              }
              if (ItemClass.Name != null)
              {
                  addedItem.Name = ItemClass.Name;
              }
              if (ItemClass.Description != null)
              {
                  addedItem.Description = ItemClass.Description;
              }
              if (ItemClass.Active != null && ItemClass.Active != "")
              {
                  try
                  {
                      addedItem.Active = Convert.ToBoolean(ItemClass.Active);
                      addedItem.ActiveSpecified = true;
                  }
                  catch { }
              }
              if (ItemClass.FullyQualifiedName != null)
              {
                  addedItem.FullyQualifiedName = ItemClass.FullyQualifiedName;                  
              }
              if (ItemClass.Taxable != null && ItemClass.Taxable != "")
              {
                  try{
                  addedItem.Taxable = Convert.ToBoolean(ItemClass.Taxable);
                  addedItem.TaxableSpecified = true;
                  }
                  catch { }
              }
              if (ItemClass.SalesTaxIncluded != null && ItemClass.SalesTaxIncluded != "")
              {
                  try
                  {
                      addedItem.SalesTaxIncluded = Convert.ToBoolean(ItemClass.SalesTaxIncluded);
                      addedItem.SalesTaxIncludedSpecified = true;
                  }
                  catch { }
              }
              if (ItemClass.UnitPrice != null)
              {
                  try{
                  addedItem.UnitPrice = Convert.ToDecimal(ItemClass.UnitPrice);
                  addedItem.UnitPriceSpecified = true;
                  }
                  catch { }
              }

              if (ItemClass.Type != null && ItemClass.Type != "")
              {
                  if (ItemClass.Type == ItemTypeEnum.Inventory.ToString())
                  {
                      addedItem.Type = ItemTypeEnum.Inventory;
                      addedItem.TypeSpecified = true;
                  }
                  else if (ItemClass.Type == ItemTypeEnum.NonInventory.ToString())
                  {
                      addedItem.Type = ItemTypeEnum.NonInventory;
                      addedItem.TypeSpecified = true;
                  }
                  else if (ItemClass.Type == ItemTypeEnum.Service.ToString())
                  {
                      addedItem.Type = ItemTypeEnum.Service;
                      addedItem.TypeSpecified = true;
                  }
                    else if (ItemClass.Type == ItemTypeEnum.Category.ToString()) //Axis 791
                    {
                        addedItem.Type = ItemTypeEnum.Category;
                        addedItem.TypeSpecified = true;
                    }
                }            

              if (ItemClass.RatePercent != null && ItemClass.RatePercent != "")
                {
                    addedItem.RatePercent = Convert.ToDecimal(ItemClass.RatePercent);
                    addedItem.RatePercentSpecified = true;
                }
                if (ItemClass.PurchaseDesc != null)
                {
                    addedItem.PurchaseDesc = ItemClass.PurchaseDesc;
                }
                if (ItemClass.PurchaseTaxIncluded != null && ItemClass.PurchaseTaxIncluded!="")
                {
                    try
                    {
                        addedItem.PurchaseTaxIncluded = Convert.ToBoolean(ItemClass.PurchaseTaxIncluded);
                        addedItem.PurchaseTaxIncludedSpecified = true;
                    }
                    catch { }
                }
                if (ItemClass.PurchaseCost != null && ItemClass.PurchaseCost != "")
                {
                    try
                    {
                        addedItem.PurchaseCost = Convert.ToDecimal(ItemClass.PurchaseCost);
                        addedItem.PurchaseCostSpecified = true;
                    }
                    catch { }
                }
                if (ItemClass.Type == ItemTypeEnum.Inventory.ToString())
                {
                    if (ItemClass.TrackQtyOnHand != null && ItemClass.TrackQtyOnHand != "")
                    {
                        addedItem.TrackQtyOnHand = Convert.ToBoolean(ItemClass.TrackQtyOnHand);
                        addedItem.TrackQtyOnHandSpecified = true;
                    }
                    if (ItemClass.QtyOnHand != null && ItemClass.QtyOnHand != "")
                    {
                        addedItem.QtyOnHand = Convert.ToDecimal(ItemClass.QtyOnHand);
                        addedItem.QtyOnHandSpecified = true;
                    }

                    if (ItemClass.InvStartDate != null && ItemClass.InvStartDate != "")
                    {
                        try
                        {
                            addedItem.InvStartDate = Convert.ToDateTime(ItemClass.InvStartDate);
                            addedItem.InvStartDateSpecified = true;
                        }
                        catch { }

                    }
                }

                //P Axis 13.1 : issue 629
                if (ItemClass.SKU != null && ItemClass.SKU != "")
                {
                    addedItem.Sku = ItemClass.SKU;
                }

                #region IncomeAccountRef
                OnlineEntities.IncomeAccountRef incomeAccount = new OnlineEntities.IncomeAccountRef();
                foreach (var inaccount in ItemClass.IncomeAccountRef)
                {
                    if (ItemClass.IncomeAccountRef.Count > 0)
                    {
                        if (inaccount.Name != string.Empty && inaccount.Name != null && inaccount.Name != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(inaccount.Name);
                           addedItem.IncomeAccountRef  = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       
                    }    
                }
                #endregion
                #region ExpenseAccountRef
                OnlineEntities.ExpenseAccountRef expenseAccount = new OnlineEntities.ExpenseAccountRef();
                foreach (var exaccount in ItemClass.ExpenseAccountRef)
                {
                    if (ItemClass.ExpenseAccountRef.Count > 0)
                    {
                        if (exaccount.Name != string.Empty && exaccount.Name != null && exaccount.Name != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(exaccount.Name);
                            addedItem.ExpenseAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                        
                    }                   
                }
                #endregion
                #region AssetAccountRef
                if (ItemClass.Type == ItemTypeEnum.Inventory.ToString())
                {
                    OnlineEntities.AssetAccountRef assetAccount = new OnlineEntities.AssetAccountRef();
                    foreach (var assaccount in ItemClass.AssetAccountRef)
                    {
                        if (ItemClass.AssetAccountRef.Count > 0)
                        {
                            if (assaccount.Name != null && assaccount.Name != string.Empty && assaccount.Name != "")
                            {
                                ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(assaccount.Name);
                                addedItem.AssetAccountRef = new ReferenceType()
                                {
                                    name = accDetails.name,
                                    Value = accDetails.Value
                                };
                            }
                        
                        }
                    }
                }
                #endregion
                #region SalesTax Code
                OnlineEntities.SalesTaxCodeRef salesTaxCode = new OnlineEntities.SalesTaxCodeRef();
                foreach (var inaccount in ItemClass.SalesTaxCodeRef)
                {
                    if (ItemClass.SalesTaxCodeRef.Count > 0)
                    {
                        if (inaccount.Name == "TAX")
                        {
                            addedItem.SalesTaxCodeRef = new ReferenceType()
                            {
                                name = "TAX",
                                Value = "TAX"
                            };
                        }
                        else if (inaccount.Name == "NON")
                        {
                            addedItem.SalesTaxCodeRef = new ReferenceType()
                            {
                                name = "NON",
                                Value = "NON"
                            };
                        }
                        else
                        {
                            string itemValue = string.Empty;
                            var dataItem = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(inaccount.Name.Trim());
                            foreach (var account in dataItem)
                            {
                                salesTaxCode.Name = account.Name;
                                itemValue = account.Id;

                                addedItem.SalesTaxCodeRef = new ReferenceType()
                                {

                                    name = salesTaxCode.Name,
                                    Value = itemValue

                                };
                            }
                        }
                    }
                }
                #endregion
                #region PurchaseTax code
                OnlineEntities.PurchaseTaxCodeRef purchaseTaxCode = new OnlineEntities.PurchaseTaxCodeRef();
                foreach (var inaccount in ItemClass.PurchaseTaxCodeRef)
                {
                    if (ItemClass.PurchaseTaxCodeRef.Count > 0)
                    {
                        if (inaccount.Name == "TAX")
                        {
                            addedItem.PurchaseTaxCodeRef = new ReferenceType()
                            {
                                name = "TAX",
                                Value = "TAX"
                            };
                        }
                        else if (inaccount.Name == "NON")
                        {
                            addedItem.PurchaseTaxCodeRef = new ReferenceType()
                            {
                                name = "NON",
                                Value = "NON"
                            };
                        }
                        else
                        {
                            string itemValue = string.Empty;
                            var dataItem = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(inaccount.Name.Trim());
                            foreach (var account in dataItem)
                            {
                                purchaseTaxCode.Name = account.Name;
                                itemValue = account.Id;
                            }
                            addedItem.PurchaseTaxCodeRef = new ReferenceType()
                            {
                                name = purchaseTaxCode.Name,
                                Value = itemValue
                            };
                        }

                    }
                }
                #endregion
                #region ParentRef

                if (ItemClass.ParentRef.Count != 0)
                {
                    addedItem.SubItem = true;
                    addedItem.SubItemSpecified = true;
                    string Itmvalue = string.Empty;

                    string[] arr = new string[15];
                    arr = ItemClass.FullyQualifiedName.Split(':');

                    string Itemvalue = null;

                  for (int i = 0; i < arr.Length - 1; i++)
                  {
                      Itemvalue = Itemvalue + ":" + arr[i];
                      StringBuilder sb = new StringBuilder(Itemvalue);

                      Itemvalue = sb.ToString();
                      if (i == 0)
                      {
                          sb.Remove(0, 1);
                          Itemvalue = sb.ToString();
                      }
                  }
                    var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(Itemvalue.Trim());
                    foreach (var item in dataItem)
                    {
                        Itmvalue = item.Id;
                    }
                    addedItem.ParentRef = new ReferenceType()
                    {
                        name = Itemvalue,
                        Value = Itmvalue
                    };

                }
                if (ItemClass.Level != null)
                {
                    addedItem.Level = Convert.ToInt32(ItemClass.Level);
                    addedItem.LevelSpecified = true;
                }
                #endregion

              #endregion
              itemAdded = new Intuit.Ipp.Data.Item();
              itemAdded = dataService.Update<Item>(addedItem);
          }
          #region error
          catch (Intuit.Ipp.Exception.IdsException ex)
          {
              CommonUtilities.GetInstance().getError = string.Empty;
              CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

          }
          catch (WebException ex)
          {
              CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
              //MessageBox.Show(CommonUtilities.GetInstance().getError);
          }
          finally
          {
              if (CommonUtilities.GetInstance().getError == string.Empty)
              {
                  if (itemAdded.domain == "QBO")
                  {
                      CommonUtilities.GetInstance().TxnId = itemAdded.Id;
                      CommonUtilities.GetInstance().SyncToken = itemAdded.SyncToken;

                  }
              }
              else
              {
              }

          }
          if (CommonUtilities.GetInstance().getError != string.Empty)
          {
              return false;
          }
          else
          {
              return true;
          }
          #endregion
      }

        /// <summary>
        /// updating Item entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="itemId"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
      public async System.Threading.Tasks.Task<bool> UpdateQBOItem(OnlineDataProcessingsImportClass.ItemClass coll, string itemId, string syncToken, Intuit.Ipp.Data.Item PreviousData = null)
      {
         
          DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);       

          Intuit.Ipp.Data.Item itemAdded = null;
          Intuit.Ipp.Data.Item addedItem = new Intuit.Ipp.Data.Item();
          ItemClass ItemClass = new ItemClass();
          ItemClass = coll;
          QueryService<Item> CustomerQueryService = new QueryService<Item>(serviceContext);
          try
          {

              #region Add Item

              addedItem.Id = itemId;
              addedItem.SyncToken = syncToken;
              if (ItemClass.isAppend == true)
              {
                  addedItem = PreviousData;

                  addedItem.sparse = true;
                  addedItem.sparseSpecified = true;
              }
              if (ItemClass.Name != null)
              {
                  addedItem.Name = ItemClass.Name;
              }
              if (ItemClass.Description != null)
              {
                  addedItem.Description = ItemClass.Description;
              }
              if (ItemClass.Active != null && ItemClass.Active != "")
              {
                  try
                  {
                      addedItem.Active = Convert.ToBoolean(ItemClass.Active);
                      addedItem.ActiveSpecified = true;
                  }
                  catch { }
              }
              if (ItemClass.FullyQualifiedName != null)
              {
                  addedItem.FullyQualifiedName = ItemClass.FullyQualifiedName;                  
              }
              if (ItemClass.Taxable != null &&ItemClass.Taxable !="")
              {
                  try
                  {
                      addedItem.Taxable = Convert.ToBoolean(ItemClass.Taxable);
                      addedItem.TaxableSpecified = true;
                  }
                  catch { }
              }
              if (ItemClass.SalesTaxIncluded != null &&ItemClass.SalesTaxIncluded != "")
              {
                  try
                  {
                      addedItem.SalesTaxIncluded = Convert.ToBoolean(ItemClass.SalesTaxIncluded);
                      addedItem.SalesTaxIncludedSpecified = true;
                  }
                  catch { }
              }
              if (ItemClass.UnitPrice != null &&ItemClass.UnitPrice != "")
              {
                  try
                  {
                      addedItem.UnitPrice = Convert.ToDecimal(ItemClass.UnitPrice);
                      addedItem.UnitPriceSpecified = true;
                  }
                  catch { }
              }
              if (ItemClass.Type != null)
              {
                  if (ItemClass.Type == ItemTypeEnum.Inventory.ToString())
                  {
                      addedItem.Type = ItemTypeEnum.Inventory;                      
                  }
                  else if (ItemClass.Type == ItemTypeEnum.NonInventory.ToString())
                  {
                      addedItem.Type = ItemTypeEnum.NonInventory;                      
                  }
                  else if (ItemClass.Type == ItemTypeEnum.Service.ToString())
                  {
                      addedItem.Type = ItemTypeEnum.Service;                     
                  }
                    else if (ItemClass.Type == ItemTypeEnum.Category.ToString()) //Axis 791
                    {
                        addedItem.Type = ItemTypeEnum.Category;
                        addedItem.TypeSpecified = true;
                    }
                    addedItem.TypeSpecified = true;
              }

              if (ItemClass.RatePercent != null && ItemClass.RatePercent != "")
                {
                  try{
                    addedItem.RatePercent = Convert.ToDecimal(ItemClass.RatePercent);
                    addedItem.RatePercentSpecified = true;
                  }
                  catch { }
                }
                if (ItemClass.PurchaseDesc != null)
                {
                    addedItem.PurchaseDesc = ItemClass.PurchaseDesc;
                }
                if (ItemClass.PurchaseTaxIncluded != null && ItemClass.PurchaseTaxIncluded != "")
                {
                    try
                    {
                        addedItem.PurchaseTaxIncluded = Convert.ToBoolean(ItemClass.PurchaseTaxIncluded);
                        addedItem.PurchaseTaxIncludedSpecified = true;
                    
                    }
                  catch { }
                }
                if (ItemClass.PurchaseCost != null && ItemClass.PurchaseCost != "")
                {
                    try{
                    addedItem.PurchaseCost = Convert.ToDecimal(ItemClass.PurchaseCost);
                    addedItem.PurchaseCostSpecified = true;
                    }
                    catch { }
                }
                if (ItemClass.Type == ItemTypeEnum.Inventory.ToString())
                {
                    if (ItemClass.TrackQtyOnHand != null && ItemClass.TrackQtyOnHand != "")
                    {
                        try
                        {
                            addedItem.TrackQtyOnHand = Convert.ToBoolean(ItemClass.TrackQtyOnHand);
                            addedItem.TrackQtyOnHandSpecified = true;
                        }
                        catch { }
                    }
                    if (ItemClass.QtyOnHand != null && ItemClass.QtyOnHand != "")
                    {
                        try{
                        addedItem.QtyOnHand = Convert.ToDecimal(ItemClass.QtyOnHand);
                        addedItem.QtyOnHandSpecified = true;
                        }
                        catch { }
                    }

                    if (ItemClass.InvStartDate != null && ItemClass.InvStartDate != "")
                    {
                        try
                        {
                            addedItem.InvStartDate = Convert.ToDateTime(ItemClass.InvStartDate);
                            addedItem.InvStartDateSpecified = true;
                        }
                        catch { }
                    }
                }

                //P Axis 13.1 : issue 629
                if (ItemClass.SKU != null && ItemClass.SKU != "")
                {
                    addedItem.Sku = ItemClass.SKU;
                }

                #region IncomeAccountRef
                OnlineEntities.IncomeAccountRef incomeAccount = new OnlineEntities.IncomeAccountRef();
                foreach (var inaccount in ItemClass.IncomeAccountRef)
                {
                    if (ItemClass.IncomeAccountRef.Count > 0)
                    {
                        if (inaccount.Name != string.Empty && inaccount.Name != null && inaccount.Name != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(inaccount.Name);
                            addedItem.IncomeAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                    
                    }
                }
                #endregion
                #region ExpenseAccountRef
                OnlineEntities.ExpenseAccountRef expenseAccount = new OnlineEntities.ExpenseAccountRef();
                foreach (var exaccount in ItemClass.ExpenseAccountRef)
                {
                    if (ItemClass.ExpenseAccountRef.Count > 0)
                    {
                        if (exaccount.Name != string.Empty && exaccount.Name != null && exaccount.Name != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(exaccount.Name);
                            addedItem.ExpenseAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       
                    }
                }
                #endregion
                #region AssetAccountRef
                if (ItemClass.Type == ItemTypeEnum.Inventory.ToString())
                {
                    OnlineEntities.AssetAccountRef assetAccount = new OnlineEntities.AssetAccountRef();
                    foreach (var assaccount in ItemClass.AssetAccountRef)
                    {
                        if (ItemClass.AssetAccountRef.Count > 0)
                        {
                            if (assaccount.Name != null && assaccount.Name != string.Empty && assaccount.Name != "")
                            {
                                ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(assaccount.Name);
                                addedItem.AssetAccountRef = new ReferenceType()
                                {
                                    name = accDetails.name,
                                    Value = accDetails.Value
                                };
                            }
                           
                        }
                    }
                }
                #endregion
                #region SalesTax Code
                OnlineEntities.SalesTaxCodeRef salesTaxCode = new OnlineEntities.SalesTaxCodeRef();
                foreach (var inaccount in ItemClass.SalesTaxCodeRef)
                {
                    if (ItemClass.SalesTaxCodeRef.Count > 0)
                    {
                        if (inaccount.Name == "TAX")
                        {
                            addedItem.SalesTaxCodeRef = new ReferenceType()
                            {
                                name = "TAX",
                                Value = "TAX"
                            };
                        }
                        else if (inaccount.Name == "NON")
                        {
                            addedItem.SalesTaxCodeRef = new ReferenceType()
                            {
                                name = "NON",
                                Value = "NON"
                            };
                        }
                        else
                        {
                            string itemValue = string.Empty;
                            var dataItem = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(inaccount.Name.Trim());
                            foreach (var account in dataItem)
                            {
                                salesTaxCode.Name = account.Name;
                                itemValue = account.Id;
                            }

                            addedItem.SalesTaxCodeRef = new ReferenceType()
                            {

                                name = salesTaxCode.Name,
                                Value = itemValue

                            };
                        }

                    }
                }
                #endregion
                #region PurchaseTax code
                OnlineEntities.PurchaseTaxCodeRef purchaseTaxCode = new OnlineEntities.PurchaseTaxCodeRef();
                foreach (var inaccount in ItemClass.PurchaseTaxCodeRef)
                {
                    if (ItemClass.PurchaseTaxCodeRef.Count > 0)
                    {
                        if (inaccount.Name == "TAX")
                        {
                            addedItem.PurchaseTaxCodeRef = new ReferenceType()
                            {
                                name = "TAX",
                                Value = "TAX"
                            };
                        }
                        else if (inaccount.Name == "NON")
                        {
                            addedItem.PurchaseTaxCodeRef = new ReferenceType()
                            {
                                name = "NON",
                                Value = "NON"
                            };
                        }
                        else
                        {
                            string itemValue = string.Empty;
                            var dataItem = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(inaccount.Name.Trim());
                            foreach (var account in dataItem)
                            {
                                purchaseTaxCode.Name = account.Name;
                                itemValue = account.Id;
                            }
                            addedItem.PurchaseTaxCodeRef = new ReferenceType()
                            {
                                name = purchaseTaxCode.Name,
                                Value = itemValue
                            };
                        }

                    }
                }
                #endregion

              #region ParentRef

              if (ItemClass.ParentRef.Count != 0)
              {
                  addedItem.SubItem = true;
                  addedItem.SubItemSpecified = true;
                  string Itmvalue = string.Empty;

                  string[] arr = new string[15];
                  arr = ItemClass.FullyQualifiedName.Split(':');

                  string Itemvalue = null;

                  for (int i = 0; i < arr.Length - 1; i++)
                  {
                      Itemvalue = Itemvalue + ":" + arr[i];
                      StringBuilder sb = new StringBuilder(Itemvalue);

                      Itemvalue = sb.ToString();
                      if (i == 0)
                      {
                          sb.Remove(0, 1);
                          Itemvalue = sb.ToString();
                      }
                  }
                  var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(Itemvalue.Trim());
                  foreach (var item in dataItem)
                  {
                      Itmvalue = item.Id;
                  }
                  addedItem.ParentRef = new ReferenceType()
                  {
                      name = Itemvalue,
                      Value = Itmvalue
                  };

              }
              if (ItemClass.Level != null)
              {
                  addedItem.Level = Convert.ToInt32(ItemClass.Level);
                  addedItem.LevelSpecified = true;
              }        
              #endregion

              #endregion

              itemAdded = new Intuit.Ipp.Data.Item();
              itemAdded = dataService.Update<Item>(addedItem);
          }

          #region error
          catch (Intuit.Ipp.Exception.IdsException ex)
          {
              CommonUtilities.GetInstance().getError = string.Empty;
              CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

          }
          catch (WebException ex)
          {
              CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
              //MessageBox.Show(CommonUtilities.GetInstance().getError);
          }
          finally
          {
              if (CommonUtilities.GetInstance().getError == string.Empty)
              {
                  if (itemAdded.domain == "QBO")
                  {
                      CommonUtilities.GetInstance().TxnId = itemAdded.Id;
                      CommonUtilities.GetInstance().SyncToken = itemAdded.SyncToken;

                  }
              }
              else
              {
              }

          }
          if (CommonUtilities.GetInstance().getError != string.Empty)
          {
              return false;
          }
          else
          {
              return true;
          }
          #endregion
          
      }

      #endregion
           
    }
    /// <summary>
    /// checking Item name is present or not in quickbook online for Item traansaction.
    /// </summary>

  public class OnlineItemQBEntryCollection : Collection<ItemClass>
  {
      public ItemClass FindItemEntry(string Name)
      {
          foreach (ItemClass item in this)
          {
              if (item.Name == Name)
              {
                  return item;
              }
          }
          return null;
      }
  }
}
