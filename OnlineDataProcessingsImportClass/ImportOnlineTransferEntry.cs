﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using OnlineEntities;

namespace DataProcessingBlocks.OnlineDataProcessingsImportClass
{
    class ImportOnlineTransferEntry
    {
        private static ImportOnlineTransferEntry m_ImportOnlineTransferEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineTransferEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import bill class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineTransferEntry GetInstance()
        {
            if (m_ImportOnlineTransferEntry == null)
                m_ImportOnlineTransferEntry = new ImportOnlineTransferEntry();
            return m_ImportOnlineTransferEntry;
        }
        /// <summary>
        /// setting values to bill data table and returns collection.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <returns></returns>
        public OnlineDataProcessingsImportClass.OnlineTransferQBEntryCollection ImportOnlineTransferData(DataTable dt, ref string logDirectory)
        {
            OnlineDataProcessingsImportClass.OnlineTransferQBEntryCollection coll = new OnlineTransferQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;

                    //Employee Validation
                    OnlineDataProcessingsImportClass.OnlineTransferQBEntry OnlineTransfer = new OnlineDataProcessingsImportClass.OnlineTransferQBEntry();
                    if (dt.Columns.Contains("TxnDate"))
                    {
                        OnlineTransfer = coll.FindTransferEntry(dr["TxnDate"].ToString());

                        if (OnlineTransfer == null)
                        {

                            #region if Transaction is null

                            OnlineTransfer = new OnlineTransferQBEntry();

                            if (dt.Columns.Contains("Id"))
                            {
                                #region Validations of ID
                                if (dr["ID"].ToString() != string.Empty)
                                {
                                    if (dr["ID"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ID (" + dr["ID"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.Id = dr["Id"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.Id = dr["Id"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.Id = dr["Id"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.Id = dr["Id"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of TxnDate
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    OnlineTransfer.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    OnlineTransfer.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                OnlineTransfer.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            OnlineTransfer.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        OnlineTransfer.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.FromAccountRef frmaccountRef = new OnlineEntities.FromAccountRef();
                            OnlineDataProcessingsImportClass.OnlineTransferQBEntry Account = new OnlineDataProcessingsImportClass.OnlineTransferQBEntry();
                            Account = new OnlineTransferQBEntry();

                            if (dt.Columns.Contains("FromAccountRef"))
                            {
                                #region Validations of FromAccountRef
                                if (dr["FromAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["FromAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FromAccountRef (" + dr["FromAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.Name = dr["FromAccountRef"].ToString().Substring(0, 30);
                                                //frmaccountRef.Name  = dr["FromAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.Name = dr["FromAccountRef"].ToString();
                                                //frmaccountRef.Name = dr["FromAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.Name = dr["FromAccountRef"].ToString();
                                            //frmaccountRef.Name = dr["FromAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.Name = dr["FromAccountRef"].ToString();
                                        //frmaccountRef.Name = dr["FromAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            Collection<OnlineEntities.FromAccountRef> accountRefCollection = new Collection<OnlineEntities.FromAccountRef>();
                            OnlineEntities.FromAccountRef data = new OnlineEntities.FromAccountRef();
                            data.Name = Account.Name;
                            accountRefCollection.Add(data);
                            OnlineTransfer.FromAccountRef = accountRefCollection;

                            OnlineEntities.ToAccountRef toaccountRef = new OnlineEntities.ToAccountRef();
                            if (dt.Columns.Contains("ToAccountRef"))
                            {
                                #region Validations of ToAccountRef
                                if (dr["ToAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["ToAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ToAccountRef (" + dr["ToAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.Name = dr["ToAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.Name = dr["ToAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.Name = dr["ToAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.Name = dr["ToAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            Collection<OnlineEntities.ToAccountRef> toaccountRefCollection = new Collection<OnlineEntities.ToAccountRef>();
                            OnlineEntities.ToAccountRef todata = new OnlineEntities.ToAccountRef();
                            todata.Name = Account.Name;
                            toaccountRefCollection.Add(todata);
                            OnlineTransfer.ToAccountRef = toaccountRefCollection;


                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.Amount = dr["Amount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.Amount = dr["Amount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.Amount = dr["Amount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Amount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("TransactionLocationType"))
                            {
                                #region Validations of TransactionLocationType
                                if (dr["TransactionLocationType"].ToString() != string.Empty)
                                {
                                    if (dr["TransactionLocationType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            coll.Add(OnlineTransfer);

                            #endregion
                        }
                        else
                        {
                            #region if transfer is not null
                            OnlineTransfer = new OnlineTransferQBEntry();

                            if (dt.Columns.Contains("ID"))
                            {
                                #region Validations of PrivateNote
                                if (dr["ID"].ToString() != string.Empty)
                                {
                                    if (dr["ID"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ID (" + dr["ID"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.Id = dr["ID"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.Id = dr["ID"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.Id = dr["ID"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.Id = dr["ID"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of TxnDate
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    OnlineTransfer.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    OnlineTransfer.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                OnlineTransfer.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            OnlineTransfer.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        OnlineTransfer.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.FromAccountRef frmaccountRef = new OnlineEntities.FromAccountRef();
                            OnlineDataProcessingsImportClass.OnlineTransferQBEntry Account = new OnlineDataProcessingsImportClass.OnlineTransferQBEntry();
                            Account = new OnlineTransferQBEntry();

                            if (dt.Columns.Contains("FromAccountRef"))
                            {
                                #region Validations of FromAccountRef
                                if (dr["FromAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["FromAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FromAccountRef (" + dr["FromAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.Name = dr["FromAccountRef"].ToString().Substring(0, 30);
                                                //frmaccountRef.Name  = dr["FromAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.Name = dr["FromAccountRef"].ToString();
                                                //frmaccountRef.Name = dr["FromAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.Name = dr["FromAccountRef"].ToString();
                                            //frmaccountRef.Name = dr["FromAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.Name = dr["FromAccountRef"].ToString();
                                        //frmaccountRef.Name = dr["FromAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            Collection<OnlineEntities.FromAccountRef> accountRefCollection = new Collection<OnlineEntities.FromAccountRef>();
                            OnlineEntities.FromAccountRef data = new OnlineEntities.FromAccountRef();
                            data.Name = Account.Name;
                            accountRefCollection.Add(data);
                            OnlineTransfer.FromAccountRef = accountRefCollection;

                            OnlineEntities.ToAccountRef toaccountRef = new OnlineEntities.ToAccountRef();
                            if (dt.Columns.Contains("ToAccountRef"))
                            {
                                #region Validations of ToAccountRef
                                if (dr["ToAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["ToAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ToAccountRef (" + dr["ToAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.Name = dr["ToAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.Name = dr["ToAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.Name = dr["ToAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.Name = dr["ToAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            Collection<OnlineEntities.ToAccountRef> toaccountRefCollection = new Collection<OnlineEntities.ToAccountRef>();
                            OnlineEntities.ToAccountRef todata = new OnlineEntities.ToAccountRef();
                            todata.Name = Account.Name;
                            toaccountRefCollection.Add(todata);
                            OnlineTransfer.ToAccountRef = toaccountRefCollection;


                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.Amount = dr["Amount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.Amount = dr["Amount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.Amount = dr["Amount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Amount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("TransactionLocationType"))
                            {
                                #region Validations of TransactionLocationType
                                if (dr["TransactionLocationType"].ToString() != string.Empty)
                                {
                                    if (dr["TransactionLocationType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        OnlineTransfer.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            coll.Add(OnlineTransfer);
                                                      
                          
                            #endregion

                        }

                    }
                    
                }

                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }
    }
}
