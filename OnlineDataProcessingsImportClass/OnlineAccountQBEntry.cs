﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class AccountClass
    {

        #region member
        private string m_Name;
        private string m_SubAccount;
        private Collection<OnlineEntities.ParentRef> m_ParentRef = new Collection<OnlineEntities.ParentRef>();
        private string m_Description;
        private string m_FullyQualifiedName;
        private string m_Active;
        private string m_Classification;
        private string m_AccountType;
        private string m_AccountSubType;
        private string m_AcctNum;
        private string m_CurrentBalance;
        private string m_CurrentBalanceWithSubAccounts;
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        private string m_TaxCodeRef;
        //594
        private bool m_isAppend;
        #endregion

        #region Properties
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string SubAccount
        {
            get { return m_SubAccount; }
            set { m_SubAccount = value; }
        }
        public Collection<OnlineEntities.ParentRef> ParentRef
        {
            get { return m_ParentRef; }
            set { m_ParentRef = value; }
        }

        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public string FullyQualifiedName
        {
            get { return m_FullyQualifiedName; }
            set { m_FullyQualifiedName = value; }
        }
        public string Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }
        public string Classification
        {
            get { return m_Classification; }
            set { m_Classification = value; }
        }
        public string AccountType
        {
            get { return m_AccountType; }
            set { m_AccountType = value; }
        }
        public string AccountSubType
        {
            get { return m_AccountSubType; }
            set { m_AccountSubType = value; }
        }
        public string AcctNum
        {
            get { return m_AcctNum; }
            set { m_AcctNum = value; }
        }
        public string CurrentBalance
        {
            get { return m_CurrentBalance; }
            set { m_CurrentBalance = value; }
        }
        public string CurrentBalanceWithSubAccounts
        {
            get { return m_CurrentBalanceWithSubAccounts; }
            set { m_CurrentBalanceWithSubAccounts = value; }
        }

        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public string TaxCodeRef
        {
            get { return m_TaxCodeRef; }
            set { m_TaxCodeRef = value; }
        }
        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region  Public Methods

        /// <summary>

        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        /// static method for resize array for Line tag  or taxline tag.
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
        /// Creating new Item transaction in quickbook online. 
        public async System.Threading.Tasks.Task<bool> CreateQBOAccount(AccountClass coll)
        {
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Account AccountAdded = null;
            Account addAccount = new Account();
            AccountClass AccountClass = new AccountClass();

            AccountClass = coll;
            try
            {
                #region Add Account
                var dataAcc = await CommonUtilities.GetAccountDetailsExistsInOnlineQBO(AccountClass.Name);
               
                if (dataAcc.Count > 0)
                {
                    foreach (var data in dataAcc)
                    {
                        addAccount.Id = data.Id;
                        addAccount.SyncToken = data.SyncToken;
                    }
                }
                if (AccountClass.Name != null)
                {
                    addAccount.Name = AccountClass.Name;
                }
                if (AccountClass.SubAccount != null && AccountClass.SubAccount != "")
                {
                    try
                    {
                        addAccount.SubAccount = Convert.ToBoolean(addAccount.SubAccount);
                        addAccount.SubAccountSpecified = true;
                    }
                    catch { }
                }

                if (AccountClass.Active != null && AccountClass.Active != "")
                {
                    try
                    {
                        addAccount.Active = Convert.ToBoolean(AccountClass.Active);
                        addAccount.ActiveSpecified = true;
                    }
                    catch { }
                }

                if (AccountClass.Description != null)
                {
                    addAccount.Description = AccountClass.Description;
                }

                if (AccountClass.Classification != null && AccountClass.Classification != "")
                {
                    try
                    {
                        foreach (string name in Enum.GetNames(typeof(AccountClassificationEnum)))
                        {
                            if (AccountClass.Classification.ToLower() == name.ToLower())
                            {
                                addAccount.Classification = (AccountClassificationEnum)Enum.Parse(typeof(AccountClassificationEnum), AccountClass.Classification, true);
                                addAccount.ClassificationSpecified = true;
                            }
                        }
                    }
                    catch (Exception ex) { }
                }
                if (AccountClass.AccountType != null)
                {
                    foreach (string name in Enum.GetNames(typeof(AccountTypeEnum)))
                    {

                        if (AccountClass.AccountType.ToLower() == name.ToLower())
                        {
                            try
                            {
                                addAccount.AccountType = (AccountTypeEnum)Enum.Parse(typeof(AccountTypeEnum), name, true);
                                addAccount.AccountTypeSpecified = true;
                            }
                            catch (Exception ex) { }
                            break;
                        }
                    }

                }
                if (AccountClass.AccountSubType != null)
                {
                    addAccount.AccountSubType = AccountClass.AccountSubType;

                }

                if (AccountClass.AcctNum != null)
                {
                    addAccount.AcctNum = AccountClass.AcctNum;
                }

                if (AccountClass.CurrentBalance != null)
                {
                    try
                    {
                        addAccount.CurrentBalance = Convert.ToDecimal(AccountClass.CurrentBalance);
                        addAccount.CurrentBalanceSpecified = true;
                    }
                    catch { }
                }

                if (AccountClass.CurrentBalanceWithSubAccounts != null)
                {
                    try
                    {
                        addAccount.CurrentBalanceWithSubAccounts = Convert.ToDecimal(AccountClass.CurrentBalanceWithSubAccounts);
                        addAccount.CurrentBalanceWithSubAccountsSpecified = true;
                    }
                    catch { }
                }

                if (AccountClass.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in AccountClass.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (AccountClass.CurrencyRef.Count != 0)
                    {
                        addAccount.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }



                if (AccountClass.TaxCodeRef != null && AccountClass.TaxCodeRef != "")
                {
                    try
                    {
                        TaxCode TaxCode = new TaxCode();
                        TaxCode.Name = AccountClass.TaxCodeRef;
                        string TaxId = string.Empty;
                        string TaxName = string.Empty;
                        TaxName = TaxCode.Name;
                        if (TaxCode.Name != null)
                        {
                            if (TaxName.Contains("'"))
                            {
                                TaxCode.Name = TaxCode.Name.Replace("'", "\\'");
                            }
                            var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name);
                           
                            if (dataTax.Count != 0)
                            {
                                foreach (var tax in dataTax)
                                {
                                    TaxId = tax.Id;
                                }

                                addAccount.TaxCodeRef = new ReferenceType()
                                {
                                    name = TaxName,
                                    Value = TaxId
                                };
                            }
                        }
                    }
                    catch { }
                }

                string ParentRefName = string.Empty;
                string ParentRefId = string.Empty;
                if (AccountClass.FullyQualifiedName != null)
                {
                    if (AccountClass.FullyQualifiedName.Contains(":"))
                    {
                        List<Intuit.Ipp.Data.Account> Accountdata = null;
                        Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountClass.FullyQualifiedName.Trim());

                        string[] arr = new string[6];
                        arr = AccountClass.FullyQualifiedName.Split(':');
                        //if (arr.Length < 6)
                        //{
                        if (Accountdata.Count == 0)
                        {
                            //try
                            //{

                            string id = string.Empty;
                            string Itemvalue = string.Empty;
                            string ParentAcc = string.Empty;

                            Account addParentAccount = new Account();
                            addParentAccount.Active = true;
                            addParentAccount.Classification = addAccount.Classification;
                            addParentAccount.ClassificationSpecified = true;
                            addParentAccount.AccountType = addAccount.AccountType;
                            addParentAccount.AccountTypeSpecified = true;
                            addParentAccount.CurrencyRef = addAccount.CurrencyRef;

                            if (addAccount.AccountSubType != null)
                            {
                                addParentAccount.AccountSubType = addAccount.AccountSubType;
                            }

                            for (int i = 0; i < arr.Length - 1; i++)
                            {


                                ParentAcc = Itemvalue;
                                if (i != 0)
                                {
                                    Itemvalue = Itemvalue + ":" + arr[i];
                                }
                                else
                                {
                                    Itemvalue = arr[i];
                                }
                                StringBuilder sb = new StringBuilder(Itemvalue);

                                Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(Itemvalue.Trim());
                                if (Accountdata.Count == 0)
                                {
                                    addParentAccount.Name = arr[i].Trim();
                                    addParentAccount.FullyQualifiedName = Itemvalue.Trim();
                                    if (i != 0)
                                    {
                                        addParentAccount.ParentRef = new ReferenceType()
                                        {
                                            name = ParentRefName,
                                            Value = ParentRefId
                                        };
                                    }
                                    AccountAdded = new Intuit.Ipp.Data.Account();
                                    AccountAdded = dataService.Update<Intuit.Ipp.Data.Account>(addParentAccount);
                                    ParentRefName = AccountAdded.FullyQualifiedName;
                                    ParentRefId = AccountAdded.Id;
                                }
                                else
                                {
                                    //for (int j = 0; i < arr.Length - 1; j++)
                                    //{


                                    //    ParentAcc = Itemvalue;
                                    //    if (j != 0)
                                    //    {
                                    //        Itemvalue = Itemvalue + ":" + arr[i];
                                    //    }
                                    //    else
                                    //    {
                                    //        Itemvalue = arr[i];
                                    //    }
                                    //}
                                    //Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(Itemvalue.Trim());

                                    foreach (var data in Accountdata)
                                    {
                                        ParentRefName = data.FullyQualifiedName;
                                        ParentRefId = data.Id;
                                    }
                                }
                            }
                            //}
                            //catch (Intuit.Ipp.Exception.IdsException ex)
                            //{
                            //    CommonUtilities.GetInstance().getError = string.Empty;
                            //    CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

                            //}
                        }
                        else
                        {

                            foreach (var data in Accountdata)
                            {
                                addAccount.ParentRef = new ReferenceType()
                                {
                                    name = data.FullyQualifiedName,
                                    Value = data.Id
                                };
                            }
                        }
                        // }

                    }
                    addAccount.FullyQualifiedName = AccountClass.FullyQualifiedName;
                }
                else
                {
                    addAccount.FullyQualifiedName = AccountClass.Name;

                }
                if (ParentRefName != string.Empty || ParentRefId != string.Empty)
                {
                    addAccount.ParentRef = new ReferenceType()
                    {
                        name = ParentRefName,
                        Value = ParentRefId
                    };
                }




                #endregion
                AccountAdded = new Intuit.Ipp.Data.Account();
                AccountAdded = dataService.Update<Intuit.Ipp.Data.Account>(addAccount);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (AccountAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = AccountAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = AccountAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        /// <summary>
        /// updating Item entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="itemId"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOAccount(OnlineDataProcessingsImportClass.AccountClass coll, string itemId, string syncToken, Intuit.Ipp.Data.Account PreviousData = null)
        {

            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.Account AccountAdded = null;
            Intuit.Ipp.Data.Account addAccount = new Intuit.Ipp.Data.Account();
            AccountClass AccountClass = new AccountClass();
            AccountClass = coll;
            QueryService<Account> CustomerQueryService = new QueryService<Account>(serviceContext);
            try
            {

                #region Add Account
             
                if (AccountClass.isAppend)
                {
                    addAccount = PreviousData;
                    addAccount.sparse = true;
                    addAccount.sparseSpecified = true;
                }
                addAccount.Id = itemId;
                addAccount.SyncToken = syncToken;

                if (AccountClass.Name != null)
                {
                    addAccount.Name = AccountClass.Name;
                }
                if (AccountClass.SubAccount != null && AccountClass.SubAccount != "")
                {
                    try
                    {
                        addAccount.SubAccount = Convert.ToBoolean(addAccount.SubAccount);
                        addAccount.SubAccountSpecified = true;
                    }
                    catch { }
                }

                if (AccountClass.FullyQualifiedName != null)
                {
                    addAccount.FullyQualifiedName = AccountClass.FullyQualifiedName;
                }

                if (AccountClass.Active != null && AccountClass.Active != "")
                {
                    try
                    {
                        addAccount.Active = Convert.ToBoolean(AccountClass.Active);
                        addAccount.ActiveSpecified = true;
                    }
                    catch { }
                }

                if (AccountClass.Description != null)
                {
                    addAccount.Description = AccountClass.Description;
                }

                if (AccountClass.Classification != null && AccountClass.Classification != "")
                {
                    if (AccountClass.Classification.ToLower() == "asset")
                    {
                        addAccount.Classification = AccountClassificationEnum.Asset;
                        addAccount.ClassificationSpecified = true;
                    }
                    else if (AccountClass.Classification.ToLower() == "equity")
                    {
                        addAccount.Classification = AccountClassificationEnum.Equity;
                        addAccount.ClassificationSpecified = true;
                    }
                    else if (AccountClass.Classification.ToLower() == "expense")
                    {
                        addAccount.Classification = AccountClassificationEnum.Expense;
                        addAccount.ClassificationSpecified = true;
                    }
                    else if (AccountClass.Classification.ToLower() == "liability")
                    {
                        addAccount.Classification = AccountClassificationEnum.Liability;
                        addAccount.ClassificationSpecified = true;
                    }
                    else if (AccountClass.Classification.ToLower() == "revenue")
                    {
                        addAccount.Classification = AccountClassificationEnum.Revenue;
                        addAccount.ClassificationSpecified = true;
                    }
                }

                if (AccountClass.AccountSubType != null)
                {
                    addAccount.AccountSubType = AccountClass.AccountSubType;
                }

                if (AccountClass.AcctNum != null)
                {
                    addAccount.AcctNum = AccountClass.AcctNum;
                }

                if (AccountClass.CurrentBalance != null)
                {
                    addAccount.CurrentBalance = Convert.ToDecimal(AccountClass.CurrentBalance);
                    addAccount.CurrentBalanceSpecified = true;
                }

                if (AccountClass.CurrentBalanceWithSubAccounts != null)
                {
                    addAccount.CurrentBalanceWithSubAccounts = Convert.ToDecimal(AccountClass.CurrentBalanceWithSubAccounts);
                    addAccount.CurrentBalanceWithSubAccountsSpecified = true;
                }

                if (AccountClass.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in AccountClass.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (AccountClass.CurrencyRef.Count != 0)
                    {
                        addAccount.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }



                if (AccountClass.TaxCodeRef != null && AccountClass.TaxCodeRef != "")
                {
                    try
                    {
                        TaxCode TaxCode = new TaxCode();
                        TaxCode.Name = AccountClass.TaxCodeRef;
                        string TaxId = string.Empty;
                        string TaxName = string.Empty;
                        TaxName = TaxCode.Name;
                        if (TaxCode.Name != null)
                        {
                            if (TaxName.Contains("'"))
                            {
                                TaxCode.Name = TaxCode.Name.Replace("'", "\\'");
                            }

                            var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name);
                           
                            if (dataTax.Count != 0)
                            {
                                foreach (var tax in dataTax)
                                {
                                    TaxId = tax.Id;
                                }

                                addAccount.TaxCodeRef = new ReferenceType()
                                {
                                    name = TaxName,
                                    Value = TaxId
                                };
                            }
                        }
                    }
                    catch { }
                }

                #endregion

                AccountAdded = new Intuit.Ipp.Data.Account();
                AccountAdded = dataService.Update<Intuit.Ipp.Data.Account>(addAccount);
            }

            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (AccountAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = AccountAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = AccountAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

        #endregion
    }
    public class OnlineAccountQBEntryCollection : Collection<AccountClass>
    {
        public AccountClass FindItemEntry(string Name)
        {
            foreach (AccountClass item in this)
            {
                if (item.Name == Name)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
