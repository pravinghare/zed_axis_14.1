﻿
using System;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("SalesReceipt", Namespace = "", IsNullable = false)]

    public class SalesReceipt
    {      

        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TxnDate;
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
      
        private string m_TxnStatus;
        private Collection<OnlineEntities.CustomField> m_CustomField = new Collection<OnlineEntities.CustomField>();
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.CustomerRef> m_CustomerRef = new Collection<OnlineEntities.CustomerRef>();
        private string m_CustomerMemo;
        private Collection<BillAddr> m_BillAddr = new Collection<BillAddr>();
        private Collection<ShipAddr> m_ShipAddr = new Collection<ShipAddr>();
        private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();       
        private Collection<ShipMethodRef> m_ShipMethodRef = new Collection<ShipMethodRef>();    
        private string m_TrackingNum;
        private string m_TotalAmt;
        private string m_ShipDate;        
        private string m_ApplyTaxAfterDiscount;
        private string m_PrintStatus;
        private string m_EmailStatus;
        private string m_BillEmail;
        
        //606
        private string m_PrimaryPhone;
        private string m_PaymentRefNumber;
        private Collection<PaymentMethodRef> m_PaymentMethodRef = new Collection<PaymentMethodRef>();
        private Collection<DepositToAccountRef> m_DepositToAccountRef = new Collection<DepositToAccountRef>();       
        private Collection<CurrencyRef> m_CurrencyRef = new Collection<CurrencyRef>();       
        private string m_ExchangeRate;
        private string m_GlobalTaxCalculation;
        private string m_HomeTotalAmt;
        private string m_TransactionLocationType;
        //594
        private bool m_isAppend;

        #endregion

        #region Constructor
        public SalesReceipt()
        {
        }
        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }


        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }

        public Collection<OnlineEntities.CustomField> CustomField
        {
            get { return m_CustomField; }
            set { m_CustomField = value; }
        }
        

        public string TxnStatus
        {
            get { return m_TxnStatus; }
            set { m_TxnStatus = value; }
        }


        public string CustomerMemo
        {
            get { return m_CustomerMemo; }
            set { m_CustomerMemo = value; }
        }

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }
       

        public Collection<ShipMethodRef> ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }
        public string TrackingNum
        {
            get { return m_TrackingNum; }
            set { m_TrackingNum = value; }
        }


        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }


        public string PrintStatus
        {
            get { return m_PrintStatus; }
            set { m_PrintStatus = value; }
        }


        public string EmailStatus
        {
            get { return m_EmailStatus; }
            set { m_EmailStatus = value; }
        }


        public string BillEmail
        {
            get { return m_BillEmail; }
            set { m_BillEmail = value; }
        }
        public string PaymentRefNumber
        {
            get { return m_PaymentRefNumber; }
            set { m_PaymentRefNumber = value; }
        }       


        public string ApplyTaxAfterDiscount
        {
            get { return m_ApplyTaxAfterDiscount; }
            set { m_ApplyTaxAfterDiscount = value; }
        }


        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }


        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }

        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }

        public string HomeTotalAmt
        {
            get { return m_HomeTotalAmt; }
            set { m_HomeTotalAmt = value; }
        }
        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        public Collection<OnlineEntities.CustomerRef> CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public Collection<BillAddr> BillAddr
        {
            get { return m_BillAddr; }
            set { m_BillAddr = value; }
        }


        public Collection<ShipAddr> ShipAddr
        {
            get { return m_ShipAddr; }
            set { m_ShipAddr = value; }
        }

        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }
        }

        public Collection<PaymentMethodRef> PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }       

        public Collection<DepositToAccountRef> DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
            set { m_DepositToAccountRef = value; }
        }        

        public Collection<CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        //606
        public string PrimaryPhone
        {
            get { return m_PrimaryPhone; }
            set { m_PrimaryPhone = value; }
        }

        #endregion

        #region  Public Methods 
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
      
        /// <summary>
        /// Creating new Salesrecipt transaction in quickbook online. 
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOSalesReceipt(OnlineDataProcessingsImportClass.SalesReceipt coll)
        {
                bool flag = false;
                int linecount = 1;
            
                DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.SalesReceipt SalesReceiptAdded = null;
            Intuit.Ipp.Data.SalesReceipt addedSalesReceipt = new Intuit.Ipp.Data.SalesReceipt();
            SalesReceipt salesReceipt = new SalesReceipt();
            salesReceipt = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;

            //bug 514
            string ratevalue = string.Empty;
            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {
                #region Add Sales Receipt

                //594
                if (salesReceipt.isAppend == true)
                {
                    addedSalesReceipt.sparse = true;
                }

                    if (salesReceipt.DocNumber != null)
                    {
                        addedSalesReceipt.DocNumber = salesReceipt.DocNumber;
                    }
                    else { addedSalesReceipt.AutoDocNumber = true; addedSalesReceipt.AutoDocNumberSpecified = true; }

                    if (salesReceipt.TxnDate != null)
                    {
                        try
                        {
                            addedSalesReceipt.TxnDate = Convert.ToDateTime(salesReceipt.TxnDate);
                            addedSalesReceipt.TxnDateSpecified = true;
                        }
                        catch { }
                    }

                    addedSalesReceipt.PrivateNote = salesReceipt.PrivateNote;

                    #region CustomFiled
                    Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                    foreach (var customData in salesReceipt.CustomField)
                    {
                        custFiled.DefinitionId = customcount.ToString();
                        custFiled.Name = customData.Name;
                        custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                        custFiled.AnyIntuitObject = customData.Value;
                        Array.Resize(ref custom_online, customcount);
                        custom_online[customcount - 1] = custFiled;
                        customcount++;
                        custFiled = new Intuit.Ipp.Data.CustomField();
                    }
                    
                    addedSalesReceipt.CustomField = custom_online;
                    #endregion

                    #region find DepartmentRef  // bug 552
                    
                    string DepartmentName = string.Empty;
                    foreach (var dept in salesReceipt.DepartmentRef)
                    {
                        DepartmentName = dept.Name;
                    }
                    string Departmentvalue = string.Empty;
                 
                    if (DepartmentName != string.Empty)
                    {
                       var dataDepartmentName = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentName.Trim());
                   
                        foreach (var dept in dataDepartmentName)
                        {
                            Departmentvalue = dept.Id;
                        }
                        addedSalesReceipt.DepartmentRef = new ReferenceType()
                        {
                            name = DepartmentName,
                            Value = Departmentvalue
                        };
                    }

                    #endregion
                    #region GlobalTaxCalculation

                    if (salesReceipt.GlobalTaxCalculation != null)
                    {

                        if (CommonUtilities.GetInstance().CountryVersion != "US")
                        {
                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                            {
                                addedSalesReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                                addedSalesReceipt.GlobalTaxCalculationSpecified = true;
                            }
                            else if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                            {
                                addedSalesReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                                addedSalesReceipt.GlobalTaxCalculationSpecified = true;
                            }
                            else
                            {
                                addedSalesReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                                addedSalesReceipt.GlobalTaxCalculationSpecified = true;
                            }
                        }

                    }

                #endregion

                // Axis 742
                if (salesReceipt.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(salesReceipt.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedSalesReceipt.TransactionLocationType = value;
                    }
                    else
                    {
                        addedSalesReceipt.TransactionLocationType = salesReceipt.TransactionLocationType;
                    }
                }

                addedSalesReceipt.TxnStatus = salesReceipt.TxnStatus;

                    foreach (var l in salesReceipt.Line)
                    {
                       if (flag == false)
                       {
                            #region
                           
                           flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if( SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }                              
                               
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }

                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString() )
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                       
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    //Line.Amount = decimal.Round(Line.Amount, 2);
                                                   // Line.Amount = CommonUtilities.GetInstance().GetLineAmount(Line.Amount);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }

                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                       
                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            
                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;                                
                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                    
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedSalesReceipt.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                       }
                       else if (flag == true)
                       {
                            #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;

                        
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                               
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != string.Empty)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;                                               
                            }

                        }                      

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }


                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;   
                            }
                        }

                        addedSalesReceipt.Line = AddLine(lines_online, Line, linecount);       
                        #endregion
                       }
                    }  

              

                #region TxnTaxDetails Bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedSalesReceipt.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedSalesReceipt.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //Issue ;; 586
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in salesReceipt.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                string Taxvalue = string.Empty;

                                if (taxRef.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxRef.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = taxRef.Name,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedSalesReceipt.TxnTaxDetail = txnTaxDetail;
                        }

                        #endregion
                    }
                }

                #endregion


                foreach (var customerRef in salesReceipt.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());

                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }
                        addedSalesReceipt.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }

                }
                ////;;586
                //foreach (var txntaxCode in salesReceipt.TxnTaxDetail)
                //{
                //    #region TxnTaxDetail
                //    ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                //    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                //    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                //    if (txntaxCode.TxnTaxCodeRef != null)
                //    {
                //        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                //        {
                //            QueryService<TaxCode> taxCodeQueryService = new QueryService<TaxCode>(serviceContext);

                //            string Taxvalue = string.Empty;

                //            if (taxRef.Name != null)
                //            {
                //                if (taxRef.Name.Contains("'"))
                //                {
                //                    taxRef.Name = taxRef.Name.Replace("'", "\\'");
                //                }
                //                dataTax = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(taxCodeQueryService.ExecuteIdsQuery("Select * From TaxCode where Name='" + taxRef.Name + "'"));
                //                if (dataTax != null)
                //                {
                //                    foreach (var tax in dataTax)
                //                    {
                //                        Taxvalue = tax.Id;
                //                    }

                //                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                //                    {
                //                        name = taxRef.Name,
                //                        Value = Taxvalue
                //                    };
                //                }
                //            }
                //        }
                //    }

                //    if (txnTaxDetail != null)
                //    {
                //        addedSalesReceipt.TxnTaxDetail = txnTaxDetail;
                //    }

                //    #endregion
                //}


                if (salesReceipt.CustomerMemo != null)
                {
                    addedSalesReceipt.CustomerMemo = new MemoRef()
                    {
                        Value = salesReceipt.CustomerMemo
                    };                   
                }
                PhysicalAddress billAddr = new PhysicalAddress();
                if (salesReceipt.BillAddr.Count>0)
                {
                    foreach (var billaddress in salesReceipt.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedSalesReceipt.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (salesReceipt.ShipAddr.Count>0)
                {
                    foreach (var shipAddress in salesReceipt.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedSalesReceipt.ShipAddr = shipAddr;
                }


                foreach (var className in salesReceipt.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedSalesReceipt.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }
                

                if (salesReceipt.ShipMethodRef.Count>0)
                {
                    string shipName = string.Empty;
                   
                    foreach(var shipData in salesReceipt.ShipMethodRef)
                    {
                        shipName= shipData.Name;
                    }
                    addedSalesReceipt.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (salesReceipt.ShipDate != null)
                {
                    try
                    {
                        addedSalesReceipt.ShipDate = Convert.ToDateTime(salesReceipt.ShipDate);
                        addedSalesReceipt.ShipDateSpecified = true;
                    }
                    catch { }
                }
                addedSalesReceipt.TrackingNum = salesReceipt.TrackingNum;

                if (salesReceipt.PrintStatus != null)
                {
                    if (salesReceipt.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedSalesReceipt.PrintStatus = PrintStatusEnum.NotSet;
                        addedSalesReceipt.PrintStatusSpecified = true;
                    }
                    else if (salesReceipt.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedSalesReceipt.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedSalesReceipt.PrintStatusSpecified = true;
                    }
                    else if (salesReceipt.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedSalesReceipt.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedSalesReceipt.PrintStatusSpecified = true;
                    }
                }
                if (salesReceipt.EmailStatus != null)
                {
                    if (salesReceipt.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedSalesReceipt.EmailStatus = EmailStatusEnum.NotSet;
                        addedSalesReceipt.EmailStatusSpecified = true;
                    }
                    else if (salesReceipt.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedSalesReceipt.EmailStatus = EmailStatusEnum.EmailSent;
                        addedSalesReceipt.EmailStatusSpecified = true;
                    }
                    else if (salesReceipt.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedSalesReceipt.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedSalesReceipt.EmailStatusSpecified = true;
                    }
                }

                ////606
                //Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                //if (salesReceipt.PrimaryPhone.Count > 0)
                //{
                //    foreach (var primaryPhone in salesReceipt.PrimaryPhone)
                //    {
                //        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                //    }
                //    addedSalesReceipt.PrimaryPhone = primaryPhoneNo;
                //}

                if (salesReceipt.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = salesReceipt.BillEmail;
                    addedSalesReceipt.BillEmail = EmailAddress;                    
                }
                if (salesReceipt.PaymentRefNumber != null)
                {
                    addedSalesReceipt.PaymentRefNum = salesReceipt.PaymentRefNumber;                   
                }


                foreach (var paymentMethod in salesReceipt.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedSalesReceipt.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }              

               
                    foreach (var accountData in salesReceipt.DepositToAccountRef)
                    {
                        if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                            addedSalesReceipt.DepositToAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       
                    }
                if (salesReceipt.ApplyTaxAfterDiscount != null)
                {
                    addedSalesReceipt.ApplyTaxAfterDiscount = Convert.ToBoolean(salesReceipt.ApplyTaxAfterDiscount);
                    addedSalesReceipt.ApplyTaxAfterDiscountSpecified = true;
                }

                if (salesReceipt.CurrencyRef.Count>0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in salesReceipt.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addedSalesReceipt.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (salesReceipt.ExchangeRate != null)
                {
                    addedSalesReceipt.ExchangeRate = Convert.ToDecimal(salesReceipt.ExchangeRate);
                    addedSalesReceipt.ExchangeRateSpecified = true;
                }

                

                #endregion

                CommonUtilities.GetInstance().newSKUItem = false;
                SalesReceiptAdded = new Intuit.Ipp.Data.SalesReceipt();
                SalesReceiptAdded = dataService.Add<Intuit.Ipp.Data.SalesReceipt>(addedSalesReceipt);
            }
            #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    CommonUtilities.GetInstance().getError = string.Empty;
                    CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

                }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
                finally
                {
                    if (CommonUtilities.GetInstance().getError == string.Empty)
                    {
                        if (SalesReceiptAdded.domain == "QBO")
                        {
                            CommonUtilities.GetInstance().TxnId = SalesReceiptAdded.Id;
                            CommonUtilities.GetInstance().SyncToken = SalesReceiptAdded.SyncToken;

                        }
                    }
                    else
                    {
                    }

                }
                if (CommonUtilities.GetInstance().getError != string.Empty)
                {
                    return false;
                }
                else
                {
                    return true;
                }
                #endregion
        }
    /// <summary>
        /// updating sales receipt entry in quickbook online by the transaction Id
    /// </summary>
    /// <param name="coll"></param>
    /// <param name="salesReceiptid"></param>
    /// <param name="syncToken"></param>
    /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOSalesReceipt(OnlineDataProcessingsImportClass.SalesReceipt coll, string salesReceiptid, string syncToken, Intuit.Ipp.Data.SalesReceipt PreviousData = null)
        {
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.SalesReceipt SalesReceiptAdded = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

          
            Intuit.Ipp.Data.SalesReceipt addedSalesReceipt = new Intuit.Ipp.Data.SalesReceipt();
            SalesReceipt salesReceipt = new SalesReceipt();
            salesReceipt = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;

            //bug 514

            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
           
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {
                #region Add Sales Receipt

                //594
                if (salesReceipt.isAppend == true)
                {
                    addedSalesReceipt = PreviousData;
                    if (addedSalesReceipt.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedSalesReceipt.Line.Length;
                        lines_online = addedSalesReceipt.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                    addedSalesReceipt.sparse = true;
                    addedSalesReceipt.sparseSpecified = true;
                }
                addedSalesReceipt.Id = salesReceiptid;
                addedSalesReceipt.SyncToken = syncToken;
                if (salesReceipt.DocNumber != null)
                {
                    addedSalesReceipt.DocNumber = salesReceipt.DocNumber;
                }
                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in salesReceipt.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addedSalesReceipt.CustomField = custom_online;
                #endregion

                if (salesReceipt.TxnDate != null)
                {
                    try
                    {
                        addedSalesReceipt.TxnDate = Convert.ToDateTime(salesReceipt.TxnDate);
                        addedSalesReceipt.TxnDateSpecified = true;
                    }
                    catch { }
                }

                addedSalesReceipt.PrivateNote = salesReceipt.PrivateNote;
                Intuit.Ipp.Data.CustomField CustomField = new Intuit.Ipp.Data.CustomField();
                foreach (var custom in salesReceipt.CustomField)
                {
                    CustomField.AnyIntuitObject = custom.Name;
                    addedSalesReceipt.CustomField = new Intuit.Ipp.Data.CustomField[] { CustomField };
                }
                addedSalesReceipt.TxnStatus = salesReceipt.TxnStatus;

                #region find DepartmentRef  // bug 552
               
                string DepartmentName = string.Empty;
                foreach (var dept in salesReceipt.DepartmentRef)
                {
                    DepartmentName = dept.Name;
                }
                string Departmentvalue = string.Empty;
                
                if (DepartmentName != string.Empty)
                {
                    var dataDepartmentName = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentName.Trim());
                    foreach (var dept in dataDepartmentName)
                    {
                        Departmentvalue = dept.Id;
                    }
                    addedSalesReceipt.DepartmentRef = new ReferenceType()
                    {
                        name = DepartmentName,
                        Value = Departmentvalue
                    };
                }

                #endregion
                // Axis 742
                if (salesReceipt.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(salesReceipt.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedSalesReceipt.TransactionLocationType = value;
                    }
                    else
                    {
                        addedSalesReceipt.TransactionLocationType = salesReceipt.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (salesReceipt.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedSalesReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedSalesReceipt.GlobalTaxCalculationSpecified = true;
                        }
                        else if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedSalesReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedSalesReceipt.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedSalesReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedSalesReceipt.GlobalTaxCalculationSpecified = true;
                        }
                    }

                }

                #endregion              

                foreach (var l in salesReceipt.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }

                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                       
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    //Line.Amount = decimal.Round(Line.Amount, 2);
                                                    //Line.Amount = CommonUtilities.GetInstance().GetLineAmount(Line.Amount);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }

                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedSalesReceipt.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != string.Empty)
                                    {
                                        string id = string.Empty;

                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (salesReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }


                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        addedSalesReceipt.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                //foreach (var txntaxCode in salesReceipt.TxnTaxDetail)
                //{
                //    #region TxnTaxDetail
                //    ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                //    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                //    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                //    if (txntaxCode.TxnTaxCodeRef != null)
                //    {
                //        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                //        {
                //            QueryService<TaxCode> taxCodeQueryService = new QueryService<TaxCode>(serviceContext);                           
                //            string Taxvalue = string.Empty;

                //            if (taxRef.Name != null)
                //            {
                //                if (taxRef.Name.Contains("'"))
                //                {
                //                    taxRef.Name = taxRef.Name.Replace("'", "\\'");
                //                }
                //                dataTax = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(taxCodeQueryService.ExecuteIdsQuery("Select * From TaxCode where Name='" + taxRef.Name + "'"));
                //                if (dataTax !=null)
                //                {
                //                    foreach (var tax in dataTax)
                //                    {
                //                        Taxvalue = tax.Id;
                //                    }

                //                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                //                    {
                //                        name = taxRef.Name,
                //                        Value = Taxvalue
                //                    };
                //                }
                //            }
                //        }
                //    }                   

                //    if (txnTaxDetail != null)
                //    {
                //        addedSalesReceipt.TxnTaxDetail = txnTaxDetail;
                //    }

                //    #endregion
                //}


            #region TxnTaxDetails Bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedSalesReceipt.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;
                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedSalesReceipt.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //BUG ;; 586
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in salesReceipt.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                QueryService<TaxCode> taxCodeQueryService = new QueryService<TaxCode>(serviceContext);

                                string Taxvalue = string.Empty;

                                if (taxRef.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxRef.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = taxRef.Name,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedSalesReceipt.TxnTaxDetail = txnTaxDetail;
                        }

                        #endregion
                    }
                }

                #endregion

                foreach (var customerRef in salesReceipt.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }
                        addedSalesReceipt.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }

                }

                if (salesReceipt.CustomerMemo != null)
                {
                    addedSalesReceipt.CustomerMemo = new MemoRef()
                    {
                        Value = salesReceipt.CustomerMemo
                    };
                }
                PhysicalAddress billAddr = new PhysicalAddress();
                if (salesReceipt.BillAddr.Count > 0)
                {
                    foreach (var billaddress in salesReceipt.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedSalesReceipt.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (salesReceipt.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in salesReceipt.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedSalesReceipt.ShipAddr = shipAddr;
                }


                foreach (var className in salesReceipt.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedSalesReceipt.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }


                if (salesReceipt.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in salesReceipt.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedSalesReceipt.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (salesReceipt.ShipDate != null)
                {
                    try
                    {
                        addedSalesReceipt.ShipDate = Convert.ToDateTime(salesReceipt.ShipDate);
                        addedSalesReceipt.ShipDateSpecified = true;
                    }
                    catch { }
                }
                addedSalesReceipt.TrackingNum = salesReceipt.TrackingNum;

                if (salesReceipt.PrintStatus != null)
                {
                    if (salesReceipt.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedSalesReceipt.PrintStatus = PrintStatusEnum.NotSet;
                        addedSalesReceipt.PrintStatusSpecified = true;
                    }
                    else if (salesReceipt.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedSalesReceipt.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedSalesReceipt.PrintStatusSpecified = true;
                    }
                    else if (salesReceipt.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedSalesReceipt.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedSalesReceipt.PrintStatusSpecified = true;
                    }
                }
                if (salesReceipt.EmailStatus != null)
                {
                    if (salesReceipt.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedSalesReceipt.EmailStatus = EmailStatusEnum.NotSet;
                        addedSalesReceipt.EmailStatusSpecified = true;
                    }
                    else if (salesReceipt.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedSalesReceipt.EmailStatus = EmailStatusEnum.EmailSent;
                        addedSalesReceipt.EmailStatusSpecified = true;
                    }
                    else if (salesReceipt.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedSalesReceipt.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedSalesReceipt.EmailStatusSpecified = true;
                    }
                }
                if (salesReceipt.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = salesReceipt.BillEmail;
                    addedSalesReceipt.BillEmail = EmailAddress;
                }
                if (salesReceipt.PaymentRefNumber != null)
                {
                    addedSalesReceipt.PaymentRefNum = salesReceipt.PaymentRefNumber;
                }


                foreach (var paymentMethod in salesReceipt.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedSalesReceipt.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }


                foreach (var accountData in salesReceipt.DepositToAccountRef)
                {
                    QueryService<Account> classQueryService = new QueryService<Account>(serviceContext);

                    string classValue = string.Empty;
                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                 
                }
               
                if (salesReceipt.ApplyTaxAfterDiscount != null)
                {
                    addedSalesReceipt.ApplyTaxAfterDiscount = Convert.ToBoolean(salesReceipt.ApplyTaxAfterDiscount);
                    addedSalesReceipt.ApplyTaxAfterDiscountSpecified = true;
                }

                if (salesReceipt.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in salesReceipt.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addedSalesReceipt.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (salesReceipt.ExchangeRate != null)
                {
                    addedSalesReceipt.ExchangeRate = Convert.ToDecimal(salesReceipt.ExchangeRate);
                    addedSalesReceipt.ExchangeRateSpecified = true;
                }

                

                #endregion                  

                SalesReceiptAdded = new Intuit.Ipp.Data.SalesReceipt();
                SalesReceiptAdded = dataService.Update<Intuit.Ipp.Data.SalesReceipt>(addedSalesReceipt);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (SalesReceiptAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = SalesReceiptAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = SalesReceiptAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        #endregion
    }
    /// <summary>
    /// checking doc number is present or not in quickbook online for Salesreceipt.
    /// </summary>
    public class OnlineSalesReceiptQBEntryCollection : Collection<SalesReceipt>
    {
            public SalesReceipt FindSalesReceiptNumberEntry(string docNumber)
            {
                foreach (SalesReceipt item in this)
                {
                    if (item.DocNumber == docNumber)
                    {
                        return item;
                    }
                }
                return null;
            }      
    }
    /// <summary>
    /// Enum declaration for GlobalTaxCalculation
    /// </summary>
        public enum GlobalTaxCalculation
        {
            TaxExcluded,
            TaxIncluded,
            NotApplicable
        }

    /// <summary>
    /// enum declaration for line detail type
    /// </summary>
        public enum LineDetailType
        {
            PaymentLineDetail,
            //
            DiscountLineDetail,
            //
            TaxLineDetail,
            //
            SalesItemLineDetail,
            //
            ItemBasedExpenseLineDetail,
            //
            AccountBasedExpenseLineDetail,
            //
            DepositLineDetail,
            //
            PurchaseOrderItemLineDetail,
            //
            ItemReceiptLineDetail,
            //
            JournalEntryLineDetail,
            //
            GroupLineDetail,
            //
            DescriptionOnly,
            //
            SubTotalLineDetail,
            //
            SalesOrderItemLineDetail
        }
    
}
