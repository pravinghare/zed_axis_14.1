﻿using System;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("CreditMemo", Namespace = "", IsNullable = false)]

    public class CreditMemo
    {

        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TxnDate;
        private string m_TxnStatus;
        private Collection<OnlineEntities.CustomField> m_CustomField = new Collection<OnlineEntities.CustomField>();
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.CustomerRef> m_CustomerRef = new Collection<OnlineEntities.CustomerRef>();
        private string m_CustomerMemo;
        private Collection<BillAddr> m_BillAddr = new Collection<BillAddr>();
        private Collection<ShipAddr> m_ShipAddr = new Collection<ShipAddr>();
        private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();
        private Collection<SalesTermRef> m_SalesTermRef = new Collection<SalesTermRef>();
        private string m_TrackingNum;
        private string m_TotalAmt;
        private string m_ShipDate;
        private string m_ApplyTaxAfterDiscount;
        private string m_PrintStatus;
        private string m_EmailStatus;
        private string m_BillEmail;
        private string m_RemainingCredit;
        private Collection<PaymentMethodRef> m_PaymentMethodRef = new Collection<PaymentMethodRef>();
        private Collection<DepositToAccountRef> m_DepositToAccountRef = new Collection<DepositToAccountRef>();
        private Collection<CurrencyRef> m_CurrencyRef = new Collection<CurrencyRef>();
        private string m_ExchangeRate;
        private string m_GlobalTaxCalculation;
        private string m_HomeTotalAmt;
        private string m_TransactionLocationType;

        //594
        private bool m_isAppend;

        #endregion

        #region Constructor
        public CreditMemo()
        {
        }
        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }


        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public Collection<OnlineEntities.CustomField> CustomField
        {
            get { return m_CustomField; }
            set { m_CustomField = value; }
        }


        public string TxnStatus
        {
            get { return m_TxnStatus; }
            set { m_TxnStatus = value; }
        }


        public string CustomerMemo
        {
            get { return m_CustomerMemo; }
            set { m_CustomerMemo = value; }
        }

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }


        public Collection<SalesTermRef> SalesTermRef
        {
            get { return m_SalesTermRef; }
            set { m_SalesTermRef = value; }
        }
        public string TrackingNum
        {
            get { return m_TrackingNum; }
            set { m_TrackingNum = value; }
        }


        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }


        public string PrintStatus
        {
            get { return m_PrintStatus; }
            set { m_PrintStatus = value; }
        }


        public string EmailStatus
        {
            get { return m_EmailStatus; }
            set { m_EmailStatus = value; }
        }


        public string BillEmail
        {
            get { return m_BillEmail; }
            set { m_BillEmail = value; }
        }
        public string RemainingCredit
        {
            get { return m_RemainingCredit; }
            set { m_RemainingCredit = value; }
        }


        public string ApplyTaxAfterDiscount
        {
            get { return m_ApplyTaxAfterDiscount; }
            set { m_ApplyTaxAfterDiscount = value; }
        }


        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }


        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }

        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }

        public string HomeTotalAmt
        {
            get { return m_HomeTotalAmt; }
            set { m_HomeTotalAmt = value; }
        }
        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        public Collection<OnlineEntities.CustomerRef> CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public Collection<BillAddr> BillAddr
        {
            get { return m_BillAddr; }
            set { m_BillAddr = value; }
        }


        public Collection<ShipAddr> ShipAddr
        {
            get { return m_ShipAddr; }
            set { m_ShipAddr = value; }
        }

        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }
        }

        public Collection<PaymentMethodRef> PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }

        public Collection<DepositToAccountRef> DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
            set { m_DepositToAccountRef = value; }
        }

        public Collection<CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region  Public Methods
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        ///Creating new creditmemo transaction in quickbook online.
        public async System.Threading.Tasks.Task<bool> CreateQBOCreditMemo(OnlineDataProcessingsImportClass.CreditMemo coll)
        {
            bool flag = false;
            int linecount = 1;
            int customcount = 1;
            DataService dataService = null;
            Intuit.Ipp.Data.CreditMemo CreditMemoAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.CreditMemo addedCreditMemo = new Intuit.Ipp.Data.CreditMemo();
            CreditMemo CreditMemo = new CreditMemo();
            CreditMemo = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;

            //bug 514
            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];

            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {
                #region Add CreditMemo

                //594
                if (CreditMemo.isAppend == true)
                {
                    addedCreditMemo.sparse = true;
                }

                if (CreditMemo.DocNumber != null)
                {
                    addedCreditMemo.DocNumber = CreditMemo.DocNumber;
                }
                else { addedCreditMemo.AutoDocNumber = true; addedCreditMemo.AutoDocNumberSpecified = true; }

                if (CreditMemo.TxnDate != null)
                {
                    try
                    {
                        addedCreditMemo.TxnDate = Convert.ToDateTime(CreditMemo.TxnDate);
                        addedCreditMemo.TxnDateSpecified = true;
                    }
                    catch { }
                }

                addedCreditMemo.PrivateNote = CreditMemo.PrivateNote;
                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                OnlineEntities.CustomField Custom = new OnlineEntities.CustomField();
                foreach (var customData in CreditMemo.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }

                addedCreditMemo.CustomField = custom_online;
                #endregion
                addedCreditMemo.TxnStatus = CreditMemo.TxnStatus;

                // Axis 742
                if (CreditMemo.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(CreditMemo.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedCreditMemo.TransactionLocationType = value;
                    }
                    else
                    {
                        addedCreditMemo.TransactionLocationType = CreditMemo.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (CreditMemo.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedCreditMemo.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedCreditMemo.GlobalTaxCalculationSpecified = true;
                        }
                        else if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedCreditMemo.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedCreditMemo.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedCreditMemo.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedCreditMemo.GlobalTaxCalculationSpecified = true;
                        }
                    }

                }

                #endregion

                foreach (var l in CreditMemo.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }


                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }

                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                     
                                        foreach (var item in dataClass)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        try
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }
                                        catch { }
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt
                                       
                                            if (CommonUtilities.GetInstance().GrossNet == true)
                                            {
                                                string rate = string.Empty;
                                                if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                                {
                                                    foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                    {
                                                        if (taxCoderef.Name != null)
                                                        {
                                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name);
                                                            if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                            {
                                                                foreach (var taxRateRef in dataTaxcode)
                                                                {
                                                                    foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                    {
                                                                        taxRateRefName = taxRate.TaxRateRef.name;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                //rate percent
                                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                    if (dataTaxRate != null)
                                                    {
                                                        foreach (var taxRateval in dataTaxRate)
                                                        {
                                                            ratevalue = taxRateval.RateValue.ToString();
                                                        }
                                                    }

                                                    if (ratevalue != string.Empty)
                                                    {

                                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                        Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                        Line.AmountSpecified = true;


                                                    }
                                                    else
                                                    {
                                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                        Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                        Line.AmountSpecified = true;
                                                    }

                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        
                                        //else
                                        //{
                                        //    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        //    Line.Amount= Convert.ToDecimal(l.LineAmount);
                                        //    Line.AmountSpecified = true;
                                        //}

                                        #endregion
                                    }
                                }
                               
                                    if (SalesItemLine.LineUnitPrice != null)
                                    {
                                        
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                             try
                                            {
                                              lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                              lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }catch{}
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {

                                            #region Gross Net for unit price
                                            if (CommonUtilities.GetInstance().GrossNet == true)
                                            {
                                                if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                                {
                                                    if (SalesItemLine.LineQty != null)
                                                    {
                                                        try
                                                        {
                                                         lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                         lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                        }
                                                        catch{}
                                                    }
                                                }
                                                else
                                                {
                                                    try{
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                         }
                                                        catch{}
                                                }

                                            }
                                            else
                                            {
                                                try{
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                 }
                                                        catch{}
                                            }
                                            #endregion
                                        }
                                         

                                    }
                                    if (SalesItemLine.LineQty != null)
                                    {
                                        try{
                                        lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                        lineSalesItemLineDetail.QtySpecified = true;
                                        }
                                        catch{}
                                    }
                                    if (SalesItemLine.ServiceDate != null)
                                    {
                                        try
                                        {
                                            lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                            lineSalesItemLineDetail.ServiceDateSpecified = true;
                                        }
                                        catch { }
                                    }
                               
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    try
                                    {
                                        DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                        DiscountLineDetail.PercentBasedSpecified = true;
                                     }
                                     catch{}
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    try{
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                    }
                                    catch { }
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    string id = string.Empty;
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = account.Name,
                                            Value = id
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedCreditMemo.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                               
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        try
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        catch { }
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    try
                                                    {
                                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                    }
                                                    catch { }

                                                }
                                            }
                                            else
                                            {
                                                try{
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                                catch { }
                                            }

                                        }
                                        else
                                        {
                                            try
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }
                                            catch { }
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                        lineSalesItemLineDetail.QtySpecified = true;
                                    } 
                                     catch{}
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    try
                                    {
                                        Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    catch { }
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    try
                                    {
                                        DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                        DiscountLineDetail.PercentBasedSpecified = true;
                                    } 
                                     catch{}
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    try{
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                    }
                                    catch { }
                                }


                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        addedCreditMemo.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US" && CommonUtilities.GetInstance().GrossNet == true && addedCreditMemo.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && CreditMemo.GlobalTaxCalculation != null)
                {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {
                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                   
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;
                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedCreditMemo.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                     
                }

                //BUG ;; 586
                if (CommonUtilities.GetInstance().CountryVersion == "US" || CommonUtilities.GetInstance().GrossNet == false || addedCreditMemo.GlobalTaxCalculation.ToString() != GlobalTaxCalculationEnum.TaxInclusive.ToString())
                {
                    foreach (var txntaxCode in CreditMemo.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedCreditMemo.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }


                #endregion

                foreach (var customerRef in CreditMemo.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }

                        addedCreditMemo.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }
                }

                if (CreditMemo.CustomerMemo != null)
                {
                    addedCreditMemo.CustomerMemo = new MemoRef()
                    {
                        Value = CreditMemo.CustomerMemo
                    };
                }

                PhysicalAddress billAddr = new PhysicalAddress();
                if (CreditMemo.BillAddr.Count > 0)
                {
                    foreach (var billaddress in CreditMemo.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedCreditMemo.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (CreditMemo.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in CreditMemo.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedCreditMemo.ShipAddr = shipAddr;
                }

                foreach (var className in CreditMemo.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());
                       
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedCreditMemo.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }


                if (CreditMemo.SalesTermRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in CreditMemo.SalesTermRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedCreditMemo.SalesTermRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (CreditMemo.ShipDate != null)
                {
                    try
                    {
                        addedCreditMemo.ShipDate = Convert.ToDateTime(CreditMemo.ShipDate);
                        addedCreditMemo.ShipDateSpecified = true;
                    }
                    catch { }
                }
                addedCreditMemo.TrackingNum = CreditMemo.TrackingNum;

                if (CreditMemo.PrintStatus != null)
                {
                    if (CreditMemo.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedCreditMemo.PrintStatus = PrintStatusEnum.NotSet;
                        addedCreditMemo.PrintStatusSpecified = true;
                    }
                    else if (CreditMemo.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedCreditMemo.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedCreditMemo.PrintStatusSpecified = true;
                    }
                    else if (CreditMemo.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedCreditMemo.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedCreditMemo.PrintStatusSpecified = true;
                    }
                }
                if (CreditMemo.EmailStatus != null)
                {
                    if (CreditMemo.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedCreditMemo.EmailStatus = EmailStatusEnum.NotSet;
                        addedCreditMemo.EmailStatusSpecified = true;
                    }
                    else if (CreditMemo.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedCreditMemo.EmailStatus = EmailStatusEnum.EmailSent;
                        addedCreditMemo.EmailStatusSpecified = true;
                    }
                    else if (CreditMemo.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedCreditMemo.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedCreditMemo.EmailStatusSpecified = true;
                    }
                }
                if (CreditMemo.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = CreditMemo.BillEmail;
                    addedCreditMemo.BillEmail = EmailAddress;
                }
                if (CreditMemo.RemainingCredit != null)
                {
                    try
                    {
                        addedCreditMemo.RemainingCredit = Convert.ToDecimal(CreditMemo.RemainingCredit);
                    } 
                     catch{}
                }


                foreach (var paymentMethod in CreditMemo.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                       
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedCreditMemo.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }


                foreach (var accountData in CreditMemo.DepositToAccountRef)
                {
                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                        addedCreditMemo.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                 
                }


                if (CreditMemo.ApplyTaxAfterDiscount != null)
                {
                    try{
                    addedCreditMemo.ApplyTaxAfterDiscount = Convert.ToBoolean(CreditMemo.ApplyTaxAfterDiscount);
                    addedCreditMemo.ApplyTaxAfterDiscountSpecified = true;
                    }
                    catch { }
                }

                if (CreditMemo.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in CreditMemo.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addedCreditMemo.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (CreditMemo.ExchangeRate != null)
                {
                    try
                    {

                        addedCreditMemo.ExchangeRate = Convert.ToDecimal(CreditMemo.ExchangeRate);
                        addedCreditMemo.ExchangeRateSpecified = true;
                    }
                    catch { }
                }

                

                #endregion

                CommonUtilities.GetInstance().newSKUItem = false;
                CreditMemoAdded = new Intuit.Ipp.Data.CreditMemo();
                CreditMemoAdded = dataService.Add<Intuit.Ipp.Data.CreditMemo>(addedCreditMemo);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (CreditMemoAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = CreditMemoAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = CreditMemoAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        /// <summary>
        /// updating creditmemo entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="salesReceiptid"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOCreditMemo(OnlineDataProcessingsImportClass.CreditMemo coll, string salesReceiptid, string syncToken, Intuit.Ipp.Data.CreditMemo PreviousData = null)
        {
            bool flag = false;
            int linecount = 1;

            DataService dataService = null;
            Intuit.Ipp.Data.CreditMemo CreditMemoAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            Intuit.Ipp.Data.CreditMemo addedCreditMemo = new Intuit.Ipp.Data.CreditMemo();
            CreditMemo CreditMemo = new CreditMemo();
            CreditMemo = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];

            //bug 514
            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {

                #region Add CreditMemo

                //594
                if (CreditMemo.isAppend == true)
                {
				 //594 PreviousData is used to maintain previous records and append new data if changes are applied ,Lines will be appended to next line directly
                    addedCreditMemo = PreviousData;
                    if (addedCreditMemo.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedCreditMemo.Line.Length;
                        lines_online = addedCreditMemo.Line;
                        Array.Resize(ref lines_online, linecount);
                    }

                    addedCreditMemo.sparse = true;
                    addedCreditMemo.sparseSpecified = true;

                }

                addedCreditMemo.Id = salesReceiptid;
                addedCreditMemo.SyncToken = syncToken;

                if (CreditMemo.DocNumber != null)
                {
                    addedCreditMemo.DocNumber = CreditMemo.DocNumber;
                }

                if (CreditMemo.TxnDate != null)
                {
                    try
                    {
                        addedCreditMemo.TxnDate = Convert.ToDateTime(CreditMemo.TxnDate);
                        addedCreditMemo.TxnDateSpecified = true;
                    }
                    catch { }
                }

                addedCreditMemo.PrivateNote = CreditMemo.PrivateNote;

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                OnlineEntities.CustomField Custom = new OnlineEntities.CustomField();
                foreach (var customData in CreditMemo.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addedCreditMemo.CustomField = custom_online;
                #endregion

                addedCreditMemo.TxnStatus = CreditMemo.TxnStatus;

                // Axis 742
                if (CreditMemo.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(CreditMemo.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedCreditMemo.TransactionLocationType = value;
                    }
                    else
                    {
                        addedCreditMemo.TransactionLocationType = CreditMemo.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (CreditMemo.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedCreditMemo.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedCreditMemo.GlobalTaxCalculationSpecified = true;
                        }
                        else if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedCreditMemo.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedCreditMemo.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedCreditMemo.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedCreditMemo.GlobalTaxCalculationSpecified = true;
                        }
                    }

                }

                #endregion

                foreach (var l in CreditMemo.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }


                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                  
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                              
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }

                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                    
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        try
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }
                                        catch { }
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                               
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {

                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;


                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                            Line.AmountSpecified = true;
                                        }

                                        //else
                                        //{
                                        //    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        //    Line.Amount= Convert.ToDecimal(l.LineAmount);
                                        //    Line.AmountSpecified = true;
                                        //}

                                        #endregion
                                    }
                                }

                                if (SalesItemLine.LineUnitPrice != null)
                                {

                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        try
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        catch { }
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    try
                                                    {
                                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                    }
                                                    catch { }
                                                }
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                                catch { }
                                            }

                                        }
                                        else
                                        {
                                            try
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }
                                            catch { }
                                        }
                                        #endregion
                                    }


                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                        lineSalesItemLineDetail.QtySpecified = true;
                                    }
                                    catch { }
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }

                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    try
                                    {
                                        DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                        DiscountLineDetail.PercentBasedSpecified = true;
                                    }
                                    catch { }
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    try
                                    {
                                        DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                        DiscountLineDetail.DiscountPercentSpecified = true;
                                    }
                                    catch { }
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    string id = string.Empty;
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = account.Name,
                                            Value = id
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedCreditMemo.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        try
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        catch { }
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (CreditMemo.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    try
                                                    {
                                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                    }
                                                    catch { }

                                                }
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                                catch { }
                                            }

                                        }
                                        else
                                        {
                                            try
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }
                                            catch { }
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                        lineSalesItemLineDetail.QtySpecified = true;
                                    }
                                    catch { }
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                           
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    try
                                    {
                                        Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    catch { }
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    try
                                    {
                                        DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                        DiscountLineDetail.PercentBasedSpecified = true;
                                    }
                                    catch { }
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    try
                                    {
                                        DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                        DiscountLineDetail.DiscountPercentSpecified = true;
                                    }
                                    catch { }
                                }


                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        addedCreditMemo.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                foreach (var customerRef in CreditMemo.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }
                        addedCreditMemo.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }
                }

                if (CreditMemo.CustomerMemo != null)
                {
                    addedCreditMemo.CustomerMemo = new MemoRef()
                    {
                        Value = CreditMemo.CustomerMemo
                    };
                }

                PhysicalAddress billAddr = new PhysicalAddress();
                if (CreditMemo.BillAddr.Count > 0)
                {
                    foreach (var billaddress in CreditMemo.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedCreditMemo.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (CreditMemo.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in CreditMemo.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedCreditMemo.ShipAddr = shipAddr;
                }

                foreach (var className in CreditMemo.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());
                      
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedCreditMemo.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }

                if (CreditMemo.SalesTermRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in CreditMemo.SalesTermRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedCreditMemo.SalesTermRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (CreditMemo.ShipDate != null)
                {
                    try
                    {
                        addedCreditMemo.ShipDate = Convert.ToDateTime(CreditMemo.ShipDate);
                        addedCreditMemo.ShipDateSpecified = true;
                    }
                    catch { }
                }
                addedCreditMemo.TrackingNum = CreditMemo.TrackingNum;

                if (CreditMemo.PrintStatus != null)
                {
                    if (CreditMemo.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedCreditMemo.PrintStatus = PrintStatusEnum.NotSet;
                        addedCreditMemo.PrintStatusSpecified = true;
                    }
                    else if (CreditMemo.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedCreditMemo.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedCreditMemo.PrintStatusSpecified = true;
                    }
                    else if (CreditMemo.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedCreditMemo.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedCreditMemo.PrintStatusSpecified = true;
                    }
                }
                if (CreditMemo.EmailStatus != null)
                {
                    if (CreditMemo.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedCreditMemo.EmailStatus = EmailStatusEnum.NotSet;
                        addedCreditMemo.EmailStatusSpecified = true;
                    }
                    else if (CreditMemo.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedCreditMemo.EmailStatus = EmailStatusEnum.EmailSent;
                        addedCreditMemo.EmailStatusSpecified = true;
                    }
                    else if (CreditMemo.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedCreditMemo.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedCreditMemo.EmailStatusSpecified = true;
                    }
                }
                if (CreditMemo.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = CreditMemo.BillEmail;
                    addedCreditMemo.BillEmail = EmailAddress;
                }
                if (CreditMemo.RemainingCredit != null)
                {
                    addedCreditMemo.RemainingCredit = Convert.ToDecimal(CreditMemo.RemainingCredit);
                }


                foreach (var paymentMethod in CreditMemo.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                       
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedCreditMemo.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }


                foreach (var accountData in CreditMemo.DepositToAccountRef)
                {
                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                        addedCreditMemo.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                  
                }
                if (CreditMemo.ApplyTaxAfterDiscount != null)
                {
                    addedCreditMemo.ApplyTaxAfterDiscount = Convert.ToBoolean(CreditMemo.ApplyTaxAfterDiscount);
                    addedCreditMemo.ApplyTaxAfterDiscountSpecified = true;
                }

                if (CreditMemo.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in CreditMemo.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addedCreditMemo.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (CreditMemo.ExchangeRate != null)
                {
                    addedCreditMemo.ExchangeRate = Convert.ToDecimal(CreditMemo.ExchangeRate);
                    addedCreditMemo.ExchangeRateSpecified = true;
                }

                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US" && CommonUtilities.GetInstance().GrossNet == true && addedCreditMemo.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && CreditMemo.GlobalTaxCalculation != null)
                {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                if (taxCode.Contains("'"))
                                {
                                    taxCode = taxCode.Replace("'", "\\'");
                                }

                               var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedCreditMemo.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                      
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //BUG ;; 586
                if (CommonUtilities.GetInstance().CountryVersion == "US" || CommonUtilities.GetInstance().GrossNet == false || addedCreditMemo.GlobalTaxCalculation.ToString() != GlobalTaxCalculationEnum.TaxInclusive.ToString())
                {
                    foreach (var txntaxCode in CreditMemo.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                   
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedCreditMemo.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }


                #endregion
                
                

                #endregion
                CreditMemoAdded = new Intuit.Ipp.Data.CreditMemo();
                CreditMemoAdded = dataService.Update<Intuit.Ipp.Data.CreditMemo>(addedCreditMemo);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (CreditMemoAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = CreditMemoAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = CreditMemoAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        #endregion
    }

    /// <summary>
    /// checking doc number is present or not in quickbook online for creditmemo.
    /// </summary>
    public class OnlineCreditCardQBEntry : Collection<CreditMemo>
    {
        public CreditMemo FindCreditMemoNumberEntry(string docNumber)
        {
            foreach (CreditMemo item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }


}
