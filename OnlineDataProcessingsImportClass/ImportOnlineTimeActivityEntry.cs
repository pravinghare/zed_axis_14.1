﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using DataProcessingBlocks;
using System.Globalization;
using EDI.Constant;
using System.Collections.ObjectModel;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Core;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
   public class ImportOnlineTimeActivityEntry
    {
         private static ImportOnlineTimeActivityEntry m_ImportOnlineTimeActivityEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineTimeActivityEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import TImeentry class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineTimeActivityEntry GetInstance()
        {
            if (m_ImportOnlineTimeActivityEntry == null)
                m_ImportOnlineTimeActivityEntry = new ImportOnlineTimeActivityEntry();
            return m_ImportOnlineTimeActivityEntry;
        }
        /// setting values to timeentry data table and returns collection.
        public async System.Threading.Tasks.Task<OnlineTimeActivityQBEntryCollection> ImportOnlineTimeActivityData(DataTable dt)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineTimeActivityQBEntryCollection coll = new OnlineTimeActivityQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                   
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                
                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Employee Validation
                    OnlineDataProcessingsImportClass.OnlineTimeActivityQBEntry TimeActivity = new OnlineDataProcessingsImportClass.OnlineTimeActivityQBEntry();
                    if (dt.Columns.Contains("Id"))
                    {
                        #region Validations of Id
                        if (dr["Id"].ToString() != string.Empty)
                        {
                            if (dr["Id"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Id (" + dr["Id"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeActivity.Id = dr["Id"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeActivity.Id = dr["Id"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeActivity.Id = dr["Id"].ToString();
                                }
                            }
                            else
                            {
                                TimeActivity.Id = dr["Id"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("NameOf"))
                    {
                        #region Validations of NameOf
                        if (dr["NameOf"].ToString() != string.Empty)
                        {
                            if (dr["NameOf"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This NameOf (" + dr["NameOf"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeActivity.NameOf = dr["NameOf"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeActivity.NameOf = dr["NameOf"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeActivity.NameOf = dr["NameOf"].ToString();
                                }
                            }
                            else
                            {
                                TimeActivity.NameOf = dr["NameOf"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TxnDate"))
                    {
                        #region Validations of PrivateNote
                        DateTime SODate = new DateTime();
                        if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["TxnDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out SODate))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TimeActivity.TxnDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeActivity.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        TimeActivity.TxnDate = datevalue;
                                    }
                                }
                                else
                                {
                                    SODate = dttest;
                                    TimeActivity.TxnDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                SODate = Convert.ToDateTime(datevalue);
                                TimeActivity.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    OnlineEntities.EmployeeOrVendorRef EmployeeOrVendorRef = new OnlineEntities.EmployeeOrVendorRef();
                    if (dt.Columns.Contains("EmployeeVendorRefName"))
                    {
                        #region Validations of EmployeeVendorRefName
                        if (dr["EmployeeVendorRefName"].ToString() != string.Empty)
                        {
                            if (dr["EmployeeVendorRefName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeeVendorRefName (" + dr["EmployeeVendorRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        EmployeeOrVendorRef.Name = dr["EmployeeVendorRefName"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        EmployeeOrVendorRef.Name = dr["EmployeeVendorRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    EmployeeOrVendorRef.Name = dr["EmployeeVendorRefName"].ToString();
                                }
                            }
                            else
                            {
                                EmployeeOrVendorRef.Name = dr["EmployeeVendorRefName"].ToString();
                            }
                        }
                        else
                        {
                            string strMessages = "The EmployeeName   is not available in quickbook.This record will not be added to QuickBooks.";
                            //  DialogResult result = CommonUtilities.ShowMessage(strMessages, System.Windows.Forms.MessageBoxIcon.Warning);
                            DialogResult result = CommonUtilities.ShowMessage1(strMessages, MessageBoxIcon.Warning);


                            //if (Convert.ToString(result) == "Ok")
                            //{

                            continue;
                        }

                        DataService dataService = null;
                        ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
                        dataService = new DataService(serviceContext);

                        if (TimeActivity.NameOf == Intuit.Ipp.Data.TimeActivityTypeEnum.Employee.ToString() || TimeActivity.NameOf == null)
                        {
                            ReadOnlyCollection<Intuit.Ipp.Data.Employee> dataEmployee = null;
                            string displayname = EmployeeOrVendorRef.Name;
                            if (displayname != string.Empty)
                            {
                                Intuit.Ipp.QueryFilter.QueryService<Intuit.Ipp.Data.Employee> ItemQueryService = new Intuit.Ipp.QueryFilter.QueryService<Intuit.Ipp.Data.Employee>(serviceContext);
                                if (displayname.Contains("'"))
                                {
                                    displayname = displayname.Replace("'", "\\'");
                                }
                                dataEmployee = new ReadOnlyCollection<Intuit.Ipp.Data.Employee>(ItemQueryService.ExecuteIdsQuery("select * from Employee where DisplayName='" + displayname + "'"));

                                if (dataEmployee.Count == 0)
                                {
                                    string strMessages = "The EmployeeName  (" + displayname + ") is not available in quickbook.This record will not be added to QuickBooks.";
                                    DialogResult result = CommonUtilities.ShowMessage1(strMessages, MessageBoxIcon.Warning);
                                        continue;
                                }
                            }
                            else
                            {
                                string strMessages = "The EmployeeVendorRefName field is empty .This record will not be added to QuickBooks.";
                                DialogResult result = CommonUtilities.ShowMessage1(strMessages, MessageBoxIcon.Warning);
                                continue;
                            }
                        }
                        //bug 498
                        else if (TimeActivity.NameOf == Intuit.Ipp.Data.TimeActivityTypeEnum.Vendor.ToString())
                        {
                            ReadOnlyCollection<Intuit.Ipp.Data.Vendor> dataVendor = null;
                            string displayname = EmployeeOrVendorRef.Name;
                            if (displayname != string.Empty)
                            {
                                Intuit.Ipp.QueryFilter.QueryService<Intuit.Ipp.Data.Vendor> ItemQueryService = new Intuit.Ipp.QueryFilter.QueryService<Intuit.Ipp.Data.Vendor>(serviceContext);
                                if (displayname.Contains("'"))
                                {
                                    displayname = displayname.Replace("'", "\\'");
                                }
                                dataVendor = new ReadOnlyCollection<Intuit.Ipp.Data.Vendor>(ItemQueryService.ExecuteIdsQuery("select * from Vendor where DisplayName='" + displayname + "'"));
                                if (dataVendor.Count == 0)
                                {
                                    string strMessages = "The Vendor Name  (" + displayname + ") is not available in quickbook.This record will not be added to QuickBooks.";
                                    DialogResult result = CommonUtilities.ShowMessage1(strMessages, MessageBoxIcon.Warning);
                                    continue;
                                }
                            }
                            else
                            {
                                string strMessages = "The EmployeeVendorRefName field is empty .This record will not be added to QuickBooks.";
                                DialogResult result = CommonUtilities.ShowMessage1(strMessages, MessageBoxIcon.Warning);
                                continue;
                            }

                        }
                        #endregion
                    }

                    TimeActivity.EmployeeOrVendorRef.Add(EmployeeOrVendorRef);

                    OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();

                    if (dt.Columns.Contains("CustomerRefName"))
                    {
                        #region Validations of CustomerRefName
                        if (dr["CustomerRefName"].ToString() != string.Empty)
                        {
                            if (dr["CustomerRefName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CustomerRefName (" + dr["CustomerRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        CustomerRef.Name = dr["CustomerRefName"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        CustomerRef.Name = dr["CustomerRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["CustomerRefName"].ToString();
                                }
                            }
                            else
                            {
                                CustomerRef.Name = dr["CustomerRefName"].ToString();
                            }
                        }
                        #endregion
                    }

                    TimeActivity.CustomerRef.Add(CustomerRef);


                    OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                    if (dt.Columns.Contains("DepartmentRefName"))
                    {
                        #region Validations of DepartmentRefName
                        if (dr["DepartmentRefName"].ToString() != string.Empty)
                        {
                            if (dr["DepartmentRefName"].ToString().Length > 2000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This DepartmentRefName (" + dr["DepartmentRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        DepartmentRef.Name = dr["DepartmentRefName"].ToString().Substring(0, 2000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                }
                            }
                            else
                            {
                                DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                            }
                        }
                        #endregion
                    }

                    TimeActivity.DepartmentRef.Add(DepartmentRef);

                    // Axis 718
                    OnlineEntities.PayrollItemRef PayrollItemRef = new OnlineEntities.PayrollItemRef();

                    if (dt.Columns.Contains("PayrollItemRef"))
                    {
                        #region Validations of ItemRefName
                        if (dr["PayrollItemRef"].ToString() != string.Empty)
                        {
                            PayrollItemRef.Name = dr["PayrollItemRef"].ToString();
                        }
                        #endregion
                    }
                    TimeActivity.PayrollItemRef.Add(PayrollItemRef);


                    OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();

                    if (dt.Columns.Contains("ItemRefName"))
                    {
                        #region Validations of ItemRefName
                        if (dr["ItemRefName"].ToString() != string.Empty)
                        {
                            if (dr["ItemRefName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ItemRefName (" + dr["ItemRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemRef.Name = dr["ItemRefName"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemRef.Name = dr["ItemRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.Name = dr["ItemRefName"].ToString();
                                }
                            }
                            else
                            {
                                ItemRef.Name = dr["ItemRefName"].ToString();
                            }
                        }
                        #endregion

                        CommonUtilities.GetInstance().is_item_avail = true;
                    }
                     
                    else
                    {
                        CommonUtilities.GetInstance().is_item_avail = false;

                    }


                    TimeActivity.ItemRef.Add(ItemRef);

                    OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();

                    if (dt.Columns.Contains("ClassRefName"))
                    {
                        #region Validations of ClassRefName
                        if (dr["ClassRefName"].ToString() != string.Empty)
                        {
                            if (dr["ClassRefName"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ClassRefName (" + dr["ClassRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ClassRef.Name = dr["ClassRefName"].ToString().Substring(0, 30);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ClassRef.Name = dr["ClassRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef.Name = dr["ClassRefName"].ToString();
                                }
                            }
                            else
                            {
                                ClassRef.Name = dr["ClassRefName"].ToString();
                            }
                        }
                        #endregion
                    }

                    TimeActivity.ClassRef.Add(ClassRef);

                    if (dt.Columns.Contains("BillableStatus"))
                    {
                        #region Validations of BillableStatus
                        if (dr["BillableStatus"].ToString() != string.Empty)
                        {
                            if (dr["BillableStatus"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Billable Status (" + dr["BillableStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeActivity.BillableStatus = dr["BillableStatus"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeActivity.BillableStatus = dr["BillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeActivity.BillableStatus = dr["BillableStatus"].ToString();
                                }
                            }
                            else
                            {
                                TimeActivity.BillableStatus = dr["BillableStatus"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Taxable"))
                    {
                        #region Validations of Taxable
                        if (dr["Taxable"].ToString() != "<None>" || dr["Taxable"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["Taxable"].ToString(), out result))
                            {
                                TimeActivity.Taxable = Convert.ToInt32(dr["Taxable"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["Taxable"].ToString().ToLower() == "true")
                                {
                                    TimeActivity.Taxable = dr["Taxable"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["Taxable"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "Taxable";
                                    }
                                    else
                                        TimeActivity.Taxable = dr["Taxable"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Taxable(" + dr["Taxable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            TimeActivity.Taxable = dr["Taxable"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeActivity.Taxable = dr["Taxable"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TimeActivity.Taxable = dr["Taxable"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("HourlyRate"))
                    {
                        #region Validations for HourlyRate
                        if (dr["HourlyRate"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["HourlyRate"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This HourlyRate( " + dr["HourlyRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeActivity.HourlyRate = dr["HourlyRate"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeActivity.HourlyRate = dr["HourlyRate"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeActivity.HourlyRate = dr["HourlyRate"].ToString();
                                }
                            }
                            else
                            {
                                TimeActivity.HourlyRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["HourlyRate"].ToString()));
                            }
                        }

                        #endregion
                    }
                                       
                    if (dt.Columns.Contains("Duration"))
                    {
                        #region Validations of Duration
                        if (dr["Duration"].ToString() != string.Empty)
                        {
                            if (dr["Duration"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Duration (" + dr["Duration"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeActivity.Hours = dr["Duration"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeActivity.Hours = dr["Duration"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeActivity.Hours = dr["Duration"].ToString();
                                }
                            }
                            else
                            {
                                TimeActivity.Hours = dr["Duration"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BreakDuration"))
                    {
                        #region Validations of BreakDuration
                        if (dr["BreakDuration"].ToString() != string.Empty)
                        {
                            if (dr["BreakDuration"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BreakDuration (" + dr["BreakDuration"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeActivity.BreakHours = dr["BreakDuration"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeActivity.BreakHours = dr["BreakDuration"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeActivity.BreakHours = dr["BreakDuration"].ToString();
                                }
                            }
                            else
                            {
                                TimeActivity.BreakHours = dr["BreakDuration"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("StartTime"))
                    {
                        DateTime SODate = new DateTime();
                        #region validations of DueDate
                        if (dr["StartTime"].ToString() != "<None>" || dr["StartTime"].ToString() != string.Empty)
                        {
                            datevalue = dr["StartTime"].ToString();
                            if (!DateTime.TryParse(datevalue, out SODate))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This StartTime (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TimeActivity.StartTime = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeActivity.StartTime = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        TimeActivity.StartTime = datevalue;
                                    }
                                }
                                else
                                {
                                    SODate = dttest;
                                    TimeActivity.StartTime = dttest.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK");
                                }

                            }
                            else
                            {
                                SODate = Convert.ToDateTime(datevalue);
                                TimeActivity.StartTime = DateTime.Parse(datevalue).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK");
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("EndTime"))
                    {
                        DateTime SODate = new DateTime();
                        #region validations of EndTime
                        if (dr["EndTime"].ToString() != "<None>" || dr["EndTime"].ToString() != string.Empty)
                        {
                            datevalue = dr["EndTime"].ToString();
                            if (!DateTime.TryParse(datevalue, out SODate))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EndTime (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TimeActivity.EndTime = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeActivity.EndTime = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        TimeActivity.EndTime = datevalue;
                                    }
                                }
                                else
                                {
                                    SODate = dttest;
                                    TimeActivity.EndTime = dttest.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK");
                                }

                            }
                            else
                            {
                                SODate = Convert.ToDateTime(datevalue);
                                TimeActivity.EndTime = DateTime.Parse(datevalue).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK");
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Description"))
                    {
                        #region Validations of Description
                        if (dr["Description"].ToString() != string.Empty)
                        {
                            if (dr["Description"].ToString().Length > 4000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Description (" + dr["Description"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeActivity.Description = dr["Description"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeActivity.Description = dr["Description"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeActivity.Description = dr["Description"].ToString();
                                }
                            }
                            else
                            {
                                TimeActivity.Description = dr["Description"].ToString();
                            }
                        }
                        #endregion
                    }

                    coll.Add(TimeActivity);

                }
                else
                {
                    return null;
                }
            }

            #endregion

            #endregion

            return coll;
        }

     } 
       
 }

