﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using DataProcessingBlocks;
using System.Globalization;
using EDI.Constant;
//using DataProcessingBlocks;

namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineDepositEntry
    {
        private static ImportOnlineDepositEntry m_ImportOnlineDepositEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineDepositEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import invoice class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineDepositEntry GetInstance()
        {
            if (m_ImportOnlineDepositEntry == null)
                m_ImportOnlineDepositEntry = new ImportOnlineDepositEntry();
            return m_ImportOnlineDepositEntry;
        }
        /// setting values to Invoice data table and returns collection.
        /// 
        public OnlineDataProcessingsImportClass.OnlineDepositeQBEntryCollection ImportOnlineDepositData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineDepositeQBEntryCollection coll = new OnlineDepositeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434

                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;

                    //Employee Validation
                    OnlineDataProcessingsImportClass.OnlineDepositeQBEntry Deposit = new OnlineDataProcessingsImportClass.OnlineDepositeQBEntry();
                    if (dt.Columns.Contains("DocNumber"))
                    {
                        Deposit = coll.FindDepositeNumberEntry(dr["DocNumber"].ToString());

                        if (Deposit == null)
                        {
                            Deposit = new OnlineDepositeQBEntry();
                            if (dt.Columns.Contains("DocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["DocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["DocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.DocNumber = dr["DocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of txn Date
                                DateTime DODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out DODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Deposit.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Deposit.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Deposit.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            DODate = dttest;
                                            Deposit.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        DODate = Convert.ToDateTime(datevalue);
                                        Deposit.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                            if (dt.Columns.Contains("DepartmentRefName"))
                            {
                                #region Validations of DepartmentRefName
                                if (dr["DepartmentRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepartmentRefName"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepartmentRefName (" + dr["DepartmentRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepartmentRef.Name = dr["DepartmentRefName"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (DepartmentRef.Name != null)
                            {
                                Deposit.DepartmentRef.Add(DepartmentRef);
                            }
                            OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRefName"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRefName"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRefName"].ToString().Length > 200)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRefName (" + dr["CurrencyRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRefName"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                            {
                                Deposit.CurrencyRef.Add(CurrencyRef);
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TxnStatus"))
                            {
                                #region Validations of TxnStatus
                                if (dr["TxnStatus"].ToString() != string.Empty)
                                {
                                    if (dr["TxnStatus"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnStatus (" + dr["TxnStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.TxnStatus = dr["TxnStatus"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.TxnStatus = dr["TxnStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.TxnStatus = dr["TxnStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.TxnStatus = dr["TxnStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            #region Custom1
                            OnlineEntities.CustomField CustomField = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName1"))
                            {
                                #region Validations of Custom1
                                if (dr["CustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName1 (" + dr["CustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField.Name = dr["CustomFieldName1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField.Name = dr["CustomFieldName1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField.Name = dr["CustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Name = dr["CustomFieldName1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue1"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue1 (" + dr["CustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField.Value = dr["CustomFieldValue1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField.Value = dr["CustomFieldValue1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField.Value = dr["CustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Value = dr["CustomFieldValue1"].ToString();
                                    }
                                }
                                #endregion
                            }


                            #endregion

                            #region Custom2
                            OnlineEntities.CustomField CustomField2 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName2"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName2 (" + dr["CustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField2.Name = dr["CustomFieldName2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField2.Name = dr["CustomFieldName2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField2.Name = dr["CustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Name = dr["CustomFieldName2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue2"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue2 (" + dr["CustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField2.Value = dr["CustomFieldValue2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                    }
                                }
                                #endregion
                            }



                            #endregion

                            #region Custom3
                            OnlineEntities.CustomField CustomField3 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName3"))
                            {
                                #region Validations of Custom3
                                if (dr["CustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName3 (" + dr["CustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField3.Name = dr["CustomFieldName3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField3.Name = dr["CustomFieldName3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField3.Name = dr["CustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Name = dr["CustomFieldName3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue3"))
                            {
                                #region Validations of Custom3
                                if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue3 (" + dr["CustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField3.Value = dr["CustomFieldValue3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomField3.Value != null || CustomField3.Name != null)
                            {
                                Deposit.CustomField.Add(CustomField3);
                            }
                            if (CustomField.Value != null || CustomField.Name != null)
                            {
                                Deposit.CustomField.Add(CustomField);
                            }

                            if (CustomField2.Value != null || CustomField2.Name != null)
                            {
                                Deposit.CustomField.Add(CustomField2);
                            }

                            #endregion


                            #region line
                            OnlineEntities.DepositLineDetails DepositLineDetails = new OnlineEntities.DepositLineDetails();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.Linkedtxn Linkedtxn = new OnlineEntities.Linkedtxn();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LinkedtxnDocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["LinkedtxnDocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LinkedtxnDocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LinkedToTxn DocNumber (" + dr["LinkedtxnDocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LinkedtxnTxnType"))
                            {
                                #region Validations of docnumber
                                if (dr["LinkedtxnTxnType"].ToString() != string.Empty)
                                {
                                    if (dr["LinkedtxnTxnType"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Linkedtxn TxnType (" + dr["LinkedtxnTxnType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (Linkedtxn.ID != null && Linkedtxn.Type != null)
                            {
                                Line.LinkedTxn.Add(Linkedtxn);
                            }
                            #region deposite line details
                            OnlineEntities.Entity Entityref = new OnlineEntities.Entity();
                            if (dt.Columns.Contains("DepositLineEntityType"))
                            {
                                #region Validations of DepositLineEntity type
                                if (dr["DepositLineEntityType"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineEntityType"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineEntityType (" + dr["DepositLineEntityType"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Entityref.Type = dr["DepositLineEntityType"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Entityref.Type = dr["DepositLineEntityType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Entityref.Type = dr["DepositLineEntityType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Entityref.Type = dr["DepositLineEntityType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("DepositLineEntityName"))
                            {
                                #region Validations of DepositLineEntityName
                                if (dr["DepositLineEntityName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineEntityName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineEntityName (" + dr["DepositLineEntityName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Entityref.Name = dr["DepositLineEntityName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Entityref.Name = dr["DepositLineEntityName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Entityref.Name = dr["DepositLineEntityName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Entityref.Name = dr["DepositLineEntityName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (Entityref.Name != null)
                            {
                                DepositLineDetails.EntityRef.Add(Entityref);

                            }

                            OnlineEntities.ClassRef classref = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("DepositLineClassRefName"))
                            {
                                #region Validations of DepositLineClassRefName
                                if (dr["DepositLineClassRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineClassRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineClassRefName (" + dr["DepositLineClassRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                classref.Name = dr["DepositLineClassRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                classref.Name = dr["DepositLineClassRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            classref.Name = dr["DepositLineClassRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        classref.Name = dr["DepositLineClassRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (classref.Name != null)
                            {
                                DepositLineDetails.ClassRef.Add(classref);

                            }
                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("DepositLineAccountRefName"))
                            {
                                #region Validations of DepositLineAccountRefName
                                if (dr["DepositLineAccountRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineAccountRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineAccountRefName (" + dr["DepositLineAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["DepositLineAccountRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (AccountRef.Name != null)
                            {
                                DepositLineDetails.AccountRef.Add(AccountRef);

                            }
                            OnlineEntities.PaymentMethodRef PaymentMethodRef = new OnlineEntities.PaymentMethodRef();

                            if (dt.Columns.Contains("DepositLinePaymentMethodRefName"))
                            {
                                #region Validations of DepositLineClassRefName
                                if (dr["DepositLinePaymentMethodRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLinePaymentMethodRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLinePaymentMethodRefName (" + dr["DepositLinePaymentMethodRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (PaymentMethodRef.Name != null)
                            {
                                DepositLineDetails.PaymentMethodRef.Add(PaymentMethodRef);
                            }
                            if (dt.Columns.Contains("DepositLineCheckNum"))
                            {
                                #region Validations of DepositLineCheckNum
                                if (dr["DepositLineCheckNum"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineCheckNum"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineCheckNum (" + dr["DepositLineCheckNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("DepositLineTxnType"))
                            {
                                #region Validations of DepositLineTxnType
                                if (dr["DepositLineTxnType"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineTxnType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineTxnType (" + dr["DepositLineTxnType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 476
                            if (dt.Columns.Contains("DepositLineTaxCodeRef"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["DepositLineTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineTaxCodeRef (" + dr["DepositLineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                    }
                                }

                                #endregion
                            }


                            if (dt.Columns.Contains("DepositLineTaxApplicableOn"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["DepositLineTaxApplicableOn"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineTaxApplicableOn"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineTaxApplicableOn (" + dr["DepositLineTaxApplicableOn"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                    }
                                }

                                #endregion
                            }


                            if (DepositLineDetails.ClassRef != null || DepositLineDetails.EntityRef != null || DepositLineDetails.PaymentMethodRef != null || DepositLineDetails.AccountRef != null || DepositLineDetails.CheckNum != null || DepositLineDetails.TxnType != null)
                                Line.DepositLineDetail.Add(DepositLineDetails);

                            Deposit.Line.Add(Line);


                            #endregion
                            #region  Line Custom Field
                            #region LineCustom1
                            OnlineEntities.CustomField LineCustomField1 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("LineCustomFieldName1"))
                            {
                                #region Validations of Custom1
                                if (dr["LineCustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldName1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldName1 (" + dr["LineCustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField1.Name = dr["LineCustomFieldName1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomFieldValue1"))
                            {
                                #region Validations of Custom2
                                if (dr["LineCustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldValue1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldValue1 (" + dr["LineCustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (LineCustomField1.Value != null || LineCustomField1.Name != null)
                            {
                                Line.LineCustomField.Add(LineCustomField1);
                            }
                            #endregion

                            #region LineCustom2
                            OnlineEntities.CustomField LineCustomField2 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("LineCustomFieldName2"))
                            {
                                #region Validations of LineCustom2
                                if (dr["LineCustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldName2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldName2 (" + dr["LineCustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField2.Name = dr["LineCustomFieldName2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomFieldValue2"))
                            {
                                #region Validations of LineCustom2
                                if (dr["LineCustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldValue2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldValue2 (" + dr["LineCustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (LineCustomField2.Name != null || LineCustomField2.Value != null)
                            {
                                Line.LineCustomField.Add(LineCustomField2);

                            }




                            #endregion

                            #region LineCustom3
                            OnlineEntities.CustomField LineCustomField3 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("LineCustomFieldName3"))
                            {
                                #region Validations of Custom3
                                if (dr["LineCustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldName3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldName3 (" + dr["LineCustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField3.Name = dr["LineCustomFieldName3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomFieldValue3"))
                            {
                                #region Validations of LineCustom3
                                if (dr["LineCustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldValue3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldValue3 (" + dr["LineCustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (LineCustomField3.Value != null || LineCustomField3.Name != null)
                            {
                                Line.LineCustomField.Add(LineCustomField3);

                            }
                            if (LineCustomField3.Value != null || LineCustomField3.Name != null || LineCustomField2.Name != null || LineCustomField2.Value != null || LineCustomField1.Value != null || LineCustomField1.Name != null)
                            {
                                Deposit.Line.Add(Line);

                            }

                            #endregion
                            #endregion
                            #endregion

                            OnlineEntities.DepositToAccountRef DepositToAccountRef = new OnlineEntities.DepositToAccountRef();

                            if (dt.Columns.Contains("DepositToAccountRefName"))
                            {
                                #region Validations of DepositToAccountRefName
                                if (dr["DepositToAccountRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositToAccountRefName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Deposit To Account RefName (" + dr["DepositToAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString().Substring(0, 10);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (DepositToAccountRef.Name != null)
                            {
                                Deposit.DepositToAccountRef.Add(DepositToAccountRef);
                            }
                            OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();

                            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();


                            if (dt.Columns.Contains("TxnTaxCodeRef"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRef (" + dr["TxnTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }

                            if (TxnTaxDetail.TaxLine.Count != 0 || TxnTaxCodeRef.Name != null)
                            {
                                Deposit.TxnTaxDetail.Add(TxnTaxDetail);
                            }



                            #region Cashback

                            OnlineEntities.CashBack CashBack = new OnlineEntities.CashBack();
                            OnlineEntities.AccountRef Accountref = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("CashBackAccountRef"))
                            {
                                #region Validations of Cashback account
                                if (dr["CashBackAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["CashBackAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CashBackAccountRef (" + dr["CashBackAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Accountref.Name = dr["CashBackAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Accountref.Name = dr["CashBackAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Accountref.Name = dr["CashBackAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Accountref.Name = dr["CashBackAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (Accountref.Name != null)
                            {
                                CashBack.AccountRef.Add(Accountref);
                            }

                            //Improvement::493
                            if (dt.Columns.Contains("CashBackAcctNum"))
                            {
                                #region Validations of Cashback account Number
                                if (dr["CashBackAcctNum"].ToString() != string.Empty)
                                {
                                    if (dr["CashBackAcctNum"].ToString().Length > 15)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CashBackAcctNum (" + dr["CashBackAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Accountref.AcctNum = dr["CashBackAcctNum"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Accountref.AcctNum = dr["CashBackAcctNum"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Accountref.AcctNum = dr["CashBackAcctNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Accountref.AcctNum = dr["CashBackAcctNum"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (Accountref.AcctNum != null)
                            {
                                CashBack.AccountRef.Add(Accountref);
                            }

                            if (dt.Columns.Contains("CashBackAmount"))
                            {
                                #region Validations of CashbackAmount
                                if (dr["CashBackAmount"].ToString() != string.Empty)
                                {
                                    if (dr["CashBackAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CashbackAmount (" + dr["CashBackAmount"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CashBack.Amount = dr["CashBackAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CashBack.Amount = dr["CashBackAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CashBack.Amount = dr["CashBackAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CashBack.Amount = dr["CashBackAmount"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CashBackMemo"))
                            {
                                #region Validations of CashBack Memo
                                if (dr["CashBackMemo"].ToString() != string.Empty)
                                {
                                    if (dr["CashBackMemo"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CashbackMemo (" + dr["CashBackMemo"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CashBack.Memo = dr["CashBackMemo"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CashBack.Memo = dr["CashBackMemo"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CashBack.Memo = dr["CashBackMemo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CashBack.Memo = dr["CashBackMemo"].ToString();
                                    }
                                }
                                #endregion
                            }

                            Deposit.CashBack.Add(CashBack);

                            #endregion


                            if (dt.Columns.Contains("TxnSource"))
                            {
                                #region Validations of TxnSource
                                if (dr["TxnSource"].ToString() != string.Empty)
                                {
                                    if (dr["TxnSource"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnSource (" + dr["TxnSource"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.TxnSource = dr["TxnSource"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.TxnSource = dr["TxnSource"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.TxnSource = dr["TxnSource"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.TxnSource = dr["TxnSource"].ToString();
                                    }
                                }
                                #endregion
                            }



                            if (dt.Columns.Contains("TotalAmt"))
                            {
                                #region Validations for TotalAmt
                                if (dr["TotalAmt"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TotalAmt"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TotalAmt ( " + dr["TotalAmt"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.TotalAmt = dr["TotalAmt"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.TotalAmt = dr["TotalAmt"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.TotalAmt = dr["TotalAmt"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.TotalAmt = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalAmt"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            //bug 476 add ExchangeRate,GlobalTaxCalculation
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "ExchangeRate")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("GlobalTaxCalculation"))
                            {
                                #region Validations of GlobalTaxCalculation
                                if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                                {
                                    if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                    }
                                }
                                #endregion
                            }

                            //end bug 476
                            coll.Add(Deposit);
                        }
                        else
                        {

                            #region line
                            OnlineEntities.DepositLineDetails DepositLineDetails = new OnlineEntities.DepositLineDetails();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.Linkedtxn Linkedtxn = new OnlineEntities.Linkedtxn();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LinkedtxnDocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["LinkedtxnDocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LinkedtxnDocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LinkedToTxn DocNumber (" + dr["LinkedtxnDocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LinkedtxnTxnType"))
                            {
                                #region Validations of docnumber
                                if (dr["LinkedtxnTxnType"].ToString() != string.Empty)
                                {
                                    if (dr["LinkedtxnTxnType"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Linkedtxn TxnType (" + dr["LinkedtxnTxnType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (Linkedtxn.ID != null && Linkedtxn.Type != null)
                            {
                                Line.LinkedTxn.Add(Linkedtxn);
                            }
                            #region deposite line details
                            OnlineEntities.Entity Entityref = new OnlineEntities.Entity();
                            if (dt.Columns.Contains("DepositLineEntityName"))
                            {
                                #region Validations of DepositLineEntitytype
                                if (dr["DepositLineEntityName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineEntityName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineEntityName (" + dr["DepositLineEntityName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Entityref.Name = dr["DepositLineEntityName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Entityref.Name = dr["DepositLineEntityName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Entityref.Name = dr["DepositLineEntityName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Entityref.Name = dr["DepositLineEntityName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (Entityref.Name != null)
                            {
                                DepositLineDetails.EntityRef.Add(Entityref);

                            }
                            if (dt.Columns.Contains("DepositLineEntityType"))
                            {
                                #region Validations of DepositLineEntitytype
                                if (dr["DepositLineEntityType"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineEntityType"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineEntityType (" + dr["DepositLineEntityType"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Entityref.Type = dr["DepositLineEntityType"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Entityref.Type = dr["DepositLineEntityType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Entityref.Type = dr["DepositLineEntityType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Entityref.Type = dr["DepositLineEntityType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (Entityref.Type != null)
                            {
                                DepositLineDetails.EntityRef.Add(Entityref);

                            }
                            OnlineEntities.ClassRef classref = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("DepositLineClassRefName"))
                            {
                                #region Validations of DepositLineClassRefName
                                if (dr["DepositLineClassRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineClassRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineClassRefName (" + dr["DepositLineClassRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                classref.Name = dr["DepositLineClassRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                classref.Name = dr["DepositLineClassRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            classref.Name = dr["DepositLineClassRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        classref.Name = dr["DepositLineClassRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (classref.Name != null)
                            {
                                DepositLineDetails.ClassRef.Add(classref);

                            }
                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("DepositLineAccountRefName"))
                            {
                                #region Validations of DepositLineAccountRefName
                                if (dr["DepositLineAccountRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineAccountRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineAccountRefName (" + dr["DepositLineAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["DepositLineAccountRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (AccountRef.Name != null)
                            {
                                DepositLineDetails.AccountRef.Add(AccountRef);

                            }

                            //Improvement::493
                            //OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("DepositLineAcctNum"))
                            {
                                #region Validations of DepositLineAcctNum
                                if (dr["DepositLineAcctNum"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineAcctNum"].ToString().Length > 15)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineAcctNum (" + dr["DepositLineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (AccountRef.AcctNum != null)
                            {
                                DepositLineDetails.AccountRef.Add(AccountRef);
                            }

                            OnlineEntities.PaymentMethodRef PaymentMethodRef = new OnlineEntities.PaymentMethodRef();

                            if (dt.Columns.Contains("DepositLinePaymentMethodRefName"))
                            {
                                #region Validations of DepositLineClassRefName
                                if (dr["DepositLinePaymentMethodRefName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLinePaymentMethodRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLinePaymentMethodRefName (" + dr["DepositLinePaymentMethodRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (PaymentMethodRef.Name != null)
                            {
                                DepositLineDetails.PaymentMethodRef.Add(PaymentMethodRef);
                            }
                            if (dt.Columns.Contains("DepositLineCheckNum"))
                            {
                                #region Validations of DepositLineCheckNum
                                if (dr["DepositLineCheckNum"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineCheckNum"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineCheckNum (" + dr["DepositLineCheckNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("DepositLineTxnType"))
                            {
                                #region Validations of DepositLineTxnType
                                if (dr["DepositLineTxnType"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineTxnType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineTxnType (" + dr["DepositLineTxnType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bbug 476
                            if (dt.Columns.Contains("DepositLineTaxCodeRef"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["DepositLineTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineTaxCodeRef (" + dr["DepositLineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("DepositLineTaxApplicableOn"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["DepositLineTaxApplicableOn"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineTaxApplicableOn"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositLineTaxApplicableOn (" + dr["DepositLineTaxApplicableOn"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                    }
                                }

                                #endregion
                            }


                            if (DepositLineDetails.ClassRef != null || DepositLineDetails.EntityRef != null || DepositLineDetails.PaymentMethodRef != null || DepositLineDetails.AccountRef != null || DepositLineDetails.CheckNum != null || DepositLineDetails.TxnType != null)
                                Line.DepositLineDetail.Add(DepositLineDetails);
                            Deposit.Line.Add(Line);
                            #endregion
                            #region  Line Custom Field
                            #region LineCustom1
                            OnlineEntities.CustomField LineCustomField1 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("LineCustomFieldName1"))
                            {
                                #region Validations of Custom1
                                if (dr["LineCustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldName1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldName1 (" + dr["LineCustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField1.Name = dr["LineCustomFieldName1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomFieldValue1"))
                            {
                                #region Validations of Custom2
                                if (dr["LineCustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldValue1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldValue1 (" + dr["LineCustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (LineCustomField1.Value != null || LineCustomField1.Name != null)
                            {
                                Line.LineCustomField.Add(LineCustomField1);
                            }
                            #endregion

                            #region LineCustom2
                            OnlineEntities.CustomField LineCustomField2 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("LineCustomFieldName2"))
                            {
                                #region Validations of LineCustom2
                                if (dr["LineCustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldName2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldName2 (" + dr["LineCustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField2.Name = dr["LineCustomFieldName2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomFieldValue2"))
                            {
                                #region Validations of LineCustom2
                                if (dr["LineCustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldValue2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldValue2 (" + dr["LineCustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (LineCustomField2.Name != null || LineCustomField2.Value != null)
                            {
                                Line.LineCustomField.Add(LineCustomField2);

                            }




                            #endregion

                            #region LineCustom3
                            OnlineEntities.CustomField LineCustomField3 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("LineCustomFieldName3"))
                            {
                                #region Validations of Custom3
                                if (dr["LineCustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldName3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldName3 (" + dr["LineCustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField3.Name = dr["LineCustomFieldName3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomFieldValue3"))
                            {
                                #region Validations of LineCustom3
                                if (dr["LineCustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomFieldValue3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomFieldValue3 (" + dr["LineCustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (LineCustomField3.Value != null || LineCustomField3.Name != null)
                            {
                                Line.LineCustomField.Add(LineCustomField3);

                            }
                            if (LineCustomField3.Value != null || LineCustomField3.Name != null || LineCustomField2.Name != null || LineCustomField2.Value != null || LineCustomField1.Value != null || LineCustomField1.Name != null)
                            {
                                Deposit.Line.Add(Line);

                            }

                            #endregion
                            #endregion
                            #endregion

                        }

                    }
                    else
                    {
                        Deposit = new OnlineDepositeQBEntry();
                        if (dt.Columns.Contains("DocNumber"))
                        {
                            #region Validations of docnumber
                            if (dr["DocNumber"].ToString() != string.Empty)
                            {
                                if (dr["DocNumber"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Deposit.DocNumber = dr["DocNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of txn Date
                            DateTime DODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out DODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Deposit.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        DODate = dttest;
                                        Deposit.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    DODate = Convert.ToDateTime(datevalue);
                                    Deposit.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                        if (dt.Columns.Contains("DepartmentRefName"))
                        {
                            #region Validations of DepartmentRefName
                            if (dr["DepartmentRefName"].ToString() != string.Empty)
                            {
                                if (dr["DepartmentRefName"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepartmentRefName (" + dr["DepartmentRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepartmentRef.Name = dr["DepartmentRefName"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    DepartmentRef.Name = dr["DepartmentRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (DepartmentRef.Name != null)
                        {
                            Deposit.DepartmentRef.Add(DepartmentRef);
                        }
                        OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                        if (dt.Columns.Contains("CurrencyRefName"))
                        {
                            #region Validations of CurrencyRef
                            if (dr["CurrencyRefName"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRefName"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRefName (" + dr["CurrencyRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CurrencyRef.Name = dr["CurrencyRefName"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CurrencyRef.Name = dr["CurrencyRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    CurrencyRef.Name = dr["CurrencyRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CurrencyRef.Name != null)
                        {
                            Deposit.CurrencyRef.Add(CurrencyRef);
                        }

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    Deposit.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TxnStatus"))
                        {
                            #region Validations of TxnStatus
                            if (dr["TxnStatus"].ToString() != string.Empty)
                            {
                                if (dr["TxnStatus"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnStatus (" + dr["TxnStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.TxnStatus = dr["TxnStatus"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.TxnStatus = dr["TxnStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.TxnStatus = dr["TxnStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    Deposit.TxnStatus = dr["TxnStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        #region Custom1
                        OnlineEntities.CustomField CustomField = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName1"))
                        {
                            #region Validations of Custom1
                            if (dr["CustomFieldName1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName1 (" + dr["CustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField.Name = dr["CustomFieldName1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField.Name = dr["CustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Name = dr["CustomFieldName1"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField.Name = dr["CustomFieldName1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue1"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldValue1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue1 (" + dr["CustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField.Value = dr["CustomFieldValue1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField.Value = dr["CustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Value = dr["CustomFieldValue1"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField.Value = dr["CustomFieldValue1"].ToString();
                                }
                            }
                            #endregion
                        }


                        #endregion

                        #region Custom2
                        OnlineEntities.CustomField CustomField2 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName2"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldName2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName2 (" + dr["CustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField2.Name = dr["CustomFieldName2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField2.Name = dr["CustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Name = dr["CustomFieldName2"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField2.Name = dr["CustomFieldName2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue2"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldValue2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue2 (" + dr["CustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                }
                            }
                            #endregion
                        }



                        #endregion

                        #region Custom3
                        OnlineEntities.CustomField CustomField3 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName3"))
                        {
                            #region Validations of Custom3
                            if (dr["CustomFieldName3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName3 (" + dr["CustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField3.Name = dr["CustomFieldName3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField3.Name = dr["CustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Name = dr["CustomFieldName3"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField3.Name = dr["CustomFieldName3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue3"))
                        {
                            #region Validations of Custom3
                            if (dr["CustomFieldValue3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue3 (" + dr["CustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomField3.Value != null || CustomField3.Name != null)
                        {
                            Deposit.CustomField.Add(CustomField3);
                        }
                        if (CustomField.Value != null || CustomField.Name != null)
                        {
                            Deposit.CustomField.Add(CustomField);
                        }

                        if (CustomField2.Value != null || CustomField2.Name != null)
                        {
                            Deposit.CustomField.Add(CustomField2);
                        }

                        #endregion


                        #region line
                        OnlineEntities.DepositLineDetails DepositLineDetails = new OnlineEntities.DepositLineDetails();
                        OnlineEntities.Line Line = new OnlineEntities.Line();
                        OnlineEntities.Linkedtxn Linkedtxn = new OnlineEntities.Linkedtxn();

                        if (dt.Columns.Contains("LineDescription"))
                        {
                            #region Validations of Associate
                            if (dr["LineDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineDescription"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineDescription = dr["LineDescription"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineAmount"))
                        {
                            #region Validations for LineAmount
                            if (dr["LineAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LinkedtxnDocNumber"))
                        {
                            #region Validations of docnumber
                            if (dr["LinkedtxnDocNumber"].ToString() != string.Empty)
                            {
                                if (dr["LinkedtxnDocNumber"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LinkedToTxn DocNumber (" + dr["LinkedtxnDocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Linkedtxn.ID = dr["LinkedtxnDocNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LinkedtxnTxnType"))
                        {
                            #region Validations of docnumber
                            if (dr["LinkedtxnTxnType"].ToString() != string.Empty)
                            {
                                if (dr["LinkedtxnTxnType"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Linkedtxn TxnType (" + dr["LinkedtxnTxnType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                    }
                                }
                                else
                                {
                                    Linkedtxn.Type = dr["LinkedtxnTxnType"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (Linkedtxn.ID != null && Linkedtxn.Type != null)
                        {
                            Line.LinkedTxn.Add(Linkedtxn);
                        }
                        #region deposite line details
                        OnlineEntities.Entity Entityref = new OnlineEntities.Entity();
                        if (dt.Columns.Contains("DepositLineEntityName"))
                        {
                            #region Validations of DepositLineEntityName
                            if (dr["DepositLineEntityName"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineEntityName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineEntityName (" + dr["DepositLineEntityName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Entityref.Name = dr["DepositLineEntityName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Entityref.Name = dr["DepositLineEntityName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Entityref.Name = dr["DepositLineEntityName"].ToString();
                                    }
                                }
                                else
                                {
                                    Entityref.Name = dr["DepositLineEntityName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (Entityref.Name != null)
                        {
                            DepositLineDetails.EntityRef.Add(Entityref);

                        }
                        if (dt.Columns.Contains("DepositLineEntityType"))
                        {
                            #region Validations of DepositLineEntitytype
                            if (dr["DepositLineEntityType"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineEntityType"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineEntityType (" + dr["DepositLineEntityType"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Entityref.Type = dr["DepositLineEntityType"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Entityref.Type = dr["DepositLineEntityType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Entityref.Type = dr["DepositLineEntityType"].ToString();
                                    }
                                }
                                else
                                {
                                    Entityref.Type = dr["DepositLineEntityType"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (Entityref.Type != null)
                        {
                            DepositLineDetails.EntityRef.Add(Entityref);

                        }
                        OnlineEntities.ClassRef classref = new OnlineEntities.ClassRef();
                        if (dt.Columns.Contains("DepositLineClassRefName"))
                        {
                            #region Validations of DepositLineClassRefName
                            if (dr["DepositLineClassRefName"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineClassRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineClassRefName (" + dr["DepositLineClassRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            classref.Name = dr["DepositLineClassRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            classref.Name = dr["DepositLineClassRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        classref.Name = dr["DepositLineClassRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    classref.Name = dr["DepositLineClassRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (classref.Name != null)
                        {
                            DepositLineDetails.ClassRef.Add(classref);

                        }

                        ////bbug 476
                        //if (dt.Columns.Contains("DepositLineTaxCodeRef"))
                        //{
                        //    #region Validations of TxnTaxCodeRefName
                        //    if (dr["DepositLineTaxCodeRef"].ToString() != string.Empty)
                        //    {
                        //        if (dr["DepositLineTaxCodeRef"].ToString().Length > 100)
                        //        {
                        //            if (isIgnoreAll == false)
                        //            {
                        //                string strMessages = "This DepositLineTaxCodeRef (" + dr["DepositLineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                        //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                        //                if (Convert.ToString(result) == "Cancel")
                        //                {
                        //                    continue;
                        //                }
                        //                if (Convert.ToString(result) == "No")
                        //                {
                        //                    return null;
                        //                }
                        //                if (Convert.ToString(result) == "Ignore")
                        //                {
                        //                    DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString().Substring(0, 100);
                        //                }
                        //                if (Convert.ToString(result) == "Abort")
                        //                {
                        //                    isIgnoreAll = true;
                        //                    DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                        //            }
                        //        }
                        //        else
                        //        {
                        //            DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                        //        }
                        //    }

                        //    #endregion
                        //}


                        ////DepositLineDetails.TxnType..Add(TxnTaxCodeRef);

                        OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                        if (dt.Columns.Contains("DepositLineAccountRefName"))
                        {
                            #region Validations of DepositLineAccountRefName
                            if (dr["DepositLineAccountRefName"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineAccountRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineAccountRefName (" + dr["DepositLineAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountRef.Name = dr["DepositLineAccountRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountRef.Name = dr["DepositLineAccountRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (AccountRef.Name != null)
                        {
                            DepositLineDetails.AccountRef.Add(AccountRef);

                        }

                        //Improvement::493
                        //OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                        if (dt.Columns.Contains("DepositLineAcctNum"))
                        {
                            #region Validations of DepositLineAcctNum
                            if (dr["DepositLineAcctNum"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineAcctNum"].ToString().Length > 15)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineAcctNum (" + dr["DepositLineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString().Substring(0, 15);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountRef.AcctNum = dr["DepositLineAcctNum"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (AccountRef.AcctNum != null)
                        {
                            DepositLineDetails.AccountRef.Add(AccountRef);
                        }

                        OnlineEntities.PaymentMethodRef PaymentMethodRef = new OnlineEntities.PaymentMethodRef();

                        if (dt.Columns.Contains("DepositLinePaymentMethodRefName"))
                        {
                            #region Validations of DepositLineClassRefName
                            if (dr["DepositLinePaymentMethodRefName"].ToString() != string.Empty)
                            {
                                if (dr["DepositLinePaymentMethodRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLinePaymentMethodRefName (" + dr["DepositLinePaymentMethodRefName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    PaymentMethodRef.Name = dr["DepositLinePaymentMethodRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (PaymentMethodRef.Name != null)
                        {
                            DepositLineDetails.PaymentMethodRef.Add(PaymentMethodRef);
                        }
                        if (dt.Columns.Contains("DepositLineCheckNum"))
                        {
                            #region Validations of DepositLineCheckNum
                            if (dr["DepositLineCheckNum"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineCheckNum"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineCheckNum (" + dr["DepositLineCheckNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                    }
                                }
                                else
                                {
                                    DepositLineDetails.CheckNum = dr["DepositLineCheckNum"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("DepositLineTxnType"))
                        {
                            #region Validations of DepositLineTxnType
                            if (dr["DepositLineTxnType"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineTxnType"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineTxnType (" + dr["DepositLineTxnType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                    }
                                }
                                else
                                {
                                    DepositLineDetails.TxnType = dr["DepositLineTxnType"].ToString();
                                }
                            }
                            #endregion
                        }

                        //bug 476
                        if (dt.Columns.Contains("DepositLineTaxCodeRef"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["DepositLineTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineTaxCodeRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineTaxCodeRef (" + dr["DepositLineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    DepositLineDetails.TaxCodeRef = dr["DepositLineTaxCodeRef"].ToString();
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("DepositLineTaxApplicableOn"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["DepositLineTaxApplicableOn"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineTaxApplicableOn"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineTaxApplicableOn (" + dr["DepositLineTaxApplicableOn"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                    }
                                }
                                else
                                {
                                    DepositLineDetails.TaxApplicableOn = dr["DepositLineTaxApplicableOn"].ToString();
                                }
                            }

                            #endregion
                        }
                        if (DepositLineDetails.ClassRef != null || DepositLineDetails.EntityRef != null || DepositLineDetails.PaymentMethodRef != null || DepositLineDetails.AccountRef != null || DepositLineDetails.CheckNum != null || DepositLineDetails.TxnType != null)
                            Line.DepositLineDetail.Add(DepositLineDetails);
                        Deposit.Line.Add(Line);
                        #endregion

                        #region  Line Custom Field
                        #region LineCustom1
                        OnlineEntities.CustomField LineCustomField1 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("LineCustomFieldName1"))
                        {
                            #region Validations of Custom1
                            if (dr["LineCustomFieldName1"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomFieldName1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomFieldName1 (" + dr["LineCustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LineCustomField1.Name = dr["LineCustomFieldName1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                    }
                                }
                                else
                                {
                                    LineCustomField1.Name = dr["LineCustomFieldName1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineCustomFieldValue1"))
                        {
                            #region Validations of Custom2
                            if (dr["LineCustomFieldValue1"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomFieldValue1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomFieldValue1 (" + dr["LineCustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                    }
                                }
                                else
                                {
                                    LineCustomField1.Value = dr["LineCustomFieldValue1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (LineCustomField1.Value != null || LineCustomField1.Name != null)
                        {
                            Line.LineCustomField.Add(LineCustomField1);
                        }
                        #endregion

                        #region LineCustom2
                        OnlineEntities.CustomField LineCustomField2 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("LineCustomFieldName2"))
                        {
                            #region Validations of LineCustom2
                            if (dr["LineCustomFieldName2"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomFieldName2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomFieldName2 (" + dr["LineCustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LineCustomField2.Name = dr["LineCustomFieldName2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                    }
                                }
                                else
                                {
                                    LineCustomField2.Name = dr["LineCustomFieldName2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineCustomFieldValue2"))
                        {
                            #region Validations of LineCustom2
                            if (dr["LineCustomFieldValue2"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomFieldValue2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomFieldValue2 (" + dr["LineCustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                    }
                                }
                                else
                                {
                                    LineCustomField2.Value = dr["LineCustomFieldValue2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (LineCustomField2.Name != null || LineCustomField2.Value != null)
                        {
                            Line.LineCustomField.Add(LineCustomField2);

                        }




                        #endregion

                        #region LineCustom3
                        OnlineEntities.CustomField LineCustomField3 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("LineCustomFieldName3"))
                        {
                            #region Validations of Custom3
                            if (dr["LineCustomFieldName3"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomFieldName3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomFieldName3 (" + dr["LineCustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LineCustomField3.Name = dr["LineCustomFieldName3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                    }
                                }
                                else
                                {
                                    LineCustomField3.Name = dr["LineCustomFieldName3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineCustomFieldValue3"))
                        {
                            #region Validations of LineCustom3
                            if (dr["LineCustomFieldValue3"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomFieldValue3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomFieldValue3 (" + dr["LineCustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                    }
                                }
                                else
                                {
                                    LineCustomField3.Value = dr["LineCustomFieldValue3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (LineCustomField3.Value != null || LineCustomField3.Name != null)
                        {
                            Line.LineCustomField.Add(LineCustomField3);

                        }
                        if (LineCustomField3.Value != null || LineCustomField3.Name != null || LineCustomField2.Name != null || LineCustomField2.Value != null || LineCustomField1.Value != null || LineCustomField1.Name != null)
                        {
                            Deposit.Line.Add(Line);

                        }

                        #endregion
                        #endregion
                        #endregion

                        OnlineEntities.DepositToAccountRef DepositToAccountRef = new OnlineEntities.DepositToAccountRef();

                        if (dt.Columns.Contains("DepositToAccountRefName"))
                        {
                            #region Validations of DepositToAccountRefName
                            if (dr["DepositToAccountRefName"].ToString() != string.Empty)
                            {
                                if (dr["DepositToAccountRefName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Deposit To Account RefName (" + dr["DepositToAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString().Substring(0, 10);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    DepositToAccountRef.Name = dr["DepositToAccountRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (DepositToAccountRef.Name != null)
                        {
                            Deposit.DepositToAccountRef.Add(DepositToAccountRef);
                        }


                        OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();

                        OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();


                        if (dt.Columns.Contains("TxnTaxCodeRef"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["TxnTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["TxnTaxCodeRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnTaxCodeRef (" + dr["TxnTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxCodeRef.Name = dr["TxnTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (TxnTaxCodeRef.Name != null)
                        {
                            TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                        }

                        if (TxnTaxDetail.TaxLine.Count != 0 || TxnTaxCodeRef.Name != null)
                        {
                            Deposit.TxnTaxDetail.Add(TxnTaxDetail);
                        }



                        #region Cashback

                        OnlineEntities.CashBack CashBack = new OnlineEntities.CashBack();
                        OnlineEntities.AccountRef Accountref = new OnlineEntities.AccountRef();
                        if (dt.Columns.Contains("CashBackAccountRef"))
                        {
                            #region Validations of Cashback account
                            if (dr["CashBackAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["CashBackAccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CashBackAccountRef (" + dr["CashBackAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Accountref.Name = dr["CashBackAccountRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Accountref.Name = dr["CashBackAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Accountref.Name = dr["CashBackAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    Accountref.Name = dr["CashBackAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (Accountref.Name != null)
                        {
                            CashBack.AccountRef.Add(Accountref);

                        }

                        //Improvement::493
                        if (dt.Columns.Contains("CashBackAcctNum"))
                        {
                            #region Validations of Cashback account Number
                            if (dr["CashBackAcctNum"].ToString() != string.Empty)
                            {
                                if (dr["CashBackAcctNum"].ToString().Length > 15)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CashBackAcctNum (" + dr["CashBackAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Accountref.AcctNum = dr["CashBackAcctNum"].ToString().Substring(0, 15);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Accountref.AcctNum = dr["CashBackAcctNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Accountref.AcctNum = dr["CashBackAcctNum"].ToString();
                                    }
                                }
                                else
                                {
                                    Accountref.AcctNum = dr["CashBackAcctNum"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (Accountref.AcctNum != null)
                        {
                            CashBack.AccountRef.Add(Accountref);
                        }

                        if (dt.Columns.Contains("CashBackAmount"))
                        {
                            #region Validations of CashbackAmount
                            if (dr["CashBackAmount"].ToString() != string.Empty)
                            {
                                if (dr["CashBackAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CashbackAmount (" + dr["CashBackAmount"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CashBack.Amount = dr["CashBackAmount"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CashBack.Amount = dr["CashBackAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CashBack.Amount = dr["CashBackAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    CashBack.Amount = dr["CashBackAmount"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CashBackMemo"))
                        {
                            #region Validations of CashBack Memo
                            if (dr["CashBackMemo"].ToString() != string.Empty)
                            {
                                if (dr["CashBackMemo"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CashbackMemo (" + dr["CashBackMemo"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CashBack.Memo = dr["CashBackMemo"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CashBack.Memo = dr["CashBackMemo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CashBack.Memo = dr["CashBackMemo"].ToString();
                                    }
                                }
                                else
                                {
                                    CashBack.Memo = dr["CashBackMemo"].ToString();
                                }
                            }
                            #endregion
                        }

                        Deposit.CashBack.Add(CashBack);

                        #endregion


                        if (dt.Columns.Contains("TxnSource"))
                        {
                            #region Validations of TxnSource
                            if (dr["TxnSource"].ToString() != string.Empty)
                            {
                                if (dr["TxnSource"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnSource (" + dr["TxnSource"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.TxnSource = dr["TxnSource"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.TxnSource = dr["TxnSource"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.TxnSource = dr["TxnSource"].ToString();
                                    }
                                }
                                else
                                {
                                    Deposit.TxnSource = dr["TxnSource"].ToString();
                                }
                            }
                            #endregion
                        }



                        if (dt.Columns.Contains("TotalAmt"))
                        {
                            #region Validations for TotalAmt
                            if (dr["TotalAmt"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TotalAmt"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TotalAmt ( " + dr["TotalAmt"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.TotalAmt = dr["TotalAmt"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.TotalAmt = dr["TotalAmt"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.TotalAmt = dr["TotalAmt"].ToString();
                                    }
                                }
                                else
                                {
                                    Deposit.TotalAmt = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalAmt"].ToString()));
                                }
                            }

                            #endregion
                        }


                        //bug 476 add ExchangeRate,GlobalTaxCalculation
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "ExchangeRate")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    Deposit.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("GlobalTaxCalculation"))
                        {
                            #region Validations of GlobalTaxCalculation
                            if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                            {
                                if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                    }
                                }
                                else
                                {
                                    Deposit.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                }
                            }
                            #endregion
                        }


                        coll.Add(Deposit);
                    }
                }

            }
            #endregion

            #endregion

            return coll;
        }
    }
}
