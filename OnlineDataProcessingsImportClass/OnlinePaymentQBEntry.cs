﻿using System;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;
//OnlinePaymentQBEntry

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("Payment", Namespace = "", IsNullable = false)]

    public class Payment
    {
       
        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_PaymentRefNum;
        private string m_TotalPayment; 
        private string m_TxnStatus;       
        private string m_TxnDate;
        private string m_ExchangeRate;
        private string m_ProcessPayment;
        private string m_UnappliedAmt;

        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.PaymentLineDetail> m_PaymentLineBased = new Collection<OnlineEntities.PaymentLineDetail>();
        private Collection<OnlineEntities.ARAccountRef> m_ARAccountRef = new Collection<OnlineEntities.ARAccountRef>();
        private Collection<OnlineEntities.DepositToAccountRef> m_DepositToAccountRef = new Collection<OnlineEntities.DepositToAccountRef>();
       
        //594
        private bool m_isAppend;

        #endregion

        #region Constructor
        public Payment()
        {
        }
        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }
       
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        private Collection<CustomerRef> m_customerRef = new Collection<CustomerRef>();

        public Collection<CustomerRef> customerRef
        {
            get { return m_customerRef; }
            set { m_customerRef = value; }
        }

        private Collection<PaymentMethodRef> m_PaymentMethodRef = new Collection<PaymentMethodRef>();

        public Collection<OnlineEntities.PaymentMethodRef> PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }
        public string PaymentRefNum
        {
            get { return m_PaymentRefNum; }
            set { m_PaymentRefNum = value; }
        }
        public string TotalPayment
        {
            get { return m_TotalPayment; }
            set { m_TotalPayment = value; }
        }
        public string TxnStatus
        {
            get { return m_TxnStatus; }
            set { m_TxnStatus = value; }
        }

       

        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }    

       
        public Collection<OnlineEntities.ARAccountRef> ARAccountRef
        {
            get { return m_ARAccountRef; }
            set { m_ARAccountRef = value; }
        }
        public Collection<OnlineEntities.DepositToAccountRef> DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
            set { m_DepositToAccountRef = value; }
        }

       
        public Collection<OnlineEntities.PaymentLineDetail> PaymentLineBased
        {
            get { return m_PaymentLineBased; }
            set { m_PaymentLineBased = value; }
        }
        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        public string ProcessPayment
        {
            get { return m_ProcessPayment; }
            set { m_ProcessPayment = value; }
        }
        public string UnappliedAmt
        {
            get { return m_UnappliedAmt; }
            set { m_UnappliedAmt = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region  Public Methods

        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>

        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

      

        /// <summary>
        /// Creating new payment transaction in quickbook online. 
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOPayment(OnlineDataProcessingsImportClass.Payment coll)
        {
            int linecount = 1;
          
            DataService dataService = null;
            Intuit.Ipp.Data.Payment PaymentAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.Payment addedPayment = new Intuit.Ipp.Data.Payment();
            Payment payment = new Payment();
            payment = coll;
            Intuit.Ipp.Data.PaymentLineDetail PaymentLineDetail = new Intuit.Ipp.Data.PaymentLineDetail();       
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.LinkedTxn linkedtxn = new Intuit.Ipp.Data.LinkedTxn();
            
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            try
            {
                #region Add Payment

                //594
                if (payment.isAppend == true)
                {
                    addedPayment.sparse = true;
                }

                if (payment.DocNumber != null)
                {
                    addedPayment.DocNumber = payment.DocNumber;
                }               

                #region TxnDate,privatenote
                if (payment.TxnDate != null)
                {
                    try
                    {
                        addedPayment.TxnDate = Convert.ToDateTime(payment.TxnDate);
                        addedPayment.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (payment.PrivateNote != null)
                {
                    addedPayment.PrivateNote = payment.PrivateNote;
                }
                if (payment.TxnStatus != null)
                {
                    addedPayment.TxnStatus = "PAID";
                  
                }
                #endregion
                #region find customer
                Customer customer = new Customer();
                string customerName = string.Empty;
                foreach (var cust in payment.customerRef)
                {
                    customerName = cust.Name;
                }
                string customervalue = string.Empty;
                if (customerName != string.Empty)
                {
                    var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerName.Trim());
                    foreach (var customer1 in dataCustomer)
                    {
                        customervalue = customer1.Id;
                    }
                    addedPayment.CustomerRef = new ReferenceType()
                    {
                        name = customerName,
                        Value = customervalue
                    };
                }
                #endregion
                #region find ARAccountRef
                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                string accountName = string.Empty;
                string accountNumber = string.Empty;
                foreach (var acc in payment.ARAccountRef)
                {
                    accountName = acc.Name;
                    
                }
                if (accountName != string.Empty && accountName != null && accountName != "")
                {
                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                    addedPayment.ARAccountRef = new ReferenceType()
                    {
                        name = accDetails.name,
                        Value = accDetails.Value
                    };
                }
               

                #endregion
                #region find DepositeToAccountRef
                QueryService<Account> DepositeToAccountRefQueryService = new QueryService<Account>(serviceContext);

                accountName = string.Empty;
                accountNumber = string.Empty;
                foreach (var acc in payment.DepositToAccountRef)
                {
                    accountName = acc.Name;
                   
                }
                if (accountName != string.Empty && accountName != null && accountName != "")
                {
                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                    addedPayment.DepositToAccountRef = new ReferenceType()
                    {
                        name = accDetails.name,
                        Value = accDetails.Value
                    };
                }
               

                #endregion
                #region find PaymentMethodRef
                if (payment.PaymentMethodRef != null)
                {
                    foreach (var paymentMethod in payment.PaymentMethodRef)
                    {
                        string classValue = string.Empty;
                        if (paymentMethod.Name != null)
                        {
                            var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());

                            foreach (var data in dataPaymentMethod)
                            {
                                classValue = data.Id;
                            }
                            addedPayment.PaymentMethodRef = new ReferenceType()
                            {
                                name = paymentMethod.Name,
                                Value = classValue
                            };
                        }
                    }
                }

                #endregion
                #region find PaymentRefNum
                if (payment.PaymentRefNum != null)
                {
                    addedPayment.PaymentRefNum = payment.PaymentRefNum;
                }
                #endregion               
                #region find TotalPayment  ExchangeRate CurrencyRef
                if (payment.TotalPayment != null)
                {
                    addedPayment.TotalAmt =Convert.ToDecimal(payment.TotalPayment);
                    addedPayment.TotalAmtSpecified = true;
                }
                if (payment.ExchangeRate != null)
                {
                    addedPayment.ExchangeRate = Convert.ToDecimal(payment.ExchangeRate);
                    addedPayment.ExchangeRateSpecified = true;
                }

                if (payment.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in payment.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (payment.CurrencyRef.Count != 0)
                    {
                        addedPayment.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #endregion

                #region find applytobill
                string applytobill = string.Empty;
                bool flag = false;
                foreach (var l in payment.Line)
                {
                    #region
                    if (flag == false)
                    {
                        if (l.LineAmount != null)
                        {
                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                            Line.AmountSpecified = true;
                        }

                        foreach (var link in l.LinkedTxn)
                        {
                            applytobill = link.Applytobillref;
                        }
                        string bankaccountvalue = string.Empty;
                        if (applytobill != string.Empty && addedPayment.CustomerRef != null && addedPayment.CustomerRef.Value != string.Empty)
                        {
                            var bankdataAccount = await CommonUtilities.GetInvoiceExistsInOnlineQBO(applytobill.Trim(), addedPayment.CustomerRef.Value);
                            foreach (var dAccount in bankdataAccount)
                            {
                               bankaccountvalue = dAccount.Id;
                            }
                            linkedtxn.TxnId = bankaccountvalue;
                            linkedtxn.TxnType = "Invoice";
                            Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };
                            lines_online[linecount - 1] = Line;
                        }
                        addedPayment.Line = AddLine(lines_online, Line, linecount);    
                        flag = true;
                    }
                    else if(flag == true)
                    {
                        Line = new Intuit.Ipp.Data.Line();
                        linkedtxn = new LinkedTxn();
                        linecount++;
                       
                        if (l.LineAmount != null)
                        {
                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                            Line.AmountSpecified = true;
                        }

                        foreach (var link in l.LinkedTxn)
                        {
                            applytobill = link.Applytobillref;
                        }
                        string bankaccountvalue = string.Empty;
                        if (applytobill != string.Empty && addedPayment.CustomerRef != null && addedPayment.CustomerRef.Value != string.Empty)
                        {
                            var bankdataAccount = await CommonUtilities.GetInvoiceExistsInOnlineQBO(applytobill.Trim(), addedPayment.CustomerRef.Value);
                            foreach (var dAccount in bankdataAccount)
                            {
                                bankaccountvalue = dAccount.Id;
                            }

                            linkedtxn.TxnId = bankaccountvalue;
                            linkedtxn.TxnType = "Invoice";
                          
                            Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };
                            Array.Resize(ref lines_online, linecount);
                            lines_online[linecount - 1] = Line;
                        }
                        addedPayment.Line = AddLine(lines_online, Line, linecount);    
                       
                    }
                   
                    #endregion
                }

                       
                #endregion

                #region UnappliedAmt ProcessPayment
             
                if (payment.UnappliedAmt != null)
                {
                    addedPayment.UnappliedAmt = 0;
                    addedPayment.UnappliedAmtSpecified = true;
                }
                if (payment.ProcessPayment != null)
                {
                    addedPayment.ProcessPayment = true;
                    addedPayment.ProcessPaymentSpecified = true;
                }
                #endregion

                PaymentAdded = new Intuit.Ipp.Data.Payment();
                PaymentAdded = dataService.Add<Intuit.Ipp.Data.Payment>(addedPayment);
                #endregion
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (PaymentAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = PaymentAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = PaymentAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

        /// <summary>
        /// updating payment entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="id"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOPayment(OnlineDataProcessingsImportClass.Payment coll, string id, string syncToken, Intuit.Ipp.Data.Payment PreviousData = null)
        {

             int linecount = 1;
            DataService dataService = null;
            Intuit.Ipp.Data.Payment PaymentAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.Payment addedPayment = new Intuit.Ipp.Data.Payment();
            Payment payment = new Payment();
            payment = coll;
            Intuit.Ipp.Data.PaymentLineDetail PaymentLineDetail = new Intuit.Ipp.Data.PaymentLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.LinkedTxn linkedtxn = new Intuit.Ipp.Data.LinkedTxn();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            bool flag = false;
            try
            {
                #region Add Payment

                //594
                if (payment.isAppend == true)
                {
                   

                    addedPayment = PreviousData;
                    addedPayment.sparse = true;
                    addedPayment.sparseSpecified = true;
                    if (addedPayment.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedPayment.Line.Length;
                        lines_online = addedPayment.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                }

                addedPayment.Id = id;
                addedPayment.SyncToken = syncToken;

                if (payment.DocNumber != null)
                {
                    addedPayment.DocNumber = payment.DocNumber;
                }

                #region TxnDate,privatenote
                if (payment.TxnDate != null)
                {
                    try
                    {
                        addedPayment.TxnDate = Convert.ToDateTime(payment.TxnDate);
                        addedPayment.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (payment.PrivateNote != null)
                {
                    addedPayment.PrivateNote = payment.PrivateNote;
                }
                if (payment.TxnStatus != null)
                {
                    addedPayment.TxnStatus = "PAID";

                }
                #endregion

                #region find customer
                Customer customer = new Customer();
                string customerName = string.Empty;
                foreach (var cust in payment.customerRef)
                {
                    customerName = cust.Name;
                }
                string customervalue = string.Empty;
                if (customerName != string.Empty)
                {
                    var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerName.Trim());
                    foreach (var customer1 in dataCustomer)
                    {
                        customervalue = customer1.Id;
                    }
                    addedPayment.CustomerRef = new ReferenceType()
                    {
                        name = customerName,
                        Value = customervalue
                    };
                }
                #endregion

                #region find ARAccountRef
                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                string accountName = string.Empty;
                string accountNumber = string.Empty;
                foreach (var acc in payment.ARAccountRef)
                {
                    accountName = acc.Name;
                    
                }
                if (accountName != string.Empty && accountName != null && accountName != "")
                {
                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                    addedPayment.ARAccountRef = new ReferenceType()
                    {
                        name = accDetails.name,
                        Value = accDetails.Value
                    };
                }
              

                #endregion

                #region find DepositeToAccountRef
                QueryService<Account> DepositeToAccountRefQueryService = new QueryService<Account>(serviceContext);

                accountName = string.Empty;
                accountNumber = string.Empty;
                foreach (var acc in payment.DepositToAccountRef)
                {
                    accountName = acc.Name;
                    
                }
                if (accountName != string.Empty && accountName != null && accountName != "")
                {
                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                    addedPayment.DepositToAccountRef = new ReferenceType()
                    {
                        name = accDetails.name,
                        Value = accDetails.Value
                    };
                }
               

                #endregion

                #region find PaymentMethodRef
                if (payment.PaymentMethodRef != null)
                {
                    foreach (var paymentMethod in payment.PaymentMethodRef)
                    {
                        string classValue = string.Empty;
                        if (paymentMethod.Name != null)
                        {
                            var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                            foreach (var data in dataPaymentMethod)
                            {
                                classValue = data.Id;
                            }
                            addedPayment.PaymentMethodRef = new ReferenceType()
                            {
                                name = paymentMethod.Name,
                                Value = classValue
                            };
                        }
                    }
                }

                #endregion
                #region find PaymentRefNum
                if (payment.PaymentRefNum != null)
                {
                    addedPayment.PaymentRefNum = payment.PaymentRefNum;
                }
                #endregion
                #region find TotalPayment  ExchangeRate CurrencyRef
                if (payment.TotalPayment != null)
                {
                    addedPayment.TotalAmt = Convert.ToDecimal(payment.TotalPayment);
                    addedPayment.TotalAmtSpecified = true;
                }
                if (payment.ExchangeRate != null)
                {
                    addedPayment.ExchangeRate = Convert.ToDecimal(payment.ExchangeRate);
                    addedPayment.ExchangeRateSpecified = true;
                }

                if (payment.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in payment.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (payment.CurrencyRef.Count != 0)
                    {
                        addedPayment.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #endregion

                #region find applytobill
                string applytobill = string.Empty;
               
                foreach (var l in payment.Line)
                {
                    #region
                    if (flag == false)
                    {
                        if (l.LineAmount != null)
                        {
                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                            Line.AmountSpecified = true;
                        }

                        foreach (var link in l.LinkedTxn)
                        {
                            applytobill = link.Applytobillref;
                        }
                        string bankaccountvalue = string.Empty;
                        if (applytobill != string.Empty && addedPayment.CustomerRef != null && addedPayment.CustomerRef.Value != string.Empty)
                        {
                            var bankdataAccount = await CommonUtilities.GetInvoiceExistsInOnlineQBO(applytobill.Trim(), addedPayment.CustomerRef.Value);
                            foreach (var dAccount in bankdataAccount)
                            {
                                bankaccountvalue = dAccount.Id;
                            }
                            linkedtxn.TxnId = bankaccountvalue;
                            linkedtxn.TxnType = "Invoice";
                            Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };
                            lines_online[linecount - 1] = Line;
                        }
                        flag = true;
                        addedPayment.Line = AddLine(lines_online, Line, linecount);
                    }
                    else if (flag == true)
                    {
                        Line = new Intuit.Ipp.Data.Line();
                        linkedtxn = new LinkedTxn();
                        linecount++;

                        if (l.LineAmount != null)
                        {
                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                            Line.AmountSpecified = true;
                        }

                        foreach (var link in l.LinkedTxn)
                        {
                            applytobill = link.Applytobillref;
                        }
                        string bankaccountvalue = string.Empty;
                        if (applytobill != string.Empty && addedPayment.CustomerRef != null && addedPayment.CustomerRef.Value != string.Empty)
                        {
                            var bankdataAccount = await CommonUtilities.GetInvoiceExistsInOnlineQBO(applytobill.Trim(), addedPayment.CustomerRef.Value);
                            foreach (var dAccount in bankdataAccount)
                            {
                                bankaccountvalue = dAccount.Id;
                            }

                            linkedtxn.TxnId = bankaccountvalue;
                            linkedtxn.TxnType = "Invoice";

                            Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };
                            Array.Resize(ref lines_online, linecount);
                            lines_online[linecount - 1] = Line;
                        }
                        addedPayment.Line = AddLine(lines_online, Line, linecount);
                    }

                    #endregion
                }
            
                #endregion

                #region UnappliedAmt ProcessPayment

                if (payment.UnappliedAmt != null)
                {
                    addedPayment.UnappliedAmt = 0;
                    addedPayment.UnappliedAmtSpecified = true;
                }
                if (payment.ProcessPayment != null)
                {
                    addedPayment.ProcessPayment = true;
                    addedPayment.ProcessPaymentSpecified = true;
                }
                #endregion
               
                #endregion

                PaymentAdded = new Intuit.Ipp.Data.Payment();
                PaymentAdded = dataService.Update<Intuit.Ipp.Data.Payment>(addedPayment);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (PaymentAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = PaymentAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = PaymentAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }   

        #endregion

    }
    /// <summary>
    /// checking doc number is present or not in quickbook online for payment.
    /// </summary>
    public class OnlinePaymentQBEntryCollection : Collection<Payment>
    {
        public Payment FindPamentNumberEntry(string docNumber)
        {
            foreach (Payment item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }

    }


}
