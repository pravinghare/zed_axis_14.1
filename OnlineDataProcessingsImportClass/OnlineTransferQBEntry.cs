﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Core;
using Intuit.Ipp.Security;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Data;
using System.Collections.ObjectModel;
using System.Net;
using System.Threading.Tasks;

namespace DataProcessingBlocks.OnlineDataProcessingsImportClass
{
    public class OnlineTransferQBEntry
    {

        #region member
        private string m_Id;
        //private string m_SyncTocken;
        private string m_TxnDate;
        private string m_PrivateNote;
        //private string m_FromAccountRef;
        //private string m_ToAccountRef;
        private string m_Amount;
        private string m_TransactionLocationType;

        private Collection<OnlineEntities.FromAccountRef> m_FromAccountRef = new Collection<OnlineEntities.FromAccountRef>();
        private Collection<OnlineEntities.ToAccountRef> m_ToAccountRef = new Collection<OnlineEntities.ToAccountRef>();

        private string m_name;

        //594
        private bool m_isAppend;


        #endregion

        #region Constructor
        public OnlineTransferQBEntry()
        {
        }
        #endregion

        #region properties

        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        //public string SyncToken
        //{
        //    get { return m_SyncTocken; }
        //    set { m_SyncTocken = value; }
        //}
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }
        //public string FromAccountRef
        //{
        //    get { return m_FromAccountRef; }
        //    set { m_FromAccountRef = value; }
        //}
        public Collection<OnlineEntities.FromAccountRef> FromAccountRef
        {
            get { return m_FromAccountRef; }
            set { m_FromAccountRef = value; }
        }

        //public string ToAccountRef
        //{
        //    get { return m_ToAccountRef; }
        //    set { m_ToAccountRef = value; }
        //}
        public Collection<OnlineEntities.ToAccountRef> ToAccountRef
        {
            get { return m_ToAccountRef; }
            set { m_ToAccountRef = value; }
        }

        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }



        #endregion

        //#region  Public Methods
        // static method for resize array for Line tag  or taxline tag.
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }


        ///create new transaction record .
        public async System.Threading.Tasks.Task<bool> CreateQBOTransfer(OnlineDataProcessingsImportClass.OnlineTransferQBEntry coll)
        {
            bool flag = false;
            int linecount = 1;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Transfer addedTransfer = new Transfer();
            Transfer TransferAdded = new Transfer();
            
             OnlineTransferQBEntry transfer = new OnlineTransferQBEntry();
            transfer = coll;
            #region 
            try
            {
                if (transfer.TxnDate != null)
                {
                    try
                    {
                        addedTransfer.TxnDate = Convert.ToDateTime(transfer.TxnDate);
                        addedTransfer.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (transfer.PrivateNote != null)
                {
                    addedTransfer.PrivateNote = transfer.PrivateNote;
                }
                #region transfer from Account
                foreach (var accountData in transfer.FromAccountRef)
                {
                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                        addedTransfer.FromAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                }
                #endregion
                #region transfer to Account
                foreach (var toaccountData in transfer.ToAccountRef)
                {
                    if (toaccountData.Name != null && toaccountData.Name != string.Empty && toaccountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(toaccountData.Name);
                        addedTransfer.ToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }

                }
                #endregion
                if (transfer.Amount != null)
                {
                    addedTransfer.Amount = Convert.ToDecimal(transfer.Amount);
                    addedTransfer.AmountSpecified = true;
                }
                if (transfer.TransactionLocationType != null)
                {
                    addedTransfer.TransactionLocationType = transfer.TransactionLocationType;
                }
               #endregion
                //#endregion
                TransferAdded = new Transfer();
                TransferAdded = dataService.Add(addedTransfer);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (TransferAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = TransferAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = TransferAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        public async System.Threading.Tasks.Task<bool> UpdateQBOTransfer(OnlineDataProcessingsImportClass.OnlineTransferQBEntry coll, string Id, string syncToken, Intuit.Ipp.Data.Transfer PreviousData = null)
        {
           
            DataService dataService = null;
            Transfer TransferAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Transfer addedTransfer = new Transfer();
            OnlineTransferQBEntry transfer = new OnlineTransferQBEntry();
            transfer = coll;

            try
            {
                #region Add Transfer
                //594
                if (transfer.isAppend == true)
                {
                    addedTransfer = PreviousData;
                    addedTransfer.sparse = true;
                    addedTransfer.sparseSpecified = true;
                }
                addedTransfer.Id = Id;
                addedTransfer.SyncToken = syncToken;

                #endregion

                #region update transfer entry

                //if (transfer.Id != null)
                //{
                //    addedTransfer.Id = transfer.Id;
                //}

                if (transfer.TxnDate != null)
                {
                    try
                    {
                        addedTransfer.TxnDate = Convert.ToDateTime(transfer.TxnDate);
                        addedTransfer.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (transfer.PrivateNote != null)
                {
                    addedTransfer.PrivateNote = transfer.PrivateNote;
                }

                //if (transfer.PrivateNote != null)
                //{
                //    addedTransfer.PrivateNote = transfer.PrivateNote;
                //}

                //if (transfer.SyncToken != null)
                //{
                //    addedTransfer.SyncToken = transfer.SyncToken;
                //}

                //QueryService<Account> AccountQueryService = new QueryService<Account>(serviceContext);
                //ReadOnlyCollection<Intuit.Ipp.Data.Account> dataAcc = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(AccountQueryService.ExecuteIdsQuery("Select * From Account where FullyQualifiedName ='" + transfer.FromAccountRef + "'"));
                //if (dataAcc.Count > 0)
                //{

                //    foreach (var data in dataAcc)
                //    {
                //        addedTransfer.Id = data.Id;
                //        addedTransfer.SyncToken = data.SyncToken;
                //    }
                //}
                //if (transfer.FromAccountRef != null)
                //{
                //    addedTransfer.FromAccountRef.name = Convert.ToString(transfer.FromAccountRef);
                //}

                //if (transfer.FromAccountRef != null)
                //{
                //    addedTransfer.FromAccountRef.name = Convert.ToString(transfer.FromAccountRef);
                //}
                //Intuit.Ipp.Data.Account frmacc = new Intuit.Ipp.Data.Account();
                //QueryService<Intuit.Ipp.Data.Account> trnsfraccountQueryService = new QueryService<Intuit.Ipp.Data.Account>(serviceContext);

                ////string accountName = string.Empty;
                ////string bankaccountNumber = string.Empty;
                //foreach (var account in frmacc.Name)
                //{
                //    addedTransfer.FromAccountRef.name = Convert.ToString(transfer.FromAccountRef);
                //}

                #region transfer from Account
                foreach (var accountData in transfer.FromAccountRef)
                {
                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                        addedTransfer.FromAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }

                }
                #endregion
                //if (transfer.FromAccountRef != null)
                //{
                //    addedTransfer.FromAccountRef.name = Convert.ToString(transfer.FromAccountRef);
                //}

                //if (transfer.ToAccountRef != null)
                //{
                //    addedTransfer.ToAccountRef.name = transfer.ToAccountRef;
                //}
                #region transfer to Account
                foreach (var toaccountData in transfer.ToAccountRef)
                {
                    if (toaccountData.Name != null && toaccountData.Name != string.Empty && toaccountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(toaccountData.Name);
                        addedTransfer.ToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }

                }
                #endregion

                if (transfer.Amount != null)
                {
                    addedTransfer.Amount = Convert.ToDecimal(transfer.Amount);
                    addedTransfer.AmountSpecified = true;
                }

                if (transfer.TransactionLocationType != null)
                {
                    addedTransfer.TransactionLocationType = transfer.TransactionLocationType;
                }

                #endregion

                TransferAdded = new Intuit.Ipp.Data.Transfer();
                
                TransferAdded = dataService.Update<Intuit.Ipp.Data.Transfer>(addedTransfer);
                
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (TransferAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = TransferAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = TransferAdded.SyncToken;
                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }


    }

    public class OnlineTransferQBEntryCollection : Collection<OnlineTransferQBEntry>
    {
        public OnlineTransferQBEntry FindTransferEntry(string TxnDate)
        {
            foreach (OnlineTransferQBEntry item in this)
            {
                if (item.TxnDate == TxnDate)
                {
                    return item;
                }
            }
            return null;
        }

    }
}
