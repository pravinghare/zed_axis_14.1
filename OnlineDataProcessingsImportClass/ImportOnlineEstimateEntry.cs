﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.ComponentModel;
using DataProcessingBlocks;
using System.Globalization;
using OnlineEntities;
using EDI.Constant;

namespace OnlineDataProcessingsImportClass
{
  public class ImportOnlineEstimateEntry
    {
      private static ImportOnlineEstimateEntry m_ImportOnlineEstimateEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineEstimateEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Estimate class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineEstimateEntry GetInstance()
        {
            if (m_ImportOnlineEstimateEntry == null)
                m_ImportOnlineEstimateEntry = new ImportOnlineEstimateEntry();
            return m_ImportOnlineEstimateEntry;
        }

        /// setting values to Estimate data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlineEstimateQBEntryCollection ImportOnlineEstimateData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineEstimateQBEntryCollection coll = new OnlineEstimateQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    string datevalue = string.Empty;
                   
                    //Employee Validation
                    OnlineDataProcessingsImportClass.OnlineEstimateQBEntry estimate = new OnlineDataProcessingsImportClass.OnlineEstimateQBEntry();
                    if (dt.Columns.Contains("DocNumber"))
                    {
                        estimate = coll.FindEstimateNumberEntry(dr["DocNumber"].ToString());

                        if (estimate == null)
                        {

                            #region if estimate is null

                            estimate = new OnlineDataProcessingsImportClass.OnlineEstimateQBEntry();

                            if (dt.Columns.Contains("DocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["DocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["DocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.DocNumber = dr["DocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of PrivateNote
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    estimate.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    estimate.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                estimate.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            estimate.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        estimate.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();

                            if (dt.Columns.Contains("DepartmentRef"))
                            {
                                #region Validations of DepartmentRef
                                if (dr["DepartmentRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepartmentRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (DepartmentRef.Name != null)
                                estimate.DepartmentRef.Add(DepartmentRef);


                            OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                                estimate.CurrencyRef.Add(CurrencyRef);

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            #region Custom1
                            OnlineEntities.CustomField CustomField = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName1"))
                            {
                                #region Validations of Custom1
                                if (dr["CustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName1 (" + dr["CustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField.Name = dr["CustomFieldName1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField.Name = dr["CustomFieldName1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField.Name = dr["CustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Name = dr["CustomFieldName1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue1"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue1 (" + dr["CustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField.Value = dr["CustomFieldValue1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField.Value = dr["CustomFieldValue1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField.Value = dr["CustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Value = dr["CustomFieldValue1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomField.Value != null || CustomField.Name != null)

                                estimate.CustomField.Add(CustomField);

                            #endregion

                            #region Custom2
                            OnlineEntities.CustomField CustomField2 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName2"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName2 (" + dr["CustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField2.Name = dr["CustomFieldName2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField2.Name = dr["CustomFieldName2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField2.Name = dr["CustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Name = dr["CustomFieldName2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue2"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue2 (" + dr["CustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField2.Value = dr["CustomFieldValue2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomField2.Value != null || CustomField2.Name != null)

                                estimate.CustomField.Add(CustomField2);

                            #endregion

                            #region Custom3
                            OnlineEntities.CustomField CustomField3 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName3"))
                            {
                                #region Validations of Custom3
                                if (dr["CustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName3 (" + dr["CustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField3.Name = dr["CustomFieldName3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField3.Name = dr["CustomFieldName3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField3.Name = dr["CustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Name = dr["CustomFieldName3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue3"))
                            {
                                #region Validations of Custom3
                                if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue3 (" + dr["CustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField3.Value = dr["CustomFieldValue3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomField3.Value != null || CustomField3.Name != null)

                                estimate.CustomField.Add(CustomField3);

                            #endregion

                            if (dt.Columns.Contains("TxnStatus"))
                            {
                                #region Validations of TxnStatus
                                if (dr["TxnStatus"].ToString() != string.Empty)
                                {
                                    if (dr["TxnStatus"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnStatus (" + dr["TxnStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.TxnStatus = dr["TxnStatus"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.TxnStatus = dr["TxnStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.TxnStatus = dr["TxnStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.TxnStatus = dr["TxnStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.SalesItemLineDetail SalesItemLineDetail = new OnlineEntities.SalesItemLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();
                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineSalesItemRefName"))
                            {
                                #region Validations of LineSalesItemRefName
                                if (dr["LineSalesItemRefName"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemRefName (" + dr["LineSalesItemRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineSalesItemRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.ClassRef classref = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("LineSalesItemClassRef"))
                            {
                                #region Validations of LineSalesItemClassRef
                                if (dr["LineSalesItemClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemClassRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemClassRef (" + dr["LineSalesItemClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                classref.Name = dr["LineSalesItemClassRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                classref.Name = dr["LineSalesItemClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            classref.Name = dr["LineSalesItemClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        classref.Name = dr["LineSalesItemClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }                           
                            if (dt.Columns.Contains("LineSalesItemUnitPrice"))
                            {
                                #region Validations of LineSalesItemUnitPrice
                                if (dr["LineSalesItemUnitPrice"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemUnitPrice"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemUnitPrice (" + dr["LineSalesItemUnitPrice"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineSalesItemQty"))
                            {
                                #region Validations for LineSalesItemQty
                                if (dr["LineSalesItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineSalesItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemQty( " + dr["LineSalesItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesItemLineDetail.LineQty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineSalesItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineSalesItemTaxCodeRefValue"))
                            {
                                #region Validations of LineSalesItemTaxCodeRefValue
                                if (dr["LineSalesItemTaxCodeRefValue"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemTaxCodeRefValue"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemTaxCodeRefValue (" + dr["LineSalesItemTaxCodeRefValue"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineSalesItemServiceDate"))
                            {
                                #region Validations of LineSalesItemServiceDate
                                DateTime SODate = new DateTime();
                                if (dr["LineSalesItemServiceDate"].ToString() != "<None>" || dr["LineSalesItemServiceDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["LineSalesItemServiceDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LineSalesItemServiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    SalesItemLineDetail.ServiceDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    SalesItemLineDetail.ServiceDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                SalesItemLineDetail.ServiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            SalesItemLineDetail.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        SalesItemLineDetail.ServiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (classref.Name != null)
                            {
                                SalesItemLineDetail.ClassRef.Add(classref);
                            }
                            if (ItemRef.Name != null || ItemRef.SKU != null)
                            {
                                SalesItemLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name != null)
                            {
                                SalesItemLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (SalesItemLineDetail.ItemRef.Count != 0 || SalesItemLineDetail.LineQty != null || SalesItemLineDetail.LineUnitPrice != null || SalesItemLineDetail.ItemRef.Count != 0 || SalesItemLineDetail.ServiceDate != null)
                            {
                                Line.SalesItemLineDetail.Add(SalesItemLineDetail);
                            }

                            OnlineEntities.DiscountLineDetail DiscountLineDetail = new OnlineEntities.DiscountLineDetail();
                            OnlineEntities.Line DiscountLine = new OnlineEntities.Line();

                            if (dt.Columns.Contains("LineDiscountAmount"))
                            {
                                #region Validations for LineDiscountAmount
                                if (dr["LineDiscountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineDiscountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountAmount( " + dr["LineDiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountLineDetail.LineDiscountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineDiscountPercentBased"))
                            {
                                #region Validations of IsActive
                                if (dr["LineDiscountPercentBased"].ToString() != "<None>" || dr["LineDiscountPercentBased"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["LineDiscountPercentBased"].ToString(), out result))
                                    {
                                        DiscountLineDetail.PercentBased = Convert.ToInt32(dr["LineDiscountPercentBased"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["LineDiscountPercentBased"].ToString().ToLower() == "true")
                                        {
                                            DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["LineDiscountPercentBased"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "LineDiscountPercentBased";
                                            }
                                            else
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LineDiscountPercentBased(" + dr["LineDiscountPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineDiscountPercent"))
                            {
                                #region Validations for LineDiscountPercent
                                if (dr["LineDiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineDiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountPercent( " + dr["LineDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {

                                                DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();

                                            }
                                            else
                                            {
                                                DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountLineDetail.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountPercent"].ToString()));
                                        }
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.DiscountAccountRef DiscountAccountRef = new OnlineEntities.DiscountAccountRef();


                            if (dt.Columns.Contains("LineDiscountAccountRefName"))
                            {
                                #region Validations of LineDiscountAccountRefName
                                if (dr["LineDiscountAccountRefName"].ToString() != string.Empty)
                                {
                                    if (dr["LineDiscountAccountRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountAccountRefName (" + dr["LineDiscountAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (DiscountAccountRef.Name != null)
                            {
                                DiscountLineDetail.DiscountAccountRef.Add(DiscountAccountRef);
                            }
                            ////Improvement::493
                            //if (dt.Columns.Contains("LineDiscountAcctNum"))
                            //{
                            //    #region Validations of LineDiscountAccountRefName
                            //    if (dr["LineDiscountAcctNum"].ToString() != string.Empty)
                            //    {
                            //        if (dr["LineDiscountAcctNum"].ToString().Length > 15)
                            //        {
                            //            if (isIgnoreAll == false)
                            //            {
                            //                string strMessages = "This LineDiscountAcctNum (" + dr["LineDiscountAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            //                if (Convert.ToString(result) == "Cancel")
                            //                {
                            //                    continue;
                            //                }
                            //                if (Convert.ToString(result) == "No")
                            //                {
                            //                    return null;
                            //                }
                            //                if (Convert.ToString(result) == "Ignore")
                            //                {
                            //                    DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString().Substring(0, 15);
                            //                }
                            //                if (Convert.ToString(result) == "Abort")
                            //                {
                            //                    isIgnoreAll = true;
                            //                    DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString();
                            //                }
                            //            }
                            //            else
                            //            {
                            //                DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString();
                            //        }
                            //    }
                            //    #endregion
                            //}

                            //if (DiscountAccountRef.AcctNum != null)
                            //{
                            //    DiscountLineDetail.DiscountAccountRef.Add(DiscountAccountRef);
                            //}

                            if (DiscountLineDetail.DiscountAccountRef.Count != 0 || DiscountLineDetail.DiscountPercent != null || DiscountLineDetail.LineDiscountAmount != null || DiscountLineDetail.PercentBased != null)
                            {
                                DiscountLine.DiscountLineDetail.Add(DiscountLineDetail);
                            }
                            if (DiscountLine.DiscountLineDetail.Count > 0 && Line.SalesItemLineDetail.Count > 0)
                            {
                                estimate.Line.Add(DiscountLine);
                                estimate.Line.Add(Line);
                            }
                            else if (DiscountLine.DiscountLineDetail.Count > 0 && Line.SalesItemLineDetail.Count == 0)
                            {
                                estimate.Line.Add(DiscountLine);
                            }
                            else if (DiscountLine.DiscountLineDetail.Count == 0 && Line.SalesItemLineDetail.Count > 0)
                            {
                                estimate.Line.Add(Line);
                            }
                            else if (Line.SalesItemLineDetail.Count == 0)
                            {
                                estimate.Line.Add(Line);
                            }
                                                       
                            OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                            OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                            OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                            if (dt.Columns.Contains("TxnTaxCodeRefName"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TotalTax"))
                            {
                                #region Validations for TotalTax
                                if (dr["TotalTax"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TotalTax"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TotalTax( " + dr["TotalTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {

                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                            }
                                            if (Convert.ToString(result) == "TotalTax")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();

                                            }
                                            else
                                            {
                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxDetail.TotalTax = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalTax"].ToString()));
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxLineAmount"))
                            {
                                #region Validations for TaxLineAmount
                                if (dr["TaxLineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TaxLineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxLineAmount( " + dr["TaxLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "TaxLineAmount")
                                            {
                                                isIgnoreAll = true;
                                                TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLine.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxLineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("TaxPercentBased"))
                            {
                                #region Validations of IsActive
                                if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                    {
                                        TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                        {
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "TaxPercentBased";
                                            }
                                            else
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxPercent"))
                            {
                                #region Validations for TaxPercent
                                if (dr["TaxPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {

                                                TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "TaxPercent")
                                            {
                                                isIgnoreAll = true;
                                                TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef.Count > 0)
                            {
                                TaxLine.TaxLineDetail.Add(TaxLineDetail);
                            }
                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }
                            if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                            {
                                TxnTaxDetail.TaxLine.Add(TaxLine);
                            }
                            if (TxnTaxDetail.TaxLine.Count != 0 || TxnTaxCodeRef.Name != null)
                            {
                                estimate.TxnTaxDetail.Add(TxnTaxDetail);
                            }

                            //bug no.338
                            #region Shipping

                            OnlineEntities.SalesItemLineDetail SalesItemLineDetail1 = new OnlineEntities.SalesItemLineDetail();
                            OnlineEntities.Line Line1 = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef1 = new OnlineEntities.ItemRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("Shipping"))
                            {
                                #region Validations for LineAmount
                                if (dr["Shipping"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Shipping"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Shipping( " + dr["Shipping"].ToString() + " ) is not valid for quickbooks. If you press cancel this record will not be added to QuickBooks. If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line1.LineAmount = dr["Shipping"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line1.LineAmount = dr["Shipping"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line1.LineAmount = dr["Shipping"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line1.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Shipping"].ToString()));
                                    }
                                }

                                #endregion

                                ItemRef1.Name = "SHIPPING_ITEM_ID";
                            }

                            if (dt.Columns.Contains("ShippingTaxCode"))
                            {
                                #region Validations of ShippingTaxCode
                                if (dr["ShippingTaxCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShippingTaxCode"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShippingTaxCode (" + dr["ShippingTaxCode"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ItemRef1.Name != null)
                            {
                                SalesItemLineDetail1.ItemRef.Add(ItemRef1);
                            }
                            if (TaxCodeRef1.Name != null)
                            {
                                SalesItemLineDetail1.TaxCodeRef.Add(TaxCodeRef1);
                            }
                            if (SalesItemLineDetail1.ItemRef.Count != 0 || SalesItemLineDetail1.TaxCodeRef.Count != 0)
                            {
                                Line1.SalesItemLineDetail.Add(SalesItemLineDetail1);
                            }
                            if (Line1.SalesItemLineDetail.Count != 0)
                            {
                                estimate.Line.Add(Line1);
                            }

                            #endregion
                            //end bug no.338

                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();

                            if (dt.Columns.Contains("CustomerFullyQualifiedName"))
                            {
                                #region Validations of CustomerFullyQualifiedName
                                if (dr["CustomerFullyQualifiedName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerFullyQualifiedName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerFullyQualifiedName (" + dr["CustomerFullyQualifiedName"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomerRef.Name != null)
                            {
                                estimate.CustomerRef.Add(CustomerRef);
                            }

                            if (dt.Columns.Contains("CustomerMemo"))
                            {
                                #region Validations of CustomerMemo
                                if (dr["CustomerMemo"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerMemo"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerMemo (" + dr["CustomerMemo"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.CustomerMemo = dr["CustomerMemo"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.CustomerMemo = dr["CustomerMemo"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.CustomerMemo = dr["CustomerMemo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.CustomerMemo = dr["CustomerMemo"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //adding Bill Address 

                            OnlineEntities.BillAddr billAddr = new OnlineEntities.BillAddr();
                            if (dt.Columns.Contains("BillAddrLine1"))
                            {
                                #region Validations of BillAddrLine1
                                if (dr["BillAddrLine1"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrLine1"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrLine1 (" + dr["BillAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillLine1 = dr["BillAddrLine1"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillLine1 = dr["BillAddrLine1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillLine1 = dr["BillAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine1 = dr["BillAddrLine1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrLine2"))
                            {
                                #region Validations of BillAddrLine2
                                if (dr["BillAddrLine2"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrLine2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrLine2 (" + dr["BillAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillLine2 = dr["BillAddrLine2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillLine2 = dr["BillAddrLine2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillLine2 = dr["BillAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine2 = dr["BillAddrLine2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrLine3"))
                            {
                                #region Validations of BillAddrLine3
                                if (dr["BillAddrLine3"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrLine3"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrLine3 (" + dr["BillAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillLine3 = dr["BillAddrLine3"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillLine3 = dr["BillAddrLine3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillLine3 = dr["BillAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine3 = dr["BillAddrLine3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrCity"))
                            {
                                #region Validations of BillAddrCity
                                if (dr["BillAddrCity"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrCity"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrCity (" + dr["BillAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillCity = dr["BillAddrCity"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillCity = dr["BillAddrCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillCity = dr["BillAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillCity = dr["BillAddrCity"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrCountry"))
                            {
                                #region Validations of BillAddrCountry
                                if (dr["BillAddrCountry"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrCountry"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrCountry (" + dr["BillAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillCountry = dr["BillAddrCountry"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillCountry = dr["BillAddrCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillCountry = dr["BillAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillCountry = dr["BillAddrCountry"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrSubDivisionCode"))
                            {
                                #region Validations of BillAddrSubDivisionCode
                                if (dr["BillAddrSubDivisionCode"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrSubDivisionCode"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrSubDivisionCode (" + dr["BillAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrPostalCode"))
                            {
                                #region Validations of BillAddrPostalCode
                                if (dr["BillAddrPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrPostalCode"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrPostalCode (" + dr["BillAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrNote"))
                            {
                                #region Validations of BillAddrNote
                                if (dr["BillAddrNote"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrNote"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrNote (" + dr["BillAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillNote = dr["BillAddrNote"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillNote = dr["BillAddrNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillNote = dr["BillAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillNote = dr["BillAddrNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrLine4"))
                            {
                                #region Validations of BillAddrLine4
                                if (dr["BillAddrLine4"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrLine4"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrLine4 (" + dr["BillAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillLine4 = dr["BillAddrLine4"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillLine4 = dr["BillAddrLine4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillLine4 = dr["BillAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine4 = dr["BillAddrLine4"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrLine5"))
                            {
                                #region Validations of BillAddrLine5
                                if (dr["BillAddrLine5"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrLine5"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrLine5 (" + dr["BillAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillLine5 = dr["BillAddrLine5"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillLine5 = dr["BillAddrLine5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillLine5 = dr["BillAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine5 = dr["BillAddrLine5"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("BillAddrLat"))
                            {
                                #region Validations of BillAddrLat
                                if (dr["BillAddrLat"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrLat"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrLat (" + dr["BillAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillAddrLat = dr["BillAddrLat"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillAddrLong"))
                            {
                                #region Validations of BillAddrLong
                                if (dr["BillAddrLong"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddrLong"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillAddrLong (" + dr["BillAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billAddr.BillAddrLong = dr["BillAddrLong"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (billAddr.BillLine1 != null || billAddr.BillLine2 != null || billAddr.BillLine3 != null || billAddr.BillLine4 != null || billAddr.BillLine5 != null || billAddr.BillCity != null || billAddr.BillCountry != null || billAddr.BillCountrySubDivisionCode != null || billAddr.BillNote != null || billAddr.BillAddrLat != null || billAddr.BillAddrLong != null || billAddr.BillPostalCode != null)
                                estimate.BillAddr.Add(billAddr);

                            OnlineEntities.ShipAddr shipAddr = new OnlineEntities.ShipAddr();
                            if (dt.Columns.Contains("ShipAddrLine1"))
                            {
                                #region Validations of ShipAddrLine1
                                if (dr["ShipAddrLine1"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine1"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine1 (" + dr["ShipAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine2"))
                            {
                                #region Validations of ShipAddrLine2
                                if (dr["ShipAddrLine2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine2 (" + dr["ShipAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine3"))
                            {
                                #region Validations of ShipAddrLine3
                                if (dr["ShipAddrLine3"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine3"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine3 (" + dr["ShipAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrCity"))
                            {
                                #region Validations of ShipAddrCity
                                if (dr["ShipAddrCity"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrCity"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrCity (" + dr["ShipAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipCity = dr["ShipAddrCity"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrCountry"))
                            {
                                #region Validations of ShipAddrCountry
                                if (dr["ShipAddrCountry"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrCountry"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrCountry (" + dr["ShipAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrSubDivisionCode"))
                            {
                                #region Validations of ShipAddrSubDivisionCode
                                if (dr["ShipAddrSubDivisionCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrSubDivisionCode"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrSubDivisionCode (" + dr["ShipAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrPostalCode"))
                            {
                                #region Validations of ShipAddrPostalCode
                                if (dr["ShipAddrPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrPostalCode"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrPostalCode (" + dr["ShipAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrNote"))
                            {
                                #region Validations of ShipAddrNote
                                if (dr["ShipAddrNote"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrNote"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrNote (" + dr["ShipAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipNote = dr["ShipAddrNote"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine4"))
                            {
                                #region Validations of ShipAddrLine4
                                if (dr["ShipAddrLine4"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine4"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine4 (" + dr["ShipAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine5"))
                            {
                                #region Validations of ShipAddrLine5
                                if (dr["ShipAddrLine5"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine5"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine5 (" + dr["ShipAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipAddrLat"))
                            {
                                #region Validations of ShipAddrLat
                                if (dr["ShipAddrLat"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLat"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLat (" + dr["ShipAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLong"))
                            {
                                #region Validations of ShipAddrLong
                                if (dr["ShipAddrLong"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLong"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLong (" + dr["ShipAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (shipAddr.ShipLine1 != null || shipAddr.ShipLine2 != null || shipAddr.ShipLine3 != null || shipAddr.ShipLine4 != null || shipAddr.ShipLine5 != null || shipAddr.ShipCity != null || shipAddr.ShipCountry != null || shipAddr.ShipCountrySubDivisionCode != null || shipAddr.ShipNote != null || shipAddr.ShipAddrLat != null || shipAddr.ShipAddrLong != null || shipAddr.ShipPostalCode != null)
                                estimate.ShipAddr.Add(shipAddr);


                            ClassRef ClassRef = new ClassRef();

                            if (dt.Columns.Contains("ClassRef"))
                            {
                                #region Validations of ClassRef
                                if (dr["ClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ClassRef (" + dr["ClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["ClassRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["ClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["ClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["ClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ClassRef.Name != null)
                            {
                                estimate.ClassRef.Add(ClassRef);
                            }
                            SalesTermRef SalesTermRef = new SalesTermRef();

                            if (dt.Columns.Contains("SalesTermRef"))
                            {
                                #region Validations of SalesTermRef
                                if (dr["SalesTermRef"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTermRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTermRef (" + dr["SalesTermRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesTermRef.Name = dr["SalesTermRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (SalesTermRef.Name != null)
                            {
                                estimate.SalesTermRef.Add(SalesTermRef);
                            }
                            ShipMethodRef ShipMethodRef = new ShipMethodRef();

                            if (dt.Columns.Contains("ShipMethodRef"))
                            {
                                #region Validations of ShipMethodRef
                                if (dr["ShipMethodRef"].ToString() != string.Empty)
                                {
                                    if (dr["ShipMethodRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipMethodRef (" + dr["ShipMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipMethodRef.Name = dr["ShipMethodRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ShipMethodRef.Name != null)
                            {
                                estimate.ShipMethodRef.Add(ShipMethodRef);
                            }

                            if (dt.Columns.Contains("ShipDate"))
                            {
                                DateTime SODate = new DateTime();
                                #region validations of ShipDate
                                if (dr["ShipDate"].ToString() != "<None>" || dr["ShipDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["ShipDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    estimate.ShipDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    estimate.ShipDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                estimate.ShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            estimate.ShipDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        estimate.ShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ApplyTaxAfterDiscount"))
                            {
                                #region Validations of IsActive
                                if (dr["ApplyTaxAfterDiscount"].ToString() != "<None>" || dr["ApplyTaxAfterDiscount"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["ApplyTaxAfterDiscount"].ToString(), out result))
                                    {
                                        estimate.ApplyTaxAfterDiscount = Convert.ToInt32(dr["ApplyTaxAfterDiscount"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["ApplyTaxAfterDiscount"].ToString().ToLower() == "true")
                                        {
                                            estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["ApplyTaxAfterDiscount"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "ApplyTaxAfterDiscount";
                                            }
                                            else
                                                estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ApplyTaxAfterDiscount(" + dr["ApplyTaxAfterDiscount"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }                           

                            if (dt.Columns.Contains("PrintStatus"))
                            {
                                #region Validations of PrintStatus
                                if (dr["PrintStatus"].ToString() != string.Empty)
                                {
                                    if (dr["PrintStatus"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrintStatus (" + dr["PrintStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.PrintStatus = dr["PrintStatus"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.PrintStatus = dr["PrintStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.PrintStatus = dr["PrintStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.PrintStatus = dr["PrintStatus"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("EmailStatus"))
                            {
                                #region Validations of EmailStatus
                                if (dr["EmailStatus"].ToString() != string.Empty)
                                {
                                    if (dr["EmailStatus"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This EmailStatus (" + dr["EmailStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.EmailStatus = dr["EmailStatus"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.EmailStatus = dr["EmailStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.EmailStatus = dr["EmailStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.EmailStatus = dr["EmailStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Telephone Number 606
                            //OnlineEntities.TelephoneNumber telephoneNo = new OnlineEntities.TelephoneNumber();
                            OnlineEntities.PhoneClass phno = new OnlineEntities.PhoneClass();
                            if (dt.Columns.Contains("PrimaryPhone"))
                            {
                                #region Validations of PrimaryPhone
                                if (dr["PrimaryPhone"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryPhone"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryPhone (" + dr["PrimaryPhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.PrimaryPhone = dr["PrimaryPhone"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                    }
                                }
                                #endregion
                            }
                            

                            if (dt.Columns.Contains("BillEmail"))
                            {
                                #region Validations of BillEmail
                                if (dr["BillEmail"].ToString() != string.Empty)
                                {
                                    //bug 464 Email length change 20 to 100
                                    if (dr["BillEmail"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillEmail (" + dr["BillEmail"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                //bug 464 Email length change 20 to 100
                                                estimate.BillEmail = dr["BillEmail"].ToString().Substring(0, 100);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.BillEmail = dr["BillEmail"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.BillEmail = dr["BillEmail"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.BillEmail = dr["BillEmail"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Expiration"))
                            {
                                DateTime SODate = new DateTime();
                                #region validations of Expiration
                                if (dr["Expiration"].ToString() != "<None>" || dr["Expiration"].ToString() != string.Empty)
                                {
                                    datevalue = dr["Expiration"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Expiration (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    estimate.Expiration = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    estimate.Expiration = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                estimate.Expiration = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            estimate.Expiration = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        estimate.Expiration = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AcceptedBy"))
                            {
                                #region Validations of AcceptedBy
                                if (dr["AcceptedBy"].ToString() != string.Empty)
                                {
                                    if (dr["AcceptedBy"].ToString().Length >100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AcceptedBy (" + dr["AcceptedBy"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.AcceptedBy = dr["AcceptedBy"].ToString().Substring(0, 100);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.AcceptedBy = dr["AcceptedBy"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.AcceptedBy = dr["AcceptedBy"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.AcceptedBy = dr["AcceptedBy"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AcceptedDate"))
                            {
                                DateTime SODate = new DateTime();
                                #region validations of AcceptedDate
                                if (dr["AcceptedDate"].ToString() != "<None>" || dr["AcceptedDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["AcceptedDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This AcceptedDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    estimate.AcceptedDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    estimate.AcceptedDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                estimate.AcceptedDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            estimate.AcceptedDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        estimate.AcceptedDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("GlobalTaxCalculation"))
                            {
                                #region Validations of GlobalTaxCalculation
                                if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                                {
                                    if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TransactionLocationType"))
                            {
                                #region Validations of TransactionLocationType
                                if (dr["TransactionLocationType"].ToString() != string.Empty)
                                {
                                    if (dr["TransactionLocationType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            #endregion

                            coll.Add(estimate);
                        }
                        else
                        {
                            #region  if estimate is not null
                            OnlineEntities.SalesItemLineDetail SalesItemLineDetail = new OnlineEntities.SalesItemLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();
                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineSalesItemRefName"))
                            {
                                #region Validations of LineSalesItemRefName
                                if (dr["LineSalesItemRefName"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemRefName (" + dr["LineSalesItemRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineSalesItemRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.ClassRef classref = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("LineSalesItemClassRef"))
                            {
                                #region Validations of LineSalesItemClassRef
                                if (dr["LineSalesItemClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemClassRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemClassRef (" + dr["LineSalesItemClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                classref.Name = dr["LineSalesItemClassRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                classref.Name = dr["LineSalesItemClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            classref.Name = dr["LineSalesItemClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        classref.Name = dr["LineSalesItemClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                          
                            if (dt.Columns.Contains("LineSalesItemUnitPrice"))
                            {
                                #region Validations of LineSalesItemUnitPrice
                                if (dr["LineSalesItemUnitPrice"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemUnitPrice"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemUnitPrice (" + dr["LineSalesItemUnitPrice"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineSalesItemQty"))
                            {
                                #region Validations for LineSalesItemQty
                                if (dr["LineSalesItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineSalesItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemQty( " + dr["LineSalesItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesItemLineDetail.LineQty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineSalesItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineSalesItemTaxCodeRefValue"))
                            {
                                #region Validations of LineSalesItemTaxCodeRefValue
                                if (dr["LineSalesItemTaxCodeRefValue"].ToString() != string.Empty)
                                {
                                    if (dr["LineSalesItemTaxCodeRefValue"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemTaxCodeRefValue (" + dr["LineSalesItemTaxCodeRefValue"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineSalesItemServiceDate"))
                            {
                                #region Validations of LineSalesItemServiceDate
                                DateTime SODate = new DateTime();
                                if (dr["LineSalesItemServiceDate"].ToString() != "<None>" || dr["LineSalesItemServiceDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["LineSalesItemServiceDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LineSalesItemServiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    SalesItemLineDetail.ServiceDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    SalesItemLineDetail.ServiceDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                SalesItemLineDetail.ServiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            SalesItemLineDetail.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        SalesItemLineDetail.ServiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (classref.Name != null)
                            {
                                SalesItemLineDetail.ClassRef.Add(classref);
                            }
                            if (ItemRef.Name != null || ItemRef.SKU != null)
                            {
                                SalesItemLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name != null)
                            {
                                SalesItemLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (SalesItemLineDetail.ItemRef.Count != 0 || SalesItemLineDetail.LineQty != null || SalesItemLineDetail.LineUnitPrice != null || SalesItemLineDetail.ItemRef.Count != 0 || SalesItemLineDetail.ServiceDate != null)
                            {
                                Line.SalesItemLineDetail.Add(SalesItemLineDetail);
                            }

                            if (Line.SalesItemLineDetail.Count != 0)
                            {
                                estimate.Line.Add(Line);
                            }

                            #region Shipping

                            OnlineEntities.SalesItemLineDetail SalesItemLineDetail1 = new OnlineEntities.SalesItemLineDetail();
                            OnlineEntities.Line Line1 = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef1 = new OnlineEntities.ItemRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("Shipping"))
                            {
                                #region Validations for LineAmount
                                if (dr["Shipping"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Shipping"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Shipping( " + dr["Shipping"].ToString() + " ) is not valid for quickbooks. If you press cancel this record will not be added to QuickBooks. If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line1.LineAmount = dr["Shipping"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line1.LineAmount = dr["Shipping"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line1.LineAmount = dr["Shipping"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line1.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Shipping"].ToString()));
                                    }
                                }

                                #endregion

                                ItemRef1.Name = "SHIPPING_ITEM_ID";
                            }

                            if (dt.Columns.Contains("ShippingTaxCode"))
                            {
                                #region Validations of ShippingTaxCode
                                if (dr["ShippingTaxCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShippingTaxCode"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShippingTaxCode (" + dr["ShippingTaxCode"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ItemRef1.Name != null)
                            {
                                SalesItemLineDetail1.ItemRef.Add(ItemRef1);
                            }
                            if (TaxCodeRef1.Name != null)
                            {
                                SalesItemLineDetail1.TaxCodeRef.Add(TaxCodeRef1);
                            }
                            if (SalesItemLineDetail1.ItemRef.Count != 0 || SalesItemLineDetail1.TaxCodeRef.Count != 0)
                            {
                                Line1.SalesItemLineDetail.Add(SalesItemLineDetail1);
                            }
                            if (Line1.SalesItemLineDetail.Count != 0)
                            {
                                estimate.Line.Add(Line1);
                            }

                            #endregion


                            OnlineEntities.PhoneClass phno = new OnlineEntities.PhoneClass();
                            if (dt.Columns.Contains("PrimaryPhone"))
                            {
                                #region Validations of PrimaryPhone
                                if (dr["PrimaryPhone"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryPhone"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryPhone (" + dr["PrimaryPhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.PrimaryPhone = dr["PrimaryPhone"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                    }
                                }
                                #endregion
                            }
                           

                            #endregion
                        }
                    }
                    else
                    {
                        estimate = new OnlineDataProcessingsImportClass.OnlineEstimateQBEntry();

                        #region if docNumber is not present

                        estimate = new OnlineDataProcessingsImportClass.OnlineEstimateQBEntry();                   

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of PrivateNote
                            DateTime SODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            estimate.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        estimate.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    estimate.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();

                        if (dt.Columns.Contains("DepartmentRef"))
                        {
                            #region Validations of DepartmentRef
                            if (dr["DepartmentRef"].ToString() != string.Empty)
                            {
                                if (dr["DepartmentRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                else
                                {
                                    DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (DepartmentRef.Name != null)
                            estimate.DepartmentRef.Add(DepartmentRef);
                        OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                        if (dt.Columns.Contains("CurrencyRef"))
                        {
                            #region Validations of CurrencyRef
                            if (dr["CurrencyRef"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CurrencyRef.Name != null)
                            estimate.CurrencyRef.Add(CurrencyRef);

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }


                        #region Custom1
                        OnlineEntities.CustomField CustomField = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName1"))
                        {
                            #region Validations of Custom1
                            if (dr["CustomFieldName1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName1 (" + dr["CustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField.Name = dr["CustomFieldName1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField.Name = dr["CustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Name = dr["CustomFieldName1"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField.Name = dr["CustomFieldName1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue1"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldValue1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue1 (" + dr["CustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField.Value = dr["CustomFieldValue1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField.Value = dr["CustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Value = dr["CustomFieldValue1"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField.Value = dr["CustomFieldValue1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomField.Value != null || CustomField.Name != null)

                            estimate.CustomField.Add(CustomField);

                        #endregion

                        #region Custom2
                        OnlineEntities.CustomField CustomField2 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName2"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldName2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName2 (" + dr["CustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField2.Name = dr["CustomFieldName2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField2.Name = dr["CustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Name = dr["CustomFieldName2"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField2.Name = dr["CustomFieldName2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue2"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldValue2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue2 (" + dr["CustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomField2.Value != null || CustomField2.Name != null)

                            estimate.CustomField.Add(CustomField2);

                        #endregion

                        #region Custom3
                        OnlineEntities.CustomField CustomField3 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName3"))
                        {
                            #region Validations of Custom3
                            if (dr["CustomFieldName3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName3 (" + dr["CustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField3.Name = dr["CustomFieldName3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField3.Name = dr["CustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Name = dr["CustomFieldName3"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField3.Name = dr["CustomFieldName3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue3"))
                        {
                            #region Validations of Custom3
                            if (dr["CustomFieldValue3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue3 (" + dr["CustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomField3.Value != null || CustomField3.Name != null)

                            estimate.CustomField.Add(CustomField3);

                        #endregion
                        if (dt.Columns.Contains("TxnStatus"))
                        {
                            #region Validations of TxnStatus
                            if (dr["TxnStatus"].ToString() != string.Empty)
                            {
                                if (dr["TxnStatus"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnStatus (" + dr["TxnStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.TxnStatus = dr["TxnStatus"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.TxnStatus = dr["TxnStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.TxnStatus = dr["TxnStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.TxnStatus = dr["TxnStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.SalesItemLineDetail SalesItemLineDetail = new OnlineEntities.SalesItemLineDetail();
                        OnlineEntities.Line Line = new OnlineEntities.Line();
                        OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                        OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();
                        if (dt.Columns.Contains("LineDescription"))
                        {
                            #region Validations of Associate
                            if (dr["LineDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineDescription"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineDescription = dr["LineDescription"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineAmount"))
                        {
                            #region Validations for LineAmount
                            if (dr["LineAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("LineSalesItemRefName"))
                        {
                            #region Validations of LineSalesItemRefName
                            if (dr["LineSalesItemRefName"].ToString() != string.Empty)
                            {
                                if (dr["LineSalesItemRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineSalesItemRefName (" + dr["LineSalesItemRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.Name = dr["LineSalesItemRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.Name = dr["LineSalesItemRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        //bug 486
                        if (dt.Columns.Contains("SKU"))
                        {
                            #region Validations of SKU
                            if (dr["SKU"].ToString() != string.Empty)
                            {
                                if (dr["SKU"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.SKU = dr["SKU"].ToString();
                                }
                            }
                            #endregion
                        }
                        OnlineEntities.ClassRef classref = new OnlineEntities.ClassRef();
                        if (dt.Columns.Contains("LineSalesItemClassRef"))
                        {
                            #region Validations of LineSalesItemClassRef
                            if (dr["LineSalesItemClassRef"].ToString() != string.Empty)
                            {
                                if (dr["LineSalesItemClassRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineSalesItemClassRef (" + dr["LineSalesItemClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            classref.Name = dr["LineSalesItemClassRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            classref.Name = dr["LineSalesItemClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        classref.Name = dr["LineSalesItemClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    classref.Name = dr["LineSalesItemClassRef"].ToString();
                                }
                            }
                            #endregion
                        }
                       
                        if (dt.Columns.Contains("LineSalesItemUnitPrice"))
                        {
                            #region Validations of LineSalesItemUnitPrice
                            if (dr["LineSalesItemUnitPrice"].ToString() != string.Empty)
                            {
                                if (dr["LineSalesItemUnitPrice"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineSalesItemUnitPrice (" + dr["LineSalesItemUnitPrice"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesItemLineDetail.LineUnitPrice = dr["LineSalesItemUnitPrice"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineSalesItemQty"))
                        {
                            #region Validations for LineSalesItemQty
                            if (dr["LineSalesItemQty"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineSalesItemQty"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineSalesItemQty( " + dr["LineSalesItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesItemLineDetail.LineQty = dr["LineSalesItemQty"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesItemLineDetail.LineQty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineSalesItemQty"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LineSalesItemTaxCodeRefValue"))
                        {
                            #region Validations of LineSalesItemTaxCodeRefValue
                            if (dr["LineSalesItemTaxCodeRefValue"].ToString() != string.Empty)
                            {
                                if (dr["LineSalesItemTaxCodeRefValue"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineSalesItemTaxCodeRefValue (" + dr["LineSalesItemTaxCodeRefValue"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef.Name = dr["LineSalesItemTaxCodeRefValue"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineSalesItemServiceDate"))
                        {
                            #region Validations of LineSalesItemServiceDate
                            DateTime SODate = new DateTime();
                            if (dr["LineSalesItemServiceDate"].ToString() != "<None>" || dr["LineSalesItemServiceDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["LineSalesItemServiceDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineSalesItemServiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesItemLineDetail.ServiceDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesItemLineDetail.ServiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SalesItemLineDetail.ServiceDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        SalesItemLineDetail.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    SalesItemLineDetail.ServiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (classref.Name != null)
                        {
                            SalesItemLineDetail.ClassRef.Add(classref);
                        }
                        if (ItemRef.Name != null || ItemRef.SKU != null)
                        {
                            SalesItemLineDetail.ItemRef.Add(ItemRef);
                        }
                        if (TaxCodeRef.Name != null)
                        {
                            SalesItemLineDetail.TaxCodeRef.Add(TaxCodeRef);
                        }
                        if (SalesItemLineDetail.ItemRef.Count != 0 || SalesItemLineDetail.LineQty != null || SalesItemLineDetail.LineUnitPrice != null || SalesItemLineDetail.ItemRef.Count != 0 || SalesItemLineDetail.ServiceDate != null)
                        {
                            Line.SalesItemLineDetail.Add(SalesItemLineDetail);
                        }

                       // estimate.Line.Add(Line);

                        OnlineEntities.DiscountLineDetail DiscountLineDetail = new OnlineEntities.DiscountLineDetail();
                        OnlineEntities.Line DiscountLine = new OnlineEntities.Line();

                        if (dt.Columns.Contains("LineDiscountAmount"))
                        {
                            #region Validations for LineDiscountAmount
                            if (dr["LineDiscountAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineDiscountAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDiscountAmount( " + dr["LineDiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    DiscountLineDetail.LineDiscountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LineDiscountPercentBased"))
                        {
                            #region Validations of IsActive
                            if (dr["LineDiscountPercentBased"].ToString() != "<None>" || dr["LineDiscountPercentBased"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["LineDiscountPercentBased"].ToString(), out result))
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToInt32(dr["LineDiscountPercentBased"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["LineDiscountPercentBased"].ToString().ToLower() == "true")
                                    {
                                        DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["LineDiscountPercentBased"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "LineDiscountPercentBased";
                                        }
                                        else
                                            DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountPercentBased(" + dr["LineDiscountPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineDiscountPercent"))
                        {
                            #region Validations for LineDiscountPercent
                            if (dr["LineDiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineDiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDiscountPercent( " + dr["LineDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {

                                            DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();

                                        }
                                        else
                                        {
                                            DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountLineDetail.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountPercent"].ToString()));
                                    }
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.DiscountAccountRef DiscountAccountRef = new OnlineEntities.DiscountAccountRef();


                        if (dt.Columns.Contains("LineDiscountAccountRefName"))
                        {
                            #region Validations of LineDiscountAccountRefName
                            if (dr["LineDiscountAccountRefName"].ToString() != string.Empty)
                            {
                                if (dr["LineDiscountAccountRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDiscountAccountRefName (" + dr["LineDiscountAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (DiscountAccountRef.Name != null)
                        {
                            DiscountLineDetail.DiscountAccountRef.Add(DiscountAccountRef);
                        }
                        ////Improvement::493
                        //if (dt.Columns.Contains("LineDiscountAcctNum"))
                        //{
                        //    #region Validations of LineDiscountAccountRefName
                        //    if (dr["LineDiscountAcctNum"].ToString() != string.Empty)
                        //    {
                        //        if (dr["LineDiscountAcctNum"].ToString().Length > 15)
                        //        {
                        //            if (isIgnoreAll == false)
                        //            {
                        //                string strMessages = "This LineDiscountAcctNum (" + dr["LineDiscountAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                        //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                        //                if (Convert.ToString(result) == "Cancel")
                        //                {
                        //                    continue;
                        //                }
                        //                if (Convert.ToString(result) == "No")
                        //                {
                        //                    return null;
                        //                }
                        //                if (Convert.ToString(result) == "Ignore")
                        //                {
                        //                    DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString().Substring(0, 15);
                        //                }
                        //                if (Convert.ToString(result) == "Abort")
                        //                {
                        //                    isIgnoreAll = true;
                        //                    DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString();
                        //            }
                        //        }
                        //        else
                        //        {
                        //            DiscountAccountRef.AcctNum = dr["LineDiscountAcctNum"].ToString();
                        //        }
                        //    }
                        //    #endregion
                        //}

                        //if (DiscountAccountRef.AcctNum != null)
                        //{
                        //    DiscountLineDetail.DiscountAccountRef.Add(DiscountAccountRef);
                        //}

                        if (DiscountLineDetail.DiscountAccountRef.Count != 0 || DiscountLineDetail.DiscountPercent != null || DiscountLineDetail.LineDiscountAmount != null || DiscountLineDetail.PercentBased != null)
                        {
                            DiscountLine.DiscountLineDetail.Add(DiscountLineDetail);
                        }
                   
                        if (DiscountLine.DiscountLineDetail.Count > 0 && Line.SalesItemLineDetail.Count > 0)
                        {
                            estimate.Line.Add(DiscountLine);
                            estimate.Line.Add(Line);
                        }
                        else if (DiscountLine.DiscountLineDetail.Count > 0 && Line.SalesItemLineDetail.Count == 0)
                        {
                            estimate.Line.Add(DiscountLine);
                        }
                        else if (DiscountLine.DiscountLineDetail.Count == 0 && Line.SalesItemLineDetail.Count > 0)
                        {
                            estimate.Line.Add(Line);
                        }
                        else if (Line.SalesItemLineDetail.Count == 0)
                        {
                            estimate.Line.Add(Line);
                        }
                                             

                        OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                        OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                        OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                        OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                        if (dt.Columns.Contains("TxnTaxCodeRefName"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                            {
                                if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TotalTax"))
                        {
                            #region Validations for TotalTax
                            if (dr["TotalTax"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TotalTax"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TotalTax( " + dr["TotalTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {

                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                        }
                                        if (Convert.ToString(result) == "TotalTax")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();

                                        }
                                        else
                                        {
                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TotalTax = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalTax"].ToString()));
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxLineAmount"))
                        {
                            #region Validations for TaxLineAmount
                            if (dr["TaxLineAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TaxLineAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TaxLineAmount( " + dr["TaxLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "TaxLineAmount")
                                        {
                                            isIgnoreAll = true;
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxLine.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxLineAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("TaxPercentBased"))
                        {
                            #region Validations of IsActive
                            if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                {
                                    TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                    {
                                        TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "TaxPercentBased";
                                        }
                                        else
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxPercent"))
                        {
                            #region Validations for TaxPercent
                            if (dr["TaxPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {

                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "TaxPercent")
                                        {
                                            isIgnoreAll = true;
                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef.Count>0)
                        {
                            TaxLine.TaxLineDetail.Add(TaxLineDetail);
                        }
                        if (TxnTaxCodeRef.Name != null)
                        {
                            TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                        }
                        if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                        {
                            TxnTaxDetail.TaxLine.Add(TaxLine);
                        }

                        if (TxnTaxDetail.TaxLine.Count != 0 || TxnTaxCodeRef.Name != null)
                        {
                            estimate.TxnTaxDetail.Add(TxnTaxDetail);
                        }

                        //bug no. 338
                        #region Shipping

                        OnlineEntities.SalesItemLineDetail SalesItemLineDetail1 = new OnlineEntities.SalesItemLineDetail();
                        OnlineEntities.Line Line1 = new OnlineEntities.Line();
                        OnlineEntities.ItemRef ItemRef1 = new OnlineEntities.ItemRef();
                        OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                        if (dt.Columns.Contains("Shipping"))
                        {
                            #region Validations for LineAmount
                            if (dr["Shipping"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Shipping"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Shipping( " + dr["Shipping"].ToString() + " ) is not valid for quickbooks. If you press cancel this record will not be added to QuickBooks. If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line1.LineAmount = dr["Shipping"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line1.LineAmount = dr["Shipping"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line1.LineAmount = dr["Shipping"].ToString();
                                    }
                                }
                                else
                                {
                                    Line1.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Shipping"].ToString()));
                                }
                            }

                            #endregion

                            ItemRef1.Name = "SHIPPING_ITEM_ID";
                        }

                        if (dt.Columns.Contains("ShippingTaxCode"))
                        {
                            #region Validations of ShippingTaxCode
                            if (dr["ShippingTaxCode"].ToString() != string.Empty)
                            {
                                if (dr["ShippingTaxCode"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShippingTaxCode (" + dr["ShippingTaxCode"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef1.Name = dr["ShippingTaxCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ItemRef1.Name != null)
                        {
                            SalesItemLineDetail1.ItemRef.Add(ItemRef1);
                        }
                        if (TaxCodeRef1.Name != null)
                        {
                            SalesItemLineDetail1.TaxCodeRef.Add(TaxCodeRef1);
                        }
                        if (SalesItemLineDetail1.ItemRef.Count != 0 || SalesItemLineDetail1.TaxCodeRef.Count != 0)
                        {
                            Line1.SalesItemLineDetail.Add(SalesItemLineDetail1);
                        }
                        if (Line1.SalesItemLineDetail.Count != 0)
                        {
                            estimate.Line.Add(Line1);
                        }

                        #endregion
                        //end bug no. 338

                        OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();

                        if (dt.Columns.Contains("CustomerFullyQualifiedName"))
                        {
                            #region Validations of CustomerFullyQualifiedName
                            if (dr["CustomerFullyQualifiedName"].ToString() != string.Empty)
                            {
                                if (dr["CustomerFullyQualifiedName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomerFullyQualifiedName (" + dr["CustomerFullyQualifiedName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["CustomerFullyQualifiedName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomerRef.Name != null)
                        {
                            estimate.CustomerRef.Add(CustomerRef);
                        }

                        if (dt.Columns.Contains("CustomerMemo"))
                        {
                            #region Validations of CustomerMemo
                            if (dr["CustomerMemo"].ToString() != string.Empty)
                            {
                                if (dr["CustomerMemo"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomerMemo (" + dr["CustomerMemo"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.CustomerMemo = dr["CustomerMemo"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.CustomerMemo = dr["CustomerMemo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.CustomerMemo = dr["CustomerMemo"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.CustomerMemo = dr["CustomerMemo"].ToString();
                                }
                            }
                            #endregion
                        }

                        //adding Bill Address 

                        OnlineEntities.BillAddr billAddr = new OnlineEntities.BillAddr();
                        if (dt.Columns.Contains("BillAddrLine1"))
                        {
                            #region Validations of BillAddrLine1
                            if (dr["BillAddrLine1"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrLine1"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrLine1 (" + dr["BillAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillLine1 = dr["BillAddrLine1"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillLine1 = dr["BillAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine1 = dr["BillAddrLine1"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine1 = dr["BillAddrLine1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrLine2"))
                        {
                            #region Validations of BillAddrLine2
                            if (dr["BillAddrLine2"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrLine2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrLine2 (" + dr["BillAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillLine2 = dr["BillAddrLine2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillLine2 = dr["BillAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine2 = dr["BillAddrLine2"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine2 = dr["BillAddrLine2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrLine3"))
                        {
                            #region Validations of BillAddrLine3
                            if (dr["BillAddrLine3"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrLine3"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrLine3 (" + dr["BillAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillLine3 = dr["BillAddrLine3"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillLine3 = dr["BillAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine3 = dr["BillAddrLine3"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine3 = dr["BillAddrLine3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrCity"))
                        {
                            #region Validations of BillAddrCity
                            if (dr["BillAddrCity"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrCity"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrCity (" + dr["BillAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillCity = dr["BillAddrCity"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillCity = dr["BillAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillCity = dr["BillAddrCity"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCity = dr["BillAddrCity"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrCountry"))
                        {
                            #region Validations of BillAddrCountry
                            if (dr["BillAddrCountry"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrCountry"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrCountry (" + dr["BillAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillCountry = dr["BillAddrCountry"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillCountry = dr["BillAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillCountry = dr["BillAddrCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCountry = dr["BillAddrCountry"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrSubDivisionCode"))
                        {
                            #region Validations of BillAddrSubDivisionCode
                            if (dr["BillAddrSubDivisionCode"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrSubDivisionCode"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrSubDivisionCode (" + dr["BillAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrPostalCode"))
                        {
                            #region Validations of BillAddrPostalCode
                            if (dr["BillAddrPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrPostalCode"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrPostalCode (" + dr["BillAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillPostalCode = dr["BillAddrPostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrNote"))
                        {
                            #region Validations of BillAddrNote
                            if (dr["BillAddrNote"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrNote"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrNote (" + dr["BillAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillNote = dr["BillAddrNote"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillNote = dr["BillAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillNote = dr["BillAddrNote"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillNote = dr["BillAddrNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrLine4"))
                        {
                            #region Validations of BillAddrLine4
                            if (dr["BillAddrLine4"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrLine4"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrLine4 (" + dr["BillAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillLine4 = dr["BillAddrLine4"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillLine4 = dr["BillAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine4 = dr["BillAddrLine4"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine4 = dr["BillAddrLine4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrLine5"))
                        {
                            #region Validations of BillAddrLine5
                            if (dr["BillAddrLine5"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrLine5"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrLine5 (" + dr["BillAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillLine5 = dr["BillAddrLine5"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillLine5 = dr["BillAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillLine5 = dr["BillAddrLine5"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine5 = dr["BillAddrLine5"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("BillAddrLat"))
                        {
                            #region Validations of BillAddrLat
                            if (dr["BillAddrLat"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrLat"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrLat (" + dr["BillAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillAddrLat = dr["BillAddrLat"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddrLong"))
                        {
                            #region Validations of BillAddrLong
                            if (dr["BillAddrLong"].ToString() != string.Empty)
                            {
                                if (dr["BillAddrLong"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillAddrLong (" + dr["BillAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billAddr.BillAddrLong = dr["BillAddrLong"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (billAddr.BillLine1 != null || billAddr.BillLine2 != null || billAddr.BillLine3 != null || billAddr.BillLine4 != null || billAddr.BillLine5 != null || billAddr.BillCity != null || billAddr.BillCountry != null || billAddr.BillCountrySubDivisionCode != null || billAddr.BillNote != null || billAddr.BillAddrLat != null || billAddr.BillAddrLong != null || billAddr.BillPostalCode != null)
                            estimate.BillAddr.Add(billAddr);

                        OnlineEntities.ShipAddr shipAddr = new OnlineEntities.ShipAddr();
                        if (dt.Columns.Contains("ShipAddrLine1"))
                        {
                            #region Validations of ShipAddrLine1
                            if (dr["ShipAddrLine1"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine1"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine1 (" + dr["ShipAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine2"))
                        {
                            #region Validations of ShipAddrLine2
                            if (dr["ShipAddrLine2"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine2 (" + dr["ShipAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine3"))
                        {
                            #region Validations of ShipAddrLine3
                            if (dr["ShipAddrLine3"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine3"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine3 (" + dr["ShipAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrCity"))
                        {
                            #region Validations of ShipAddrCity
                            if (dr["ShipAddrCity"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrCity"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrCity (" + dr["ShipAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipCity = dr["ShipAddrCity"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrCountry"))
                        {
                            #region Validations of ShipAddrCountry
                            if (dr["ShipAddrCountry"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrCountry"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrCountry (" + dr["ShipAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrSubDivisionCode"))
                        {
                            #region Validations of ShipAddrSubDivisionCode
                            if (dr["ShipAddrSubDivisionCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrSubDivisionCode"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrSubDivisionCode (" + dr["ShipAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrPostalCode"))
                        {
                            #region Validations of ShipAddrPostalCode
                            if (dr["ShipAddrPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrPostalCode"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrPostalCode (" + dr["ShipAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrNote"))
                        {
                            #region Validations of ShipAddrNote
                            if (dr["ShipAddrNote"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrNote"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrNote (" + dr["ShipAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipNote = dr["ShipAddrNote"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine4"))
                        {
                            #region Validations of ShipAddrLine4
                            if (dr["ShipAddrLine4"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine4"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine4 (" + dr["ShipAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine5"))
                        {
                            #region Validations of ShipAddrLine5
                            if (dr["ShipAddrLine5"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine5"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine5 (" + dr["ShipAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipAddrLat"))
                        {
                            #region Validations of ShipAddrLat
                            if (dr["ShipAddrLat"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLat"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLat (" + dr["ShipAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLong"))
                        {
                            #region Validations of ShipAddrLong
                            if (dr["ShipAddrLong"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLong"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLong (" + dr["ShipAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (shipAddr.ShipLine1 != null || shipAddr.ShipLine2 != null || shipAddr.ShipLine3 != null || shipAddr.ShipLine4 != null || shipAddr.ShipLine5 != null || shipAddr.ShipCity != null || shipAddr.ShipCountry != null || shipAddr.ShipCountrySubDivisionCode != null || shipAddr.ShipNote != null || shipAddr.ShipAddrLat != null || shipAddr.ShipAddrLong != null || shipAddr.ShipPostalCode != null)
                            estimate.ShipAddr.Add(shipAddr);


                        ClassRef ClassRef = new ClassRef();

                        if (dt.Columns.Contains("ClassRef"))
                        {
                            #region Validations of ClassRef
                            if (dr["ClassRef"].ToString() != string.Empty)
                            {
                                if (dr["ClassRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ClassRef (" + dr["ClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef.Name = dr["ClassRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef.Name = dr["ClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["ClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef.Name = dr["ClassRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ClassRef.Name != null)
                        {
                            estimate.ClassRef.Add(ClassRef);
                        }
                        SalesTermRef SalesTermRef = new SalesTermRef();

                        if (dt.Columns.Contains("SalesTermRef"))
                        {
                            #region Validations of SalesTermRef
                            if (dr["SalesTermRef"].ToString() != string.Empty)
                            {
                                if (dr["SalesTermRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesTermRef (" + dr["SalesTermRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesTermRef.Name = dr["SalesTermRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (SalesTermRef.Name != null)
                        {
                            estimate.SalesTermRef.Add(SalesTermRef);
                        }
                        ShipMethodRef ShipMethodRef = new ShipMethodRef();

                        if (dt.Columns.Contains("ShipMethodRef"))
                        {
                            #region Validations of ShipMethodRef
                            if (dr["ShipMethodRef"].ToString() != string.Empty)
                            {
                                if (dr["ShipMethodRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipMethodRef (" + dr["ShipMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipMethodRef.Name = dr["ShipMethodRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ShipMethodRef.Name != null)
                        {
                            estimate.ShipMethodRef.Add(ShipMethodRef);
                        }

                        if (dt.Columns.Contains("ShipDate"))
                        {
                            DateTime SODate = new DateTime();
                            #region validations of ShipDate
                            if (dr["ShipDate"].ToString() != "<None>" || dr["ShipDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["ShipDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.ShipDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.ShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            estimate.ShipDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        estimate.ShipDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    estimate.ShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ApplyTaxAfterDiscount"))
                        {
                            #region Validations of IsActive
                            if (dr["ApplyTaxAfterDiscount"].ToString() != "<None>" || dr["ApplyTaxAfterDiscount"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["ApplyTaxAfterDiscount"].ToString(), out result))
                                {
                                    estimate.ApplyTaxAfterDiscount = Convert.ToInt32(dr["ApplyTaxAfterDiscount"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["ApplyTaxAfterDiscount"].ToString().ToLower() == "true")
                                    {
                                        estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["ApplyTaxAfterDiscount"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "ApplyTaxAfterDiscount";
                                        }
                                        else
                                            estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ApplyTaxAfterDiscount(" + dr["ApplyTaxAfterDiscount"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            estimate.ApplyTaxAfterDiscount = dr["ApplyTaxAfterDiscount"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrintStatus"))
                        {
                            #region Validations of PrintStatus
                            if (dr["PrintStatus"].ToString() != string.Empty)
                            {
                                if (dr["PrintStatus"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrintStatus (" + dr["PrintStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.PrintStatus = dr["PrintStatus"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.PrintStatus = dr["PrintStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.PrintStatus = dr["PrintStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.PrintStatus = dr["PrintStatus"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("EmailStatus"))
                        {
                            #region Validations of EmailStatus
                            if (dr["EmailStatus"].ToString() != string.Empty)
                            {
                                if (dr["EmailStatus"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmailStatus (" + dr["EmailStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.EmailStatus = dr["EmailStatus"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.EmailStatus = dr["EmailStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.EmailStatus = dr["EmailStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.EmailStatus = dr["EmailStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Telephone Number 606
                        OnlineEntities.PhoneClass phno = new OnlineEntities.PhoneClass();
                        if (dt.Columns.Contains("PrimaryPhone"))
                        {
                            #region Validations of PrimaryPhone
                            if (dr["PrimaryPhone"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryPhone"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryPhone (" + dr["PrimaryPhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.PrimaryPhone = dr["PrimaryPhone"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                }
                            }
                            #endregion
                        }
                                               

                        if (dt.Columns.Contains("BillEmail"))
                        {
                            #region Validations of BillEmail
                            if (dr["BillEmail"].ToString() != string.Empty)
                            {
                                //bug 464 Email length change 20 to 100
                                if (dr["BillEmail"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillEmail (" + dr["BillEmail"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            //bug 464 Email length change 20 to 100
                                            estimate.BillEmail = dr["BillEmail"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.BillEmail = dr["BillEmail"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.BillEmail = dr["BillEmail"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.BillEmail = dr["BillEmail"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Expiration"))
                        {
                            DateTime SODate = new DateTime();
                            #region validations of Expiration
                            if (dr["Expiration"].ToString() != "<None>" || dr["Expiration"].ToString() != string.Empty)
                            {
                                datevalue = dr["Expiration"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expiration (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.Expiration = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.Expiration = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            estimate.Expiration = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        estimate.Expiration = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    estimate.Expiration = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AcceptedBy"))
                        {
                            #region Validations of AcceptedBy
                            if (dr["AcceptedBy"].ToString() != string.Empty)
                            {
                                if (dr["AcceptedBy"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AcceptedBy (" + dr["AcceptedBy"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.AcceptedBy = dr["AcceptedBy"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.AcceptedBy = dr["AcceptedBy"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.AcceptedBy = dr["AcceptedBy"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.AcceptedBy = dr["AcceptedBy"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AcceptedDate"))
                        {
                            DateTime SODate = new DateTime();
                            #region validations of AcceptedDate
                            if (dr["AcceptedDate"].ToString() != "<None>" || dr["AcceptedDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["AcceptedDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AcceptedDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                estimate.AcceptedDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                estimate.AcceptedDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            estimate.AcceptedDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        estimate.AcceptedDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    estimate.AcceptedDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("GlobalTaxCalculation"))
                        {
                            #region Validations of GlobalTaxCalculation
                            if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                            {
                                if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TransactionLocationType"))
                        {
                            #region Validations of TransactionLocationType
                            if (dr["TransactionLocationType"].ToString() != string.Empty)
                            {
                                if (dr["TransactionLocationType"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            estimate.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            estimate.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        estimate.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                else
                                {
                                    estimate.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                }
                            }
                            #endregion
                        }

                        #endregion

                        coll.Add(estimate);
                    }

                }
                else
                {
                    return null;
                }

            }
            #endregion

            #endregion

            return coll;

        }
    }
}
