﻿using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Collections.ObjectModel;
using System;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Runtime.CompilerServices;

namespace OnlineDataProcessingsImportClass
{
    public class OnlineAttachmentQBEntry
    {
        
        #region member
        private string m_FilePath;
        
        //private string m_SyncTocken;
       
        private string m_AttachmentNote;
       
        private string m_EntityType;
        private string m_DocNumber;
        private string m_FullName;
       
        private bool m_isAppend;


        #endregion

        #region Constructor
        public OnlineAttachmentQBEntry()
        {
        }
        #endregion

        #region properties

        public string FilePath
        {
            get { return m_FilePath; }
            set { m_FilePath = value; }
        }

        //public string SyncToken
        //{
        //    get { return m_SyncTocken; }
        //    set { m_SyncTocken = value; }
        //}
       
        public string AttachmentNote
        {
            get { return m_AttachmentNote; }
            set { m_AttachmentNote = value; }
        }

       
        public string EntityType
        {
            get { return m_EntityType; }
            set { m_EntityType = value; }
        }

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }

        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }
        #endregion

        //#region  Public Methods
        // static method for resize array for Line tag  or taxline tag.
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]

        ///create new transaction record .
        public async Task<bool> CreateQBOAttachment(OnlineAttachmentQBEntry coll)
        {

            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Attachable attachable = new Attachable();
            Attachable UploadAttachable = new Attachable();
            Intuit.Ipp.Data.AttachableRef attachableEntry = new Intuit.Ipp.Data.AttachableRef();
            OnlineAttachmentQBEntry attachment = new OnlineAttachmentQBEntry();
            attachment = coll;
            try
            {
                #region Add Vendor

                if (attachment.FilePath != null)
                {
                    attachable.FileName = Path.GetFileName(attachment.FilePath); ;
                }
                attachable.Note = attachment.AttachmentNote;
                if (attachment.EntityType != null)
                {
                    attachableEntry.EntityRef = new ReferenceType();

                    attachableEntry.EntityRef.name = attachment.EntityType;
                    attachableEntry.EntityRef.type = attachment.EntityType;
                    attachableEntry.EntityRef.Value = attachment.EntityType;
                    string extension = Path.GetExtension(attachment.FilePath);

                    attachable.ContentType = CommonUtilities.GetFileContenttype(extension);
                    // attachable.FileName = "image1.jpg";
                    if (attachment.FilePath != null)
                    {
                        attachable.FileName = Path.GetFileName(attachment.FilePath); ;
                    }

                    switch (attachment.EntityType.ToLower())
                    {
                        case "invoice":
                            var invoiceDetails = await CommonUtilities.GetInvoiceDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!invoiceDetails.Count.Equals(0))
                            {
                                //Getting listID and Edit Sequence number for modifing Estimate data.
                                foreach (var tempSalesReceiptData in invoiceDetails)
                                {
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                    attachableEntry.EntityRef.name = "Invoice";
                                }
                            }

                            break;
                        case "estimate":
                            var estimateDetails = await CommonUtilities.GetEstimateDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!estimateDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in estimateDetails)
                                {
                                    attachableEntry.EntityRef.name = "Estimate";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "salesreceipt":
                        case "sales receipt":
                            var salesReceiptDetails = await CommonUtilities.GetSalesReceiptDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!salesReceiptDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in salesReceiptDetails)
                                {
                                    attachableEntry.EntityRef.name = "SalesReceipt";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "creditmemo":
                        case "credit memo":
                            var creditMemoDetails = await CommonUtilities.GetCreditMemoDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!creditMemoDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in creditMemoDetails)
                                {
                                    attachableEntry.EntityRef.name = "CreditMemo";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "bill":
                            var billDetails = await CommonUtilities.GetBillDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!billDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in billDetails)
                                {
                                    attachableEntry.EntityRef.name = "Bill";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "purchaseorder":
                        case "purchase order":
                            var purchaseDetails = await CommonUtilities.GetPurchaseOrderDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!purchaseDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in purchaseDetails)
                                {
                                    attachableEntry.EntityRef.name = "PurchaseOrder";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "purchase":
                        case "checkpurchase":
                        case "check purchase":
                        case "cashpurchase":
                        case "cash purchase":
                        case "creditcardpurchase":
                        case "credit card purchase":
                            var purchase = await CommonUtilities.GetPurchaseDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!purchase.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in purchase)
                                {
                                    attachableEntry.EntityRef.name = "Purchase";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "vendorcredit":
                        case "vendor credit":
                            var vendorCreditDetails = await CommonUtilities.GetVendorCreditDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!vendorCreditDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in vendorCreditDetails)
                                {
                                    attachableEntry.EntityRef.name = "VendorCredit";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "timetracking":
                        case "time tracking":
                        case "timeactivity":
                        case "time activity":
                            var timetrackingDetails = await CommonUtilities.GetTimeActivityDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!timetrackingDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in timetrackingDetails)
                                {
                                    attachableEntry.EntityRef.name = "TimeActivity";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "journal":
                        case "journalentry":
                        case "journal entry":

                            var journalDetails = await CommonUtilities.GetJournalDetailsExistsInOnlineQBO(attachment.DocNumber);
                            if (!journalDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in journalDetails)
                                {
                                    attachableEntry.EntityRef.name = "JournalEntry";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "item":
                            var itemDetails = await CommonUtilities.GetItemExistsInOnlineQBO(attachment.FullName);
                            if (!itemDetails.Count.Equals(0))
                            {
                                foreach (var tempSalesReceiptData in itemDetails)
                                {
                                    attachableEntry.EntityRef.name = "Item";
                                    attachableEntry.EntityRef.Value = tempSalesReceiptData.Id;
                                }
                            }
                            break;
                        case "customer":
                            var customerDetails = await CommonUtilities.GetCustomerExistsInOnlineQBO(attachment.FullName);
                            if (!customerDetails.Count.Equals(0))
                            {
                                foreach (var tempCustomerData in customerDetails)
                                {
                                    attachableEntry.EntityRef.name = "Customer";
                                    attachableEntry.EntityRef.Value = tempCustomerData.Id;
                                }
                            }
                            break;
                        default:
                            break;
                    }

                    attachable.AttachableRef = new Intuit.Ipp.Data.AttachableRef[1];
                    attachable.AttachableRef[0] = attachableEntry;
                    FileInfo file = new FileInfo(@attachment.FilePath);
                    FileStream fs = file.OpenRead();
                    UploadAttachable = new Attachable();
                    UploadAttachable = dataService.Upload(attachable, fs);

                }



                #endregion
            }

            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                //CommonUtilities.GetInstance().getError = string.Empty;
                //CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            catch (Exception ex)
            {
                CommonUtilities.GetInstance().getError = ex.Message;
                //MessageBox.Show(ex.Message);
                //MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (UploadAttachable == null)
                    {
                        if (attachment.EntityType == "Item")
                        {
                            CommonUtilities.GetInstance().getError = "Invalid Item Name";
                        }
                        else if(attachment.EntityType == "Customer")
                        {
                            CommonUtilities.GetInstance().getError = "Invalid Customer Name";
                        }
                        else
                        {
                            CommonUtilities.GetInstance().getError = "Invalid DocNumber or EntityType";
                        }
                    }
                    else if (UploadAttachable.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = UploadAttachable.Id;
                        CommonUtilities.GetInstance().SyncToken = UploadAttachable.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        public async Task<bool> UpdateQBOAttachment(OnlineDataProcessingsImportClass.OnlineAttachmentQBEntry coll, string Id, string syncToken, Intuit.Ipp.Data.Attachable PreviousData = null)
        {
            
            DataService dataService = null;
            Vendor VendorAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Attachable addedVendor = new Attachable();
            OnlineAttachmentQBEntry Attachment = new OnlineAttachmentQBEntry();
            Attachment = coll;

            try
            {
                
                
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (VendorAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = VendorAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = VendorAdded.SyncToken;
                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }
    }

    public class OnlineAttachmentQBEntryCollection : Collection<OnlineAttachmentQBEntry>
    {
             
    }

}
