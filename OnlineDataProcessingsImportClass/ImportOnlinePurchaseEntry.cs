﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.Globalization;
using EDI.Constant;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using OnlineEntities;


namespace OnlineDataProcessingsImportClass
{
   public class ImportOnlinePurchaseEntry
    {
       private static ImportOnlinePurchaseEntry m_ImportOnlinePurchaseEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlinePurchaseEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import purchase entry class
        /// </summary>
        /// <returns></returns>
        /// 
        public static ImportOnlinePurchaseEntry GetInstance()
        {
            if (m_ImportOnlinePurchaseEntry == null)
                m_ImportOnlinePurchaseEntry = new ImportOnlinePurchaseEntry();
            return m_ImportOnlinePurchaseEntry;
        }
        /// setting values to purchase entry data table and returns collection.
        public async System.Threading.Tasks.Task<OnlinePurchaseQBEntryCollection> ImportOnlinePurchaseData(DataTable dt)
       {
           //Create an instance of Employee Entry collections.
           OnlineDataProcessingsImportClass.OnlinePurchaseQBEntryCollection coll = new OnlinePurchaseQBEntryCollection();
           isIgnoreAll = false;
           isQuit = false;
           int validateRowCount = 1;
           TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
           BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
           #region For Constant Entry

           #region Checking Validations
           foreach (DataRow dr in dt.Rows)
           {
               if (bkWorker.CancellationPending != true)
               {
                   //Bug 1434
                   System.Threading.Thread.Sleep(100);
                   bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                   CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                   validateRowCount = validateRowCount + 1;
                   try
                   {
                       int counterrows = 0;
                       bool resultrows = false;
                       for (int l = 0; l < dt.Columns.Count; l++)
                       {
                           resultrows = dr.IsNull(dt.Columns[l]);
                           if (resultrows)
                           {
                               counterrows = counterrows + 1;
                               if (counterrows != dt.Columns.Count)
                               {
                                   resultrows = false;
                               }
                           }

                       }
                       if (resultrows == true && counterrows == dt.Columns.Count)
                       {
                           continue;
                       }
                   }
                   catch { }
                   //   DateTime EmployeeDt = new DateTime();
                   string datevalue = string.Empty;
                   
                   //Employee Validation
                   OnlineDataProcessingsImportClass.OnlinePurchaseQBEntry Purchase = new OnlineDataProcessingsImportClass.OnlinePurchaseQBEntry();
                   if (dt.Columns.Contains("DocNumber"))
                   {
                       Purchase = coll.FindPurchaseNumberEntry(dr["DocNumber"].ToString());

                       if (Purchase == null)
                       {

                           #region if Purchase is null

                           Purchase = new OnlinePurchaseQBEntry();

                           if (dt.Columns.Contains("DocNumber"))
                           {
                               #region Validations of docnumber
                               if (dr["DocNumber"].ToString() != string.Empty)
                               {
                                   if (dr["DocNumber"].ToString().Length > 21)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Purchase.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Purchase.DocNumber = dr["DocNumber"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Purchase.DocNumber = dr["DocNumber"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.DocNumber = dr["DocNumber"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("TxnDate"))
                           {
                               #region Validations of PrivateNote
                               DateTime SODate = new DateTime();
                               if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                               {
                                   datevalue = dr["TxnDate"].ToString();
                                   if (!DateTime.TryParse(datevalue, out SODate))
                                   {
                                       DateTime dttest = new DateTime();
                                       bool IsValid = false;

                                       try
                                       {
                                           dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                           IsValid = true;
                                       }
                                       catch
                                       {
                                           IsValid = false;
                                       }
                                       if (IsValid == false)
                                       {
                                           if (isIgnoreAll == false)
                                           {
                                               string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                               DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                               if (Convert.ToString(result) == "Cancel")
                                               {
                                                   continue;
                                               }
                                               if (Convert.ToString(result) == "No")
                                               {
                                                   return null;
                                               }
                                               if (Convert.ToString(result) == "Ignore")
                                               {
                                                   Purchase.TxnDate = datevalue;
                                               }
                                               if (Convert.ToString(result) == "Abort")
                                               {
                                                   isIgnoreAll = true;
                                                   Purchase.TxnDate = datevalue;
                                               }
                                           }
                                           else
                                           {
                                               Purchase.TxnDate = datevalue;
                                           }
                                       }
                                       else
                                       {
                                           SODate = dttest;
                                           Purchase.TxnDate = dttest.ToString("yyyy-MM-dd");
                                       }

                                   }
                                   else
                                   {
                                       SODate = Convert.ToDateTime(datevalue);
                                       Purchase.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("PrivateNote"))
                           {
                               #region Validations of PrivateNote
                               if (dr["PrivateNote"].ToString() != string.Empty)
                               {
                                   if (dr["PrivateNote"].ToString().Length > 4000)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Purchase.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Purchase.PrivateNote = dr["PrivateNote"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Purchase.PrivateNote = dr["PrivateNote"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.PrivateNote = dr["PrivateNote"].ToString();
                                   }
                               }
                               #endregion
                           }

                            PaymentMethodRef PaymentMethodRef = new PaymentMethodRef();

                            if (dt.Columns.Contains("PaymentMethodRef"))
                            {
                                #region Validations of PaymentMethodRef
                                if (dr["PaymentMethodRef"].ToString() != string.Empty)
                                {
                                    if (dr["PaymentMethodRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentMethodRef (" + dr["PaymentMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (PaymentMethodRef.Name != null)
                            {
                                Purchase.PaymentMethodRef.Add(PaymentMethodRef);
                            }

                            OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();

                           if (dt.Columns.Contains("DepartmentRef"))
                           {
                               #region Validations of DepartmentRef
                               if (dr["DepartmentRef"].ToString() != string.Empty)
                               {
                                   if (dr["DepartmentRef"].ToString().Length > 4000)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 4000);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (DepartmentRef.Name != null)
                           {
                               Purchase.DepartmentRef.Add(DepartmentRef);
                           }

                           //bug no. 418
                           OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                           if (dt.Columns.Contains("CurrencyRef"))
                           {
                               #region Validations of CurrencyRef
                               if (dr["CurrencyRef"].ToString() != string.Empty)
                               {
                                   if (dr["CurrencyRef"].ToString().Length > 20)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (CurrencyRef.Name != null)
                           {
                               Purchase.CurrencyRef.Add(CurrencyRef);
                           }

                           if (dt.Columns.Contains("ExchangeRate"))
                           {
                               #region Validations for ExchangeRate
                               if (dr["ExchangeRate"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Purchase.ExchangeRate = dr["ExchangeRate"].ToString();
                                           }
                                           if (Convert.ToString(result) == "ExchangeRate")
                                           {
                                               isIgnoreAll = true;
                                               Purchase.ExchangeRate = dr["ExchangeRate"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Purchase.ExchangeRate = dr["ExchangeRate"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                   }
                               }

                               #endregion
                           }

                           #region if purchase

                           OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                           OnlineEntities.Line Line = new OnlineEntities.Line();
                           OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                           OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                           OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                           OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                           if (dt.Columns.Contains("LineDescription"))
                           {
                               #region Validations of Associate
                               if (dr["LineDescription"].ToString() != string.Empty)
                               {
                                   if (dr["LineDescription"].ToString().Length > 4000)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Line.LineDescription = dr["LineDescription"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Line.LineDescription = dr["LineDescription"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Line.LineDescription = dr["LineDescription"].ToString();
                                   }
                               }
                               #endregion
                           }
                           if (dt.Columns.Contains("LineAmount"))
                           {
                               #region Validations for LineAmount
                               if (dr["LineAmount"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Line.LineAmount = dr["LineAmount"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Line.LineAmount = dr["LineAmount"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Line.LineAmount = dr["LineAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                   }
                               }

                               #endregion
                           }
                           if (dt.Columns.Contains("LineItemRef"))
                           {
                               #region Validations of LineItemRef
                               if (dr["LineItemRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineItemRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemRef.Name = dr["LineItemRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemRef.Name = dr["LineItemRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemRef.Name = dr["LineItemRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           //bug 486
                           if (dt.Columns.Contains("SKU"))
                           {
                               #region Validations of SKU
                               if (dr["SKU"].ToString() != string.Empty)
                               {
                                   if (dr["SKU"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemRef.SKU = dr["SKU"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemRef.SKU = dr["SKU"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemRef.SKU = dr["SKU"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineClassRef"))
                           {
                               #region Validations of LineClassRef
                               if (dr["LineClassRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineClassRef"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ClassRef.Name = dr["LineClassRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ClassRef.Name = dr["LineClassRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ClassRef.Name = dr["LineClassRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineItemQty"))
                           {
                               #region Validations for LineItemQty
                               if (dr["LineItemQty"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                   }
                               }

                               #endregion
                           }
                           if (dt.Columns.Contains("LineItemUnitPrice"))
                           {
                               #region Validations for LineItemUnitPrice
                               if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                   }
                               }

                               #endregion
                           }

                           if (dt.Columns.Contains("LineItemTaxCodeRef"))
                           {
                               #region Validations of LineItemTaxCodeRef
                               if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineCustomerRef"))
                           {
                               #region Validations of LineCustomerRef
                               if (dr["LineCustomerRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineCustomerRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineBillableStatus"))
                           {
                               #region Validations of LineBillableStatus
                               if (dr["LineBillableStatus"].ToString() != string.Empty)
                               {
                                   if (dr["LineBillableStatus"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (ItemRef.Name != null || ItemRef.SKU != null)
                           {
                               ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                           }
                           if (TaxCodeRef.Name != null)
                           {
                               ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                           }
                           if (ClassRef.Name != null)
                           {
                               ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                           }
                           if (CustomerRef.Name != null)
                           {
                               ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                           }
                           if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                           {
                               Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                           }

                           OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                           OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();
                           if (dt.Columns.Contains("LineAccountDescription"))
                           {
                               #region Validations of LineAccountDescription
                               if (dr["LineAccountDescription"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountDescription"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineAccountAmount"))
                           {
                               #region Validations for LineAccountAmount
                               if (dr["LineAccountAmount"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                   }
                               }

                               #endregion
                           }

                           OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                           if (dt.Columns.Contains("LineAccountCustomerRef"))
                           {
                               #region Validations of LineAccountCustomerRef
                               if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                   }
                               }
                               #endregion
                           }
                           if (CustomerRef1.Name != null)
                           {
                               AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                           }

                           OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                           if (dt.Columns.Contains("LineAccountClassRef"))
                           {
                               #region Validations of LineAccountClassRef
                               if (dr["LineAccountClassRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountClassRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (ClassRef1.Name != null)
                           {
                               AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                           }

                           OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                           if (dt.Columns.Contains("LineAccountRef"))
                           {
                               #region Validations of LineAccountRef
                               if (dr["LineAccountRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountRef.Name = dr["LineAccountRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountRef.Name = dr["LineAccountRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountRef.Name = dr["LineAccountRef"].ToString();
                                   }
                               }
                               #endregion
                               // bug 480
                               #region Account Validation
                               if (AccountRef.Name != null)
                               {
                                   List<Intuit.Ipp.Data.Account> Accountdata = null;
                                   string AccountName = string.Empty;
                                   AccountName = AccountRef.Name.Trim();
                                   Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);

                                   //bug 480
                                   if (Accountdata != null)
                                   {
                                       if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                       {
                                           //480
                                           Messages m1 = new Messages();
                                           m1.buttonCancel.Text = "Add";
                                           m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                           m1.ShowDialog();
                                           //Add
                                           if (m1.DialogResult.ToString().ToLower() == "ok")
                                           {
                                               CommonUtilities cUtility = new CommonUtilities();
                                               cUtility.CreateAccountInQBO(AccountName);
                                           }
                                           //Quit
                                           if (m1.DialogResult.ToString().ToLower() == "no")
                                           {
                                               break;
                                               //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                               //{
                                               //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                               //    bkWorker.CancelAsync();
                                               //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                               //}
                                               //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                               //{
                                               //    axisForm.IsBusy = false;
                                               //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                               //    bkWorker.CancelAsync();

                                               //}
                                               //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                               //{
                                               //    axisForm.IsBusy = false;
                                               //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                               //    bkWorker.CancelAsync();
                                               //}

                                               ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                               //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                           }
                                           //Ignore
                                           if (m1.DialogResult.ToString() == "Ignore")
                                           {

                                           }
                                           //IgnoreAll
                                           if (m1.DialogResult.ToString() == "Abort")
                                           {
                                               CommonUtilities.GetInstance().ignoreAllAccount = true;
                                           }
                                       }
                                   }
                               }
                               #endregion
                           }

                           if (AccountRef.Name != null)
                           {
                               AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                           }

                          
                           if (dt.Columns.Contains("LineAccountBillableStatus"))
                           {
                               #region Validations of LineAccountBillableStatus
                               if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                   }
                               }
                               #endregion
                           }

                           OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                           if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                           {
                               #region Validations of LineAccountTaxCodeRef
                               if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (TaxCodeRef1.Name != null)
                           {
                               AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                           }

                           if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                               AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);                          


                           if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                           {
                               Purchase.Line.Add(AccountBasedExpenseLine);
                               Purchase.Line.Add(Line);
                           }
                           else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                           {
                               Purchase.Line.Add(AccountBasedExpenseLine);
                           }
                           else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                           {
                               Purchase.Line.Add(Line);
                           }
                           else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                           {
                               Purchase.Line.Add(Line);
                               Purchase.Line.Add(AccountBasedExpenseLine);
                           }
                           #endregion

                           OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                           OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                           OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                           OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                           if (dt.Columns.Contains("TxnTaxCodeRefName"))
                           {
                               #region Validations of TxnTaxCodeRefName
                               if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                               {
                                   if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("TotalTax"))
                           {
                               #region Validations for TotalTax
                               if (dr["TotalTax"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["TotalTax"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This TotalTax( " + dr["TotalTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {

                                               TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                           }
                                           if (Convert.ToString(result) == "TotalTax")
                                           {
                                               isIgnoreAll = true;
                                               TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();

                                           }
                                           else
                                           {
                                               TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TxnTaxDetail.TotalTax = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalTax"].ToString()));
                                       }
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("TaxLineAmount"))
                           {
                               #region Validations for TaxLineAmount
                               if (dr["TaxLineAmount"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["TaxLineAmount"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This TaxLineAmount( " + dr["TaxLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                           }
                                           if (Convert.ToString(result) == "TaxLineAmount")
                                           {
                                               isIgnoreAll = true;
                                               TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxLine.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxLineAmount"].ToString()));
                                   }
                               }

                               #endregion
                           }

                           if (dt.Columns.Contains("TaxPercentBased"))
                           {
                               #region Validations of IsActive
                               if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                               {

                                   int result = 0;
                                   if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                   {
                                       TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                   }
                                   else
                                   {
                                       string strvalid = string.Empty;
                                       if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                       {
                                           TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                       }
                                       else
                                       {
                                           if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                           {
                                               strvalid = "TaxPercentBased";
                                           }
                                           else
                                               TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                       }
                                       if (strvalid != string.Empty)
                                       {
                                           if (isIgnoreAll == false)
                                           {
                                               string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                               DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                               if (Convert.ToString(results) == "Cancel")
                                               {
                                                   continue;
                                               }
                                               if (Convert.ToString(result) == "No")
                                               {
                                                   return null;
                                               }
                                               if (Convert.ToString(results) == "Ignore")
                                               {
                                                   TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                               }
                                               if (Convert.ToString(results) == "Abort")
                                               {
                                                   isIgnoreAll = true;
                                                   TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                               }
                                           }
                                           else
                                           {
                                               TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                           }
                                       }

                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("TaxPercent"))
                           {
                               #region Validations for TaxPercent
                               if (dr["TaxPercent"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {

                                               TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                           }
                                           if (Convert.ToString(result) == "TaxPercent")
                                           {
                                               isIgnoreAll = true;
                                               TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                   }
                               }

                               #endregion
                           }

                           if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef.Count > 0)
                           {
                               TaxLine.TaxLineDetail.Add(TaxLineDetail);
                           }
                           if (TxnTaxCodeRef.Name != null)
                           {
                               TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                           }
                           if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                           {
                               TxnTaxDetail.TaxLine.Add(TaxLine);
                           }
                           if (TxnTaxDetail.TxnTaxCodeRef.Count != 0 || TxnTaxDetail.TaxLine.Count != 0)
                           {
                               Purchase.TxnTaxDetail.Add(TxnTaxDetail);
                           }

                           OnlineEntities.AccountRef AccountRef1 = new OnlineEntities.AccountRef();

                           if (dt.Columns.Contains("AccountRef"))
                           {
                               #region Validations of AccountRef
                               if (dr["AccountRef"].ToString() != string.Empty)
                               {
                                   if (dr["AccountRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This AccountRef (" + dr["AccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountRef1.Name = dr["AccountRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountRef1.Name = dr["AccountRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountRef1.Name = dr["AccountRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountRef1.Name = dr["AccountRef"].ToString();
                                   }

                                   // bug 480
                                   #region Account Validation
                                   List<Intuit.Ipp.Data.Account> Accountdata = null;
                                   string AccountName = string.Empty;
                                   AccountName = AccountRef1.Name.Trim();
                                   Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                   if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                   {
                                       //480
                                       Messages m1 = new Messages();
                                       m1.buttonCancel.Text = "Add";
                                       m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                       m1.ShowDialog();
                                       //Add
                                       if (m1.DialogResult.ToString().ToLower() == "ok")
                                       {
                                           CommonUtilities cUtility = new CommonUtilities();
                                          await cUtility.CreateAccountInQBO(AccountName);
                                       }
                                       //Quit
                                       if (m1.DialogResult.ToString().ToLower() == "no")
                                       {
                                           break;
                                           //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                           //{
                                           //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                           //    bkWorker.CancelAsync();
                                           //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                           //}
                                           //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                           //{
                                           //    axisForm.IsBusy = false;
                                           //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                           //    bkWorker.CancelAsync();

                                           //}
                                           //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                           //{
                                           //    axisForm.IsBusy = false;
                                           //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                           //    bkWorker.CancelAsync();
                                           //}

                                           ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                           //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                       }
                                       //Ignore
                                       if (m1.DialogResult.ToString() == "Ignore")
                                       {

                                       }
                                       //IgnoreAll
                                       if (m1.DialogResult.ToString() == "Abort")
                                       {
                                           CommonUtilities.GetInstance().ignoreAllAccount = true;
                                       }
                                   }


                                   #endregion
                               }
                               #endregion
                           }

                           if (AccountRef1.Name != null)
                           {
                               Purchase.AccountRef.Add(AccountRef1);
                           }

                          

                           OnlineEntities.Entity Entity = new OnlineEntities.Entity();

                           if (dt.Columns.Contains("EntityType"))
                           {
                               #region Validations of EntityType
                               if (dr["EntityType"].ToString() != string.Empty)
                               {
                                   if (dr["EntityType"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This EntityType (" + dr["EntityType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Entity.Type = dr["EntityType"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Entity.Type = dr["EntityType"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Entity.Type = dr["EntityType"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Entity.Type = dr["EntityType"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("EntityRef"))
                           {
                               #region Validations of EntityRef
                               if (dr["EntityRef"].ToString() != string.Empty)
                               {
                                   if (dr["EntityRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This EntityRef (" + dr["EntityRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Entity.Name = dr["EntityRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Entity.Name = dr["EntityRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Entity.Name = dr["EntityRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Entity.Name = dr["EntityRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (Entity.Name != null)
                           {
                               Purchase.Entity.Add(Entity);
                           }

                           if (dt.Columns.Contains("Credit"))
                           {
                               #region Validations of Credit
                               if (dr["Credit"].ToString() != string.Empty)
                               {
                                   if (dr["Credit"].ToString().Length > 10)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This Credit (" + dr["Credit"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Purchase.Credit = dr["Credit"].ToString().Substring(0, 10);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Purchase.Credit = dr["Credit"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Purchase.Credit = dr["Credit"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.Credit = dr["Credit"].ToString();
                                   }
                               }
                               #endregion
                           }                           

                           if (dt.Columns.Contains("PrintStatus"))
                           {
                               #region Validations of PrintStatus
                               if (dr["PrintStatus"].ToString() != string.Empty)
                               {
                                   if (dr["PrintStatus"].ToString().Length > 20)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This PrintStatus (" + dr["PrintStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Purchase.PrintStatus = dr["PrintStatus"].ToString().Substring(0, 20);

                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Purchase.PrintStatus = dr["PrintStatus"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Purchase.PrintStatus = dr["PrintStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.PrintStatus = dr["PrintStatus"].ToString();
                                   }
                               }
                               #endregion
                           }
                         
                           if (dt.Columns.Contains("GlobalTaxCalculation"))
                           {
                               #region Validations of GlobalTaxCalculation
                               if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                               {
                                   if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                   }
                               }
                               #endregion
                           }

                            if (dt.Columns.Contains("TransactionLocationType"))
                            {
                                #region Validations of TransactionLocationType
                                if (dr["TransactionLocationType"].ToString() != string.Empty)
                                {
                                    if (dr["TransactionLocationType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            #endregion

                            coll.Add(Purchase);
                       }
                       else
                       {
                           #region if purchase is not null

                           OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                           OnlineEntities.Line Line = new OnlineEntities.Line();
                           OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                           OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                           OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                           OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                           if (dt.Columns.Contains("LineDescription"))
                           {
                               #region Validations of Associate
                               if (dr["LineDescription"].ToString() != string.Empty)
                               {
                                   if (dr["LineDescription"].ToString().Length > 4000)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Line.LineDescription = dr["LineDescription"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Line.LineDescription = dr["LineDescription"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Line.LineDescription = dr["LineDescription"].ToString();
                                   }
                               }
                               #endregion
                           }
                           if (dt.Columns.Contains("LineAmount"))
                           {
                               #region Validations for LineAmount
                               if (dr["LineAmount"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Line.LineAmount = dr["LineAmount"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Line.LineAmount = dr["LineAmount"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Line.LineAmount = dr["LineAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                   }
                               }

                               #endregion
                           }
                           if (dt.Columns.Contains("LineItemRef"))
                           {
                               #region Validations of LineItemRef
                               if (dr["LineItemRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineItemRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemRef.Name = dr["LineItemRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemRef.Name = dr["LineItemRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemRef.Name = dr["LineItemRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           //bug 486
                           if (dt.Columns.Contains("SKU"))
                           {
                               #region Validations of SKU
                               if (dr["SKU"].ToString() != string.Empty)
                               {
                                   if (dr["SKU"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemRef.SKU = dr["SKU"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemRef.SKU = dr["SKU"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemRef.SKU = dr["SKU"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineClassRef"))
                           {
                               #region Validations of LineClassRef
                               if (dr["LineClassRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineClassRef"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ClassRef.Name = dr["LineClassRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ClassRef.Name = dr["LineClassRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ClassRef.Name = dr["LineClassRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineItemQty"))
                           {
                               #region Validations for LineItemQty
                               if (dr["LineItemQty"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                   }
                               }

                               #endregion
                           }
                           if (dt.Columns.Contains("LineItemUnitPrice"))
                           {
                               #region Validations for LineItemUnitPrice
                               if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                   }
                               }

                               #endregion
                           }

                           if (dt.Columns.Contains("LineItemTaxCodeRef"))
                           {
                               #region Validations of LineItemTaxCodeRef
                               if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                   }
                               }
                               #endregion
                           }


                           if (dt.Columns.Contains("LineCustomerRef"))
                           {
                               #region Validations of LineCustomerRef
                               if (dr["LineCustomerRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineCustomerRef"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineBillableStatus"))
                           {
                               #region Validations of LineBillableStatus
                               if (dr["LineBillableStatus"].ToString() != string.Empty)
                               {
                                   if (dr["LineBillableStatus"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (ItemRef.Name != null || ItemRef.SKU != null)
                           {
                               ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                           }
                           if (TaxCodeRef.Name != null)
                           {
                               ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                           }
                           if (ClassRef.Name != null)
                           {
                               ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                           }
                           if (CustomerRef.Name != null)
                           {
                               ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                           }
                           if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                           {
                               Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                           }

                           OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                           OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();
                           if (dt.Columns.Contains("LineAccountDescription"))
                           {
                               #region Validations of LineAccountDescription
                               if (dr["LineAccountDescription"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountDescription"].ToString().Length > 40)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("LineAccountAmount"))
                           {
                               #region Validations for LineAccountAmount
                               if (dr["LineAccountAmount"].ToString() != string.Empty)
                               {
                                   decimal amount;
                                   if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                   }
                               }

                               #endregion
                           }

                           OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                           if (dt.Columns.Contains("LineAccountCustomerRef"))
                           {
                               #region Validations of LineAccountCustomerRef
                               if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                   }
                               }
                               #endregion
                           }
                           if (CustomerRef1.Name != null)
                           {
                               AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                           }

                           OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                           if (dt.Columns.Contains("LineAccountClassRef"))
                           {
                               #region Validations of LineAccountClassRef
                               if (dr["LineAccountClassRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountClassRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (ClassRef1.Name != null)
                           {
                               AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                           }

                           OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                           if (dt.Columns.Contains("LineAccountRef"))
                           {
                               #region Validations of LineAccountRef
                               if (dr["LineAccountRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountRef.Name = dr["LineAccountRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountRef.Name = dr["LineAccountRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountRef.Name = dr["LineAccountRef"].ToString();
                                   }

                                   // bug 480
                                   #region Account Validation
                                   List<Intuit.Ipp.Data.Account> Accountdata = null;
                                   string AccountName = string.Empty;
                                   AccountName = AccountRef.Name.Trim();
                                   Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                   if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                   {
                                       //480
                                       Messages m1 = new Messages();
                                       m1.buttonCancel.Text = "Add";
                                       m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                       m1.ShowDialog();
                                       //Add
                                       if (m1.DialogResult.ToString().ToLower() == "ok")
                                       {
                                           CommonUtilities cUtility = new CommonUtilities();
                                           cUtility.CreateAccountInQBO(AccountName);
                                       }
                                       //Quit
                                       if (m1.DialogResult.ToString().ToLower() == "no")
                                       {
                                           break;
                                           //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                           //{
                                           //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                           //    bkWorker.CancelAsync();
                                           //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                           //}
                                           //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                           //{
                                           //    axisForm.IsBusy = false;
                                           //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                           //    bkWorker.CancelAsync();

                                           //}
                                           //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                           //{
                                           //    axisForm.IsBusy = false;
                                           //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                           //    bkWorker.CancelAsync();
                                           //}

                                           ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                           //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                       }
                                       //Ignore
                                       if (m1.DialogResult.ToString() == "Ignore")
                                       {

                                       }
                                       //IgnoreAll
                                       if (m1.DialogResult.ToString() == "Abort")
                                       {
                                           CommonUtilities.GetInstance().ignoreAllAccount = true;
                                       }
                                   }


                                   #endregion
                               }
                               #endregion
                           }
                          
                           if (AccountRef.Name != null)
                           {
                               AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                           }

                           if (dt.Columns.Contains("LineAccountBillableStatus"))
                           {
                               #region Validations of LineAccountBillableStatus
                               if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                   }
                               }
                               #endregion
                           }

                           OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                           if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                           {
                               #region Validations of LineAccountTaxCodeRef
                               if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                               {
                                   if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (TaxCodeRef1.Name != null)
                           {
                               AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                           }

                           if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                               AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);


                           if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                           {
                               Purchase.Line.Add(AccountBasedExpenseLine);
                               Purchase.Line.Add(Line);
                           }
                           else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                           {
                               Purchase.Line.Add(AccountBasedExpenseLine);
                           }
                           else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                           {
                               Purchase.Line.Add(Line);
                           }
                           else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                           {
                               Purchase.Line.Add(Line);
                               Purchase.Line.Add(AccountBasedExpenseLine);
                           }
                           #endregion
                       }
                   }
                   else
                   {
                       Purchase = new OnlinePurchaseQBEntry();

                       #region if Purchase is null

                       if (dt.Columns.Contains("TxnDate"))
                       {
                           #region Validations of PrivateNote
                           DateTime SODate = new DateTime();
                           if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                           {
                               datevalue = dr["TxnDate"].ToString();
                               if (!DateTime.TryParse(datevalue, out SODate))
                               {
                                   DateTime dttest = new DateTime();
                                   bool IsValid = false;

                                   try
                                   {
                                       dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                       IsValid = true;
                                   }
                                   catch
                                   {
                                       IsValid = false;
                                   }
                                   if (IsValid == false)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Purchase.TxnDate = datevalue;
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Purchase.TxnDate = datevalue;
                                           }
                                       }
                                       else
                                       {
                                           Purchase.TxnDate = datevalue;
                                       }
                                   }
                                   else
                                   {
                                       SODate = dttest;
                                       Purchase.TxnDate = dttest.ToString("yyyy-MM-dd");
                                   }

                               }
                               else
                               {
                                   SODate = Convert.ToDateTime(datevalue);
                                   Purchase.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("PrivateNote"))
                       {
                           #region Validations of PrivateNote
                           if (dr["PrivateNote"].ToString() != string.Empty)
                           {
                               if (dr["PrivateNote"].ToString().Length > 4000)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Purchase.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Purchase.PrivateNote = dr["PrivateNote"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.PrivateNote = dr["PrivateNote"].ToString();
                                   }
                               }
                               else
                               {
                                   Purchase.PrivateNote = dr["PrivateNote"].ToString();
                               }
                           }
                           #endregion
                       }
                        // Axis 740
                        PaymentMethodRef PaymentMethodRef = new PaymentMethodRef();

                        if (dt.Columns.Contains("PaymentMethodRef"))
                        {
                            #region Validations of PaymentMethodRef
                            if (dr["PaymentMethodRef"].ToString() != string.Empty)
                            {
                                if (dr["PaymentMethodRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PaymentMethodRef (" + dr["PaymentMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                    }
                                }
                                else
                                {
                                    PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (PaymentMethodRef.Name != null)
                        {
                            Purchase.PaymentMethodRef.Add(PaymentMethodRef);
                        }
                        // Axis 740 End
                        OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();

                       if (dt.Columns.Contains("DepartmentRef"))
                       {
                           #region Validations of Associate
                           if (dr["DepartmentRef"].ToString() != string.Empty)
                           {
                               if (dr["DepartmentRef"].ToString().Length > 4000)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 4000);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                   }
                               }
                               else
                               {
                                   DepartmentRef.Name = dr["DepartmentRef"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (DepartmentRef.Name != null)
                       {
                           Purchase.DepartmentRef.Add(DepartmentRef);
                       }

                       //bug no. 418
                       OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                       if (dt.Columns.Contains("CurrencyRef"))
                       {
                           #region Validations of CurrencyRef
                           if (dr["CurrencyRef"].ToString() != string.Empty)
                           {
                               if (dr["CurrencyRef"].ToString().Length > 20)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                   }
                               }
                               else
                               {
                                   CurrencyRef.Name = dr["CurrencyRef"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (CurrencyRef.Name != null)
                       {
                           Purchase.CurrencyRef.Add(CurrencyRef);
                       }

                       if (dt.Columns.Contains("ExchangeRate"))
                       {
                           #region Validations for ExchangeRate
                           if (dr["ExchangeRate"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Purchase.ExchangeRate = dr["ExchangeRate"].ToString();
                                       }
                                       if (Convert.ToString(result) == "ExchangeRate")
                                       {
                                           isIgnoreAll = true;
                                           Purchase.ExchangeRate = dr["ExchangeRate"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.ExchangeRate = dr["ExchangeRate"].ToString();
                                   }
                               }
                               else
                               {
                                   Purchase.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                               }
                           }

                           #endregion
                       }

                       #region if purchaseOrder is not null

                       OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                       OnlineEntities.Line Line = new OnlineEntities.Line();
                       OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                       OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                       OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                       OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                       if (dt.Columns.Contains("LineDescription"))
                       {
                           #region Validations of Associate
                           if (dr["LineDescription"].ToString() != string.Empty)
                           {
                               if (dr["LineDescription"].ToString().Length > 4000)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Line.LineDescription = dr["LineDescription"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Line.LineDescription = dr["LineDescription"].ToString();
                                   }
                               }
                               else
                               {
                                   Line.LineDescription = dr["LineDescription"].ToString();
                               }
                           }
                           #endregion
                       }
                       if (dt.Columns.Contains("LineAmount"))
                       {
                           #region Validations for LineAmount
                           if (dr["LineAmount"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Line.LineAmount = dr["LineAmount"].ToString();
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Line.LineAmount = dr["LineAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Line.LineAmount = dr["LineAmount"].ToString();
                                   }
                               }
                               else
                               {
                                   Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                               }
                           }

                           #endregion
                       }
                       if (dt.Columns.Contains("LineItemRef"))
                       {
                           #region Validations of LineItemRef
                           if (dr["LineItemRef"].ToString() != string.Empty)
                           {
                               if (dr["LineItemRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           ItemRef.Name = dr["LineItemRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemRef.Name = dr["LineItemRef"].ToString();
                                   }
                               }
                               else
                               {
                                   ItemRef.Name = dr["LineItemRef"].ToString();
                               }
                           }
                           #endregion
                       }


                       //bug 486
                       if (dt.Columns.Contains("SKU"))
                       {
                           #region Validations of SKU
                           if (dr["SKU"].ToString() != string.Empty)
                           {
                               if (dr["SKU"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           ItemRef.SKU = dr["SKU"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemRef.SKU = dr["SKU"].ToString();
                                   }
                               }
                               else
                               {
                                   ItemRef.SKU = dr["SKU"].ToString();
                               }
                           }
                           #endregion
                       }


                       if (dt.Columns.Contains("LineClassRef"))
                       {
                           #region Validations of LineClassRef
                           if (dr["LineClassRef"].ToString() != string.Empty)
                           {
                               if (dr["LineClassRef"].ToString().Length > 40)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           ClassRef.Name = dr["LineClassRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ClassRef.Name = dr["LineClassRef"].ToString();
                                   }
                               }
                               else
                               {
                                   ClassRef.Name = dr["LineClassRef"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("LineItemQty"))
                       {
                           #region Validations for LineItemQty
                           if (dr["LineItemQty"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                   }
                               }
                               else
                               {
                                   ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                               }
                           }

                           #endregion
                       }
                       if (dt.Columns.Contains("LineItemUnitPrice"))
                       {
                           #region Validations for LineItemUnitPrice
                           if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                   }
                               }
                               else
                               {
                                   ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                               }
                           }

                           #endregion
                       }

                       if (dt.Columns.Contains("LineItemTaxCodeRef"))
                       {
                           #region Validations of LineItemTaxCodeRef
                           if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                           {
                               if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                   }
                               }
                               else
                               {
                                   TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                               }
                           }
                           #endregion
                       }


                       if (dt.Columns.Contains("LineCustomerRef"))
                       {
                           #region Validations of LineCustomerRef
                           if (dr["LineCustomerRef"].ToString() != string.Empty)
                           {
                               if (dr["LineCustomerRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                   }
                               }
                               else
                               {
                                   CustomerRef.Name = dr["LineCustomerRef"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("LineBillableStatus"))
                       {
                           #region Validations of LineBillableStatus
                           if (dr["LineBillableStatus"].ToString() != string.Empty)
                           {
                               if (dr["LineBillableStatus"].ToString().Length > 40)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                   }
                               }
                               else
                               {
                                   ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (ItemRef.Name != null || ItemRef.SKU != null)
                       {
                           ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                       }
                       if (TaxCodeRef.Name != null)
                       {
                           ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                       }
                       if (ClassRef.Name != null)
                       {
                           ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                       }
                       if (CustomerRef.Name != null)
                       {
                           ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                       }
                       if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                       {
                           Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                       }

                       OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                       OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();
                       if (dt.Columns.Contains("LineAccountDescription"))
                       {
                           #region Validations of LineAccountDescription
                           if (dr["LineAccountDescription"].ToString() != string.Empty)
                           {
                               if (dr["LineAccountDescription"].ToString().Length > 40)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                   }
                               }
                               else
                               {
                                   AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("LineAccountAmount"))
                       {
                           #region Validations for LineAccountAmount
                           if (dr["LineAccountAmount"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                   }
                               }
                               else
                               {
                                   AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                               }
                           }

                           #endregion
                       }

                       OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                       if (dt.Columns.Contains("LineAccountCustomerRef"))
                       {
                           #region Validations of LineAccountCustomerRef
                           if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                           {
                               if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                   }
                               }
                               else
                               {
                                   CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                               }
                           }
                           #endregion
                       }
                       if (CustomerRef1.Name != null)
                       {
                           AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                       }

                       OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                       if (dt.Columns.Contains("LineAccountClassRef"))
                       {
                           #region Validations of LineAccountClassRef
                           if (dr["LineAccountClassRef"].ToString() != string.Empty)
                           {
                               if (dr["LineAccountClassRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                   }
                               }
                               else
                               {
                                   ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (ClassRef1.Name != null)
                       {
                           AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                       }

                       OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                       if (dt.Columns.Contains("LineAccountRef"))
                       {
                           #region Validations of LineAccountRef
                           if (dr["LineAccountRef"].ToString() != string.Empty)
                           {
                               if (dr["LineAccountRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           AccountRef.Name = dr["LineAccountRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountRef.Name = dr["LineAccountRef"].ToString();
                                   }
                               }
                               else
                               {
                                   AccountRef.Name = dr["LineAccountRef"].ToString();
                               }

                               // bug 480
                               #region Account Validation
                               List<Intuit.Ipp.Data.Account> Accountdata = null;
                               string AccountName = string.Empty;
                               AccountName = AccountRef.Name.Trim();
                               Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                               if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                               {
                                   //480
                                   Messages m1 = new Messages();
                                   m1.buttonCancel.Text = "Add";
                                   m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                   m1.ShowDialog();
                                   //Add
                                   if (m1.DialogResult.ToString().ToLower() == "ok")
                                   {
                                       CommonUtilities cUtility = new CommonUtilities();
                                       cUtility.CreateAccountInQBO(AccountName);
                                   }
                                   //Quit
                                   if (m1.DialogResult.ToString().ToLower() == "no")
                                   {
                                       break;
                                       //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                       //{
                                       //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                       //    bkWorker.CancelAsync();
                                       //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                       //}
                                       //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                       //{
                                       //    axisForm.IsBusy = false;
                                       //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                       //    bkWorker.CancelAsync();

                                       //}
                                       //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                       //{
                                       //    axisForm.IsBusy = false;
                                       //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                       //    bkWorker.CancelAsync();
                                       //}

                                       ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                       //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                   }
                                   //Ignore
                                   if (m1.DialogResult.ToString() == "Ignore")
                                   {

                                   }
                                   //IgnoreAll
                                   if (m1.DialogResult.ToString() == "Abort")
                                   {
                                       CommonUtilities.GetInstance().ignoreAllAccount = true;
                                   }
                               }


                               #endregion
                           }
                           #endregion
                       }

                       if (AccountRef.Name != null)
                       {
                           AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                       }
                       
                       if (dt.Columns.Contains("LineAccountBillableStatus"))
                       {
                           #region Validations of LineAccountBillableStatus
                           if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                           {
                               if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                   }
                               }
                               else
                               {
                                   AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                               }
                           }
                           #endregion
                       }

                       OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                       if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                       {
                           #region Validations of LineAccountTaxCodeRef
                           if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                           {
                               if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                   }
                               }
                               else
                               {
                                   TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (TaxCodeRef1.Name != null)
                       {
                           AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                       }

                       if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                           AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);


                       if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                       {
                           Purchase.Line.Add(AccountBasedExpenseLine);
                           Purchase.Line.Add(Line);
                       }
                       else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                       {
                           Purchase.Line.Add(AccountBasedExpenseLine);
                       }
                       else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                       {
                           Purchase.Line.Add(Line);
                       }
                       else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                       {
                           Purchase.Line.Add(Line);
                           Purchase.Line.Add(AccountBasedExpenseLine);
                       }
                       #endregion

                       OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                       OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                       OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                       OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                       if (dt.Columns.Contains("TxnTaxCodeRefName"))
                       {
                           #region Validations of TxnTaxCodeRefName
                           if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                           {
                               if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                   }
                               }
                               else
                               {
                                   TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("TotalTax"))
                       {
                           #region Validations for TotalTax
                           if (dr["TotalTax"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["TotalTax"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This TotalTax( " + dr["TotalTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {

                                           TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                       }
                                       if (Convert.ToString(result) == "TotalTax")
                                       {
                                           isIgnoreAll = true;
                                           TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();

                                       }
                                       else
                                       {
                                           TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TxnTaxDetail.TotalTax = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalTax"].ToString()));
                                   }
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("TaxLineAmount"))
                       {
                           #region Validations for TaxLineAmount
                           if (dr["TaxLineAmount"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["TaxLineAmount"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This TaxLineAmount( " + dr["TaxLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                       }
                                       if (Convert.ToString(result) == "TaxLineAmount")
                                       {
                                           isIgnoreAll = true;
                                           TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                   }
                               }
                               else
                               {
                                   TaxLine.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxLineAmount"].ToString()));
                               }
                           }

                           #endregion
                       }

                       if (dt.Columns.Contains("TaxPercentBased"))
                       {
                           #region Validations of IsActive
                           if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                           {

                               int result = 0;
                               if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                               {
                                   TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                               }
                               else
                               {
                                   string strvalid = string.Empty;
                                   if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                   {
                                       TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                   }
                                   else
                                   {
                                       if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                       {
                                           strvalid = "TaxPercentBased";
                                       }
                                       else
                                           TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                   }
                                   if (strvalid != string.Empty)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(results) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(results) == "Ignore")
                                           {
                                               TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                           }
                                           if (Convert.ToString(results) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                       }
                                   }

                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("TaxPercent"))
                       {
                           #region Validations for TaxPercent
                           if (dr["TaxPercent"].ToString() != string.Empty)
                           {
                               decimal amount;
                               if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {

                                           TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                       }
                                       if (Convert.ToString(result) == "TaxPercent")
                                       {
                                           isIgnoreAll = true;
                                           TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                   }
                               }
                               else
                               {
                                   TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                               }
                           }

                           #endregion
                       }

                       if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef.Count > 0)
                       {
                           TaxLine.TaxLineDetail.Add(TaxLineDetail);
                       }
                       if (TxnTaxCodeRef.Name != null)
                       {
                           TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                       }
                       if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                       {
                           TxnTaxDetail.TaxLine.Add(TaxLine);
                       }
                       if (TxnTaxDetail.TxnTaxCodeRef.Count != 0 || TxnTaxDetail.TaxLine.Count != 0)
                       {
                           Purchase.TxnTaxDetail.Add(TxnTaxDetail);
                       }

                       OnlineEntities.AccountRef AccountRef1 = new OnlineEntities.AccountRef();

                       if (dt.Columns.Contains("AccountRef"))
                       {
                           #region Validations of AccountRef
                           if (dr["AccountRef"].ToString() != string.Empty)
                           {
                               if (dr["AccountRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This AccountRef (" + dr["AccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           AccountRef1.Name = dr["AccountRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           AccountRef1.Name = dr["AccountRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       AccountRef1.Name = dr["AccountRef"].ToString();
                                   }
                               }
                               else
                               {
                                   AccountRef1.Name = dr["AccountRef"].ToString();
                               }

                               // bug 480
                               #region Account Validation
                               List<Intuit.Ipp.Data.Account> Accountdata = null;
                               string AccountName = string.Empty;
                               AccountName = AccountRef1.Name.Trim();
                               Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                               if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                               {
                                   //480
                                   Messages m1 = new Messages();
                                   m1.buttonCancel.Text = "Add";
                                   m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                   m1.ShowDialog();
                                   //Add
                                   if (m1.DialogResult.ToString().ToLower() == "ok")
                                   {
                                       CommonUtilities cUtility = new CommonUtilities();
                                      await cUtility.CreateAccountInQBO(AccountName);
                                   }
                                   //Quit
                                   if (m1.DialogResult.ToString().ToLower() == "no")
                                   {
                                       break;
                                       //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                       //{
                                       //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                       //    bkWorker.CancelAsync();
                                       //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                       //}
                                       //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                       //{
                                       //    axisForm.IsBusy = false;
                                       //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                       //    bkWorker.CancelAsync();

                                       //}
                                       //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                       //{
                                       //    axisForm.IsBusy = false;
                                       //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                       //    bkWorker.CancelAsync();
                                       //}

                                       ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                       //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                   }
                                   //Ignore
                                   if (m1.DialogResult.ToString() == "Ignore")
                                   {

                                   }
                                   //IgnoreAll
                                   if (m1.DialogResult.ToString() == "Abort")
                                   {
                                       CommonUtilities.GetInstance().ignoreAllAccount = true;
                                   }
                               }


                               #endregion
                           }
                           #endregion
                       }

                       if (AccountRef1.Name != null)
                       {
                           Purchase.AccountRef.Add(AccountRef1);
                       }
                       
                       OnlineEntities.Entity Entity = new OnlineEntities.Entity();

                       if (dt.Columns.Contains("EntityType"))
                       {
                           #region Validations of EntityType
                           if (dr["EntityType"].ToString() != string.Empty)
                           {
                               if (dr["EntityType"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This EntityType (" + dr["EntityType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Entity.Type = dr["EntityType"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Entity.Type = dr["EntityType"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Entity.Type = dr["EntityType"].ToString();
                                   }
                               }
                               else
                               {
                                   Entity.Type = dr["EntityType"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("EntityRef"))
                       {
                           #region Validations of EntityRef
                           if (dr["EntityRef"].ToString() != string.Empty)
                           {
                               if (dr["EntityRef"].ToString().Length > 100)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This EntityRef (" + dr["EntityRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Entity.Name = dr["EntityRef"].ToString().Substring(0, 100);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Entity.Name = dr["EntityRef"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Entity.Name = dr["EntityRef"].ToString();
                                   }
                               }
                               else
                               {
                                   Entity.Name = dr["EntityRef"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (Entity.Name != null)
                       {
                           Purchase.Entity.Add(Entity);
                       }

                       if (dt.Columns.Contains("Credit"))
                       {
                           #region Validations of Credit
                           if (dr["Credit"].ToString() != string.Empty)
                           {
                               if (dr["Credit"].ToString().Length > 10)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This Credit (" + dr["Credit"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Purchase.Credit = dr["Credit"].ToString().Substring(0, 10);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Purchase.Credit = dr["Credit"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.Credit = dr["Credit"].ToString();
                                   }
                               }
                               else
                               {
                                   Purchase.Credit = dr["Credit"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("PrintStatus"))
                       {
                           #region Validations of PrintStatus
                           if (dr["PrintStatus"].ToString() != string.Empty)
                           {
                               if (dr["PrintStatus"].ToString().Length > 20)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This PrintStatus (" + dr["PrintStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Purchase.PrintStatus = dr["PrintStatus"].ToString().Substring(0, 20);

                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Purchase.PrintStatus = dr["PrintStatus"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.PrintStatus = dr["PrintStatus"].ToString();
                                   }
                               }
                               else
                               {
                                   Purchase.PrintStatus = dr["PrintStatus"].ToString();
                               }
                           }
                           #endregion
                       }

                       if (dt.Columns.Contains("GlobalTaxCalculation"))
                       {
                           #region Validations of GlobalTaxCalculation
                           if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                           {
                               if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                               {
                                   if (isIgnoreAll == false)
                                   {
                                       string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                       DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                       if (Convert.ToString(result) == "Cancel")
                                       {
                                           continue;
                                       }
                                       if (Convert.ToString(result) == "No")
                                       {
                                           return null;
                                       }
                                       if (Convert.ToString(result) == "Ignore")
                                       {
                                           Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                       }
                                       if (Convert.ToString(result) == "Abort")
                                       {
                                           isIgnoreAll = true;
                                           Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                   }
                               }
                               else
                               {
                                   Purchase.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                               }
                           }
                           #endregion
                       }

                        if (dt.Columns.Contains("TransactionLocationType"))
                        {
                            #region Validations of TransactionLocationType
                            if (dr["TransactionLocationType"].ToString() != string.Empty)
                            {
                                if (dr["TransactionLocationType"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                else
                                {
                                    Purchase.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                }
                            }
                            #endregion
                        }

                        #endregion

                        coll.Add(Purchase);
                   }
               }
               else
               {
                   return null;
               }
           }
           #endregion

           #endregion

           return coll;

       }
    }
}
