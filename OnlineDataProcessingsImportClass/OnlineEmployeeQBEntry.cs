﻿
using System;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using System.Net;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("Employee", Namespace = "", IsNullable = false)]

    public class Employee
    {        

        #region member
        private string m_EmployeeNumber;
        private string m_Organization;
        private string m_Title;
        private string m_GivenName;
        private string m_MiddleName;
        private string m_FamilyName;
        private string m_Suffix;
        private string m_DisplayName;
        private string m_PrintOnCheckName;
        private string m_Active;
        private Collection<OnlineEntities.TelephoneNumber> m_PrimaryPhone = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.TelephoneNumber> m_Mobile = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.EmailAddressEmployee> m_PrimaryEmailAddr = new Collection<OnlineEntities.EmailAddressEmployee>();
       
        private string m_SSN; 
        private Collection<OnlineEntities.BillAddr> m_PrimaryAddr = new Collection<OnlineEntities.BillAddr>();
       
        private string m_BillableTime;
        private string m_BillRate;
        private string m_BirthDate;
        private string m_Gender;
        private string m_HiredDate;
        private string m_ReleasedDate;

        //594
        private bool m_isAppend;
        #endregion

        #region Constructor
        public Employee()
        {
        }
        #endregion

        #region properties
        public string EmployeeNumber
        {
            get { return m_EmployeeNumber; }
            set { m_EmployeeNumber = value; }
        }
        public string Organization
        {
            get { return m_Organization; }
            set { m_Organization = value; }
        }
        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }        
        public string GivenName
        {
            get { return m_GivenName; }
            set { m_GivenName = value; }
        }
        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }
        public string FamilyName
        {
            get { return m_FamilyName; }
            set { m_FamilyName = value; }
        }
        public string Suffix
        {
            get { return m_Suffix; }
            set { m_Suffix = value; }
        }
        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }
        public string PrintOnCheckName
        {
            get { return m_PrintOnCheckName; }
            set { m_PrintOnCheckName = value; }
        }
        public string Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }

        public Collection<OnlineEntities.TelephoneNumber> PrimaryPhone
        {
            get { return m_PrimaryPhone; }
            set { m_PrimaryPhone = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> Mobile
        {
            get { return m_Mobile; }
            set { m_Mobile = value; }
        }
        public Collection<OnlineEntities.EmailAddressEmployee> PrimaryEmailAddr
        {
            get { return m_PrimaryEmailAddr; }
            set { m_PrimaryEmailAddr = value; }
        }
        public Collection<OnlineEntities.BillAddr> PrimaryAddr
        {
            get { return m_PrimaryAddr; }
            set { m_PrimaryAddr = value; }
        }
               
        public string SSN
        {
            get { return m_SSN; }
            set { m_SSN = value; }
        }
              
        public string BillableTime
        {
            get { return m_BillableTime; }
            set { m_BillableTime = value; }
        }
        public string BillRate
        {
            get { return m_BillRate; }
            set { m_BillRate = value; }
        }
        public string BirthDate
        {
            get { return m_BirthDate; }
            set { m_BirthDate = value; }
        }
        public string Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }
        public string HiredDate
        {
            get { return m_HiredDate; }
            set { m_HiredDate = value; }
        }
        public string ReleasedDate
        {
            get { return m_ReleasedDate; }
            set { m_ReleasedDate = value; }
        }
        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region  Public Methods
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
        /// <summary>
        /// Creating new Employee entry in quickbook online
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>

        public async System.Threading.Tasks.Task<bool> CreateQBOEmployee(OnlineDataProcessingsImportClass.Employee coll)
        {
           
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.Employee employeeAdded = null;
            Intuit.Ipp.Data.Employee addedEmployee = new Intuit.Ipp.Data.Employee();
            Employee employee = new Employee();
            employee = coll;
          
            try
            {
                #region Add Employee

                if (employee.EmployeeNumber != null)
                {
                    addedEmployee.EmployeeNumber = employee.EmployeeNumber;
                }
                if (employee.Title != null)
                {
                    addedEmployee.Title = employee.Title;
                }
                if (employee.GivenName != null)
                {
                    addedEmployee.GivenName = employee.GivenName;
                }
                if (employee.MiddleName != null)
                {                   
                    addedEmployee.MiddleName = employee.MiddleName;
                }
                if (employee.FamilyName != null)
                {
                    addedEmployee.FamilyName = employee.FamilyName;
                }
                if (employee.Suffix != null)
                {
                    addedEmployee.Suffix = employee.Suffix;                   
                }
                if (employee.DisplayName != null)
                {
                    addedEmployee.DisplayName = employee.DisplayName;
                }
                if (employee.SSN != null)
                {
                    addedEmployee.SSN = employee.SSN;
                }
                if (employee.PrintOnCheckName != null)
                {
                    addedEmployee.PrintOnCheckName = employee.PrintOnCheckName;
                }
                if (employee.Gender == Intuit.Ipp.Data.gender.Female.ToString())
                {
                    addedEmployee.Gender = Intuit.Ipp.Data.gender.Female ;
                }
               else if (employee.Gender == Intuit.Ipp.Data.gender.Male.ToString())
                {
                    addedEmployee.Gender = Intuit.Ipp.Data.gender.Male;
                }
                if (employee.BirthDate != null)
                {
                    try
                    {
                        addedEmployee.BirthDate = Convert.ToDateTime(employee.BirthDate);
                    }
                    catch { }
                }
                if (employee.HiredDate != null)
                {
                    try
                    {
                        addedEmployee.HiredDate = Convert.ToDateTime(employee.HiredDate);
                    }
                    catch { }
                }
                if (employee.ReleasedDate != null)
                {
                    try
                    {
                    addedEmployee.ReleasedDate =Convert.ToDateTime(employee.ReleasedDate);
                    }
                    catch { }
                }
                if (employee.Organization != null)
                {
                    addedEmployee.Organization =Convert.ToBoolean(employee.Organization);
                }
                if (employee.Active != null)
                {
                    addedEmployee.Active =Convert.ToBoolean(employee.Active);
                }
                if (employee.BillableTime != null)
                {
                    addedEmployee.BillableTime =Convert.ToBoolean(employee.BillableTime);
                }
                Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (employee.PrimaryPhone.Count > 0)
                {
                    foreach (var primaryPhone in employee.PrimaryPhone)
                    {
                        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                    }
                    addedEmployee.PrimaryPhone = primaryPhoneNo;
                }

                Intuit.Ipp.Data.TelephoneNumber mobileNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (employee.Mobile.Count > 0)
                {
                    foreach (var mobile in employee.Mobile)
                    {
                        mobileNo.FreeFormNumber = mobile.Mobile;
                    }
                    addedEmployee.Mobile = mobileNo;
                }

                Intuit.Ipp.Data.EmailAddress emailAddress = new Intuit.Ipp.Data.EmailAddress();
                if (employee.PrimaryEmailAddr.Count > 0)
                {
                    foreach (var emailAddr in employee.PrimaryEmailAddr)
                    {
                        emailAddress.Address = emailAddr.EmailAddress;
                    }
                    addedEmployee.PrimaryEmailAddr = emailAddress;
                }
               
                if (employee.BillRate != null)
                {
                    addedEmployee.BillRate =Convert.ToDecimal(employee.BillRate);
                }

                PhysicalAddress primaryAddr = new PhysicalAddress();
                if (employee.PrimaryAddr.Count > 0)
                {
                    foreach (var address in employee.PrimaryAddr)
                    {
                        primaryAddr.Line1 = address.BillLine1;
                        primaryAddr.Line2 = address.BillLine2;
                        primaryAddr.Line3 = address.BillLine3;
                        primaryAddr.City = address.BillCity;
                        primaryAddr.Country = address.BillCountry;
                        primaryAddr.CountrySubDivisionCode = address.BillCountrySubDivisionCode;
                        primaryAddr.PostalCode = address.BillPostalCode;
                        primaryAddr.Note = address.BillNote;
                        primaryAddr.Line4 = address.BillLine4;
                        primaryAddr.Line5 = address.BillLine5;
                        primaryAddr.Lat = address.BillAddrLat;
                        primaryAddr.Long = address.BillAddrLong;
                    }

                    addedEmployee.PrimaryAddr = primaryAddr;
                }               
              
                #endregion
                employeeAdded = new Intuit.Ipp.Data.Employee();
                employeeAdded = dataService.Add<Intuit.Ipp.Data.Employee>(addedEmployee);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (employeeAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = employeeAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = employeeAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        /// <summary>
        ///  updating Employee entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="employeeid"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOEmployee(OnlineDataProcessingsImportClass.Employee coll, string employeeid, string syncToken, Intuit.Ipp.Data.Employee PreviousData = null)
        {
          
            Intuit.Ipp.Data.Employee emplyeeAdded = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.Employee addedEmployee = new Intuit.Ipp.Data.Employee();
            Employee employee = new Employee();
            employee = coll;
         
            try
            {              

                #region Add Employee

               
				 //594 PreviousData is used to maintain previous records and append new data if changes are applied ,Lines will be appended to next line directly	
                if (employee.isAppend == true)
                {
                    addedEmployee = PreviousData;
                    addedEmployee.sparse = true;
                    addedEmployee.sparseSpecified = true;
                }
				 addedEmployee.Id = employeeid;
                addedEmployee.SyncToken = syncToken;
                if (employee.EmployeeNumber != null)
                {
                    addedEmployee.EmployeeNumber = employee.EmployeeNumber;
                }
                if (employee.Title != null)
                {
                    addedEmployee.Title = employee.Title;
                }
                if (employee.GivenName != null)
                {
                    addedEmployee.GivenName = employee.GivenName;
                }
                if (employee.MiddleName != null)
                {
                    addedEmployee.MiddleName = employee.MiddleName;
                }
                if (employee.FamilyName != null)
                {
                    addedEmployee.FamilyName = employee.FamilyName;
                }
                if (employee.Suffix != null)
                {
                    addedEmployee.Suffix = employee.Suffix;
                }
                if (employee.DisplayName != null)
                {
                    addedEmployee.DisplayName = employee.DisplayName;
                }
                if (employee.SSN != null)
                {
                    addedEmployee.SSN = employee.SSN;
                }
                if (employee.PrintOnCheckName != null)
                {
                    addedEmployee.PrintOnCheckName = employee.PrintOnCheckName;
                }
                if (employee.Gender == Intuit.Ipp.Data.gender.Female.ToString())
                {
                    addedEmployee.Gender = Intuit.Ipp.Data.gender.Female;
                }
                else if (employee.Gender == Intuit.Ipp.Data.gender.Male.ToString())
                {
                    addedEmployee.Gender = Intuit.Ipp.Data.gender.Male;
                }
                if (employee.BirthDate != null)
                {
                    try {
                    addedEmployee.BirthDate = Convert.ToDateTime(employee.BirthDate);
                    }
                    catch { }
                }
                if (employee.HiredDate != null)
                {
                    try
                    {
                        addedEmployee.HiredDate = Convert.ToDateTime(employee.HiredDate);
                    }
                    catch { }
                }
                if (employee.ReleasedDate != null)
                {
                    try
                    {
                        addedEmployee.ReleasedDate = Convert.ToDateTime(employee.ReleasedDate);
                    }
                    catch { }
                }
                if (employee.Organization != null)
                {
                    addedEmployee.Organization = Convert.ToBoolean(employee.Organization);
                }
                if (employee.Active != null)
                {
                    addedEmployee.Active = Convert.ToBoolean(employee.Active);
                }
                if (employee.BillableTime != null)
                {
                    addedEmployee.BillableTime = Convert.ToBoolean(employee.BillableTime);
                }
                Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (employee.PrimaryPhone.Count > 0)
                {
                    foreach (var primaryPhone in employee.PrimaryPhone)
                    {
                        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                    }
                    addedEmployee.PrimaryPhone = primaryPhoneNo;
                }

                Intuit.Ipp.Data.TelephoneNumber mobileNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (employee.Mobile.Count > 0)
                {
                    foreach (var mobile in employee.Mobile)
                    {
                        mobileNo.FreeFormNumber = mobile.Mobile;
                    }
                    addedEmployee.Mobile = mobileNo;
                }
                Intuit.Ipp.Data.EmailAddress emailAddress = new Intuit.Ipp.Data.EmailAddress();
                if (employee.PrimaryEmailAddr.Count > 0)
                {
                    foreach (var emailAddr in employee.PrimaryEmailAddr)
                    {
                        emailAddress.Address = emailAddr.EmailAddress;
                    }
                    addedEmployee.PrimaryEmailAddr = emailAddress;
                }

                if (employee.BillRate != null)
                {
                    addedEmployee.BillRate = Convert.ToDecimal(employee.BillRate);
                }

                PhysicalAddress primaryAddr = new PhysicalAddress();
                if (employee.PrimaryAddr.Count > 0)
                {
                    foreach (var address in employee.PrimaryAddr)
                    {
                        primaryAddr.Line1 = address.BillLine1;
                        primaryAddr.Line2 = address.BillLine2;
                        primaryAddr.Line3 = address.BillLine3;
                        primaryAddr.City = address.BillCity;
                        primaryAddr.Country = address.BillCountry;
                        primaryAddr.CountrySubDivisionCode = address.BillCountrySubDivisionCode;
                        primaryAddr.PostalCode = address.BillPostalCode;
                        primaryAddr.Note = address.BillNote;
                        primaryAddr.Line4 = address.BillLine4;
                        primaryAddr.Line5 = address.BillLine5;
                        primaryAddr.Lat = address.BillAddrLat;
                        primaryAddr.Long = address.BillAddrLong;
                    }

                    addedEmployee.PrimaryAddr = primaryAddr;
                }

                #endregion

                emplyeeAdded = new Intuit.Ipp.Data.Employee();
                emplyeeAdded = dataService.Update<Intuit.Ipp.Data.Employee>(addedEmployee);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (emplyeeAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = emplyeeAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = emplyeeAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
      
        #endregion
    }
    /// <summary>
    /// checking emp number is present or not in quickbook online for Employee transaction.
    /// </summary>
    public class OnlineEmployeeQBEntryCollection : Collection<Employee>
    {
        public Employee FindEmployeeNumberEntry(string employeeNumber)
        {
            foreach (Employee item in this)
            {
                if (item.EmployeeNumber == employeeNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }
    
}
