﻿using System;
using System.Collections.ObjectModel;
using Intuit.Ipp.DataService;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using OnlineEntities;
using System.Net;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class OnlineEstimateQBEntry
   {
       #region Member
       private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TxnDate;
        private string m_TxnStatus;
        private string m_CustomerMemo;
        private Collection<OnlineEntities.CustomField> m_CustomField = new Collection<OnlineEntities.CustomField>();
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.ClassRef> m_ClassRef = new Collection<OnlineEntities.ClassRef>();
        private Collection<OnlineEntities.CustomerRef> m_CustomerRef = new Collection<OnlineEntities.CustomerRef>();
        private Collection<OnlineEntities.SalesTermRef> m_SalesTermRef = new Collection<OnlineEntities.SalesTermRef>();      
        private string m_ShipDate;
        private Collection<OnlineEntities.BillAddr> m_BillAddr = new Collection<OnlineEntities.BillAddr>();
        private Collection<OnlineEntities.ShipAddr> m_ShipAddr = new Collection<OnlineEntities.ShipAddr>();
        private Collection<OnlineEntities.ShipMethodRef> m_ShipMethodRef = new Collection<OnlineEntities.ShipMethodRef>();
        private string m_ApplyTaxAfterDiscount;
        private string m_PrintStatus;
       //606
        private string m_PrimaryPhone;
        private string m_EmailStatus;
        private string m_BillEmail;
       //606
        //private Collection<OnlineEntities.TelephoneNumber> m_PrimaryPhone = new Collection<OnlineEntities.TelephoneNumber>();
        //private string m_PrimaryPhone;

        private string m_Expiration;      
        private string m_AcceptedBy;       
        private string m_AcceptedDate;
        private string m_GlobalTaxCalculation;
        private string m_TransactionLocationType;

        //594
        private bool m_isAppend;
        private bool m_Sparse;
        public bool Sparse
        {
            get { return m_Sparse; }
            set { m_Sparse = value; }
        }
       

       
       #endregion

       #region properties

        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }
       /// <summary>
       /// new adding Currency
       /// </summary>
        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }
        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }
        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public Collection<OnlineEntities.SalesTermRef> SalesTermRef
        {
            get { return m_SalesTermRef; }
            set { m_SalesTermRef = value; }
        }
        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public Collection<OnlineEntities.CustomField> CustomField
        {
            get { return m_CustomField; }
            set { m_CustomField = value; }
        }


        public string TxnStatus
        {
            get { return m_TxnStatus; }
            set { m_TxnStatus = value; }
        }


        public string CustomerMemo
        {
            get { return m_CustomerMemo; }
            set { m_CustomerMemo = value; }
        }

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }


        public Collection<ShipMethodRef> ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }    


        public string PrintStatus
        {
            get { return m_PrintStatus; }
            set { m_PrintStatus = value; }
        }


        public string EmailStatus
        {
            get { return m_EmailStatus; }
            set { m_EmailStatus = value; }
        }


        public string BillEmail
        {
            get { return m_BillEmail; }
            set { m_BillEmail = value; }
        }

       //606
        public string PrimaryPhone
        {
            get { return m_PrimaryPhone; }
            set { m_PrimaryPhone = value; }
        }
        //public string PrimaryPhone
        //{
        //    get { return m_PrimaryPhone; }
        //    set { m_PrimaryPhone = value; }
        //}
       
        public string ApplyTaxAfterDiscount
        {
            get { return m_ApplyTaxAfterDiscount; }
            set { m_ApplyTaxAfterDiscount = value; }
        }

        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        public Collection<OnlineEntities.CustomerRef> CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public Collection<BillAddr> BillAddr
        {
            get { return m_BillAddr; }
            set { m_BillAddr = value; }
        }


        public Collection<ShipAddr> ShipAddr
        {
            get { return m_ShipAddr; }
            set { m_ShipAddr = value; }
        }

        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }
        }
        public string Expiration
        {
            get { return m_Expiration; }
            set { m_Expiration = value; }
        }        

        public string AcceptedBy
        {
            get { return m_AcceptedBy; }
            set { m_AcceptedBy = value; }
        }    

        public string AcceptedDate
        {
            get { return m_AcceptedDate; }
            set { m_AcceptedDate = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

       #region  Public Methods
       /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
       /// </summary>
       /// <param name="lines"></param>
       /// <param name="line"></param>
       /// <param name="linecount"></param>
       /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
       
       /// <summary>
        /// Creating new Estimate transaction in quickbook online. 
       /// </summary>
       /// <param name="coll"></param>
       /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOEstimate(OnlineDataProcessingsImportClass.OnlineEstimateQBEntry coll)
        {
            bool flag = false;
            int linecount = 1;

            DataService dataService = null;
            Intuit.Ipp.Data.Estimate estimateAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.Estimate addedEstimate = new Intuit.Ipp.Data.Estimate();
            OnlineEstimateQBEntry estimate = new OnlineEstimateQBEntry();
            estimate = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;

            //bug 514
            bool taxFlag = false;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag


            try
            {
                #region Add Estimate

                //594
                if (estimate.isAppend == true)
                {
                    addedEstimate.sparse = true;
                }

                if (estimate.DocNumber != null)
                {
                    addedEstimate.DocNumber = estimate.DocNumber;
                }
                else { addedEstimate.AutoDocNumber = true; addedEstimate.AutoDocNumberSpecified = true; }

                if (estimate.TxnDate != null)
                {
                    addedEstimate.TxnDate = Convert.ToDateTime(estimate.TxnDate);
                    addedEstimate.TxnDateSpecified = true;
                }

                // Axis 742
                if (estimate.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(estimate.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedEstimate.TransactionLocationType = value;
                    }
                    else
                    {
                        addedEstimate.TransactionLocationType = estimate.TransactionLocationType;
                    }
                }

                foreach (var departmentData in estimate.DepartmentRef)
                {
                    string customervalue = string.Empty;
                    if (departmentData.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetDepartmentExistsInOnlineQBO(departmentData.Name.Trim());

                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }
                        addedEstimate.DepartmentRef = new ReferenceType()
                        {
                            name = departmentData.Name,
                            Value = customervalue
                        };
                    }
                }

                addedEstimate.PrivateNote = estimate.PrivateNote;

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();             
                foreach (var customData in estimate.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }              
                addedEstimate.CustomField = custom_online;
                #endregion

                if (estimate.TxnStatus != null)
                {
                    addedEstimate.TxnStatus = estimate.TxnStatus;

                }
                #region GlobalTaxCalculation

                if (estimate.GlobalTaxCalculation != null)
                {
                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedEstimate.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedEstimate.GlobalTaxCalculationSpecified = true;
                        }
                        else if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedEstimate.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedEstimate.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedEstimate.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedEstimate.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion              
                foreach (var l in estimate.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                              
                              
                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }
                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name !=null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                       // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                               // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount),2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                           // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                    lineSalesItemLineDetail.ServiceDateSpecified = true;
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                            string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;

                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                 
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedEstimate.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }                               
                                
                                  // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{

                                    foreach (var i in SalesItemLine.ItemRef)
                                    {
                                        string id = string.Empty;
                                        string name = string.Empty;
                                        if (CommonUtilities.GetInstance().SKULookup == false)
                                        {
                                            if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.SKU,
                                                    Value = id
                                                };
                                            }


                                            else if (i.Name != null)
                                            {
                                                if (i.Name != "SHIPPING_ITEM_ID")
                                                {
                                                    id = string.Empty;
                                                    var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                    foreach (var item in dataItem)
                                                    {
                                                        id = item.Id;
                                                    }
                                                    lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                    {
                                                        name = i.Name,
                                                        Value = id
                                                    };
                                                }
                                                else if (i.Name == "SHIPPING_ITEM_ID")
                                                {
                                                    lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                    {
                                                        Value = "SHIPPING_ITEM_ID"
                                                    };
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (var j in SalesItemLine.ItemRef)
                                            {
                                                if (j.SKU != null)
                                                {
                                                    var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                    foreach (var item in dataItem)
                                                    {
                                                        id = item.Id;
                                                        name = item.FullyQualifiedName;
                                                    }
                                                    lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                    {
                                                        name = name,
                                                        Value = id
                                                    };
                                                }
                                            }

                                        }
                                    }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name !=null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                      
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                    lineSalesItemLineDetail.ServiceDateSpecified = true;
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                               
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;                            

                            }
                        }
                        addedEstimate.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                //BUG ;; 586

                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in estimate.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }

                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedEstimate.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US" && CommonUtilities.GetInstance().GrossNet == true && addedEstimate.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && estimate.GlobalTaxCalculation != null)
                {
                    #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                              
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());

                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;


                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;

                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);

                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedEstimate.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                }
                //else if (CommonUtilities.GetInstance().CountryVersion == "US" )
                //{
                //    foreach (var txntaxCode in estimate.TxnTaxDetail)
                //    {
                //        #region TxnTaxDetail
                //        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                //        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                //        if (txntaxCode.TxnTaxCodeRef != null)
                //        {
                //            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                //            {
                //                TaxCode TaxCode = new TaxCode();
                //                TaxCode.Name = taxRef.Name;
                //                string Taxvalue = string.Empty;
                //                string TaxName = string.Empty;
                //                TaxName = TaxCode.Name;
                //                if (TaxCode.Name != null)
                //                {
                //                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                //                    if (dataTax != null)
                //                    {
                //                        foreach (var tax in dataTax)
                //                        {
                //                            Taxvalue = tax.Id;
                //                        }

                //                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                //                        {
                //                            name = TaxName,
                //                            Value = Taxvalue
                //                        };
                //                    }
                //                }

                //            }
                //        }


                //        if (txnTaxDetail != null)
                //        {
                //            addedEstimate.TxnTaxDetail = txnTaxDetail;
                //        }

                //        #endregion
                //    }
                //}

                #endregion

                foreach (var customerRef in estimate.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());

                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }

                        addedEstimate.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }
                }
                
                if (estimate.CustomerMemo != null)
                {
                    addedEstimate.CustomerMemo = new MemoRef()
                    {
                        Value = estimate.CustomerMemo
                    };
                }
                PhysicalAddress billAddr = new PhysicalAddress();
                if (estimate.BillAddr.Count > 0)
                {
                    foreach (var billaddress in estimate.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedEstimate.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (estimate.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in estimate.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedEstimate.ShipAddr = shipAddr;
                }

                
                    foreach (var className in estimate.ClassRef)
                    {
                        string classValue = string.Empty;
                        if (className.Name != null)
                        {
                           var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());
                       
                            foreach (var data in dataClass)
                            {
                                classValue = data.Id;
                            }
                            addedEstimate.ClassRef = new ReferenceType()
                            {
                                name = className.Name,
                                Value = classValue
                            };
                        }
                    }


                foreach (var salesTermRef in estimate.SalesTermRef)
                {
                    string classValue = string.Empty;
                    if (salesTermRef.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(salesTermRef.Name.Trim());

                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedEstimate.SalesTermRef = new ReferenceType()
                        {
                            name = salesTermRef.Name,
                            Value = classValue
                        };
                    }
                }            

                if (estimate.ShipMethodRef.Count>0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in estimate.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedEstimate.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (estimate.ShipDate != null)
                {
                    addedEstimate.ShipDate = Convert.ToDateTime(estimate.ShipDate);
                    addedEstimate.ShipDateSpecified = true;
                }             

                if (estimate.PrintStatus != null)
                {
                    if (estimate.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedEstimate.PrintStatus = PrintStatusEnum.NotSet;
                        addedEstimate.PrintStatusSpecified = true;
                    }
                    else if (estimate.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedEstimate.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedEstimate.PrintStatusSpecified = true;
                    }
                    else if (estimate.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedEstimate.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedEstimate.PrintStatusSpecified = true;
                    }
                }
                ////606
                //Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                //if (estimate.PrimaryPhone.Count > 0)
                //{
                //    foreach (var primaryPhone in estimate.PrimaryPhone)
                //    {
                //        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                //    }
                //    addedEstimate.PrimaryPhone = primaryPhoneNo;
                //}

                //OnlineEntities.PhoneClass phno = new OnlineEntities.PhoneClass();

                //606
                //if (estimate.PrimaryPhone.Count > 0)
                //{
                //    addedEstimate.AnyIntuitObject = estimate.PrimaryPhone;                    
                //}

                if (estimate.EmailStatus != null)
                {
                    if (estimate.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedEstimate.EmailStatus = EmailStatusEnum.NotSet;
                        addedEstimate.EmailStatusSpecified = true;
                    }
                    else if (estimate.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedEstimate.EmailStatus = EmailStatusEnum.EmailSent;
                        addedEstimate.EmailStatusSpecified = true;
                    }
                    else if (estimate.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedEstimate.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedEstimate.EmailStatusSpecified = true;
                    }
                }
                if (estimate.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = estimate.BillEmail;
                    addedEstimate.BillEmail = EmailAddress;
                }
                //606
                //Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                //if (estimate.PrimaryPhone.Count > 0)
                //{
                //    foreach (var primaryPhone in estimate.PrimaryPhone)
                //    {
                //        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                //    }
                //    addedEstimate.PrimaryPhone = primaryPhoneNo;
                //}
                
                if (estimate.ApplyTaxAfterDiscount != null)
                {
                    addedEstimate.ApplyTaxAfterDiscount = Convert.ToBoolean(estimate.ApplyTaxAfterDiscount);
                    addedEstimate.ApplyTaxAfterDiscountSpecified = true;
                }                

                if (estimate.Expiration != null)
                {
                    addedEstimate.ExpirationDate = Convert.ToDateTime(estimate.Expiration);
                    addedEstimate.ExpirationDateSpecified = true;
                }
                if (estimate.AcceptedBy != null)
                {
                    addedEstimate.AcceptedBy = estimate.AcceptedBy;                   
                }

                if (estimate.AcceptedDate != null)
                {
                    addedEstimate.AcceptedDate = Convert.ToDateTime(estimate.AcceptedDate);
                    addedEstimate.AcceptedDateSpecified = true;
                }

                
                
                
                if (estimate.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var Data in estimate.CurrencyRef)
                    {
                        CurrencyRefName = Data.Name;
                    }
                    addedEstimate.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }
                #endregion

                CommonUtilities.GetInstance().newSKUItem = false;
                estimateAdded = new Intuit.Ipp.Data.Estimate();
                estimateAdded = dataService.Add<Intuit.Ipp.Data.Estimate>(addedEstimate);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (estimateAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = estimateAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = estimateAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

       /// <summary>
        /// updating Estimate entry in quickbook online by the transaction Id..
       /// </summary>
       /// <param name="coll"></param>
       /// <param name="estimateid"></param>
       /// <param name="syncToken"></param>
       /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOEstimate(OnlineDataProcessingsImportClass.OnlineEstimateQBEntry coll, string estimateid, string syncToken, Intuit.Ipp.Data.Estimate PreviousData = null)
        {
            bool flag = false;
            int linecount = 1;

            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.Estimate estimateAdded = null;

            Intuit.Ipp.Data.Estimate addedEstimate = new Intuit.Ipp.Data.Estimate();
            OnlineEstimateQBEntry estimate = new OnlineEstimateQBEntry();
            estimate = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
           
            //bug 514
            bool taxFlag = false;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag
            try
            {               

                #region Add Estimate

                //594
                if (estimate.isAppend == true)
                {
                    addedEstimate = PreviousData;

                    if (addedEstimate.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedEstimate.Line.Length;
                        lines_online = addedEstimate.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                    addedEstimate.sparse = true;
                    addedEstimate.sparseSpecified = true;
                }

                addedEstimate.Id = estimateid;
                addedEstimate.SyncToken = syncToken;                

                if (estimate.DocNumber != null)
                {
                    addedEstimate.DocNumber = estimate.DocNumber;
                }              

                if (estimate.TxnDate != null)
                {
                    addedEstimate.TxnDate = Convert.ToDateTime(estimate.TxnDate);
                    addedEstimate.TxnDateSpecified = true;
                }

                // Axis 742
                if (estimate.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(estimate.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedEstimate.TransactionLocationType = value;
                    }
                    else
                    {
                        addedEstimate.TransactionLocationType = estimate.TransactionLocationType;
                    }
                }

                foreach (var departmentData in estimate.DepartmentRef)
                {
                    string customervalue = string.Empty;
                    if (departmentData.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetDepartmentExistsInOnlineQBO(departmentData.Name.Trim());

                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }
                        addedEstimate.DepartmentRef = new ReferenceType()
                        {
                            name = departmentData.Name,
                            Value = customervalue
                        };
                    }
                }

                addedEstimate.PrivateNote = estimate.PrivateNote;

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in estimate.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
              
                addedEstimate.CustomField = custom_online;
                #endregion

                if (estimate.TxnStatus != null)
                {
                    addedEstimate.TxnStatus = estimate.TxnStatus;

                }
                #region GlobalTaxCalculation

                if (estimate.GlobalTaxCalculation != null)
                {
                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedEstimate.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedEstimate.GlobalTaxCalculationSpecified = true;
                        }
                        else if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedEstimate.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedEstimate.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedEstimate.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedEstimate.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion              
                foreach (var l in estimate.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }


                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    string id = string.Empty;
                                  
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                    lineSalesItemLineDetail.ServiceDateSpecified = true;
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }


                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;

                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedEstimate.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var SalesItemLine in l.SalesItemLineDetail)
                            {
                                if (SalesItemLine != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{

                                foreach (var i in SalesItemLine.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in SalesItemLine.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }
                                foreach (var i in SalesItemLine.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (estimate.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (SalesItemLine.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(SalesItemLine.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(SalesItemLine.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (SalesItemLine.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(SalesItemLine.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (SalesItemLine.ServiceDate != null)
                                {
                                    lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(SalesItemLine.ServiceDate);
                                    lineSalesItemLineDetail.ServiceDateSpecified = true;
                                }
                                foreach (var taxCoderef in SalesItemLine.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != null && account.Name != string.Empty && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedEstimate.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }
              
                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US" && CommonUtilities.GetInstance().GrossNet == true && addedEstimate.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && estimate.GlobalTaxCalculation != null)
                {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                               
                               var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
               
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;


                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;

                                addedEstimate.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)
                }
			 //BUG ;; 586
              if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in estimate.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }

                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedEstimate.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                #endregion

                foreach (var customerRef in estimate.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }

                        addedEstimate.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }
                }

                if (estimate.CustomerMemo != null)
                {
                    addedEstimate.CustomerMemo = new MemoRef()
                    {
                        Value = estimate.CustomerMemo
                    };
                }
                PhysicalAddress billAddr = new PhysicalAddress();
                if (estimate.BillAddr.Count > 0)
                {
                    foreach (var billaddress in estimate.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedEstimate.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (estimate.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in estimate.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedEstimate.ShipAddr = shipAddr;
                }


                foreach (var className in estimate.ClassRef)
                {
                    string classValue = string.Empty;
                  
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());

                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedEstimate.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }



                foreach (var salesTermRef in estimate.SalesTermRef)
                {
                    string classValue = string.Empty;
                   
                    if (salesTermRef.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(salesTermRef.Name.Trim());
                      
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedEstimate.SalesTermRef = new ReferenceType()
                        {
                            name = salesTermRef.Name,
                            Value = classValue
                        };
                    }
                }

                if (estimate.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in estimate.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedEstimate.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (estimate.ShipDate != null)
                {
                    addedEstimate.ShipDate = Convert.ToDateTime(estimate.ShipDate);
                    addedEstimate.ShipDateSpecified = true;
                }

                if (estimate.PrintStatus != null)
                {
                    if (estimate.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedEstimate.PrintStatus = PrintStatusEnum.NotSet;
                        addedEstimate.PrintStatusSpecified = true;
                    }
                    else if (estimate.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedEstimate.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedEstimate.PrintStatusSpecified = true;
                    }
                    else if (estimate.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedEstimate.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedEstimate.PrintStatusSpecified = true;
                    }
                }
                if (estimate.EmailStatus != null)
                {
                    if (estimate.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedEstimate.EmailStatus = EmailStatusEnum.NotSet;
                        addedEstimate.EmailStatusSpecified = true;
                    }
                    else if (estimate.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedEstimate.EmailStatus = EmailStatusEnum.EmailSent;
                        addedEstimate.EmailStatusSpecified = true;
                    }
                    else if (estimate.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedEstimate.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedEstimate.EmailStatusSpecified = true;
                    }
                }
                if (estimate.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = estimate.BillEmail;
                    addedEstimate.BillEmail = EmailAddress;
                }

                if (estimate.ApplyTaxAfterDiscount != null)
                {
                    addedEstimate.ApplyTaxAfterDiscount = Convert.ToBoolean(estimate.ApplyTaxAfterDiscount);
                    addedEstimate.ApplyTaxAfterDiscountSpecified = true;
                }

                if (estimate.Expiration != null)
                {
                    addedEstimate.ExpirationDate = Convert.ToDateTime(estimate.Expiration);
                    addedEstimate.ExpirationDateSpecified = true;
                }
                if (estimate.AcceptedBy != null)
                {
                    addedEstimate.AcceptedBy = estimate.AcceptedBy;
                }

                if (estimate.AcceptedDate != null)
                {
                    addedEstimate.AcceptedDate = Convert.ToDateTime(estimate.AcceptedDate);
                    addedEstimate.AcceptedDateSpecified = true;
                }

                

                if (estimate.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var Data in estimate.CurrencyRef)
                    {
                        CurrencyRefName = Data.Name;
                    }
                    addedEstimate.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }
                #endregion

                estimateAdded = new Intuit.Ipp.Data.Estimate();
                estimateAdded = dataService.Update<Intuit.Ipp.Data.Estimate>(addedEstimate);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (estimateAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = estimateAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = estimateAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        #endregion

   }
    /// <summary>
   /// checking doc number is present or not in quickbook online for Estimate.
    /// </summary>
   public class OnlineEstimateQBEntryCollection : Collection<OnlineEstimateQBEntry>
   {
       public OnlineEstimateQBEntry FindEstimateNumberEntry(string docNumber)
       {
           foreach (OnlineEstimateQBEntry item in this)
           {
               if (item.DocNumber == docNumber)
               {
                   return item;
               }
           }
           return null;
       }

       public Collection<OnlineEstimateQBEntry> PopulateEstimateEntryList(string QBOFileName, string docNumber)
       {
           return this;
       }
   }
}
