﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using OnlineEntities;


namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineAttachmentEntry
    {
        private static ImportOnlineAttachmentEntry m_ImportOnlineAttachmentEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineAttachmentEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Customer class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineAttachmentEntry GetInstance()
        {
            if (m_ImportOnlineAttachmentEntry == null)
                m_ImportOnlineAttachmentEntry = new ImportOnlineAttachmentEntry();
            return m_ImportOnlineAttachmentEntry;
        }
        /// setting values to customer data table and returns collection.
        public OnlineAttachmentQBEntryCollection ImportOnlineAttachmentData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineAttachmentQBEntryCollection coll = new OnlineAttachmentQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry
            //DateTime VendorDt = new DateTime();
            string datevalue = string.Empty;
            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }


                    OnlineAttachmentQBEntry Attachment = new OnlineAttachmentQBEntry();


                    if (dt.Columns.Contains("DocNumber"))
                    {
                        #region Validations of NameOf
                        if (dr["DocNumber"].ToString() != string.Empty)
                        {
                            if (dr["DocNumber"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Attachment.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Attachment.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Attachment.DocNumber = dr["DocNumber"].ToString();
                                }
                            }
                            else
                            {
                                Attachment.DocNumber = dr["DocNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FilePath"))
                    {
                        #region Validations of FilePath
                        if (dr["FilePath"].ToString() != string.Empty)
                        {
                            Attachment.FilePath = dr["FilePath"].ToString();
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AttachmentNote"))
                    {
                        #region Validations of FilePath
                        if (dr["AttachmentNote"].ToString() != string.Empty)
                        {
                            Attachment.AttachmentNote = dr["AttachmentNote"].ToString();
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EntityType"))
                    {
                        #region Validations of FilePath
                        if (dr["EntityType"].ToString() != string.Empty)
                        {
                            Attachment.EntityType = dr["EntityType"].ToString();
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FullName"))
                    {
                        #region Validations of FilePath
                        if (dr["FullName"].ToString() != string.Empty)
                        {
                            Attachment.FullName = dr["FullName"].ToString();
                        }
                        #endregion
                    }
                    coll.Add(Attachment);
                }
                else
                {
                    return null;
                }
            }

            #endregion

            #endregion

            return coll;

        }
    }
}
