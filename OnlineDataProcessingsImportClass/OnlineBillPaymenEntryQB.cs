﻿using System;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("BillPayment", Namespace = "", IsNullable = false)]

    public class BillPayment
    {

        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TotalAmt;
        private string m_TxnDate;
        private string m_ExchangeRate;
        private string m_ProcessBillPayment;
        private string m_PayType;

        private Collection<OnlineEntities.VendorRef> m_vendorRef = new Collection<OnlineEntities.VendorRef>();
        private Collection<OnlineEntities.APAccountRef> m_APAccountRef = new Collection<OnlineEntities.APAccountRef>();
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private Collection<OnlineEntities.CheckPayment> m_CheckPaymentRef = new Collection<OnlineEntities.CheckPayment>();
        private Collection<OnlineEntities.CreditCardPayment> m_CreditCardPaymentRef = new Collection<OnlineEntities.CreditCardPayment>();

        //594
        private bool m_isAppend;

        #endregion

        #region Constructor
        public BillPayment()
        {
        }
        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public Collection<OnlineEntities.APAccountRef> APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }
        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }

        public Collection<OnlineEntities.CheckPayment> CheckPaymentRef
        {
            get { return m_CheckPaymentRef; }
            set { m_CheckPaymentRef = value; }
        }
        public Collection<OnlineEntities.CreditCardPayment> CreditCardPaymentRef
        {
            get { return m_CreditCardPaymentRef; }
            set { m_CreditCardPaymentRef = value; }
        }

        public Collection<OnlineEntities.VendorRef> vendorRef
        {
            get { return m_vendorRef; }
            set { m_vendorRef = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }
        public string ProcessBillPayment
        {
            get { return m_ProcessBillPayment; }
            set { m_ProcessBillPayment = value; }
        }
       
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        private Collection<OnlineEntities.Linkedtxn> m_LinkedTxn = new Collection<OnlineEntities.Linkedtxn>();
        public Collection<OnlineEntities.Linkedtxn> LinkedTxn
        {
            get { return m_LinkedTxn; }
            set { m_LinkedTxn = value; }
        }
        public string PayType
        {
            get { return m_PayType; }
            set { m_PayType = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region  Public Methods
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
        /// <summary>
        /// Creating new bill payment transaction in quickbook online. 
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOBillPayment(OnlineDataProcessingsImportClass.BillPayment coll)
        {
            bool flag = false;
            int linecount = 1;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.BillPayment addedbillPayment = new Intuit.Ipp.Data.BillPayment();
            Intuit.Ipp.Data.BillPayment BillPaymentAdded = new Intuit.Ipp.Data.BillPayment();
            
            BillPayment billpayment = new BillPayment();
            billpayment = coll;
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.PaymentLineDetail PaymentLineDetail = new Intuit.Ipp.Data.PaymentLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.LinkedTxn linkedTxn = new LinkedTxn();
            Intuit.Ipp.Data.LinkedTxn linkedtxn = new Intuit.Ipp.Data.LinkedTxn();
            DiscountOverride payment_discount = new DiscountOverride();
            try
            {
                #region Add Payment

                //594
                if (billpayment.isAppend == true)
                {
                    addedbillPayment.sparse = true;
                }

                if (billpayment.DocNumber != null)
                {
                    addedbillPayment.DocNumber = billpayment.DocNumber;
                }

                #region TxnDate,privatenote
                if (billpayment.TxnDate != null)
                {
                    try
                    {
                        addedbillPayment.TxnDate = Convert.ToDateTime(billpayment.TxnDate);
                        addedbillPayment.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (billpayment.PrivateNote != null)
                {
                    addedbillPayment.PrivateNote = billpayment.PrivateNote;
                }

                #endregion

                #region find DepartmentRef
               
                string DepartmentName = string.Empty;
                foreach (var dept in billpayment.DepartmentRef)
                {
                    DepartmentName = dept.Name;
                }
                string Departmentvalue = string.Empty;
               
                if (DepartmentName != string.Empty)
                {
                    var dataDepartmentName = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentName);
                   
                    foreach (var d in dataDepartmentName)
                    {
                        Departmentvalue = d.Id;
                    }
                    addedbillPayment.DepartmentRef = new ReferenceType()
                     {
                         name = DepartmentName,
                         Value = Departmentvalue
                     };
                }

                #endregion

                #region find ExchangeRate CurrencyRef
                if (billpayment.ExchangeRate != null)
                {
                    addedbillPayment.ExchangeRate = Convert.ToDecimal(billpayment.ExchangeRate);
                    addedbillPayment.ExchangeRateSpecified = true;
                }

                if (billpayment.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in billpayment.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedbillPayment.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #endregion

                #region discountline ,paymentline


                foreach (var l in billpayment.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;

                        #region DiscountLineDetail
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }

                                Line.AnyIntuitObject = DiscountLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        #endregion                   
                      
                        #region Line Apply To ref
                        else
                        {
                            if (l.LineAmount != null)
                            {
                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;
                            }                            

                            #region find applytobill

                            string applytobill = string.Empty;

                            foreach (var link in l.LinkedTxn)
                            {
                                applytobill = link.Applytobillref;
                            }
                            string bankaccountvalue = string.Empty;
                           
                            if (applytobill != string.Empty)
                            {
                                var bankdataAccount = await CommonUtilities.GetBillExistsInOnlineQBO(applytobill.Trim());

                                foreach (var dAccount in bankdataAccount)
                                {
                                    bankaccountvalue = dAccount.Id;
                                }

                                linkedtxn.TxnId = bankaccountvalue;
                                linkedtxn.TxnType = "Bill";
                                Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };

                                lines_online[linecount - 1] = Line;
                            }

                            #endregion
                        }
                        #endregion

                        addedbillPayment.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        linkedtxn = new LinkedTxn();
                        PaymentLineDetail = new Intuit.Ipp.Data.PaymentLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        payment_discount = new DiscountOverride();
                        linecount++;

                        #region DiscountLineDetail
                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }                               

                                Line.AnyIntuitObject = DiscountLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }


                        }
                        #endregion

                        #region Line Apply To Ref
                        else
                        {
                            if (l.LineAmount != null)
                            {
                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;
                            }
                           
                            #region find applytobill
                            string applytobill = string.Empty;

                            foreach (var link in l.LinkedTxn)
                            {
                                applytobill = link.Applytobillref;
                            }
                            string bankaccountvalue = string.Empty;
                            if (applytobill != string.Empty)
                            {
                                var bankdataAccount = await CommonUtilities.GetBillExistsInOnlineQBO(applytobill.Trim());
                                foreach (var dAccount in bankdataAccount)
                                {
                                    bankaccountvalue = dAccount.Id;
                                }

                                linkedtxn.TxnId = bankaccountvalue;
                                linkedtxn.TxnType = "Bill";
                                Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                            #endregion
                        }
                        #endregion

                        addedbillPayment.Line = AddLine(lines_online, Line, linecount);
                        #endregion                    
                    }
                }

                #endregion

                #region vendor
                foreach (var VendorRef in billpayment.vendorRef)
                {
                    string vendorvalue = string.Empty;
                   
                    if (VendorRef.Name!= null)
                    {
                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(VendorRef.Name.Trim());
                        
                        foreach (var vendors in dataVendor)
                        {
                            vendorvalue = vendors.Id;
                        }                       

                        addedbillPayment.VendorRef = new ReferenceType()
                        {
                            name = VendorRef.Name,
                            Value = vendorvalue
                        };
                    }
                }
                #endregion

                #region find ARAccountRef
                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                string accountNumber = string.Empty;
                string accountName = string.Empty;
                foreach (var acc in billpayment.APAccountRef)
                {
                    accountName = acc.Name;
                    //accountNumber = acc.AcctNum;
                }
                string accountvalue = string.Empty;
                if (accountName != string.Empty && accountName != null && accountName != "")
                {
                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                    addedbillPayment.APAccountRef = new ReferenceType()
                    {
                        name = accDetails.name,
                        Value = accDetails.Value
                    };
                }
                

                #endregion
                OnlineEntities.CheckPayment chkpay = new OnlineEntities.CheckPayment();
                OnlineEntities.CreditCardPayment creditCardPay = new OnlineEntities.CreditCardPayment();
                Intuit.Ipp.Data.BillPaymentCheck BillPaymentCheck_obj = new Intuit.Ipp.Data.BillPaymentCheck();
                Intuit.Ipp.Data.BillPaymentCreditCard BillPaymentCreditCard_obj = new Intuit.Ipp.Data.BillPaymentCreditCard();
                if (billpayment.PayType != null)
                {

                    if (billpayment.PayType == BillPaymentTypeEnum.Check.ToString())
                    {
                        addedbillPayment.PayType = BillPaymentTypeEnum.Check;
                        addedbillPayment.PayTypeSpecified = true;
                        #region find BankAccountRef
                        QueryService<Intuit.Ipp.Data.Account> bankaccountQueryService = new QueryService<Intuit.Ipp.Data.Account>(serviceContext);

                        string bankaccountName = string.Empty;
                        string bankaccountNumber = string.Empty;
                        foreach (var billaccount in billpayment.CheckPaymentRef)
                        {
                            foreach (var bankaccount in billaccount.BankAccountRef)
                            {
                                bankaccountName = bankaccount.Name;
                                //bankaccountNumber = bankaccount.AcctNum;
                            }
                        }
                        string bankaccountvalue = string.Empty;
                        if (bankaccountName != string.Empty && bankaccountName != null && bankaccountName != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(bankaccountName);
                            BillPaymentCheck_obj.BankAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       

                        #endregion

                        #region printstaus

                        string printstatus = string.Empty;
                        foreach (var billaccount in billpayment.CheckPaymentRef)
                        {
                            if (billaccount.Printstatus != null)
                            {
                                printstatus = billaccount.Printstatus.ToString();
                            }
                        }

                        if (printstatus!=string.Empty)
                        {
                            if (printstatus == PrintStatusEnum.NotSet.ToString())
                            {
                                BillPaymentCheck_obj.PrintStatus = PrintStatusEnum.NotSet;

                                BillPaymentCheck_obj.PrintStatusSpecified = true;
                            }
                            else if (printstatus == PrintStatusEnum.NeedToPrint.ToString())
                            {
                                BillPaymentCheck_obj.PrintStatus = PrintStatusEnum.NeedToPrint;

                                BillPaymentCheck_obj.PrintStatusSpecified = true;
                            }
                            else if (printstatus == PrintStatusEnum.PrintComplete.ToString())
                            {
                                BillPaymentCheck_obj.PrintStatus = PrintStatusEnum.PrintComplete;

                                BillPaymentCheck_obj.PrintStatusSpecified = true;
                            }
                           
                        }
                        #endregion

                        addedbillPayment.AnyIntuitObject = BillPaymentCheck_obj;

                    }
                    else if (billpayment.PayType == BillPaymentTypeEnum.CreditCard.ToString())
                    {
                        addedbillPayment.PayType = BillPaymentTypeEnum.CreditCard;
                        addedbillPayment.PayTypeSpecified = true;
                       #region find CCAccountRef
                        QueryService<Intuit.Ipp.Data.Account> bankaccountQueryService = new QueryService<Intuit.Ipp.Data.Account>(serviceContext);

                        string bankaccountName = string.Empty;
                        string bankaccountNumber = string.Empty;
                        foreach (var CreditCardaccount in billpayment.CreditCardPaymentRef)
                        {
                            foreach (var ccaccount in CreditCardaccount.CCAccountRef)
                            {
                                bankaccountName = ccaccount.Name;
                                //bankaccountNumber = ccaccount.AcctNum;
                            }
                        }
                        
                        string bankaccountvalue = string.Empty;
                        if (bankaccountName != string.Empty || bankaccountName != null || bankaccountName != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(bankaccountName);
                            BillPaymentCreditCard_obj.CCAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }  
                       

                       #endregion

                       addedbillPayment.AnyIntuitObject = BillPaymentCreditCard_obj;

                    }

                }

                if (billpayment.TotalAmt != null)
                {
                    addedbillPayment.TotalAmt =Convert.ToDecimal(billpayment.TotalAmt);
                    addedbillPayment.TotalAmtSpecified = true;
                }               
                #endregion
                BillPaymentAdded = dataService.Add<Intuit.Ipp.Data.BillPayment>(addedbillPayment);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (BillPaymentAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = BillPaymentAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = BillPaymentAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

        /// updating bill payment entry in quickbook online by the transaction Id.. 

        public async System.Threading.Tasks.Task<bool> UpdateQBOBillPayment(OnlineDataProcessingsImportClass.BillPayment coll, string ID, string syncToken, Intuit.Ipp.Data.BillPayment PreviousData=null)
        {
            bool flag = false;
            int linecount = 1;
            DataService dataService = null;
            Intuit.Ipp.Data.BillPayment BillPaymentAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.BillPayment addedbillPayment = new Intuit.Ipp.Data.BillPayment();
            BillPayment billpayment = new BillPayment();
            billpayment = coll;
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.PaymentLineDetail PaymentLineDetail = new Intuit.Ipp.Data.PaymentLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.LinkedTxn linkedTxn = new LinkedTxn();
            Intuit.Ipp.Data.LinkedTxn linkedtxn = new Intuit.Ipp.Data.LinkedTxn();
            DiscountOverride payment_discount = new DiscountOverride();
            try
            {
                #region Add Payment

                //594 PreviousData is used to maintain previous records and append new data if changes are applied ,Lines will be appended to next line directly
                if (billpayment.isAppend == true)
                {
                    addedbillPayment = PreviousData;
                    addedbillPayment.sparse = true;
                    addedbillPayment.sparseSpecified = true;
                    if (addedbillPayment.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedbillPayment.Line.Length;
                        lines_online = addedbillPayment.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                }

                addedbillPayment.Id = ID;
                addedbillPayment.SyncToken = syncToken;

                if (billpayment.DocNumber != null)
                {
                    addedbillPayment.DocNumber = billpayment.DocNumber;
                }

                #region TxnDate,privatenote
                if (billpayment.TxnDate != null)
                {
                    try
                    {
                        addedbillPayment.TxnDate = Convert.ToDateTime(billpayment.TxnDate);
                        addedbillPayment.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (billpayment.PrivateNote != null)
                {
                    addedbillPayment.PrivateNote = billpayment.PrivateNote;
                }

                #endregion

                #region find DepartmentRef
                
                string DepartmentName = string.Empty;
                foreach (var dept in billpayment.DepartmentRef)
                {
                    DepartmentName = dept.Name;
                }
                string Departmentvalue = string.Empty;
             
                if (DepartmentName != string.Empty)
                {
                    var dataDepartmentName = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentName);
                   
                    foreach (var d in dataDepartmentName)
                    {
                        Departmentvalue = d.Id;
                    }
                    addedbillPayment.DepartmentRef = new ReferenceType()
                    {
                        name = DepartmentName,
                        Value = Departmentvalue
                    };
                }

                #endregion

                #region find ExchangeRate CurrencyRef
                if (billpayment.ExchangeRate != null)
                {
                    addedbillPayment.ExchangeRate = Convert.ToDecimal(billpayment.ExchangeRate);
                    addedbillPayment.ExchangeRateSpecified = true;
                }

                if (billpayment.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in billpayment.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedbillPayment.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #endregion

                #region discountline ,paymentline


                foreach (var l in billpayment.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;

                        #region DiscountLineDetail
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }

                                Line.AnyIntuitObject = DiscountLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        #endregion

                        #region Line Apply To ref
                        else
                        {
                            if (l.LineAmount != null)
                            {
                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;
                            }

                            #region find applytobill

                            string applytobill = string.Empty;

                            foreach (var link in l.LinkedTxn)
                            {
                                applytobill = link.Applytobillref;
                            }
                            string bankaccountvalue = string.Empty;
                           
                            if (applytobill != string.Empty)
                            {
                                var bankdataAccount = await CommonUtilities.GetBillExistsInOnlineQBO(applytobill.Trim());
                               
                                foreach (var dAccount in bankdataAccount)
                                {
                                    bankaccountvalue = dAccount.Id;
                                }

                                linkedtxn.TxnId = bankaccountvalue;
                                linkedtxn.TxnType = "Bill";
                                Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };
                                lines_online[linecount - 1] = Line;
                            }

                            #endregion
                        }
                        #endregion

                        addedbillPayment.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        linkedtxn = new LinkedTxn();
                        PaymentLineDetail = new Intuit.Ipp.Data.PaymentLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        payment_discount = new DiscountOverride();
                        linecount++;

                        #region DiscountLineDetail
                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                  
                                }

                                Line.AnyIntuitObject = DiscountLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        #endregion

                        #region Line Apply To Ref
                        else
                        {
                            if (l.LineAmount != null)
                            {
                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;
                            }

                            #region find applytobill
                            string applytobill = string.Empty;

                            foreach (var link in l.LinkedTxn)
                            {
                                applytobill = link.Applytobillref;
                            }
                            string bankaccountvalue = string.Empty;
                           
                            if (applytobill != string.Empty)
                            {
                                var bankdataAccount = await CommonUtilities.GetBillExistsInOnlineQBO(applytobill.Trim());

                                foreach (var dAccount in bankdataAccount)
                                {
                                    bankaccountvalue = dAccount.Id;
                                }

                                linkedtxn.TxnId = bankaccountvalue;
                                linkedtxn.TxnType = "Bill";
                                Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { linkedtxn };
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                            #endregion
                        }
                        #endregion

                        addedbillPayment.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                #endregion

                #region vendor
                foreach (var VendorRef in billpayment.vendorRef)
                {
                    string vendorvalue = string.Empty;
                   
                    if (VendorRef.Name != null)
                    {
                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(VendorRef.Name.Trim());
                       
                        foreach (var vendors in dataVendor)
                        {
                            vendorvalue = vendors.Id;
                        } 

                        addedbillPayment.VendorRef = new ReferenceType()
                        {
                            name = VendorRef.Name,
                            Value = vendorvalue
                        };
                    }
                }
                #endregion

                #region find ARAccountRef
                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                string accountName = string.Empty;
                string accountNumber = string.Empty;
                foreach (var acc in billpayment.APAccountRef)
                {
                    accountName = acc.Name;
                    //accountNumber = acc.AcctNum;
                }
                if (accountName != string.Empty && accountName != null && accountName != "")
                {
                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                    addedbillPayment.APAccountRef = new ReferenceType()
                    {
                        name = accDetails.name,
                        Value = accDetails.Value
                    };
                }
                

                #endregion
                OnlineEntities.CheckPayment chkpay = new OnlineEntities.CheckPayment();
                OnlineEntities.CreditCardPayment creditCardPay = new OnlineEntities.CreditCardPayment();
                Intuit.Ipp.Data.BillPaymentCheck BillPaymentCheck_obj = new Intuit.Ipp.Data.BillPaymentCheck();
                Intuit.Ipp.Data.BillPaymentCreditCard BillPaymentCreditCard_obj = new Intuit.Ipp.Data.BillPaymentCreditCard();
                if (billpayment.PayType != null)
                {
                    if (billpayment.PayType == BillPaymentTypeEnum.Check.ToString())
                    {
                        addedbillPayment.PayType = BillPaymentTypeEnum.Check;
                        addedbillPayment.PayTypeSpecified = true;
                        #region find BankAccountRef
                        QueryService<Intuit.Ipp.Data.Account> bankaccountQueryService = new QueryService<Intuit.Ipp.Data.Account>(serviceContext);

                        string bankaccountName = string.Empty;
                        foreach (var billaccount in billpayment.CheckPaymentRef)
                        {
                            foreach (var bankaccount in billaccount.BankAccountRef)
                            {
                                bankaccountName = bankaccount.Name;
                            }
                        }
                        string bankaccountvalue = string.Empty;
                        string bankaccountNumber = string.Empty;
                        if (bankaccountName != string.Empty && bankaccountName != null && bankaccountName != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(bankaccountName);
                            BillPaymentCheck_obj.BankAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       
                        #endregion

                        #region printstaus

                        string printstatus = string.Empty;
                        foreach (var billaccount in billpayment.CheckPaymentRef)
                        {
                            if (billaccount.Printstatus != null)
                            {
                                printstatus = billaccount.Printstatus.ToString();
                            }

                        }



                        if (printstatus != string.Empty)
                        {
                            if (printstatus == PrintStatusEnum.NotSet.ToString())
                            {
                                BillPaymentCheck_obj.PrintStatus = PrintStatusEnum.NotSet;

                                BillPaymentCheck_obj.PrintStatusSpecified = true;
                            }
                            else if (printstatus == PrintStatusEnum.NeedToPrint.ToString())
                            {
                                BillPaymentCheck_obj.PrintStatus = PrintStatusEnum.NeedToPrint;

                                BillPaymentCheck_obj.PrintStatusSpecified = true;
                            }
                            else if (printstatus == PrintStatusEnum.PrintComplete.ToString())
                            {
                                BillPaymentCheck_obj.PrintStatus = PrintStatusEnum.PrintComplete;

                                BillPaymentCheck_obj.PrintStatusSpecified = true;
                            }

                        }
                        #endregion

                        addedbillPayment.AnyIntuitObject = BillPaymentCheck_obj;
                    }
                    else if (billpayment.PayType == BillPaymentTypeEnum.CreditCard.ToString())
                    {
                        addedbillPayment.PayType = BillPaymentTypeEnum.CreditCard;
                        addedbillPayment.PayTypeSpecified = true;


                        #region find CCAccountRef
                        QueryService<Intuit.Ipp.Data.Account> bankaccountQueryService = new QueryService<Intuit.Ipp.Data.Account>(serviceContext);

                        string bankaccountName = string.Empty;
                        string bankaccountNumber = string.Empty;
                        foreach (var CreditCardaccount in billpayment.CreditCardPaymentRef)
                        {
                            foreach (var ccaccount in CreditCardaccount.CCAccountRef)
                            {
                                bankaccountName = ccaccount.Name;
                                //bankaccountNumber = ccaccount.AcctNum;
                            }
                        }
                        string bankaccountvalue = string.Empty;
                        if (bankaccountName != string.Empty && bankaccountName != null && bankaccountName != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(bankaccountName);
                            BillPaymentCreditCard_obj.CCAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       
                        #endregion
                        addedbillPayment.AnyIntuitObject = BillPaymentCreditCard_obj;
                    }
                }

                if (billpayment.TotalAmt != null)
                {
                    addedbillPayment.TotalAmt = Convert.ToDecimal(billpayment.TotalAmt);
                    addedbillPayment.TotalAmtSpecified = true;
                }
                #endregion
                BillPaymentAdded = dataService.Update<Intuit.Ipp.Data.BillPayment>(addedbillPayment);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (BillPaymentAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = BillPaymentAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = BillPaymentAdded.SyncToken;
                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        #endregion

    }

    /// <summary>
    /// checking doc number is present or not in quickbook online for billpayment.
    /// </summary>
    public class OnlineBillPaymentQBEntryCollection : Collection<BillPayment>
    {
        public BillPayment FindPamentNumberEntry(string docNumber)
        {
            foreach (BillPayment item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }

    }
}


