﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using TransactionImporter;
namespace OnlineDataProcessingsImportClass
{
    class ImportOnlineAccountEntry
    {
        private static ImportOnlineAccountEntry m_ImportOnlineAccountEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineAccountEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import item class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineAccountEntry GetInstance()
        {
            if (m_ImportOnlineAccountEntry == null)
                m_ImportOnlineAccountEntry = new ImportOnlineAccountEntry();
            return m_ImportOnlineAccountEntry;
        }
        /// setting values to item data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlineAccountQBEntryCollection ImportOnlineAccountsData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineAccountQBEntryCollection coll = new OnlineAccountQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Items Validation
                    OnlineDataProcessingsImportClass.AccountClass Account = new OnlineDataProcessingsImportClass.AccountClass();
                    //if (dt.Columns.Contains("Name"))
                    //{
                        //Account = coll.FindItemEntry(dr["Name"].ToString());

                        //if (Account == null)
                        //{

                            #region if item is null

                            Account = new AccountClass();

                            if (dt.Columns.Contains("Name"))
                            {
                                #region Validations of Name
                                if (dr["Name"].ToString() != string.Empty)
                                {

                                    //if (dr["Name"].ToString().Contains(":") || dr["Name"].ToString().Contains("\""))
                                    //{
                                    //    if (isIgnoreAll == false)
                                    //    {
                                    //        string strMessages = "This Name (" + dr["Name"].ToString() + ") Could not contains Symbols ':' or '\"'.If you press cancel this record will not be added to QuickBooks.If Ignore it will remove these characters.";
                                    //        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    //        if (Convert.ToString(result) == "Cancel")
                                    //        {
                                    //            continue;
                                    //        }
                                    //        if (Convert.ToString(result) == "No")
                                    //        {
                                    //            return null;
                                    //        }
                                    //        if (Convert.ToString(result) == "Ignore")
                                    //        {
                                    //            Account.Name = dr["Name"].ToString().Replace(":", string.Empty).Replace("\"", string.Empty);
                                    //        }
                                    //        if (Convert.ToString(result) == "Abort")
                                    //        {
                                    //            isIgnoreAll = true;
                                    //            Account.Name = dr["Name"].ToString();
                                    //        }
                                    //    }
                                    //}

                                    if (dr["Name"].ToString().Length > 100)
                                    {
                                        
                                        if (isIgnoreAll == false )
                                        {
                                            string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.Name = dr["Name"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.Name = dr["Name"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.Name = dr["Name"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.Name = dr["Name"].ToString();
                                    }
                                }
                                #endregion
                            }

                           if (dt.Columns.Contains("SubAccount"))
                           {
                               #region Validations of SubAccount
                               if (dr["SubAccount"].ToString() != "<None>" || dr["SubAccount"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["SubAccount"].ToString(), out result))
                                    {
                                        Account.Active = Convert.ToInt32(dr["SubAccount"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["SubAccount"].ToString().ToLower() == "true")
                                        {
                                            Account.Active = dr["SubAccount"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["SubAccount"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "SubAccount";
                                            }
                                            else
                                                Account.Active = dr["SubAccount"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SubAccount(" + dr["SubAccount"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Account.Active = dr["SubAccount"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Account.Active = dr["SubAccount"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Account.Active = dr["SubAccount"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                           }


                           if (dt.Columns.Contains("Description"))
                           {
                               #region Validations of Description
                               if (dr["Description"].ToString() != string.Empty)
                               {
                                   if (dr["Description"].ToString().Length > 4000)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This Description (" + dr["Description"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Account.Description = dr["Description"].ToString().Substring(0, 4000);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Account.Description = dr["Description"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Account.Description = dr["Description"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Account.Description = dr["Description"].ToString();
                                   }
                               }
                               #endregion
                           }

                           if (dt.Columns.Contains("FullyQualifiedName"))
                           {
                               #region Validations of FullyQualifiedName
                               if (dr["FullyQualifiedName"].ToString() != string.Empty)
                               {
                                   if (dr["FullyQualifiedName"].ToString().Length > 100)
                                   {
                                       if (isIgnoreAll == false)
                                       {
                                           string strMessages = "This FullyQualifiedName (" + dr["FullyQualifiedName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                           DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                           if (Convert.ToString(result) == "Cancel")
                                           {
                                               continue;
                                           }
                                           if (Convert.ToString(result) == "No")
                                           {
                                               return null;
                                           }
                                           if (Convert.ToString(result) == "Ignore")
                                           {
                                               Account.FullyQualifiedName = dr["FullyQualifiedName"].ToString().Substring(0, 15);
                                           }
                                           if (Convert.ToString(result) == "Abort")
                                           {
                                               isIgnoreAll = true;
                                               Account.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Account.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                       }
                                   }
                                   else
                                   {
                                       Account.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                   }
                               }
                               #endregion

                               OnlineEntities.ParentRef ParentRef = new OnlineEntities.ParentRef();
                               string[] arr = new string[15];
                               if (Account.FullyQualifiedName.Contains(":"))
                               {
                                   arr = Account.FullyQualifiedName.Split(':');

                                   string ParentName = "";
                                   int cnt = 0;
                                   foreach (var a in arr)
                                   {
                                       cnt++;
                                   }
                                   for (int i = 0; i < cnt - 1; i++)
                                   {
                                       ParentName += arr[i] + ":";
                                   }
                                   ParentName = ParentName.Remove(ParentName.Length - 1);
                                   ParentRef.Name = ParentName;
                                   Account.ParentRef.Add(ParentRef);
                                   Account.Name = arr[cnt - 1];
                               }
                               else
                               {
                                   if (!dt.Columns.Contains("Name") || (dr["Name"].ToString() == string.Empty))
                                   {
                                       Account.Name = Account.FullyQualifiedName;
                                   }

                               }
                           }

                         
                          

                           if (dt.Columns.Contains("Active"))
                            {
                                #region Validations of Active
                                if (dr["Active"].ToString() != "<None>" || dr["Active"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["Active"].ToString(), out result))
                                    {
                                        Account.Active = Convert.ToInt32(dr["Active"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["Active"].ToString().ToLower() == "true")
                                        {
                                            Account.Active = dr["Active"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["Active"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "Active";
                                            }
                                            else
                                                Account.Active = dr["Active"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Active(" + dr["Active"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Account.Active = dr["Active"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Account.Active = dr["Active"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Account.Active = dr["Active"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                           if (dt.Columns.Contains("Classification"))
                            {
                                #region Validations of Description
                                if (dr["Classification"].ToString() != string.Empty)
                                {
                                    if (dr["Classification"].ToString().Length > 50)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Classification (" + dr["Classification"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.Classification = dr["Classification"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.Classification = dr["Classification"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.Classification = dr["Classification"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.Classification = dr["Classification"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AccountType"))
                            {
                                #region Validations of Description
                                if (dr["AccountType"].ToString() != string.Empty)
                                {
                                    if (dr["AccountType"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AccountType (" + dr["AccountType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.AccountType = dr["AccountType"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.AccountType = dr["AccountType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.AccountType = dr["AccountType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.AccountType = dr["AccountType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AccountSubType"))
                            {
                                #region Validations of Description
                                if (dr["AccountSubType"].ToString() != string.Empty)
                                {
                                    if (dr["AccountSubType"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AccountSubType (" + dr["AccountSubType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.AccountSubType = dr["AccountSubType"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.AccountSubType = dr["AccountSubType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.AccountSubType = dr["AccountSubType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.AccountSubType = dr["AccountSubType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AcctNum"))
                            {
                               if( dr["AcctNum"] !=null)
                                    Account.AcctNum = dr["AcctNum"].ToString();
                            }

                            if (dt.Columns.Contains("CurrentBalance"))
                            {
                                #region Validations of UnitPrice
                                if (dr["CurrentBalance"].ToString() != string.Empty)
                                {
                                    decimal amount;

                                    if (!decimal.TryParse(dr["CurrentBalance"].ToString(), out amount))
                                    { 
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrentBalance (" + dr["CurrentBalance"].ToString() + ") not valid for quickbooks  .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.CurrentBalance = dr["CurrentBalance"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.CurrentBalance = dr["CurrentBalance"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.CurrentBalance = dr["CurrentBalance"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.CurrentBalance = dr["CurrentBalance"].ToString();
                                    }
                                }
                                #endregion
                            }   
                         
                           if (dt.Columns.Contains("CurrentBalanceWithSubAccounts"))
                            {
                                #region Validations of UnitPrice
                                if (dr["CurrentBalanceWithSubAccounts"].ToString() != string.Empty)
                                {
                                    decimal amount;

                                    if (!decimal.TryParse(dr["CurrentBalance"].ToString(), out amount))
                                    { 
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrentBalanceWithSubAccounts (" + dr["CurrentBalanceWithSubAccounts"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Account.CurrentBalance = dr["CurrentBalanceWithSubAccounts"].ToString();//.Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Account.CurrentBalance = dr["CurrentBalanceWithSubAccounts"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Account.CurrentBalance = dr["CurrentBalanceWithSubAccounts"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Account.CurrentBalance = dr["CurrentBalanceWithSubAccounts"].ToString();
                                    }
                                }
                                #endregion
                            }   

                             OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();
                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CurrencyRef.Name != null)
                            {
                                Account.CurrencyRef.Add(CurrencyRef);
                            }
                            #endregion

                            coll.Add(Account);
                        //}
                    //}

                }
                else
                {
                    return null;
                }

            }
            #endregion
            
            #endregion

            return coll;

        }
             
    }
}
