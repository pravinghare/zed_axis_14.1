﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using DataProcessingBlocks;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System.Windows.Forms;
using System.Net;
using System.Threading.Tasks;
using OnlineEntities;

namespace OnlineDataProcessingsImportClass
{
    public class OnlinePurchaseQBEntry
    {
        #region member
        private string m_DocNumber;

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }
        private string m_PrivateNote;

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }
        private string m_TxnDate;

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();

        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }

        // Axis 740
        public Collection<PaymentMethodRef> PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }

        //bug no. 418
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        private string m_ExchangeRate;
        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();

        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();

        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        private Collection<OnlineEntities.AccountRef> m_AccountRef = new Collection<OnlineEntities.AccountRef>();

        public Collection<OnlineEntities.AccountRef> AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }
        private string m_PaymentType;

        public string PaymentType
        {
            get { return m_PaymentType; }
            set { m_PaymentType = value; }
        }

        private Collection<OnlineEntities.Entity> m_Entity = new Collection<OnlineEntities.Entity>();
        private Collection<PaymentMethodRef> m_PaymentMethodRef = new Collection<PaymentMethodRef>(); // Axis 740

        public Collection<OnlineEntities.Entity> Entity
        {
            get { return m_Entity; }
            set { m_Entity = value; }
        }
        private string m_Credit;

        public string Credit
        {
            get { return m_Credit; }
            set { m_Credit = value; }
        }
        private string m_PrintStatus;

        public string PrintStatus
        {
            get { return m_PrintStatus; }
            set { m_PrintStatus = value; }
        }
        private string m_GlobalTaxCalculation;

        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }

        private string m_TransactionLocationType;

        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }


        //594
        private bool m_isAppend;
        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }


        #endregion

        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        /// <summary>
        /// Creating new Purchase transaction in quickbook online.
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOPurchase(OnlineDataProcessingsImportClass.OnlinePurchaseQBEntry coll)
        {
            bool flag = false;
            int linecount = 1;
            int linecount1 = 0;
            DataService dataService = null;
            Intuit.Ipp.Data.Purchase purchaseAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.Purchase addedPurchase = new Intuit.Ipp.Data.Purchase();
            OnlinePurchaseQBEntry purchase = new OnlinePurchaseQBEntry();
            purchase = coll;
            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            #region tax member
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            int array_count = 0;
            bool taxFlag = true;
            #endregion

            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            try
            {
                #region Add Purchase

                //594
                if (purchase.isAppend == true)
                {
                    addedPurchase.sparse = true;
                }

                if (purchase.DocNumber != null)
                {
                    addedPurchase.DocNumber = purchase.DocNumber;
                }

                if (purchase.TxnDate != null)
                {
                    try
                    {
                        addedPurchase.TxnDate = Convert.ToDateTime(purchase.TxnDate);
                        addedPurchase.TxnDateSpecified = true;
                    }
                    catch { }
                }

                if (purchase.PrivateNote != null)
                {
                    addedPurchase.PrivateNote = purchase.PrivateNote;
                }

                // Axis 740
                foreach (var paymentMethod in purchase.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedPurchase.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }
                // Axis 740 end

                //bug no. 418
                #region find DepartmentRef
                string DepartmentName = string.Empty;
                foreach (var dept in purchase.DepartmentRef)
                {
                    DepartmentName = dept.Name;
                }
                string Departmentvalue = string.Empty;
                if (DepartmentName != string.Empty)
                {
                    var dataDepartmentName = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentName.Trim());
                    foreach (var d in dataDepartmentName)
                    {
                        Departmentvalue = d.Id;
                    }
                    addedPurchase.DepartmentRef = new ReferenceType()
                    {
                        name = DepartmentName,
                        Value = Departmentvalue
                    };
                }

                #endregion

                #region find ExchangeRate CurrencyRef
                if (purchase.ExchangeRate != null)
                {
                    addedPurchase.ExchangeRate = Convert.ToDecimal(purchase.ExchangeRate);
                    addedPurchase.ExchangeRateSpecified = true;
                }

                if (purchase.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in purchase.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedPurchase.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #endregion

                // Axis 742
                if (purchase.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(purchase.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedPurchase.TransactionLocationType = value;
                    }
                    else
                    {
                        addedPurchase.TransactionLocationType = purchase.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (purchase.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedPurchase.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedPurchase.GlobalTaxCalculationSpecified = true;
                        }
                        else if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedPurchase.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedPurchase.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedPurchase.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedPurchase.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion

                foreach (var l in purchase.Line)
                {
                    if (flag == false)
                    {
                        #region
                        flag = true;
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }


                                // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{  
                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name);
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }



                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                #region TaxCodeRef
                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }
                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                           
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }


                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                             

                                #region TaxCodeRef
                                //Bug 480
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Value,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchase.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        linecount++;

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                string displayname = string.Empty;

                                //  // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{
                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }
                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                #region TaxCodeRef
                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }
                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                    if (accountData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                    if (customerData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                    
                                }

                                #region TaxCodeRef
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchase.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                //if (CommonUtilities.GetInstance().CountryVersion != "US")
                //{
                //    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)
                //    #region TxnTaxDetail

                //    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                //    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                //    Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();

                //    for (int i = 0; i < array_count; i++)
                //    {
                //        string taxCode = string.Empty;
                //        taxCode = Array_taxcodeName[i].ToString();
                //        QueryService<TaxCode> TAxService = new QueryService<TaxCode>(serviceContext);
                //        if (taxCode.Contains("'"))
                //        {
                //            taxCode = taxCode.Replace("'", "\\'");
                //        }
                //        dataTaxcode = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(TAxService.ExecuteIdsQuery("select * from TaxCode where Name='" + taxCode.Trim() + "'"));
                //        foreach (var tax in dataTaxcode)
                //        {
                //            if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                //            {
                //                foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                //                {
                //                    GstRef = taxRate.TaxRateRef;
                //                    GSTtaxRateRefName = taxRate.TaxRateRef.name;
                //                }
                //            }
                //        }

                //        taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                //        taxLine.DetailTypeSpecified = true;

                //        taxLineDetail.PercentBased = true;
                //        taxLineDetail.PercentBasedSpecified = true;

                //        taxLineDetail.TaxRateRef = GstRef;

                //        string taxValue = string.Empty;
                //        ReadOnlyCollection<Intuit.Ipp.Data.TaxRate> dataTaxRate = null;
                //        QueryService<TaxRate> taxQueryService = new QueryService<TaxRate>(serviceContext);
                //        if (GSTtaxRateRefName.Contains("'"))
                //        {
                //            GSTtaxRateRefName = GSTtaxRateRefName.Replace("'", "\\'");
                //        }
                //        dataTaxRate = new ReadOnlyCollection<Intuit.Ipp.Data.TaxRate>(taxQueryService.ExecuteIdsQuery("Select * From TaxRate where Name='" + GSTtaxRateRefName.Trim() + "'"));
                //        foreach (var dTaxRate in dataTaxRate)
                //        {
                //            taxValue = dTaxRate.RateValue.ToString();
                //        }

                //        decimal s = Convert.ToDecimal(taxValue);
                //        decimal taxVal = (s) / 100;
                //        taxLineDetail.TaxPercent = taxVal;
                //        taxLineDetail.TaxPercentSpecified = true;

                //        taxLine.Amount = Math.Round(Convert.ToDecimal(Array_taxAmount[i] * taxLineDetail.TaxPercent), 2, MidpointRounding.AwayFromZero);
                //        taxLine.AmountSpecified = true;

                //        taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                //        taxLineDetail.NetAmountTaxableSpecified = true;

                //        taxLine.AnyIntuitObject = taxLineDetail;
                //        linecount1++;
                //        Array.Resize(ref lines_online1, linecount1);
                //        lines_online1[linecount1 - 1] = taxLine;
                //        txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);

                //        taxLine = new Intuit.Ipp.Data.Line();
                //        taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                //        if (txnTaxDetail != null)
                //        {
                //            if (TotalTax != 0)
                //            {
                //                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                //                txnTaxDetail.TotalTaxSpecified = true;
                //            }
                //            addedPurchase.TxnTaxDetail = txnTaxDetail;
                //        }
                //    }

                //    #endregion
                //}
                //else
                //{

                //}




                foreach (var AccountRef in purchase.AccountRef)
                {
                    if (AccountRef.Name != null && AccountRef.Name != string.Empty && AccountRef.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(AccountRef.Name);
                        addedPurchase.AccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                   
                }

                if (purchase.PaymentType == PaymentTypeEnum.Cash.ToString())
                {
                    addedPurchase.PaymentType = PaymentTypeEnum.Cash;
                    addedPurchase.PaymentTypeSpecified = true;
                }
                else if (purchase.PaymentType == PaymentTypeEnum.Check.ToString())
                {
                    addedPurchase.PaymentType = PaymentTypeEnum.Check;
                    addedPurchase.PaymentTypeSpecified = true;
                }
                else if (purchase.PaymentType == PaymentTypeEnum.CreditCard.ToString())
                {
                    addedPurchase.PaymentType = PaymentTypeEnum.CreditCard;
                    addedPurchase.PaymentTypeSpecified = true;
                }



                if (purchase.Credit != null)
                {
                    addedPurchase.Credit = Convert.ToBoolean(purchase.Credit);
                    addedPurchase.CreditSpecified = true;
                }
                if (purchase.PrintStatus != null)
                {
                    if (purchase.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedPurchase.PrintStatus = PrintStatusEnum.NotSet;
                        addedPurchase.PrintStatusSpecified = true;
                    }
                    else if (purchase.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedPurchase.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedPurchase.PrintStatusSpecified = true;
                    }
                    else if (purchase.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedPurchase.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedPurchase.PrintStatusSpecified = true;
                    }
                }

                #region find EntityRef

                Intuit.Ipp.Data.EntityTypeRef entityref = new EntityTypeRef();
                foreach (var ent in purchase.Entity)
                {
                    if (ent.Type == EntityTypeEnum.Customer.ToString())
                    {
                        string id = string.Empty;
                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(ent.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addedPurchase.EntityRef = new ReferenceType()
                        {
                            name = ent.Name,
                            Value = id,
                            type = EntityTypeEnum.Customer.ToString()
                        };
                    }
                    else if (ent.Type == EntityTypeEnum.Vendor.ToString() || ent.Type == null)
                    {
                        string id = string.Empty;
                        var dataItem = await CommonUtilities.GetVendorExistsInOnlineQBO(ent.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addedPurchase.EntityRef = new ReferenceType()
                        {
                            name = ent.Name,
                            Value = id,
                            type = EntityTypeEnum.Vendor.ToString()
                        };
                    }
                    else if (ent.Type == EntityTypeEnum.Employee.ToString())
                    {
                        string id = string.Empty;
                        var dataItem = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(ent.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addedPurchase.EntityRef = new ReferenceType()
                        {
                            name = ent.Name,
                            Value = id,
                            type = EntityTypeEnum.Employee.ToString()
                        };
                    }
                }

                #endregion
                
                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedPurchase.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && purchase.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {
                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Array_taxAmount[i];
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedPurchase.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //BUG ;; 586

                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in purchase.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedPurchase.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                //if (CommonUtilities.GetInstance().CountryVersion == "US" || CommonUtilities.GetInstance().GrossNet == false || addedPurchase.GlobalTaxCalculation.ToString() != GlobalTaxCalculationEnum.TaxInclusive.ToString())
                //{
                //    foreach (var txntaxCode in purchase.TxnTaxDetail)
                //    {
                //        #region TxnTaxDetail
                //        ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                //        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                //        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                //        if (txntaxCode.TxnTaxCodeRef != null)
                //        {
                //            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                //            {
                //                QueryService<TaxCode> taxCodeQueryService = new QueryService<TaxCode>(serviceContext);
                //                TaxCode TaxCode = new TaxCode();
                //                TaxCode.Name = taxRef.Name;
                //                string Taxvalue = string.Empty;
                //                string TaxName = string.Empty;
                //                TaxName = TaxCode.Name;
                //                if (TaxCode.Name != null)
                //                {
                //                    if (TaxCode.Name.Contains("'"))
                //                    {
                //                        TaxCode.Name = TaxCode.Name.Replace("'", "\\'");
                //                    }
                //                    dataTax = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(taxCodeQueryService.ExecuteIdsQuery("Select * From TaxCode where Name='" + TaxCode.Name.Trim() + "'"));
                //                    if (dataTax != null)
                //                    {
                //                        foreach (var tax in dataTax)
                //                        {
                //                            Taxvalue = tax.Id;
                //                        }

                //                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                //                        {
                //                            name = TaxName,
                //                            Value = Taxvalue
                //                        };
                //                    }
                //                }
                //            }
                //        }

                //        foreach (var txnline in txntaxCode.TaxLine)
                //        {
                //            if (txnline != null)
                //            {
                //                taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                //                taxLine.DetailTypeSpecified = true;
                //            }

                //            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();

                //            foreach (var taxlineDetail in txnline.TaxLineDetail)
                //            {
                //                if (dataTax != null)
                //                {
                //                    foreach (var taxRateRef in dataTax)
                //                    {
                //                        foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                //                        {
                //                            taxLineDetail.TaxRateRef = taxRate.TaxRateRef;
                //                        }
                //                    }
                //                }
                //                if (taxlineDetail.PercentBased != null)
                //                {
                //                    taxLineDetail.PercentBased = Convert.ToBoolean(taxlineDetail.PercentBased);
                //                    taxLineDetail.PercentBasedSpecified = true;
                //                }
                //                if (taxlineDetail.TaxPercent != null)
                //                {
                //                    taxLineDetail.TaxPercent = Convert.ToDecimal(taxlineDetail.TaxPercent);
                //                    taxLineDetail.TaxPercentSpecified = true;
                //                }
                //            }

                //            taxLine.AnyIntuitObject = taxLineDetail;
                //        }

                //        if (taxLine.AnyIntuitObject != null)
                //        {
                //            txnTaxDetail.TaxLine = new Intuit.Ipp.Data.Line[] { taxLine };
                //            txnTaxDetail.TotalTax = Convert.ToDecimal(txntaxCode.TotalTax);
                //        }
                //        addedPurchase.TxnTaxDetail = txnTaxDetail;

                //        #endregion
                //    }
                //}

                #endregion
                #endregion

                CommonUtilities.GetInstance().newSKUItem = false;
                purchaseAdded = new Intuit.Ipp.Data.Purchase();
                purchaseAdded = dataService.Add<Intuit.Ipp.Data.Purchase>(addedPurchase);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (purchaseAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = purchaseAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = purchaseAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

        /// <summary>
        /// updating Purchase entry in quickbook online by the transaction Id
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="salesReceiptid"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOPurchase(OnlineDataProcessingsImportClass.OnlinePurchaseQBEntry coll, string salesReceiptid, string syncToken, Intuit.Ipp.Data.Purchase PreviousData = null)
        {
            bool flag = false;
            int linecount = 1;
            int linecount1 = 0;
            Intuit.Ipp.Data.Purchase purchaseAdded = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.Purchase addedPurchase = new Intuit.Ipp.Data.Purchase();
            OnlinePurchaseQBEntry purchase = new OnlinePurchaseQBEntry();
            purchase = coll;
            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];


            #region tax member
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            int array_count = 0;
            bool taxFlag = true;
            #endregion

            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            try
            {
                if (purchase.isAppend)
                {
                    addedPurchase = PreviousData;

                    if (addedPurchase.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedPurchase.Line.Length;
                        lines_online = addedPurchase.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                    addedPurchase.sparse = true;
                    addedPurchase.sparseSpecified = true;
                }
                addedPurchase.Id = salesReceiptid;
                addedPurchase.SyncToken = syncToken;

                #region Add Purchase

                //594
               

                if (purchase.DocNumber != null)
                {
                    addedPurchase.DocNumber = purchase.DocNumber;
                }

                if (purchase.TxnDate != null)
                {
                    try
                    {
                        addedPurchase.TxnDate = Convert.ToDateTime(purchase.TxnDate);
                        addedPurchase.TxnDateSpecified = true;
                    }
                    catch { }
                }

                if (purchase.PrivateNote != null)
                {
                    addedPurchase.PrivateNote = purchase.PrivateNote;
                }

                // Axis 740
                foreach (var paymentMethod in purchase.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedPurchase.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }
                // Axis 740 end

                //bug no. 418
                #region find DepartmentRef
                string DepartmentName = string.Empty;
                foreach (var dept in purchase.DepartmentRef)
                {
                    DepartmentName = dept.Name;
                }
                string Departmentvalue = string.Empty;
                if (DepartmentName != string.Empty)
                {
                    var dataDepartmentName = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentName.Trim());
                    foreach (var d in dataDepartmentName)
                    {
                        Departmentvalue = d.Id;
                    }
                    addedPurchase.DepartmentRef = new ReferenceType()
                    {
                        name = DepartmentName,
                        Value = Departmentvalue
                    };
                }

                #endregion

                #region find ExchangeRate CurrencyRef
                if (purchase.ExchangeRate != null)
                {
                    addedPurchase.ExchangeRate = Convert.ToDecimal(purchase.ExchangeRate);
                    addedPurchase.ExchangeRateSpecified = true;
                }

                if (purchase.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in purchase.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedPurchase.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }

                #endregion

                // Axis 742
                if (purchase.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(purchase.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedPurchase.TransactionLocationType = value;
                    }
                    else
                    {
                        addedPurchase.TransactionLocationType = purchase.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (purchase.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedPurchase.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedPurchase.GlobalTaxCalculationSpecified = true;
                        }
                        else if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedPurchase.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedPurchase.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedPurchase.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedPurchase.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion
                foreach (var l in purchase.Line)
                {
                    if (flag == false)
                    {
                        #region
                        flag = true;
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }


                                // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{  
                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }
                                }

                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                #region TaxCodeRef
                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }
                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                    
                                }

                                #region TaxCodeRef
                                //Bug 480
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Value,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchase.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        linecount++;

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                string displayname = string.Empty;

                                //  // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{
                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                          
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }
                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                       
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                #region TaxCodeRef
                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                           
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }
                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchase.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                    if (accountData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                    if (customerData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }

                                #region TaxCodeRef
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchase.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }



                foreach (var AccountRef in purchase.AccountRef)
                {
                    if (AccountRef.Name != null && AccountRef.Name != string.Empty && AccountRef.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(AccountRef.Name);
                        addedPurchase.AccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                  
                }

                if (purchase.PaymentType == PaymentTypeEnum.Cash.ToString())
                {
                    addedPurchase.PaymentType = PaymentTypeEnum.Cash;
                    addedPurchase.PaymentTypeSpecified = true;
                }
                else if (purchase.PaymentType == PaymentTypeEnum.Check.ToString())
                {
                    addedPurchase.PaymentType = PaymentTypeEnum.Check;
                    addedPurchase.PaymentTypeSpecified = true;
                }
                else if (purchase.PaymentType == PaymentTypeEnum.CreditCard.ToString())
                {
                    addedPurchase.PaymentType = PaymentTypeEnum.CreditCard;
                    addedPurchase.PaymentTypeSpecified = true;
                }



                if (purchase.Credit != null)
                {
                    addedPurchase.Credit = Convert.ToBoolean(purchase.Credit);
                    addedPurchase.CreditSpecified = true;
                }
                if (purchase.PrintStatus != null)
                {
                    if (purchase.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedPurchase.PrintStatus = PrintStatusEnum.NotSet;
                        addedPurchase.PrintStatusSpecified = true;
                    }
                    else if (purchase.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedPurchase.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedPurchase.PrintStatusSpecified = true;
                    }
                    else if (purchase.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedPurchase.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedPurchase.PrintStatusSpecified = true;
                    }
                }

                #region find EntityRef

                Intuit.Ipp.Data.EntityTypeRef entityref = new EntityTypeRef();
                foreach (var ent in purchase.Entity)
                {
                    if (ent.Type == EntityTypeEnum.Customer.ToString())
                    {
                        string id = string.Empty;
                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(ent.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addedPurchase.EntityRef = new ReferenceType()
                        {
                            name = ent.Name,
                            Value = id,
                            type = EntityTypeEnum.Customer.ToString()
                        };
                    }
                    else if (ent.Type == EntityTypeEnum.Vendor.ToString() || ent.Type == null)
                    {
                        string id = string.Empty;
                        var dataItem = await CommonUtilities.GetVendorExistsInOnlineQBO(ent.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addedPurchase.EntityRef = new ReferenceType()
                        {
                            name = ent.Name,
                            Value = id,
                            type = EntityTypeEnum.Vendor.ToString()
                        };
                    }
                    else if (ent.Type == EntityTypeEnum.Employee.ToString())
                    {
                        string id = string.Empty;
                        var dataItem = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(ent.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addedPurchase.EntityRef = new ReferenceType()
                        {
                            name = ent.Name,
                            Value = id,
                            type = EntityTypeEnum.Employee.ToString()
                        };
                    }
                }

                #endregion



                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedPurchase.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && purchase.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            decimal TotalTax = 0;
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);

                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;

                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                if (TotalTax != 0)
                                {
                                    txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                    txnTaxDetail.TotalTaxSpecified = true;
                                }
                                addedPurchase.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //BUG ;; 586
                foreach (var txntaxCode in purchase.TxnTaxDetail)
                {
                    #region TxnTaxDetail
                   
                    //Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                      
                    if (txntaxCode.TxnTaxCodeRef != null)
                    {
                        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                        {
                            TaxCode TaxCode = new TaxCode();
                            TaxCode.Name = taxRef.Name;
                            string Taxvalue = string.Empty;
                            string TaxName = string.Empty;
                            TaxName = TaxCode.Name;
                            if (TaxCode.Name != null)
                            {
                                var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                if (dataTax != null)
                                {
                                    foreach (var tax in dataTax)
                                    {
                                        Taxvalue = tax.Id;
                                    }

                                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                    {
                                        name = TaxName,
                                        Value = Taxvalue
                                    };
                                }
                            }

                        }
                    }

                    if (txnTaxDetail != null)
                    {
                        addedPurchase.TxnTaxDetail = txnTaxDetail;
                    }
                    #endregion
                }

                //if (CommonUtilities.GetInstance().CountryVersion == "US" || CommonUtilities.GetInstance().GrossNet == false || addedPurchase.GlobalTaxCalculation.ToString() != GlobalTaxCalculationEnum.TaxInclusive.ToString())
                //{
                //    foreach (var txntaxCode in purchase.TxnTaxDetail)
                //    {
                //        #region TxnTaxDetail
                //        ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                //        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                //        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                //        if (txntaxCode.TxnTaxCodeRef != null)
                //        {
                //            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                //            {
                //                QueryService<TaxCode> taxCodeQueryService = new QueryService<TaxCode>(serviceContext);
                //                TaxCode TaxCode = new TaxCode();
                //                TaxCode.Name = taxRef.Name;
                //                string Taxvalue = string.Empty;
                //                string TaxName = string.Empty;
                //                TaxName = TaxCode.Name;
                //                if (TaxCode.Name != null)
                //                {
                //                    if (TaxCode.Name.Contains("'"))
                //                    {
                //                        TaxCode.Name = TaxCode.Name.Replace("'", "\\'");
                //                    }
                //                    dataTax = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(taxCodeQueryService.ExecuteIdsQuery("Select * From TaxCode where Name='" + TaxCode.Name.Trim() + "'"));
                //                    if (dataTax != null)
                //                    {
                //                        foreach (var tax in dataTax)
                //                        {
                //                            Taxvalue = tax.Id;
                //                        }

                //                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                //                        {
                //                            name = TaxName,
                //                            Value = Taxvalue
                //                        };
                //                    }
                //                }
                //            }
                //        }

                //        foreach (var txnline in txntaxCode.TaxLine)
                //        {
                //            if (txnline != null)
                //            {
                //                taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                //                taxLine.DetailTypeSpecified = true;
                //            }

                //            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();

                //            foreach (var taxlineDetail in txnline.TaxLineDetail)
                //            {
                //                if (dataTax != null)
                //                {
                //                    foreach (var taxRateRef in dataTax)
                //                    {
                //                        foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                //                        {
                //                            taxLineDetail.TaxRateRef = taxRate.TaxRateRef;
                //                        }
                //                    }
                //                }
                //                if (taxlineDetail.PercentBased != null)
                //                {
                //                    taxLineDetail.PercentBased = Convert.ToBoolean(taxlineDetail.PercentBased);
                //                    taxLineDetail.PercentBasedSpecified = true;
                //                }
                //                if (taxlineDetail.TaxPercent != null)
                //                {
                //                    taxLineDetail.TaxPercent = Convert.ToDecimal(taxlineDetail.TaxPercent);
                //                    taxLineDetail.TaxPercentSpecified = true;
                //                }
                //            }

                //            taxLine.AnyIntuitObject = taxLineDetail;
                //        }

                //        if (taxLine.AnyIntuitObject != null)
                //        {
                //            txnTaxDetail.TaxLine = new Intuit.Ipp.Data.Line[] { taxLine };
                //            txnTaxDetail.TotalTax = Convert.ToDecimal(txntaxCode.TotalTax);
                //        }
                //        addedPurchase.TxnTaxDetail = txnTaxDetail;

                //        #endregion
                //    }
                //}

                #endregion
                #endregion

                purchaseAdded = new Intuit.Ipp.Data.Purchase();
                purchaseAdded = dataService.Update<Intuit.Ipp.Data.Purchase>(addedPurchase);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (purchaseAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = purchaseAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = purchaseAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

    }

    /// <summary>
    /// checking doc number is present or not in quickbook online for Purchase.
    /// </summary>
    public class OnlinePurchaseQBEntryCollection : Collection<OnlinePurchaseQBEntry>
    {
        public OnlinePurchaseQBEntry FindPurchaseNumberEntry(string docNumber)
        {
            foreach (OnlinePurchaseQBEntry item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }

}
