﻿using System;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("JournalEntry", Namespace = "", IsNullable = false)]

    public class JournalEntryclass
    {

        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TxnDate;
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.JournalEntryLineDetail> m_JournalEntryBased = new Collection<OnlineEntities.JournalEntryLineDetail>();
        private Collection<OnlineEntities.AccountRef> m_AccountRef = new Collection<OnlineEntities.AccountRef>();
        private Collection<OnlineEntities.ClassRef> m_ClassRef = new Collection<OnlineEntities.ClassRef>();
        
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private decimal m_CrAmount = 0;
        private decimal m_DrAmount = 0;
        //586
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        //Axis 701
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        private string m_ExchangeRate;
        //Axis 701 ends
        //594
        private bool m_isAppend;

        #endregion

        #region Constructor
        public JournalEntryclass()
        {
        }
        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.JournalEntryLineDetail> JournalEntryBased
        {
            get { return m_JournalEntryBased; }
            set { m_JournalEntryBased = value; }
        }

        public Collection<OnlineEntities.AccountRef> AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }
        public Collection<OnlineEntities.ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }
        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }
        //586
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }

        //Axis 701
        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }
        //Axis 701 ends

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }


        #endregion

        #region  Public Methods
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        /// Creating new Journal transaction in quickbook online. 
        public async System.Threading.Tasks.Task<bool> CreateQBOJournal(OnlineDataProcessingsImportClass.JournalEntryclass coll)
        {

            int linecount = 1;
            int linecount1 = 0;
            DataService dataService = null;
            Intuit.Ipp.Data.JournalEntry journalentryAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.JournalEntry addedJournalEntry = new Intuit.Ipp.Data.JournalEntry();
            JournalEntryclass journalentry = new JournalEntryclass();
            journalentry = coll;
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            #region tax member
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            int array_count = 0;
            bool taxFlag = false;
            #endregion

            string taxapplicableon = string.Empty;
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists 

            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST  
            try
            {
                #region Add addedJournalEntry

                ////594
                //if (journalentry.isAppend == true)
                //{
                //    addedJournalEntry.sparse = true;
                //}

                if (journalentry.DocNumber != null)
                {
                    addedJournalEntry.DocNumber = journalentry.DocNumber;
                }

                if (journalentry.TxnDate != null)
                {
                    try
                    {
                        addedJournalEntry.TxnDate = Convert.ToDateTime(journalentry.TxnDate);
                        addedJournalEntry.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (journalentry.PrivateNote != null)
                {
                    addedJournalEntry.PrivateNote = journalentry.PrivateNote;
                }
                Intuit.Ipp.Data.JournalEntryLineDetail JournalEntryLineDetail = new Intuit.Ipp.Data.JournalEntryLineDetail();
                bool flag = false;

                #region Line Tag
                foreach (var l in journalentry.Line)
                {
                    if (flag == false)
                    {
                        flag = true;
                        Line.Description = l.LineDescription;
                        Line.AmountSpecified = true;
                        if (l.JournalEntryLineDetail.Count > 0)
                        {
                            Line.DetailType = LineDetailTypeEnum.JournalEntryLineDetail;
                            Line.DetailTypeSpecified = true;

                            foreach (var journalentryBased in l.JournalEntryLineDetail)
                            {
                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                                string accountNumber = string.Empty;
                                string accountName = string.Empty;
                                foreach (var acc in journalentryBased.AccountRef)
                                {
                                    accountName = acc.Name;
                                   // accountNumber = acc.AcctNum;
                                }
                                if (accountName != string.Empty && accountName != null && accountName != "")
                                {
                                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                                    JournalEntryLineDetail.AccountRef = new ReferenceType()
                                    {
                                        name = accDetails.name,
                                        Value = accDetails.Value
                                    };
                                }
                                
                                #endregion

                                #region BillableStatus
                                if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    JournalEntryLineDetail.BillableStatusSpecified = true;
                                }
                                else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    JournalEntryLineDetail.BillableStatusSpecified = true;
                                }
                                else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    JournalEntryLineDetail.BillableStatusSpecified = true;
                                }

                                #endregion

                                #region posting type

                                if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Credit.ToString())
                                {
                                    JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Credit;
                                    Line.Amount = Convert.ToDecimal(journalentryBased.Credit);
                                    //595
                                   // Line.Amount = -Line.Amount;
                                }
                                else if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Debit.ToString())
                                {
                                    JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Debit;
                                    Line.Amount = Convert.ToDecimal(journalentryBased.Debit);

                                }
                                JournalEntryLineDetail.PostingTypeSpecified = true;
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in journalentryBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            // bug 458
                                            foreach (var TaxDetails in journalentryBased.TxnTaxDetail)
                                            {
                                                taxapplicableon = TaxDetails.TaxApplicableOn;
                                                if (taxapplicableon != null)
                                                {
                                                    if (taxapplicableon.ToLower() == "sales" || taxapplicableon.ToLower() == "purchase")
                                                    {
                                                        if (TaxDetails.TaxApplicableOn.ToLower() == "sales")
                                                        {

                                                            JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                            JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Sales;
                                                        }
                                                        if (TaxDetails.TaxApplicableOn.ToLower() == "purchase")
                                                        {
                                                            JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                            JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Purchase;
                                                        }
                                                    }
                                                }
                                                JournalEntryLineDetail.TaxAmount = (Convert.ToDecimal(TaxDetails.TaxAmount));
                                                JournalEntryLineDetail.TaxAmountSpecified = true;

                                            }

                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                           
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }

                                            //if (taxapplicableon.ToLower() == TaxApplicableOnEnum.Purchase.ToString().ToLower() || taxapplicableon.ToLower() == TaxApplicableOnEnum.Purchase.ToString().ToLower())
                                            //{
                                            //    if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Purchase)
                                            //    {
                                            //        foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            //        {
                                            //            if (taxRate != null)
                                            //            {
                                            //                GstRef = taxRate.TaxRateRef;
                                            //                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            //            }
                                            //        }
                                            //    }

                                            //    if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Sales)
                                            //    {
                                            //        foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            //        {
                                            //            if (taxRate != null)
                                            //            {
                                            //                GstRef = taxRate.TaxRateRef;
                                            //                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            //            }
                                            //        }
                                            //    }
                                            //}



                                            //string taxValue = string.Empty;
                                            //ReadOnlyCollection<Intuit.Ipp.Data.TaxRate> dataTaxRate = null;
                                            //QueryService<TaxRate> taxQueryService = new QueryService<TaxRate>(serviceContext);

                                            //if (GSTtaxRateRefName.Contains("'"))
                                            //{
                                            //    GSTtaxRateRefName = GSTtaxRateRefName.Replace("'", "\\'");
                                            //}
                                            //dataTaxRate = new ReadOnlyCollection<Intuit.Ipp.Data.TaxRate>(taxQueryService.ExecuteIdsQuery("Select * From TaxRate where Name='" + GSTtaxRateRefName + "'"));
                                            //foreach (var dTaxRate in dataTaxRate)
                                            //{
                                            //    taxValue = dTaxRate.RateValue.ToString();
                                            //}

                                            //if (taxValue != "")
                                            //{
                                            //    JournalEntryLineDetail.TaxAmountSpecified = true;
                                            //    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                            //}
                                            //else
                                            //{
                                            //    JournalEntryLineDetail.TaxAmountSpecified = true;
                                            //    taxValue = "0";
                                            //    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                            //}


                                            //}
                                        }
                                        JournalEntryLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region find classRef
                                string className = string.Empty;
                                foreach (var clas in journalentryBased.ClassRef)
                                {
                                    className = clas.Name;
                                }
                                string classNamevalue = string.Empty;
                                if (className != string.Empty)
                                {
                                    var dataclassName = await CommonUtilities.GetClassExistsInOnlineQBO(className.Trim());
                                    foreach (var dclass in dataclassName)
                                    {
                                        classNamevalue = dclass.Id;
                                    }
                                    JournalEntryLineDetail.ClassRef = new ReferenceType()
                                    {
                                        name = className,
                                        Value = classNamevalue
                                    };
                                }

                                #endregion

                                #region find DepartmentRef
                                string DepartmentName = string.Empty;
                                foreach (var dept in journalentryBased.DepartmentRef)
                                {
                                    DepartmentName = dept.Name;
                                }
                                string Departmentvalue = string.Empty;
                                if (DepartmentName != string.Empty)
                                {
                                    var dataDepartmentName = await CommonUtilities.GetDepartmentFullyQualifyNameInOnlineQBO(DepartmentName.Trim());
                                    foreach (var d in dataDepartmentName)
                                    {
                                        Departmentvalue = d.Id;
                                    }
                                    JournalEntryLineDetail.DepartmentRef = new ReferenceType()
                                    {
                                        name = DepartmentName,
                                        Value = Departmentvalue
                                    };
                                }

                                #endregion

                                #region find EntityRef

                                Intuit.Ipp.Data.EntityTypeRef entityref = new EntityTypeRef();
                                foreach (var ent in journalentryBased.Entity)
                                {
                                    if (ent.Type == EntityTypeEnum.Customer.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Customer;
                                        entityref.TypeSpecified = true;

                                        #region CustomerRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                    else if (ent.Type == EntityTypeEnum.Vendor.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Vendor;
                                        entityref.TypeSpecified = true;

                                        #region vendorRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetVendorExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }

                                    else if (ent.Type == EntityTypeEnum.Employee.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Employee;
                                        entityref.TypeSpecified = true;

                                        #region EmployeeRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                }
                                if (entityref.EntityRef != null)
                                {
                                    JournalEntryLineDetail.Entity = entityref;
                                }
                                #endregion

                                Line.AnyIntuitObject = JournalEntryLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedJournalEntry.Line = AddLine(lines_online, Line, linecount);
                    }
                    else if (flag == true)
                    {
                        Line = new Intuit.Ipp.Data.Line();
                        JournalEntryLineDetail = new Intuit.Ipp.Data.JournalEntryLineDetail();
                        linecount++;
                        Line.Description = l.LineDescription;
                        Line.AmountSpecified = true;
                        if (l.JournalEntryLineDetail.Count > 0)
                        {
                            Line.DetailType = LineDetailTypeEnum.JournalEntryLineDetail;
                            Line.DetailTypeSpecified = true;
                            #region line data
                            foreach (var journalentryBased in l.JournalEntryLineDetail)
                            {

                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                                string accountName = string.Empty;
                                string accountNumber = string.Empty;
                                foreach (var acc in journalentryBased.AccountRef)
                                {
                                    accountName = acc.Name;
                                    //accountNumber = acc.AcctNum;
                                }
                                if (accountName != string.Empty && accountName != null && accountName != "")
                                {
                                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                                    JournalEntryLineDetail.AccountRef = new ReferenceType()
                                    {
                                        name = accDetails.name,
                                        Value = accDetails.Value
                                    };
                                }
                                                         
                                
                                #endregion

                                #region BillableStatus
                                if (journalentryBased.BillableStatus != null)
                                {
                                    if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        JournalEntryLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        JournalEntryLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        JournalEntryLineDetail.BillableStatusSpecified = true;
                                    }
                                }

                                #endregion

                                #region posting type
                                if (journalentryBased.PostingType != null)
                                {
                                    if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Credit.ToString())
                                    {
                                        JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Credit;
                                        Line.Amount = Convert.ToDecimal(journalentryBased.Credit);
                                    }
                                    else if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Debit.ToString())
                                    {
                                        JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Debit;
                                        Line.Amount = Convert.ToDecimal(journalentryBased.Debit);

                                    }
                                    JournalEntryLineDetail.PostingTypeSpecified = true;
                                }
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in journalentryBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {


                                            //bug 458
                                            foreach (var TaxDetails in journalentryBased.TxnTaxDetail)
                                            {
                                                taxapplicableon = TaxDetails.TaxApplicableOn.ToLower();
                                                if (taxapplicableon == "sales" || taxapplicableon == "purchase")
                                                {
                                                    if (TaxDetails.TaxApplicableOn.ToLower() == "sales")
                                                    {
                                                        JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                        JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Sales;
                                                    }
                                                    if (TaxDetails.TaxApplicableOn.ToLower() == "purchase")
                                                    {
                                                        JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                        JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Purchase;
                                                    }
                                                }
                                                JournalEntryLineDetail.TaxAmount = (Convert.ToDecimal(TaxDetails.TaxAmount));
                                                JournalEntryLineDetail.TaxAmountSpecified = true;
                                            }

                                            int i = 0;
                                            for (i = 0; i < array_count; i++)
                                            {
                                                if (taxCoderef.Name == Array_taxcodeName[i])
                                                {
                                                    taxFlag = false;
                                                    break;
                                                }
                                                else
                                                {
                                                    taxFlag = true;

                                                }
                                            }
                                            if (taxFlag == false)
                                            {
                                                if (taxCoderef.Name == Array_taxcodeName[i])
                                                {
                                                    Array_taxAmount[i] += Line.Amount;
                                                }
                                            }
                                            else
                                            {
                                                array_count = array_count + 1;
                                                Array.Resize(ref Array_taxcodeName, array_count);
                                                Array.Resize(ref Array_taxAmount, array_count);
                                                Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                Array_taxAmount[array_count - 1] = Line.Amount;
                                                taxFlag = false;
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                                //if (taxapplicableon.ToLower() == TaxApplicableOnEnum.Purchase.ToString().ToLower() || taxapplicableon.ToLower() == TaxApplicableOnEnum.Purchase.ToString().ToLower())
                                                //{
                                                //    if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Purchase)
                                                //    {

                                                //        foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                                //        {
                                                //            GstRef = taxRate.TaxRateRef;
                                                //            GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                                //        }
                                                //    }

                                                //    if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Sales)
                                                //    {

                                                //        foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                                //        {
                                                //            GstRef = taxRate.TaxRateRef;
                                                //            GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                                //        }
                                                //    }
                                                //}

                                                //string taxValue = string.Empty;
                                                //ReadOnlyCollection<Intuit.Ipp.Data.TaxRate> dataTaxRate = null;
                                                //QueryService<TaxRate> taxQueryService = new QueryService<TaxRate>(serviceContext);
                                                //if (GSTtaxRateRefName.Contains("'"))
                                                //{
                                                //    GSTtaxRateRefName = GSTtaxRateRefName.Replace("'", "\\'");
                                                //}
                                                //dataTaxRate = new ReadOnlyCollection<Intuit.Ipp.Data.TaxRate>(taxQueryService.ExecuteIdsQuery("Select * From TaxRate where Name='" + GSTtaxRateRefName + "'"));
                                                //foreach (var dTaxRate in dataTaxRate)
                                                //{
                                                //    taxValue = dTaxRate.RateValue.ToString();
                                                //}

                                                //if (taxValue != "")
                                                //{
                                                //    JournalEntryLineDetail.TaxAmountSpecified = true;
                                                //    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                                //}
                                                //else
                                                //{
                                                //    taxValue = "0";
                                                //    JournalEntryLineDetail.TaxAmountSpecified = true;
                                                //    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                                //}
                                                //JournalEntryLineDetail.TaxAmountSpecified = true;
                                            }




                                        }

                                        JournalEntryLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region find classRef
                               
                                string className = string.Empty;
                                foreach (var clas in journalentryBased.ClassRef)
                                {
                                    className = clas.Name;
                                }
                                string classNamevalue = string.Empty;
                                if (className != string.Empty)
                                {
                                    var dataclassName = await CommonUtilities.GetClassExistsInOnlineQBO(className.Trim());
                                    foreach (var dclass in dataclassName)
                                    {
                                        classNamevalue = dclass.Id;
                                    }
                                    JournalEntryLineDetail.ClassRef = new ReferenceType()
                                    {
                                        name = className,
                                        Value = classNamevalue
                                    };
                                }

                                #endregion

                                #region find DepartmentRef
                                string DepartmentName = string.Empty;
                                foreach (var dept in journalentryBased.DepartmentRef)
                                {
                                    DepartmentName = dept.Name;
                                }
                                string Departmentvalue = string.Empty;
                                if (DepartmentName != string.Empty)
                                {
                                    var dataDepartmentName = await CommonUtilities.GetDepartmentFullyQualifyNameInOnlineQBO(DepartmentName.Trim());
                                    foreach (var d in dataDepartmentName)
                                    {
                                        Departmentvalue = d.Id;
                                    }
                                    JournalEntryLineDetail.DepartmentRef = new ReferenceType()
                                    {
                                        name = DepartmentName,
                                        Value = Departmentvalue
                                    };
                                }

                                #endregion

                                #region find EntityRef

                                Intuit.Ipp.Data.EntityTypeRef entityref = new EntityTypeRef();
                                foreach (var ent in journalentryBased.Entity)
                                {
                                    if (ent.Type == EntityTypeEnum.Customer.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Customer;
                                        entityref.TypeSpecified = true;

                                        #region CustomerRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                    else if (ent.Type == EntityTypeEnum.Vendor.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Vendor;
                                        entityref.TypeSpecified = true;

                                        #region vendorRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetVendorExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }

                                    else if (ent.Type == EntityTypeEnum.Employee.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Employee;
                                        entityref.TypeSpecified = true;

                                        #region EmployeeRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                }
                                if (entityref.EntityRef != null)
                                {
                                    JournalEntryLineDetail.Entity = entityref;
                                }
                                #endregion

                                Line.AnyIntuitObject = JournalEntryLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                                addedJournalEntry.Line = AddLine(lines_online, Line, linecount);

                            }
                            #endregion
                        }

                    }

                }

                #endregion

                //Axis 701
                if (journalentry.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in journalentry.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    addedJournalEntry.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (journalentry.ExchangeRate != null)
                {
                    addedJournalEntry.ExchangeRate = Convert.ToDecimal(journalentry.ExchangeRate);
                    addedJournalEntry.ExchangeRateSpecified = true;
                }
                //Axis 701 ends
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)
                    #region TxnTaxDetail

                    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    
                    Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                    string taxid = string.Empty;
                    for (int i = 0; i < array_count; i++)
                    {
                        string taxCode = string.Empty;
                        taxCode = Array_taxcodeName[i].ToString();
                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                        foreach (var tax in dataTaxcode)
                        {
                            if (taxapplicableon != null)
                            {
                                if (taxapplicableon.ToLower() == TaxApplicableOnEnum.Purchase.ToString().ToLower() || taxapplicableon.ToLower() == TaxApplicableOnEnum.Sales.ToString().ToLower())
                                {
                                    //if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Sales)
                                    //bug 458
                                    if (taxapplicableon.ToLower().CompareTo("sales") == 0)
                                    {
                                        if (tax.SalesTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }
                                    //if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Purchase)
                                    //bug 458
                                    else if (taxapplicableon.ToLower().CompareTo("purchase") == 0)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                       
                                    }
                                    if (tax.SalesTaxRateList.TaxRateDetail != null || tax.PurchaseTaxRateList.TaxRateDetail != null)
                                    {
                                        taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                        taxLine.DetailTypeSpecified = true;

                                        taxLineDetail.PercentBased = true;
                                        taxLineDetail.PercentBasedSpecified = true;


                                        taxLineDetail.TaxRateRef = GstRef;

                                        decimal s = 0;
                                        string taxValue = string.Empty;
                                        var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                        foreach (var dTaxRate in dataTaxRate)
                                        {
                                            taxValue = dTaxRate.RateValue.ToString();
                                            taxid = dTaxRate.Id.ToString();
                                        }
                                        if (taxValue != "")
                                        {
                                            s = Convert.ToDecimal(taxValue);

                                        }
                                        else
                                        {
                                            taxValue = "0";
                                            s = Convert.ToDecimal(taxValue);
                                        }

                                        taxLineDetail.TaxPercent = s;

                                        decimal taxVal = (s) / 100;
                                      //taxLineDetail.TaxPercent = decimal.Round(taxVal, 2);

                                        taxLineDetail.TaxPercentSpecified = true;

                                        taxLine.Amount = decimal.Round(Array_taxAmount[i] * taxVal, 2);
                                        //Bug;;595
                                        taxLine.Amount = -taxLine.Amount;
                                        taxLine.AmountSpecified = true;

                                        taxLineDetail.NetAmountTaxable = Array_taxAmount[i];
                                        //Bug;;595
                                        taxLineDetail.NetAmountTaxable = -taxLineDetail.NetAmountTaxable;
                                        taxLineDetail.NetAmountTaxableSpecified = true;

                                        taxLine.AnyIntuitObject = taxLineDetail;
                                        linecount1++;
                                        Array.Resize(ref lines_online1, linecount1);
                                        lines_online1[linecount1 - 1] = taxLine;
                                        txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);

                                        taxLine = new Intuit.Ipp.Data.Line();
                                        taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                        if (txnTaxDetail != null)
                                        {
                                            addedJournalEntry.TxnTaxDetail = txnTaxDetail;
                                        }
                                    }
                                }
                            }

                        }

                        //taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                        //taxLine.DetailTypeSpecified = true;

                        //taxLineDetail.PercentBased = true;
                        //taxLineDetail.PercentBasedSpecified = true;


                        //taxLineDetail.TaxRateRef = GstRef;

                        //decimal s = 0;
                        //string taxValue = string.Empty;
                        //ReadOnlyCollection<Intuit.Ipp.Data.TaxRate> dataTaxRate = null;
                        //QueryService<TaxRate> taxQueryService = new QueryService<TaxRate>(serviceContext);
                        //if (GSTtaxRateRefName.Contains("'"))
                        //{
                        //    GSTtaxRateRefName = GSTtaxRateRefName.Replace("'", "\\'");
                        //}
                        //dataTaxRate = new ReadOnlyCollection<Intuit.Ipp.Data.TaxRate>(taxQueryService.ExecuteIdsQuery("Select * From TaxRate where Name='" + GSTtaxRateRefName + "'"));
                        //foreach (var dTaxRate in dataTaxRate)
                        //{
                        //    taxValue = dTaxRate.RateValue.ToString();
                        //    taxid = dTaxRate.Id.ToString();
                        //}
                        //if (taxValue != "")
                        //{
                        //    s = Convert.ToDecimal(taxValue);

                        //}
                        //else
                        //{
                        //    taxValue = "0";
                        //    s = Convert.ToDecimal(taxValue);
                        //}
                        //decimal taxVal = (s) / 100;
                        //taxLineDetail.TaxPercent = decimal.Round(taxVal, 2);
                        //taxLineDetail.TaxPercentSpecified = true;

                        //taxLine.Amount = decimal.Round(Array_taxAmount[i] * taxLineDetail.TaxPercent, 2);
                        //taxLine.AmountSpecified = true;

                        //taxLineDetail.NetAmountTaxable = Array_taxAmount[i];
                        //taxLineDetail.NetAmountTaxableSpecified = true;

                        //taxLine.AnyIntuitObject = taxLineDetail;
                        //linecount1++;
                        //Array.Resize(ref lines_online1, linecount1);
                        //lines_online1[linecount1 - 1] = taxLine;
                        //txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);

                        //taxLine = new Intuit.Ipp.Data.Line();
                        //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                        //if (txnTaxDetail != null)
                        //{
                        //    addedJournalEntry.TxnTaxDetail = txnTaxDetail;
                        //}



                    }

                    #endregion
                }

                journalentryAdded = new Intuit.Ipp.Data.JournalEntry();
                journalentryAdded = dataService.Add<Intuit.Ipp.Data.JournalEntry>(addedJournalEntry);

                #endregion
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (journalentryAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = journalentryAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = journalentryAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }




        ///  updating Journal entry in quickbook online by the transaction Id..
        public async System.Threading.Tasks.Task<bool> UpdateQBOJournalEntry(OnlineDataProcessingsImportClass.JournalEntryclass coll, string journalEntryid, string syncToken, Intuit.Ipp.Data.JournalEntry PreviousData=null)
        {
            int linecount = 1;
            int linecount1 = 0;
            DataService dataService = null;
            Intuit.Ipp.Data.JournalEntry journalentryAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.JournalEntry addedJournalEntry = new Intuit.Ipp.Data.JournalEntry();
            JournalEntryclass journalentry = new JournalEntryclass();
            journalentry = coll;
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            #region tax member
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            int array_count = 0;
            bool taxFlag = false;
            #endregion

            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists           
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST 
            bool flag = false;
            try
            {

                #region Add addedJournalEntry

                addedJournalEntry.Id = journalEntryid;
                addedJournalEntry.SyncToken = syncToken;

                //594
                if (journalentry.isAppend == true)
                {
                  
                    addedJournalEntry = PreviousData;
                    addedJournalEntry.sparse = true;
                    addedJournalEntry.sparseSpecified = true;
                    string TaxCodeName = string.Empty;
                    decimal TaxAmount = 0;
                    if (addedJournalEntry.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedJournalEntry.Line.Length;
                        lines_online = addedJournalEntry.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                    foreach (var line in addedJournalEntry.Line)
                    {
                        // Intuit.Ipp.Data.Line line = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.JournalEntryLineDetail line1 = new Intuit.Ipp.Data.JournalEntryLineDetail();

                        line1 = (Intuit.Ipp.Data.JournalEntryLineDetail)line.AnyIntuitObject;
                      
                        TaxCodeName = string.Empty;
                        if (line1.TaxCodeRef != null)
                        {
                            var dataTaxcode = await CommonUtilities.GetTaxCodeIdDetailsExistsInOnlineQBO(line1.TaxCodeRef.Value);
                            foreach (var tax in dataTaxcode)
                            {
                                TaxCodeName = tax.Name;
                                break;
                            }
                            TaxAmount = line.Amount;
                            //  TaxCodeName = line1.TaxCodeRef.name;
                            if (array_count == 0)
                            {
                                Array_taxcodeName[0] = TaxCodeName;
                                array_count++;
                                Array_taxAmount[0] = TaxAmount;
                            }
                            else
                            {
                                int i = 0;
                                for (i = 0; i < array_count; i++)
                                {
                                    if (TaxCodeName == Array_taxcodeName[i])
                                    {
                                        taxFlag = false;
                                        break;
                                    }
                                    else
                                    {
                                        taxFlag = true;
                                    }
                                }
                                if (taxFlag == false)
                                {
                                    if (TaxCodeName == Array_taxcodeName[i])
                                    {
                                        Array_taxAmount[i] += TaxAmount;
                                    }
                                }
                                else
                                {
                                    array_count = array_count + 1;
                                    Array.Resize(ref Array_taxcodeName, array_count);
                                    Array.Resize(ref Array_taxAmount, array_count);
                                    Array_taxcodeName[array_count - 1] = TaxCodeName;
                                    Array_taxAmount[array_count - 1] = TaxAmount;
                                    taxFlag = false;
                                }
                            }
                           

                        }
                    }
                  

                   


                }

                if (journalentry.DocNumber != null)
                {
                    addedJournalEntry.DocNumber = journalentry.DocNumber;
                }

                if (journalentry.TxnDate != null)
                {
                    try
                    {
                        addedJournalEntry.TxnDate = Convert.ToDateTime(journalentry.TxnDate);
                        addedJournalEntry.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (journalentry.PrivateNote != null)
                {
                    addedJournalEntry.PrivateNote = journalentry.PrivateNote;
                }
                Intuit.Ipp.Data.JournalEntryLineDetail JournalEntryLineDetail = new Intuit.Ipp.Data.JournalEntryLineDetail();
              

                #region Line Tag
                foreach (var l in journalentry.Line)
                {
                    if (flag == false)
                    {
                        flag = true;
                        Line.Description = l.LineDescription;
                        Line.AmountSpecified = true;
                        if (l.JournalEntryLineDetail.Count > 0)
                        {
                            Line.DetailType = LineDetailTypeEnum.JournalEntryLineDetail;
                            Line.DetailTypeSpecified = true;

                            foreach (var journalentryBased in l.JournalEntryLineDetail)
                            {
                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                                string accountName = string.Empty;
                                string accountNumber = string.Empty;
                                foreach (var acc in journalentryBased.AccountRef)
                                {
                                    accountName = acc.Name;
                                    //accountNumber = acc.AcctNum;
                                }
                                if (accountName != string.Empty && accountName != null && accountName != "")
                                {
                                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                                    JournalEntryLineDetail.AccountRef = new ReferenceType()
                                    {
                                        name = accDetails.name,
                                        Value = accDetails.Value
                                    };
                                }
                               
                               
                                #endregion

                                #region BillableStatus
                                if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    JournalEntryLineDetail.BillableStatusSpecified = true;
                                }
                                else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    JournalEntryLineDetail.BillableStatusSpecified = true;
                                }
                                else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    JournalEntryLineDetail.BillableStatusSpecified = true;
                                }

                                #endregion

                                #region posting type

                                if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Credit.ToString())
                                {
                                    JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Credit;
                                    Line.Amount = Convert.ToDecimal(journalentryBased.Credit);

                                }
                                else if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Debit.ToString())
                                {
                                    JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Debit;
                                    Line.Amount = Convert.ToDecimal(journalentryBased.Debit);

                                }
                                JournalEntryLineDetail.PostingTypeSpecified = true;
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in journalentryBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            //Axis 595
                                            foreach (var TaxDetails in journalentryBased.TxnTaxDetail)
                                            {
                                                string taxapplicableon = TaxDetails.TaxApplicableOn;
                                                if (taxapplicableon != null)
                                                {
                                                    if (taxapplicableon.ToLower() == "sales" || taxapplicableon.ToLower() == "purchase")
                                                    {
                                                        if (TaxDetails.TaxApplicableOn.ToLower() == "sales")
                                                        {

                                                            JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                            JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Sales;
                                                        }
                                                        if (TaxDetails.TaxApplicableOn.ToLower() == "purchase")
                                                        {
                                                            JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                            JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Purchase;
                                                        }
                                                    }
                                                }
                                                //JournalEntryLineDetail.TaxAmount = (Convert.ToDecimal(TaxDetails.TaxAmount));
                                                //JournalEntryLineDetail.TaxAmountSpecified = true;

                                            }


                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                                //AXIS 595
                                                if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Purchase)
                                                {
                                                    foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                                    {
                                                        if (taxRate != null)
                                                        {
                                                            GstRef = taxRate.TaxRateRef;
                                                            GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                                        }
                                                    }
                                                }

                                                if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Sales)
                                                {
                                                    foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                                    {
                                                        if (taxRate != null)
                                                        {
                                                            GstRef = taxRate.TaxRateRef;
                                                            GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                                        }
                                                    }
                                                }
                                                //End AXIS 595

                                                string taxValue = string.Empty;
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                              
                                                foreach (var dTaxRate in dataTaxRate)
                                                {
                                                    taxValue = dTaxRate.RateValue.ToString();
                                                }

                                                if (taxValue != "")
                                                {
                                                    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                                }
                                                else
                                                {
                                                    taxValue = "0";
                                                    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                                }
                                                JournalEntryLineDetail.TaxAmountSpecified = true;

                                            }
                                        }
                                        JournalEntryLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                              // JournalEntryLineDetail.TaxApplicableOnSpecified = true;

                                #region find classRef
                                string className = string.Empty;
                                foreach (var clas in journalentryBased.ClassRef)
                                {
                                    className = clas.Name;
                                }
                                string classNamevalue = string.Empty;
                                if (className != string.Empty)
                                {
                                    var dataclassName = await CommonUtilities.GetClassExistsInOnlineQBO(className.Trim());
                                    foreach (var dclass in dataclassName)
                                    {
                                        classNamevalue = dclass.Id;
                                    }
                                    JournalEntryLineDetail.ClassRef = new ReferenceType()
                                    {
                                        name = className,
                                        Value = classNamevalue
                                    };
                                }

                                #endregion

                                #region find DepartmentRef
                                string DepartmentName = string.Empty;
                                foreach (var dept in journalentryBased.DepartmentRef)
                                {
                                    DepartmentName = dept.Name;
                                }
                                string Departmentvalue = string.Empty;
                                if (DepartmentName != string.Empty)
                                {
                                    var dataDepartmentName = await CommonUtilities.GetDepartmentFullyQualifyNameInOnlineQBO(DepartmentName.Trim());
                                    
                                    foreach (var d in dataDepartmentName)
                                    {
                                        Departmentvalue = d.Id;
                                    }
                                    JournalEntryLineDetail.DepartmentRef = new ReferenceType()
                                    {
                                        name = DepartmentName,
                                        Value = Departmentvalue
                                    };
                                }

                                #endregion

                                #region find EntityRef

                                Intuit.Ipp.Data.EntityTypeRef entityref = new EntityTypeRef();
                                foreach (var ent in journalentryBased.Entity)
                                {
                                    if (ent.Type == EntityTypeEnum.Customer.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Customer;
                                        entityref.TypeSpecified = true;

                                        #region CustomerRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(ent.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                    else if (ent.Type == EntityTypeEnum.Vendor.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Vendor;
                                        entityref.TypeSpecified = true;

                                        #region vendorRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetVendorExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }

                                    else if (ent.Type == EntityTypeEnum.Employee.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Employee;
                                        entityref.TypeSpecified = true;

                                        #region EmployeeRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                }
                                if (entityref.EntityRef != null)
                                {
                                    JournalEntryLineDetail.Entity = entityref;
                                }
                                #endregion

                                Line.AnyIntuitObject = JournalEntryLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedJournalEntry.Line = AddLine(lines_online, Line, linecount);
                    }
                    else if (flag == true)
                    {
                        Line = new Intuit.Ipp.Data.Line();
                        JournalEntryLineDetail = new Intuit.Ipp.Data.JournalEntryLineDetail();
                        linecount++;
                        Line.Description = l.LineDescription;
                        Line.AmountSpecified = true;
                        if (l.JournalEntryLineDetail.Count > 0)
                        {
                            Line.DetailType = LineDetailTypeEnum.JournalEntryLineDetail;
                            Line.DetailTypeSpecified = true;
                            #region line data
                            foreach (var journalentryBased in l.JournalEntryLineDetail)
                            {

                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                                string accountName = string.Empty;
                                string accountNumber = string.Empty;
                                foreach (var acc in journalentryBased.AccountRef)
                                {
                                    accountName = acc.Name;
                                    //accountNumber = acc.AcctNum;
                                }
                                if (accountName != string.Empty && accountName != null && accountName != "")
                                {
                                    ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountName);
                                    JournalEntryLineDetail.AccountRef = new ReferenceType()
                                    {
                                        name = accDetails.name,
                                        Value = accDetails.Value
                                    };
                                }
                               

                                #endregion

                                #region BillableStatus
                                if (journalentryBased.BillableStatus != null)
                                {
                                    if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        JournalEntryLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        JournalEntryLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (journalentryBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        JournalEntryLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        JournalEntryLineDetail.BillableStatusSpecified = true;
                                    }
                                }

                                #endregion

                                #region posting type
                                if (journalentryBased.PostingType != null)
                                {
                                    if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Credit.ToString())
                                    {
                                        JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Credit;
                                        Line.Amount = Convert.ToDecimal(journalentryBased.Credit);
                                    }
                                    else if (journalentryBased.PostingType == Intuit.Ipp.Data.PostingTypeEnum.Debit.ToString())
                                    {
                                        JournalEntryLineDetail.PostingType = Intuit.Ipp.Data.PostingTypeEnum.Debit;
                                        Line.Amount = Convert.ToDecimal(journalentryBased.Debit);
                                    }
                                    JournalEntryLineDetail.PostingTypeSpecified = true;
                                }
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in journalentryBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            //Axis 595
                                            foreach (var TaxDetails in journalentryBased.TxnTaxDetail)
                                            {
                                                string taxapplicableon = TaxDetails.TaxApplicableOn;
                                                if (taxapplicableon != null)
                                                {
                                                    if (taxapplicableon.ToLower() == "sales" || taxapplicableon.ToLower() == "purchase")
                                                    {
                                                        if (TaxDetails.TaxApplicableOn.ToLower() == "sales")
                                                        {

                                                            JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                            JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Sales;
                                                        }
                                                        if (TaxDetails.TaxApplicableOn.ToLower() == "purchase")
                                                        {
                                                            JournalEntryLineDetail.TaxApplicableOnSpecified = true;
                                                            JournalEntryLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Purchase;
                                                        }
                                                    }
                                                }
                                                //JournalEntryLineDetail.TaxAmount = (Convert.ToDecimal(TaxDetails.TaxAmount));
                                                //JournalEntryLineDetail.TaxAmountSpecified = true;

                                            }
                                            //Axis 595 END
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;
                                                    }
                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;

                                                //AXIS 595
                                                if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Purchase)
                                                {
                                                    foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                                    {
                                                        if (taxRate != null)
                                                        {
                                                            GstRef = taxRate.TaxRateRef;
                                                            GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                                        }
                                                    }
                                                }

                                                if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Sales)
                                                {
                                                    foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                                    {
                                                        if (taxRate != null)
                                                        {
                                                            GstRef = taxRate.TaxRateRef;
                                                            GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                                        }
                                                    }
                                                }
                                                //End AXIS 595

                                                string taxValue = string.Empty;
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                                foreach (var dTaxRate in dataTaxRate)
                                                {
                                                    taxValue = dTaxRate.RateValue.ToString();
                                                }
                                                if (taxValue != "")
                                                {
                                                    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                                }
                                                else
                                                {
                                                    taxValue = "0";
                                                    JournalEntryLineDetail.TaxAmount = (Line.Amount * Convert.ToInt32(taxValue)) / 100;
                                                }
                                                JournalEntryLineDetail.TaxAmountSpecified = true;
                                            }
                                        }

                                        JournalEntryLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion


                                // JournalEntryLineDetail.TaxApplicableOnSpecified = true;

                                #region find classRef
                                string className = string.Empty;
                                foreach (var clas in journalentryBased.ClassRef)
                                {
                                    className = clas.Name;
                                }
                                string classNamevalue = string.Empty;
                                if (className != string.Empty)
                                {
                                    var dataclassName = await CommonUtilities.GetClassExistsInOnlineQBO(className.Trim());
                                    foreach (var dclass in dataclassName)
                                    {
                                        classNamevalue = dclass.Id;
                                    }
                                    JournalEntryLineDetail.ClassRef = new ReferenceType()
                                    {
                                        name = className,
                                        Value = classNamevalue
                                    };
                                }

                                #endregion

                                #region find DepartmentRef
                                string DepartmentName = string.Empty;
                                foreach (var dept in journalentryBased.DepartmentRef)
                                {
                                    DepartmentName = dept.Name;
                                }
                                string Departmentvalue = string.Empty;
                                if (DepartmentName != string.Empty)
                                {
                                    var dataDepartmentName = await CommonUtilities.GetDepartmentFullyQualifyNameInOnlineQBO(DepartmentName.Trim());
                                    
                                    foreach (var d in dataDepartmentName)
                                    {
                                        Departmentvalue = d.Id;
                                    }
                                    JournalEntryLineDetail.DepartmentRef = new ReferenceType()
                                    {
                                        name = DepartmentName,
                                        Value = Departmentvalue
                                    };
                                }

                                #endregion

                                #region find EntityRef

                                Intuit.Ipp.Data.EntityTypeRef entityref = new EntityTypeRef();
                                foreach (var ent in journalentryBased.Entity)
                                {
                                    if (ent.Type == EntityTypeEnum.Customer.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Customer;
                                        entityref.TypeSpecified = true;

                                        #region CustomerRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                    else if (ent.Type == EntityTypeEnum.Vendor.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Vendor;
                                        entityref.TypeSpecified = true;

                                        #region vendorRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetVendorExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }

                                    else if (ent.Type == EntityTypeEnum.Employee.ToString())
                                    {
                                        entityref.Type = EntityTypeEnum.Employee;
                                        entityref.TypeSpecified = true;

                                        #region EmployeeRef

                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(ent.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        entityref.EntityRef = new ReferenceType()
                                        {
                                            name = ent.Name,
                                            Value = id
                                        };
                                        #endregion
                                    }
                                }
                                if (entityref.EntityRef != null)
                                {
                                    JournalEntryLineDetail.Entity = entityref;
                                }
                                #endregion

                                Line.AnyIntuitObject = JournalEntryLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                                addedJournalEntry.Line = AddLine(lines_online, Line, linecount);

                            }
                            #endregion
                        }

                    }

                }

                #endregion

                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)
                    #region TxnTaxDetail

                    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                   
                    for (int i = 0; i < array_count; i++)
                    {
                        string taxCode = string.Empty;
                        taxCode = Array_taxcodeName[i].ToString();
                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                        foreach (var tax in dataTaxcode)
                        {
                            //AXIS 595
                            if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Purchase)
                            {
                                foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                {
                                    if (taxRate != null)
                                    {
                                        GstRef = taxRate.TaxRateRef;
                                        GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                    }
                                }
                            }

                            if (JournalEntryLineDetail.TaxApplicableOn == TaxApplicableOnEnum.Sales)
                            {
                                foreach (var taxRate in tax.SalesTaxRateList.TaxRateDetail)
                                {
                                    if (taxRate != null)
                                    {
                                        GstRef = taxRate.TaxRateRef;
                                        GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                    }
                                }
                            }
                            //End AXIS 595
                            //if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                            //{
                            //    foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                            //    {
                            //        GstRef = taxRate.TaxRateRef;
                            //        GSTtaxRateRefName = taxRate.TaxRateRef.name;
                            //    }
                            //}
                        }
                        taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                        taxLine.DetailTypeSpecified = true;

                        taxLineDetail.PercentBased = true;
                        taxLineDetail.PercentBasedSpecified = true;

                        taxLineDetail.TaxRateRef = GstRef;

                        string taxValue = string.Empty;
                        decimal s = 0;
                        var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                        foreach (var dTaxRate in dataTaxRate)
                        {
                            taxValue = dTaxRate.RateValue.ToString();
                        }
                        if (taxValue != "")
                        {
                            s = Convert.ToDecimal(taxValue);
                        }
                        else
                        {
                            taxValue = "0";
                            s = Convert.ToDecimal(taxValue);
                        }
                        taxLineDetail.TaxPercent = s;
                        //taxLineDetail.TaxPercent = decimal.Round(taxVal, 2);
                        decimal taxVal = (s) / 100;

                        taxLineDetail.TaxPercentSpecified = true;

                       // taxLine.Amount = decimal.Round(Array_taxAmount[i] * taxLineDetail.TaxPercent, 2);
                        taxLine.Amount = decimal.Round(Array_taxAmount[i] * taxVal, 2);
                        //Bug;;595
                        taxLine.Amount = -taxLine.Amount;
                        taxLine.AmountSpecified = true;

                        taxLineDetail.NetAmountTaxable = Array_taxAmount[i];
                        //Bug;;595
                        taxLineDetail.NetAmountTaxable = -taxLineDetail.NetAmountTaxable;
                        taxLineDetail.NetAmountTaxableSpecified = true;

                        taxLine.AnyIntuitObject = taxLineDetail;
                        linecount1++;
                        Array.Resize(ref lines_online1, linecount1);
                        lines_online1[linecount1 - 1] = taxLine;
                        txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);

                        taxLine = new Intuit.Ipp.Data.Line();
                        taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                        if (txnTaxDetail != null)
                        {
                            addedJournalEntry.TxnTaxDetail = txnTaxDetail;
                        }
                    }

                    #endregion
                }
                addedJournalEntry.TotalAmtSpecified = false;
                addedJournalEntry.HomeTotalAmtSpecified = false;
                addedJournalEntry.AdjustmentSpecified = false;

                //Axis 701
                if (journalentry.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in journalentry.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    addedJournalEntry.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (journalentry.ExchangeRate != null)
                {
                    addedJournalEntry.ExchangeRate = Convert.ToDecimal(journalentry.ExchangeRate);
                    addedJournalEntry.ExchangeRateSpecified = true;
                }
                //Axis 701 ends
                //586
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in journalentry.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                   
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }

                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedJournalEntry.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                journalentryAdded = new Intuit.Ipp.Data.JournalEntry();
                journalentryAdded = dataService.Update<Intuit.Ipp.Data.JournalEntry>(addedJournalEntry);

                #endregion
            }

            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (journalentryAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = journalentryAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = journalentryAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion


        }

        #endregion

        //internal bool UpdateQBOJournalEntry(JournalEntryclass entry)
        //{
        //    throw new NotImplementedException();
        //}
    }
    /// <summary>
    /// checking doc number is present or not in quickbook online for Journal.
    /// </summary>
    public class OnlineJournalEntryQBCollection : Collection<JournalEntryclass>
    {
        public JournalEntryclass FindJournalQBEntry(string docNumber)
        {
            foreach (JournalEntryclass item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }
}


