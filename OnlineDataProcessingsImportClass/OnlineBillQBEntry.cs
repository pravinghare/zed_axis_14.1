﻿
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("Bill", Namespace = "", IsNullable = false)]

    public class Bill
    {

        #region member
        private string m_DocNumber;
        private string m_GlobalTaxCal;
        private string m_PrivateNote;
        private string m_TrackingNum;

        private string m_TxnDate;
        private string m_DueDate;

//594
        private bool m_isAppend;



        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.VendorRef> m_VendorRef = new Collection<OnlineEntities.VendorRef>();
        private Collection<OnlineEntities.AccountBasedExpenseLineDetail> m_AccountBased = new Collection<OnlineEntities.AccountBasedExpenseLineDetail>();

        private Collection<OnlineEntities.APAccountRef> m_APAccountRef = new Collection<OnlineEntities.APAccountRef>();
        private Collection<OnlineEntities.SalesTermRef> m_SalesTermRef = new Collection<OnlineEntities.SalesTermRef>();

        private string m_Balance;

        //Axis 11.2 bug no.337    
        private string m_ExchangeRate;
        private string m_TransactionLocationType;

        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();

        //End Axis 11.2 bug no.337

        #endregion

        #region Constructor
        public Bill()
        {
        }
        #endregion

        #region properties


        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }

        public string GlobalTaxCal
        {
            get { return m_GlobalTaxCal; }
            set { m_GlobalTaxCal = value; }
        }
        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }
        //Axis 11.2 bug no.337
        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }
        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }
        //End Axis 11.2 bug no.337
        public string TrackingNum
        {
            get { return m_TrackingNum; }
            set { m_TrackingNum = value; }
        }


        public string Balance
        {
            get { return m_Balance; }
            set { m_Balance = value; }
        }


        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        public Collection<OnlineEntities.VendorRef> VendorRef
        {
            get { return m_VendorRef; }
            set { m_VendorRef = value; }
        }

        public Collection<OnlineEntities.AccountBasedExpenseLineDetail> AccountBased
        {
            get { return m_AccountBased; }
            set { m_AccountBased = value; }
        }

        public Collection<OnlineEntities.APAccountRef> APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }

        public Collection<OnlineEntities.SalesTermRef> SalesTermRef
        {
            get { return m_SalesTermRef; }
            set { m_SalesTermRef = value; }
        }
		//594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region  Public Methods
        // static method for resize array for Line tag  or taxline tag.
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
       

        ///create new transaction record .
        public async System.Threading.Tasks.Task<bool> CreateQBOBill(OnlineDataProcessingsImportClass.Bill coll)
        {
            decimal totalTax = 0;
            //List<string> TaxRateRefGN = new List<string>();
            List<string> TotalTaxGN = new List<string>();
            List<string> TaxRateRefGN = new List<string>();
            int linecount = 1;
            int linecount1 = 0;
            string storeTaxCodeRef = string.Empty;
            DataService dataService = null;
            Intuit.Ipp.Data.Bill BillAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.Bill addedbill = new Intuit.Ipp.Data.Bill();
            Bill bill = new Bill();
            bill = coll; // assign collection of data entity to bill object
            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];//array contains bunch of Line Tag
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            #region tax member
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            int array_count = 0;
            bool taxFlag = false;
            #endregion
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            

            try
            {
                #region Add Bill
                #region TxnDate,privatenote
                //594
                if (bill.isAppend == true)
                {
                    addedbill.sparse = true;
                }
                if (bill.TxnDate != null)
                {
                    try
                    {
                        addedbill.TxnDate = Convert.ToDateTime(bill.TxnDate);
                        addedbill.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (bill.PrivateNote != null)
                {
                    addedbill.PrivateNote = bill.PrivateNote;
                }
                if (bill.DocNumber != null)
                {
                    addedbill.DocNumber = bill.DocNumber;
                }
                if (bill.DepartmentRef != null)
                {
                    foreach (var DepartmentRef in bill.DepartmentRef)
                    {
                        string Vendorvalue = string.Empty;
                       
                        if (DepartmentRef.Name != null)
                        {
                            var dataVendor = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentRef.Name.Trim());
                          
                            foreach (var vendor in dataVendor)
                            {
                                Vendorvalue = vendor.Id;
                            }
                            addedbill.DepartmentRef = new ReferenceType()
                            {
                                name = DepartmentRef.Name,
                                Value = Vendorvalue
                            };
                        }
                    }
                }

                if (bill.CurrencyRef != null)
                {
                    foreach (var CurrencyRef in bill.CurrencyRef)
                    {
                        QueryService<Intuit.Ipp.Data.Currency> customerQueryService = new QueryService<Intuit.Ipp.Data.Currency>(serviceContext);

                        string Vendorvalue = string.Empty;
                        ReadOnlyCollection<Intuit.Ipp.Data.Currency> dataVendor;
                        if (CurrencyRef.Name != null)
                        {
                            //if (CurrencyRef.Name.Contains("'"))
                            //{
                            //    CurrencyRef.Name = CurrencyRef.Name.Replace("'", "\\'");
                            //}
                            //dataVendor = new ReadOnlyCollection<Intuit.Ipp.Data.Currency>(customerQueryService.ExecuteIdsQuery("Select * From Currency where Name ='" + CurrencyRef.Name + "'"));
                            //foreach (var vendor in dataVendor)
                            //{
                            //    Vendorvalue = vendor.Id;
                            //}
                            addedbill.CurrencyRef = new ReferenceType()
                            {
                               // name = CurrencyRef.Name,
                               // Value = Vendorvalue
                                 Value = CurrencyRef.Name
                            };
                        }
                    }
                }
                #endregion              
                if (bill.ExchangeRate != null)
                {
                    addedbill.ExchangeRate = Convert.ToDecimal(bill.ExchangeRate);
                    addedbill.ExchangeRateSpecified = true;
                }

                // Axis 742
                if (bill.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(bill.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedbill.TransactionLocationType = value;
                    }
                    else
                    {
                        addedbill.TransactionLocationType = bill.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (bill.GlobalTaxCal != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedbill.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedbill.GlobalTaxCalculationSpecified = true;
                        }
                        else if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedbill.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedbill.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedbill.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedbill.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                //Axis 719
                if (bill.APAccountRef != null)
                {
                    foreach (var APAccountRef in bill.APAccountRef)
                    {
                        string accountvalue = string.Empty;
                        if (APAccountRef.Name != null)
                        {
                            if (APAccountRef.Name.Contains("'"))
                            {
                                APAccountRef.Name = APAccountRef.Name.Replace("'", "\\'");
                            }
                            var dataAccount = await CommonUtilities.GetAccountDetailsExistsInOnlineQBO(APAccountRef.Name.Trim());
                           
                            foreach (var accounts in dataAccount)
                            {
                                accountvalue = accounts.Id;
                            }

                            addedbill.APAccountRef = new ReferenceType()
                            {
                                name = APAccountRef.Name,
                                Value = accountvalue
                            };
                        }
                    }
                }
                //Axis 719 ends

                #endregion              

                //Axis 719

                //Axis 719 ends

                //bug 469 axis 12.0

                Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                Line = new Intuit.Ipp.Data.Line();
                bool flag = false;     

                string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
                ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists

                foreach (var l in bill.Line)
                {
                    if (flag == false)
                    {
                       #region 
                        flag = true;

                        Line.Description = l.LineDescription;

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var accountBased in l.AccountBasedExpenseLineDetail)
                            {
                                if (accountBased.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                              
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue!=string.Empty)
                                                {
                                                    //Line.Amount =Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    //Line.Amount =Math.Round( Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)),2);

                                                    Line.Amount = (Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(accountBased.LineAmount) - Line.Amount);
                                                    TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(accountBased.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    Line.Amount = Math.Round(Line.Amount, 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                    Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                               // Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                            Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (accountBased != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                Line.Description = accountBased.Descreption;

                                #region find customer
                               
                                foreach (var cust in accountBased.customerRef)
                                {
                                    string customervalue = string.Empty;
                                   
                                    if (cust.Name != null)
                                    {
                                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(cust.Name.Trim());
                                       
                                        foreach (var customer1 in dataCustomer)
                                        {
                                            customervalue = customer1.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = cust.Name,
                                            Value = customervalue
                                        };
                                    }
                                }
                                #endregion
                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);

                                foreach (var acc in accountBased.AccountRef)
                                {
                                    if (acc.Name != null && acc.Name != string.Empty && acc.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(acc.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }

                                #endregion

                                #region find ClassRef
                                foreach (var acc in accountBased.ClassRef)
                                {
                                    string accountvalue = string.Empty;
                                
                                    if (acc.Name != null)
                                    {
                                        var dataAccount = await CommonUtilities.GetClassExistsInOnlineQBO(acc.Name.Trim());
                                       
                                        foreach (var dAccount in dataAccount)
                                        {
                                            accountvalue = dAccount.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = acc.Name,
                                            Value = accountvalue
                                        };
                                    }
                                }

                                #endregion
                                #region BillableStatus
                                if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                #region TaxCodeRef
                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(accountBased.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(accountBased.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(accountBased.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        
                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {

                            foreach (var s in l.ItemBasedExpenseLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                Line.Description = s.Descreption;

                                if (s.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                        Line.Amount = Convert.ToDecimal(s.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                           
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = (Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(s.LineAmount) - Line.Amount);
                                                    TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(s.LineAmount) - Line.Amount).ToString());
                                                    //TaxRateRefGN.Add(taxRateRefName.Trim(), (Convert.ToDecimal(s.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    TaxRateRefGN.Add(ratevalue);
                                                    Line.Amount = Math.Round(Line.Amount, 2);

                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                    Line.Amount =Math.Round( Convert.ToDecimal(s.LineAmount),2);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                Line.Amount = Convert.ToDecimal(s.LineAmount);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                            Line.Amount = Convert.ToDecimal(s.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }

                                #region ItemRef
                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;

                                    if (CommonUtilities.GetInstance().SKULookup == false||CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }
                                        
                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name=item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }
                                #endregion
                                #region ClassRef
                                foreach (var i in s.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region CustomerRef
                                foreach (var i in s.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion                              
                                #region Qty
                                if (s.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(s.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {                                        
                                            string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {

                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(s.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(s.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(s.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        
                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;  
                            }
                        }
                        addedbill.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                       #region
                        Line = new Intuit.Ipp.Data.Line();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        linecount++;

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var accountBased in l.AccountBasedExpenseLineDetail)
                            {
                                if (accountBased.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                      
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                              
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = (Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(accountBased.LineAmount) - Line.Amount);
                                                    TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(accountBased.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    TaxRateRefGN.Add(ratevalue);

                                                    Line.Amount = Math.Round(Line.Amount, 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(accountBased.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(accountBased.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                            Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (accountBased != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                Line.Description = accountBased.Descreption;
                                #region find customer
                                
                                foreach (var cust in accountBased.customerRef)
                                {
                                    string customervalue = string.Empty;
                                    if (cust.Name != null)
                                    {
                                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(cust.Name.Trim());
                                        foreach (var customer1 in dataCustomer)
                                        {
                                            customervalue = customer1.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = cust.Name,
                                            Value = customervalue
                                        };
                                    }
                                }
                                #endregion
                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);


                                foreach (var acc in accountBased.AccountRef)
                                {
                                    string accountvalue = string.Empty;
                                    ReadOnlyCollection<Intuit.Ipp.Data.Account> dataAccount;
                                    if (acc.Name != null && acc.Name != string.Empty && acc.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(acc.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                  
                                }

                                #endregion
                                #region find ClassRef
                               
                                foreach (var acc in accountBased.ClassRef)
                                {
                                    string accountvalue = string.Empty;
                                    if (acc.Name != null)
                                    {
                                        var dataAccount = await CommonUtilities.GetClassExistsInOnlineQBO(acc.Name.Trim());
                                        foreach (var dAccount in dataAccount)
                                        {
                                            accountvalue = dAccount.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = acc.Name,
                                            Value = accountvalue
                                        };
                                    }
                                }
                                #endregion
                                #region BillableStatus
                                if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {

                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(accountBased.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(accountBased.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(accountBased.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        
                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {

                            foreach (var s in l.ItemBasedExpenseLineDetail)
                            {
                                Line.Description = s.Descreption;
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (s.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                        Line.Amount = Convert.ToDecimal(s.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                       
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = (Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(s.LineAmount) - Line.Amount);
                                                     TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(s.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    TaxRateRefGN.Add(ratevalue);
                                                    Line.Amount = Math.Round(Line.Amount, 2, MidpointRounding.AwayFromZero);

                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    //Line.Amount =Math.Round( Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)),2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(s.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                               // Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(s.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                            Line.Amount = Convert.ToDecimal(s.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                #region ItemRef
                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }
                                }
                                #endregion

                                #region ClassRef
                                foreach (var i in s.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region CustomerRef
                                foreach (var i in s.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion                               
                                #region Qty
                                if (s.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(s.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }
                                #endregion
                                #region TaxCodeRef
                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {

                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {

                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region BillableStatus
                                if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedbill.Line = AddLine(lines_online, Line, linecount);

                       #endregion
                    }

                }

                #region Bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedbill.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && bill.GlobalTaxCal != null)
                        {
                            #region TxnTaxDetail

                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {
                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                              
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());

                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                   
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());

                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedbill.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //BUG ;; 586
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in bill.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
            
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedbill.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                #endregion
                #endregion

                #region VendorRef, bal,due date
                foreach (var VendorRef in bill.VendorRef)
                {
                    string vendorvalue = string.Empty;
                    if (VendorRef.Name != null)
                    {
                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(VendorRef.Name.Trim());
                        foreach (var vendors in dataVendor)
                        {
                            vendorvalue = vendors.Id;
                        }
                        addedbill.VendorRef = new ReferenceType()
                        {
                            name = VendorRef.Name,
                            Value = vendorvalue
                        };
                    }
                }
                //bug 504
                if (bill.DueDate != null)
                {
                    try
                    {
                        addedbill.DueDate = Convert.ToDateTime(bill.DueDate);
                        addedbill.DueDateSpecified = true;
                    }
                    catch { }
                }
                if (bill.Balance != null)
                {
                    addedbill.Balance = Convert.ToDecimal(bill.Balance);
                }


                #endregion
                BillAdded = new Intuit.Ipp.Data.Bill();
                CommonUtilities.GetInstance().newSKUItem = false;

                #region Issue 514 try
              //  Intuit.Ipp.Data.Line[] Taxline = null;
              //  Intuit.Ipp.Data.Line Taxline1 = null;
              //  Intuit.Ipp.Data.TaxLineDetail TaxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();

              //  Intuit.Ipp.Data.TxnTaxDetail TxnTaxdetail = new Intuit.Ipp.Data.TxnTaxDetail();
              //  Dictionary<string,decimal> TaxAmountGN = new  Dictionary<string,decimal>();
              //  Dictionary<string, decimal> TaxNetAmountGN = new Dictionary<string, decimal>();
              //   Dictionary<string, string> TaxCoderef = new Dictionary<string, string>();

              //  KeyValuePair<string, string> keyval =new KeyValuePair<string,string>();

              //  string TaxRateName = string.Empty;
              //  decimal TaxAmount = 0;
              //  decimal NetAmount = 0;
              //  decimal TotalTax = 0;
              // string result=string.Empty;

              //  for (int temp = 0; temp < TaxRateRefGN.Count; temp++)
              //  {
              //      TaxRateName= TaxRateRefGN[temp];
              //      TaxAmount=Convert.ToDecimal(TaxRateRefGN[temp+1]);
              //      NetAmount = Convert.ToDecimal(TaxRateRefGN[temp + 2]);

              //      try
              //      {
              //          TaxCoderef.Add(TaxRateRefGN[temp], TaxRateRefGN[temp + 3]);
              //      }
              //      catch { }
              //      TaxRateRefGN.RemoveAt(temp);
              //      TaxRateRefGN.RemoveAt(temp);
              //      TaxRateRefGN.RemoveAt(temp);
              //      TaxRateRefGN.RemoveAt(temp);

              //     if(TaxAmountGN.ContainsKey(TaxRateName))
              //     {
              //        TaxAmountGN[TaxRateName]=TaxAmountGN[TaxRateName]+TaxAmount;
              //     }
              //     else
              //     {
              //        TaxAmountGN.Add(TaxRateName,TaxAmount);
              //     }

              //     if (TaxNetAmountGN.ContainsKey(TaxRateName))
              //     {
              //         TaxNetAmountGN[TaxRateName] = TaxNetAmountGN[TaxRateName] + NetAmount;
              //     }
              //     else
              //     {
              //         TaxNetAmountGN.Add(TaxRateName, NetAmount);
              //     }
              //      temp=-1;
              //  }

              //  //foreach (KeyValuePair<string, string> item in TaxRateRefGN)
              //  //{
              //  //     Console.WriteLine("Key: {0}, Value: {1}", item.Key, item.Value);

              //  //     if (TaxCalGN == null)
              //  //    {
              //  //        TaxCalGN.Add(item.Key,item.Value);
              //  //    }
              //  //    if(TaxCalGN.TryGetValue(item.Key, out result))
              //  //    {
              //  //        Console.WriteLine(result);
              //  //    }
              //  //    else
              //  //    {
              //  //         Console.WriteLine("Could not find the specified key.");
              //  //    }
              //  //}

              //  //for (int temp = 0; temp < TaxRateRefGN.Count; temp++)
              //  //{
              //  //    key = TaxRateRefGN.Keys
              //  //    value= TaxRateRefGN.GetValues(temp).ToString();
              //  //    string[] values = TaxRateRefGN.GetValues("partiban");
              //  //    foreach (string keya in values)
              //  //    {
                          
              //  //    }
              //  //    if (TaxCalGN == null)
              //  //    {
              //  //        TaxCalGN.Add(key,value);
              //  //    }
              //  //    else if(TaxCalGN.AllKeys.c(temp))
                   

              //  //}
              //  foreach (var str in TaxAmountGN)
              //  {
              //      try
              //      {
              //          TotalTax = TotalTax + Convert.ToDecimal(TaxAmountGN[str.Key]);
              //      }
              //      catch { }
              //  }
              //  Taxline = new Intuit.Ipp.Data.Line[TaxCoderef.Count];
              //  TxnTaxdetail.TaxLine = new Intuit.Ipp.Data.Line[TaxCoderef.Count];
              //  int Count = 0;
              // foreach(var str in TaxCoderef)
              // {
              //     try
              //     {
              //         Taxline1 = new Intuit.Ipp.Data.Line();
              //         Taxline1.Amount = TaxAmountGN[str.Key];
              //         Taxline1.AmountSpecified = true;
              //         Taxline1.DetailType = LineDetailTypeEnum.TaxLineDetail;
                       
              //        // Taxline[Count].Amount = TaxAmountGN[str.Key];
              //       // Taxline[Count].AmountSpecified = true;
              //        // Taxline[Count].DetailType = LineDetailTypeEnum.TaxLineDetail;
              //         TaxLineDetail.TaxRateRef = new ReferenceType()
              //         {
              //             Value = str.Value,
              //             name = str.Key
              //         };
                      
              //         TaxLineDetail.NetAmountTaxable = TaxNetAmountGN[str.Key];
              //         //Taxline[Count].AnyIntuitObject = TaxLineDetail;
              //         Taxline1.AnyIntuitObject = TaxLineDetail;
              //         Taxline[Count] = Taxline1;
              //         TxnTaxdetail.TaxLine[Count] = Taxline1;
                        
              //         Count++;
              //     }
              //     catch(Exception ex)
              //     { MessageBox.Show(ex.ToString()); }
              // }
              
              // TxnTaxdetail.TotalTax = totalTax;
              // TxnTaxdetail.TotalTaxSpecified = true;
              // //Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
              // //Array.Resize(ref lines_online, linecount);
              // //lines_online[linecount - 1] = Line;
              // addedbill.Line = AddLine1(addedbill.Line, TxnTaxdetail, linecount++);
              ////addedbill.Line[addedbill.Line.Length+1].line = TxnTaxdetail;

              // //for (int temp = 0; temp < TaxNetAmountGN.Count; temp++)
              // //{
              // //     //TxnTaxdetail.DefaultTaxCodeRef = ItemBasedExpenseLineDetail.TaxCodeRef;
              // //     TxnTaxdetail.TotalTax = 100;
              // //     TxnTaxdetail.TotalTaxSpecified = true;
              // //     foreach (var str in TaxAmountGN)
              // //     {
              // //         Taxline[temp].Amount = str.Value;
              // //         Taxline[temp].AmountSpecified = true;
              // //         Taxline[temp].DetailType = LineDetailTypeEnum.TaxLineDetail;
              // //         TaxLineDetail.TaxRateRef = new ReferenceType()
              // //         {
              // //             Value = TaxCoderef.Keys.ElementAt
              // //             name = ""
              // //         };
              // //         TaxLineDetail.NetAmountTaxable = TaxNetAmountGN[TaxNetAmountGN]
              // //         Taxline[temp].AnyIntuitObject = TaxLineDetail;
              // //     }
              // //     foreach (var str in TaxCoderef)
              // //     {

              // //     }
                    
              // //     TxnTaxdetail.TaxLine = Taxline;

              // //     //Taxline.
              // //     //TxnTaxDetail.TaxLine=
              // //     for (int i = 0; i < TaxAmountGN.Count;i++)
                        
                  
              // //     TaxLineDetail.TaxPercent = 20;
              // //     TaxLineDetail.TaxPercentSpecified = true;
                    
                    
              // // }
               
             


              //  //TaxLineDetail.TaxLineDetailEx  = new IntuitAnyType(){}

                #endregion

                BillAdded = dataService.Add<Intuit.Ipp.Data.Bill>(addedbill); //dataService.Add method adding a new entity of bill type.
                
                
               

            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (BillAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = BillAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = BillAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

        ///update existing entity by the transaction Id.
        public async System.Threading.Tasks.Task<bool> UpdateQBOBill(OnlineDataProcessingsImportClass.Bill coll, string billid, string syncToken, Intuit.Ipp.Data.Bill PreviousData=null)
        {
            decimal totalTax = 0;
            //List<string> TaxRateRefGN = new List<string>();
            List<string> TotalTaxGN = new List<string>();
            List<string> TaxRateRefGN = new List<string>();
            int linecount = 1;
            int linecount1 = 0;
          
            string storeTaxCodeRef = string.Empty;

            DataService dataService = null;
            Intuit.Ipp.Data.Bill BillAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.Bill addedbill = new Intuit.Ipp.Data.Bill();
            Bill bill = new Bill();
            bill = coll;

            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();

            #region tax member
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            
            int array_count = 0;
            bool taxFlag = false;
            #endregion
            
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            bool flag = false;
            try
            {
                addedbill.Id = billid;
                addedbill.SyncToken = syncToken;

                #region Add Bill

                //594
                if (bill.isAppend == true)
                {
                   
			         //594 PreviousData is used to maintain previous records and append new data if changes are applied ,Lines will be appended to next line directly
                    addedbill = PreviousData;
                    addedbill.sparse = true;
                    addedbill.sparseSpecified = true;
                    if (addedbill.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedbill.Line.Length;
                        lines_online = addedbill.Line;
                        Array.Resize(ref lines_online, linecount);
                    }

                }

                #region TxnDate,privatenote
                if (bill.TxnDate != null)
                {
                    try
                    {
                        addedbill.TxnDate = Convert.ToDateTime(bill.TxnDate);
                        addedbill.TxnDateSpecified = true;
                    }
                    catch { }
                }
                if (bill.PrivateNote != null)
                {
                    addedbill.PrivateNote = bill.PrivateNote;
                }
                if (bill.DocNumber != null)
                {
                    addedbill.DocNumber = bill.DocNumber;
                }
                if (bill.DepartmentRef != null)
                {
                    foreach (var DepartmentRef in bill.DepartmentRef)
                    {
                        string Vendorvalue = string.Empty;
                        if (DepartmentRef.Name != null)
                        {
                            var dataVendor = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentRef.Name.Trim());
                            foreach (var vendor in dataVendor)
                            {
                                Vendorvalue = vendor.Id;
                            }
                            addedbill.DepartmentRef = new ReferenceType()
                            {
                                name = DepartmentRef.Name,
                                Value = Vendorvalue
                            };
                        }
                    }
                }

                if (bill.CurrencyRef != null)
                {
                    foreach (var CurrencyRef in bill.CurrencyRef)
                    {
                        //QueryService<Intuit.Ipp.Data.Currency> customerQueryService = new QueryService<Intuit.Ipp.Data.Currency>(serviceContext);

                        //string Vendorvalue = string.Empty;
                        //ReadOnlyCollection<Intuit.Ipp.Data.Currency> dataVendor;
                        if (CurrencyRef.Name != null)
                        {
                            //if (CurrencyRef.Name.Contains("'"))
                            //{
                            //    CurrencyRef.Name = CurrencyRef.Name.Replace("'", "\\'");
                            //}
                            //dataVendor = new ReadOnlyCollection<Intuit.Ipp.Data.Currency>(customerQueryService.ExecuteIdsQuery("Select * From Currency where Name ='" + CurrencyRef.Name + "'"));
                            //foreach (var vendor in dataVendor)
                            //{
                            //    Vendorvalue = vendor.Id;
                            //}
                            addedbill.CurrencyRef = new ReferenceType()
                            {
                                Value = CurrencyRef.Name
                                // name = CurrencyRef.Name,
                                // Value = Vendorvalue
                            };
                        }
                    }
                }
                #endregion
                if (bill.ExchangeRate != null)
                {
                    addedbill.ExchangeRateSpecified = true;
                    addedbill.ExchangeRate = Convert.ToDecimal(bill.ExchangeRate);
                }

                // Axis 742
                if (bill.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(bill.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedbill.TransactionLocationType = value;
                    }
                    else
                    {
                        addedbill.TransactionLocationType = bill.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (bill.GlobalTaxCal != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedbill.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedbill.GlobalTaxCalculationSpecified = true;
                        }
                        else if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedbill.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedbill.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedbill.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedbill.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                //Axis 719
                if (bill.APAccountRef != null)
                {
                    foreach (var APAccountRef in bill.APAccountRef)
                    {
                        string accountvalue = string.Empty;
                        if (APAccountRef.Name != null)
                        {
                            var dataAccount = await CommonUtilities.GetAccountDetailsExistsInOnlineQBO(APAccountRef.Name);
                            foreach (var accounts in dataAccount)
                            {
                                accountvalue = accounts.Id;
                            }

                            addedbill.APAccountRef = new ReferenceType()
                            {
                                name = APAccountRef.Name,
                                Value = accountvalue
                            };
                        }
                    }
                }
                //Axis 719 ends

                #endregion

                Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                Line = new Intuit.Ipp.Data.Line();
                

                string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST 

                ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists

                foreach (var l in bill.Line)
                {
                    if (flag == false)
                    {
                        #region
                        flag = true;

                        Line.Description = l.LineDescription;

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var accountBased in l.AccountBasedExpenseLineDetail)
                            {
                                if (accountBased.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());

                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount =Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    //Line.Amount =Math.Round( Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)),2);

                                                    Line.Amount = (Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(accountBased.LineAmount) - Line.Amount);
                                                    TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(accountBased.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    Line.Amount = Math.Round(Line.Amount, 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                    Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                            Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (accountBased != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                Line.Description = accountBased.Descreption;

                                #region find customer
                               
                                foreach (var cust in accountBased.customerRef)
                                {
                                    string customervalue = string.Empty;
                                    if (cust.Name != null)
                                    {
                                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(cust.Name.Trim());
                                       
                                        foreach (var customer1 in dataCustomer)
                                        {
                                            customervalue = customer1.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = cust.Name,
                                            Value = customervalue
                                        };
                                    }
                                }
                                #endregion
                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);


                                foreach (var acc in accountBased.AccountRef)
                                {
                                    if (acc.Name != null && acc.Name != string.Empty && acc.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(acc.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                    
                                }

                                #endregion
                                #region find ClassRef
                               
                                foreach (var acc in accountBased.ClassRef)
                                {
                                    string accountvalue = string.Empty;
                                    if (acc.Name != null)
                                    {
                                        var dataAccount = await CommonUtilities.GetClassExistsInOnlineQBO(acc.Name.Trim());

                                        foreach (var dAccount in dataAccount)
                                        {
                                            accountvalue = dAccount.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = acc.Name,
                                            Value = accountvalue
                                        };
                                    }
                                }

                                #endregion
                                #region BillableStatus
                                if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                #region TaxCodeRef
                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                          
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {

                            foreach (var s in l.ItemBasedExpenseLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                Line.Description = s.Descreption;

                                if (s.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                        Line.Amount = Convert.ToDecimal(s.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                       
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = (Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(s.LineAmount) - Line.Amount);
                                                    TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(s.LineAmount) - Line.Amount).ToString());
                                                    //TaxRateRefGN.Add(taxRateRefName.Trim(), (Convert.ToDecimal(s.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    TaxRateRefGN.Add(ratevalue);
                                                    Line.Amount = Math.Round(Line.Amount, 2);

                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(s.LineAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                Line.Amount = Convert.ToDecimal(s.LineAmount);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                            Line.Amount = Convert.ToDecimal(s.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }

                                #region ItemRef
                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                         
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                      
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }
                                #endregion
                                #region ClassRef
                                foreach (var i in s.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region CustomerRef
                                foreach (var i in s.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region Qty
                                if (s.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(s.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {

                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                           
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                #region BillableStatus
                                if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedbill.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        linecount++;

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var accountBased in l.AccountBasedExpenseLineDetail)
                            {
                                if (accountBased.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                       
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = (Convert.ToDecimal(accountBased.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(accountBased.LineAmount) - Line.Amount);
                                                    TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(accountBased.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    TaxRateRefGN.Add(ratevalue);

                                                    Line.Amount = Math.Round(Line.Amount, 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(accountBased.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(accountBased.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(accountBased.LineAmount));
                                            Line.Amount = Convert.ToDecimal(accountBased.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (accountBased != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                Line.Description = accountBased.Descreption;
                                #region find customer
                                foreach (var cust in accountBased.customerRef)
                                {
                                    string customervalue = string.Empty;
                                  
                                    if (cust.Name != null)
                                    {
                                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(cust.Name.Trim());
                                        foreach (var customer1 in dataCustomer)
                                        {
                                            customervalue = customer1.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = cust.Name,
                                            Value = customervalue
                                        };
                                    }
                                }
                                #endregion
                                #region find AccountRef
                                QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);


                                foreach (var acc in accountBased.AccountRef)
                                {
                                    string accountvalue = string.Empty;
                                    ReadOnlyCollection<Intuit.Ipp.Data.Account> dataAccount;
                                    if (acc.Name != null && acc.Name != string.Empty && acc.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(acc.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                  
                                }

                                #endregion
                                #region find ClassRef
                                
                                foreach (var acc in accountBased.ClassRef)
                                {
                                    string accountvalue = string.Empty;
                                   
                                    if (acc.Name != null)
                                    {
                                        var dataAccount = await CommonUtilities.GetClassExistsInOnlineQBO(acc.Name.Trim());

                                        foreach (var dAccount in dataAccount)
                                        {
                                            accountvalue = dAccount.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = acc.Name,
                                            Value = accountvalue
                                        };
                                    }
                                }
                                #endregion
                                #region BillableStatus
                                if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (accountBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                #region TaxCodeRef
                                foreach (var taxCoderef in accountBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            int i = 0;
                                            for (i = 0; i < array_count; i++)
                                            {
                                                if (taxCoderef.Name == Array_taxcodeName[i])
                                                {
                                                    taxFlag = false;
                                                    break;
                                                }
                                                else
                                                {
                                                    taxFlag = true;

                                                }

                                            }
                                            if (taxFlag == false)
                                            {
                                                if (taxCoderef.Name == Array_taxcodeName[i])
                                                {
                                                    Array_taxAmount[i] += Line.Amount;
                                                    Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                }
                                            }
                                            else
                                            {
                                                array_count = array_count + 1;
                                                Array.Resize(ref Array_taxcodeName, array_count);
                                                Array.Resize(ref Array_taxAmount, array_count);
                                                Array.Resize(ref Array_AmountWithTax, array_count);
                                                Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                Array_taxAmount[array_count - 1] = Line.Amount;
                                                Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                taxFlag = false;
                                            }
                                            
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                          
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {

                            foreach (var s in l.ItemBasedExpenseLineDetail)
                            {
                                Line.Description = s.Descreption;
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (s.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                        Line.Amount = Convert.ToDecimal(s.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {

                                                QueryService<TaxCode> TAxService = new QueryService<TaxCode>(serviceContext);
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = (Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    totalTax = totalTax + (Convert.ToDecimal(s.LineAmount) - Line.Amount);
                                                    TaxRateRefGN.Add(taxRateRefName.Trim());
                                                    TaxRateRefGN.Add((Convert.ToDecimal(s.LineAmount) - Line.Amount).ToString());
                                                    TaxRateRefGN.Add(Line.Amount.ToString());
                                                    TaxRateRefGN.Add(ratevalue);
                                                    Line.Amount = Math.Round(Line.Amount, 2, MidpointRounding.AwayFromZero);

                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    //Line.Amount =Math.Round( Convert.ToDecimal(s.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)),2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(s.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(s.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(s.LineAmount));
                                            Line.Amount = Convert.ToDecimal(s.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (bill.GlobalTaxCal == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                #region ItemRef
                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }
                                }
                                #endregion

                                #region ClassRef
                                foreach (var i in s.ClassRef)
                                {
                                    string id = string.Empty;
                                   
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());

                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region CustomerRef
                                foreach (var i in s.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region Qty
                                if (s.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(s.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }
                                #endregion
                                #region TaxCodeRef
                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {

                                        string id = string.Empty;

                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            int i = 0;
                                            for (i = 0; i < array_count; i++)
                                            {
                                                if (taxCoderef.Name == Array_taxcodeName[i])
                                                {
                                                    taxFlag = false;
                                                    break;
                                                }
                                                else
                                                {
                                                    taxFlag = true;

                                                }

                                            }
                                            if (taxFlag == false)
                                            {
                                                if (taxCoderef.Name == Array_taxcodeName[i])
                                                {
                                                    Array_taxAmount[i] += Line.Amount;
                                                    Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                }
                                            }
                                            else
                                            {
                                                array_count = array_count + 1;
                                                Array.Resize(ref Array_taxcodeName, array_count);
                                                Array.Resize(ref Array_taxAmount, array_count);
                                                Array.Resize(ref Array_AmountWithTax, array_count);
                                                Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                Array_taxAmount[array_count - 1] = Line.Amount;
                                                Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                taxFlag = false;
                                            }
                                            
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                          
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region BillableStatus
                                if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedbill.Line = AddLine(lines_online, Line, linecount);

                        #endregion
                    }

                }
                #region Bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedbill.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && bill.GlobalTaxCal != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {
                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                               
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                              
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);

                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedbill.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //BUG ;; 586
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    foreach (var txntaxCode in bill.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }

                            }
                        }

                        if (txnTaxDetail != null)
                        {
                            addedbill.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                    }
                }

                #endregion
                #endregion

                #region VendorRef, bal,due date
                foreach (var VendorRef in bill.VendorRef)
                {
                    string vendorvalue = string.Empty;
                    if (VendorRef.Name != null)
                    {
                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(VendorRef.Name.Trim());
                        foreach (var vendors in dataVendor)
                        {
                            vendorvalue = vendors.Id;
                        }
                        addedbill.VendorRef = new ReferenceType()
                        {
                            name = VendorRef.Name,
                            Value = vendorvalue
                        };
                    }
                }
                if (bill.DueDate != null)
                {
                    try
                    {
                        addedbill.DueDate = Convert.ToDateTime(bill.DueDate);
                    }
                    catch { }
                }
                if (bill.Balance != null)
                {
                    addedbill.Balance = Convert.ToDecimal(bill.Balance);
                }


                #endregion

                BillAdded = new Intuit.Ipp.Data.Bill();
                BillAdded = dataService.Update<Intuit.Ipp.Data.Bill>(addedbill);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (BillAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = BillAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = BillAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        #endregion
    }
    /// <summary>
    /// checking doc number is present or not in quickbook online for bill.
    /// </summary>
    public class OnlineBillQBEntryCollection : Collection<Bill>
    {
        public Bill FindBillNumberEntry(string docNumber)
        {
            foreach (Bill item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }

    }
}

