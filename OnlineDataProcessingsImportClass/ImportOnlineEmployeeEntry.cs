﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using OnlineEntities;
using System.Runtime.CompilerServices;

namespace OnlineDataProcessingsImportClass
{
   public class ImportOnlineEmployeeEntry
    {
        private static ImportOnlineEmployeeEntry m_ImportOnlineEmployeeEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineEmployeeEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Employee class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineEmployeeEntry GetInstance()
        {
            if (m_ImportOnlineEmployeeEntry == null)
                m_ImportOnlineEmployeeEntry = new ImportOnlineEmployeeEntry();
            return m_ImportOnlineEmployeeEntry;
        }
        /// setting values to Employee data table and returns collection.
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public OnlineDataProcessingsImportClass.OnlineEmployeeQBEntryCollection ImportOnlineEmplyeeData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineEmployeeQBEntryCollection coll = new OnlineEmployeeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {                    
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Employee Validation
                    OnlineDataProcessingsImportClass.Employee employee = new OnlineDataProcessingsImportClass.Employee();
                    if (dt.Columns.Contains("EmployeeNumber"))
                    {
                        employee = coll.FindEmployeeNumberEntry(dr["EmployeeNumber"].ToString());

                        if (employee == null)
                        {

                            #region if employee is null

                            employee = new Employee();

                            if (dt.Columns.Contains("EmployeeNumber"))
                            {
                                #region Validations of EmployeeNumber
                                if (dr["EmployeeNumber"].ToString() != string.Empty)
                                {
                                    if (dr["EmployeeNumber"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This EmployeeNumber (" + dr["EmployeeNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.EmployeeNumber = dr["EmployeeNumber"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.EmployeeNumber = dr["EmployeeNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.EmployeeNumber = dr["EmployeeNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.EmployeeNumber = dr["EmployeeNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Title"))
                            {
                                #region Validations of Title
                                if (dr["Title"].ToString() != string.Empty)
                                {
                                    if (dr["Title"].ToString().Length > 15)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Title (" + dr["Title"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.Title = dr["Title"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.Title = dr["Title"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.Title = dr["Title"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.Title = dr["Title"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("GivenName"))
                            {
                                #region Validations of GivenName
                                if (dr["GivenName"].ToString() != string.Empty)
                                {
                                    if (dr["GivenName"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This GivenName (" + dr["GivenName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.GivenName = dr["GivenName"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.GivenName = dr["GivenName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.GivenName = dr["GivenName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.GivenName = dr["GivenName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("MiddleName"))
                            {
                                #region Validations of GivenName
                                if (dr["MiddleName"].ToString() != string.Empty)
                                {
                                    if (dr["MiddleName"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.MiddleName = dr["MiddleName"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.MiddleName = dr["MiddleName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.MiddleName = dr["MiddleName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("FamilyName"))
                            {
                                #region Validations of FamilyName
                                if (dr["FamilyName"].ToString() != string.Empty)
                                {
                                    if (dr["FamilyName"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FamilyName (" + dr["FamilyName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.FamilyName = dr["FamilyName"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.FamilyName = dr["FamilyName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.FamilyName = dr["FamilyName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.FamilyName = dr["FamilyName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Suffix"))
                            {
                                #region Validations of Suffix
                                if (dr["Suffix"].ToString() != string.Empty)
                                {
                                    if (dr["Suffix"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Suffix (" + dr["Suffix"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.Suffix = dr["Suffix"].ToString().Substring(0, 10);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.Suffix = dr["Suffix"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.Suffix = dr["Suffix"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.Suffix = dr["Suffix"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("DisplayName"))
                            {
                                #region Validations of DisplayName
                                if (dr["DisplayName"].ToString() != string.Empty)
                                {
                                    if (dr["DisplayName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DisplayName (" + dr["DisplayName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.DisplayName = dr["DisplayName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.DisplayName = dr["DisplayName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.DisplayName = dr["DisplayName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.DisplayName = dr["DisplayName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SSN"))
                            {
                                #region Validations of SSN
                                if (dr["SSN"].ToString() != string.Empty)
                                {
                                    if (dr["SSN"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SSN (" + dr["SSN"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.SSN = dr["SSN"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.SSN = dr["SSN"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.SSN = dr["SSN"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.SSN = dr["SSN"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PrintOnCheckName"))
                            {
                                #region Validations of PrintOnCheckName
                                if (dr["PrintOnCheckName"].ToString() != string.Empty)
                                {
                                    if (dr["PrintOnCheckName"].ToString().Length > 110)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrintOnCheckName (" + dr["PrintOnCheckName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString().Substring(0, 110);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Gender"))
                            {
                                #region Validations of Gender
                                if (dr["Gender"].ToString() != string.Empty)
                                {
                                    if (dr["Gender"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Gender (" + dr["Gender"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.Gender = dr["Gender"].ToString().Substring(0, 10);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.Gender = dr["Gender"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.Gender = dr["Gender"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.Gender = dr["Gender"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("BirthDate"))
                            {
                                #region Validations of BirthDate
                                DateTime birthDate = new DateTime();
                                if (dr["BirthDate"].ToString() != "<None>" || dr["BirthDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["BirthDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out birthDate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This BirthDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    employee.BirthDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    employee.BirthDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                employee.BirthDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            birthDate = dttest;
                                            employee.BirthDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        birthDate = Convert.ToDateTime(datevalue);
                                        employee.BirthDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("HiredDate"))
                            {
                                #region Validations of HiredDate
                                DateTime hiredDate = new DateTime();
                                if (dr["HiredDate"].ToString() != "<None>" || dr["HiredDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["HiredDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out hiredDate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This HiredDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    employee.HiredDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    employee.HiredDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                employee.HiredDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            hiredDate = dttest;
                                            employee.HiredDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        hiredDate = Convert.ToDateTime(datevalue);
                                        employee.HiredDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ReleasedDate"))
                            {
                                #region Validations of ReleasedDate
                                DateTime releasedDate = new DateTime();
                                if (dr["ReleasedDate"].ToString() != "<None>" || dr["ReleasedDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["ReleasedDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out releasedDate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This HiredDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    employee.ReleasedDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    employee.ReleasedDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                employee.ReleasedDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            releasedDate = dttest;
                                            employee.ReleasedDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        releasedDate = Convert.ToDateTime(datevalue);
                                        employee.ReleasedDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Organization"))
                            {
                                #region Validations of Organization
                                if (dr["Organization"].ToString() != "<None>" || dr["Organization"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["Organization"].ToString(), out result))
                                    {
                                        employee.Organization = Convert.ToInt32(dr["Organization"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["Organization"].ToString().ToLower() == "true")
                                        {
                                            employee.Organization = dr["Organization"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["Organization"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "Organization";
                                            }
                                            else
                                                employee.Organization = dr["Organization"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Organization(" + dr["Organization"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    employee.Organization = dr["Organization"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    employee.Organization = dr["Organization"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                employee.Organization = dr["Organization"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Active"))
                            {
                                #region Validations of Active
                                if (dr["Active"].ToString() != "<None>" || dr["Active"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["Active"].ToString(), out result))
                                    {
                                        employee.Organization = Convert.ToInt32(dr["Active"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["Active"].ToString().ToLower() == "true")
                                        {
                                            employee.Active = dr["Active"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["Active"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "Active";
                                            }
                                            else
                                                employee.Active = dr["Active"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Active(" + dr["Active"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    employee.Active = dr["Active"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    employee.Active = dr["Active"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                employee.Active = dr["Active"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BillableTime"))
                            {
                                #region Validations of BillableTime
                                if (dr["BillableTime"].ToString() != "<None>" || dr["BillableTime"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["BillableTime"].ToString(), out result))
                                    {
                                        employee.BillableTime = Convert.ToInt32(dr["BillableTime"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["BillableTime"].ToString().ToLower() == "true")
                                        {
                                            employee.BillableTime = dr["BillableTime"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["BillableTime"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "BillableTime";
                                            }
                                            else
                                                employee.BillableTime = dr["BillableTime"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This BillableTime(" + dr["BillableTime"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    employee.BillableTime = dr["BillableTime"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    employee.BillableTime = dr["BillableTime"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                employee.BillableTime = dr["BillableTime"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            //PrimaryAddress
                            OnlineEntities.BillAddr primaryAddr = new OnlineEntities.BillAddr();
                            if (dt.Columns.Contains("PrimaryAddrLine1"))
                            {
                                #region Validations of PrimaryAddrLine1
                                if (dr["PrimaryAddrLine1"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrLine1"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrLine1 (" + dr["PrimaryAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrLine2"))
                            {
                                #region Validations of PrimaryAddrLine2
                                if (dr["PrimaryAddrLine2"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrLine2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrLine2 (" + dr["PrimaryAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrLine3"))
                            {
                                #region Validations of PrimaryAddrLine3
                                if (dr["PrimaryAddrLine3"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrLine3"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrLine3 (" + dr["PrimaryAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrCity"))
                            {
                                #region Validations of PrimaryAddrCity
                                if (dr["PrimaryAddrCity"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrCity"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrCity (" + dr["PrimaryAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrCountry"))
                            {
                                #region Validations of PrimaryAddrCountry
                                if (dr["PrimaryAddrCountry"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrCountry"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrCountry (" + dr["PrimaryAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrSubDivisionCode"))
                            {
                                #region Validations of PrimaryAddrSubDivisionCode
                                if (dr["PrimaryAddrSubDivisionCode"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrSubDivisionCode"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrSubDivisionCode (" + dr["PrimaryAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrPostalCode"))
                            {
                                #region Validations of PrimaryAddrPostalCode
                                if (dr["PrimaryAddrPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrPostalCode"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrPostalCode (" + dr["PrimaryAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrNote"))
                            {
                                #region Validations of PrimaryAddrNote
                                if (dr["PrimaryAddrNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrNote"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrNote (" + dr["PrimaryAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrLine4"))
                            {
                                #region Validations of PrimaryAddrLine4
                                if (dr["PrimaryAddrLine4"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrLine4"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrLine4 (" + dr["PrimaryAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrLine5"))
                            {
                                #region Validations of PrimaryAddrLine5
                                if (dr["PrimaryAddrLine5"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrLine5"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrLine5 (" + dr["PrimaryAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PrimaryAddrLat"))
                            {
                                #region Validations of PrimaryAddrLat
                                if (dr["PrimaryAddrLat"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrLat"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrLat (" + dr["PrimaryAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrimaryAddrLong"))
                            {
                                #region Validations of PrimaryAddrLong
                                if (dr["PrimaryAddrLong"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryAddrLong"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryAddrLong (" + dr["PrimaryAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (primaryAddr.BillLine1 != null || primaryAddr.BillLine2 != null || primaryAddr.BillLine3 != null || primaryAddr.BillLine4 != null || primaryAddr.BillLine5 != null || primaryAddr.BillCity != null || primaryAddr.BillCountry != null || primaryAddr.BillCountrySubDivisionCode != null || primaryAddr.BillNote != null || primaryAddr.BillAddrLat != null || primaryAddr.BillAddrLong != null || primaryAddr.BillPostalCode != null)
                                employee.PrimaryAddr.Add(primaryAddr);
                           
                            //Telephone Number
                            OnlineEntities.TelephoneNumber telephoneNo = new OnlineEntities.TelephoneNumber();
                            if (dt.Columns.Contains("PrimaryPhone"))
                            {
                                #region Validations of PrimaryPhone
                                if (dr["PrimaryPhone"].ToString() != string.Empty)
                                {
                                    if (dr["PrimaryPhone"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrimaryPhone (" + dr["PrimaryPhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Mobile"))
                            {
                                #region Validations of Mobile
                                if (dr["Mobile"].ToString() != string.Empty)
                                {
                                    if (dr["Mobile"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Mobile (" + dr["Mobile"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                telephoneNo.Mobile = dr["Mobile"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                telephoneNo.Mobile = dr["Mobile"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            telephoneNo.Mobile = dr["Mobile"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        telephoneNo.Mobile = dr["Mobile"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (telephoneNo.PrimaryPhone != null)
                                employee.PrimaryPhone.Add(telephoneNo);
                            if (telephoneNo.Mobile != null)
                                employee.Mobile.Add(telephoneNo);

                            //EmailAddress
                            OnlineEntities.EmailAddressEmployee emailAddress = new OnlineEntities.EmailAddressEmployee();
                            if (dt.Columns.Contains("EmailAddress"))
                            {
                                #region Validations of EmailAddress
                                if (dr["EmailAddress"].ToString() != string.Empty)
                                {
                                    if (dr["EmailAddress"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Mobile (" + dr["EmailAddress"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                emailAddress.EmailAddress = dr["EmailAddress"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                emailAddress.EmailAddress = dr["EmailAddress"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            emailAddress.EmailAddress = dr["EmailAddress"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        emailAddress.EmailAddress = dr["EmailAddress"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (emailAddress.EmailAddress != null)
                                employee.PrimaryEmailAddr.Add(emailAddress);

                            if (dt.Columns.Contains("BillRate"))
                            {
                                #region Validations for BillRate
                                if (dr["BillRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["BillRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillRate (" + dr["BillRate"].ToString() + ") is not valid for quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.BillRate = dr["BillRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.BillRate = dr["BillRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.BillRate = dr["BillRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.BillRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["BillRate"].ToString()));
                                    }
                                }
                                #endregion
                            }

                            #endregion

                            coll.Add(employee);
                        }                        
                    }
                    else
                    {
                        employee = new Employee();

                        #region if EmployeeNumber not present
                        employee = new Employee();
                       
                        if (dt.Columns.Contains("Title"))
                        {
                            #region Validations of Title
                            if (dr["Title"].ToString() != string.Empty)
                            {
                                if (dr["Title"].ToString().Length > 15)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Title (" + dr["Title"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.Title = dr["Title"].ToString().Substring(0, 15);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.Title = dr["Title"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.Title = dr["Title"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.Title = dr["Title"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("GivenName"))
                        {
                            #region Validations of GivenName
                            if (dr["GivenName"].ToString() != string.Empty)
                            {
                                if (dr["GivenName"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This GivenName (" + dr["GivenName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.GivenName = dr["GivenName"].ToString().Substring(0, 25);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.GivenName = dr["GivenName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.GivenName = dr["GivenName"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.GivenName = dr["GivenName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("MiddleName"))
                        {
                            #region Validations of GivenName
                            if (dr["MiddleName"].ToString() != string.Empty)
                            {
                                if (dr["MiddleName"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.MiddleName = dr["MiddleName"].ToString().Substring(0, 25);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.MiddleName = dr["MiddleName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.MiddleName = dr["MiddleName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("FamilyName"))
                        {
                            #region Validations of FamilyName
                            if (dr["FamilyName"].ToString() != string.Empty)
                            {
                                if (dr["FamilyName"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This FamilyName (" + dr["FamilyName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.FamilyName = dr["FamilyName"].ToString().Substring(0, 25);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.FamilyName = dr["FamilyName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.FamilyName = dr["FamilyName"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.FamilyName = dr["FamilyName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Suffix"))
                        {
                            #region Validations of Suffix
                            if (dr["Suffix"].ToString() != string.Empty)
                            {
                                if (dr["Suffix"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Suffix (" + dr["Suffix"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.Suffix = dr["Suffix"].ToString().Substring(0, 10);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.Suffix = dr["Suffix"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.Suffix = dr["Suffix"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.Suffix = dr["Suffix"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("DisplayName"))
                        {
                            #region Validations of DisplayName
                            if (dr["DisplayName"].ToString() != string.Empty)
                            {
                                if (dr["DisplayName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DisplayName (" + dr["DisplayName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.DisplayName = dr["DisplayName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.DisplayName = dr["DisplayName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.DisplayName = dr["DisplayName"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.DisplayName = dr["DisplayName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SSN"))
                        {
                            #region Validations of SSN
                            if (dr["SSN"].ToString() != string.Empty)
                            {
                                if (dr["SSN"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SSN (" + dr["SSN"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.SSN = dr["SSN"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.SSN = dr["SSN"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.SSN = dr["SSN"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.SSN = dr["SSN"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("PrintOnCheckName"))
                        {
                            #region Validations of PrintOnCheckName
                            if (dr["PrintOnCheckName"].ToString() != string.Empty)
                            {
                                if (dr["PrintOnCheckName"].ToString().Length > 110)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrintOnCheckName (" + dr["PrintOnCheckName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString().Substring(0, 110);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.PrintOnCheckName = dr["PrintOnCheckName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Gender"))
                        {
                            #region Validations of Gender
                            if (dr["Gender"].ToString() != string.Empty)
                            {
                                if (dr["Gender"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Gender (" + dr["Gender"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.Gender = dr["Gender"].ToString().Substring(0, 10);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.Gender = dr["Gender"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.Gender = dr["Gender"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.Gender = dr["Gender"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("BirthDate"))
                        {
                            #region Validations of BirthDate
                            DateTime birthDate = new DateTime();
                            if (dr["BirthDate"].ToString() != "<None>" || dr["BirthDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["BirthDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out birthDate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BirthDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.BirthDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.BirthDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            employee.BirthDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        birthDate = dttest;
                                        employee.BirthDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    birthDate = Convert.ToDateTime(datevalue);
                                    employee.BirthDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("HiredDate"))
                        {
                            #region Validations of HiredDate
                            DateTime hiredDate = new DateTime();
                            if (dr["HiredDate"].ToString() != "<None>" || dr["HiredDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["HiredDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out hiredDate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This HiredDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.HiredDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.HiredDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            employee.HiredDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        hiredDate = dttest;
                                        employee.HiredDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    hiredDate = Convert.ToDateTime(datevalue);
                                    employee.HiredDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReleasedDate"))
                        {
                            #region Validations of ReleasedDate
                            DateTime releasedDate = new DateTime();
                            if (dr["ReleasedDate"].ToString() != "<None>" || dr["ReleasedDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["ReleasedDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out releasedDate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This HiredDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                employee.ReleasedDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.ReleasedDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            employee.ReleasedDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        releasedDate = dttest;
                                        employee.ReleasedDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    releasedDate = Convert.ToDateTime(datevalue);
                                    employee.ReleasedDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Organization"))
                        {
                            #region Validations of Organization
                            if (dr["Organization"].ToString() != "<None>" || dr["Organization"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["Organization"].ToString(), out result))
                                {
                                    employee.Organization = Convert.ToInt32(dr["Organization"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["Organization"].ToString().ToLower() == "true")
                                    {
                                        employee.Organization = dr["Organization"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["Organization"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "Organization";
                                        }
                                        else
                                            employee.Organization = dr["Organization"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Organization(" + dr["Organization"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                employee.Organization = dr["Organization"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.Organization = dr["Organization"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.Organization = dr["Organization"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Active"))
                        {
                            #region Validations of Active
                            if (dr["Active"].ToString() != "<None>" || dr["Active"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["Active"].ToString(), out result))
                                {
                                    employee.Organization = Convert.ToInt32(dr["Active"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["Active"].ToString().ToLower() == "true")
                                    {
                                        employee.Active = dr["Active"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["Active"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "Active";
                                        }
                                        else
                                            employee.Active = dr["Active"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Active(" + dr["Active"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                employee.Active = dr["Active"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.Active = dr["Active"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.Active = dr["Active"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillableTime"))
                        {
                            #region Validations of BillableTime
                            if (dr["BillableTime"].ToString() != "<None>" || dr["BillableTime"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["BillableTime"].ToString(), out result))
                                {
                                    employee.BillableTime = Convert.ToInt32(dr["BillableTime"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["BillableTime"].ToString().ToLower() == "true")
                                    {
                                        employee.BillableTime = dr["BillableTime"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["BillableTime"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "BillableTime";
                                        }
                                        else
                                            employee.BillableTime = dr["BillableTime"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillableTime(" + dr["BillableTime"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                employee.BillableTime = dr["BillableTime"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                employee.BillableTime = dr["BillableTime"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            employee.BillableTime = dr["BillableTime"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        //PrimaryAddress
                        OnlineEntities.BillAddr primaryAddr = new OnlineEntities.BillAddr();
                        if (dt.Columns.Contains("PrimaryAddrLine1"))
                        {
                            #region Validations of PrimaryAddrLine1
                            if (dr["PrimaryAddrLine1"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrLine1"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrLine1 (" + dr["PrimaryAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillLine1 = dr["PrimaryAddrLine1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrLine2"))
                        {
                            #region Validations of PrimaryAddrLine2
                            if (dr["PrimaryAddrLine2"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrLine2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrLine2 (" + dr["PrimaryAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillLine2 = dr["PrimaryAddrLine2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrLine3"))
                        {
                            #region Validations of PrimaryAddrLine3
                            if (dr["PrimaryAddrLine3"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrLine3"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrLine3 (" + dr["PrimaryAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillLine3 = dr["PrimaryAddrLine3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrCity"))
                        {
                            #region Validations of PrimaryAddrCity
                            if (dr["PrimaryAddrCity"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrCity"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrCity (" + dr["PrimaryAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillCity = dr["PrimaryAddrCity"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrCountry"))
                        {
                            #region Validations of PrimaryAddrCountry
                            if (dr["PrimaryAddrCountry"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrCountry"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrCountry (" + dr["PrimaryAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillCountry = dr["PrimaryAddrCountry"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrSubDivisionCode"))
                        {
                            #region Validations of PrimaryAddrSubDivisionCode
                            if (dr["PrimaryAddrSubDivisionCode"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrSubDivisionCode"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrSubDivisionCode (" + dr["PrimaryAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillCountrySubDivisionCode = dr["PrimaryAddrSubDivisionCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrPostalCode"))
                        {
                            #region Validations of PrimaryAddrPostalCode
                            if (dr["PrimaryAddrPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrPostalCode"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrPostalCode (" + dr["PrimaryAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillPostalCode = dr["PrimaryAddrPostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrNote"))
                        {
                            #region Validations of PrimaryAddrNote
                            if (dr["PrimaryAddrNote"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrNote"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrNote (" + dr["PrimaryAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillNote = dr["PrimaryAddrNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrLine4"))
                        {
                            #region Validations of PrimaryAddrLine4
                            if (dr["PrimaryAddrLine4"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrLine4"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrLine4 (" + dr["PrimaryAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillLine4 = dr["PrimaryAddrLine4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrLine5"))
                        {
                            #region Validations of PrimaryAddrLine5
                            if (dr["PrimaryAddrLine5"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrLine5"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrLine5 (" + dr["PrimaryAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillLine5 = dr["PrimaryAddrLine5"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("PrimaryAddrLat"))
                        {
                            #region Validations of PrimaryAddrLat
                            if (dr["PrimaryAddrLat"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrLat"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrLat (" + dr["PrimaryAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillAddrLat = dr["PrimaryAddrLat"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrimaryAddrLong"))
                        {
                            #region Validations of PrimaryAddrLong
                            if (dr["PrimaryAddrLong"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryAddrLong"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryAddrLong (" + dr["PrimaryAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    primaryAddr.BillAddrLong = dr["PrimaryAddrLong"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (primaryAddr.BillLine1 != null || primaryAddr.BillLine2 != null || primaryAddr.BillLine3 != null || primaryAddr.BillLine4 != null || primaryAddr.BillLine5 != null || primaryAddr.BillCity != null || primaryAddr.BillCountry != null || primaryAddr.BillCountrySubDivisionCode != null || primaryAddr.BillNote != null || primaryAddr.BillAddrLat != null || primaryAddr.BillAddrLong != null || primaryAddr.BillPostalCode != null)
                            employee.PrimaryAddr.Add(primaryAddr);

                        //Telephone Number
                        OnlineEntities.TelephoneNumber telephoneNo = new OnlineEntities.TelephoneNumber();
                        if (dt.Columns.Contains("PrimaryPhone"))
                        {
                            #region Validations of PrimaryPhone
                            if (dr["PrimaryPhone"].ToString() != string.Empty)
                            {
                                if (dr["PrimaryPhone"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrimaryPhone (" + dr["PrimaryPhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Mobile"))
                        {
                            #region Validations of Mobile
                            if (dr["Mobile"].ToString() != string.Empty)
                            {
                                if (dr["Mobile"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Mobile (" + dr["Mobile"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            telephoneNo.Mobile = dr["Mobile"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            telephoneNo.Mobile = dr["Mobile"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        telephoneNo.Mobile = dr["Mobile"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.Mobile = dr["Mobile"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (telephoneNo.PrimaryPhone != null)
                            employee.PrimaryPhone.Add(telephoneNo);
                        if (telephoneNo.Mobile != null)
                            employee.Mobile.Add(telephoneNo);

                        //EmailAddress
                        OnlineEntities.EmailAddressEmployee emailAddress = new OnlineEntities.EmailAddressEmployee();
                        if (dt.Columns.Contains("EmailAddress"))
                        {
                            #region Validations of EmailAddress
                            if (dr["EmailAddress"].ToString() != string.Empty)
                            {
                                if (dr["EmailAddress"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Mobile (" + dr["EmailAddress"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            emailAddress.EmailAddress = dr["EmailAddress"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            emailAddress.EmailAddress = dr["EmailAddress"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        emailAddress.EmailAddress = dr["EmailAddress"].ToString();
                                    }
                                }
                                else
                                {
                                    emailAddress.EmailAddress = dr["EmailAddress"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (emailAddress.EmailAddress != null)
                            employee.PrimaryEmailAddr.Add(emailAddress);

                        if (dt.Columns.Contains("BillRate"))
                        {
                            #region Validations for BillRate
                            if (dr["BillRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["BillRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillRate (" + dr["BillRate"].ToString() + ") is not valid for quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            employee.BillRate = dr["BillRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            employee.BillRate = dr["BillRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        employee.BillRate = dr["BillRate"].ToString();
                                    }
                                }
                                else
                                {
                                    employee.BillRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["BillRate"].ToString()));
                                }
                            }
                            #endregion
                        }
                        #endregion

                        coll.Add(employee);
                    }
                    
               }
               else
               {
                  return null;
               }

        }
            #endregion
            
            #endregion
           
            return coll;

        }
    }
}
