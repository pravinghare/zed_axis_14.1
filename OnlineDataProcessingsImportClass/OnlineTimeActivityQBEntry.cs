﻿using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using System.Collections.ObjectModel;
using System;
using System.IO;
using System.Json;
using EDI.Constant;

namespace OnlineDataProcessingsImportClass
{
    public class OnlineTimeActivityQBEntry
    {
        #region member
        private string m_Id;
        private string m_TxnDate;
        private string m_NameOf;
        private Collection<OnlineEntities.EmployeeOrVendorRef> m_EmployeeOrVendorRef = new Collection<OnlineEntities.EmployeeOrVendorRef>();
        private Collection<OnlineEntities.CustomerRef> m_CustomerRef = new Collection<OnlineEntities.CustomerRef>();
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private Collection<OnlineEntities.PayrollItemRef> m_PayrollItemRef = new Collection<OnlineEntities.PayrollItemRef>();
        private Collection<OnlineEntities.ItemRef> m_ItemRef = new Collection<OnlineEntities.ItemRef>();
        private Collection<OnlineEntities.ClassRef> m_ClassRef = new Collection<OnlineEntities.ClassRef>();   
        private string m_BillableStatus;
        private string m_Taxable;
        private string m_HourlyRate;
        private string m_Hours;
        private string m_Minutes;
        private string m_BreakHours;
        private string m_BreakMinutes;
        private string m_StartTime;
        private string m_EndTime;       
        private string m_Description;

        //594
        private bool m_isAppend;

       
        #endregion

        #region Properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }  
        public string NameOf
        {
            get { return m_NameOf; }
            set { m_NameOf = value; }
        }      

        public Collection<OnlineEntities.EmployeeOrVendorRef> EmployeeOrVendorRef
        {
            get { return m_EmployeeOrVendorRef; }
            set { m_EmployeeOrVendorRef = value; }
        }       

        public Collection<OnlineEntities.CustomerRef> CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }       

        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }
        public Collection<OnlineEntities.PayrollItemRef> PayrollItemRef
        {
            get { return m_PayrollItemRef; }
            set { m_PayrollItemRef = value; }
        }
        public Collection<OnlineEntities.ItemRef> ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }

        public Collection<OnlineEntities.ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }      

        public string BillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }
      

        public string Taxable
        {
            get { return m_Taxable; }
            set { m_Taxable = value; }
        }
       

        public string HourlyRate
        {
            get { return m_HourlyRate; }
            set { m_HourlyRate = value; }
        }
       

        public string Hours
        {
            get { return m_Hours; }
            set { m_Hours = value; }
        }
       

        public string Minutes
        {
            get { return m_Minutes; }
            set { m_Minutes = value; }
        }
       

        public string BreakHours
        {
            get { return m_BreakHours; }
            set { m_BreakHours = value; }
        }

        public string BreakMinutes
        {
            get { return m_BreakMinutes; }
            set { m_BreakMinutes = value; }
        }
      
        public string StartTime
        {
            get { return m_StartTime; }
            set { m_StartTime = value; }
        }
       

        public string EndTime
        {
            get { return m_EndTime; }
            set { m_EndTime = value; }
        }
       

        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region public method
        /// <summary>
        /// Creating new Time Activity transaction in quickbook online. 
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOTimeActivityEntry(OnlineDataProcessingsImportClass.OnlineTimeActivityQBEntry coll)
        {

            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.TimeActivity addTimeActivity = new Intuit.Ipp.Data.TimeActivity();
            OnlineTimeActivityQBEntry TimeActivity = new OnlineTimeActivityQBEntry();
            TimeActivity = coll;
            Intuit.Ipp.Data.TimeActivity TimeActivityAdded = null;
           
            try
            {
                #region Add Time Activity

                //594
                if (TimeActivity.isAppend == true)
                {
                    addTimeActivity.sparse = true;
                    addTimeActivity.sparseSpecified = true;
                }

                if (TimeActivity.TxnDate != null)
                {
                    try
                    {
                        addTimeActivity.TxnDate = Convert.ToDateTime(TimeActivity.TxnDate);
                        addTimeActivity.TxnDateSpecified = true;
                    }
                    catch { }
                }
                 
                #region Add Time Activity NameOf
                if (TimeActivity.NameOf == null)
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Employee;
                    addTimeActivity.NameOfSpecified = true;
                }
                else if (TimeActivity.NameOf == TimeActivityTypeEnum.Employee.ToString())
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Employee;
                    addTimeActivity.NameOfSpecified = true;
                }
                else if (TimeActivity.NameOf == TimeActivityTypeEnum.Vendor.ToString())
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Vendor;
                    addTimeActivity.NameOfSpecified = true;
                }
                else if (TimeActivity.NameOf == TimeActivityTypeEnum.Other.ToString())
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Other;
                    addTimeActivity.NameOfSpecified = true;
                }
                
                #endregion

                #region Get Employee or vendor id

                 //find department 
                if (TimeActivity.EmployeeOrVendorRef.Count > 0)
                {
                    foreach (var i in TimeActivity.EmployeeOrVendorRef)
                    {

                        string displayname = string.Empty;
                        string id = string.Empty;
                        if (TimeActivity.NameOf.ToLower() == TimeActivityTypeEnum.Employee.ToString().ToLower() || TimeActivity.NameOf == null)
                        {
                                displayname = i.Name;
                                if (displayname != string.Empty)
                                {
                                    var dataEmployee = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(displayname.Trim());
                               
                                    //if (dataEmployee.Count == 0)
                                    //{
                                    //    string strMessages = "The EmployeeName  (" + displayname + ") is not available in quickbook.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    //    System.Windows.Forms.DialogResult result = CommonUtilities.ShowMessage(strMessages, System.Windows.Forms.MessageBoxIcon.Warning);
                                 
                                    //    if (Convert.ToString(result) == "Cancel")
                                    //    {
                                    //        continue;
                                    //    }
                                    //    if (Convert.ToString(result) == "No")
                                    //    {
                                    //        return false;
                                    //    }
                                    //    if (Convert.ToString(result) == "Ignore")
                                    //    {
                                    //        return false;
                                    //    }
                                    //    if (Convert.ToString(result) == "Abort")
                                    //    {
                                    //        return false;
                                    //    }

                                    //}

                                    foreach (var item in dataEmployee)
                                    {
                                        id = item.Id;
                                    }
                                    addTimeActivity.AnyIntuitObject = new ReferenceType()
                                    {
                                        name = displayname,
                                        Value = id,
                                      // type="Employee"
                                       
                                    };
                                    addTimeActivity.ItemElementName=ItemChoiceType5.EmployeeRef;
                                }
                            
                        }
                        //bug 498
                        else if (TimeActivity.NameOf.ToLower() == TimeActivityTypeEnum.Vendor.ToString().ToLower())
                        {
                            displayname = i.Name;
                            if (displayname != string.Empty)
                            {
                                var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(displayname.Trim());
                                //if (dataVendor.Count == 0)
                                //{
                                //    CommonUtilities.GetInstance().getError = "Vendor Name not exists in quickbook";
                                //    break;

                                //}

                                foreach (var item in dataVendor)
                                {
                                    id = item.Id;
                                }
                                addTimeActivity.AnyIntuitObject = new ReferenceType()
                                {
                                    name = displayname,
                                    Value = id,
                                   // type="Vendor"
                                };
                                 addTimeActivity.ItemElementName=ItemChoiceType5.VendorRef;
                               
                            }
                        }

                    }
                }
                #endregion

                #region Get Customer Id
                foreach (var customerRef in TimeActivity.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }

                        addTimeActivity.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }
                }
                
                    
                #endregion

                #region  find department
               
                    foreach (var i in TimeActivity.DepartmentRef)
                    {                       
                        string id = string.Empty;
                        if (i.Name != null)
                        {
                            var dataDepartment = await CommonUtilities.GetDepartmentExistsInOnlineQBO(i.Name.Trim());
                            foreach (var item in dataDepartment)
                            {
                                id = item.Id;
                            }
                            addTimeActivity.DepartmentRef = new ReferenceType()
                            {
                                name = i.Name,
                                Value = id
                            };
                        }
                    }

                #endregion

                #region  find item id

                foreach (var i in TimeActivity.ItemRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addTimeActivity.ItemRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }

                #endregion


                #region  find Payrollitem id

                foreach (var i in TimeActivity.PayrollItemRef)
                {
                    string id = string.Empty;
                    try
                    {
                        if (i.Name != null)
                        {
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Constants.QuickbookGraphQlUrl);
                            request.Method = "POST";
                            request.Headers.Add("Authorization", "Bearer " + CommonUtilities.GetBearertoken());
                            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                            Byte[] byteArray = encoding.GetBytes(Constants.ReadEmployeeCompensation);
                            request.ContentLength = byteArray.Length;
                            request.ContentType = "application/json";
                            using (Stream dataStream = request.GetRequestStream())
                            {
                                dataStream.Write(byteArray, 0, byteArray.Length);
                            }
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            string responseText = string.Empty;
                            using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                            {
                                responseText = reader.ReadToEnd();
                            }

                            var jsonResponse = JsonValue.Parse(responseText);
                            foreach (JsonObject employeeEdge in jsonResponse["data"]["company"]["employeeContacts"]["edges"])
                            {
                                string employeeId = string.Empty;

                                JsonValue employeeNode = employeeEdge["node"];
                                foreach (JsonObject compensationEdge in employeeEdge["node"]["externalIds"])
                                {
                                    employeeId = (string)compensationEdge["localId"];

                                }
                                if (employeeId != string.Empty && addTimeActivity.AnyIntuitObject != null && addTimeActivity.AnyIntuitObject.Value == employeeId)
                                {
                                    foreach (JsonObject compensationEdge in employeeNode["profiles"]["employee"]["compensations"]["edges"])
                                    {
                                        JsonValue compensationNode = compensationEdge["node"];
                                        var compensationId = (string)compensationNode["id"];
                                        var compensationName = (string)compensationNode["employerCompensation"]["name"];
                                        // var compensationAmount = compensationNode["amount"] == null ? 0 : double.Parse((string)compensationNode["amount"]);
                                        if (compensationName != null && compensationName == i.Name)
                                        {
                                            i.Value = compensationId;
                                            addTimeActivity.PayrollItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = i.Value
                                            };
                                            break;
                                        }
                                    }
                                }
                                if (i.Value != null)
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                // End 718



                #endregion
                #region find class id

                foreach (var i in TimeActivity.ClassRef)
                    {                       
                        string id = string.Empty;
                        if (i.Name != null)
                        {
                           var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                       
                           foreach (var item in dataItem)
                            {
                                id = item.Id;
                            }
                            addTimeActivity.ClassRef = new ReferenceType()
                            {
                                name = i.Name,
                                Value = id
                            };
                        }
                    }
                
                #endregion

                #region BillableStatus
                if(TimeActivity.BillableStatus !=null)
                {
                    if (TimeActivity.BillableStatus == BillableStatusEnum.Billable.ToString())
                    {
                        addTimeActivity.BillableStatus = BillableStatusEnum.Billable;
                        addTimeActivity.BillableStatusSpecified = true;
                    }
                    else if (TimeActivity.BillableStatus == BillableStatusEnum.HasBeenBilled.ToString())
                    {
                        addTimeActivity.BillableStatus = BillableStatusEnum.HasBeenBilled;
                        addTimeActivity.BillableStatusSpecified = true;
                    }
                    else if (TimeActivity.BillableStatus == BillableStatusEnum.NotBillable.ToString())
                    {
                        addTimeActivity.BillableStatus = BillableStatusEnum.NotBillable;
                        addTimeActivity.BillableStatusSpecified = true;
                    }
                }

                #endregion

                #region  find item id

                //foreach (var i in TimeActivity.)
                //{
                //    string id = string.Empty;
                //    if (i.Name != null)
                //    {
                //        ReadOnlyCollection<Intuit.Ipp.Data.Item> dataItem = null;
                //        QueryService<Item> ItemQueryService = new QueryService<Item>(serviceContext);
                //        if (i.Name.Contains("'"))
                //        {
                //            i.Name = i.Name.Replace("'", "\\'");
                //        }
                //        dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Item>(ItemQueryService.ExecuteIdsQuery("Select * From Item where FullyQualifiedName='" + i.Name.Trim() + "'"));
                //        foreach (var item in dataItem)
                //        {
                //            id = item.Id;
                //        }
                //        addTimeActivity.PayrollItemRef = new ReferenceType()
                //        {
                //            name = i.Name,
                //            Value = id
                //        };
                //    }
                //}
               
                #endregion

                if (TimeActivity.Taxable != null)
                {
                    addTimeActivity.Taxable = Convert.ToBoolean(TimeActivity.Taxable);
                    addTimeActivity.TaxableSpecified = true;
                }
                if (TimeActivity.HourlyRate != null)
                {
                    addTimeActivity.HourlyRate = Convert.ToDecimal(TimeActivity.HourlyRate);
                    addTimeActivity.HourlyRateSpecified = true;
                }

                if (TimeActivity.Hours != null)
                {
                    int valOfHours = 0, valOfMinute = 0;

                    if (TimeActivity.Hours.Contains(":"))
                    {
                        string[] arry = new string[2];
                        string[] array_msg = TimeActivity.Hours.ToString().Split(':');
                        valOfHours = Convert.ToInt32(array_msg[0]);
                        valOfMinute = Convert.ToInt32(array_msg[1]);
                        string min = valOfMinute.ToString();
                        while (min.Length > 2)
                        {
                            min = min.Remove(min.Length - 1);
                        }
                        valOfMinute = Convert.ToInt32(min);
                        if (valOfMinute > 59)
                        {
                            valOfMinute = valOfMinute - 60;
                            valOfHours++;
                        }

                        TimeActivity.Hours = valOfHours.ToString();
                        TimeActivity.Minutes = valOfMinute.ToString();

                        if (Convert.ToInt32(TimeActivity.Hours) > 0)
                        {
                            addTimeActivity.Hours = Convert.ToInt32(TimeActivity.Hours);
                            addTimeActivity.HoursSpecified = true;
                        }
                        if (Convert.ToInt32(TimeActivity.Minutes) > 0)
                        {
                            addTimeActivity.Minutes = Convert.ToInt32(TimeActivity.Minutes);
                            addTimeActivity.MinutesSpecified = true;
                        }
                    }
                    else if (TimeActivity.Hours.Contains("."))
                    {
                        string[] arry = new string[2];
                        string[] array_msg = TimeActivity.Hours.ToString().Split('.');
                        valOfHours = Convert.ToInt32(array_msg[0]);
                        valOfMinute = Convert.ToInt32(array_msg[1]);

                        decimal cal = Convert.ToDecimal(0.1);
                        if (valOfMinute > 0)
                        {
                            string min = array_msg[1];
                            while (min.Length > 2)
                            {
                                min = min.Remove(min.Length - 1);
                            }                                                     
                                                
                            decimal value = 0;
                            if (min.Length > 1)
                            {
                                valOfMinute = Convert.ToInt32(min);   
                                value = Convert.ToDecimal(valOfMinute) * cal;
                            }
                            else
                            {
                                valOfMinute = Convert.ToInt32(min);   
                                value = Convert.ToDecimal(valOfMinute);
                            }
                            value = value * 6;
                            if (value.ToString().Contains("."))
                            {
                                string[] array_val = value.ToString().Split('.');
                                int d = Convert.ToInt32(array_val[1]);
                                int d1 = Convert.ToInt32(array_val[0]);
                                if (d > 5)
                                {
                                    d1 = d1 + 1;
                                }
                                valOfMinute = d1;
                            }
                            else
                            {
                                valOfMinute = Convert.ToInt32(value);
                            }
                        }

                        TimeActivity.Hours = valOfHours.ToString();
                        TimeActivity.Minutes = valOfMinute.ToString();

                        if (Convert.ToInt32(TimeActivity.Hours) > 0)
                        {
                            addTimeActivity.Hours = Convert.ToInt32(TimeActivity.Hours);
                            addTimeActivity.HoursSpecified = true;
                        }
                        if (Convert.ToInt32(TimeActivity.Minutes) > 0)
                        {
                            addTimeActivity.Minutes = Convert.ToInt32(TimeActivity.Minutes);
                            addTimeActivity.MinutesSpecified = true;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(TimeActivity.Hours) > 0)
                        {
                            addTimeActivity.Hours = Convert.ToInt32(TimeActivity.Hours);
                            addTimeActivity.HoursSpecified = true;
                        }
                        if (Convert.ToInt32(TimeActivity.Minutes) > 0)
                        {
                            addTimeActivity.Minutes = Convert.ToInt32(TimeActivity.Minutes);
                            addTimeActivity.MinutesSpecified = true;
                        }
                    }
                }
                else if (TimeActivity.StartTime != null || TimeActivity.EndTime != null)
                {
                    if (TimeActivity.StartTime != null)
                    {
                        try
                        {
                            addTimeActivity.StartTime = Convert.ToDateTime(TimeActivity.StartTime);
                            addTimeActivity.StartTimeSpecified = true;
                        }
                        catch { }
                    }
                    if (TimeActivity.EndTime != null)
                    {
                        try
                        {
                            addTimeActivity.EndTime = Convert.ToDateTime(TimeActivity.EndTime);
                            addTimeActivity.EndTimeSpecified = true;
                        }
                        catch
                        { }
                    }
                }

                if (TimeActivity.BreakHours != null)
                {
                    int iii = 0;

                    string[] array_msg1 = TimeActivity.BreakHours.ToString().Split(':');
                    foreach (string s in array_msg1)
                    {

                        if (iii == 0)
                        {
                            TimeActivity.BreakHours = s;
                        }
                        else if (iii == 1)
                        {
                            TimeActivity.BreakMinutes = s;
                        }
                        iii++;
                    }
                    addTimeActivity.BreakHours = Convert.ToInt32(TimeActivity.BreakHours);
                    addTimeActivity.BreakHoursSpecified = true;

                    addTimeActivity.BreakMinutes = Convert.ToInt32(TimeActivity.BreakMinutes);
                    addTimeActivity.BreakMinutesSpecified = true;
                }               

                addTimeActivity.Description = TimeActivity.Description;
               
                #endregion
                TimeActivityAdded = new Intuit.Ipp.Data.TimeActivity();
                TimeActivityAdded = dataService.Add<Intuit.Ipp.Data.TimeActivity>(addTimeActivity);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (TimeActivityAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = TimeActivityAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = TimeActivityAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        /// <summary>
        /// updating Time Activity entry in quickbook online by the transaction Id
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="TimeActivityId"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOTimeActivityEntry(OnlineDataProcessingsImportClass.OnlineTimeActivityQBEntry coll, string TimeActivityId, string syncToken, Intuit.Ipp.Data.TimeActivity PreviousData = null)
        {           
            Intuit.Ipp.Data.TimeActivity TimeActivityAdded = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.TimeActivity addTimeActivity = new Intuit.Ipp.Data.TimeActivity();
            OnlineTimeActivityQBEntry TimeActivity = new OnlineTimeActivityQBEntry();
            TimeActivity = coll;
            try
            {
                #region Add Time Activity

                //594
                if (TimeActivity.isAppend == true)
                {
                    addTimeActivity = PreviousData;
                    addTimeActivity.sparse = true;
                    addTimeActivity.sparseSpecified = true;
                }

                addTimeActivity.Id = TimeActivityId;
                addTimeActivity.SyncToken = syncToken;

                if (TimeActivity.TxnDate != null)
                {
                    try
                    {
                        addTimeActivity.TxnDate = Convert.ToDateTime(TimeActivity.TxnDate);
                        addTimeActivity.TxnDateSpecified = true;
                    }
                    catch { }
                }

                #region Add Time Activity NameOf
                if (TimeActivity.NameOf == null)
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Employee;
                    addTimeActivity.NameOfSpecified = true;
                }
                else if (TimeActivity.NameOf == TimeActivityTypeEnum.Employee.ToString())
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Employee;
                    addTimeActivity.NameOfSpecified = true;
                }
                else if (TimeActivity.NameOf == TimeActivityTypeEnum.Vendor.ToString())
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Vendor;
                    addTimeActivity.NameOfSpecified = true;
                }
                else if (TimeActivity.NameOf == TimeActivityTypeEnum.Other.ToString())
                {
                    addTimeActivity.NameOf = TimeActivityTypeEnum.Other;
                    addTimeActivity.NameOfSpecified = true;
                }

                #endregion

                #region Get Employee or vendor id

                // find department 
                if (TimeActivity.EmployeeOrVendorRef.Count > 0)
                {
                    foreach (var i in TimeActivity.EmployeeOrVendorRef)
                    {
                        string displayname = string.Empty;
                        string id = string.Empty;
                        if (TimeActivity.NameOf != TimeActivityTypeEnum.Vendor.ToString())
                        {
                            displayname = i.Name;
                            if (displayname != string.Empty)
                            {
                                var dataEmployee = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(displayname.Trim());
                                foreach (var item in dataEmployee)
                                {
                                    id = item.Id;
                                }
                                addTimeActivity.AnyIntuitObject = new ReferenceType()
                                {
                                    name = displayname,
                                    Value = id
                                };
                            }

                        }
                        else if (TimeActivity.NameOf == TimeActivityTypeEnum.Vendor.ToString())
                        {
                            displayname = i.Name;
                            if (displayname != string.Empty)
                            {
                                var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(displayname.Trim());
                                foreach (var item in dataVendor)
                                {
                                    id = item.Id;
                                }
                                addTimeActivity.AnyIntuitObject = new ReferenceType()
                                {
                                    name = displayname,
                                    Value = id
                                };
                            }
                        }

                    }
                }
                #endregion

                #region Get Customer Id
                foreach (var customerRef in TimeActivity.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }

                        addTimeActivity.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }

                }


                #endregion

                #region  find department

                foreach (var i in TimeActivity.DepartmentRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataDepartment = await CommonUtilities.GetDepartmentExistsInOnlineQBO(i.Name.Trim());
                        foreach (var item in dataDepartment)
                        {
                            id = item.Id;
                        }
                        addTimeActivity.DepartmentRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }

                #endregion

                #region  find item id

                foreach (var i in TimeActivity.ItemRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addTimeActivity.ItemRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }

                #endregion

                #region  find Payrollitem id

                foreach (var i in TimeActivity.PayrollItemRef)
                {
                    string id = string.Empty;
                    try
                    {
                        if (i.Name != null)
                        {
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Constants.QuickbookGraphQlUrl);
                            request.Method = "POST";
                            request.Headers.Add("Authorization", "Bearer " + CommonUtilities.GetBearertoken());
                            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                            Byte[] byteArray = encoding.GetBytes(Constants.ReadEmployeeCompensation);
                            request.ContentLength = byteArray.Length;
                            request.ContentType = "application/json";
                            using (Stream dataStream = request.GetRequestStream())
                            {
                                dataStream.Write(byteArray, 0, byteArray.Length);
                            }
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            string responseText = string.Empty;
                            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                            {
                                responseText = reader.ReadToEnd();
                            }

                            var jsonResponse = JsonValue.Parse(responseText);
                            foreach (System.Json.JsonObject employeeEdge in jsonResponse["data"]["company"]["employeeContacts"]["edges"])
                            {
                                string employeeId = string.Empty;

                                JsonValue employeeNode = employeeEdge["node"]["externalIds"];
                                foreach (JsonObject compensationEdge in employeeEdge["node"]["externalIds"])
                                {
                                    employeeId = (string)compensationEdge["localId"];

                                }
                                if (employeeId != string.Empty && addTimeActivity.AnyIntuitObject != null && addTimeActivity.AnyIntuitObject.Value == employeeId)
                                {
                                    foreach (JsonObject compensationEdge in employeeNode["profiles"]["employee"]["compensations"]["edges"])
                                    {
                                        JsonValue compensationNode = compensationEdge["node"];
                                        var compensationId = (string)compensationNode["id"];
                                        var compensationName = (string)compensationNode["employerCompensation"]["name"];
                                        // var compensationAmount = compensationNode["amount"] == null ? 0 : double.Parse((string)compensationNode["amount"]);
                                        if (compensationName != null && compensationName == i.Name)
                                        {
                                            i.Value = compensationId;
                                            addTimeActivity.PayrollItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = i.Value
                                            };
                                            break;
                                        }
                                    }
                                }
                               
                                if (i.Value != null)
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                // End 718

                #endregion

                #region find class id

                foreach (var i in TimeActivity.ClassRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                        
                        foreach (var item in dataItem)
                        {
                            id = item.Id;
                        }
                        addTimeActivity.ClassRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }

                #endregion

                #region BillableStatus
                if (TimeActivity.BillableStatus != null)
                {
                    if (TimeActivity.BillableStatus == BillableStatusEnum.Billable.ToString())
                    {
                        addTimeActivity.BillableStatus = BillableStatusEnum.Billable;
                        addTimeActivity.BillableStatusSpecified = true;
                    }
                    else if (TimeActivity.BillableStatus == BillableStatusEnum.HasBeenBilled.ToString())
                    {
                        addTimeActivity.BillableStatus = BillableStatusEnum.HasBeenBilled;
                        addTimeActivity.BillableStatusSpecified = true;
                    }
                    else if (TimeActivity.BillableStatus == BillableStatusEnum.NotBillable.ToString())
                    {
                        addTimeActivity.BillableStatus = BillableStatusEnum.NotBillable;
                        addTimeActivity.BillableStatusSpecified = true;
                    }
                }

                #endregion

                if (TimeActivity.Taxable != null)
                {
                    addTimeActivity.Taxable = Convert.ToBoolean(TimeActivity.Taxable);
                    addTimeActivity.TaxableSpecified = true;
                }
                if (TimeActivity.HourlyRate != null)
                {
                    addTimeActivity.HourlyRate = Convert.ToDecimal(TimeActivity.HourlyRate);
                    addTimeActivity.HourlyRateSpecified = true;
                }
                if (TimeActivity.Hours != null)
                {
                    int valOfHours = 0, valOfMinute = 0;

                    if (TimeActivity.Hours.Contains(":"))
                    {
                        string[] arry = new string[2];
                        string[] array_msg = TimeActivity.Hours.ToString().Split(':');
                        valOfHours = Convert.ToInt32(array_msg[0]);
                        valOfMinute = Convert.ToInt32(array_msg[1]);
                        string min = valOfMinute.ToString();
                        while (min.Length > 2)
                        {
                            min = min.Remove(min.Length - 1);
                        }
                        valOfMinute = Convert.ToInt32(min);
                        if (valOfMinute > 59)
                        {
                            valOfMinute = valOfMinute - 60;
                            valOfHours++;
                        }

                        TimeActivity.Hours = valOfHours.ToString();
                        TimeActivity.Minutes = valOfMinute.ToString();

                        if (Convert.ToInt32(TimeActivity.Hours) > 0)
                        {
                            addTimeActivity.Hours = Convert.ToInt32(TimeActivity.Hours);
                            addTimeActivity.HoursSpecified = true;
                        }
                        if (Convert.ToInt32(TimeActivity.Minutes) > 0)
                        {
                            addTimeActivity.Minutes = Convert.ToInt32(TimeActivity.Minutes);
                            addTimeActivity.MinutesSpecified = true;
                        }
                    }
                    else if (TimeActivity.Hours.Contains("."))
                    {
                        string[] arry = new string[2];
                        string[] array_msg = TimeActivity.Hours.ToString().Split('.');
                        valOfHours = Convert.ToInt32(array_msg[0]);
                        valOfMinute = Convert.ToInt32(array_msg[1]);

                        decimal cal = Convert.ToDecimal(0.1);
                        if (valOfMinute > 0)
                        {
                            string min = array_msg[1];
                            while (min.Length > 2)
                            {
                                min = min.Remove(min.Length - 1);
                            }

                            decimal value = 0;
                            if (min.Length > 1)
                            {
                                valOfMinute = Convert.ToInt32(min);
                                value = Convert.ToDecimal(valOfMinute) * cal;
                            }
                            else
                            {
                                valOfMinute = Convert.ToInt32(min);
                                value = Convert.ToDecimal(valOfMinute);
                            }
                            value = value * 6;
                            if (value.ToString().Contains("."))
                            {
                                string[] array_val = value.ToString().Split('.');
                                int d = Convert.ToInt32(array_val[1]);
                                int d1 = Convert.ToInt32(array_val[0]);
                                if (d > 5)
                                {
                                    d1 = d1 + 1;
                                }
                                valOfMinute = d1;
                            }
                            else
                            {
                                valOfMinute = Convert.ToInt32(value);
                            }
                        }

                        TimeActivity.Hours = valOfHours.ToString();
                        TimeActivity.Minutes = valOfMinute.ToString();

                        if (Convert.ToInt32(TimeActivity.Hours) > 0)
                        {
                            addTimeActivity.Hours = Convert.ToInt32(TimeActivity.Hours);
                            addTimeActivity.HoursSpecified = true;
                        }
                        if (Convert.ToInt32(TimeActivity.Minutes) > 0)
                        {
                            addTimeActivity.Minutes = Convert.ToInt32(TimeActivity.Minutes);
                            addTimeActivity.MinutesSpecified = true;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(TimeActivity.Hours) > 0)
                        {
                            addTimeActivity.Hours = Convert.ToInt32(TimeActivity.Hours);
                            addTimeActivity.HoursSpecified = true;
                        }
                        if (Convert.ToInt32(TimeActivity.Minutes) > 0)
                        {
                            addTimeActivity.Minutes = Convert.ToInt32(TimeActivity.Minutes);
                            addTimeActivity.MinutesSpecified = true;
                        }
                    }
                }
                else if (TimeActivity.StartTime != null || TimeActivity.EndTime != null)
                {
                    if (TimeActivity.StartTime != null)
                    {
                        try
                        {
                            addTimeActivity.StartTime = Convert.ToDateTime(TimeActivity.StartTime);
                            addTimeActivity.StartTimeSpecified = true;
                        }
                        catch { }
                    }
                    if (TimeActivity.EndTime != null)
                    {
                        try
                        {

                            addTimeActivity.EndTime = Convert.ToDateTime(TimeActivity.EndTime);
                            addTimeActivity.EndTimeSpecified = true;
                        }
                        catch { }
                    }
                }

                if (TimeActivity.BreakHours != null)
                {
                    int iii = 0;

                    string[] array_msg1 = TimeActivity.BreakHours.ToString().Split(':');
                    foreach (string s in array_msg1)
                    {

                        if (iii == 0)
                        {
                            TimeActivity.BreakHours = s;
                        }
                        else if (iii == 1)
                        {
                            TimeActivity.BreakMinutes = s;
                        }
                        iii++;
                    }
                    addTimeActivity.BreakHours = Convert.ToInt32(TimeActivity.BreakHours);
                    addTimeActivity.BreakHoursSpecified = true;

                    addTimeActivity.BreakMinutes = Convert.ToInt32(TimeActivity.BreakMinutes);
                    addTimeActivity.BreakMinutesSpecified = true;
                }              

                addTimeActivity.Description = TimeActivity.Description;

                #endregion
                TimeActivityAdded = new Intuit.Ipp.Data.TimeActivity();
                TimeActivityAdded = dataService.Update<Intuit.Ipp.Data.TimeActivity>(addTimeActivity);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (TimeActivityAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = TimeActivityAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = TimeActivityAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }


        #endregion
    }

  public class OnlineTimeActivityQBEntryCollection : Collection<OnlineTimeActivityQBEntry>
  {

  }
}
