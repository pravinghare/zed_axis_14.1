﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;
//using DataProcessingBlocks;



namespace OnlineDataProcessingsImportClass
{
    public class OnlineDepositeQBEntry
    {
        #region Member
        private string m_DocNumber;
        private string m_TxnDate;
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        private string m_PrivateNote;
        private string m_TxnStatus;
        private Collection<OnlineEntities.AttachableRef> m_AttachableRef = new Collection<OnlineEntities.AttachableRef>();
        private Collection<OnlineEntities.CustomField> m_CustomField = new Collection<OnlineEntities.CustomField>();
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.Entity> m_Entity = new Collection<OnlineEntities.Entity>();
        private Collection<OnlineEntities.DepositLineDetails> m_DepositLineDetails = new Collection<OnlineEntities.DepositLineDetails>();
        private Collection<OnlineEntities.DepositToAccountRef> m_DepositToAccountRef = new Collection<DepositToAccountRef>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.CashBack> m_CashBack = new Collection<OnlineEntities.CashBack>();
        private string m_TxnSource;
        private string m_TotalAmt;
        //bug 476 
        private string m_ExchaneRate;
        private string m_GlobalTaxCalculation;

        //594
        private bool m_isAppend;


        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public Collection<DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }

        public Collection<CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }


        public string TxnStatus
        {
            get { return m_TxnStatus; }
            set { m_TxnStatus = value; }
        }
        public Collection<OnlineEntities.AttachableRef> AttachableRef
        {
            get { return m_AttachableRef; }
            set { m_AttachableRef = value; }
        }
        public Collection<OnlineEntities.CustomField> CustomField
        {
            get { return m_CustomField; }
            set { m_CustomField = value; }
        }

        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.Entity> Entity
        {
            get { return m_Entity; }
            set { m_Entity = value; }
        }
      
        public Collection<OnlineEntities.DepositToAccountRef> DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
           set { m_DepositToAccountRef = value; }
        }

        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }

        public Collection<OnlineEntities.CashBack> CashBack
        {
            get { return m_CashBack; }
            set { m_CashBack = value; }
        }

        public string TxnSource
        {
            get { return m_TxnSource; }
            set { m_TxnSource = value; }
        }
        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }

  //bug 476 Axis 12.0
        public string ExchangeRate
        {
            get { return m_ExchaneRate; }
            set { m_ExchaneRate= value; }
        }

        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation= value; }
        }
        //public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region public method


        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        /// <summary>
        /// Creating new Deposit transaction in quickbook online.
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBODeposit(OnlineDataProcessingsImportClass.OnlineDepositeQBEntry coll)
        {

            DataService dataService = null;
            Intuit.Ipp.Data.Deposit DepositAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.Deposit addDeposit = new Intuit.Ipp.Data.Deposit();
            OnlineDepositeQBEntry Deposit = new OnlineDepositeQBEntry();
            Deposit = coll;
            Intuit.Ipp.Data.DepositLineDetail lineDepositLineDetail = new Intuit.Ipp.Data.DepositLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();


            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            int linecountN = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            Intuit.Ipp.Data.CustomField[] Line_onlines = new Intuit.Ipp.Data.CustomField[linecountN];
            Intuit.Ipp.Data.LinkedTxn LinkedTxn = new Intuit.Ipp.Data.LinkedTxn();
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            try
            {
                #region Add Deposit
               
                #region Add DocNo
                if (Deposit.DocNumber != null)
                {
                    addDeposit.DocNumber = Deposit.DocNumber;
                }
                #endregion


                #region Add TxnDate
                if (Deposit.TxnDate != null)
                {
                    try
                    {
                        addDeposit.TxnDate = Convert.ToDateTime(Deposit.TxnDate);
                        addDeposit.TxnDateSpecified = true;
                    }
                    catch { }
                }
                #endregion


                #region find department
                foreach (var i in Deposit.DepartmentRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataDepartment = await CommonUtilities.GetDepartmentExistsInOnlineQBO(i.Name.Trim());
                       
                        foreach (var item in dataDepartment)
                        {
                            id = item.Id;
                        }
                        addDeposit.DepartmentRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }
                #endregion
                #region  find Currency
                if (Deposit.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var DepstData in Deposit.CurrencyRef)
                    {
                        CurrencyRefName = DepstData.Name;
                      
                    }
                    addDeposit.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }


                #endregion
                #region privateNote
                if (Deposit.PrivateNote != null)
                {
                    addDeposit.PrivateNote = Deposit.PrivateNote;
                }

                if (Deposit.TxnStatus != null)
                {
                    addDeposit.TxnStatus = Deposit.TxnStatus;
                }
                #endregion

               
                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in Deposit.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addDeposit.CustomField = custom_online;
                #endregion

                #region Line

                Intuit.Ipp.Data.CustomField LinecustFiled = new Intuit.Ipp.Data.CustomField();
                
                foreach (var l in Deposit.Line)
                {
                    if (flag == false)
                    {
                         #region

                        flag = true;
                        if (l.LinkedTxn.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                            Line.AmountSpecified = true;

                            #region SalesReceipt
                            foreach (var i in l.LinkedTxn)
                            {
                                if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                {
                                    string id = string.Empty;
                                    if (i.ID != null)
                                    {
                                        var dataSalesReceipt = await CommonUtilities.GetSalesReceiptDetailsExistsInOnlineQBO(i.ID.Trim());

                                        Intuit.Ipp.Data.SalesReceipt SalesReceipt = new Intuit.Ipp.Data.SalesReceipt();

                                        if (dataSalesReceipt.Count > 0)
                                        {
                                            foreach (var data in dataSalesReceipt)
                                            {
                                                LinkedTxn.TxnId = data.Id;
                                                LinkedTxn.TxnType = i.Type;
                                                LinkedTxn.TxnLineId = "0";
                                            }
                                        }
                                    }

                                }
                                if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                {
                                    string id = string.Empty;
                                    if (i.ID != null)
                                    {
                                        var dataInvoice = await CommonUtilities.GetInvoiceDetailsExistsInOnlineQBO(i.ID.Trim());

                                        if (dataInvoice.Count > 0)
                                        {
                                            foreach (var data in dataInvoice)
                                            {
                                                LinkedTxn.TxnId = data.Id;
                                                LinkedTxn.TxnType = i.Type;
                                                LinkedTxn.TxnLineId = "0";
                                            }
                                        }
                                    }
                                }
                                Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { LinkedTxn };
                                lines_online[linecount - 1] = Line;
                            }
                            #endregion

                        }
                        if (l.LineCustomField.Count > 0)
                        {
                            #region Line Custom Field

                            foreach (var customData in l.LineCustomField)
                            {
                                LinecustFiled.DefinitionId = linecountN.ToString();
                                LinecustFiled.Name = customData.Name;
                                LinecustFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                                LinecustFiled.AnyIntuitObject = customData.Value;
                                Array.Resize(ref Line_onlines, linecountN);
                                Line_onlines[linecountN - 1] = LinecustFiled;
                                linecountN++;
                                LinecustFiled = new Intuit.Ipp.Data.CustomField();
                            }
                            Line.CustomField = Line_onlines;

                            #endregion
                        }
                        if (l.DepositLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            if (l.LineAmount != null)
                            {

                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;

                            }
                            #region Deposit Line Details
                            foreach (var s in l.DepositLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DepositLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (s.TaxCodeRef != null)
                                {
                                    TaxCode TaxCode = new TaxCode();
                                    TaxCode.Name = s.TaxCodeRef;
                                    string Taxvalue = string.Empty;
                                    string TaxName = string.Empty;
                                    TaxName = TaxCode.Name;
                                    if (TaxCode.Name != null)
                                    {
                                        var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                        
                                        if (dataTax.Count != 0)
                                        {
                                            foreach (var tax in dataTax)
                                            {
                                                Taxvalue = tax.Id;
                                            }

                                            lineDepositLineDetail.TaxCodeRef = new ReferenceType()
                                            {
                                                name = TaxName,
                                                Value = Taxvalue
                                            };
                                        }
                                    }

                                }

                                if (s.TaxApplicableOn != null)
                                {
                                    if (s.TaxApplicableOn.ToLower() == "sales")
                                    {
                                       
                                        lineDepositLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Sales;
                                        lineDepositLineDetail.TaxApplicableOnSpecified = true;
                                    }
                                    if (s.TaxApplicableOn.ToLower() == "purchase")
                                    {
                                      
                                        lineDepositLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Purchase;
                                        lineDepositLineDetail.TaxApplicableOnSpecified = true;
                                    }      

                                }


                                #region Entity Ref
                                foreach (var i in s.EntityRef)
                                {

                                    if (i.Type == EntityTypeEnum.Customer.ToString() || i.Type == null)
                                    {
                                        string id = string.Empty;
                                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());

                                        foreach (var item in dataCustomer)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.Entity = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id,
                                            type = EntityTypeEnum.Customer.ToString()
                                        };
                                    }
                                    else if (i.Type == EntityTypeEnum.Vendor.ToString())
                                    {
                                        string id = string.Empty;
                                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(i.Name.Trim());
                                        
                                        foreach (var item in dataVendor)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.Entity = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id,
                                            type = EntityTypeEnum.Vendor.ToString()
                                        };
                                    }
                                    else if (i.Type == EntityTypeEnum.Employee.ToString())
                                    {
                                        string id = string.Empty;
                                        var dataEmployee = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(i.Name.Trim());
                                       
                                        foreach (var item in dataEmployee)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.Entity = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id,
                                            type = EntityTypeEnum.Employee.ToString()
                                        };
                                    }

                                }
                                #endregion
                                #region ClassRef
                                foreach (var i in s.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());

                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region AccountRef
                                foreach (var i in s.AccountRef)
                                {
                                    if (i.Name != null && i.Name != string.Empty && i.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(i.Name);
                                        lineDepositLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                #endregion
                                #region PaymentMethodRef
                                foreach (var i in s.PaymentMethodRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(i.Name);
                                        
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.PaymentMethodRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                if (s.CheckNum != null)
                                {
                                    lineDepositLineDetail.CheckNum = s.CheckNum;
                                    lineDepositLineDetail.TxnTypeSpecified = true;
                                }
                                #region LineDepositeTxnType
                                if (s.TxnType != null)
                                {
                                    if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.APCreditCard.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.APCreditCard;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Bill.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Bill;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CarryOver.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CarryOver;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CashPurchase.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CashPurchase;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Charge.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Charge;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Check.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Check;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CreditMemo.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CreditMemo;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Deposit.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Deposit;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTRefund.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTRefund;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Estimate.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Estimate;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Invoice;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.JournalEntry.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.JournalEntry;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Paycheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Paycheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PriorPayment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PriorPayment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.RefundCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.RefundCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesOrder.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesOrder;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.TimeActivity.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.TimeActivity;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Transfer.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Transfer;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.VendorCredit.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.VendorCredit;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = lineDepositLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                            #endregion
                        }

                        addDeposit.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        try
                        {

                            Line = new Intuit.Ipp.Data.Line();
                            LinkedTxn = new Intuit.Ipp.Data.LinkedTxn();
                            lineDepositLineDetail = new Intuit.Ipp.Data.DepositLineDetail();
                            linecount++;

                            if (l.LinkedTxn.Count > 0)
                            {
                                Line.Description = l.LineDescription;
                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;
                                #region SalesReceipt
                                foreach (var i in l.LinkedTxn)
                                {
                                    if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                    {
                                        string id = string.Empty;
                                        if (i.ID != null)
                                        {
                                            var dataSalesReceipt = await CommonUtilities.GetSalesReceiptDetailsExistsInOnlineQBO(i.ID.Trim());

                                            Intuit.Ipp.Data.SalesReceipt SalesReceipt = new Intuit.Ipp.Data.SalesReceipt();

                                            if (dataSalesReceipt.Count > 0)
                                            {
                                                foreach (var data in dataSalesReceipt)
                                                {
                                                    LinkedTxn.TxnId = data.Id;
                                                    LinkedTxn.TxnType = i.Type;
                                                    LinkedTxn.TxnLineId = "0";
                                                }
                                            }
                                        }

                                    }
                                    if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                    {
                                        string id = string.Empty;
                                        if (i.ID != null)
                                        {
                                            var dataInvoice = await CommonUtilities.GetInvoiceDetailsExistsInOnlineQBO(i.ID.Trim());
                                          
                                            if (dataInvoice.Count > 0)
                                            {
                                                foreach (var data in dataInvoice)
                                                {
                                                    LinkedTxn.TxnId = data.Id;
                                                    LinkedTxn.TxnType = i.Type;
                                                    LinkedTxn.TxnLineId = "0";
                                                }
                                            }
                                        }
                                    }

                                    Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { LinkedTxn };
                                    Array.Resize(ref lines_online, linecount);
                                    lines_online[linecount - 1] = Line;
                                }
                                #endregion

                            }
                            if (l.LineCustomField.Count > 0)
                            {
                                #region Line Custom Field

                                foreach (var customData in l.LineCustomField)
                                {
                                    LinecustFiled.DefinitionId = linecountN.ToString();
                                    LinecustFiled.Name = customData.Name;
                                    LinecustFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                                    LinecustFiled.AnyIntuitObject = customData.Value;
                                    Array.Resize(ref Line_onlines, linecountN);
                                    Line_onlines[linecountN - 1] = LinecustFiled;
                                    linecountN++;
                                    LinecustFiled = new Intuit.Ipp.Data.CustomField();
                                }
                                Line.CustomField = Line_onlines;
                                #endregion
                            }
                            #region
                            if (l.DepositLineDetail.Count > 0)
                            {
                                Line.Description = l.LineDescription;
                                if (l.LineAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(l.LineAmount);
                                    Line.AmountSpecified = true;

                                }
                                #region Deposit Line Details
                                foreach (var s in l.DepositLineDetail)
                                {
                                    if (s != null)
                                    {
                                        Line.DetailType = LineDetailTypeEnum.DepositLineDetail;
                                        Line.DetailTypeSpecified = true;
                                    }
                                    #region Taxcoderef
                                    if (s.TaxCodeRef != null)
                                    {
                                        TaxCode TaxCode = new TaxCode();
                                        TaxCode.Name = s.TaxCodeRef;
                                        string Taxvalue = string.Empty;
                                        string TaxName = string.Empty;
                                        TaxName = TaxCode.Name;
                                        if (TaxCode.Name != null)
                                        {
                                            var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                           
                                            if (dataTax != null)
                                            {
                                                foreach (var tax in dataTax)
                                                {
                                                    Taxvalue = tax.Id;
                                                }

                                                lineDepositLineDetail.TaxCodeRef = new ReferenceType()
                                                {
                                                    name = TaxName,
                                                    Value = Taxvalue
                                                };
                                            }
                                        }

                                    }

                                    if (s.TaxApplicableOn != null)
                                    {
                                        if (s.TaxApplicableOn == TaxApplicableOnEnum.Sales.ToString())
                                        {
                                           
                                            lineDepositLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Sales;
                                            lineDepositLineDetail.TaxApplicableOnSpecified = true;
                                        }

                                        if (s.TaxApplicableOn == TaxApplicableOnEnum.Purchase.ToString())
                                        {
                                           
                                            lineDepositLineDetail.TaxApplicableOn = TaxApplicableOnEnum.Purchase;
                                            lineDepositLineDetail.TaxApplicableOnSpecified = true;
                                        }

                                    }

                                    #endregion

                                    #region Entity Ref
                                    foreach (var i in s.EntityRef)
                                    {

                                        if (i.Type == EntityTypeEnum.Customer.ToString() || i.Type == null)
                                        {

                                            string id = string.Empty;
                                            string Name = string.Empty;
                                            var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataCustomer)
                                            {
                                                id = item.Id;
                                            }
                                            lineDepositLineDetail.Entity = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id,
                                                type = EntityTypeEnum.Customer.ToString()
                                            };
                                        }
                                        else if (i.Type == EntityTypeEnum.Vendor.ToString())
                                        {

                                            string id = string.Empty;
                                            string Name = string.Empty;
                                            var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataVendor)
                                            {
                                                id = item.Id;
                                            }
                                            lineDepositLineDetail.Entity = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id,
                                                type = EntityTypeEnum.Vendor.ToString()
                                            };
                                        }
                                        else if (i.Type == EntityTypeEnum.Employee.ToString())
                                        {
                                            string id = string.Empty;
                                            string Name = string.Empty;
                                            var dataEmployee = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataEmployee)
                                            {
                                                id = item.Id;
                                            }

                                            lineDepositLineDetail.Entity = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id,
                                                type = EntityTypeEnum.Employee.ToString()
                                            };
                                        }

                                    }
                                    #endregion
                                    #region ClassRef
                                    foreach (var i in s.ClassRef)
                                    {
                                        if (i.Name != null)
                                        {
                                            string id = string.Empty;
                                            var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineDepositLineDetail.ClassRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    #endregion
                                    #region AccountRef
                                    foreach (var i in s.AccountRef)
                                    {
                                        if (i.Name != null && i.Name != string.Empty && i.Name != "")
                                        {
                                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(i.Name);
                                            lineDepositLineDetail.AccountRef = new ReferenceType()
                                            {
                                                name = accDetails.name,
                                                Value = accDetails.Value
                                            };
                                        }
                                       
                                    }
                                    #endregion

                                    #region PaymentMethodRef
                                    foreach (var i in s.PaymentMethodRef)
                                    {
                                        if (i.Name != null)
                                        {
                                            string id = string.Empty;
                                            var dataItem = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(i.Name);

                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineDepositLineDetail.PaymentMethodRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    #endregion
                                    if (s.CheckNum != null)
                                    {
                                        lineDepositLineDetail.CheckNum = s.CheckNum;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    #region LineDepositeTxnType
                                    if (s.TxnType != null)
                                    {
                                        if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.APCreditCard.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.APCreditCard;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Bill.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Bill;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CarryOver.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CarryOver;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CashPurchase.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CashPurchase;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Charge.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Charge;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Check.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Check;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CreditMemo.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CreditMemo;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Deposit.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Deposit;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTRefund.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTRefund;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Estimate.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Estimate;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Invoice;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.JournalEntry.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.JournalEntry;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Paycheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Paycheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PriorPayment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PriorPayment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.RefundCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.RefundCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesOrder.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesOrder;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.TimeActivity.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.TimeActivity;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Transfer.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Transfer;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.VendorCredit.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.VendorCredit;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                    }
                                    #endregion

                                    Line.AnyIntuitObject = lineDepositLineDetail;
                                    Array.Resize(ref lines_online, linecount);
                                    lines_online[linecount - 1] = Line;
                                }
                                #endregion
                            }
                            addDeposit.Line = AddLine(lines_online, Line, linecount);
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());

                        }
                    }

                }
                #endregion

                #region Deposit to Account
                foreach (var accounts in Deposit.DepositToAccountRef)
                {
                    if (accounts.Name != null && accounts.Name != string.Empty && accounts.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accounts.Name);
                        addDeposit.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                   
                }
                #endregion

                #region Taxcoderef
                foreach (var txntaxCode in Deposit.TxnTaxDetail)
                {
                    #region TxnTaxDetail
                    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    if (txntaxCode.TxnTaxCodeRef != null)
                    {
                        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                        {
                            TaxCode TaxCode = new TaxCode();
                            TaxCode.Name = taxRef.Name;
                            string Taxvalue = string.Empty;
                            string TaxName = string.Empty;
                            TaxName = TaxCode.Name;
                            if (TaxCode.Name != null)
                            {
                                var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                if (dataTax != null)
                                {
                                    foreach (var tax in dataTax)
                                    {
                                        Taxvalue = tax.Id;
                                    }

                                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                    {
                                        name = TaxName,
                                        Value = Taxvalue
                                    };
                                }
                            }
                        }
                    }

                    if (txnTaxDetail != null)
                    {
                        addDeposit.TxnTaxDetail = txnTaxDetail;
                    }
                    #endregion
                }

                #endregion

                #region cashback
                Intuit.Ipp.Data.CashBackInfo CashBack = new Intuit.Ipp.Data.CashBackInfo();
                foreach (var c in Deposit.CashBack)
                {
                    if (c.AccountRef.Count > 0)
                    {
                        foreach (var i in c.AccountRef)
                        {
                            if (i.Name != null && i.Name != string.Empty && i.Name != "")
                            {
                                ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(i.Name);
                                CashBack.AccountRef = new ReferenceType()
                                {
                                    name = accDetails.name,
                                    Value = accDetails.Value
                                };
                            }
                           
                        }
                        
                      
                    }


                    if (c.Amount != null)
                    {
                        CashBack.Amount = Convert.ToDecimal(c.Amount);
                        CashBack.AmountSpecified = true;
                    }
                    if (c.Memo != null)
                    {
                        CashBack.Memo = c.Memo;
                    }
                    if (CashBack.Amount != 0 && CashBack.AccountRef != null)
                    {
                        addDeposit.CashBack = CashBack;
                    }
                }


                #endregion


                #region txnSource
                if (Deposit.TxnSource != null)
                {
                    addDeposit.TxnSource = Deposit.TxnSource;
                   // addDeposit.TxnSource = true;
                }
                #endregion
                #region TotalAmt
                if (Deposit.TotalAmt != null)
                {
                    addDeposit.TotalAmt = Convert.ToDecimal(Deposit.TotalAmt);
                    addDeposit.TotalAmtSpecified = true;
                }
                #endregion

                //bug 476 axis 12.0
                #region ExchangeRate
                if (Deposit.ExchangeRate != null)
                {
                    addDeposit.ExchangeRate = Convert.ToDecimal(Deposit.ExchangeRate);
                    addDeposit.ExchangeRateSpecified= true;
                }
                #endregion

                #region GlobalTaxCalculation

                if (Deposit.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (Deposit.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addDeposit.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addDeposit.GlobalTaxCalculationSpecified = true;
                        }
                        else if (Deposit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addDeposit.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addDeposit.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addDeposit.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addDeposit.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion              

                #endregion
                DepositAdded = new Intuit.Ipp.Data.Deposit();
                DepositAdded = dataService.Add<Intuit.Ipp.Data.Deposit>(addDeposit);

            }

            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (DepositAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = DepositAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = DepositAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }
        /// <summary>
        /// Updating Existing Deposit transaction in quickbook online.
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBODeposit(OnlineDataProcessingsImportClass.OnlineDepositeQBEntry coll, string DepositId, string syncToken, Intuit.Ipp.Data.Deposit PreviousData = null)
        {

            DataService dataService = null;
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.Deposit DepositAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            Intuit.Ipp.Data.Deposit addDeposit = new Intuit.Ipp.Data.Deposit();
            OnlineDepositeQBEntry Deposit = new OnlineDepositeQBEntry();
            Deposit = coll;
            Intuit.Ipp.Data.DepositLineDetail lineDepositLineDetail = new Intuit.Ipp.Data.DepositLineDetail();
            // Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();

            // Intuit.Ipp.Data.cas Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            int linecountN = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            Intuit.Ipp.Data.CustomField[] Line_onlines = new Intuit.Ipp.Data.CustomField[linecountN];
            Intuit.Ipp.Data.LinkedTxn LinkedTxn = new Intuit.Ipp.Data.LinkedTxn();
            try
            {
               
                #region Add Deposit

             //594 PreviousData is used to maintain previous records and append new data if changes are applied ,Lines will be appended to next line directly
                if (Deposit.isAppend == true)
                {
                    addDeposit = PreviousData;
                    if (addDeposit.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addDeposit.Line.Length;
                        lines_online = addDeposit.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                    addDeposit.sparse = true;
                    addDeposit.sparseSpecified = true;

                }

                addDeposit.Id = DepositId;
                addDeposit.SyncToken = syncToken;
                #region Add DocNo
                if (Deposit.DocNumber != null)
                {
                    addDeposit.DocNumber = Deposit.DocNumber;
                }
                #endregion

                #region Add TxnDate
                if (Deposit.TxnDate != null)
                {
                    try
                    {
                        addDeposit.TxnDate = Convert.ToDateTime(Deposit.TxnDate);
                        addDeposit.TxnDateSpecified = true;
                    }
                    catch { }
                }
                #endregion

                #region find department
                foreach (var i in Deposit.DepartmentRef)
                {
                    string id = string.Empty;
                    if (i.Name != null)
                    {
                        var dataDepartment = await CommonUtilities.GetDepartmentExistsInOnlineQBO(i.Name.Trim());
                        
                        foreach (var item in dataDepartment)
                        {
                            id = item.Id;
                        }
                        addDeposit.DepartmentRef = new ReferenceType()
                        {
                            name = i.Name,
                            Value = id
                        };
                    }
                }
                #endregion
                #region  find Currency
                if (Deposit.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var DepstData in Deposit.CurrencyRef)
                    {
                        CurrencyRefName = DepstData.Name;

                    }
                    addDeposit.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }


                #endregion

                #region privateNote
                if (Deposit.PrivateNote != null)
                {
                    addDeposit.PrivateNote = Deposit.PrivateNote;
                }

                if (Deposit.TxnStatus != null)
                {
                    addDeposit.TxnStatus = Deposit.TxnStatus;
                }
                #endregion

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in Deposit.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addDeposit.CustomField = custom_online;
                #endregion

                #region Line

                Intuit.Ipp.Data.CustomField LinecustFiled = new Intuit.Ipp.Data.CustomField();

                foreach (var l in Deposit.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.LinkedTxn.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                            Line.AmountSpecified = true;

                            #region SalesReceipt
                            foreach (var i in l.LinkedTxn)
                            {
                                if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                {
                                    string id = string.Empty;
                                    if (i.ID != null)
                                    {
                                        var dataSalesReceipt = await CommonUtilities.GetSalesReceiptDetailsExistsInOnlineQBO(i.ID.Trim());

                                        Intuit.Ipp.Data.SalesReceipt SalesReceipt = new Intuit.Ipp.Data.SalesReceipt();

                                        if (dataSalesReceipt.Count > 0)
                                        {
                                            foreach (var data in dataSalesReceipt)
                                            {
                                                LinkedTxn.TxnId = data.Id;
                                                LinkedTxn.TxnType = i.Type;
                                                LinkedTxn.TxnLineId = "0";
                                            }
                                        }
                                    }

                                }
                                if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                {
                                    string id = string.Empty;
                                    if (i.ID != null)
                                    {
                                        var dataInvoice = await CommonUtilities.GetInvoiceDetailsExistsInOnlineQBO(i.ID.Trim());
                                       
                                        if (dataInvoice.Count > 0)
                                        {
                                            foreach (var data in dataInvoice)
                                            {
                                                LinkedTxn.TxnId = data.Id;
                                                LinkedTxn.TxnType = i.Type;
                                                LinkedTxn.TxnLineId = "0";
                                            }
                                        }
                                    }
                                }
                                Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { LinkedTxn };
                                lines_online[linecount - 1] = Line;
                            }
                            #endregion

                        }
                        if (l.LineCustomField.Count > 0)
                        {
                            #region Line Custom Field

                            foreach (var customData in l.LineCustomField)
                            {
                                LinecustFiled.DefinitionId = linecountN.ToString();
                                LinecustFiled.Name = customData.Name;
                                LinecustFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                                LinecustFiled.AnyIntuitObject = customData.Value;
                                Array.Resize(ref Line_onlines, linecountN);
                                Line_onlines[linecountN - 1] = LinecustFiled;
                                linecountN++;
                                LinecustFiled = new Intuit.Ipp.Data.CustomField();
                            }
                            Line.CustomField = Line_onlines;

                            #endregion
                        }
                        if (l.DepositLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            if (l.LineAmount != null)
                            {

                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;

                            }
                            #region Deposit Line Details
                            foreach (var s in l.DepositLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DepositLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                #region Entity Ref
                                foreach (var i in s.EntityRef)
                                {
                                    if (i.Type == EntityTypeEnum.Customer.ToString() || i.Type == null)
                                    {
                                        string id = string.Empty;
                                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataCustomer)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.Entity = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id,
                                            type = EntityTypeEnum.Customer.ToString()
                                        };
                                    }
                                    else if (i.Type == EntityTypeEnum.Vendor.ToString())
                                    {
                                        string id = string.Empty;
                                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(i.Name.Trim());
                                       
                                        foreach (var item in dataVendor)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.Entity = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id,
                                            type = EntityTypeEnum.Vendor.ToString()
                                        };
                                    }
                                    else if (i.Type == EntityTypeEnum.Employee.ToString())
                                    {
                                        string id = string.Empty;
                                        var dataEmployee = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataEmployee)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.Entity = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id,
                                            type = EntityTypeEnum.Employee.ToString()
                                        };
                                    }
                                }
                                #endregion
                                #region ClassRef
                                foreach (var i in s.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());

                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                #region AccountRef
                                foreach (var i in s.AccountRef)
                                {
                                    if (i.Name != null && i.Name != string.Empty && i.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(i.Name);
                                        lineDepositLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                #endregion
                                #region PaymentMethodRef
                                foreach (var i in s.PaymentMethodRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(i.Name);
                                        
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineDepositLineDetail.PaymentMethodRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #endregion
                                if (s.CheckNum != null)
                                {
                                    lineDepositLineDetail.CheckNum = s.CheckNum;
                                    lineDepositLineDetail.TxnTypeSpecified = true;
                                }
                                #region LineDepositeTxnType
                                if (s.TxnType != null)
                                {
                                    if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.APCreditCard.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.APCreditCard;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Bill.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Bill;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CarryOver.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CarryOver;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CashPurchase.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CashPurchase;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Charge.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Charge;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Check.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Check;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CreditMemo.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CreditMemo;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Deposit.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Deposit;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTRefund.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTRefund;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Estimate.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Estimate;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Invoice;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.JournalEntry.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.JournalEntry;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Paycheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Paycheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PriorPayment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PriorPayment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.RefundCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.RefundCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesOrder.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesOrder;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.TimeActivity.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.TimeActivity;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Transfer.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Transfer;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.VendorCredit.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.VendorCredit;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment.ToString())
                                    {
                                        lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = lineDepositLineDetail;
                                lines_online[linecount - 1] = Line;
                            }
                            #endregion
                        }

                        addDeposit.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        try
                        {

                            Line = new Intuit.Ipp.Data.Line();
                            LinkedTxn = new Intuit.Ipp.Data.LinkedTxn();
                            lineDepositLineDetail = new Intuit.Ipp.Data.DepositLineDetail();
                            linecount++;

                            if (l.LinkedTxn.Count > 0)
                            {
                                Line.Description = l.LineDescription;
                                Line.Amount = Convert.ToDecimal(l.LineAmount);
                                Line.AmountSpecified = true;
                                #region SalesReceipt
                                foreach (var i in l.LinkedTxn)
                                {
                                    if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                    {
                                        string id = string.Empty;
                                        if (i.ID != null)
                                        {
                                            var dataSalesReceipt = await CommonUtilities.GetSalesReceiptDetailsExistsInOnlineQBO(i.ID.Trim());

                                            Intuit.Ipp.Data.SalesReceipt SalesReceipt = new Intuit.Ipp.Data.SalesReceipt();

                                            if (dataSalesReceipt.Count > 0)
                                            {
                                                foreach (var data in dataSalesReceipt)
                                                {
                                                    LinkedTxn.TxnId = data.Id;
                                                    LinkedTxn.TxnType = i.Type;
                                                    LinkedTxn.TxnLineId = "0";
                                                }
                                            }
                                        }

                                    }
                                    if (i.Type == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                    {
                                        string id = string.Empty;
                                        if (i.ID != null)
                                        {
                                            var dataInvoice = await CommonUtilities.GetInvoiceDetailsExistsInOnlineQBO(i.ID.Trim());
                                            
                                            if (dataInvoice.Count > 0)
                                            {
                                                foreach (var data in dataInvoice)
                                                {
                                                    LinkedTxn.TxnId = data.Id;
                                                    LinkedTxn.TxnType = i.Type;
                                                    LinkedTxn.TxnLineId = "0";
                                                }
                                            }
                                        }
                                    }

                                    Line.LinkedTxn = new Intuit.Ipp.Data.LinkedTxn[] { LinkedTxn };
                                    Array.Resize(ref lines_online, linecount);
                                    lines_online[linecount - 1] = Line;
                                }
                                #endregion

                            }
                            if (l.LineCustomField.Count > 0)
                            {
                                #region Line Custom Field

                                foreach (var customData in l.LineCustomField)
                                {
                                    LinecustFiled.DefinitionId = linecountN.ToString();
                                    LinecustFiled.Name = customData.Name;
                                    LinecustFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                                    LinecustFiled.AnyIntuitObject = customData.Value;
                                    Array.Resize(ref Line_onlines, linecountN);
                                    Line_onlines[linecountN - 1] = LinecustFiled;
                                    linecountN++;
                                    LinecustFiled = new Intuit.Ipp.Data.CustomField();
                                }
                                Line.CustomField = Line_onlines;
                                #endregion
                            }
                            #region
                            if (l.DepositLineDetail.Count > 0)
                            {
                                Line.Description = l.LineDescription;
                                if (l.LineAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(l.LineAmount);
                                    Line.AmountSpecified = true;

                                }
                                #region Deposit Line Details
                                foreach (var s in l.DepositLineDetail)
                                {
                                    if (s != null)
                                    {
                                        Line.DetailType = LineDetailTypeEnum.DepositLineDetail;
                                        Line.DetailTypeSpecified = true;
                                    }

                                    #region Entity Ref
                                    foreach (var i in s.EntityRef)
                                    {
                                        if (i.Type == EntityTypeEnum.Customer.ToString() || i.Type == null)
                                        {
                                            string id = string.Empty;
                                            string Name = string.Empty;

                                            var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataCustomer)
                                            {
                                                id = item.Id;

                                            }
                                            lineDepositLineDetail.Entity = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id,
                                                type = EntityTypeEnum.Customer.ToString()
                                            };
                                        }
                                        else if (i.Type == EntityTypeEnum.Vendor.ToString())
                                        {

                                            string id = string.Empty;
                                            string Name = string.Empty;
                                            var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(i.Name.Trim());

                                            foreach (var item in dataVendor)
                                            {
                                                id = item.Id;

                                            }
                                            lineDepositLineDetail.Entity = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id,
                                                type = EntityTypeEnum.Vendor.ToString()
                                            };
                                        }
                                        else if (i.Type == EntityTypeEnum.Employee.ToString())
                                        {
                                            string id = string.Empty;
                                            string Name = string.Empty;
                                            var dataEmployee = await CommonUtilities.GetEmployeeDetailsExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataEmployee)
                                            {
                                                id = item.Id;
                                            }

                                            lineDepositLineDetail.Entity = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id,
                                                type = EntityTypeEnum.Employee.ToString()
                                            };
                                        }

                                    }
                                    #endregion
                                    #region ClassRef
                                    foreach (var i in s.ClassRef)
                                    {
                                        if (i.Name != null)
                                        {
                                            string id = string.Empty;
                                            var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineDepositLineDetail.ClassRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    #endregion
                                    #region AccountRef
                                    foreach (var i in s.AccountRef)
                                    {
                                        if (i.Name != null && i.Name != string.Empty && i.Name != "")
                                        {
                                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(i.Name);
                                            lineDepositLineDetail.AccountRef = new ReferenceType()
                                            {
                                                name = accDetails.name,
                                                Value = accDetails.Value
                                            };
                                        }
                                      
                                    }
                                    #endregion
                                    #region PaymentMethodRef
                                    foreach (var i in s.PaymentMethodRef)
                                    {
                                        if (i.Name != null)
                                        {
                                            string id = string.Empty;
                                            var dataItem = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(i.Name);

                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineDepositLineDetail.PaymentMethodRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    #endregion
                                    if (s.CheckNum != null)
                                    {
                                        lineDepositLineDetail.CheckNum = s.CheckNum;
                                        lineDepositLineDetail.TxnTypeSpecified = true;
                                    }
                                    #region LineDepositeTxnType
                                    if (s.TxnType != null)
                                    {
                                        if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.APCreditCard.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.APCreditCard;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ARRefundCreditCard;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Bill.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Bill;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BillPaymentCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.BuildAssembly;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CarryOver.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CarryOver;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CashPurchase.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CashPurchase;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Charge.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Charge;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Check.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Check;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.CreditMemo.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.CreditMemo;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Deposit.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Deposit;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFPLiabilityCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTBillPayment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.EFTRefund.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.EFTRefund;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Estimate.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Estimate;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.InventoryTransfer;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Invoice.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Invoice;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ItemReceipt;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.JournalEntry.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.JournalEntry;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.LiabilityAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Paycheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Paycheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PayrollLiabilityCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PriorPayment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PriorPayment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.PurchaseOrder;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.ReceivePayment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.RefundCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.RefundCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesOrder.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesOrder;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesReceipt;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.SalesTaxPaymentCheck;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.TimeActivity.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.TimeActivity;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.Transfer.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.Transfer;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.VendorCredit.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.VendorCredit;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                        else if (s.TxnType == Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment.ToString())
                                        {
                                            lineDepositLineDetail.TxnType = Intuit.Ipp.Data.TxnTypeEnum.YTDAdjustment;
                                            lineDepositLineDetail.TxnTypeSpecified = true;
                                        }
                                    }
                                    #endregion

                                    Line.AnyIntuitObject = lineDepositLineDetail;
                                    Array.Resize(ref lines_online, linecount);
                                    lines_online[linecount - 1] = Line;
                                }
                                #endregion
                            }
                            addDeposit.Line = AddLine(lines_online, Line, linecount);
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());

                        }
                    }

                }
                #endregion

                #region Deposit to Account
                foreach (var accounts in Deposit.DepositToAccountRef)
                {
                    QueryService<Account> customerQueryService = new QueryService<Account>(serviceContext);

                    string accountvalue = string.Empty;
                    if (accounts.Name != null && accounts.Name != string.Empty && accounts.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accounts.Name);
                        addDeposit.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                  
                }
                #endregion

                #region Taxcoderef
                foreach (var txntaxCode in Deposit.TxnTaxDetail)
                {
                    #region TxnTaxDetail
                    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    if (txntaxCode.TxnTaxCodeRef != null)
                    {
                        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                        {
                            TaxCode TaxCode = new TaxCode();
                            TaxCode.Name = taxRef.Name;
                            string Taxvalue = string.Empty;
                            string TaxName = string.Empty;
                            TaxName = TaxCode.Name;
                            if (TaxCode.Name != null)
                            {
                                var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                              
                                if (dataTax != null)
                                {
                                    foreach (var tax in dataTax)
                                    {
                                        Taxvalue = tax.Id;
                                    }

                                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                    {
                                        name = TaxName,
                                        Value = Taxvalue
                                    };
                                }
                            }
                        }
                    }

                    if (txnTaxDetail != null)
                    {
                        addDeposit.TxnTaxDetail = txnTaxDetail;
                    }
                    #endregion
                }

                #endregion

                #region cashback
                Intuit.Ipp.Data.CashBackInfo CashBack = new Intuit.Ipp.Data.CashBackInfo();
                foreach (var c in Deposit.CashBack)
                {
                    if (c.AccountRef.Count > 0)
                    {
                        foreach (var i in c.AccountRef)
                        {
                            if (i.Name != null && i.Name != string.Empty && i.Name != "")
                            {
                                ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(i.Name);
                                CashBack.AccountRef = new ReferenceType()
                                {
                                    name = accDetails.name,
                                    Value = accDetails.Value
                                };
                            }
                          
                        }
                    }

                    if (c.Amount != null)
                    {
                        CashBack.Amount = Convert.ToDecimal(c.Amount);
                        CashBack.AmountSpecified = true;
                    }
                    if (c.Memo != null)
                    {
                        CashBack.Memo = c.Memo;
                    }
                    if (CashBack.Amount != 0 && CashBack.AccountRef != null)
                    {
                        addDeposit.CashBack = CashBack;
                    }
                }


                #endregion


                #region txnSource
                if (Deposit.TxnSource != null)
                {
                    addDeposit.TxnSource = Deposit.TxnSource;
                    // addDeposit.TxnSource = true;
                }
                #endregion
                #region TotalAmt
                if (Deposit.TotalAmt != null)
                {
                    addDeposit.TotalAmt = Convert.ToDecimal(Deposit.TotalAmt);
                    addDeposit.TotalAmtSpecified = true;
                }
                #endregion

                #endregion
                DepositAdded = new Intuit.Ipp.Data.Deposit();
                DepositAdded = dataService.Update<Intuit.Ipp.Data.Deposit>(addDeposit);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (DepositAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = DepositAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = DepositAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }
        #endregion
    }

    /// <summary>
    /// checking doc number is present or not in quickbook online for Estimate.
    /// </summary>
    public class OnlineDepositeQBEntryCollection : Collection<OnlineDepositeQBEntry>
    {
        public OnlineDepositeQBEntry FindDepositeNumberEntry(string docNumber)
        {
            foreach (OnlineDepositeQBEntry item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }


}
