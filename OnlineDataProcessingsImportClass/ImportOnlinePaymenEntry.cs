﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using OnlineEntities;
namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlinePaymenEntry
    {
        private static ImportOnlinePaymenEntry m_ImportOnlinePaymenEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlinePaymenEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import payment class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlinePaymenEntry GetInstance()
        {
            if (m_ImportOnlinePaymenEntry == null)
                m_ImportOnlinePaymenEntry = new ImportOnlinePaymenEntry();
            return m_ImportOnlinePaymenEntry;
        }
        /// setting values to payment data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlinePaymentQBEntryCollection ImportOnlinePaymentData(DataTable dt, ref string logDirectory)
        {

            OnlineDataProcessingsImportClass.OnlinePaymentQBEntryCollection coll = new OnlinePaymentQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;          
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    string datevalue = string.Empty;
                   
                    //Employee Validation
                    OnlineDataProcessingsImportClass.Payment payment = new OnlineDataProcessingsImportClass.Payment();

                    if (dt.Columns.Contains("DocNumber"))
                    {
                        payment = coll.FindPamentNumberEntry(dr["DocNumber"].ToString());
                        if (payment == null)
                           {
                               payment = new Payment();
                               #region if payment is null
                               if (dt.Columns.Contains("DocNumber"))
                               {
                                   #region Validations of docnumber
                                   if (dr["DocNumber"].ToString() != string.Empty)
                                   {
                                       if (dr["DocNumber"].ToString().Length > 21)
                                       {
                                           if (isIgnoreAll == false)
                                           {
                                               string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                               DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                               if (Convert.ToString(result) == "Cancel")
                                               {
                                                   continue;
                                               }
                                               if (Convert.ToString(result) == "No")
                                               {
                                                   return null;
                                               }
                                               if (Convert.ToString(result) == "Ignore")
                                               {
                                                   payment.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                               }
                                               if (Convert.ToString(result) == "Abort")
                                               {
                                                   isIgnoreAll = true;
                                                   payment.DocNumber = dr["DocNumber"].ToString();
                                               }
                                           }
                                           else
                                           {
                                               payment.DocNumber = dr["DocNumber"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           payment.DocNumber = dr["DocNumber"].ToString();
                                       }
                                   }
                                   #endregion
                               }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of TxnDate
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    payment.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    payment.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                payment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            payment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        payment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payment.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payment.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payment.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.Line Line = new OnlineEntities.Line();

                            
                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                            if (dt.Columns.Contains("CustomerRef"))
                            {
                                #region Validations of LineCustomerRef
                                if (dr["CustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerRef (" + dr["CustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["CustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["CustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["CustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["CustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CustomerRef.Name != null)
                            {
                                payment.customerRef.Add(CustomerRef);
                            }

                            OnlineEntities.ARAccountRef ARAccountRef = new OnlineEntities.ARAccountRef();
                            if (dt.Columns.Contains("ARAccountRef"))
                            {
                                #region Validations of ARAccountRef
                                if (dr["ARAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["ARAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ARAccountRef (" + dr["ARAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ARAccountRef.Name = dr["ARAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ARAccountRef.Name = dr["ARAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ARAccountRef.Name = dr["ARAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ARAccountRef.Name = dr["ARAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ARAccountRef.Name != null)
                            {
                                payment.ARAccountRef.Add(ARAccountRef);
                            }
                           
                            if (dt.Columns.Contains("TxnStatus"))
                            {
                                #region Validations of TxnStatus
                                if (dr["TxnStatus"].ToString() != string.Empty)
                                {
                                    if (dr["TxnStatus"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnStatus (" + dr["TxnStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payment.TxnStatus = dr["TxnStatus"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payment.TxnStatus = dr["TxnStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payment.TxnStatus = dr["TxnStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.TxnStatus = dr["TxnStatus"].ToString();
                                    }
                                }
                                #endregion
                            }
                            //DepositeToAccountRef
                            OnlineEntities.DepositToAccountRef DepositToAccountRef = new OnlineEntities.DepositToAccountRef();
                            if (dt.Columns.Contains("DepositeToAccountRef"))
                            {
                                #region Validations of ARAccountRef
                                if (dr["DepositeToAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepositeToAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositeToAccountRef (" + dr["DepositeToAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (DepositToAccountRef.Name != null)
                            {
                                payment.DepositToAccountRef.Add(DepositToAccountRef);
                            }
                           
                            OnlineEntities.PaymentMethodRef PaymentMethodRef = new OnlineEntities.PaymentMethodRef();
                            if (dt.Columns.Contains("PaymentMethodRef"))
                            {
                                #region Validations of PaymentMethodRef
                                if (dr["PaymentMethodRef"].ToString() != string.Empty)
                                {
                                    if (dr["PaymentMethodRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentMethodRef (" + dr["PaymentMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (PaymentMethodRef.Name != null)
                            {
                                payment.PaymentMethodRef.Add(PaymentMethodRef);
                            }

                            #region Validations of PaymentRefNum
                            if (dt.Columns.Contains("PaymentRefNum"))
                            {

                                if (dr["PaymentRefNum"].ToString() != string.Empty)
                                {
                                    if (dr["PaymentRefNum"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentRefNum (" + dr["PaymentRefNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payment.PaymentRefNum = dr["PaymentRefNum"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payment.PaymentRefNum = dr["PaymentRefNum"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payment.PaymentRefNum = dr["PaymentRefNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.PaymentRefNum = dr["PaymentRefNum"].ToString();
                                    }
                                }

                            }
                            #endregion


                            #region Validations of TotalAmount
                            if (dt.Columns.Contains("TotalAmount"))
                            {

                                if (dr["TotalAmount"].ToString() != string.Empty)
                                {
                                    if (dr["TotalAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TotalAmount (" + dr["TotalAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payment.TotalPayment = dr["TotalAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payment.TotalPayment = dr["TotalAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payment.TotalPayment = dr["TotalAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.TotalPayment = dr["TotalAmount"].ToString();
                                    }
                                }

                            }
                            #endregion

                            CurrencyRef CurrencyRef = new CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                            {
                                payment.CurrencyRef.Add(CurrencyRef);
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "ExchangeRate")
                                            {
                                                isIgnoreAll = true;
                                                payment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    }
                                }

                                #endregion
                            }


                            if (dt.Columns.Contains("LinePaymentAmount"))
                            {
                                #region Validations for LinePaymentAmount
                                if (dr["LinePaymentAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LinePaymentAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LinePaymentAmount( " + dr["LinePaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LinePaymentAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            OnlineEntities.Linkedtxn LinkedTxn = new OnlineEntities.Linkedtxn();

                            if (dt.Columns.Contains("ApplytoInvoiceRef"))
                            {
                                #region Validations of ApplytoInvoiceRef
                                if (dr["ApplytoInvoiceRef"].ToString() != string.Empty)
                                {
                                    if (dr["ApplytoInvoiceRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ApplytoInvoiceRef (" + dr["ApplytoInvoiceRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (LinkedTxn.Applytobillref != null || Line.LineAmount != null)
                            {
                                Line.LinkedTxn.Add(LinkedTxn);
                                payment.Line.Add(Line);
                            }

                            if (dt.Columns.Contains("ProcessPayment"))
                            {
                                #region Validations of PrivateNote
                                if (dr["ProcessPayment"].ToString() != string.Empty)
                                {
                                    if (dr["ProcessPayment"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ProcessPayment (" + dr["ProcessPayment"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payment.ProcessPayment = dr["ProcessPayment"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payment.ProcessPayment = dr["ProcessPayment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payment.ProcessPayment = dr["ProcessPayment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.ProcessPayment = dr["ProcessPayment"].ToString();
                                    }
                                }
                                #endregion
                            }
                         
                            if (dt.Columns.Contains("UnappliedAmt"))
                            {
                                #region Validations of UnappliedAmt
                                if (dr["UnappliedAmt"].ToString() != string.Empty)
                                {
                                    if (dr["UnappliedAmt"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnappliedAmt (" + dr["UnappliedAmt"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                               payment.UnappliedAmt= dr["UnappliedAmt"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payment.UnappliedAmt = dr["UnappliedAmt"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payment.UnappliedAmt = dr["UnappliedAmt"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.UnappliedAmt = dr["UnappliedAmt"].ToString();
                                    }
                                }
                                #endregion
                            } 
                           

                               coll.Add(payment);
                               #endregion
                           }
                           else
                           {
                               #region if payment is not null                             
                              OnlineEntities.Line Line = new OnlineEntities.Line();  
                               if (dt.Columns.Contains("LinePaymentAmount"))
                               {
                                   #region Validations for LinePaymentAmount
                                   if (dr["LinePaymentAmount"].ToString() != string.Empty)
                                   {
                                       decimal amount;
                                       if (!decimal.TryParse(dr["LinePaymentAmount"].ToString(), out amount))
                                       {
                                           if (isIgnoreAll == false)
                                           {
                                               string strMessages = "This LinePaymentAmount( " + dr["LinePaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                               DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                               if (Convert.ToString(result) == "Cancel")
                                               {
                                                   continue;
                                               }
                                               if (Convert.ToString(result) == "No")
                                               {
                                                   return null;
                                               }
                                               if (Convert.ToString(result) == "Ignore")
                                               {
                                                   Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                               }
                                               if (Convert.ToString(result) == "Abort")
                                               {
                                                   isIgnoreAll = true;
                                                   Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                               }
                                           }
                                           else
                                           {
                                               Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LinePaymentAmount"].ToString()));
                                       }
                                   }

                                   #endregion
                               }

                               OnlineEntities.Linkedtxn LinkedTxn = new OnlineEntities.Linkedtxn();

                               if (dt.Columns.Contains("ApplytoInvoiceRef"))
                               {
                                   #region Validations of ApplytoInvoiceRef
                                   if (dr["ApplytoInvoiceRef"].ToString() != string.Empty)
                                   {
                                       if (dr["ApplytoInvoiceRef"].ToString().Length > 100)
                                       {
                                           if (isIgnoreAll == false)
                                           {
                                               string strMessages = "This ApplytoInvoiceRef (" + dr["ApplytoInvoiceRef"].ToString() + ") is exceeded maximum length of quickbooks online. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                               DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                               if (Convert.ToString(result) == "Cancel")
                                               {
                                                   continue;
                                               }
                                               if (Convert.ToString(result) == "No")
                                               {
                                                   return null;
                                               }
                                               if (Convert.ToString(result) == "Ignore")
                                               {
                                                   LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString().Substring(0, 100);
                                               }
                                               if (Convert.ToString(result) == "Abort")
                                               {
                                                   isIgnoreAll = true;
                                                   LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                               }
                                           }
                                           else
                                           {
                                               LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                           }
                                       }
                                       else
                                       {
                                           LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                       }
                                   }
                                   #endregion
                               }

                               if (LinkedTxn.Applytobillref != null || Line.LineAmount != null)
                               {
                                   Line.LinkedTxn.Add(LinkedTxn);
                                   payment.Line.Add(Line);
                               }                             

                               #endregion
                           }                          
                     
                    }
                    else
                    {

                        #region no docnumber
                        payment = new Payment();
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of TxnDate
                            DateTime SODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payment.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            payment.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        payment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    payment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payment.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            payment.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    payment.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }


                        OnlineEntities.Line Line = new OnlineEntities.Line();
                       
                        OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                        if (dt.Columns.Contains("CustomerRef"))
                        {
                            #region Validations of LineCustomerRef
                            if (dr["CustomerRef"].ToString() != string.Empty)
                            {
                                if (dr["CustomerRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomerRef (" + dr["CustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef.Name = dr["CustomerRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef.Name = dr["CustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["CustomerRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["CustomerRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (CustomerRef.Name != null)
                        {
                            payment.customerRef.Add(CustomerRef);
                        }

                        OnlineEntities.ARAccountRef ARAccountRef = new OnlineEntities.ARAccountRef();
                        if (dt.Columns.Contains("ARAccountRef"))
                        {
                            #region Validations of ARAccountRef
                            if (dr["ARAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["ARAccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ARAccountRef (" + dr["ARAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ARAccountRef.Name = dr["ARAccountRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ARAccountRef.Name = dr["ARAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ARAccountRef.Name = dr["ARAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ARAccountRef.Name = dr["ARAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ARAccountRef.Name != null)
                        {
                            payment.ARAccountRef.Add(ARAccountRef);
                        }
                       
                        if (dt.Columns.Contains("TxnStatus"))
                        {
                            #region Validations of TxnStatus
                            if (dr["TxnStatus"].ToString() != string.Empty)
                            {
                                if (dr["TxnStatus"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnStatus (" + dr["TxnStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payment.TxnStatus = dr["TxnStatus"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            payment.TxnStatus = dr["TxnStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.TxnStatus = dr["TxnStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    payment.TxnStatus = dr["TxnStatus"].ToString();
                                }
                            }
                            #endregion
                        }
                        //DepositeToAccountRef
                        OnlineEntities.DepositToAccountRef DepositToAccountRef = new OnlineEntities.DepositToAccountRef();
                        if (dt.Columns.Contains("DepositeToAccountRef"))
                        {
                            #region Validations of ARAccountRef
                            if (dr["DepositeToAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["DepositeToAccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositeToAccountRef (" + dr["DepositeToAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    DepositToAccountRef.Name = dr["DepositeToAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (DepositToAccountRef.Name != null)
                        {
                            payment.DepositToAccountRef.Add(DepositToAccountRef);
                        }
                        

                        OnlineEntities.PaymentMethodRef PaymentMethodRef = new OnlineEntities.PaymentMethodRef();
                        if (dt.Columns.Contains("PaymentMethodRef"))
                        {
                            #region Validations of PaymentMethodRef
                            if (dr["PaymentMethodRef"].ToString() != string.Empty)
                            {
                                if (dr["PaymentMethodRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PaymentMethodRef (" + dr["PaymentMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                    }
                                }
                                else
                                {
                                    PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (PaymentMethodRef.Name != null)
                        {
                            payment.PaymentMethodRef.Add(PaymentMethodRef);
                        }

                        #region Validations of PaymentRefNum
                        if (dt.Columns.Contains("PaymentRefNum"))
                        {

                            if (dr["PaymentRefNum"].ToString() != string.Empty)
                            {
                                if (dr["PaymentRefNum"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PaymentRefNum (" + dr["PaymentRefNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payment.PaymentRefNum = dr["PaymentRefNum"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            payment.PaymentRefNum = dr["PaymentRefNum"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.PaymentRefNum = dr["PaymentRefNum"].ToString();
                                    }
                                }
                                else
                                {
                                    payment.PaymentRefNum = dr["PaymentRefNum"].ToString();
                                }
                            }

                        }
                        #endregion


                        #region Validations of TotalAmount
                        if (dt.Columns.Contains("TotalAmount"))
                        {

                            if (dr["TotalAmount"].ToString() != string.Empty)
                            {
                                if (dr["TotalAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TotalAmount (" + dr["TotalAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payment.TotalPayment = dr["TotalAmount"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            payment.TotalPayment = dr["TotalAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.TotalPayment = dr["TotalAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    payment.TotalPayment = dr["TotalAmount"].ToString();
                                }
                            }

                        }
                        #endregion

                        CurrencyRef CurrencyRef = new CurrencyRef();

                        if (dt.Columns.Contains("CurrencyRef"))
                        {
                            #region Validations of CurrencyRef
                            if (dr["CurrencyRef"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CurrencyRef.Name != null)
                        {
                            payment.CurrencyRef.Add(CurrencyRef);
                        }

                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "ExchangeRate")
                                        {
                                            isIgnoreAll = true;
                                            payment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    payment.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LinePaymentAmount"))
                        {
                            #region Validations for LinePaymentAmount
                            if (dr["LinePaymentAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LinePaymentAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LinePaymentAmount( " + dr["LinePaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LinePaymentAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LinePaymentAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        OnlineEntities.Linkedtxn LinkedTxn = new OnlineEntities.Linkedtxn();

                        if (dt.Columns.Contains("ApplytoInvoiceRef"))
                        {
                            #region Validations of ApplytoInvoiceRef
                            if (dr["ApplytoInvoiceRef"].ToString() != string.Empty)
                            {
                                if (dr["ApplytoBillRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ApplytoInvoiceRef (" + dr["ApplytoInvoiceRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                    }
                                }
                                else
                                {
                                    LinkedTxn.Applytobillref = dr["ApplytoInvoiceRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (LinkedTxn.Applytobillref != null || Line.LineAmount != null)
                        {
                            Line.LinkedTxn.Add(LinkedTxn);
                            payment.Line.Add(Line);
                        }
                        if (dt.Columns.Contains("ProcessPayment"))
                        {
                            #region Validations of PrivateNote
                            if (dr["ProcessPayment"].ToString() != string.Empty)
                            {
                                if (dr["ProcessPayment"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ProcessPayment (" + dr["ProcessPayment"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payment.ProcessPayment = dr["ProcessPayment"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            payment.ProcessPayment = dr["ProcessPayment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.ProcessPayment = dr["ProcessPayment"].ToString();
                                    }
                                }
                                else
                                {
                                    payment.ProcessPayment = dr["ProcessPayment"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("UnappliedAmt"))
                        {
                            #region Validations of UnappliedAmt
                            if (dr["UnappliedAmt"].ToString() != string.Empty)
                            {
                                if (dr["UnappliedAmt"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This UnappliedAmt (" + dr["UnappliedAmt"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payment.UnappliedAmt = dr["UnappliedAmt"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            payment.UnappliedAmt = dr["UnappliedAmt"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payment.UnappliedAmt = dr["UnappliedAmt"].ToString();
                                    }
                                }
                                else
                                {
                                    payment.UnappliedAmt = dr["UnappliedAmt"].ToString();
                                }
                            }
                            #endregion
                        } 

                        coll.Add(payment);
                        #endregion
                    }
                }
           
                else
                {
                    return null;
                }
            }
            #endregion

            #endregion
            return coll;

        }
    }
}

