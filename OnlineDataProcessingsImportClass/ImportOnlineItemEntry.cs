﻿using System;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using EDI.Constant;
using DataProcessingBlocks;
using System.ComponentModel;
using System.Globalization;
using OnlineEntities;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineItemEntry
    {
        private static ImportOnlineItemEntry m_ImportOnlineItemEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineItemEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import item class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineItemEntry GetInstance()
        {
            if (m_ImportOnlineItemEntry == null)
                m_ImportOnlineItemEntry = new ImportOnlineItemEntry();
            return m_ImportOnlineItemEntry;
        }
        /// setting values to item data table and returns collection.
        public async System.Threading.Tasks.Task<OnlineItemQBEntryCollection> ImportOnlineItemsData(DataTable dt)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineItemQBEntryCollection coll = new OnlineItemQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Items Validation
                    OnlineDataProcessingsImportClass.ItemClass item = new OnlineDataProcessingsImportClass.ItemClass();
                    if (dt.Columns.Contains("Name"))
                    {
                        item = coll.FindItemEntry(dr["Name"].ToString());

                        if (item == null)
                        {

                            #region if item is null

                            item = new ItemClass();

                            if (dt.Columns.Contains("Name"))
                            {
                                #region Validations of Name
                                if (dr["Name"].ToString() != string.Empty)
                                {
                                    if (dr["Name"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.Name = dr["Name"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.Name = dr["Name"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.Name = dr["Name"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.Name = dr["Name"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations of Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description (" + dr["Description"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.Description = dr["Description"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.Description = dr["Description"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.Description = dr["Description"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.Description = dr["Description"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Active"))
                            {
                                #region Validations of Active
                                if (dr["Active"].ToString() != "<None>" || dr["Active"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["Active"].ToString(), out result))
                                    {
                                        item.Active = Convert.ToInt32(dr["Active"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["Active"].ToString().ToLower() == "true")
                                        {
                                            item.Active = dr["Active"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["Active"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "Active";
                                            }
                                            else
                                                item.Active = dr["Active"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Active(" + dr["Active"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    item.Active = dr["Active"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    item.Active = dr["Active"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                item.Active = dr["Active"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("FullyQualifiedName"))
                            {
                                #region Validations of FullyQualifiedName
                                if (dr["FullyQualifiedName"].ToString() != string.Empty)
                                {
                                    if (dr["FullyQualifiedName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FullyQualifiedName (" + dr["FullyQualifiedName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.FullyQualifiedName = dr["FullyQualifiedName"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.ParentRef ParentRef = new OnlineEntities.ParentRef();
                            string[] arr = new string[15];
                            if (item.FullyQualifiedName.Contains(":"))
                            {
                                arr = item.FullyQualifiedName.Split(':');

                                string pname = "";
                                int cnt = 0;
                                foreach (var a in arr)
                                {
                                    cnt++;
                                }
                                for (int i = 0; i < cnt - 1; i++)
                                {
                                    pname += arr[i] + ":";
                                }
                                pname = pname.Remove(pname.Length - 1);
                                ParentRef.Name = pname;
                                item.ParentRef.Add(ParentRef);
                                item.Name = arr[cnt - 1];
                            }
                            else
                            {
                                if (!dt.Columns.Contains("Name") || (dr["Name"].ToString() == string.Empty))
                                {
                                    item.Name = item.FullyQualifiedName;
                                }

                            }


                            if (dt.Columns.Contains("Taxable"))
                            {
                                #region Validations of Taxable
                                if (dr["Taxable"].ToString() != "<None>" || dr["Taxable"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["Taxable"].ToString(), out result))
                                    {
                                        item.Taxable = Convert.ToInt32(dr["Taxable"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["Taxable"].ToString().ToLower() == "true")
                                        {
                                            item.Taxable = dr["Taxable"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["Taxable"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "Taxable";
                                            }
                                            else
                                                item.Taxable = dr["Taxable"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Taxable(" + dr["Taxable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    item.Taxable = dr["Taxable"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    item.Taxable = dr["Taxable"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                item.Taxable = dr["Taxable"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesTaxIncluded"))
                            {
                                #region Validations of SalesTaxIncluded
                                if (dr["SalesTaxIncluded"].ToString() != "<None>" || dr["SalesTaxIncluded"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["SalesTaxIncluded"].ToString(), out result))
                                    {
                                        item.SalesTaxIncluded = Convert.ToInt32(dr["SalesTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["SalesTaxIncluded"].ToString().ToLower() == "true")
                                        {
                                            item.SalesTaxIncluded = dr["SalesTaxIncluded"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["SalesTaxIncluded"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "SalesTaxIncluded";
                                            }
                                            else
                                                item.SalesTaxIncluded = dr["SalesTaxIncluded"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SalesTaxIncluded(" + dr["SalesTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    item.SalesTaxIncluded = dr["SalesTaxIncluded"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    item.SalesTaxIncluded = dr["SalesTaxIncluded"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                item.SalesTaxIncluded = dr["SalesTaxIncluded"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("UnitPrice"))
                            {
                                #region Validations of UnitPrice
                                if (dr["UnitPrice"].ToString() != string.Empty)
                                {
                                    if (dr["UnitPrice"].ToString().Length > 99999999999)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitPrice (" + dr["UnitPrice"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.UnitPrice = dr["UnitPrice"].ToString();//.Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.UnitPrice = dr["UnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.UnitPrice = dr["UnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.UnitPrice = dr["UnitPrice"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("RatePercent"))
                            {
                                #region Validations of RatePercent
                                if (dr["RatePercent"].ToString() != string.Empty)
                                {
                                    if (dr["RatePercent"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This RatePercent (" + dr["RatePercent"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.RatePercent = dr["RatePercent"].ToString();//.Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.RatePercent = dr["RatePercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.RatePercent = dr["RatePercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.RatePercent = dr["RatePercent"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Type"))
                            {
                                #region Validations of Type
                                if (dr["Type"].ToString() != string.Empty)
                                {
                                    if (dr["Type"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Type (" + dr["Type"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.Type = dr["Type"].ToString();//.Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.Type = dr["Type"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.Type = dr["Type"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.Type = dr["Type"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.IncomeAccountRef incomeAccountRef = new OnlineEntities.IncomeAccountRef();
                            if (dt.Columns.Contains("IncomeAccountRef"))
                            {
                                #region Validations of IncomeAccountRef
                                if (dr["IncomeAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["IncomeAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IncomeAccountRef (" + dr["IncomeAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                incomeAccountRef.Name = dr["IncomeAccountRef"].ToString();//.Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                incomeAccountRef.Name = dr["IncomeAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            incomeAccountRef.Name = dr["IncomeAccountRef"].ToString();//.Substring(0, 15);
                                        }

                                    }
                                    else
                                    {
                                        incomeAccountRef.Name = dr["IncomeAccountRef"].ToString();
                                    }

                                }
                                #endregion
                            }
                            if (incomeAccountRef != null)
                            {
                                item.IncomeAccountRef.Add(incomeAccountRef);
                            }
                            
                            if (dt.Columns.Contains("PurchaseDesc"))
                            {
                                #region Validations of PurchaseDesc
                                if (dr["PurchaseDesc"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseDesc"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PurchaseDesc (" + dr["PurchaseDesc"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.PurchaseDesc = dr["PurchaseDesc"].ToString();//.Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PurchaseTaxIncluded"))
                            {
                                #region Validations of PurchaseTaxIncluded
                                if (dr["PurchaseTaxIncluded"].ToString() != "<None>" || dr["PurchaseTaxIncluded"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["PurchaseTaxIncluded"].ToString(), out result))
                                    {
                                        item.PurchaseTaxIncluded = Convert.ToInt32(dr["PurchaseTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["PurchaseTaxIncluded"].ToString().ToLower() == "true")
                                        {
                                            item.PurchaseTaxIncluded = dr["PurchaseTaxIncluded"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["PurchaseTaxIncluded"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "PurchaseTaxIncluded";
                                            }
                                            else
                                                item.PurchaseTaxIncluded = dr["PurchaseTaxIncluded"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This PurchaseTaxIncluded(" + dr["PurchaseTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    item.PurchaseTaxIncluded = dr["PurchaseTaxIncluded"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    item.PurchaseTaxIncluded = dr["PurchaseTaxIncluded"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                item.PurchaseTaxIncluded = dr["PurchaseTaxIncluded"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PurchaseCost"))
                            {
                                #region Validations of PurchaseCost
                                if (dr["PurchaseCost"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseCost"].ToString().Length > 99999999999)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PurchaseCost (" + dr["PurchaseCost"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.PurchaseCost = dr["PurchaseCost"].ToString();//.Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.PurchaseCost = dr["PurchaseCost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.PurchaseCost = dr["PurchaseCost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.PurchaseCost = dr["PurchaseCost"].ToString();
                                    }
                                }
                                #endregion
                            }
                            OnlineEntities.ExpenseAccountRef ExpenseAccountRef = new OnlineEntities.ExpenseAccountRef();
                            if (dt.Columns.Contains("ExpenseAccountRef"))
                            {
                                #region Validations of ExpenseAccountRef
                                if (dr["ExpenseAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAccountRef (" + dr["ExpenseAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseAccountRef.Name = dr["ExpenseAccountRef"].ToString().Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseAccountRef.Name = dr["ExpenseAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ExpenseAccountRef.Name = dr["ExpenseAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ExpenseAccountRef.Name = dr["ExpenseAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (ExpenseAccountRef != null)
                            {
                                item.ExpenseAccountRef.Add(ExpenseAccountRef);
                            }
                            
                            OnlineEntities.AssetAccountRef AssetAccountRef = new OnlineEntities.AssetAccountRef();
                            if (dt.Columns.Contains("AssetAccountRef"))
                            {
                                #region Validations of AssetAccountRef
                                if (dr["AssetAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["AssetAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AssetAccountRef (" + dr["AssetAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AssetAccountRef.Name = dr["AssetAccountRef"].ToString();//.Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AssetAccountRef.Name = dr["AssetAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AssetAccountRef.Name = dr["AssetAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AssetAccountRef.Name = dr["AssetAccountRef"].ToString();
                                    }

                                }
                                #endregion
                            }
                            if (AssetAccountRef != null)
                            {
                                item.AssetAccountRef.Add(AssetAccountRef);
                            }
                           
                            if (dt.Columns.Contains("TrackQtyOnHand"))
                            {
                                #region Validations of TrackQtyOnHand
                                if (dr["TrackQtyOnHand"].ToString() != string.Empty)
                                {
                                    if (dr["TrackQtyOnHand"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TrackQtyOnHand (" + dr["TrackQtyOnHand"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.TrackQtyOnHand = dr["TrackQtyOnHand"].ToString();//.Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.TrackQtyOnHand = dr["TrackQtyOnHand"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.TrackQtyOnHand = dr["TrackQtyOnHand"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.TrackQtyOnHand = dr["TrackQtyOnHand"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("QtyOnHand"))
                            {
                                #region Validations of QtyOnHand
                                if (dr["QtyOnHand"].ToString() != string.Empty)
                                {
                                    if (dr["QtyOnHand"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This QtyOnHand (" + dr["QtyOnHand"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.QtyOnHand = dr["QtyOnHand"].ToString();//.Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.QtyOnHand = dr["QtyOnHand"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.QtyOnHand = dr["QtyOnHand"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.QtyOnHand = dr["QtyOnHand"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.SalesTaxCodeRef SalesTaxCodeRef = new OnlineEntities.SalesTaxCodeRef();
                            if (dt.Columns.Contains("SalesTaxCodeRef"))
                            {
                                #region Validations of SalesTaxCodeRef
                                if (dr["SalesTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTaxCodeRef (" + dr["SalesTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesTaxCodeRef.Name = dr["SalesTaxCodeRef"].ToString();//.Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesTaxCodeRef.Name = dr["SalesTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesTaxCodeRef.Name = dr["SalesTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesTaxCodeRef.Name = dr["SalesTaxCodeRef"].ToString();
                                    }

                                }
                                #endregion
                            }
                            if (SalesTaxCodeRef.Name != null)
                            {
                                item.SalesTaxCodeRef.Add(SalesTaxCodeRef);
                            }

                            OnlineEntities.PurchaseTaxCodeRef PurchaseTaxCodeRef = new PurchaseTaxCodeRef();
                            if (dt.Columns.Contains("PurchaseTaxCodeRef"))
                            {
                                #region Validations of PurchaseTaxCodeRef
                                if (dr["PurchaseTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PurchaseTaxCodeRef (" + dr["PurchaseTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseTaxCodeRef.Name = dr["PurchaseTaxCodeRef"].ToString();//.Substring(0, 15);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseTaxCodeRef.Name = dr["PurchaseTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseTaxCodeRef.Name = dr["PurchaseTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseTaxCodeRef.Name = dr["PurchaseTaxCodeRef"].ToString();
                                    }

                                }
                                #endregion
                            }
                            if (PurchaseTaxCodeRef.Name != null)
                            {
                                item.PurchaseTaxCodeRef.Add(PurchaseTaxCodeRef);
                            }

                            if (dt.Columns.Contains("InvStartDate"))
                            {
                                #region Validations of InvStartDate
                                DateTime releasedDate = new DateTime();
                                if (dr["InvStartDate"].ToString() != "<None>" || dr["InvStartDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["InvStartDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out releasedDate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This item.InvStartDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    item.InvStartDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    item.InvStartDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                item.InvStartDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            releasedDate = dttest;
                                            item.InvStartDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        releasedDate = Convert.ToDateTime(datevalue);
                                        item.InvStartDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            #endregion
                            //P Axis 13.1 : issue 629
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                item.SKU = dr["SKU"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                item.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            item.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        item.SKU = dr["SKU"].ToString();
                                    }
                                }
                               
                            }
                            //P Axis 13.1 : issue 629 END
                            #endregion
                            coll.Add(item);
                        }
                    }

                }
                else
                {
                    return null;
                }

            }
            #endregion


            #region Create Parent for Item
            foreach (DataRow dr in dt.Rows)
            {
                if (dt.Columns.Contains("FullyQualifiedName"))
                {
                    if (dr["FullyQualifiedName"].ToString() != string.Empty)
                    {
                        ReadOnlyCollection<Intuit.Ipp.Data.Item> OnlineItem = null;
                        OnlineItem = await CommonUtilities.GetItemExistsInOnlineQBO(dr["FullyQualifiedName"].ToString());
                        if (OnlineItem.Count == 0)
                        {

                            string ItemName = dr["FullyQualifiedName"].ToString();
                            if (ItemName.Contains(":") && CommonUtilities.GetInstance().ItemCategoriesFeature == true)
                            {
                                DataService dataService = null;
                                ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
                                dataService = new DataService(serviceContext);
                                QueryService<Item> ItemQueryService = new QueryService<Item>(serviceContext);
                                string[] arr = new string[15];
                                string[] arr1 = new string[15];
                                string itemvalue = string.Empty;
                                string ParentCategory = "";
                                string id = "";
                                string newVar1 = "";
                                string newVar2 = "";
                                string newVar11 = "";
                                string newVar22 = "";
                                int arrCount = 0;
                                arr1 = ItemName.Split(':');
                                arrCount = arr1.Length;
                                if (ItemName.Contains("'"))
                                {
                                    ItemName = ItemName.Replace("'", "\\'");
                                }
                                arr = ItemName.Split(':');
                                int i = 0;
                                int not_a_Category_Break = 0;
                                string itemname = arr[0];
                                string itemname1 = arr1[0];

                                ReadOnlyCollection<Intuit.Ipp.Data.Item> dataItem1;
                                if (arr.Length < 6)
                                {
                                    foreach (var c in arr)
                                    {

                                        if (i == 0)
                                        {
                                            if (itemname != null)
                                            {
                                                dataItem1 = new ReadOnlyCollection<Intuit.Ipp.Data.Item>(ItemQueryService.ExecuteIdsQuery("Select * From Item where FullyQualifiedName ='" + itemname + "'"));
                                                foreach (var item in dataItem1)
                                                {
                                                    if (item.Type != ItemTypeEnum.Category)
                                                    {
                                                        //We can't modify the category type from service,inv/noninv to Category if first element is not a category hence break
                                                        not_a_Category_Break = 1;
                                                        break;
                                                        // Item ItemCat1 = new Intuit.Ipp.Data.Item();

                                                        // ItemCat1.Id = item.Id;
                                                        // ItemCat1.Active = true;
                                                        // ItemCat1.ActiveSpecified = true;
                                                        //ItemCat1.FullyQualifiedName = itemname;
                                                        // ItemCat1.Name = itemname;
                                                        // ItemCat1.Type = ItemTypeEnum.Category;
                                                        // ItemCat1.TypeSpecified = true;
                                                        // ParentCategory = itemname;
                                                        // try
                                                        // {
                                                        //     ItemCat1 = dataService.Update<Item>(ItemCat1);
                                                        //     if (ItemCat1.domain == "QBO")
                                                        //     {
                                                        //         itemvalue = ItemCat1.Id;
                                                        //     }

                                                        //     //ItemCat1 = dataService.Add<Item>(ItemCat1);
                                                        //     //if (ItemCat1.domain == "QBO")
                                                        //     //{
                                                        //     //    itemvalue = ItemCat1.Id;
                                                        //     //}
                                                        // }
                                                        // catch 
                                                        // {
                                                        //     itemvalue = item.Id;
                                                        // }
                                                    }
                                                    else
                                                    {
                                                        ParentCategory = arr[i];
                                                        foreach (var item1 in dataItem1)
                                                        {
                                                            itemvalue = item1.Id;
                                                        }
                                                    }
                                                }
                                                if (not_a_Category_Break == 1)
                                                { break; }

                                                if (dataItem1.Count == 0)
                                                {
                                                    Intuit.Ipp.Data.Item ItemCat1 = new Intuit.Ipp.Data.Item();
                                                    ItemCat1.Active = true;
                                                    ItemCat1.ActiveSpecified = true;
                                                    ItemCat1.FullyQualifiedName = itemname;
                                                    ItemCat1.Name = itemname;
                                                    ItemCat1.Type = ItemTypeEnum.Category;
                                                    ItemCat1.TypeSpecified = true;
                                                    ParentCategory = itemname;
                                                    try
                                                    {
                                                        ItemCat1 = dataService.Add<Item>(ItemCat1);
                                                        if (ItemCat1.domain == "QBO")
                                                        {
                                                            itemvalue = ItemCat1.Id;
                                                        }
                                                    }
                                                    catch
                                                    { }
                                                }
                                            }

                                        }
                                        else if (i < arrCount - 1 && i != 0)
                                        {
                                            if (i == 1)
                                            {
                                                newVar1 = arr[0] + ":" + arr[1];
                                                newVar11 = arr1[0] + ":" + arr1[1];

                                                newVar2 = arr[0];
                                                newVar22 = arr1[0];
                                            }
                                            if (i == 2)
                                            {
                                                newVar1 = arr[0] + ":" + arr[1] + ":" + arr[2];
                                                newVar11 = arr1[0] + ":" + arr1[1] + ":" + arr1[2];

                                                newVar2 = arr[0] + ":" + arr[1];
                                                newVar22 = arr1[0] + ":" + arr1[1];

                                            }
                                            if (i == 3)
                                            {
                                                newVar1 = arr[0] + ":" + arr[1] + ":" + arr[2] + ":" + arr[3];
                                                newVar11 = arr1[0] + ":" + arr1[1] + ":" + arr1[2] + ":" + arr1[3];

                                                newVar2 = arr[0] + ":" + arr[1] + ":" + arr[2];
                                                newVar22 = arr1[0] + ":" + arr1[1] + ":" + arr1[2];

                                            }
                                            dataItem1 = new ReadOnlyCollection<Intuit.Ipp.Data.Item>(ItemQueryService.ExecuteIdsQuery("Select * From Item where FullyQualifiedName ='" + newVar1 + "'"));
                                            if (dataItem1.Count == 0)
                                            {
                                                ServiceContext serviceContext1 = await CommonUtilities.GetQBOServiceContext();
                                                dataService = new DataService(serviceContext1);
                                                Item ItemSubCategory = new Item();
                                                ItemSubCategory.Active = true;
                                                ItemSubCategory.Name = arr1[i];
                                                ItemSubCategory.FullyQualifiedName = newVar11;
                                                ItemSubCategory.Type = ItemTypeEnum.Category;
                                                ItemSubCategory.TypeSpecified = true;
                                                ItemSubCategory.Level = i;
                                                ItemSubCategory.LevelSpecified = true;
                                                ItemSubCategory.SubItem = true;
                                                ItemSubCategory.SubItemSpecified = true;
                                                ItemSubCategory.ParentRef = new ReferenceType()
                                                {
                                                    name = ParentCategory,
                                                    Value = itemvalue,
                                                };
                                                ParentCategory = newVar11;
                                                try
                                                {
                                                    ItemSubCategory = dataService.Add<Item>(ItemSubCategory);
                                                    if (ItemSubCategory.domain == "QBO")
                                                    {
                                                        itemvalue = ItemSubCategory.Id;
                                                    }
                                                }
                                                catch
                                                {
                                                }
                                            }
                                            else
                                            {
                                                foreach (var item1 in dataItem1)
                                                {
                                                    if (item1.Type != ItemTypeEnum.Category)
                                                    {
                                                        ServiceContext serviceContext1 = await CommonUtilities.GetQBOServiceContext();
                                                        dataService = new DataService(serviceContext1);
                                                        Item ItemSubCategory = new Item();
                                                        ItemSubCategory.Active = true;
                                                        ItemSubCategory.Name = arr1[i];
                                                        ItemSubCategory.FullyQualifiedName = newVar11;
                                                        ItemSubCategory.Type = ItemTypeEnum.Category;
                                                        ItemSubCategory.TypeSpecified = true;
                                                        ItemSubCategory.Level = i;
                                                        ItemSubCategory.LevelSpecified = true;
                                                        ItemSubCategory.SubItem = true;
                                                        ItemSubCategory.SubItemSpecified = true;
                                                        ItemSubCategory.ParentRef = new ReferenceType()
                                                        {
                                                            name = ParentCategory,
                                                            Value = itemvalue,
                                                        };
                                                        ParentCategory = newVar11;
                                                        try
                                                        {
                                                            ItemSubCategory = dataService.Add<Item>(ItemSubCategory);
                                                            if (ItemSubCategory.domain == "QBO")
                                                            {
                                                                itemvalue = ItemSubCategory.Id;
                                                            }
                                                        }
                                                        catch
                                                        { }
                                                    }
                                                    else
                                                    {
                                                        itemvalue = item1.Id;
                                                    }
                                                }
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                            //else if(ItemName.Contains(":") && CommonUtilities.GetInstance().ItemCategoriesFeature == false)
                            //{
                            //    CommonUtilities cUtility = new CommonUtilities();
                            //    cUtility.CreateItemInQBO(dr["FullyQualifiedName"].ToString());
                            //}
                        }
                        else
                        {
                            if (OnlineItem.Count == 1)
                            {
                                foreach (var iteminfo in OnlineItem)
                                {
                                    if (iteminfo.Type != ItemTypeEnum.Category)
                                    {
                                        string ItemName = dr["FullyQualifiedName"].ToString();
                                        if (ItemName.Contains(":") && CommonUtilities.GetInstance().ItemCategoriesFeature == true)
                                        {
                                            DataService dataService = null;
                                            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
                                            dataService = new DataService(serviceContext);

                                            ReadOnlyCollection<Intuit.Ipp.Data.Item> dataItem = null;
                                            QueryService<Item> ItemQueryService = new QueryService<Item>(serviceContext);
                                            string[] arr = new string[15];
                                            string[] arr1 = new string[15];
                                            string itemvalue = string.Empty;
                                            string ParentCategory = "";
                                            string id = "";
                                            string newVar1 = "";
                                            string newVar2 = "";
                                            string newVar11 = "";
                                            string newVar22 = "";
                                            int arrCount = 0;
                                            arr1 = ItemName.Split(':');
                                            arrCount = arr1.Length;
                                            if (ItemName.Contains("'"))
                                            {
                                                ItemName = ItemName.Replace("'", "\\'");
                                            }
                                            arr = ItemName.Split(':');
                                            int i = 0;

                                            string itemname = arr[0];
                                            string itemname1 = arr1[0];

                                            ReadOnlyCollection<Intuit.Ipp.Data.Item> dataItem1;
                                            if (arr.Length < 6)
                                            {
                                                foreach (var c in arr)
                                                {

                                                    if (i == 0)
                                                    {
                                                        if (itemname != null)
                                                        {
                                                            dataItem1 = new ReadOnlyCollection<Intuit.Ipp.Data.Item>(ItemQueryService.ExecuteIdsQuery("Select * From Item where FullyQualifiedName ='" + itemname + "'"));
                                                            foreach (var item in dataItem1)
                                                            {
                                                                if (item.Type != ItemTypeEnum.Category)
                                                                {
                                                                    Intuit.Ipp.Data.Item ItemCat1 = new Intuit.Ipp.Data.Item();
                                                                    ItemCat1.Active = true;
                                                                    ItemCat1.ActiveSpecified = true;
                                                                    ItemCat1.FullyQualifiedName = itemname;
                                                                    ItemCat1.Name = itemname;
                                                                    ItemCat1.Type = ItemTypeEnum.Category;
                                                                    ItemCat1.TypeSpecified = true;
                                                                    ParentCategory = itemname;
                                                                    try
                                                                    {
                                                                        ItemCat1 = dataService.Add<Item>(ItemCat1);
                                                                        if (ItemCat1.domain == "QBO")
                                                                        {
                                                                            itemvalue = ItemCat1.Id;
                                                                        }
                                                                    }
                                                                    catch { }
                                                                }
                                                                else
                                                                {
                                                                    ParentCategory = arr[i];
                                                                    foreach (var item1 in dataItem1)
                                                                    {
                                                                        itemvalue = item1.Id;
                                                                    }

                                                                }
                                                            }
                                                            if (dataItem1.Count == 0)
                                                            {
                                                                Intuit.Ipp.Data.Item ItemCat1 = new Intuit.Ipp.Data.Item();
                                                                ItemCat1.Active = true;
                                                                ItemCat1.ActiveSpecified = true;
                                                                ItemCat1.FullyQualifiedName = itemname;
                                                                ItemCat1.Name = itemname;
                                                                ItemCat1.Type = ItemTypeEnum.Category;
                                                                ItemCat1.TypeSpecified = true;
                                                                ParentCategory = itemname;
                                                                try
                                                                {
                                                                    ItemCat1 = dataService.Add<Item>(ItemCat1);
                                                                    if (ItemCat1.domain == "QBO")
                                                                    {
                                                                        itemvalue = ItemCat1.Id;
                                                                    }
                                                                }
                                                                catch { }
                                                            }

                                                        }

                                                    }
                                                    else if (i < arrCount - 1 && i != 0)
                                                    {
                                                        if (i == 1)
                                                        {
                                                            newVar1 = arr[0] + ":" + arr[1];
                                                            newVar11 = arr1[0] + ":" + arr1[1];

                                                            newVar2 = arr[0];
                                                            newVar22 = arr1[0];
                                                        }
                                                        if (i == 2)
                                                        {
                                                            newVar1 = arr[0] + ":" + arr[1] + ":" + arr[2];
                                                            newVar11 = arr1[0] + ":" + arr1[1] + ":" + arr1[2];

                                                            newVar2 = arr[0] + ":" + arr[1];
                                                            newVar22 = arr1[0] + ":" + arr1[1];

                                                        }
                                                        if (i == 3)
                                                        {
                                                            newVar1 = arr[0] + ":" + arr[1] + ":" + arr[2] + ":" + arr[3];
                                                            newVar11 = arr1[0] + ":" + arr1[1] + ":" + arr1[2] + ":" + arr1[3];

                                                            newVar2 = arr[0] + ":" + arr[1] + ":" + arr[2];
                                                            newVar22 = arr1[0] + ":" + arr1[1] + ":" + arr1[2];

                                                        }
                                                        dataItem1 = new ReadOnlyCollection<Intuit.Ipp.Data.Item>(ItemQueryService.ExecuteIdsQuery("Select * From Item where FullyQualifiedName ='" + newVar1 + "'"));
                                                        if (dataItem1.Count == 0)
                                                        {
                                                            ServiceContext serviceContext1 = await CommonUtilities.GetQBOServiceContext();
                                                            dataService = new DataService(serviceContext1);
                                                            Item ItemSubCategory = new Item();
                                                            ItemSubCategory.Active = true;
                                                            ItemSubCategory.Name = arr1[i];
                                                            ItemSubCategory.FullyQualifiedName = newVar11;
                                                            ItemSubCategory.Type = ItemTypeEnum.Category;
                                                            ItemSubCategory.TypeSpecified = true;
                                                            ItemSubCategory.Level = i;
                                                            ItemSubCategory.LevelSpecified = true;
                                                            ItemSubCategory.SubItem = true;
                                                            ItemSubCategory.SubItemSpecified = true;
                                                            ItemSubCategory.ParentRef = new ReferenceType()
                                                            {
                                                                name = ParentCategory,
                                                                Value = itemvalue,
                                                            };
                                                            ParentCategory = newVar11;
                                                            try
                                                            {
                                                                ItemSubCategory = dataService.Add<Item>(ItemSubCategory);
                                                                if (ItemSubCategory.domain == "QBO")
                                                                {
                                                                    itemvalue = ItemSubCategory.Id;
                                                                }
                                                            }
                                                            catch { }
                                                        }
                                                        else
                                                        {
                                                            foreach (var item1 in dataItem1)
                                                            {
                                                                if (item1.Type != ItemTypeEnum.Category)
                                                                {
                                                                    ServiceContext serviceContext1 = await CommonUtilities.GetQBOServiceContext();
                                                                    dataService = new DataService(serviceContext1);
                                                                    Item ItemSubCategory = new Item();
                                                                    ItemSubCategory.Active = true;
                                                                    ItemSubCategory.Name = arr1[i];
                                                                    ItemSubCategory.FullyQualifiedName = newVar11;
                                                                    ItemSubCategory.Type = ItemTypeEnum.Category;
                                                                    ItemSubCategory.TypeSpecified = true;
                                                                    ItemSubCategory.Level = i;
                                                                    ItemSubCategory.LevelSpecified = true;
                                                                    ItemSubCategory.SubItem = true;
                                                                    ItemSubCategory.SubItemSpecified = true;
                                                                    ItemSubCategory.ParentRef = new ReferenceType()
                                                                    {
                                                                        name = ParentCategory,
                                                                        Value = itemvalue,
                                                                    };
                                                                    ParentCategory = newVar11;
                                                                    try
                                                                    {
                                                                        ItemSubCategory = dataService.Add<Item>(ItemSubCategory);
                                                                        if (ItemSubCategory.domain == "QBO")
                                                                        {
                                                                            itemvalue = ItemSubCategory.Id;
                                                                        }
                                                                    }
                                                                    catch { }
                                                                }
                                                                else
                                                                {
                                                                    itemvalue = item1.Id;

                                                                }

                                                            }
                                                        }
                                                    }
                                                    i++;
                                                }

                                            }
                                        }
                                        //else if(ItemName.Contains(":") && CommonUtilities.GetInstance().ItemCategoriesFeature == false)
                                        //{
                                        //    CommonUtilities cUtility = new CommonUtilities();
                                        //    cUtility.CreateItemInQBO(dr["FullyQualifiedName"].ToString());
                                        //}
                                    }
                                }
                            }

                        }
                    }
                }
            }
            #endregion

            #endregion

            return coll;

        }

    }
}
