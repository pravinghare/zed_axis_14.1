﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using OnlineEntities;
namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineBillPaymenEntry
    {
        private static ImportOnlineBillPaymenEntry m_ImportOnlineBillPaymenEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineBillPaymenEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import bill pay
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineBillPaymenEntry GetInstance()
        {
            if (m_ImportOnlineBillPaymenEntry == null)
                m_ImportOnlineBillPaymenEntry = new ImportOnlineBillPaymenEntry();
            return m_ImportOnlineBillPaymenEntry;
        }
        /// setting values to billpayments data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlineBillPaymentQBEntryCollection ImportOnlineBillPaymentData(DataTable dt, ref string logDirectory)
        {

            OnlineDataProcessingsImportClass.OnlineBillPaymentQBEntryCollection coll = new OnlineBillPaymentQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;          
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                   
                    string datevalue = string.Empty;
                 
                    //Employee Validation
                    OnlineDataProcessingsImportClass.BillPayment billpayment = new OnlineDataProcessingsImportClass.BillPayment();
                    if (dt.Columns.Contains("DocNumber"))
                    {
                        billpayment = coll.FindPamentNumberEntry(dr["DocNumber"].ToString());

                        if (billpayment == null)
                        {

                            #region if billpayment is null

                            billpayment = new BillPayment();

                            if (dt.Columns.Contains("DocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["DocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["DocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.DocNumber = dr["DocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of TxnDate
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    billpayment.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    billpayment.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                billpayment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            billpayment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        billpayment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.DepartmentRef departmentRef = new OnlineEntities.DepartmentRef();
                            if (dt.Columns.Contains("DepartmentRef"))
                            {
                                #region Validations of VendorRefName
                                if (dr["DepartmentRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepartmentRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                departmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                departmentRef.Name = dr["DepartmentRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            departmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        departmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (departmentRef.Name != null)
                            {
                                billpayment.DepartmentRef.Add(departmentRef);
                            }

                            CurrencyRef CurrencyRef = new CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                            {
                                billpayment.CurrencyRef.Add(CurrencyRef);
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "ExchangeRate")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            OnlineEntities.Line payLine = new OnlineEntities.Line();

                            if (dt.Columns.Contains("LinePaymentAmount"))
                            {
                                #region Validations for LinePaymentAmount
                                if (dr["LinePaymentAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LinePaymentAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LinePaymentAmount( " + dr["LinePaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payLine.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LinePaymentAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            OnlineEntities.Linkedtxn LinkedTxn = new OnlineEntities.Linkedtxn();

                            if (dt.Columns.Contains("ApplytoBillRef"))
                            {
                                #region Validations of ApplytoBillRef
                                if (dr["ApplytoBillRef"].ToString() != string.Empty)
                                {
                                    if (dr["ApplytoBillRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ApplytoBillRef (" + dr["ApplytoBillRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (LinkedTxn.Applytobillref != null || payLine.LineAmount != null)
                            {
                                payLine.LinkedTxn.Add(LinkedTxn);
                                billpayment.Line.Add(payLine);
                            } 

                            #region discount line

                            OnlineEntities.DiscountLineDetail DiscountLineDetail = new OnlineEntities.DiscountLineDetail();
                            OnlineEntities.Line DiscountLine = new OnlineEntities.Line();

                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            if (dt.Columns.Contains("LineDiscountAmount"))
                            {
                                #region Validations for LineDiscountAmount
                                if (dr["LineDiscountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineDiscountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountAmount( " + dr["LineDiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountLineDetail.LineDiscountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineDiscountPercentBased"))
                            {
                                #region Validations of IsActive
                                if (dr["LineDiscountPercentBased"].ToString() != "<None>" || dr["LineDiscountPercentBased"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["LineDiscountPercentBased"].ToString(), out result))
                                    {
                                        DiscountLineDetail.PercentBased = Convert.ToInt32(dr["LineDiscountPercentBased"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["LineDiscountPercentBased"].ToString().ToLower() == "true")
                                        {
                                            DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["LineDiscountPercentBased"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "LineDiscountPercentBased";
                                            }
                                            else
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LineDiscountPercentBased(" + dr["LineDiscountPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineDiscountPercent"))
                            {
                                #region Validations for LineDiscountPercent
                                if (dr["LineDiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineDiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountPercent( " + dr["LineDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {

                                                DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();

                                            }
                                            else
                                            {
                                                DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountLineDetail.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountPercent"].ToString()));
                                        }
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.DiscountAccountRef DiscountAccountRef = new OnlineEntities.DiscountAccountRef();


                            if (dt.Columns.Contains("LineDiscountAccountRefName"))
                            {
                                #region Validations of LineDiscountAccountRefName
                                if (dr["LineDiscountAccountRefName"].ToString() != string.Empty)
                                {
                                    if (dr["LineDiscountAccountRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountAccountRefName (" + dr["LineDiscountAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (DiscountAccountRef.Name != null)
                            {
                                DiscountLineDetail.DiscountAccountRef.Add(DiscountAccountRef);
                            }
                           

                            if ( DiscountLineDetail.DiscountAccountRef.Count != 0 || DiscountLineDetail.DiscountPercent != null || DiscountLineDetail.LineDiscountAmount != null || DiscountLineDetail.PercentBased != null)
                            {
                                DiscountLine.DiscountLineDetail.Add(DiscountLineDetail);
                                billpayment.Line.Add(DiscountLine);                                
                            }     

                            #endregion                                                                              

                           OnlineEntities.VendorRef vendorRef = new OnlineEntities.VendorRef();
                            if (dt.Columns.Contains("VendorRef"))
                            {
                                #region Validations of VendorRefName
                                if (dr["VendorRef"].ToString() != string.Empty)
                                {
                                    if (dr["VendorRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorRef (" + dr["VendorRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorRef.Name = dr["VendorRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorRef.Name = dr["VendorRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            vendorRef.Name = dr["VendorRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorRef.Name = dr["VendorRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (vendorRef.Name != null)
                            {
                                billpayment.vendorRef.Add(vendorRef);
                            }


                            OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                            if (dt.Columns.Contains("APAccountRef"))
                            {
                                #region Validations of APAccountRefName
                                if (dr["APAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["APAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This APAccountRef (" + dr["APAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                APAccountRef.Name = dr["APAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                APAccountRef.Name = dr["APAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            APAccountRef.Name = dr["APAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (APAccountRef.Name != null)
                            {
                                billpayment.APAccountRef.Add(APAccountRef);
                            }
                           

                            if (dt.Columns.Contains("PayType"))
                            {
                                #region Validations of PayType
                                if (dr["PayType"].ToString() != string.Empty)
                                {
                                    if (dr["PayType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PayType (" + dr["PayType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.PayType = dr["PayType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.PayType = dr["PayType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.PayType = dr["PayType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.PayType = dr["PayType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.CheckPayment chkPayment = new OnlineEntities.CheckPayment();

                            OnlineEntities.BankAccountRef bankAccountRef = new OnlineEntities.BankAccountRef();
                            if (dt.Columns.Contains("BankAccountRef"))
                            {
                                #region Validations of bankAccountRef
                                if (dr["BankAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["BankAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BankAccountRef (" + dr["BankAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bankAccountRef.Name = dr["BankAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            } 
                           
                            if (dt.Columns.Contains("Printstatus"))
                            {
                                #region Validations of Printstatus
                                if (dr["Printstatus"].ToString() != string.Empty)
                                {
                                    if (dr["Printstatus"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Printstatus (" + dr["Printstatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                chkPayment.Printstatus = dr["Printstatus"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                chkPayment.Printstatus = dr["Printstatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            chkPayment.Printstatus = dr["Printstatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        chkPayment.Printstatus = dr["Printstatus"].ToString();
                                    }
                                }
                                if (bankAccountRef.Name != string.Empty)
                                {
                                    chkPayment.BankAccountRef.Add(bankAccountRef);
                                    billpayment.CheckPaymentRef.Add(chkPayment);

                                }
                                #endregion
                            }
                            if (bankAccountRef.Name != null)
                            {
                                chkPayment.BankAccountRef.Add(bankAccountRef);
                                billpayment.CheckPaymentRef.Add(chkPayment);
                            }

                            OnlineEntities.CreditCardPayment creditCardPayment = new OnlineEntities.CreditCardPayment();

                            OnlineEntities.CCAccountRef CCAccountRef = new OnlineEntities.CCAccountRef();
                            if (dt.Columns.Contains("CCAccountRef"))
                            {
                                #region Validations of CCAccountRef
                                if (dr["CCAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["CCAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CCAccountRef (" + dr["CCAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CCAccountRef.Name = dr["CCAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CCAccountRef.Name != null)
                            {
                                creditCardPayment.CCAccountRef.Add(CCAccountRef);
                                billpayment.CreditCardPaymentRef.Add(creditCardPayment);
                            }
                           
                            if (dt.Columns.Contains("Totalamt"))
                            {
                                #region Validations for Totalamt
                                if (dr["Totalamt"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Totalamt"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Totalamt( " + dr["Totalamt"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.TotalAmt = dr["Totalamt"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.TotalAmt = dr["Totalamt"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.TotalAmt = dr["Totalamt"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.TotalAmt = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Totalamt"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("ProcessBillPayment"))
                            {
                                #region Validations of PrivateNote
                                if (dr["ProcessBillPayment"].ToString() != string.Empty)
                                {
                                    if (dr["ProcessBillPayment"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ProcessBillPayment (" + dr["ProcessBillPayment"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.ProcessBillPayment = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }
                            coll.Add(billpayment);
    
                            #endregion
                        }
                        else
                        {
                            #region if billpayment is not null                        
                           
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of TxnDate
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    billpayment.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    billpayment.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                billpayment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            billpayment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        billpayment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.DepartmentRef departmentRef = new OnlineEntities.DepartmentRef();
                            if (dt.Columns.Contains("DepartmentRef"))
                            {
                                #region Validations of VendorRefName
                                if (dr["DepartmentRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepartmentRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                departmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                departmentRef.Name = dr["DepartmentRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            departmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        departmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (departmentRef.Name != null)
                            {
                                billpayment.DepartmentRef.Add(departmentRef);
                            }

                            CurrencyRef CurrencyRef = new CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                            {
                                billpayment.CurrencyRef.Add(CurrencyRef);
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "ExchangeRate")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            OnlineEntities.Line payLine = new OnlineEntities.Line();

                            if (dt.Columns.Contains("LinePaymentAmount"))
                            {
                                #region Validations for LinePaymentAmount
                                if (dr["LinePaymentAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LinePaymentAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LinePaymentAmount( " + dr["LinePaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payLine.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LinePaymentAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            OnlineEntities.Linkedtxn LinkedTxn = new OnlineEntities.Linkedtxn();

                            if (dt.Columns.Contains("ApplytoBillRef"))
                            {
                                #region Validations of ApplytoBillRef
                                if (dr["ApplytoBillRef"].ToString() != string.Empty)
                                {
                                    if (dr["ApplytoBillRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ApplytoBillRef (" + dr["ApplytoBillRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (LinkedTxn.Applytobillref != null || payLine.LineAmount != null)
                            {
                                payLine.LinkedTxn.Add(LinkedTxn);
                                billpayment.Line.Add(payLine);
                            }   

                            OnlineEntities.VendorRef vendorRef = new OnlineEntities.VendorRef();
                            if (dt.Columns.Contains("VendorRefName"))
                            {
                                #region Validations of VendorRefName
                                if (dr["VendorRefName"].ToString() != string.Empty)
                                {
                                    if (dr["VendorRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorRefName (" + dr["VendorRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorRef.Name = dr["VendorRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorRef.Name = dr["VendorRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            vendorRef.Name = dr["VendorRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorRef.Name = dr["VendorRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (vendorRef.Name != null)
                            {
                                billpayment.vendorRef.Add(vendorRef);
                            }
                            
                            OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                            if (dt.Columns.Contains("APAccountRefName"))
                            {
                                #region Validations of APAccountRefName
                                if (dr["APAccountRefName"].ToString() != string.Empty)
                                {
                                    if (dr["APAccountRefName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This APAccountRefName (" + dr["APAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                APAccountRef.Name = dr["APAccountRefName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                APAccountRef.Name = dr["APAccountRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            APAccountRef.Name = dr["APAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (APAccountRef.Name != null)
                            {
                                billpayment.APAccountRef.Add(APAccountRef);
                            }
                            if (dt.Columns.Contains("PayType"))
                            {
                                #region Validations of PayType
                                if (dr["PayType"].ToString() != string.Empty)
                                {
                                    if (dr["PayType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PayType (" + dr["PayType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.PayType = dr["PayType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.PayType = dr["PayType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.PayType = dr["PayType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.PayType = dr["PayType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            
                            OnlineEntities.CheckPayment chkPayment = new OnlineEntities.CheckPayment();

                            OnlineEntities.BankAccountRef bankAccountRef = new OnlineEntities.BankAccountRef();
                            if (dt.Columns.Contains("BankAccountRef"))
                            {
                                #region Validations of bankAccountRef
                                if (dr["BankAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["BankAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BankAccountRef (" + dr["BankAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bankAccountRef.Name = dr["BankAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Printstatus"))
                            {
                                #region Validations of Printstatus
                                if (dr["Printstatus"].ToString() != string.Empty)
                                {
                                    if (dr["Printstatus"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Printstatus (" + dr["Printstatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                chkPayment.Printstatus = dr["Printstatus"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                chkPayment.Printstatus = dr["Printstatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            chkPayment.Printstatus = dr["Printstatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        chkPayment.Printstatus = dr["Printstatus"].ToString();
                                    }
                                }
                                if (bankAccountRef.Name != string.Empty)
                                {
                                    chkPayment.BankAccountRef.Add(bankAccountRef);
                                    billpayment.CheckPaymentRef.Add(chkPayment);

                                }
                                #endregion
                            }
                            if (bankAccountRef.Name != null)
                            {
                                chkPayment.BankAccountRef.Add(bankAccountRef);
                                billpayment.CheckPaymentRef.Add(chkPayment);
                            }

                            OnlineEntities.CreditCardPayment creditCardPayment = new OnlineEntities.CreditCardPayment();

                            OnlineEntities.CCAccountRef CCAccountRef = new OnlineEntities.CCAccountRef();
                            if (dt.Columns.Contains("CCAccountRef"))
                            {
                                #region Validations of CCAccountRef
                                if (dr["CCAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["CCAccountRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CCAccountRef (" + dr["CCAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CCAccountRef.Name = dr["CCAccountRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CCAccountRef.Name != null)
                            {
                                creditCardPayment.CCAccountRef.Add(CCAccountRef);
                                billpayment.CreditCardPaymentRef.Add(creditCardPayment);
                            }


                            if (dt.Columns.Contains("Totalamt"))
                            {
                                #region Validations for Totalamt
                                if (dr["Totalamt"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Totalamt"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Totalamt( " + dr["Totalamt"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.TotalAmt = dr["Totalamt"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.TotalAmt = dr["Totalamt"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.TotalAmt = dr["Totalamt"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.TotalAmt = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Totalamt"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("ProcessBillPayment"))
                            {
                                #region Validations of PrivateNote
                                if (dr["ProcessBillPayment"].ToString() != string.Empty)
                                {
                                    if (dr["ProcessBillPayment"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ProcessBillPayment (" + dr["ProcessBillPayment"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.ProcessBillPayment = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }                        
                            #endregion

                        }

                    }
                    else
                    {                      
                        #region No Doc no.
                        billpayment = new BillPayment();


                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of TxnDate
                            DateTime SODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                billpayment.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                billpayment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            billpayment.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        billpayment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    billpayment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billpayment.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    billpayment.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.DepartmentRef departmentRef = new OnlineEntities.DepartmentRef();
                        if (dt.Columns.Contains("DepartmentRef"))
                        {
                            #region Validations of VendorRefName
                            if (dr["DepartmentRef"].ToString() != string.Empty)
                            {
                                if (dr["DepartmentRef"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            departmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            departmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        departmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                else
                                {
                                    departmentRef.Name = dr["DepartmentRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (departmentRef.Name != null)
                        {
                            billpayment.DepartmentRef.Add(departmentRef);
                        }

                        CurrencyRef CurrencyRef = new CurrencyRef();

                        if (dt.Columns.Contains("CurrencyRef"))
                        {
                            #region Validations of CurrencyRef
                            if (dr["CurrencyRef"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CurrencyRef.Name != null)
                        {
                            billpayment.CurrencyRef.Add(CurrencyRef);
                        }

                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "ExchangeRate")
                                        {
                                            isIgnoreAll = true;
                                            billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    billpayment.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                }
                            }

                            #endregion
                        }

                        OnlineEntities.Line payLine = new OnlineEntities.Line();

                        if (dt.Columns.Contains("LinePaymentAmount"))
                        {
                            #region Validations for LinePaymentAmount
                            if (dr["LinePaymentAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LinePaymentAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LinePaymentAmount( " + dr["LinePaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        payLine.LineAmount = dr["LinePaymentAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    payLine.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LinePaymentAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        OnlineEntities.Linkedtxn LinkedTxn = new OnlineEntities.Linkedtxn();

                        if (dt.Columns.Contains("ApplytoBillRef"))
                        {
                            #region Validations of ApplytoBillRef
                            if (dr["ApplytoBillRef"].ToString() != string.Empty)
                            {
                                if (dr["ApplytoBillRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ApplytoBillRef (" + dr["ApplytoBillRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                    }
                                }
                                else
                                {
                                    LinkedTxn.Applytobillref = dr["ApplytoBillRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (LinkedTxn.Applytobillref != null || payLine.LineAmount != null)
                        {
                            payLine.LinkedTxn.Add(LinkedTxn);
                            billpayment.Line.Add(payLine);
                        }                    
                        
                        #region discount line

                        OnlineEntities.DiscountLineDetail DiscountLineDetail = new OnlineEntities.DiscountLineDetail();
                        OnlineEntities.Line DiscountLine = new OnlineEntities.Line();
                        OnlineEntities.Line Line = new OnlineEntities.Line();
                        if (dt.Columns.Contains("LineDiscountAmount"))
                        {
                            #region Validations for LineDiscountAmount
                            if (dr["LineDiscountAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineDiscountAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDiscountAmount( " + dr["LineDiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountLineDetail.LineDiscountAmount = dr["LineDiscountAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    DiscountLineDetail.LineDiscountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LineDiscountPercentBased"))
                        {
                            #region Validations of IsActive
                            if (dr["LineDiscountPercentBased"].ToString() != "<None>" || dr["LineDiscountPercentBased"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["LineDiscountPercentBased"].ToString(), out result))
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToInt32(dr["LineDiscountPercentBased"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["LineDiscountPercentBased"].ToString().ToLower() == "true")
                                    {
                                        DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["LineDiscountPercentBased"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "LineDiscountPercentBased";
                                        }
                                        else
                                            DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDiscountPercentBased(" + dr["LineDiscountPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DiscountLineDetail.PercentBased = dr["LineDiscountPercentBased"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineDiscountPercent"))
                        {
                            #region Validations for LineDiscountPercent
                            if (dr["LineDiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineDiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDiscountPercent( " + dr["LineDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {

                                            DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();

                                        }
                                        else
                                        {
                                            DiscountLineDetail.DiscountPercent = dr["LineDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountLineDetail.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineDiscountPercent"].ToString()));
                                    }
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.DiscountAccountRef DiscountAccountRef = new OnlineEntities.DiscountAccountRef();


                        if (dt.Columns.Contains("LineDiscountAccountRefName"))
                        {
                            #region Validations of LineDiscountAccountRefName
                            if (dr["LineDiscountAccountRefName"].ToString() != string.Empty)
                            {
                                if (dr["LineDiscountAccountRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDiscountAccountRefName (" + dr["LineDiscountAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    DiscountAccountRef.Name = dr["LineDiscountAccountRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (DiscountAccountRef.Name != null)
                        {
                            DiscountLineDetail.DiscountAccountRef.Add(DiscountAccountRef);
                        }
                        
                       
                        if (DiscountLineDetail.DiscountAccountRef.Count != 0 || DiscountLineDetail.DiscountPercent != null || DiscountLineDetail.LineDiscountAmount != null || DiscountLineDetail.PercentBased != null)
                        {
                            DiscountLine.DiscountLineDetail.Add(DiscountLineDetail);
                            billpayment.Line.Add(DiscountLine);
                        }
                        #endregion

                       
                        OnlineEntities.VendorRef vendorRef = new OnlineEntities.VendorRef();
                        if (dt.Columns.Contains("VendorRefName"))
                        {
                            #region Validations of VendorRefName
                            if (dr["VendorRefName"].ToString() != string.Empty)
                            {
                                if (dr["VendorRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorRefName (" + dr["VendorRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            vendorRef.Name = dr["VendorRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vendorRef.Name = dr["VendorRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorRef.Name = dr["VendorRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorRef.Name = dr["VendorRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (vendorRef.Name != null)
                        {
                            billpayment.vendorRef.Add(vendorRef);
                        }


                        OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                        if (dt.Columns.Contains("APAccountRefName"))
                        {
                            #region Validations of APAccountRefName
                            if (dr["APAccountRefName"].ToString() != string.Empty)
                            {
                                if (dr["APAccountRefName"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This APAccountRefName (" + dr["APAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            APAccountRef.Name = dr["APAccountRefName"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            APAccountRef.Name = dr["APAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    APAccountRef.Name = dr["APAccountRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (APAccountRef.Name != null)
                        {
                            billpayment.APAccountRef.Add(APAccountRef);
                        }

                       

                        if (dt.Columns.Contains("PayType"))
                        {
                            #region Validations of PayType
                            if (dr["PayType"].ToString() != string.Empty)
                            {
                                if (dr["PayType"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PayType (" + dr["PayType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billpayment.PayType = dr["PayType"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billpayment.PayType = dr["PayType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.PayType = dr["PayType"].ToString();
                                    }
                                }
                                else
                                {
                                    billpayment.PayType = dr["PayType"].ToString();
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.CheckPayment chkPayment = new OnlineEntities.CheckPayment();

                        OnlineEntities.BankAccountRef bankAccountRef = new OnlineEntities.BankAccountRef();
                        if (dt.Columns.Contains("BankAccountRef"))
                        {
                            #region Validations of bankAccountRef
                            if (dr["BankAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["BankAccountRef"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BankAccountRef (" + dr["BankAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bankAccountRef.Name = dr["BankAccountRef"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    bankAccountRef.Name = dr["BankAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        
                       
                        if (dt.Columns.Contains("Printstatus"))
                        {
                            #region Validations of Printstatus
                            if (dr["Printstatus"].ToString() != string.Empty)
                            {
                                if (dr["Printstatus"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Printstatus (" + dr["Printstatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            chkPayment.Printstatus = dr["Printstatus"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            chkPayment.Printstatus = dr["Printstatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        chkPayment.Printstatus = dr["Printstatus"].ToString();
                                    }
                                }
                                else
                                {
                                    chkPayment.Printstatus = dr["Printstatus"].ToString();
                                }
                            }
                            if (bankAccountRef.Name != string.Empty)
                            {
                                chkPayment.BankAccountRef.Add(bankAccountRef);
                                billpayment.CheckPaymentRef.Add(chkPayment);

                            }
                            #endregion
                        }
                        if (bankAccountRef.Name != null)
                        {
                            chkPayment.BankAccountRef.Add(bankAccountRef);
                            billpayment.CheckPaymentRef.Add(chkPayment);
                        }

                        OnlineEntities.CreditCardPayment creditCardPayment = new OnlineEntities.CreditCardPayment();

                        OnlineEntities.CCAccountRef CCAccountRef = new OnlineEntities.CCAccountRef();
                        if (dt.Columns.Contains("CCAccountRef"))
                        {
                            #region Validations of CCAccountRef
                            if (dr["CCAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["CCAccountRef"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CCAccountRef (" + dr["CCAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CCAccountRef.Name = dr["CCAccountRef"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CCAccountRef.Name = dr["CCAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (CCAccountRef.Name != null)
                        {
                            creditCardPayment.CCAccountRef.Add(CCAccountRef);
                            billpayment.CreditCardPaymentRef.Add(creditCardPayment);
                        }
                       

                        if (dt.Columns.Contains("Totalamt"))
                        {
                            #region Validations for Totalamt
                            if (dr["Totalamt"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Totalamt"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Totalamt( " + dr["Totalamt"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billpayment.TotalAmt = dr["Totalamt"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billpayment.TotalAmt = dr["Totalamt"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.TotalAmt = dr["Totalamt"].ToString();
                                    }
                                }
                                else
                                {
                                    billpayment.TotalAmt = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Totalamt"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("ProcessBillPayment"))
                        {
                            #region Validations of PrivateNote
                            if (dr["ProcessBillPayment"].ToString() != string.Empty)
                            {
                                if (dr["ProcessBillPayment"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ProcessBillPayment (" + dr["ProcessBillPayment"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            billpayment.ProcessBillPayment = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    billpayment.ProcessBillPayment = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }
                        coll.Add(billpayment);
                        #endregion                       
                    }
                }


                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;
            
        }
    }
}

