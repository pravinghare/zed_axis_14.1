﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System.Windows.Forms;
using System.Net;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class OnlinePurchaseOrderEntry
    {
        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TxnDate;
        private Collection<OnlineEntities.CustomField> m_CustomField = new Collection<OnlineEntities.CustomField>();
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.VendorRef> m_VendorRef = new Collection<OnlineEntities.VendorRef>();
        private Collection<OnlineEntities.APAccountRef> m_APAccountRef = new Collection<OnlineEntities.APAccountRef>();
        private string m_TotalAmt;
        private Collection<OnlineEntities.ClassRef> m_ClassRef = new Collection<OnlineEntities.ClassRef>();
        private Collection<OnlineEntities.SalesTermRef> m_SalesTermRef = new Collection<OnlineEntities.SalesTermRef>();
        private string m_DueDate;
        private Collection<OnlineEntities.VendorAddr> m_VendorAddr = new Collection<OnlineEntities.VendorAddr>();
        private Collection<OnlineEntities.ShipAddr> m_ShipAddr = new Collection<OnlineEntities.ShipAddr>();
        private Collection<OnlineEntities.ShipMethodRef> m_ShipMethodRef = new Collection<OnlineEntities.ShipMethodRef>();
        private string m_POStatus;
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        private string m_ExchangeRate;
        private string m_GlobalTaxCalculation;

        //P Axis 13.1 : issue 687
        private string m_POEmail;
        //594
        private bool m_isAppend;

        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public Collection<OnlineEntities.CustomField> CustomField
        {
            get { return m_CustomField; }
            set { m_CustomField = value; }
        }

        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        public Collection<OnlineEntities.VendorRef> VendorRef
        {
            get { return m_VendorRef; }
            set { m_VendorRef = value; }
        }

        public Collection<OnlineEntities.APAccountRef> APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }

        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }

        public Collection<OnlineEntities.ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public Collection<OnlineEntities.SalesTermRef> SalesTermRef
        {
            get { return m_SalesTermRef; }
            set { m_SalesTermRef = value; }
        }

        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }

        public Collection<OnlineEntities.VendorAddr> VendorAddr
        {
            get { return m_VendorAddr; }
            set { m_VendorAddr = value; }
        }

        public Collection<OnlineEntities.ShipAddr> ShipAddr
        {
            get { return m_ShipAddr; }
            set { m_ShipAddr = value; }
        }

        public Collection<OnlineEntities.ShipMethodRef> ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }

        public string POStatus
        {
            get { return m_POStatus; }
            set { m_POStatus = value; }
        }

        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        //P Axis 13.1 : issue 687
        public string POEmail
        {
            get { return m_POEmail; }
            set { m_POEmail = value; }
        }
        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }
        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        /// <summary>
        /// Creating new PO transaction in quickbook online. 
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOPurchaseOrder(OnlineDataProcessingsImportClass.OnlinePurchaseOrderEntry coll)
        {
            bool flag = false;
            int linecount = 1;

            DataService dataService = null;
            Intuit.Ipp.Data.PurchaseOrder PurchaseOrderAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            Intuit.Ipp.Data.PurchaseOrder addedPurchaseOrder = new Intuit.Ipp.Data.PurchaseOrder();
            OnlinePurchaseOrderEntry purchaseOrder = new OnlinePurchaseOrderEntry();
            purchaseOrder = coll;
            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;

            //bug 514
            bool taxFlag = false;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {
                #region Add Purchase Order

                //594
                if (purchaseOrder.isAppend == true)
                {
                    addedPurchaseOrder.sparse = true;
                }

                if (purchaseOrder.DocNumber != null)
                {
                    addedPurchaseOrder.DocNumber = purchaseOrder.DocNumber;
                }

                if (purchaseOrder.TxnDate != null)
                {
                    try
                    {
                        addedPurchaseOrder.TxnDate = Convert.ToDateTime(purchaseOrder.TxnDate);
                        addedPurchaseOrder.TxnDateSpecified = true;
                    }
                    catch { }
                }

                addedPurchaseOrder.PrivateNote = purchaseOrder.PrivateNote;

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in purchaseOrder.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addedPurchaseOrder.CustomField = custom_online;
                #endregion

                #region GlobalTaxCalculation

                if (purchaseOrder.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedPurchaseOrder.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedPurchaseOrder.GlobalTaxCalculationSpecified = true;
                        }
                        else if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedPurchaseOrder.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedPurchaseOrder.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedPurchaseOrder.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedPurchaseOrder.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion
                foreach (var l in purchaseOrder.Line)
                {
                    if (flag == false)
                    {
                        #region
                        flag = true;
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBased in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBased != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                string displayname = string.Empty;

                                foreach (var i in ItemBased.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }

                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }

                                    }
                                }

                                foreach (var i in ItemBased.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBased.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }



                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBased.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBased.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBased.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBased.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBased.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBased.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBased.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBased.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBased.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                foreach (var taxCoderef in ItemBased.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #region BillableStatus
                                if (ItemBased.BillableStatus != null)
                                {
                                    if (ItemBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (ItemBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (ItemBased.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var account in l.AccountBasedExpenseLineDetail)
                            {

                                if (account != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (account.Description != null)
                                {
                                    Line.Description = account.Description;
                                }
                                if (account.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in account.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount =Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in account.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                        
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in account.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in account.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                foreach (var taxCoderef in account.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(account.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(account.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(account.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                           
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (account.BillableStatus != null)
                                {
                                    if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchaseOrder.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        linecount++;

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var s in l.ItemBasedExpenseLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                string displayname = string.Empty;

                                // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{

                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }

                                foreach (var i in s.ClassRef)
                                {
                                    displayname = i.Name;
                                    string id = string.Empty;
                                    if (displayname != string.Empty)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(displayname.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in s.CustomerRef)
                                {
                                    displayname = i.Name;
                                    string id = string.Empty;
                                    if (displayname != string.Empty)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(displayname.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (s.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(s.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }


                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (s.BillableStatus != null)
                                {
                                    if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var account in l.AccountBasedExpenseLineDetail)
                            {
                                if (account != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (account.Description != null)
                                {
                                    Line.Description = account.Description;
                                }
                                if (account.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in account.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                       
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in account.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                        
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in account.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in account.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                  
                                }
                                foreach (var taxCoderef in account.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(account.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(account.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(account.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (account.BillableStatus != null)
                                {
                                    if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchaseOrder.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedPurchaseOrder.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && purchaseOrder.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {
                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = taxVal;
                                    taxLineDetail.TaxPercentSpecified = true;
                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedPurchaseOrder.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //586
                if (CommonUtilities.GetInstance().CountryVersion == "US") //|| CommonUtilities.GetInstance().GrossNet == false || addedPurchaseOrder.GlobalTaxCalculation.ToString() != GlobalTaxCalculationEnum.TaxInclusive.ToString())
                {

                    foreach (var txntaxCode in purchaseOrder.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                    
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }


                //        foreach (var txnline in txntaxCode.TaxLine)
                //        {
                //            if (txnline != null)
                //            {
                //                taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                //                taxLine.DetailTypeSpecified = true;
                //            }

                //            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();

                //            foreach (var taxlineDetail in txnline.TaxLineDetail)
                //            {
                //                if (dataTax != null)
                //                {
                //                    foreach (var taxRateRef in dataTax)
                //                    {
                //                        foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                //                        {
                //                            taxLineDetail.TaxRateRef = taxRate.TaxRateRef;
                //                        }
                //                    }
                //                }
                //                if (taxlineDetail.PercentBased != null)
                //                {
                //                    taxLineDetail.PercentBased = Convert.ToBoolean(taxlineDetail.PercentBased);
                //                    taxLineDetail.PercentBasedSpecified = true;
                //                }
                //                if (taxlineDetail.TaxPercent != null)
                //                {
                //                    taxLineDetail.TaxPercent = Convert.ToDecimal(taxlineDetail.TaxPercent);
                //                    taxLineDetail.TaxPercentSpecified = true;
                //                }
                //            }

                //            taxLine.AnyIntuitObject = taxLineDetail;
                //        }
                //        if (taxLine != null)
                //        {
                //            txnTaxDetail.TaxLine = new Intuit.Ipp.Data.Line[] { taxLine };
                //        }

                //        txnTaxDetail.TotalTax = Convert.ToDecimal(txntaxCode.TotalTax);

                        if (txnTaxDetail != null)
                        {
                            addedPurchaseOrder.TxnTaxDetail = txnTaxDetail;
                        }

                        #endregion
                    }

                }


                #endregion

                foreach (var vendorRef in purchaseOrder.VendorRef)
                {
                    string Vendorvalue = string.Empty;
                    if (vendorRef.Name != null)
                    {
                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(vendorRef.Name.Trim());
                       
                        foreach (var customer in dataVendor)
                        {
                            Vendorvalue = customer.Id;
                        }
                        string displayName = vendorRef.Name;

                        addedPurchaseOrder.VendorRef = new ReferenceType()
                        {
                            name = vendorRef.Name,
                            Value = Vendorvalue
                        };
                        
                    }

                }
                //P Axis 13.1 : issue 687
                if (purchaseOrder.POEmail != null)
                {
                    EmailAddress email = new EmailAddress();
                    email.Address = purchaseOrder.POEmail;
                    addedPurchaseOrder.POEmail = email;
                }
                foreach (var APAccountRef in purchaseOrder.APAccountRef)
                {
                    if (APAccountRef.Name != null && APAccountRef.Name != string.Empty && APAccountRef.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(APAccountRef.Name);
                        addedPurchaseOrder.APAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                   
                }
                foreach (var className in purchaseOrder.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedPurchaseOrder.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }

                foreach (var className in purchaseOrder.SalesTermRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(className.Name.Trim());
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedPurchaseOrder.SalesTermRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }


                if (purchaseOrder.DueDate != null)
                {
                    try
                    {
                        addedPurchaseOrder.DueDate = Convert.ToDateTime(purchaseOrder.DueDate);
                        addedPurchaseOrder.DueDateSpecified = true;
                    }
                    catch { }
                }

                PhysicalAddress vendorAddr = new PhysicalAddress();
                if (purchaseOrder.VendorAddr.Count > 0)
                {
                    foreach (var vendorAddress in purchaseOrder.VendorAddr)
                    {
                        vendorAddr.Line1 = vendorAddress.VendorLine1;
                        vendorAddr.Line2 = vendorAddress.VendorLine2;
                        vendorAddr.Line3 = vendorAddress.VendorLine3;
                        vendorAddr.City = vendorAddress.VendorCity;
                        vendorAddr.Country = vendorAddress.VendorCountry;
                        vendorAddr.CountrySubDivisionCode = vendorAddress.VendorCountrySubDivisionCode;
                        vendorAddr.PostalCode = vendorAddress.VendorPostalCode;
                        vendorAddr.Note = vendorAddress.VendorNote;
                        vendorAddr.Line4 = vendorAddress.VendorLine4;
                        vendorAddr.Line5 = vendorAddress.VendorLine5;
                        vendorAddr.Lat = vendorAddress.VendorAddrLat;
                        vendorAddr.Long = vendorAddress.VendorAddrLong;
                    }

                    addedPurchaseOrder.VendorAddr = vendorAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (purchaseOrder.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in purchaseOrder.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedPurchaseOrder.ShipAddr = shipAddr;
                }

                if (purchaseOrder.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in purchaseOrder.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedPurchaseOrder.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (purchaseOrder.POStatus != null)
                {
                    if (purchaseOrder.POStatus == PurchaseOrderStatusEnum.Closed.ToString())
                    {
                        addedPurchaseOrder.POStatus = PurchaseOrderStatusEnum.Closed;
                        addedPurchaseOrder.POStatusSpecified = true;
                    }
                    else if (purchaseOrder.POStatus == PurchaseOrderStatusEnum.Open.ToString())
                    {
                        addedPurchaseOrder.POStatus = PurchaseOrderStatusEnum.Open;
                        addedPurchaseOrder.POStatusSpecified = true;
                    }
                }

                if (purchaseOrder.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in purchaseOrder.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    addedPurchaseOrder.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (purchaseOrder.ExchangeRate != null)
                {
                    addedPurchaseOrder.ExchangeRate = Convert.ToDecimal(purchaseOrder.ExchangeRate);
                    addedPurchaseOrder.ExchangeRateSpecified = true;
                }




                #endregion
                CommonUtilities.GetInstance().newSKUItem = true;
                PurchaseOrderAdded = new Intuit.Ipp.Data.PurchaseOrder();
                PurchaseOrderAdded = dataService.Add<Intuit.Ipp.Data.PurchaseOrder>(addedPurchaseOrder);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (PurchaseOrderAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = PurchaseOrderAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = PurchaseOrderAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        /// <summary>
        /// updating PO entry in quickbook online by the transaction Id
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="salesReceiptid"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOPurchaseOrder(OnlineDataProcessingsImportClass.OnlinePurchaseOrderEntry coll, string salesReceiptid, string syncToken, Intuit.Ipp.Data.PurchaseOrder PreviousData = null)
        {
            bool flag = false;
            int linecount = 1;

            DataService dataService = null;
            Intuit.Ipp.Data.PurchaseOrder PurchaseOrderAdded = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.PurchaseOrder addedPurchaseOrder = new Intuit.Ipp.Data.PurchaseOrder();
            OnlinePurchaseOrderEntry purchaseOrder = new OnlinePurchaseOrderEntry();
            purchaseOrder = coll;
            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            //bug 514
            bool taxFlag = false;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
           
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {
               
                #region Add Purchase Order

                //594
                if (purchaseOrder.isAppend == true)
                {
                    addedPurchaseOrder = PreviousData;
                    addedPurchaseOrder.sparse = true;
                    addedPurchaseOrder.sparseSpecified = true;
                    if (addedPurchaseOrder.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedPurchaseOrder.Line.Length;
                        lines_online = addedPurchaseOrder.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                }

                addedPurchaseOrder.Id = salesReceiptid;
                addedPurchaseOrder.SyncToken = syncToken;
                if (purchaseOrder.DocNumber != null)
                {
                    addedPurchaseOrder.DocNumber = purchaseOrder.DocNumber;
                }

                if (purchaseOrder.TxnDate != null)
                {
                    try
                    {
                        addedPurchaseOrder.TxnDate = Convert.ToDateTime(purchaseOrder.TxnDate);
                        addedPurchaseOrder.TxnDateSpecified = true;
                    }
                    catch { }
                }

                addedPurchaseOrder.PrivateNote = purchaseOrder.PrivateNote;

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in purchaseOrder.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addedPurchaseOrder.CustomField = custom_online;
                #endregion

                #region GlobalTaxCalculation

                if (purchaseOrder.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedPurchaseOrder.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedPurchaseOrder.GlobalTaxCalculationSpecified = true;
                        }
                        else if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedPurchaseOrder.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedPurchaseOrder.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedPurchaseOrder.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedPurchaseOrder.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion
                foreach (var l in purchaseOrder.Line)
                {
                    if (flag == false)
                    {
                        #region
                        flag = true;
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var s in l.ItemBasedExpenseLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                string displayname = string.Empty;

                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }

                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }

                                    }
                                }

                                foreach (var i in s.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in s.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }



                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (s.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(s.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                #region BillableStatus
                                if (s.BillableStatus != null)
                                {
                                    if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var account in l.AccountBasedExpenseLineDetail)
                            {
                                if (account != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (account.Description != null)
                                {
                                    Line.Description = account.Description;
                                }
                                if (account.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in account.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount =Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in account.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in account.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                        
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in account.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                foreach (var taxCoderef in account.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(account.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(account.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(account.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (account.BillableStatus != null)
                                {
                                    if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchaseOrder.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        linecount++;

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var s in l.ItemBasedExpenseLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                string displayname = string.Empty;

                                // bug 486 Axis 12.0
                                //if (CommonUtilities.GetInstance().SKULookup == false)
                                //{

                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                           
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }

                                        // CommonUtilities.GetInstance().newSKUItem = false;
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }
                                }

                                foreach (var i in s.ClassRef)
                                {
                                    displayname = i.Name;
                                    string id = string.Empty;
                                    if (displayname != string.Empty)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(displayname.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in s.CustomerRef)
                                {
                                    displayname = i.Name;
                                    string id = string.Empty;
                                    if (displayname != string.Empty)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(displayname.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = displayname,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                               
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (s.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(s.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }


                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (s.BillableStatus != null)
                                {
                                    if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (s.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var account in l.AccountBasedExpenseLineDetail)
                            {
                                if (account != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (account.Description != null)
                                {
                                    Line.Description = account.Description;
                                }
                                if (account.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (purchaseOrder.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in account.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(account.AccountAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(account.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in account.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in account.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in account.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                foreach (var taxCoderef in account.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(account.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(account.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(account.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (account.BillableStatus != null)
                                {
                                    if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                    else if (account.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                        AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                    }
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedPurchaseOrder.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }


                foreach (var vendorRef in purchaseOrder.VendorRef)
                {
                    string Vendorvalue = string.Empty;
                    if (vendorRef.Name != null)
                    {
                        var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(vendorRef.Name.Trim());
                      
                        if (dataVendor.Count!=0)
                        {
                            foreach (var customer in dataVendor)
                            {
                                Vendorvalue = customer.Id;
                            }
                            string displayName = vendorRef.Name;

                            addedPurchaseOrder.VendorRef = new ReferenceType()
                            {
                                name = vendorRef.Name,
                                Value = Vendorvalue
                            };
                        }
                    }

                }
                //P Axis 13.1 : issue 687
                if (purchaseOrder.POEmail != null)
                {
                    EmailAddress email = new EmailAddress();
                    email.Address = purchaseOrder.POEmail;
                    addedPurchaseOrder.POEmail = email;
                }
                foreach (var APAccountRef in purchaseOrder.APAccountRef)
                {
                    if (APAccountRef.Name != null && APAccountRef.Name != string.Empty && APAccountRef.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(APAccountRef.Name);
                        addedPurchaseOrder.APAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                   
                }
                foreach (var className in purchaseOrder.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetClassExistsInOnlineQBO(className.Name.Trim());
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedPurchaseOrder.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }

                foreach (var className in purchaseOrder.SalesTermRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(className.Name.Trim());
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedPurchaseOrder.SalesTermRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }


                if (purchaseOrder.DueDate != null)
                {
                    try
                    {
                        addedPurchaseOrder.DueDate = Convert.ToDateTime(purchaseOrder.DueDate);
                        addedPurchaseOrder.DueDateSpecified = true;
                    }
                    catch { }
                }

                PhysicalAddress vendorAddr = new PhysicalAddress();
                if (purchaseOrder.VendorAddr.Count > 0)
                {
                    foreach (var vendorAddress in purchaseOrder.VendorAddr)
                    {
                        vendorAddr.Line1 = vendorAddress.VendorLine1;
                        vendorAddr.Line2 = vendorAddress.VendorLine2;
                        vendorAddr.Line3 = vendorAddress.VendorLine3;
                        vendorAddr.City = vendorAddress.VendorCity;
                        vendorAddr.Country = vendorAddress.VendorCountry;
                        vendorAddr.CountrySubDivisionCode = vendorAddress.VendorCountrySubDivisionCode;
                        vendorAddr.PostalCode = vendorAddress.VendorPostalCode;
                        vendorAddr.Note = vendorAddress.VendorNote;
                        vendorAddr.Line4 = vendorAddress.VendorLine4;
                        vendorAddr.Line5 = vendorAddress.VendorLine5;
                        vendorAddr.Lat = vendorAddress.VendorAddrLat;
                        vendorAddr.Long = vendorAddress.VendorAddrLong;
                    }

                    addedPurchaseOrder.VendorAddr = vendorAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (purchaseOrder.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in purchaseOrder.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedPurchaseOrder.ShipAddr = shipAddr;
                }

                if (purchaseOrder.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in purchaseOrder.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedPurchaseOrder.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (purchaseOrder.POStatus != null)
                {
                    if (purchaseOrder.POStatus == PurchaseOrderStatusEnum.Closed.ToString())
                    {
                        addedPurchaseOrder.POStatus = PurchaseOrderStatusEnum.Closed;
                        addedPurchaseOrder.POStatusSpecified = true;
                    }
                    else if (purchaseOrder.POStatus == PurchaseOrderStatusEnum.Open.ToString())
                    {
                        addedPurchaseOrder.POStatus = PurchaseOrderStatusEnum.Open;
                        addedPurchaseOrder.POStatusSpecified = true;
                    }
                }

                if (purchaseOrder.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in purchaseOrder.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    addedPurchaseOrder.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (purchaseOrder.ExchangeRate != null)
                {
                    addedPurchaseOrder.ExchangeRate = Convert.ToDecimal(purchaseOrder.ExchangeRate);
                    addedPurchaseOrder.ExchangeRateSpecified = true;
                }



                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedPurchaseOrder.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && purchaseOrder.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {
                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = taxVal;
                                    taxLineDetail.TaxPercentSpecified = true;
                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedPurchaseOrder.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                //586
                if (CommonUtilities.GetInstance().CountryVersion == "US") //|| CommonUtilities.GetInstance().GrossNet == false || addedPurchaseOrder.GlobalTaxCalculation.ToString() != GlobalTaxCalculationEnum.TaxInclusive.ToString())
                {

                    foreach (var txntaxCode in purchaseOrder.TxnTaxDetail)
                    {
                        #region TxnTaxDetail
                        Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        if (txntaxCode.TxnTaxCodeRef != null)
                        {
                            foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                            {
                                TaxCode TaxCode = new TaxCode();
                                TaxCode.Name = taxRef.Name;
                                string Taxvalue = string.Empty;
                                string TaxName = string.Empty;
                                TaxName = TaxCode.Name;
                                if (TaxCode.Name != null)
                                {
                                    var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                   
                                    if (dataTax != null)
                                    {
                                        foreach (var tax in dataTax)
                                        {
                                            Taxvalue = tax.Id;
                                        }

                                        txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                        {
                                            name = TaxName,
                                            Value = Taxvalue
                                        };
                                    }
                                }
                            }
                        }

                //        foreach (var txnline in txntaxCode.TaxLine)
                //        {
                //            if (txnline != null)
                //            {
                //                taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                //                taxLine.DetailTypeSpecified = true;
                //            }

                //            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();

                //            foreach (var taxlineDetail in txnline.TaxLineDetail)
                //            {
                //                if (dataTax != null)
                //                {
                //                    foreach (var taxRateRef in dataTax)
                //                    {
                //                        foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                //                        {
                //                            taxLineDetail.TaxRateRef = taxRate.TaxRateRef;
                //                        }
                //                    }
                //                }
                //                if (taxlineDetail.PercentBased != null)
                //                {
                //                    taxLineDetail.PercentBased = Convert.ToBoolean(taxlineDetail.PercentBased);
                //                    taxLineDetail.PercentBasedSpecified = true;
                //                }
                //                if (taxlineDetail.TaxPercent != null)
                //                {
                //                    taxLineDetail.TaxPercent = Convert.ToDecimal(taxlineDetail.TaxPercent);
                //                    taxLineDetail.TaxPercentSpecified = true;
                //                }
                //            }

                //            taxLine.AnyIntuitObject = taxLineDetail;
                //        }
                //        if (taxLine != null)
                //        {
                //            txnTaxDetail.TaxLine = new Intuit.Ipp.Data.Line[] { taxLine };
                //        }

                //        txnTaxDetail.TotalTax = Convert.ToDecimal(txntaxCode.TotalTax);

                        if (txnTaxDetail != null)
                        {
                            addedPurchaseOrder.TxnTaxDetail = txnTaxDetail;
                        }

                        #endregion
                    }

                }


                #endregion

                #endregion
                PurchaseOrderAdded = new Intuit.Ipp.Data.PurchaseOrder();
                PurchaseOrderAdded = dataService.Update<Intuit.Ipp.Data.PurchaseOrder>(addedPurchaseOrder);
            }

            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (PurchaseOrderAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = PurchaseOrderAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = PurchaseOrderAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }
    }

    /// <summary>
    ///  checking doc number is present or not in quickbook online for PO.
    /// </summary>

    public class OnlinePurchaseOrderQBEntryCollection : Collection<OnlinePurchaseOrderEntry>
    {
        public OnlinePurchaseOrderEntry FindPurchaseOrderNumberEntry(string docNumber)
        {
            foreach (OnlinePurchaseOrderEntry item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
