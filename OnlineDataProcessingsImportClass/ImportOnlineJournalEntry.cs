﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineJournalEntry
    {
        private static ImportOnlineJournalEntry m_ImportOnlineJournalEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineJournalEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import journal class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineJournalEntry GetInstance()
        {
            if (m_ImportOnlineJournalEntry == null)
                m_ImportOnlineJournalEntry = new ImportOnlineJournalEntry();
            return m_ImportOnlineJournalEntry;
        }
        /// setting values to journal data table and returns collection.
        public async System.Threading.Tasks.Task<OnlineJournalEntryQBCollection> ImportOnlineJournalentryData(DataTable dt)
        {
       
            OnlineDataProcessingsImportClass.OnlineJournalEntryQBCollection coll = new OnlineJournalEntryQBCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {

                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    string datevalue = string.Empty;

                   OnlineDataProcessingsImportClass.JournalEntryclass journalEntry = new OnlineDataProcessingsImportClass.JournalEntryclass();
                   Intuit.Ipp.Data.JournalEntry entry=new Intuit.Ipp.Data.JournalEntry ();
                    if (dt.Columns.Contains("DocNumber"))
                    {
                        journalEntry = coll.FindJournalQBEntry(dr["DocNumber"].ToString());

                        if (journalEntry == null)
                        {
                            journalEntry = new JournalEntryclass();
                            if (dt.Columns.Contains("DocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["DocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["DocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                journalEntry.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journalEntry.DocNumber = dr["DocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            journalEntry.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        journalEntry.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of TxnDate
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    journalEntry.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journalEntry.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                journalEntry.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            journalEntry.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        journalEntry.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                journalEntry.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journalEntry.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            journalEntry.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        journalEntry.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }


                            OnlineEntities.JournalEntryLineDetail JournalEntryLineDetail = new OnlineEntities.JournalEntryLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();
                            OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();

                            #region

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of LineDescription
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations of LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    if (dr["LineAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount (" + dr["LineAmount"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                #endregion
                            }

                          
                            OnlineEntities.Entity EntityRef = new OnlineEntities.Entity();
                            if (dt.Columns.Contains("LineEntityRef"))
                            {
                                #region Validations of LineEntityRef
                                if (dr["LineEntityRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineEntityRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineEntityRef (" + dr["LineEntityRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                EntityRef.Name = dr["LineEntityRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                EntityRef.Name = dr["LineEntityRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            EntityRef.Name = dr["LineEntityRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EntityRef.Name = dr["LineEntityRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                           
                            if (dt.Columns.Contains("LineEntityType"))
                            {
                                #region Validations of LineEntityType
                                if (dr["LineEntityType"].ToString() != string.Empty)
                                {
                                    if (dr["LineEntityType"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineEntityType (" + dr["LineEntityType"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                EntityRef.Type = dr["LineEntityType"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                EntityRef.Type = dr["LineEntityType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            EntityRef.Type = dr["LineEntityType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EntityRef.Type = dr["LineEntityType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (EntityRef.Type != null || EntityRef.Name != null)
                            {
                                JournalEntryLineDetail.Entity.Add(EntityRef);
                            }
                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }

                                    // bug 480
                                    #region Account Validation
                                    List<Intuit.Ipp.Data.Account> Accountdata = null;
                                    string AccountName = string.Empty;
                                    AccountName = AccountRef.Name.Trim();
                                    Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                    // bug 480
                                    if (Accountdata != null)
                                    {
                                        if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                        {
                                            //480
                                            Messages m1 = new Messages();
                                            m1.buttonCancel.Text = "Add";
                                            m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                            m1.ShowDialog();
                                            //Add
                                            if (m1.DialogResult.ToString().ToLower() == "ok")
                                            {
                                                CommonUtilities cUtility = new CommonUtilities();
                                                await  cUtility.CreateAccountInQBO(AccountName);
                                            }
                                            //Quit
                                            if (m1.DialogResult.ToString().ToLower() == "no")
                                            {
                                                break;
                                                //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                                //{
                                                //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                                //    bkWorker.CancelAsync();
                                                //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                                //}
                                                //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                                //{
                                                //    axisForm.IsBusy = false;
                                                //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                                //    bkWorker.CancelAsync();

                                                //}
                                                //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                                //{
                                                //    axisForm.IsBusy = false;
                                                //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                                //    bkWorker.CancelAsync();
                                                //}

                                                ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                                //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                            }
                                            //Ignore
                                            if (m1.DialogResult.ToString() == "Ignore")
                                            {

                                            }
                                            //IgnoreAll
                                            if (m1.DialogResult.ToString() == "Abort")
                                            {
                                                CommonUtilities.GetInstance().ignoreAllAccount = true;
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                #endregion
                            }

                            if (AccountRef.Name != null)
                            {
                                JournalEntryLineDetail.AccountRef.Add(AccountRef);
                            }

                            

                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of LineBillableStatus
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Debit") == true && dt.Columns.Contains("Credit") == true)
                            {
                                #region Validations of Debit
                                if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() == string.Empty)
                                {
                                    if (dr["Debit"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Debit (" + dr["Debit"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                JournalEntryLineDetail.Debit = dr["Debit"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                        JournalEntryLineDetail.PostingType = "Debit";

                                    }
                                }
                                if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() != string.Empty)
                                {
                                    if (dr["Credit"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Credit (" + dr["Credit"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                JournalEntryLineDetail.Credit = dr["Credit"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                        JournalEntryLineDetail.PostingType = "Credit";
                                    }
                                }
                                #endregion
                            }
                            

                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 504)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (ClassRef.Name != null)
                            {
                                JournalEntryLineDetail.ClassRef.Add(ClassRef);
                            }
                            OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                            if (dt.Columns.Contains("LineDepartmentRef"))
                            {
                                #region Validations of LineDepartmentRef
                                if (dr["LineDepartmentRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineDepartmentRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDepartmentRef (" + dr["LineDepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepartmentRef.Name = dr["LineDepartmentRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (DepartmentRef.Name != null)
                            {
                                JournalEntryLineDetail.DepartmentRef.Add(DepartmentRef);
                            }


                            if (dt.Columns.Contains("LineTaxCodeRef"))
                            {
                                #region Validations of LineTaxCodeRef
                                if (dr["LineTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineTaxCodeRef (" + dr["LineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 458

                            if (dt.Columns.Contains("LineTaxApplicableOn"))
                            {
                                #region Validations of LineTaxApplicableOn
                                if (dr["LineTaxApplicableOn"].ToString() != string.Empty)
                                {
                                    if (dr["LineTaxApplicableOn"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineTaxCodeRef (" + dr["LineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("LineTaxAmount"))
                            {
                                #region Validations of LineTaxAmount
                                if (dr["LineTaxAmount"].ToString() != string.Empty)
                                {
                                    if (dr["LineTaxAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineTaxAmount (" + dr["LineTaxAmount"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TxnTaxDetail.TaxApplicableOn != string.Empty )
                            {
                                JournalEntryLineDetail.TxnTaxDetail.Add(TxnTaxDetail);
                            }

                            // end 458

                            if (TaxCodeRef.Name != null)
                            {
                                JournalEntryLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }

                            //586
                            //OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                            OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                            OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                            if (dt.Columns.Contains("TxnTaxCodeRefName"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            #endregion

                            if (JournalEntryLineDetail.AccountRef.Count != 0 || JournalEntryLineDetail.DepartmentRef.Count != 0 || JournalEntryLineDetail.ClassRef.Count != 0 || JournalEntryLineDetail.Entity.Count != 0 || JournalEntryLineDetail.PostingType != null || JournalEntryLineDetail.BillableStatus != null)
                            {
                                Line.JournalEntryLineDetail.Add(JournalEntryLineDetail);
                            }

                            //586
                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }
                            if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                            {
                                TxnTaxDetail.TaxLine.Add(TaxLine);
                            }
                            if (TxnTaxDetail.TaxLine.Count != 0 || TxnTaxCodeRef.Name != null)
                            {
                                journalEntry.TxnTaxDetail.Add(TxnTaxDetail);
                            }
                            //
                               
                            journalEntry.Line.Add(Line);

                            //Axis 701
                            OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                            {
                                journalEntry.CurrencyRef.Add(CurrencyRef);
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "ExchangeRate")
                                            {
                                                isIgnoreAll = true;
                                                journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        journalEntry.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            //Axis 701 ends

                            coll.Add(journalEntry);

                        }

                        else
                        {
                            OnlineEntities.JournalEntryLineDetail JournalEntryLineDetail = new OnlineEntities.JournalEntryLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();
                             OnlineEntities.TxnTaxDetail TxnTaxDetail  = new OnlineEntities.TxnTaxDetail();
                            #region

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of LineDescription
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations of LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    if (dr["LineAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount (" + dr["LineAmount"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                #endregion
                            }

                           
                            OnlineEntities.Entity EntityRef = new OnlineEntities.Entity();
                            if (dt.Columns.Contains("LineEntityRef"))
                            {
                                #region Validations of LineEntityRef
                                if (dr["LineEntityRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineEntityRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineEntityRef (" + dr["LineEntityRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                EntityRef.Name = dr["LineEntityRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                EntityRef.Name = dr["LineEntityRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            EntityRef.Name = dr["LineEntityRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EntityRef.Name = dr["LineEntityRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineEntityType"))
                            {
                                #region Validations of LineEntityType
                                if (dr["LineEntityType"].ToString() != string.Empty)
                                {
                                    if (dr["LineEntityType"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineEntityType (" + dr["LineEntityType"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                EntityRef.Type = dr["LineEntityType"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                EntityRef.Type = dr["LineEntityType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            EntityRef.Type = dr["LineEntityType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EntityRef.Type = dr["LineEntityType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (EntityRef.Name != null)
                            {
                                JournalEntryLineDetail.Entity.Add(EntityRef);
                            }

                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }

                                    // bug 480
                                    #region Account Validation
                                    List<Intuit.Ipp.Data.Account> Accountdata = null;
                                    string AccountName = string.Empty;
                                    AccountName = AccountRef.Name.Trim();
                                    Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                    if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                    {
                                        //480
                                        Messages m1 = new Messages();
                                        m1.buttonCancel.Text = "Add";
                                        m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                        m1.ShowDialog();
                                        //Add
                                        if (m1.DialogResult.ToString().ToLower() == "ok")
                                        {
                                            CommonUtilities cUtility = new CommonUtilities();
                                            await  cUtility.CreateAccountInQBO(AccountName);
                                        }
                                        //Quit
                                        if (m1.DialogResult.ToString().ToLower() == "no")
                                        {
                                            break;
                                            //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                            //{
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                            //    bkWorker.CancelAsync();
                                            //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                            //}
                                            //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                            //{
                                            //    axisForm.IsBusy = false;
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                            //    bkWorker.CancelAsync();

                                            //}
                                            //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                            //{
                                            //    axisForm.IsBusy = false;
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                            //    bkWorker.CancelAsync();
                                            //}

                                            ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                            //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                        }
                                        //Ignore
                                        if (m1.DialogResult.ToString() == "Ignore")
                                        {

                                        }
                                        //IgnoreAll
                                        if (m1.DialogResult.ToString() == "Abort")
                                        {
                                            CommonUtilities.GetInstance().ignoreAllAccount = true;
                                        }
                                    }


                                    #endregion
                                }
                                #endregion
                            }

                            if (AccountRef.Name != null)
                            {
                                JournalEntryLineDetail.AccountRef.Add(AccountRef);
                            }

                           
                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of LineBillableStatus
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }       
                  


                            if (dt.Columns.Contains("Debit") == true && dt.Columns.Contains("Credit") == true)
                            {
                                #region Validations of Debit
                                if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() == string.Empty)
                                {
                                    if (dr["Debit"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Debit (" + dr["Debit"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                JournalEntryLineDetail.Debit = dr["Debit"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                        JournalEntryLineDetail.PostingType = "Debit";

                                    }
                                }
                                if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() != string.Empty)
                                {
                                    if (dr["Credit"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Credit (" + dr["Credit"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                JournalEntryLineDetail.Credit = dr["Credit"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                        JournalEntryLineDetail.PostingType = "Credit";
                                    }
                                }
                                #endregion
                            }
                         
                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 504)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (ClassRef.Name != null)
                            {
                                JournalEntryLineDetail.ClassRef.Add(ClassRef);
                            }
                            OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                            if (dt.Columns.Contains("LineDepartmentRef"))
                            {
                                #region Validations of LineDepartmentRef
                                if (dr["LineDepartmentRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineDepartmentRef"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDepartmentRef (" + dr["LineDepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepartmentRef.Name = dr["LineDepartmentRef"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                        
                            if (DepartmentRef.Name != null)
                            {
                                JournalEntryLineDetail.DepartmentRef.Add(DepartmentRef);
                            }

                            if (dt.Columns.Contains("LineTaxCodeRef"))
                            {
                                #region Validations of LineTaxCodeRef
                                if (dr["LineTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineTaxCodeRef (" + dr["LineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }


                            //bug 458
                            if (dt.Columns.Contains("LineTaxApplicableOn"))
                            {
                                #region Validations of LineTaxApplicableOn
                                if (dr["LineTaxApplicableOn"].ToString() != string.Empty)
                                {
                                    if (dr["LineTaxApplicableOn"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineTaxCodeRef (" + dr["LineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("LineTaxAmount"))
                            {
                                #region Validations of LineTaxAmount
                                if (dr["LineTaxAmount"].ToString() != string.Empty)
                                {
                                    if (dr["LineTaxAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineTaxAmount (" + dr["LineTaxAmount"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TxnTaxDetail.TaxApplicableOn != string.Empty)
                            {
                                JournalEntryLineDetail.TxnTaxDetail.Add(TxnTaxDetail);
                            }

                            // end 458

                            //586
                            //OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                            OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                            OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                            if (dt.Columns.Contains("TxnTaxCodeRefName"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TaxCodeRef.Name != null)
                            {
                                JournalEntryLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            #endregion
                            if (JournalEntryLineDetail.AccountRef.Count != 0 || JournalEntryLineDetail.DepartmentRef.Count != 0 || JournalEntryLineDetail.ClassRef.Count != 0 || JournalEntryLineDetail.Entity.Count != 0 || JournalEntryLineDetail.PostingType != null || JournalEntryLineDetail.BillableStatus != null)
                            {                            
                                Line.JournalEntryLineDetail.Add(JournalEntryLineDetail);
                            }

                            //586
                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }
                            if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                            {
                                TxnTaxDetail.TaxLine.Add(TaxLine);
                            }
                            if (TxnTaxDetail.TaxLine.Count != 0 || TxnTaxCodeRef.Name != null)
                            {
                                journalEntry.TxnTaxDetail.Add(TxnTaxDetail);
                            }
                            //

                            journalEntry.Line.Add(Line);

                            //Axis 701
                            OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                            {
                                journalEntry.CurrencyRef.Add(CurrencyRef);
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "ExchangeRate")
                                            {
                                                isIgnoreAll = true;
                                                journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        journalEntry.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            //Axis 701 ends  
                        }

                    }
                    else
                    {
                        journalEntry = new JournalEntryclass();                       

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of TxnDate
                            DateTime SODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                journalEntry.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journalEntry.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            journalEntry.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        journalEntry.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    journalEntry.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            journalEntry.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            journalEntry.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        journalEntry.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    journalEntry.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }


                        OnlineEntities.JournalEntryLineDetail JournalEntryLineDetail = new OnlineEntities.JournalEntryLineDetail();
                        OnlineEntities.Line Line = new OnlineEntities.Line();
                        OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();
                        OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();

                        #region Line

                        if (dt.Columns.Contains("LineDescription"))
                        {
                            #region Validations of LineDescription
                            if (dr["LineDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineDescription"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineDescription = dr["LineDescription"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineAmount"))
                        {
                            #region Validations of LineAmount
                            if (dr["LineAmount"].ToString() != string.Empty)
                            {
                                if (dr["LineAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAmount (" + dr["LineAmount"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineAmount = dr["LineAmount"].ToString();
                                }
                            }
                            #endregion
                        }


                        OnlineEntities.Entity EntityRef = new OnlineEntities.Entity();
                        if (dt.Columns.Contains("LineEntityRef"))
                        {
                            #region Validations of LineEntityRef
                            if (dr["LineEntityRef"].ToString() != string.Empty)
                            {
                                if (dr["LineEntityRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineEntityRef (" + dr["LineEntityRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            EntityRef.Name = dr["LineEntityRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            EntityRef.Name = dr["LineEntityRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EntityRef.Name = dr["LineEntityRef"].ToString();
                                    }
                                }
                                else
                                {
                                    EntityRef.Name = dr["LineEntityRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineEntityType"))
                        {
                            #region Validations of LineEntityType
                            if (dr["LineEntityType"].ToString() != string.Empty)
                            {
                                if (dr["LineEntityType"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineEntityType (" + dr["LineEntityType"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            EntityRef.Type = dr["LineEntityType"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            EntityRef.Type = dr["LineEntityType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EntityRef.Type = dr["LineEntityType"].ToString();
                                    }
                                }
                                else
                                {
                                    EntityRef.Type = dr["LineEntityType"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (EntityRef.Name != null)
                        {
                            JournalEntryLineDetail.Entity.Add(EntityRef);
                        }
                        OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                        if (dt.Columns.Contains("LineAccountRef"))
                        {
                            #region Validations of LineAccountRef
                            if (dr["LineAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountRef.Name = dr["LineAccountRef"].ToString();
                                }

                                // bug 480
                                #region Account Validation
                                List<Intuit.Ipp.Data.Account> Accountdata = null;
                                string AccountName = string.Empty;
                                AccountName = AccountRef.Name.Trim();
                                Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                {
                                    //480
                                    Messages m1 = new Messages();
                                    m1.buttonCancel.Text = "Add";
                                    m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                    m1.ShowDialog();
                                    //Add
                                    if (m1.DialogResult.ToString().ToLower() == "ok")
                                    {
                                        CommonUtilities cUtility = new CommonUtilities();
                                        await  cUtility.CreateAccountInQBO(AccountName);
                                    }
                                    //Quit
                                    if (m1.DialogResult.ToString().ToLower() == "no")
                                    {
                                        break;
                                        //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                        //{
                                        //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                        //    bkWorker.CancelAsync();
                                        //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                        //}
                                        //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                        //{
                                        //    axisForm.IsBusy = false;
                                        //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                        //    bkWorker.CancelAsync();

                                        //}
                                        //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                        //{
                                        //    axisForm.IsBusy = false;
                                        //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                        //    bkWorker.CancelAsync();
                                        //}

                                        ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                        //TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                    }
                                    //Ignore
                                    if (m1.DialogResult.ToString() == "Ignore")
                                    {

                                    }
                                    //IgnoreAll
                                    if (m1.DialogResult.ToString() == "Abort")
                                    {
                                        CommonUtilities.GetInstance().ignoreAllAccount = true;
                                    }
                                }


                                #endregion
                            }
                            #endregion
                        }

                        if (AccountRef.Name != null)
                        {
                            JournalEntryLineDetail.AccountRef.Add(AccountRef);
                        }

                       
                        if (dt.Columns.Contains("LineBillableStatus"))
                        {
                            #region Validations of LineBillableStatus
                            if (dr["LineBillableStatus"].ToString() != string.Empty)
                            {
                                if (dr["LineBillableStatus"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    JournalEntryLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Debit") == true && dt.Columns.Contains("Credit") == true)
                        {
                            #region Validations of Debit
                            if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() == string.Empty)
                            {
                                if (dr["Debit"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Debit (" + dr["Debit"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            JournalEntryLineDetail.Debit = dr["Debit"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                    }
                                }
                                else
                                {
                                    JournalEntryLineDetail.Debit = dr["Debit"].ToString();
                                    JournalEntryLineDetail.PostingType = "Debit";

                                }
                            }
                            if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() != string.Empty)
                            {
                                if (dr["Credit"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Credit (" + dr["Credit"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            JournalEntryLineDetail.Credit = dr["Credit"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                    }
                                }
                                else
                                {
                                    JournalEntryLineDetail.Credit = dr["Credit"].ToString();
                                    JournalEntryLineDetail.PostingType = "Credit";
                                }
                            }
                            #endregion
                        }


                        OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                        if (dt.Columns.Contains("LineClassRef"))
                        {
                            #region Validations of LineClassRef
                            if (dr["LineClassRef"].ToString() != string.Empty)
                            {
                                if (dr["LineClassRef"].ToString().Length > 504)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef.Name = dr["LineClassRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (ClassRef.Name != null)
                        {
                            JournalEntryLineDetail.ClassRef.Add(ClassRef);
                        }
                        OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                        if (dt.Columns.Contains("LineDepartmentRef"))
                        {
                            #region Validations of LineDepartmentRef
                            if (dr["LineDepartmentRef"].ToString() != string.Empty)
                            {
                                if (dr["LineDepartmentRef"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDepartmentRef (" + dr["LineDepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepartmentRef.Name = dr["LineDepartmentRef"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                    }
                                }
                                else
                                {
                                    DepartmentRef.Name = dr["LineDepartmentRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (DepartmentRef.Name != null)
                        {
                            JournalEntryLineDetail.DepartmentRef.Add(DepartmentRef);
                        }

                        if (dt.Columns.Contains("LineTaxCodeRef"))
                        {
                            #region Validations of LineTaxCodeRef
                            if (dr["LineTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["LineTaxCodeRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineTaxCodeRef (" + dr["LineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef.Name = dr["LineTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        //bug 458

                        if (dt.Columns.Contains("LineTaxApplicableOn"))
                        {
                            #region Validations of LineTaxApplicableOn
                            if (dr["LineTaxApplicableOn"].ToString() != string.Empty)
                            {
                                if (dr["LineTaxApplicableOn"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineTaxCodeRef (" + dr["LineTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxDetail.TaxApplicableOn = dr["LineTaxApplicableOn"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("LineTaxAmount"))
                        {
                            #region Validations of LineTaxAmount
                            if (dr["LineTaxAmount"].ToString() != string.Empty)
                            {
                                if (dr["LineTaxAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineTaxAmount (" + dr["LineTaxAmount"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxDetail.TaxAmount = dr["LineTaxAmount"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (TxnTaxDetail.TaxApplicableOn != string.Empty)
                        {
                            JournalEntryLineDetail.TxnTaxDetail.Add(TxnTaxDetail);
                        }

                        // end 458


                        if (TaxCodeRef.Name != null)
                        {
                            JournalEntryLineDetail.TaxCodeRef.Add(TaxCodeRef);
                        }
                        #endregion

                        //586
                        //OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                        OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                        OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                        OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                        if (dt.Columns.Contains("TxnTaxCodeRefName"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                            {
                                if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (JournalEntryLineDetail.AccountRef.Count != 0 || JournalEntryLineDetail.DepartmentRef.Count != 0 || JournalEntryLineDetail.ClassRef.Count != 0 || JournalEntryLineDetail.Entity.Count != 0 || JournalEntryLineDetail.PostingType != null || JournalEntryLineDetail.BillableStatus != null)
                        {
                            Line.JournalEntryLineDetail.Add(JournalEntryLineDetail);
                        }

                        //586
                        if (TxnTaxCodeRef.Name != null)
                        {
                            TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                        }
                        if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                        {
                            TxnTaxDetail.TaxLine.Add(TaxLine);
                        }
                        if (TxnTaxDetail.TaxLine.Count != 0 || TxnTaxCodeRef.Name != null)
                        {
                            journalEntry.TxnTaxDetail.Add(TxnTaxDetail);
                        }
                        //
                        journalEntry.Line.Add(Line);

                        //Axis 701
                        OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();

                        if (dt.Columns.Contains("CurrencyRef"))
                        {
                            #region Validations of CurrencyRef
                            if (dr["CurrencyRef"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CurrencyRef.Name != null)
                        {
                            journalEntry.CurrencyRef.Add(CurrencyRef);
                        }

                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "ExchangeRate")
                                        {
                                            isIgnoreAll = true;
                                            journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        journalEntry.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    journalEntry.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                }
                            }

                            #endregion
                        }
                        //Axis 701 ends
                        coll.Add(journalEntry);
                    }
                }
            }
        
            #endregion

            #endregion

            return coll;

        }
    }
}

