﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using TransactionImporter;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineBillEntry
    {
        private static ImportOnlineBillEntry m_ImportOnlineBillEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineBillEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import bill class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineBillEntry GetInstance()
        {
            if (m_ImportOnlineBillEntry == null)
                m_ImportOnlineBillEntry = new ImportOnlineBillEntry();
            return m_ImportOnlineBillEntry;
        }
        /// <summary>
        /// setting values to bill data table and returns collection.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<OnlineBillQBEntryCollection> ImportOnlineBillData(DataTable dt)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineBillQBEntryCollection coll = new OnlineBillQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;

            OnlineEntities.TxnTaxDetail TxnTaxDetail = null;
            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = null;
            OnlineEntities.TaxRateRef TaxRateRef = null;

            TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                   
                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Employee Validation
                    OnlineDataProcessingsImportClass.Bill bill = new OnlineDataProcessingsImportClass.Bill();

                   

                    if (dt.Columns.Contains("DocNumber"))
                    {
                        bill = coll.FindBillNumberEntry(dr["DocNumber"].ToString());

                        if (bill == null)
                        {

                            #region if bill is null

                            bill = new Bill();

                            if (dt.Columns.Contains("DocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["DocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["DocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.DocNumber = dr["DocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bill.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of TxnDate
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    bill.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    bill.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                bill.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            bill.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        bill.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bill.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }
                            //Axis 11.2 bug no. 337
                            OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                            if (dt.Columns.Contains("DepartmentRef"))
                            {
                                #region Validations of DepartmentRef
                                if (dr["DepartmentRef"].ToString() != string.Empty)
                                {
                                    if (dr["DepartmentRef"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (DepartmentRef.Name != null)
                            {
                                bill.DepartmentRef.Add(DepartmentRef);
                            }
                            OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();
                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CurrencyRef.Name != null)
                            {
                                bill.CurrencyRef.Add(CurrencyRef);
                            }
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations of ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    if (dr["ExchangeRate"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate (" + dr["ExchangeRate"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.ExchangeRate = dr["ExchangeRate"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TransactionLocationType"))
                            {
                                #region Validations of TransactionLocationType
                                if (dr["TransactionLocationType"].ToString() != string.Empty)
                                {
                                    if (dr["TransactionLocationType"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bill.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //End Axis 11.2 bug no. 337
                            OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of LineDescription
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations of LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    if (dr["LineAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount (" + dr["LineAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                #endregion
                            }
                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                            if (dt.Columns.Contains("LineCustomerRef"))
                            {
                                #region Validations of LineCustomerRef
                                if (dr["LineCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CustomerRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef);
                            }

                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }

                                    // bug 480
                                    #region Account Validation
                                    List<Intuit.Ipp.Data.Account> Accountdata = null;
                                    string AccountName = string.Empty;
                                    AccountName = AccountRef.Name.Trim();
                                    Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                    if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                    {
                                        //480
                                        Messages m1 = new Messages();
                                        m1.buttonCancel.Text = "Add";
                                        m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                        m1.ShowDialog();
                                        //Add
                                        if (m1.DialogResult.ToString().ToLower() == "ok")
                                        {
                                            CommonUtilities cUtility = new CommonUtilities();
                                            await  cUtility.CreateAccountInQBO(AccountName);
                                        }
                                        //Quit
                                        if (m1.DialogResult.ToString().ToLower() == "no")
                                        {
                                            break;

                                            //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                            //{
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                            //    bkWorker.CancelAsync();
                                            //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                            //}
                                            //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                            //{
                                            //    axisForm.IsBusy = false;
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                            //    bkWorker.CancelAsync();

                                            //}
                                            //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                            //{
                                            //    axisForm.IsBusy = false;
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                            //    bkWorker.CancelAsync();
                                            //   // TransactionImporter.ImportSplashScreen.m_frmSplash.Dispose();
                                            //   // TransactionImporter.ImportSplashScreen.m_frmSplash.Close();
                                            //    CommonUtilities.GetInstance().iSplashScreen.CloseImportSplash();
                                            //    break;

                                            //     //   new EventHandler(Coin1_ClickHandler);

                                            //}

                                          //  DataProcessingBlocks.CommonUtilities.CloseSplash();
                                         //   TransactionImporter.ImportSplashScreen.m_frmSplash.fun1();
                                           // TransactionImporter.ImportSplashScreen.CloseForm();
                                            
                                       }
                                        //Ignore
                                        if (m1.DialogResult.ToString() == "Ignore")
                                        {

                                        }
                                        //IgnoreAll
                                        if (m1.DialogResult.ToString() == "Abort")
                                        {
                                            CommonUtilities.GetInstance().ignoreAllAccount = true;
                                        }
                                    }


                                    #endregion
                                }
                                #endregion
                         
                            
                            }

                            if (AccountRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            }

                            ////Improvement::493
                            //if (dt.Columns.Contains("LineAcctNum"))
                            //{
                            //    #region Validations of LineAcctNum
                            //    if (dr["LineAcctNum"].ToString() != string.Empty)
                            //    {
                            //        if (dr["LineAcctNum"].ToString().Length > 15)
                            //        {
                            //            if (isIgnoreAll == false)
                            //            {
                            //                string strMessages = "This LineAcctNum (" + dr["LineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            //                if (Convert.ToString(result) == "Cancel")
                            //                {
                            //                    continue;
                            //                }
                            //                if (Convert.ToString(result) == "No")
                            //                {
                            //                    return null;
                            //                }
                            //                if (Convert.ToString(result) == "Ignore")
                            //                {
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString().Substring(0, 15);
                            //                }
                            //                if (Convert.ToString(result) == "Abort")
                            //                {
                            //                    isIgnoreAll = true;
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //                }
                            //            }
                            //            else
                            //            {
                            //                AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //        }
                            //    }
                            //    #endregion


                            //}

                            // if (AccountRef.AcctNum != null)
                            // {
                            //     AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            // }

                            //Axis 11.2 bug no.337
                            OnlineEntities.ClassRef AccountClassRef = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("LineAccountClass"))
                            {
                                #region Validations of LineAccountClass
                                if (dr["LineAccountClass"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountClass"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountClass (" + dr["LineAccountClass"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountClassRef.Name = dr["LineAccountClass"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (AccountClassRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.ClassRef.Add(AccountClassRef);
                            }
                            //End Axis 11.2 bug no.337
                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of LineBillableStatus
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.TaxCodeRef lineTaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                            {
                                #region Validations of LineAccountTaxCodeRef
                                if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (lineTaxCodeRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.TaxCodeRef.Add(lineTaxCodeRef);
                            }
                            if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.APAccountRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                            {
                                Line.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail); 
                            }                           
                            
                           
                            //ItembasedExpansionlinedetail....

                            OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            CustomerRef = new OnlineEntities.CustomerRef();
                            TaxCodeRef = new OnlineEntities.TaxCodeRef();
                            OnlineEntities.Line ItemBasedExpenseLine = new OnlineEntities.Line();

                            if (dt.Columns.Contains("LineItemDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineItemDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemDescription (" + dr["LineItemDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemAmount"))
                            {
                                #region Validations for LineItemAmount
                                if (dr["LineItemAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemAmount( " + dr["LineItemAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemRef"))
                            {
                                #region Validations of LineItemRef
                                if (dr["LineItemRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineItemRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU= dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemQty"))
                            {
                                #region Validations for LineItemQty
                                if (dr["LineItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemUnitPrice"))
                            {
                                #region Validations for LineItemUnitPrice
                                if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemTaxCodeRef"))
                            {
                                #region Validations of LineItemTaxCodeRef
                                if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemCustomerRef"))
                            {
                                #region Validations of LineItemCustomerRef
                                if (dr["LineItemCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemCustomerRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemCustomerRef (" + dr["LineItemCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineItemCustomerRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemBillableStatus"))
                            {
                                #region Validations of LineItemBillableStatus
                                if (dr["LineItemBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemBillableStatus"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemBillableStatus (" + dr["LineItemBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }
                            //bug 486
                            if (ItemRef.Name != null||ItemRef.SKU!=null)
                            {
                                ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (ClassRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                            }
                            if (CustomerRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                            }

                            if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                            {
                                ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);                               
                            }                                                      
                           
                          //  bill.Line.Add(ItemBasedExpenseLine);

                            if (Line.AccountBasedExpenseLineDetail.Count > 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                bill.Line.Add(ItemBasedExpenseLine);
                                bill.Line.Add(Line);
                            }
                            else if (Line.AccountBasedExpenseLineDetail.Count > 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count == 0)
                            {
                                bill.Line.Add(Line);
                            }
                            else if (Line.AccountBasedExpenseLineDetail.Count == 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                bill.Line.Add(ItemBasedExpenseLine);
                            }                           
                            else if (Line.AccountBasedExpenseLineDetail.Count == 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count == 0 )
                            {
                                bill.Line.Add(ItemBasedExpenseLine);
                                bill.Line.Add(Line);
                            }

                            #region Tax

                          
                            OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                            TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                            OnlineEntities.TaxLine  TaxLine = new OnlineEntities.TaxLine();
                            TaxRateRef = new OnlineEntities.TaxRateRef();

                            if(CommonUtilities.GetInstance().CountryVersion=="US")
                            {
                               if (dt.Columns.Contains("TxnTaxCodeRefName"))
                               {
                                  #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                               }
                            }
                            if (dt.Columns.Contains("TotalTax"))
                            {
                                #region Validations of TotalTax
                                if (dr["TotalTax"].ToString() != string.Empty)
                                {
                                    if (dr["TotalTax"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TotalTax (" + dr["TotalTax"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxLineAmount"))
                            {
                                #region Validations of TaxLineAmount
                                if (dr["TaxLineAmount"].ToString() != string.Empty)
                                {
                                    if (dr["TaxLineAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxLineAmount (" + dr["TaxLineAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxLine.Amount = dr["TaxLineAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                    }
                                }
                                #endregion
                            }
                          
                            if (dt.Columns.Contains("TaxPercentBased"))
                            {
                                #region Validations of TaxPercentBased
                                if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                    {
                                        TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                        {
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "TaxPercentBased";
                                            }
                                            else
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxPercent"))
                            {
                                #region Validations for TaxPercent
                                if (dr["TaxPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (CommonUtilities.GetInstance().CountryVersion != "US")
                            {
                                if (dt.Columns.Contains("NetAmountTaxable"))
                                {
                                    #region Validations of NetAmountTaxable
                                    if (dr["NetAmountTaxable"].ToString() != string.Empty)
                                    {
                                        if (dr["NetAmountTaxable"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This NetAmountTaxable (" + dr["NetAmountTaxable"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString().Substring(0, 100);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("TaxRateRef"))
                                {
                                    #region Validations of TaxRateRef
                                    if (dr["TaxRateRef"].ToString() != string.Empty)
                                    {
                                        if (dr["TaxRateRef"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TaxRateRef (" + dr["TaxRateRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TaxRateRef.Name = dr["TaxRateRef"].ToString().Substring(0, 100);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                if (TaxRateRef.Name != null)
                                {
                                    TaxLineDetail.TaxRateRef.Add(TaxRateRef);
                                }

                            }

                            if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef.Count>0)
                            {
                                TaxLine.TaxLineDetail.Add(TaxLineDetail);
                            }
                            
                            if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                            {
                                TxnTaxDetail.TaxLine.Add(TaxLine);
                            }
                            if (TxnTaxDetail.TxnTaxCodeRef.Count != 0 || TxnTaxDetail.TaxLine.Count != 0)
                            {
                                bill.TxnTaxDetail.Add(TxnTaxDetail);
                            }
                           
                            //586
                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }

                            #endregion

                            OnlineEntities.VendorRef vendorRef = new OnlineEntities.VendorRef();
                            if (dt.Columns.Contains("VendorRefName"))
                            {
                                #region Validations of VendorRefName
                                if (dr["VendorRefName"].ToString() != string.Empty)
                                {
                                    if (dr["VendorRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorRefName (" + dr["VendorRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                vendorRef.Name = dr["VendorRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                vendorRef.Name = dr["VendorRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            vendorRef.Name = dr["VendorRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorRef.Name = dr["VendorRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (vendorRef.Name != null)
                            {
                                bill.VendorRef.Add(vendorRef);
                            }

                            if (dt.Columns.Contains("GlobalTaxCalculation"))
                            {
                                #region Validations of bill.GlobalTaxCal
                                if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                                {
                                    if (dr["GlobalTaxCalculation"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString();
                                    }
                                }
                                #endregion
                            }


                            OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                            if (dt.Columns.Contains("APAccountRefName"))
                            {
                                #region Validations of APAccountRefName
                                if (dr["APAccountRefName"].ToString() != string.Empty)
                                {
                                    if (dr["APAccountRefName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This APAccountRefName (" + dr["APAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                APAccountRef.Name = dr["APAccountRefName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                APAccountRef.Name = dr["APAccountRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            APAccountRef.Name = dr["APAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRefName"].ToString();
                                    }

                                    // bug 480
                                    #region Account Validation
                                    List<Intuit.Ipp.Data.Account> Accountdata = null;
                                    string AccountName = string.Empty;
                                    AccountName = APAccountRef.Name.Trim();
                                    Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                    if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                    {
                                        //480
                                        Messages m1 = new Messages();
                                        m1.buttonCancel.Text = "Add";
                                        m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                        m1.ShowDialog();
                                        //Add
                                        if (m1.DialogResult.ToString().ToLower() == "ok")
                                        {
                                            CommonUtilities cUtility = new CommonUtilities();
                                            await  cUtility.CreateAccountInQBO(AccountName);
                                        }
                                        //Quit
                                        if (m1.DialogResult.ToString().ToLower() == "no")
                                        {
                                            break;
                                          //  if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                          //  {
                                          //      bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                          //      bkWorker.CancelAsync();
                                          //      //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                          //  }
                                          //  if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                          //  {
                                          //      axisForm.IsBusy = false;
                                          //      bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                          //      bkWorker.CancelAsync();

                                          //  }
                                          //  if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                          //  {
                                          //      axisForm.IsBusy = false;
                                          //      bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                          //      bkWorker.CancelAsync();
                                          //  }

                                          ////  DataProcessingBlocks.CommonUtilities.CloseSplash();

                                          //  TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                        }
                                        //Ignore
                                        if (m1.DialogResult.ToString() == "Ignore")
                                        {

                                        }
                                        //IgnoreAll
                                        if (m1.DialogResult.ToString() == "Abort")
                                        {
                                            CommonUtilities.GetInstance().ignoreAllAccount = true;
                                        }
                                    }


                                    #endregion
                                }



                                #endregion
                            }
                            if (APAccountRef.Name != null)
                            {
                                bill.APAccountRef.Add(APAccountRef);
                            }

                            ////Improvement::493
                            //if (dt.Columns.Contains("APAcctNum"))
                            //{
                            //    #region Validations of APAcctNum
                            //    if (dr["APAcctNum"].ToString() != string.Empty)
                            //    {
                            //        if (dr["APAcctNum"].ToString().Length > 15)
                            //        {
                            //            if (isIgnoreAll == false)
                            //            {
                            //                string strMessages = "This APAcctNum (" + dr["APAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            //                if (Convert.ToString(result) == "Cancel")
                            //                {
                            //                    continue;
                            //                }
                            //                if (Convert.ToString(result) == "No")
                            //                {
                            //                    return null;
                            //                }
                            //                if (Convert.ToString(result) == "Ignore")
                            //                {
                            //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString().Substring(0, 15);
                            //                }
                            //                if (Convert.ToString(result) == "Abort")
                            //                {
                            //                    isIgnoreAll = true;
                            //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                            //                }
                            //            }
                            //            else
                            //            {
                            //                APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                            //        }
                            //   }
                            //    #endregion
                            //}
                            //if (APAccountRef.AcctNum != null)
                            //{
                            //    bill.APAccountRef.Add(APAccountRef);
                            //}

                            OnlineEntities.SalesTermRef SalesTermRef = new OnlineEntities.SalesTermRef();
                            if (dt.Columns.Contains("SalesTermRefName"))
                            {
                                #region Validations of SalesTermRefName
                                if (dr["SalesTermRefName"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTermRefName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTermRefName (" + dr["SalesTermRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesTermRef.Name = dr["SalesTermRefName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesTermRef.Name = dr["SalesTermRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesTermRef.Name = dr["SalesTermRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesTermRef.Name = dr["SalesTermRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (SalesTermRef.Name != null)
                            {
                                bill.SalesTermRef.Add(SalesTermRef);

                            }
                            if (dt.Columns.Contains("DueDate"))
                            {
                                #region Validations of DueDate
                                DateTime SODate = new DateTime();
                                if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["DueDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DueDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    bill.DueDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    bill.DueDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                bill.DueDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            bill.DueDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        bill.DueDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Balance"))
                            {
                                #region Validations for Balance
                                if (dr["Balance"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Balance"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Balance( " + dr["Balance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.Balance = dr["Balance"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.Balance = dr["Balance"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            bill.Balance = dr["Balance"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.Balance = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Balance"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            coll.Add(bill);
                            #endregion
                        }
                        else
                        {
                            #region  if bill is not null
                            OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of LineDescription
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations of LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    if (dr["LineAmount"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount (" + dr["LineAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                #endregion
                            }
                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                            if (dt.Columns.Contains("LineCustomerRef"))
                            {
                                #region Validations of LineCustomerRef
                                if (dr["LineCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CustomerRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef);
                            }

                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }

                                    // bug 480
                                    #region Account Validation
                                    List<Intuit.Ipp.Data.Account> Accountdata = null;
                                    string AccountName = string.Empty;
                                    AccountName = AccountRef.Name.Trim();
                                    Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                    if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                    {
                                        //480
                                        Messages m1 = new Messages();
                                        m1.buttonCancel.Text = "Add";
                                        m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                        m1.ShowDialog();
                                        //Add
                                        if (m1.DialogResult.ToString().ToLower() == "ok")
                                        {
                                            CommonUtilities cUtility = new CommonUtilities();
                                            await  cUtility.CreateAccountInQBO(AccountName);
                                        }
                                        //Quit
                                        if (m1.DialogResult.ToString().ToLower() == "no")
                                        {
										   break;
                                            //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                            //{
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                            //    bkWorker.CancelAsync();
                                            //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                            //}
                                            //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                            //{
                                            //    axisForm.IsBusy = false;
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                            //    bkWorker.CancelAsync();

                                            //}
                                            //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                            //{
                                            //    axisForm.IsBusy = false;
                                            //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                            //    bkWorker.CancelAsync();
                                            //}

                                           // DataProcessingBlocks.CommonUtilities.CloseSplash();

                                          //  TransactionImporter.ImportSplashScreen.CloseForm();
                                        }
                                        //Ignore
                                        if (m1.DialogResult.ToString() == "Ignore")
                                        {

                                        }
                                        //IgnoreAll
                                        if (m1.DialogResult.ToString() == "Abort")
                                        {
                                            CommonUtilities.GetInstance().ignoreAllAccount = true;
                                        }
                                    }


                                    #endregion
                                }


                                #endregion
                             
                            }
                            if (AccountRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            }

                            ////Improvement::493
                            //if (dt.Columns.Contains("LineAcctNum"))
                            //{
                            //    #region Validations of LineAcctNum
                            //    if (dr["LineAcctNum"].ToString() != string.Empty)
                            //    {
                            //        if (dr["LineAcctNum"].ToString().Length > 15)
                            //        {
                            //            if (isIgnoreAll == false)
                            //            {
                            //                string strMessages = "This LineAcctNum (" + dr["LineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            //                if (Convert.ToString(result) == "Cancel")
                            //                {
                            //                    continue;
                            //                }
                            //                if (Convert.ToString(result) == "No")
                            //                {
                            //                    return null;
                            //                }
                            //                if (Convert.ToString(result) == "Ignore")
                            //                {
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString().Substring(0, 15);
                            //                }
                            //                if (Convert.ToString(result) == "Abort")
                            //                {
                            //                    isIgnoreAll = true;
                            //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //                }
                            //            }
                            //            else
                            //            {
                            //                AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                            //        }
                            //    }
                            //    #endregion

                            //}

                            //if (AccountRef.AcctNum != null)
                            //{
                            //    AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            //}

                            //Axis 11.2 bug no.337
                            OnlineEntities.ClassRef AccountClassRef = new OnlineEntities.ClassRef();
                            if (dt.Columns.Contains("LineAccountClass"))
                            {
                                #region Validations of LineAccountClass
                                if (dr["LineAccountClass"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountClass"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountClass (" + dr["LineAccountClass"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountClassRef.Name = dr["LineAccountClass"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (AccountClassRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.ClassRef.Add(AccountClassRef);
                            }
                            //End Axis 11.2 bug no.337
                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.TaxCodeRef lineTaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                            {
                                #region Validations of LineAccountTaxCodeRef
                                if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (lineTaxCodeRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.TaxCodeRef.Add(lineTaxCodeRef);
                            }
                            if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.APAccountRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                            {
                                Line.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);                               
                            }
                         
                            //ItembasedExpansionlinedetail....

                            OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            CustomerRef = new OnlineEntities.CustomerRef();
                            TaxCodeRef = new OnlineEntities.TaxCodeRef();
                            OnlineEntities.Line ItemBasedExpenseLine = new OnlineEntities.Line();

                            if (dt.Columns.Contains("LineItemDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineItemDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemDescription (" + dr["LineItemDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemAmount"))
                            {
                                #region Validations for LineItemAmount
                                if (dr["LineItemAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemAmount( " + dr["LineItemAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemRef"))
                            {
                                #region Validations of LineItemRef
                                if (dr["LineItemRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineItemRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemQty"))
                            {
                                #region Validations for LineItemQty
                                if (dr["LineItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemUnitPrice"))
                            {
                                #region Validations for LineItemUnitPrice
                                if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemTaxCodeRef"))
                            {
                                #region Validations of LineItemTaxCodeRef
                                if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("LineItemCustomerRef"))
                            {
                                #region Validations of LineItemCustomerRef
                                if (dr["LineItemCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemCustomerRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemCustomerRef (" + dr["LineItemCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineItemCustomerRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemBillableStatus"))
                            {
                                #region Validations of LineItemBillableStatus
                                if (dr["LineItemBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemBillableStatus"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemBillableStatus (" + dr["LineItemBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (ItemRef.Name != null || ItemRef.SKU != null)
                            {
                                ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (ClassRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                            }
                            if (CustomerRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                            }
                            if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                            {
                                ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                               
                            }
                          
                            if (Line.AccountBasedExpenseLineDetail.Count > 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                bill.Line.Add(ItemBasedExpenseLine);
                                bill.Line.Add(Line);
                            }
                            else if (Line.AccountBasedExpenseLineDetail.Count > 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count == 0)
                            {
                                bill.Line.Add(Line);
                            }
                            else if (Line.AccountBasedExpenseLineDetail.Count == 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                bill.Line.Add(ItemBasedExpenseLine);
                            }
                            else if (Line.AccountBasedExpenseLineDetail.Count == 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count == 0)
                            {
                                bill.Line.Add(ItemBasedExpenseLine);
                                bill.Line.Add(Line);
                            }

                            #region Tax
                            if (CommonUtilities.GetInstance().CountryVersion != "US")
                            {
                                OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                                TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                                OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();
                                TaxRateRef = new OnlineEntities.TaxRateRef();
                              
                                //586
                                if (CommonUtilities.GetInstance().CountryVersion == "US")
                                {
                                    if (dt.Columns.Contains("TxnTaxCodeRefName"))
                                    {
                                        #region Validations of TxnTaxCodeRefName
                                        if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                        {
                                            if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        #endregion
                                    }
                                }

                                if (dt.Columns.Contains("TaxLineAmount"))
                                {
                                    #region Validations of TaxLineAmount
                                    if (dr["TaxLineAmount"].ToString() != string.Empty)
                                    {
                                        if (dr["TaxLineAmount"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TaxLineAmount (" + dr["TaxLineAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TaxLine.Amount = dr["TaxLineAmount"].ToString().Substring(0, 100);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("TaxRateRef"))
                                {
                                    #region Validations of TaxRateRef
                                    if (dr["TaxRateRef"].ToString() != string.Empty)
                                    {
                                        if (dr["TaxRateRef"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TaxRateRef (" + dr["TaxRateRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TaxRateRef.Name = dr["TaxRateRef"].ToString().Substring(0, 100);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                if (TaxRateRef.Name != null)
                                {
                                    TaxLineDetail.TaxRateRef.Add(TaxRateRef);
                                }

                                if (dt.Columns.Contains("TaxPercentBased"))
                                {
                                    #region Validations of TaxPercentBased
                                    if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                                    {

                                        int result = 0;
                                        if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                        {
                                            TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                            {
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "TaxPercentBased";
                                                }
                                                else
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                }
                                            }

                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("TaxPercent"))
                                {
                                    #region Validations for TaxPercent
                                    if (dr["TaxPercent"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("NetAmountTaxable"))
                                {
                                    #region Validations of NetAmountTaxable
                                    if (dr["NetAmountTaxable"].ToString() != string.Empty)
                                    {
                                        if (dr["NetAmountTaxable"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This NetAmountTaxable (" + dr["NetAmountTaxable"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString().Substring(0, 100);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                        }
                                    }
                                    #endregion
                                }


                                if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef != null)
                                {
                                    TaxLine.TaxLineDetail.Add(TaxLineDetail);
                                }

                                if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                                {                                    
                                    TxnTaxDetail.TaxLine.Add(TaxLine);
                                }

                                //586
                                if (TxnTaxCodeRef.Name != null)
                                {
                                    TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                                }

                            }
                            #endregion
                           
                            #endregion
                        }

                        }
                    else
                    {
                        bill = new Bill();                     
                        

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of TxnDate
                            DateTime SODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            bill.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        bill.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    bill.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bill.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bill.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    bill.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }
                        //Axis 11.2 bug no. 337
                        OnlineEntities.DepartmentRef DepartmentRef = new OnlineEntities.DepartmentRef();
                        if (dt.Columns.Contains("DepartmentRef"))
                        {
                            #region Validations of DepartmentRef
                            if (dr["DepartmentRef"].ToString() != string.Empty)
                            {
                                if (dr["DepartmentRef"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepartmentRef (" + dr["DepartmentRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                    }
                                }
                                else
                                {
                                    DepartmentRef.Name = dr["DepartmentRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (DepartmentRef.Name != null)
                        {
                            bill.DepartmentRef.Add(DepartmentRef);
                        }
                        OnlineEntities.CurrencyRef CurrencyRef = new OnlineEntities.CurrencyRef();
                        if (dt.Columns.Contains("CurrencyRef"))
                        {
                            #region Validations of CurrencyRef
                            if (dr["CurrencyRef"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRef"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (CurrencyRef.Name != null)
                        {
                            bill.CurrencyRef.Add(CurrencyRef);
                        }
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations of ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                if (dr["ExchangeRate"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate (" + dr["ExchangeRate"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bill.ExchangeRate = dr["ExchangeRate"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TransactionLocationType"))
                        {
                            #region Validations of TransactionLocationType
                            if (dr["TransactionLocationType"].ToString() != string.Empty)
                            {
                                if (dr["TransactionLocationType"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TransactionLocationType (" + dr["TransactionLocationType"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bill.TransactionLocationType = dr["TransactionLocationType"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bill.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                    }
                                }
                                else
                                {
                                    bill.TransactionLocationType = dr["TransactionLocationType"].ToString();
                                }
                            }
                            #endregion
                        }

                        //End Axis 11.2 bug no. 337
                        OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                        OnlineEntities.Line Line = new OnlineEntities.Line();
                        OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                        if (dt.Columns.Contains("LineDescription"))
                        {
                            #region Validations of LineDescription
                            if (dr["LineDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineDescription"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.Descreption = dr["LineDescription"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineAmount"))
                        {
                            #region Validations of LineAmount
                            if (dr["LineAmount"].ToString() != string.Empty)
                            {
                                if (dr["LineAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAmount (" + dr["LineAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.LineAmount= dr["LineAmount"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.LineAmount = dr["LineAmount"].ToString();
                                }
                            }
                            #endregion
                        }
                        OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                        if (dt.Columns.Contains("LineCustomerRef"))
                        {
                            #region Validations of LineCustomerRef
                            if (dr["LineCustomerRef"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomerRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (CustomerRef.Name != null)
                        {
                            AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef);
                        }

                        OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();
                        if (dt.Columns.Contains("LineAccountRef"))
                        {
                            #region Validations of LineAccountRef
                            if (dr["LineAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountRef.Name = dr["LineAccountRef"].ToString();
                                }
                                // bug 480
                                #region Account Validation
                                List<Intuit.Ipp.Data.Account> Accountdata = null;
                                string AccountName = string.Empty;
                                AccountName = AccountRef.Name.Trim();
                                Accountdata = await CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                if (Accountdata != null)//bug 480
                                {
                                    if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                    {
                                        //480
                                        Messages m1 = new Messages();
                                        m1.buttonCancel.Text = "Add";
                                        m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                        m1.ShowDialog();
                                        //Add
                                        if (m1.DialogResult.ToString().ToLower() == "ok")
                                        {
                                            CommonUtilities cUtility = new CommonUtilities();
                                            await  cUtility.CreateAccountInQBO(AccountName);
                                        }
                                        //Quit
                                        if (m1.DialogResult.ToString().ToLower() == "no")
                                        {
                                        break;
                                        //if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
                                        //{
                                        //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                                        //    bkWorker.CancelAsync();
                                        //    //DataProcessingBlocks.CommonUtilities.CloseImportSplash();

                                        //}
                                        //if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
                                        //{
                                        //    axisForm.IsBusy = false;
                                        //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                                        //    bkWorker.CancelAsync();

                                        //}
                                        //if (axisForm.backgroundWorkerProcessLoader.IsBusy)
                                        //{
                                        //    axisForm.IsBusy = false;
                                        //    bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                                        //    bkWorker.CancelAsync();
                                        //}

                                      //  DataProcessingBlocks.CommonUtilities.CloseSplash();
                                      //  TransactionImporter.ImportSplashScreen.m_frmSplash.CloseImportSplash();
                                        //TransactionImporter.ImportSplashScreen.CloseForm();
                                    }
                                    //Ignore
                                    if (m1.DialogResult.ToString() == "Ignore")
                                    {

                                        }
                                        //IgnoreAll
                                        if (m1.DialogResult.ToString() == "Abort")
                                        {
                                            CommonUtilities.GetInstance().ignoreAllAccount = true;
                                        }
                                    }
                                }

                                #endregion
                            }
                            #endregion
                        }

                        ////Improvement::493
                        //if (dt.Columns.Contains("LineAcctNum"))
                        //{
                        //    #region Validations of LineAcctNum
                        //    if (dr["LineAcctNum"].ToString() != string.Empty)
                        //    {
                        //        if (dr["LineAcctNum"].ToString().Length > 15)
                        //        {
                        //            if (isIgnoreAll == false)
                        //            {
                        //                string strMessages = "This LineAcctNum (" + dr["LineAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                        //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                        //                if (Convert.ToString(result) == "Cancel")
                        //                {
                        //                    continue;
                        //                }
                        //                if (Convert.ToString(result) == "No")
                        //                {
                        //                    return null;
                        //                }
                        //                if (Convert.ToString(result) == "Ignore")
                        //                {
                        //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString().Substring(0, 15);
                        //                }
                        //                if (Convert.ToString(result) == "Abort")
                        //                {
                        //                    isIgnoreAll = true;
                        //                    AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                        //            }
                        //        }
                        //        else
                        //        {
                        //            AccountRef.AcctNum = dr["LineAcctNum"].ToString();
                        //        }
                        //    }
                        //    #endregion

                        //}

                        //if (AccountRef.AcctNum != null)
                        //{
                        //    AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                        //}

                        //Axis 11.2 bug no.337
                        OnlineEntities.ClassRef AccountClassRef = new OnlineEntities.ClassRef();
                        if (dt.Columns.Contains("LineAccountClass"))
                        {
                            #region Validations of LineAccountClass
                            if (dr["LineAccountClass"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountClass"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountClass (" + dr["LineAccountClass"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountClassRef.Name = dr["LineAccountClass"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountClassRef.Name = dr["LineAccountClass"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (AccountClassRef.Name != null)
                        {
                            AccountBasedExpenseLineDetail.ClassRef.Add(AccountClassRef);
                        }
                        //End Axis 11.2 bug no.337
                        if (AccountRef.Name != null)
                        {
                            AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                        }

                        if (dt.Columns.Contains("LineBillableStatus"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["LineBillableStatus"].ToString() != string.Empty)
                            {
                                if (dr["LineBillableStatus"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.TaxCodeRef lineTaxCodeRef = new OnlineEntities.TaxCodeRef();

                        if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                        {
                            #region Validations of LineAccountTaxCodeRef
                            if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    lineTaxCodeRef.Name = dr["LineAccountTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (lineTaxCodeRef.Name != null)
                        {
                            AccountBasedExpenseLineDetail.TaxCodeRef.Add(lineTaxCodeRef);
                        }
                        if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.APAccountRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                        {
                            Line.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);
                            
                        }


                        //ItembasedExpansionlinedetail....

                        OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                        OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                        OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                        CustomerRef = new OnlineEntities.CustomerRef();
                        TaxCodeRef = new OnlineEntities.TaxCodeRef();
                        OnlineEntities.Line ItemBasedExpenseLine = new OnlineEntities.Line();


                        if (dt.Columns.Contains("LineItemDescription"))
                        {
                            #region Validations of Associate
                            if (dr["LineItemDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineItemDescription"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemDescription (" + dr["LineItemDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.Descreption = dr["LineItemDescription"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemAmount"))
                        {
                            #region Validations for LineItemAmount
                            if (dr["LineItemAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineItemAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemAmount( " + dr["LineItemAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.LineAmount = dr["LineItemAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemRef"))
                        {
                            #region Validations of LineItemRef
                            if (dr["LineItemRef"].ToString() != string.Empty)
                            {
                                if (dr["LineItemRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.Name = dr["LineItemRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        //bug 486
                        if (dt.Columns.Contains("SKU"))
                        {
                            #region Validations of SKU
                            if (dr["SKU"].ToString() != string.Empty)
                            {
                                if (dr["SKU"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.SKU = dr["SKU"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineClassRef"))
                        {
                            #region Validations of LineClassRef
                            if (dr["LineClassRef"].ToString() != string.Empty)
                            {
                                if (dr["LineClassRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef.Name = dr["LineClassRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemQty"))
                        {
                            #region Validations for LineItemQty
                            if (dr["LineItemQty"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemUnitPrice"))
                        {
                            #region Validations for LineItemUnitPrice
                            if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemTaxCodeRef"))
                        {
                            #region Validations of LineItemTaxCodeRef
                            if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemCustomerRef"))
                        {
                            #region Validations of LineItemCustomerRef
                            if (dr["LineItemCustomerRef"].ToString() != string.Empty)
                            {
                                if (dr["LineItemCustomerRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemCustomerRef (" + dr["LineItemCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef.Name = dr["LineItemCustomerRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["LineItemCustomerRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemBillableStatus"))
                        {
                            #region Validations of LineItemBillableStatus
                            if (dr["LineItemBillableStatus"].ToString() != string.Empty)
                            {
                                if (dr["LineItemBillableStatus"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemBillableStatus (" + dr["LineItemBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = dr["LineItemBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        //bug 486
                        if (ItemRef.Name != null||ItemRef.SKU!=null)
                        {
                            ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                        }
                        if (TaxCodeRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                        }
                        if (ClassRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                        }
                        if (CustomerRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                        }
                        if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                        {
                            ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);                           
                        }
                     

                        if (Line.AccountBasedExpenseLineDetail.Count > 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            bill.Line.Add(ItemBasedExpenseLine);
                            bill.Line.Add(Line);
                        }
                        else if (Line.AccountBasedExpenseLineDetail.Count > 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count == 0)
                        {
                            bill.Line.Add(Line);
                        }
                        else if (Line.AccountBasedExpenseLineDetail.Count == 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            bill.Line.Add(ItemBasedExpenseLine);
                        }
                        else if (Line.AccountBasedExpenseLineDetail.Count == 0 && ItemBasedExpenseLine.ItemBasedExpenseLineDetail.Count == 0)
                        {
                            bill.Line.Add(ItemBasedExpenseLine);
                            bill.Line.Add(Line);
                        }

                        #region Tax

                        TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                   OnlineEntities.TaxLineDetail     TaxLineDetail = new OnlineEntities.TaxLineDetail();
                       TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                   OnlineEntities.TaxLine    TaxLine = new OnlineEntities.TaxLine();
                       TaxRateRef = new OnlineEntities.TaxRateRef();

                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                        {
                            if (dt.Columns.Contains("TxnTaxCodeRefName"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                            }
                        }
                        if (dt.Columns.Contains("TotalTax"))
                        {
                            #region Validations of TotalTax
                            if (dr["TotalTax"].ToString() != string.Empty)
                            {
                                if (dr["TotalTax"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TotalTax (" + dr["TotalTax"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxLineAmount"))
                        {
                            #region Validations of TaxLineAmount
                            if (dr["TaxLineAmount"].ToString() != string.Empty)
                            {
                                if (dr["TaxLineAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TaxLineAmount (" + dr["TaxLineAmount"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxPercentBased"))
                        {
                            #region Validations of TaxPercentBased
                            if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                {
                                    TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                    {
                                        TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "TaxPercentBased";
                                        }
                                        else
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxPercent"))
                        {
                            #region Validations for TaxPercent
                            if (dr["TaxPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (CommonUtilities.GetInstance().CountryVersion != "US")
                        {
                            if (dt.Columns.Contains("NetAmountTaxable"))
                            {
                                #region Validations of NetAmountTaxable
                                if (dr["NetAmountTaxable"].ToString() != string.Empty)
                                {
                                    if (dr["NetAmountTaxable"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This NetAmountTaxable (" + dr["NetAmountTaxable"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLineDetail.NetAmountTaxable = dr["NetAmountTaxable"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxRateRef"))
                            {
                                #region Validations of TaxRateRef
                                if (dr["TaxRateRef"].ToString() != string.Empty)
                                {
                                    if (dr["TaxRateRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxRateRef (" + dr["TaxRateRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxRateRef.Name = dr["TaxRateRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxRateRef.Name = dr["TaxRateRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (TaxRateRef.Name != null)
                            {
                                TaxLineDetail.TaxRateRef.Add(TaxRateRef);
                            }

                        }

                        if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef != null)
                        {
                            TaxLine.TaxLineDetail.Add(TaxLineDetail);
                        }

                        if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                        {
                            TxnTaxDetail.TaxLine.Add(TaxLine);
                        }
                        if (TxnTaxDetail.TxnTaxCodeRef.Count != 0 || TxnTaxDetail.TaxLine.Count != 0)
                        {
                            bill.TxnTaxDetail.Add(TxnTaxDetail);
                        }

                        //586
                        if (TxnTaxCodeRef.Name != null)
                        {
                            TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                        }

                        #endregion


                        OnlineEntities.VendorRef vendorRef = new OnlineEntities.VendorRef();
                        if (dt.Columns.Contains("VendorRefName"))
                        {
                            #region Validations of VendorRefName
                            if (dr["VendorRefName"].ToString() != string.Empty)
                            {
                                if (dr["VendorRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorRefName (" + dr["VendorRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            vendorRef.Name = dr["VendorRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vendorRef.Name = dr["VendorRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendorRef.Name = dr["VendorRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorRef.Name = dr["VendorRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (vendorRef.Name != null)
                        {
                            bill.VendorRef.Add(vendorRef);
                        }

                        if (dt.Columns.Contains("GlobalTaxCalculation"))
                        {
                            #region Validations of bill.GlobalTaxCal
                            if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                            {
                                if (dr["GlobalTaxCalculation"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString();
                                    }
                                }
                                else
                                {
                                    bill.GlobalTaxCal = dr["GlobalTaxCalculation"].ToString();
                                }
                            }
                            #endregion
                        }


                        OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                        if (dt.Columns.Contains("APAccountRefName"))
                        {
                            #region Validations of APAccountRefName
                            if (dr["APAccountRefName"].ToString() != string.Empty)
                            {
                                if (dr["APAccountRefName"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This APAccountRefName (" + dr["APAccountRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            APAccountRef.Name = dr["APAccountRefName"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            APAccountRef.Name = dr["APAccountRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    APAccountRef.Name = dr["APAccountRefName"].ToString();
                                }

                                // bug 480
                                #region Account Validation
                                List<Intuit.Ipp.Data.Account> Accountdata = null;
                                string AccountName = string.Empty;
                                AccountName = APAccountRef.Name.Trim();
                                Accountdata = await  CommonUtilities.GetAccountExistsInOnlineQBO(AccountName);
                                if (Accountdata.Count == 0 && CommonUtilities.GetInstance().ignoreAllAccount == false)
                                {
                                    //480
                                    Messages m1 = new Messages();
                                    m1.buttonCancel.Text = "Add";
                                    m1.SetDialogBox("Account Name \"" + AccountName + "\" that has been specified has not been found in QuickBooks. ", MessageBoxIcon.Question);
                                    m1.ShowDialog();
                                    //Add
                                    if (m1.DialogResult.ToString().ToLower() == "ok")
                                    {
                                        CommonUtilities cUtility = new CommonUtilities();
                                        await  cUtility.CreateAccountInQBO(AccountName);
                                    }
                                    //Quit
                                    if (m1.DialogResult.ToString().ToLower() == "no")
                                    {
                                        break;
        
                                    }
                                    //Ignore
                                    if (m1.DialogResult.ToString() == "Ignore")
                                    {

                                    }
                                    //IgnoreAll
                                    if (m1.DialogResult.ToString() == "Abort")
                                    {
                                        CommonUtilities.GetInstance().ignoreAllAccount = true;
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                        if (APAccountRef.Name != null)
                        {
                            bill.APAccountRef.Add(APAccountRef);
                        }

                        ////Improvement::493
                        //if (dt.Columns.Contains("APAcctNum"))
                        //{
                        //    #region Validations of APAcctNum
                        //    if (dr["APAcctNum"].ToString() != string.Empty)
                        //    {
                        //        if (dr["APAcctNum"].ToString().Length > 15)
                        //        {
                        //            if (isIgnoreAll == false)
                        //            {
                        //                string strMessages = "This APAcctNum (" + dr["APAcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                        //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                        //                if (Convert.ToString(result) == "Cancel")
                        //                {
                        //                    continue;
                        //                }
                        //                if (Convert.ToString(result) == "No")
                        //                {
                        //                    return null;
                        //                }
                        //                if (Convert.ToString(result) == "Ignore")
                        //                {
                        //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString().Substring(0, 15);
                        //                }
                        //                if (Convert.ToString(result) == "Abort")
                        //                {
                        //                    isIgnoreAll = true;
                        //                    APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                        //            }
                        //        }
                        //        else
                        //        {
                        //            APAccountRef.AcctNum = dr["APAcctNum"].ToString();
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //if (APAccountRef.AcctNum != null)
                        //{
                        //    bill.APAccountRef.Add(APAccountRef);
                        //}

                        OnlineEntities.SalesTermRef SalesTermRef = new OnlineEntities.SalesTermRef();
                        if (dt.Columns.Contains("SalesTermRefName"))
                        {
                            #region Validations of SalesTermRefName
                            if (dr["SalesTermRefName"].ToString() != string.Empty)
                            {
                                if (dr["SalesTermRefName"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesTermRefName (" + dr["SalesTermRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesTermRef.Name = dr["SalesTermRefName"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesTermRef.Name = dr["SalesTermRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesTermRef.Name = dr["SalesTermRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesTermRef.Name = dr["SalesTermRefName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (SalesTermRef.Name != null)
                        {
                            bill.SalesTermRef.Add(SalesTermRef);

                        }
                        //bug 504
                        if (dt.Columns.Contains("DueDate"))
                        {
                            #region Validations of DueDate
                            DateTime SODate = new DateTime();
                            if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["DueDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DueDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                bill.DueDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                bill.DueDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            bill.DueDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        bill.DueDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    bill.DueDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Balance"))
                        {
                            #region Validations for Balance
                            if (dr["Balance"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Balance"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Balance( " + dr["Balance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bill.Balance = dr["Balance"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bill.Balance = dr["Balance"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bill.Balance = dr["Balance"].ToString();
                                    }
                                }
                                else
                                {
                                    bill.Balance = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Balance"].ToString()));
                                }
                            }

                            #endregion
                        }
                        coll.Add(bill);
                       
                    }
                }


                else
                {
                    return null;
                }
            }
            #endregion

            #endregion
            return coll;

        }

       
    }
}
