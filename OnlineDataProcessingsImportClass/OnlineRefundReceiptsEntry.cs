﻿
using System;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    [XmlRootAttribute("RefundReceipt", Namespace = "", IsNullable = false)]

    public class RefundReceipt
    {

        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TxnDate;
        private string m_TxnStatus;
        private Collection<OnlineEntities.CustomField> m_CustomField = new Collection<OnlineEntities.CustomField>();
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
        private Collection<OnlineEntities.TxnTaxDetail> m_TxnTaxDetail = new Collection<OnlineEntities.TxnTaxDetail>();
        private Collection<OnlineEntities.CustomerRef> m_CustomerRef = new Collection<OnlineEntities.CustomerRef>();
        private string m_CustomerMemo;
        private Collection<BillAddr> m_BillAddr = new Collection<BillAddr>();
        private Collection<ShipAddr> m_ShipAddr = new Collection<ShipAddr>();
        private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();
        private Collection<ShipMethodRef> m_ShipMethodRef = new Collection<ShipMethodRef>();
        private string m_TrackingNum;
        private string m_TotalAmt;
        private string m_ShipDate;
        private string m_ApplyTaxAfterDiscount;
        private string m_PrintStatus;
        private string m_EmailStatus;
        private string m_BillEmail;
        private string m_PaymentRefNumber;
        private Collection<PaymentMethodRef> m_PaymentMethodRef = new Collection<PaymentMethodRef>();
        private Collection<DepositToAccountRef> m_DepositToAccountRef = new Collection<DepositToAccountRef>();
        private Collection<CurrencyRef> m_CurrencyRef = new Collection<CurrencyRef>();
        private string m_ExchangeRate;
        private string m_GlobalTaxCalculation;
        private string m_HomeTotalAmt;

        //594
        private bool m_isAppend;

        #endregion

        #region Constructor
        public RefundReceipt()
        {
        }
        #endregion

        #region properties

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }


        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public Collection<OnlineEntities.CustomField> CustomField
        {
            get { return m_CustomField; }
            set { m_CustomField = value; }
        }


        public string TxnStatus
        {
            get { return m_TxnStatus; }
            set { m_TxnStatus = value; }
        }


        public string CustomerMemo
        {
            get { return m_CustomerMemo; }
            set { m_CustomerMemo = value; }
        }

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }


        public Collection<ShipMethodRef> ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }
        public string TrackingNum
        {
            get { return m_TrackingNum; }
            set { m_TrackingNum = value; }
        }


        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }


        public string PrintStatus
        {
            get { return m_PrintStatus; }
            set { m_PrintStatus = value; }
        }


        public string EmailStatus
        {
            get { return m_EmailStatus; }
            set { m_EmailStatus = value; }
        }


        public string BillEmail
        {
            get { return m_BillEmail; }
            set { m_BillEmail = value; }
        }
        public string PaymentRefNumber
        {
            get { return m_PaymentRefNumber; }
            set { m_PaymentRefNumber = value; }
        }


        public string ApplyTaxAfterDiscount
        {
            get { return m_ApplyTaxAfterDiscount; }
            set { m_ApplyTaxAfterDiscount = value; }
        }


        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }


        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }


        public string HomeTotalAmt
        {
            get { return m_HomeTotalAmt; }
            set { m_HomeTotalAmt = value; }
        }
        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }
        public Collection<OnlineEntities.TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
        public Collection<OnlineEntities.CustomerRef> CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public Collection<BillAddr> BillAddr
        {
            get { return m_BillAddr; }
            set { m_BillAddr = value; }
        }


        public Collection<ShipAddr> ShipAddr
        {
            get { return m_ShipAddr; }
            set { m_ShipAddr = value; }
        }

        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }
        }

        public Collection<PaymentMethodRef> PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }

        public Collection<DepositToAccountRef> DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
            set { m_DepositToAccountRef = value; }
        }

        public Collection<CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }

        #endregion

        #region  Public Methods
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        /// <summary>
        /// Creating new Refundrecipt transaction in quickbook online. 
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBORefundReceipt(OnlineDataProcessingsImportClass.RefundReceipt coll)
        {
            bool flag = false;
            int linecount = 1;

            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.RefundReceipt RefundReceiptAdded = null;
            Intuit.Ipp.Data.RefundReceipt addedRefundReceipt = new Intuit.Ipp.Data.RefundReceipt();
            RefundReceipt RefundReceipt = new RefundReceipt();
            RefundReceipt = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line DiscountLine = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            try
            {
                #region Add Refund Receipt

                //594
                if (RefundReceipt.isAppend == true)
                {
                    addedRefundReceipt.sparse = true;
                }

                if (RefundReceipt.DocNumber != null)
                {
                    addedRefundReceipt.DocNumber = RefundReceipt.DocNumber;
                }
                else { addedRefundReceipt.AutoDocNumber = true; addedRefundReceipt.AutoDocNumberSpecified = true; }

                if (RefundReceipt.TxnDate != null)
                {
                    try
                    {
                        addedRefundReceipt.TxnDate = Convert.ToDateTime(RefundReceipt.TxnDate);
                        addedRefundReceipt.TxnDateSpecified = true;
                    }
                    catch { }
                }

                addedRefundReceipt.PrivateNote = RefundReceipt.PrivateNote;

                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in RefundReceipt.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }

                addedRefundReceipt.CustomField = custom_online;
                #endregion

                addedRefundReceipt.TxnStatus = RefundReceipt.TxnStatus;

                foreach (var l in RefundReceipt.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var s in l.SalesItemLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }


                                        else if (i.Name != null)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }

                                    else
                                    {
                                        foreach (var j in s.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }

                                    }
                                }

                                foreach (var i in s.ClassRef)
                                {

                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (RefundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    //Line.Amount = decimal.Round(Line.Amount, 2);
                                                    //Line.Amount = CommonUtilities.GetInstance().GetLineAmount(Line.Amount);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 7);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (RefundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (s.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(s.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (s.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(s.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }

                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedRefundReceipt.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var s in l.SalesItemLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in s.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var j in s.ItemRef)
                                        {
                                            if (j.SKU != null)
                                            {
                                                var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(j.SKU.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                    name = item.FullyQualifiedName;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = name,
                                                    Value = id
                                                };
                                            }
                                        }
                                    }
                                }
                                foreach (var i in s.ClassRef)
                                {

                                    if (i.Name != string.Empty)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());

                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (RefundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 7);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (RefundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (s.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(s.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (s.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(s.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }


                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                    
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        addedRefundReceipt.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                foreach (var txntaxCode in RefundReceipt.TxnTaxDetail)
                {
                    #region TxnTaxDetail
                    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    if (txntaxCode.TxnTaxCodeRef != null)
                    {
                        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                        {
                            string Taxvalue = string.Empty;

                            if (taxRef.Name != null)
                            {
                                var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxRef.Name.Trim());
                                if (dataTax != null)
                                {
                                    foreach (var tax in dataTax)
                                    {
                                        Taxvalue = tax.Id;
                                    }

                                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                    {
                                        name = taxRef.Name,
                                        Value = Taxvalue
                                    };
                                }
                            }
                        }
                    }

                    if (txnTaxDetail != null)
                    {
                        addedRefundReceipt.TxnTaxDetail = txnTaxDetail;
                    }

                    #endregion
                }


                foreach (var customerRef in RefundReceipt.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }
                        addedRefundReceipt.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }

                }

                if (RefundReceipt.CustomerMemo != null)
                {
                    addedRefundReceipt.CustomerMemo = new MemoRef()
                    {
                        Value = RefundReceipt.CustomerMemo
                    };
                }
                PhysicalAddress billAddr = new PhysicalAddress();
                if (RefundReceipt.BillAddr.Count > 0)
                {
                    foreach (var billaddress in RefundReceipt.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedRefundReceipt.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (RefundReceipt.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in RefundReceipt.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedRefundReceipt.ShipAddr = shipAddr;
                }


                foreach (var className in RefundReceipt.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetItemExistsInOnlineQBO(className.Name.Trim());
                       
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedRefundReceipt.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }


                if (RefundReceipt.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in RefundReceipt.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedRefundReceipt.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (RefundReceipt.ShipDate != null)
                {
                    try
                    {
                        addedRefundReceipt.ShipDate = Convert.ToDateTime(RefundReceipt.ShipDate);
                        addedRefundReceipt.ShipDateSpecified = true;
                    }
                    catch { }
                }
                addedRefundReceipt.TrackingNum = RefundReceipt.TrackingNum;

                if (RefundReceipt.PrintStatus != null)
                {
                    if (RefundReceipt.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedRefundReceipt.PrintStatus = PrintStatusEnum.NotSet;
                        addedRefundReceipt.PrintStatusSpecified = true;
                    }
                    else if (RefundReceipt.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedRefundReceipt.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedRefundReceipt.PrintStatusSpecified = true;
                    }
                    else if (RefundReceipt.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedRefundReceipt.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedRefundReceipt.PrintStatusSpecified = true;
                    }
                }
                if (RefundReceipt.EmailStatus != null)
                {
                    if (RefundReceipt.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedRefundReceipt.EmailStatus = EmailStatusEnum.NotSet;
                        addedRefundReceipt.EmailStatusSpecified = true;
                    }
                    else if (RefundReceipt.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedRefundReceipt.EmailStatus = EmailStatusEnum.EmailSent;
                        addedRefundReceipt.EmailStatusSpecified = true;
                    }
                    else if (RefundReceipt.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedRefundReceipt.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedRefundReceipt.EmailStatusSpecified = true;
                    }
                }
                if (RefundReceipt.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = RefundReceipt.BillEmail;
                    addedRefundReceipt.BillEmail = EmailAddress;
                }
                if (RefundReceipt.PaymentRefNumber != null)
                {
                    addedRefundReceipt.PaymentRefNum = RefundReceipt.PaymentRefNumber;
                }


                foreach (var paymentMethod in RefundReceipt.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedRefundReceipt.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }


                foreach (var accountData in RefundReceipt.DepositToAccountRef)
                {
                    QueryService<Account> classQueryService = new QueryService<Account>(serviceContext);

                    string classValue = string.Empty;
                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                        addedRefundReceipt.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                  
                }


                if (RefundReceipt.ApplyTaxAfterDiscount != null)
                {
                    addedRefundReceipt.ApplyTaxAfterDiscount = Convert.ToBoolean(RefundReceipt.ApplyTaxAfterDiscount);
                    addedRefundReceipt.ApplyTaxAfterDiscountSpecified = true;
                }

                if (RefundReceipt.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in RefundReceipt.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addedRefundReceipt.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (RefundReceipt.ExchangeRate != null)
                {
                    addedRefundReceipt.ExchangeRate = Convert.ToDecimal(RefundReceipt.ExchangeRate);
                    addedRefundReceipt.ExchangeRateSpecified = true;
                }

                #region GlobalTaxCalculation

                if (RefundReceipt.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (RefundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedRefundReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedRefundReceipt.GlobalTaxCalculationSpecified = true;
                        }
                        else if (RefundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedRefundReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedRefundReceipt.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedRefundReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedRefundReceipt.GlobalTaxCalculationSpecified = true;
                        }
                    }

                }

                #endregion

                #endregion

                CommonUtilities.GetInstance().newSKUItem = false;
                RefundReceiptAdded = new Intuit.Ipp.Data.RefundReceipt();
                RefundReceiptAdded = dataService.Add<Intuit.Ipp.Data.RefundReceipt>(addedRefundReceipt);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (RefundReceiptAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = RefundReceiptAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = RefundReceiptAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }
        /// <summary>
        /// updating sales receipt entry in quickbook online by the transaction Id
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="salesReceiptid"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBORefundReceipt(OnlineDataProcessingsImportClass.RefundReceipt coll, string refundReceiptid, string syncToken, Intuit.Ipp.Data.RefundReceipt PreviousData = null)
        {
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.RefundReceipt RefundReceiptAdded = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.RefundReceipt addedRefundReceipt = new Intuit.Ipp.Data.RefundReceipt();
            RefundReceipt refundReceipt = new RefundReceipt();
            refundReceipt = coll;
            Intuit.Ipp.Data.SalesItemLineDetail lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];
            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;
            try
            {
                #region Add Sales Receipt

                //594
                if (refundReceipt.isAppend == true)
                {
                    addedRefundReceipt = PreviousData;
                    addedRefundReceipt.sparse = true;
                    addedRefundReceipt.sparseSpecified = true;
                    if (addedRefundReceipt.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedRefundReceipt.Line.Length;
                        lines_online = addedRefundReceipt.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
                }

                addedRefundReceipt.Id = refundReceiptid;
                addedRefundReceipt.SyncToken = syncToken;
                if (refundReceipt.DocNumber != null)
                {
                    addedRefundReceipt.DocNumber = refundReceipt.DocNumber;
                }
                #region CustomFiled
                Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
                foreach (var customData in refundReceipt.CustomField)
                {
                    custFiled.DefinitionId = customcount.ToString();
                    custFiled.Name = customData.Name;
                    custFiled.Type = Intuit.Ipp.Data.CustomFieldTypeEnum.StringType;
                    custFiled.AnyIntuitObject = customData.Value;
                    Array.Resize(ref custom_online, customcount);
                    custom_online[customcount - 1] = custFiled;
                    customcount++;
                    custFiled = new Intuit.Ipp.Data.CustomField();
                }
                addedRefundReceipt.CustomField = custom_online;
                #endregion

                if (refundReceipt.TxnDate != null)
                {
                    try
                    {
                        addedRefundReceipt.TxnDate = Convert.ToDateTime(refundReceipt.TxnDate);
                        addedRefundReceipt.TxnDateSpecified = true;
                    }
                    catch { }
                }

                addedRefundReceipt.PrivateNote = refundReceipt.PrivateNote;
                Intuit.Ipp.Data.CustomField CustomField = new Intuit.Ipp.Data.CustomField();
                foreach (var custom in refundReceipt.CustomField)
                {
                    CustomField.AnyIntuitObject = custom.Name;
                    addedRefundReceipt.CustomField = new Intuit.Ipp.Data.CustomField[] { CustomField };
                }
                addedRefundReceipt.TxnStatus = refundReceipt.TxnStatus;

                foreach (var l in refundReceipt.Line)
                {
                    if (flag == false)
                    {
                        #region

                        flag = true;
                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var s in l.SalesItemLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in s.ItemRef)
                                {
                                    if (i.Name != null)
                                    {
                                        if (i.Name != "SHIPPING_ITEM_ID")
                                        {
                                            string id = string.Empty;
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                        else if (i.Name == "SHIPPING_ITEM_ID")
                                        {
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                Value = "SHIPPING_ITEM_ID"
                                            };
                                        }
                                    }
                                }

                                foreach (var i in s.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (refundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 7);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (refundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (s.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(s.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (s.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(s.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }

                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                           
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }
                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.DiscountLineDetail.Count > 0)
                        {

                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }

                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    string id = string.Empty;
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                    
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                lines_online[linecount - 1] = Line;
                            }
                        }
                        addedRefundReceipt.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        lineSalesItemLineDetail = new Intuit.Ipp.Data.SalesItemLineDetail();
                        DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();
                        linecount++;


                        if (l.SalesItemLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var s in l.SalesItemLineDetail)
                            {
                                if (s != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in s.ItemRef)
                                {
                                    if (CommonUtilities.GetInstance().SKULookup == false)
                                    {
                                        if (i.Name != string.Empty)
                                        {
                                            if (i.Name != "SHIPPING_ITEM_ID")
                                            {
                                                string id = string.Empty;
                                                var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                                foreach (var item in dataItem)
                                                {
                                                    id = item.Id;
                                                }
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    name = i.Name,
                                                    Value = id
                                                };
                                            }
                                            else if (i.Name == "SHIPPING_ITEM_ID")
                                            {
                                                lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                                {
                                                    Value = "SHIPPING_ITEM_ID"
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            string id = string.Empty;
                                            string name = string.Empty;
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            lineSalesItemLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }

                                    }
                                }
                                foreach (var i in s.ClassRef)
                                {

                                    if (i.Name != string.Empty)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        lineSalesItemLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (refundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in s.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.SalesTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 2);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            Line.Amount = decimal.Round(Convert.ToDecimal(l.LineAmount), 7);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (s.LineUnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                        lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (refundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (s.LineQty != null)
                                                {
                                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(s.LineQty), 2);
                                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 2);
                                                lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(s.LineUnitPrice), 7);
                                            lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (s.LineQty != null)
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(s.LineQty);
                                    lineSalesItemLineDetail.QtySpecified = true;
                                }
                                if (s.ServiceDate != null)
                                {
                                    try
                                    {
                                        lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(s.ServiceDate);
                                        lineSalesItemLineDetail.ServiceDateSpecified = true;
                                    }
                                    catch { }
                                }
                                foreach (var taxCoderef in s.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                Line.AnyIntuitObject = lineSalesItemLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }

                        }

                        if (l.DiscountLineDetail.Count > 0)
                        {
                            foreach (var discount in l.DiscountLineDetail)
                            {
                                if (discount.LineDiscountAmount != null)
                                {
                                    Line.Amount = Convert.ToDecimal(discount.LineDiscountAmount);
                                    Line.AmountSpecified = true;
                                }
                                if (discount != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.DiscountLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                if (discount.PercentBased != null)
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(discount.PercentBased);
                                    DiscountLineDetail.PercentBasedSpecified = true;
                                }
                                if (discount.DiscountPercent != null)
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(discount.DiscountPercent);
                                    DiscountLineDetail.DiscountPercentSpecified = true;
                                }


                                foreach (var account in discount.DiscountAccountRef)
                                {
                                    string id = string.Empty;
                                    if (account.Name != string.Empty && account.Name != null && account.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(account.Name);
                                        DiscountLineDetail.DiscountAccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                Line.AnyIntuitObject = DiscountLineDetail;

                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        addedRefundReceipt.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                foreach (var txntaxCode in refundReceipt.TxnTaxDetail)
                {
                    #region TxnTaxDetail
                    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    if (txntaxCode.TxnTaxCodeRef != null)
                    {
                        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                        {
                            string Taxvalue = string.Empty;

                            if (taxRef.Name != null)
                            {
                                var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxRef.Name.Trim());
                                if (dataTax != null)
                                {
                                    foreach (var tax in dataTax)
                                    {
                                        Taxvalue = tax.Id;
                                    }

                                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                    {
                                        name = taxRef.Name,
                                        Value = Taxvalue
                                    };
                                }
                            }
                        }
                    }

                    if (txnTaxDetail != null)
                    {
                        addedRefundReceipt.TxnTaxDetail = txnTaxDetail;
                    }

                    #endregion
                }


                foreach (var customerRef in refundReceipt.CustomerRef)
                {
                    string customervalue = string.Empty;
                    if (customerRef.Name != null)
                    {
                        var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerRef.Name.Trim());
                        foreach (var customer in dataCustomer)
                        {
                            customervalue = customer.Id;
                        }
                        addedRefundReceipt.CustomerRef = new ReferenceType()
                        {
                            name = customerRef.Name,
                            Value = customervalue
                        };
                    }
                }

                if (refundReceipt.CustomerMemo != null)
                {
                    addedRefundReceipt.CustomerMemo = new MemoRef()
                    {
                        Value = refundReceipt.CustomerMemo
                    };
                }
                PhysicalAddress billAddr = new PhysicalAddress();
                if (refundReceipt.BillAddr.Count > 0)
                {
                    foreach (var billaddress in refundReceipt.BillAddr)
                    {
                        billAddr.Line1 = billaddress.BillLine1;
                        billAddr.Line2 = billaddress.BillLine2;
                        billAddr.Line3 = billaddress.BillLine3;
                        billAddr.City = billaddress.BillCity;
                        billAddr.Country = billaddress.BillCountry;
                        billAddr.CountrySubDivisionCode = billaddress.BillCountrySubDivisionCode;
                        billAddr.PostalCode = billaddress.BillPostalCode;
                        billAddr.Note = billaddress.BillNote;
                        billAddr.Line4 = billaddress.BillLine4;
                        billAddr.Line5 = billaddress.BillLine5;
                        billAddr.Lat = billaddress.BillAddrLat;
                        billAddr.Long = billaddress.BillAddrLong;
                    }

                    addedRefundReceipt.BillAddr = billAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (refundReceipt.ShipAddr.Count > 0)
                {
                    foreach (var shipAddress in refundReceipt.ShipAddr)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipLine3;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedRefundReceipt.ShipAddr = shipAddr;
                }


                foreach (var className in refundReceipt.ClassRef)
                {
                    string classValue = string.Empty;
                    if (className.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetItemExistsInOnlineQBO(className.Name.Trim());
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedRefundReceipt.ClassRef = new ReferenceType()
                        {
                            name = className.Name,
                            Value = classValue
                        };
                    }
                }


                if (refundReceipt.ShipMethodRef.Count > 0)
                {
                    string shipName = string.Empty;

                    foreach (var shipData in refundReceipt.ShipMethodRef)
                    {
                        shipName = shipData.Name;
                    }
                    addedRefundReceipt.ShipMethodRef = new ReferenceType()
                    {
                        name = shipName,
                        Value = shipName
                    };
                }
                if (refundReceipt.ShipDate != null)
                {
                    try
                    {
                        addedRefundReceipt.ShipDate = Convert.ToDateTime(refundReceipt.ShipDate);
                        addedRefundReceipt.ShipDateSpecified = true;
                    }
                    catch { }
                }
                addedRefundReceipt.TrackingNum = refundReceipt.TrackingNum;

                if (refundReceipt.PrintStatus != null)
                {
                    if (refundReceipt.PrintStatus == PrintStatusEnum.NotSet.ToString())
                    {
                        addedRefundReceipt.PrintStatus = PrintStatusEnum.NotSet;
                        addedRefundReceipt.PrintStatusSpecified = true;
                    }
                    else if (refundReceipt.PrintStatus == PrintStatusEnum.NeedToPrint.ToString())
                    {
                        addedRefundReceipt.PrintStatus = PrintStatusEnum.NeedToPrint;
                        addedRefundReceipt.PrintStatusSpecified = true;
                    }
                    else if (refundReceipt.PrintStatus == PrintStatusEnum.PrintComplete.ToString())
                    {
                        addedRefundReceipt.PrintStatus = PrintStatusEnum.PrintComplete;
                        addedRefundReceipt.PrintStatusSpecified = true;
                    }
                }
                if (refundReceipt.EmailStatus != null)
                {
                    if (refundReceipt.EmailStatus == EmailStatusEnum.NotSet.ToString())
                    {
                        addedRefundReceipt.EmailStatus = EmailStatusEnum.NotSet;
                        addedRefundReceipt.EmailStatusSpecified = true;
                    }
                    else if (refundReceipt.EmailStatus == EmailStatusEnum.EmailSent.ToString())
                    {
                        addedRefundReceipt.EmailStatus = EmailStatusEnum.EmailSent;
                        addedRefundReceipt.EmailStatusSpecified = true;
                    }
                    else if (refundReceipt.EmailStatus == EmailStatusEnum.NeedToSend.ToString())
                    {
                        addedRefundReceipt.EmailStatus = EmailStatusEnum.NeedToSend;
                        addedRefundReceipt.EmailStatusSpecified = true;
                    }
                }
                if (refundReceipt.BillEmail != null)
                {
                    EmailAddress EmailAddress = new Intuit.Ipp.Data.EmailAddress();
                    EmailAddress.Address = refundReceipt.BillEmail;
                    addedRefundReceipt.BillEmail = EmailAddress;
                }
                if (refundReceipt.PaymentRefNumber != null)
                {
                    addedRefundReceipt.PaymentRefNum = refundReceipt.PaymentRefNumber;
                }


                foreach (var paymentMethod in refundReceipt.PaymentMethodRef)
                {
                    string classValue = string.Empty;
                    if (paymentMethod.Name != null)
                    {
                        var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                        foreach (var data in dataPaymentMethod)
                        {
                            classValue = data.Id;
                        }
                        addedRefundReceipt.PaymentMethodRef = new ReferenceType()
                        {
                            name = paymentMethod.Name,
                            Value = classValue
                        };
                    }
                }


                foreach (var accountData in refundReceipt.DepositToAccountRef)
                {
                    QueryService<Account> classQueryService = new QueryService<Account>(serviceContext);

                    string classValue = string.Empty;
                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                    {
                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                        addedRefundReceipt.DepositToAccountRef = new ReferenceType()
                        {
                            name = accDetails.name,
                            Value = accDetails.Value
                        };
                    }
                    
                }
                if (refundReceipt.ApplyTaxAfterDiscount != null)
                {
                    addedRefundReceipt.ApplyTaxAfterDiscount = Convert.ToBoolean(refundReceipt.ApplyTaxAfterDiscount);
                    addedRefundReceipt.ApplyTaxAfterDiscountSpecified = true;
                }

                if (refundReceipt.CurrencyRef.Count > 0)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var shipData in refundReceipt.CurrencyRef)
                    {
                        CurrencyRefName = shipData.Name;
                    }
                    addedRefundReceipt.CurrencyRef = new ReferenceType()
                    {
                        name = CurrencyRefName,
                        Value = CurrencyRefName
                    };
                }

                if (refundReceipt.ExchangeRate != null)
                {
                    addedRefundReceipt.ExchangeRate = Convert.ToDecimal(refundReceipt.ExchangeRate);
                    addedRefundReceipt.ExchangeRateSpecified = true;
                }

                #region GlobalTaxCalculation

                if (refundReceipt.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (refundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedRefundReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedRefundReceipt.GlobalTaxCalculationSpecified = true;
                        }
                        else if (refundReceipt.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedRefundReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedRefundReceipt.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedRefundReceipt.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedRefundReceipt.GlobalTaxCalculationSpecified = true;
                        }
                    }

                }

                #endregion

                #endregion

                RefundReceiptAdded = new Intuit.Ipp.Data.RefundReceipt();
                RefundReceiptAdded = dataService.Update<Intuit.Ipp.Data.RefundReceipt>(addedRefundReceipt);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (RefundReceiptAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = RefundReceiptAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = RefundReceiptAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        #endregion
    }
    /// <summary>
    /// checking doc number is present or not in quickbook online for Salesreceipt.
    /// </summary>
    public class ImportOnlineRefundReceiptQBEntryCollection : Collection<RefundReceipt>
    {
    
        internal RefundReceipt FindRefundReceiptNumberEntry(string docNumber)
        {
            foreach (RefundReceipt item in this)
            {
                if (item.DocNumber == docNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }
    //public class OnlineRefundReceiptQBEntryCollection : Collection<RefundReceipt>
    //{
    //    public RefundReceipt FindRefundReceiptNumberEntry(string docNumber)
    //    {
    //        foreach (RefundReceipt item in this)
    //        {
    //            if (item.DocNumber == docNumber)
    //            {
    //                return item;
    //            }
    //        }
    //        return null;
    //    }

    //}

    #region

    ///// <summary>
    ///// Enum declaration for GlobalTaxCalculation
    ///// </summary>
    //public enum GlobalTaxCalculation
    //{
    //    TaxExcluded,
    //    TaxIncluded,
    //    NotApplicable
    //}

    ///// <summary>
    ///// enum declaration for line detail type
    ///// </summary>
    //public enum LineDetailType
    //{
    //    PaymentLineDetail,
    //    //
    //    DiscountLineDetail,
    //    //
    //    TaxLineDetail,
    //    //
    //    SalesItemLineDetail,
    //    //
    //    ItemBasedExpenseLineDetail,
    //    //
    //    AccountBasedExpenseLineDetail,
    //    //
    //    DepositLineDetail,
    //    //
    //    PurchaseOrderItemLineDetail,
    //    //
    //    ItemReceiptLineDetail,
    //    //
    //    JournalEntryLineDetail,
    //    //
    //    GroupLineDetail,
    //    //
    //    DescriptionOnly,
    //    //
    //    SubTotalLineDetail,
    //    //
    //    SalesOrderItemLineDetail
    //}
#endregion

}
