﻿using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Collections.ObjectModel;
using System;
using System.Text;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{

    public class OnlineCustomerQBEntry
    {
        #region Private Member Variable

        private string m_GivenName;
        private string m_MiddleName;
        private string m_FamilyName;
        private string m_Title;
        private string m_Suffix;
        private string m_FullyQualifiedName;
        private string m_CompanyName;
        private string m_DisplayName;
        private string m_IsActive;
        private Collection<OnlineEntities.TelephoneNumber> m_PrimaryPhone = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.TelephoneNumber> m_AlternatePhone = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.TelephoneNumber> m_Mobile = new Collection<OnlineEntities.TelephoneNumber>();
        private Collection<OnlineEntities.TelephoneNumber> m_Fax = new Collection<OnlineEntities.TelephoneNumber>();
      
        private Collection<OnlineEntities.EmailAddressEmployee> m_PrimaryEmailAddr = new Collection<OnlineEntities.EmailAddressEmployee>();
        private Collection<OnlineEntities.EmailAddressEmployee> m_WebAddr = new Collection<OnlineEntities.EmailAddressEmployee>();
        private Collection<OnlineEntities.TaxCodeRef> m_TaxCodeRef = new Collection<OnlineEntities.TaxCodeRef>();
        private string m_Taxable;
        private Collection<OnlineEntities.BillAddr> m_PrimaryAddr = new Collection<OnlineEntities.BillAddr>();
        private Collection<OnlineEntities.ShipAddr> m_PrimaryAddrship = new Collection<OnlineEntities.ShipAddr>();
        private string m_Notes;
        private string m_Job;
        private Collection<OnlineEntities.ParentRef> m_ParentRef = new Collection<OnlineEntities.ParentRef>();
        private string m_Level;
        private Collection<OnlineEntities.SalesTermRef> m_SalesTermRef = new Collection<OnlineEntities.SalesTermRef>();
        private Collection<PaymentMethodRef> m_PaymentMethodRef = new Collection<PaymentMethodRef>();
        private string m_OpenBalance;
        private string m_OpenBalanceDate;
        private Collection<OnlineEntities.CurrencyRef> m_CurrencyRef = new Collection<OnlineEntities.CurrencyRef>();
        private string m_PreferredDeliveryMethod;

        private string m_ResaleNum;
        //594
        private bool m_isAppend;
        #endregion

        #region Constructor
        public OnlineCustomerQBEntry()
        {
        }
        #endregion

        #region Public Properties

        public string GivenName
        {
            get { return m_GivenName; }
            set { m_GivenName = value; }
        }
        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }

        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }
        public string FamilyName
        {
            get { return m_FamilyName; }
            set { m_FamilyName = value; }
        }
        public string Suffix
        {
            get { return m_Suffix; }
            set { m_Suffix = value; }
        }
        public string FullyQualifiedName
        {
            get { return m_FullyQualifiedName; }
            set { m_FullyQualifiedName = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }
        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }
        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> PrimaryPhone
        {
            get { return m_PrimaryPhone; }
            set { m_PrimaryPhone = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> AlternatePhone
        {
            get { return m_AlternatePhone; }
            set { m_AlternatePhone = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> Mobile
        {
            get { return m_Mobile; }
            set { m_Mobile = value; }
        }
        public Collection<OnlineEntities.TelephoneNumber> Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
       
        public Collection<OnlineEntities.EmailAddressEmployee> PrimaryEmailAddr
        {
            get { return m_PrimaryEmailAddr; }
            set { m_PrimaryEmailAddr = value; }
        }
        public Collection<OnlineEntities.EmailAddressEmployee> WebAddr
        {
            get { return m_WebAddr; }
            set { m_WebAddr = value; }
        }
        public Collection<OnlineEntities.TaxCodeRef> TaxCodeRef
        {
            get { return m_TaxCodeRef; }
            set { m_TaxCodeRef = value; }
        }
        public string Taxable
        {
            get { return m_Taxable; }
            set { m_Taxable = value; }
        }

        public Collection<OnlineEntities.BillAddr> PrimaryAddr
        {
            get { return m_PrimaryAddr; }
            set { m_PrimaryAddr = value; }
        }
        public Collection<OnlineEntities.ShipAddr> PrimaryAddrship
        {
            get { return m_PrimaryAddrship; }
            set { m_PrimaryAddrship = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }
        public string Job
        {
            get { return m_Job; }
            set { m_Job = value; }
        }

        public Collection<OnlineEntities.ParentRef> ParentRef
        {
            get { return m_ParentRef; }
            set { m_ParentRef = value; }
        }
        public string Level
        {
            get { return m_Level; }
            set { m_Level = value; }
        }
        public Collection<OnlineEntities.SalesTermRef> SalesTermRef
        {
            get { return m_SalesTermRef; }
            set { m_SalesTermRef = value; }
        }

        public Collection<OnlineEntities.PaymentMethodRef> PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }
        public string OpenBalance
        {
            get { return m_OpenBalance; }
            set { m_OpenBalance = value; }
        }
        public string ResaleNum
        {
            get { return m_ResaleNum; }
            set { m_ResaleNum = value; }
        }
        
        public string OpenBalanceDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_OpenBalanceDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_OpenBalanceDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_OpenBalanceDate = value;
            }
        }

        public Collection<OnlineEntities.CurrencyRef> CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public string PreferredDeliveryMethod
        {
            get { return m_PreferredDeliveryMethod; }
            set { m_PreferredDeliveryMethod = value; }
        }
        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }
        #endregion



        #region  Public Methods
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }

        /// <summary>
        /// Creating new customer entry in quickbook online.
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoOptimization)]



        public async System.Threading.Tasks.Task<bool> CreateQBOCustomer(OnlineDataProcessingsImportClass.OnlineCustomerQBEntry coll)
        {


            #region Create Parent for customer  Issue 466
            string currencyRef = "";
            foreach (var item in coll.CurrencyRef)
            {
                currencyRef = item.Name;
                break;
            }
            if (coll.FullyQualifiedName != string.Empty && coll.FullyQualifiedName.Contains(":") )
            {
                ReadOnlyCollection<Intuit.Ipp.Data.Customer> OnlineCustomer = null;
                OnlineCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(coll.FullyQualifiedName);
                    if (OnlineCustomer.Count == 0)
                    {
                       await CommonUtilities.GetCustomerExists_createInOnlineQBO(coll.FullyQualifiedName, currencyRef,false);
                    }
              
            }
            
            #endregion Issue 466



            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.Customer customerAdded = null;
            Intuit.Ipp.Data.Customer addedCustomer = new Intuit.Ipp.Data.Customer();
            OnlineCustomerQBEntry customer = new OnlineCustomerQBEntry();
            customer = coll;

            try
            {
                #region Add customer
                var dataItem1 = await CommonUtilities.GetCustomerExistsInOnlineQBO(customer.DisplayName.Trim());
                
                if (dataItem1.Count > 0)
                {
                    foreach (var data in dataItem1)
                    {
                        addedCustomer.Id = data.Id;
                        addedCustomer.SyncToken = data.SyncToken;
                    }
                }

                if (customer.GivenName != null)
                {
                    addedCustomer.GivenName = customer.GivenName;
                }
                if (customer.MiddleName != null)
                {
                    addedCustomer.MiddleName = customer.MiddleName;
                }
                if (customer.Title != null)
                {
                    addedCustomer.Title = customer.Title;
                }
                if (customer.FamilyName != null)
                {
                    addedCustomer.FamilyName = customer.FamilyName;
                }
                if (customer.Suffix != null)
                {
                    addedCustomer.Suffix = customer.Suffix;
                }
                if (customer.FullyQualifiedName != null)
                {
                    addedCustomer.FullyQualifiedName = customer.FullyQualifiedName;
                }
                if (customer.CompanyName != null)
                {
                    addedCustomer.CompanyName = customer.CompanyName;
                }
                if (customer.DisplayName != null)
                {
                    addedCustomer.DisplayName = customer.DisplayName;
                }
                if (customer.IsActive != null)
                {
                    addedCustomer.Active = Convert.ToBoolean(customer.IsActive);
                    addedCustomer.ActiveSpecified = true;
                }
                Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (customer.PrimaryPhone.Count > 0)
                {
                    foreach (var primaryPhone in customer.PrimaryPhone)
                    {
                        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                    }
                    addedCustomer.PrimaryPhone = primaryPhoneNo;
                }

                Intuit.Ipp.Data.TelephoneNumber altNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (customer.AlternatePhone.Count > 0)
                {
                    foreach (var altPhn in customer.AlternatePhone)
                    {
                        altNo.FreeFormNumber = altPhn.AlternatePhone;
                    }
                    addedCustomer.AlternatePhone = altNo;
                }
                Intuit.Ipp.Data.TelephoneNumber mob = new Intuit.Ipp.Data.TelephoneNumber();
                if (customer.Mobile.Count > 0)
                {
                    foreach (var mobNo in customer.Mobile)
                    {
                        mob.FreeFormNumber = mobNo.Mobile;
                    }
                    addedCustomer.Mobile = mob;
                }
                Intuit.Ipp.Data.TelephoneNumber fax = new Intuit.Ipp.Data.TelephoneNumber();
                if (customer.Fax.Count > 0)
                {
                    foreach (var f in customer.Fax)
                    {
                        fax.FreeFormNumber = f.Fax;
                    }
                    addedCustomer.Fax = fax;
                }

                Intuit.Ipp.Data.EmailAddress emailAddress = new Intuit.Ipp.Data.EmailAddress();
                if (customer.PrimaryEmailAddr.Count > 0)
                {
                    foreach (var emailAddr in customer.PrimaryEmailAddr)
                    {
                        emailAddress.Address = emailAddr.EmailAddress;
                    }
                    addedCustomer.PrimaryEmailAddr = emailAddress;
                }

                Intuit.Ipp.Data.WebSiteAddress webaddr = new Intuit.Ipp.Data.WebSiteAddress();
                if (customer.WebAddr.Count > 0)
                {
                    foreach (var emailAddr in customer.WebAddr)
                    {
                        webaddr.URI = emailAddr.WebAddress;
                    }
                    addedCustomer.WebAddr = webaddr;
                }

                if (customer.TaxCodeRef != null)
                {
                    foreach (var tref in customer.TaxCodeRef)
                    {
                        string Customervalue = string.Empty;
                        if (tref.Name != null)
                        {
                            var dataCustomer = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(tref.Name.Trim());
                            foreach (var cust in dataCustomer)
                            {
                                Customervalue = cust.Id;
                            }

                            addedCustomer.DefaultTaxCodeRef = new ReferenceType()
                            {
                                name = tref.Name,
                                Value = Customervalue
                            };
                        }

                    }
                }
                if (customer.Taxable != null)
                {
                    addedCustomer.Taxable = Convert.ToBoolean(customer.Taxable);
                    addedCustomer.TaxableSpecified = true;
                }

                PhysicalAddress primaryAddr = new PhysicalAddress();
                if (customer.PrimaryAddr.Count > 0)
                {
                    foreach (var address in customer.PrimaryAddr)
                    {
                        primaryAddr.Line1 = address.BillLine1;
                        primaryAddr.Line2 = address.BillLine2;
                        primaryAddr.Line3 = address.BillLine3;
                        primaryAddr.City = address.BillCity;
                        primaryAddr.Country = address.BillCountry;
                        primaryAddr.CountrySubDivisionCode = address.BillCountrySubDivisionCode;
                        primaryAddr.PostalCode = address.BillPostalCode;
                        primaryAddr.Note = address.BillNote;
                        primaryAddr.Line4 = address.BillLine4;
                        primaryAddr.Line5 = address.BillLine5;
                        primaryAddr.Lat = address.BillAddrLat;
                        primaryAddr.Long = address.BillAddrLong;
                    }

                    addedCustomer.BillAddr = primaryAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (customer.PrimaryAddrship.Count > 0)
                {
                    foreach (var shipAddress in customer.PrimaryAddrship)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipCountrySubDivisionCode;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedCustomer.ShipAddr = shipAddr;
                }
                if (customer.Notes != null)
                {
                    addedCustomer.Notes = customer.Notes;
                }               

                if (customer.ParentRef.Count != 0)
                {
                    string[] arr = new string[15];
                    arr = customer.FullyQualifiedName.Split(':');
                    string custvalue = string.Empty;
                    string Customervalue = null;

                    for (int i = 0; i < arr.Length - 1; i++)
                    {
                        Customervalue = Customervalue + ":" + arr[i];
                        StringBuilder sb = new StringBuilder(Customervalue);

                        Customervalue = sb.ToString();
                        if (i == 0)
                        {
                            sb.Remove(0, 1);
                            Customervalue = sb.ToString();
                        }
                    }

                    var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(Customervalue.Trim());

                    foreach (var item in dataCustomer)
                    {
                        custvalue = item.Id;
                    }
                    addedCustomer.ParentRef = new ReferenceType()
                    {
                        name = Customervalue,
                        Value = custvalue
                    };
                    addedCustomer.Job = true;
                    addedCustomer.JobSpecified = true;
                }
                else
                {
                    addedCustomer.Job = false;
                    addedCustomer.JobSpecified = false;
                }

                if (customer.Level != null)
                {
                    addedCustomer.Level =Convert.ToInt32(customer.Level);
                    addedCustomer.LevelSpecified = true;
                }        

                foreach (var salesTermRef in customer.SalesTermRef)
                {
                    string classValue = string.Empty;
                    if (salesTermRef.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(salesTermRef.Name.Trim());

                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedCustomer.SalesTermRef = new ReferenceType()
                        {
                            name = salesTermRef.Name,
                            Value = classValue
                        };
                    }
                }

                #region find PaymentMethodRef
                if (customer.PaymentMethodRef != null)
                {
                    foreach (var paymentMethod in customer.PaymentMethodRef)
                    {
                        string classValue = string.Empty;
                        if (paymentMethod.Name != null)
                        {
                            var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());

                            foreach (var data in dataPaymentMethod)
                            {
                                classValue = data.Id;
                            }
                            addedCustomer.PaymentMethodRef = new ReferenceType()
                            {
                                name = paymentMethod.Name,
                                Value = classValue
                            };
                        }
                    }
                }

                #endregion

                if (customer.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in customer.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedCustomer.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }
                if (customer.PreferredDeliveryMethod != null)
                {
                    addedCustomer.PreferredDeliveryMethod = customer.PreferredDeliveryMethod;

                }
                if (customer.OpenBalance != null)
                {
                    addedCustomer.Balance =Convert.ToDecimal(customer.OpenBalance);
                    addedCustomer.BalanceSpecified = true;
                }
                if (customer.OpenBalanceDate != null)
                {
                    try
                    {
                        addedCustomer.OpenBalanceDate = Convert.ToDateTime(customer.OpenBalanceDate);
                        addedCustomer.OpenBalanceDateSpecified = true;
                    }
                    catch { }
                }
                if (customer.ResaleNum != null)
                {
                    addedCustomer.ResaleNum = customer.ResaleNum;                 
                }
            
                #endregion
                customerAdded = new Intuit.Ipp.Data.Customer();
                customerAdded = dataService.Update<Intuit.Ipp.Data.Customer>(addedCustomer);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (customerAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = customerAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = customerAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion
        }

        /// <summary>
        /// updating customer entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="custid"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOCustomer(OnlineDataProcessingsImportClass.OnlineCustomerQBEntry coll, string custid, string syncToken, Intuit.Ipp.Data.Customer PreviousData = null)
        {
            #region Create Parent for customer Issue 466
            string currencyRef = "";
            foreach (var item in coll.CurrencyRef)
            {
                currencyRef = item.Name;
                break;
            }
            if (coll.FullyQualifiedName != string.Empty && coll.FullyQualifiedName.Contains(":"))
            {
                ReadOnlyCollection<Intuit.Ipp.Data.Customer> OnlineCustomer = null;
                OnlineCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(coll.FullyQualifiedName);
                if (OnlineCustomer.Count == 0)
                {
                    CommonUtilities.GetCustomerExists_createInOnlineQBO(coll.FullyQualifiedName, currencyRef,false);
                }

            }

            #endregion Issue 466
			
            Intuit.Ipp.Data.Customer customerAdded = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.Customer addedCustomer = new Intuit.Ipp.Data.Customer();
            OnlineCustomerQBEntry customer = new OnlineCustomerQBEntry();
            customer = coll;
            QueryService<Customer> CustQueryService = new QueryService<Customer>(serviceContext);

            try
            {
                #region Add customer
 				//594 PreviousData is used to maintain previous records and append new data if changes are applied ,Lines will be appended to next line directly
                if (customer.isAppend == true)
                {
                    addedCustomer = PreviousData;
                    addedCustomer.sparse = true;
                    addedCustomer.sparseSpecified = true;
                }

                addedCustomer.Id = custid;
                addedCustomer.SyncToken = syncToken;
                
                if (customer.GivenName != null)
                {
                    addedCustomer.GivenName = customer.GivenName;
                }
                if (customer.MiddleName != null)
                {
                    addedCustomer.MiddleName = customer.MiddleName;
                }
                if (customer.Title != null)
                {
                    addedCustomer.Title = customer.Title;
                }
                if (customer.FamilyName != null)
                {
                    addedCustomer.FamilyName = customer.FamilyName;
                }
                if (customer.Suffix != null)
                {
                    addedCustomer.Suffix = customer.Suffix;
                }
                if (customer.FullyQualifiedName != null)
                {
                    addedCustomer.FullyQualifiedName = customer.FullyQualifiedName;
                }
                if (customer.CompanyName != null)
                {
                    addedCustomer.CompanyName = customer.CompanyName;
                }
                if (customer.DisplayName != null)
                {
                    addedCustomer.DisplayName = customer.DisplayName;
                }
                if (customer.IsActive != null)
                {
                    addedCustomer.Active = Convert.ToBoolean(customer.IsActive);
                    addedCustomer.ActiveSpecified = true;
                }
                Intuit.Ipp.Data.TelephoneNumber primaryPhoneNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (customer.PrimaryPhone.Count > 0)
                {
                    foreach (var primaryPhone in customer.PrimaryPhone)
                    {
                        primaryPhoneNo.FreeFormNumber = primaryPhone.PrimaryPhone;
                    }
                    addedCustomer.PrimaryPhone = primaryPhoneNo;
                }

                Intuit.Ipp.Data.TelephoneNumber altNo = new Intuit.Ipp.Data.TelephoneNumber();
                if (customer.AlternatePhone.Count > 0)
                {
                    foreach (var altPhn in customer.AlternatePhone)
                    {
                        altNo.FreeFormNumber = altPhn.AlternatePhone;
                    }
                    addedCustomer.AlternatePhone = altNo;
                }
                Intuit.Ipp.Data.TelephoneNumber mob = new Intuit.Ipp.Data.TelephoneNumber();
                if (customer.Mobile.Count > 0)
                {
                    foreach (var mobNo in customer.Mobile)
                    {
                        mob.FreeFormNumber = mobNo.Mobile;
                    }
                    addedCustomer.Mobile = mob;
                }

                Intuit.Ipp.Data.TelephoneNumber fax = new Intuit.Ipp.Data.TelephoneNumber();
                 if (customer.Fax.Count > 0)
                {
                    foreach (var f in customer.Fax)
                    {
                        fax.FreeFormNumber = f.Fax;
                    }
                    addedCustomer.Fax = fax;
                }

                Intuit.Ipp.Data.EmailAddress emailAddress = new Intuit.Ipp.Data.EmailAddress();
                if (customer.PrimaryEmailAddr.Count > 0)
                {
                    foreach (var emailAddr in customer.PrimaryEmailAddr)
                    {
                        emailAddress.Address = emailAddr.EmailAddress;
                    }
                    addedCustomer.PrimaryEmailAddr = emailAddress;
                }

                Intuit.Ipp.Data.WebSiteAddress webaddr = new Intuit.Ipp.Data.WebSiteAddress();
                if (customer.WebAddr.Count > 0)
                {
                    foreach (var emailAddr in customer.WebAddr)
                    {
                        webaddr.URI = emailAddr.WebAddress;
                    }
                    addedCustomer.WebAddr = webaddr;
                }

                if (customer.TaxCodeRef != null)
                {
                    foreach (var tref in customer.TaxCodeRef)
                    {
                        string Customervalue = string.Empty;
                        if (tref.Name != null)
                        {
                            var dataCustomer = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(tref.Name.Trim());

                            foreach (var cust in dataCustomer)
                            {
                                Customervalue = cust.Id;
                            }

                            addedCustomer.DefaultTaxCodeRef = new ReferenceType()
                            {
                                name = tref.Name,
                                Value = Customervalue
                            };
                        }
                    }
                }
                if (customer.Taxable != null)
                {
                    addedCustomer.Taxable = Convert.ToBoolean(customer.Taxable);
                    addedCustomer.TaxableSpecified = true;
                }

                PhysicalAddress primaryAddr = new PhysicalAddress();
                if (customer.PrimaryAddr.Count > 0)
                {
                    foreach (var address in customer.PrimaryAddr)
                    {
                        primaryAddr.Line1 = address.BillLine1;
                        primaryAddr.Line2 = address.BillLine2;
                        primaryAddr.Line3 = address.BillLine3;
                        primaryAddr.City = address.BillCity;
                        primaryAddr.Country = address.BillCountry;
                        primaryAddr.CountrySubDivisionCode = address.BillCountrySubDivisionCode;
                        primaryAddr.PostalCode = address.BillPostalCode;
                        primaryAddr.Note = address.BillNote;
                        primaryAddr.Line4 = address.BillLine4;
                        primaryAddr.Line5 = address.BillLine5;
                        primaryAddr.Lat = address.BillAddrLat;
                        primaryAddr.Long = address.BillAddrLong;
                    }

                    addedCustomer.BillAddr = primaryAddr;
                }

                PhysicalAddress shipAddr = new PhysicalAddress();
                if (customer.PrimaryAddrship.Count > 0)
                {
                    foreach (var shipAddress in customer.PrimaryAddrship)
                    {
                        shipAddr.Line1 = shipAddress.ShipLine1;
                        shipAddr.Line2 = shipAddress.ShipLine2;
                        shipAddr.Line3 = shipAddress.ShipLine3;
                        shipAddr.City = shipAddress.ShipCity;
                        shipAddr.Country = shipAddress.ShipCountry;
                        shipAddr.CountrySubDivisionCode = shipAddress.ShipCountrySubDivisionCode;
                        shipAddr.PostalCode = shipAddress.ShipPostalCode;
                        shipAddr.Note = shipAddress.ShipNote;
                        shipAddr.Line4 = shipAddress.ShipLine4;
                        shipAddr.Line5 = shipAddress.ShipLine5;
                        shipAddr.Lat = shipAddress.ShipAddrLat;
                        shipAddr.Long = shipAddress.ShipAddrLong;
                    }

                    addedCustomer.ShipAddr = shipAddr;
                }
                if (customer.Notes != null)
                {
                    addedCustomer.Notes = customer.Notes;
                }               

                if (customer.ParentRef.Count != 0)
                {
                    string[] arr = new string[15];
                    arr = customer.FullyQualifiedName.Split(':');
                    string custvalue = string.Empty;
                    string Customervalue = null;

                    for (int i = 0; i < arr.Length - 1; i++)
                    {
                        Customervalue = Customervalue + ":" + arr[i];
                        StringBuilder sb = new StringBuilder(Customervalue);

                        Customervalue = sb.ToString();
                        if (i == 0)
                        {
                            sb.Remove(0, 1);
                            Customervalue = sb.ToString();
                        }
                    }

                    var dataCustomer = await CommonUtilities.GetCustomerExistsInOnlineQBO(Customervalue.Trim());
                    foreach (var item in dataCustomer)
                    {
                        custvalue = item.Id;
                    }
                    addedCustomer.ParentRef = new ReferenceType()
                    {
                        name = Customervalue,
                        Value = custvalue
                    };
                    addedCustomer.Job = true;
                    addedCustomer.JobSpecified = true;
                }
                else
                {
                    addedCustomer.Job = false;
                    addedCustomer.JobSpecified = false;
                }


                if (customer.Level != null)
                {
                    addedCustomer.Level = Convert.ToInt32(customer.Level);
                    addedCustomer.LevelSpecified = true;
                }

                foreach (var salesTermRef in customer.SalesTermRef)
                {
                    string classValue = string.Empty;
                    if (salesTermRef.Name != null)
                    {
                        var dataClass = await CommonUtilities.GetTermDetailsExistsInOnlineQBO(salesTermRef.Name.Trim());
                       
                        foreach (var data in dataClass)
                        {
                            classValue = data.Id;
                        }
                        addedCustomer.SalesTermRef = new ReferenceType()
                        {
                            name = salesTermRef.Name,
                            Value = classValue
                        };
                    }
                }

                #region find PaymentMethodRef
                if (customer.PaymentMethodRef != null)
                {
                    foreach (var paymentMethod in customer.PaymentMethodRef)
                    {
                        string classValue = string.Empty;
                        if (paymentMethod.Name != null)
                        {
                            var dataPaymentMethod = await CommonUtilities.GetPaymentMethodExistsInQuickBooks(paymentMethod.Name.Trim());
                            foreach (var data in dataPaymentMethod)
                            {
                                classValue = data.Id;
                            }
                            addedCustomer.PaymentMethodRef = new ReferenceType()
                            {
                                name = paymentMethod.Name,
                                Value = classValue
                            };
                        }
                    }
                }

                #endregion

                if (customer.CurrencyRef != null)
                {
                    string CurrencyRefName = string.Empty;

                    foreach (var CurrencyData in customer.CurrencyRef)
                    {
                        CurrencyRefName = CurrencyData.Name;
                    }
                    if (CurrencyRefName != string.Empty)
                    {
                        addedCustomer.CurrencyRef = new ReferenceType()
                        {
                            name = CurrencyRefName,
                            Value = CurrencyRefName
                        };
                    }
                }
                if (customer.PreferredDeliveryMethod != null)
                {
                    addedCustomer.PreferredDeliveryMethod = customer.PreferredDeliveryMethod;

                }
                if (customer.OpenBalance != null)
                {
                    addedCustomer.Balance = Convert.ToDecimal(customer.OpenBalance);
                    addedCustomer.BalanceSpecified = true;
                }
                if (customer.OpenBalanceDate != null)
                {
                    try
                    {
                        addedCustomer.OpenBalanceDate = Convert.ToDateTime(customer.OpenBalanceDate);
                        addedCustomer.OpenBalanceDateSpecified = true;
                    }
                    catch { }
                }
                if (customer.ResaleNum != null)
                {
                    addedCustomer.ResaleNum = customer.ResaleNum;
                  
                }

                #endregion
                customerAdded = new Intuit.Ipp.Data.Customer();
                customerAdded = dataService.Update<Intuit.Ipp.Data.Customer>(addedCustomer);

            }

            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (customerAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = customerAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = customerAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

        #endregion
    }


    public class OnlineCustomerQBEntryCollection : Collection<OnlineCustomerQBEntry>
    {

    }
  
}


   


