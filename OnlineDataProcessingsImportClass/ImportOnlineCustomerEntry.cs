﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using OnlineEntities;


namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineCustomerEntry
    {
        private static ImportOnlineCustomerEntry m_ImportOnlineCustomerEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineCustomerEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Customer class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineCustomerEntry GetInstance()
        {
            if (m_ImportOnlineCustomerEntry == null)
                m_ImportOnlineCustomerEntry = new ImportOnlineCustomerEntry();
            return m_ImportOnlineCustomerEntry;
        }
        /// setting values to customer data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlineCustomerQBEntryCollection ImportOnlinecustomerData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineCustomerQBEntryCollection coll = new OnlineCustomerQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry
            DateTime CustomerDt = new DateTime();
            string datevalue = string.Empty;
            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }                  
                 
               
                    OnlineDataProcessingsImportClass.OnlineCustomerQBEntry customer = new OnlineDataProcessingsImportClass.OnlineCustomerQBEntry();


                    if (dt.Columns.Contains("GivenName"))
                    {
                        #region Validations of NameOf
                        if (dr["GivenName"].ToString() != string.Empty)
                        {
                            if (dr["GivenName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This GivenName (" + dr["GivenName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.GivenName = dr["GivenName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.GivenName = dr["GivenName"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.GivenName = dr["GivenName"].ToString();
                                }
                            }
                            else
                            {
                                customer.GivenName = dr["GivenName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MiddleName"))
                    {
                        #region Validations of MiddleName
                        if (dr["MiddleName"].ToString() != string.Empty)
                        {
                            if (dr["MiddleName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.MiddleName = dr["MiddleName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.MiddleName = dr["MiddleName"].ToString();
                                }
                            }
                            else
                            {
                                customer.MiddleName = dr["MiddleName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Title"))
                    {
                        #region Validations of Title
                        if (dr["Title"].ToString() != string.Empty)
                        {
                            if (dr["Title"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Title (" + dr["Title"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.Title = dr["Title"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.Title = dr["Title"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.Title = dr["Title"].ToString();
                                }
                            }
                            else
                            {
                                customer.Title = dr["Title"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FamilyName"))
                    {
                        #region Validations of FamilyName
                        if (dr["FamilyName"].ToString() != string.Empty)
                        {
                            if (dr["FamilyName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FamilyName (" + dr["FamilyName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.FamilyName = dr["FamilyName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.FamilyName = dr["FamilyName"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.FamilyName = dr["FamilyName"].ToString();
                                }
                            }
                            else
                            {
                                customer.FamilyName = dr["FamilyName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Suffix"))
                    {
                        #region Validations of Suffix
                        if (dr["Suffix"].ToString() != string.Empty)
                        {
                            if (dr["Suffix"].ToString().Length > 10)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Suffix (" + dr["Suffix"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.Suffix = dr["Suffix"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.Suffix = dr["Suffix"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.Suffix = dr["Suffix"].ToString();
                                }
                            }
                            else
                            {
                                customer.Suffix = dr["Suffix"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FullyQualifiedName"))
                    {
                        #region Validations of FullyQualifiedName
                        if (dr["FullyQualifiedName"].ToString() != string.Empty)
                        {
                            if (dr["FullyQualifiedName"].ToString().Length > 200)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FullyQualifiedName (" + dr["FullyQualifiedName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.FullyQualifiedName = dr["FullyQualifiedName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                                }
                            }
                            else
                            {
                                customer.FullyQualifiedName = dr["FullyQualifiedName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CompanyName"))
                    {
                        #region Validations of compName
                        if (dr["CompanyName"].ToString() != string.Empty)
                        {
                            if (dr["CompanyName"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CompanyName (" + dr["CompanyName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.CompanyName = dr["CompanyName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.CompanyName = dr["CompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.CompanyName = dr["CompanyName"].ToString();
                                }
                            }
                            else
                            {
                                customer.CompanyName = dr["CompanyName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("DisplayName"))
                    {
                        #region Validations of DisplayName
                        if (dr["DisplayName"].ToString() != string.Empty)
                        {
                            if (dr["DisplayName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {

                                    string strMessages = "This DisplayName (" + dr["DisplayName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.DisplayName = dr["DisplayName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.DisplayName = dr["DisplayName"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.DisplayName = dr["DisplayName"].ToString();
                                }
                            }
                            else
                            {
                                customer.DisplayName = dr["DisplayName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of Description
                        if (dr["IsActive"].ToString() != string.Empty)
                        {
                            if (dr["IsActive"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.IsActive = dr["IsActive"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.IsActive = dr["IsActive"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.IsActive = dr["IsActive"].ToString();
                                }
                            }
                            else
                            {
                                customer.IsActive = dr["IsActive"].ToString();
                            }
                        }
                        #endregion
                    }

                    OnlineEntities.TelephoneNumber telephoneNo = new OnlineEntities.TelephoneNumber();
                    if (dt.Columns.Contains("PrimaryPhone"))
                    {
                        #region Validations of PrimaryPhone
                        if (dr["PrimaryPhone"].ToString() != string.Empty)
                        {
                            if (dr["PrimaryPhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PrimaryPhone (" + dr["PrimaryPhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("AlternatePhone"))
                    {
                        #region Validations of AlternatePhone
                        if (dr["AlternatePhone"].ToString() != string.Empty)
                        {
                            if (dr["AlternatePhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AlternatePhone (" + dr["AlternatePhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Mobile"))
                    {
                        #region Validations of Mobile
                        if (dr["Mobile"].ToString() != string.Empty)
                        {
                            if (dr["Mobile"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Mobile (" + dr["Mobile"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.Mobile = dr["Mobile"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.Mobile = dr["Mobile"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.Mobile = dr["Mobile"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.Mobile = dr["Mobile"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Fax"))
                    {
                        #region Validations of Fax
                        if (dr["Fax"].ToString() != string.Empty)
                        {
                            if (dr["Fax"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.Fax = dr["Fax"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.Fax = dr["Fax"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.Fax = dr["Fax"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (telephoneNo.PrimaryPhone != null)
                        customer.PrimaryPhone.Add(telephoneNo);
                    if (telephoneNo.AlternatePhone != null)
                        customer.AlternatePhone.Add(telephoneNo);
                    if (telephoneNo.Mobile != null)
                        customer.Mobile.Add(telephoneNo);

                    if (telephoneNo.Fax != null)
                        customer.Fax.Add(telephoneNo);

                    OnlineEntities.EmailAddressEmployee emailAddress = new OnlineEntities.EmailAddressEmployee();
                    if (dt.Columns.Contains("PrimaryEmailAddr"))
                    {
                        #region Validations of PrimaryEmailAddr
                        if (dr["PrimaryEmailAddr"].ToString() != string.Empty)
                        {
                            if (dr["PrimaryEmailAddr"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PrimaryEmailAddr (" + dr["PrimaryEmailAddr"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString();
                                    }
                                }
                                else
                                {
                                    emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString();
                                }
                            }
                            else
                            {
                                emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("WebAddr"))
                    {
                        #region Validations of WebAddr
                        if (dr["WebAddr"].ToString() != string.Empty)
                        {
                            if (dr["WebAddr"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This WebAddr (" + dr["WebAddr"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        emailAddress.WebAddress = dr["WebAddr"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        emailAddress.WebAddress = dr["WebAddr"].ToString();
                                    }
                                }
                                else
                                {
                                    emailAddress.WebAddress = dr["WebAddr"].ToString();
                                }
                            }
                            else
                            {
                                emailAddress.WebAddress = dr["WebAddr"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (emailAddress.EmailAddress != null)
                        customer.PrimaryEmailAddr.Add(emailAddress);

                    if (emailAddress.WebAddress != null)
                        customer.WebAddr.Add(emailAddress);

                    OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();
                    if (dt.Columns.Contains("DefaultTaxCodeRef"))
                    {
                        #region Validations of DefaultTaxCodeRef
                        if (dr["DefaultTaxCodeRef"].ToString() != string.Empty)
                        {
                            if (dr["DefaultTaxCodeRef"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This DefaultTaxCodeRef (" + dr["DefaultTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TaxCodeRef.Name = dr["DefaultTaxCodeRef"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TaxCodeRef.Name = dr["DefaultTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef.Name = dr["DefaultTaxCodeRef"].ToString();
                                }
                            }
                            else
                            {
                                TaxCodeRef.Name = dr["DefaultTaxCodeRef"].ToString();
                            }
                        }
                        #endregion
                    }
                    customer.TaxCodeRef.Add(TaxCodeRef);

                    if (dt.Columns.Contains("Taxable"))
                    {
                        #region Validations of Taxable
                        if (dr["Taxable"].ToString() != string.Empty)
                        {
                            if (dr["Taxable"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Taxable (" + dr["Taxable"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.Taxable = dr["Taxable"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.Taxable = dr["Taxable"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.Taxable = dr["Taxable"].ToString();
                                }
                            }
                            else
                            {
                                customer.Taxable = dr["Taxable"].ToString();
                            }
                        }
                        #endregion
                    }

                      #region bill shipadd

                    OnlineEntities.BillAddr billAddr = new OnlineEntities.BillAddr();
                    if (dt.Columns.Contains("BillAddr1"))
                    {
                        #region Validations of BillAddrLine1
                        if (dr["BillAddr1"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr1"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr1 (" + dr["BillAddr1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine1 = dr["BillAddr1"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine1 = dr["BillAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine1 = dr["BillAddr1"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine1 = dr["BillAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr2"))
                    {
                        #region Validations of BillAddrLine2
                        if (dr["BillAddr2"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr2"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr2 (" + dr["BillAddr2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine2 = dr["BillAddr2"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine2 = dr["BillAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine2 = dr["BillAddr2"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine2 = dr["BillAddr2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr3"))
                    {
                        #region Validations of BillAddrLine3
                        if (dr["BillAddr3"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr3"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr3 (" + dr["BillAddr3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine3 = dr["BillAddr3"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine3 = dr["BillAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine3 = dr["BillAddr3"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine3 = dr["BillAddr3"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("BillAddr4"))
                    {
                        #region Validations of BillAddr4
                        if (dr["BillAddr4"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr4"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr4 (" + dr["BillAddr4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine4 = dr["BillAddr4"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine4 = dr["BillAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine4 = dr["BillAddr4"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine4 = dr["BillAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr5"))
                    {
                        #region Validations of BillAddr5
                        if (dr["BillAddr5"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr5"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr5 (" + dr["BillAddr5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine5 = dr["BillAddr5"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine5 = dr["BillAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine5 = dr["BillAddr5"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine5 = dr["BillAddr5"].ToString();
                            }
                        }
                        #endregion
                    }
                
               
                    if (dt.Columns.Contains("BillCity"))
                    {
                        #region Validations of BillAddrCity
                        if (dr["BillCity"].ToString() != string.Empty)
                        {
                            if (dr["BillCity"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillCity (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillCity = dr["BillCity"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillCity = dr["BillCity"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCity = dr["BillCity"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillCity = dr["BillCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillCountry"))
                    {
                        #region Validations of BillAddrCountry
                        if (dr["BillCountry"].ToString() != string.Empty)
                        {
                            if (dr["BillCountry"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillCountry (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillCountry = dr["BillCountry"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillCountry = dr["BillCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCountry = dr["BillCountry"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillCountry = dr["BillCountry"].ToString();
                            }
                        }
                        #endregion
                    }                 
                   


                    if (dt.Columns.Contains("BillPostalCode"))
                    {
                        #region Validations of BillAddrPostalCode
                        if (dr["BillPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["BillPostalCode"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillPostalCode (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillPostalCode = dr["BillPostalCode"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillPostalCode = dr["BillPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillPostalCode = dr["BillPostalCode"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillPostalCode = dr["BillPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillNote"))
                    {
                        #region Validations of BillAddrNote
                        if (dr["BillNote"].ToString() != string.Empty)
                        {
                            if (dr["BillNote"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillNote (" + dr["BillNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillNote = dr["BillNote"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillNote = dr["BillNote"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillNote = dr["BillNote"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillNote = dr["BillNote"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("BillAddrSubDivisionCode"))
                    {
                        #region Validations of BillAddrSubDivisionCode
                        if (dr["BillAddrSubDivisionCode"].ToString() != string.Empty)
                        {
                            if (dr["BillAddrSubDivisionCode"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddrSubDivisionCode (" + dr["BillAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("BillAddrLat"))
                    {
                        #region Validations of BillAddrLat
                        if (dr["BillAddrLat"].ToString() != string.Empty)
                        {
                            if (dr["BillAddrLat"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddrLat (" + dr["BillAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillAddrLat = dr["BillAddrLat"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddrLong"))
                    {
                        #region Validations of BillAddrLong
                        if (dr["BillAddrLong"].ToString() != string.Empty)
                        {
                            if (dr["BillAddrLong"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddrLong (" + dr["BillAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillAddrLong = dr["BillAddrLong"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                            }
                        }
                        #endregion
                    }

                   

                    if (billAddr.BillLine1 != null || billAddr.BillLine2 != null || billAddr.BillLine3 != null || billAddr.BillLine4 != null || billAddr.BillLine5 != null || billAddr.BillCity != null || billAddr.BillCountry != null || billAddr.BillNote != null || billAddr.BillPostalCode != null)
                        customer.PrimaryAddr.Add(billAddr);

                    OnlineEntities.ShipAddr shipAddr = new OnlineEntities.ShipAddr();

                  
                 
                
                    if (dt.Columns.Contains("ShipAddr1"))
                    {
                        #region Validations of ShipAddr1
                        if (dr["ShipAddr1"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr1"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddr1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipLine1 = dr["ShipAddr1"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipLine1 = dr["ShipAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine1 = dr["ShipAddr1"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipLine1 = dr["ShipAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipAddr2"))
                    {
                        #region Validations of ShipAddr2
                        if (dr["ShipAddr2"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr2"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddr2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipLine2 = dr["ShipAddr2"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipLine2 = dr["ShipAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine2 = dr["ShipAddr2"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipLine2 = dr["ShipAddr2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipAddr3"))
                    {
                        #region Validations of ShipAddr3
                        if (dr["ShipAddr3"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr3"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddr3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipLine3 = dr["ShipAddr3"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipLine3 = dr["ShipAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine3 = dr["ShipAddr3"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipLine3 = dr["ShipAddr3"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("ShipAddr4"))
                    {
                        #region Validations of ShipAddr4
                        if (dr["ShipAddr4"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr4"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddr4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipLine4 = dr["ShipAddr4"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipLine4 = dr["ShipAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine4 = dr["ShipAddr4"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipLine4 = dr["ShipAddr4"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("ShipAddr5"))
                    {
                        #region Validations of ShipAddr5
                        if (dr["ShipAddr5"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr5"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddr5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipLine5 = dr["ShipAddr5"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipLine5 = dr["ShipAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine5 = dr["ShipAddr5"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipLine5 = dr["ShipAddr5"].ToString();
                            }
                        }
                        #endregion
                    }


                    
              
              
                    if (dt.Columns.Contains("ShipCity"))
                    {
                        #region Validations of ShipCity
                        if (dr["ShipCity"].ToString() != string.Empty)
                        {
                            if (dr["ShipCity"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipCity (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipCity = dr["ShipCity"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipCity = dr["ShipCity"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCity = dr["ShipCity"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipCity = dr["ShipCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipCountry"))
                    {
                        #region Validations of ShipCountry
                        if (dr["ShipCountry"].ToString() != string.Empty)
                        {
                            if (dr["ShipCountry"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipCountry (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipCountry = dr["ShipCountry"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipCountry = dr["ShipCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCountry = dr["ShipCountry"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipCountry = dr["ShipCountry"].ToString();
                            }
                        }
                        #endregion
                    }



                    if (dt.Columns.Contains("ShipPostalCode"))
                    {
                        #region Validations of ShipAddrPostalCode
                        if (dr["ShipPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["ShipPostalCode"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipPostalCode (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipPostalCode = dr["ShipPostalCode"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipPostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipPostalCode = dr["ShipPostalCode"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipPostalCode = dr["ShipPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipNote"))
                    {
                        #region Validations of ShipNote
                        if (dr["ShipNote"].ToString() != string.Empty)
                        {
                            if (dr["ShipNote"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipNote (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipNote = dr["ShipNote"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipNote = dr["ShipNote"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipNote = dr["ShipNote"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipNote = dr["ShipNote"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ShipAddrSubDivisionCode"))
                    {
                        #region Validations of ShipAddrSubDivisionCode
                        if (dr["ShipAddrSubDivisionCode"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddrSubDivisionCode"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddrSubDivisionCode (" + dr["ShipAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ShipAddrLat"))
                    {
                        #region Validations of ShipAddrLat
                        if (dr["ShipAddrLat"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddrLat"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddrLat (" + dr["ShipAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipAddrLong"))
                    {
                        #region Validations of ShipAddrLong
                        if (dr["ShipAddrLong"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddrLong"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipAddrLong (" + dr["ShipAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                }
                            }
                            else
                            {
                                shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                            }
                        }
                        #endregion
                    }

                

                    if (shipAddr.ShipLine1 != null || shipAddr.ShipLine2 != null || shipAddr.ShipLine3 != null || shipAddr.ShipLine4 != null || shipAddr.ShipLine5 != null || shipAddr.ShipCity != null || shipAddr.ShipCountry != null || shipAddr.ShipNote != null ||shipAddr.ShipPostalCode != null)
                        customer.PrimaryAddrship.Add(shipAddr);
                    #endregion                  

                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Description
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 2000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes (" + dr["Notes"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.Notes = dr["Notes"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.Notes = dr["Notes"].ToString();
                                }
                            }
                            else
                            {
                                customer.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Job"))
                    {
                        #region Validations of Job
                        if (dr["Job"].ToString() != string.Empty)
                        {
                            if (dr["Job"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Job (" + dr["Job"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.Job = dr["Job"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.Job = dr["Job"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.Job = dr["Job"].ToString();
                                }
                            }
                            else
                            {
                                customer.Job = dr["Job"].ToString();
                            }
                        }
                        #endregion
                    }

                    OnlineEntities.ParentRef ParentRef = new OnlineEntities.ParentRef();
                    string[] arr = new string[15];
                    if (customer.FullyQualifiedName.Contains(":"))
                    {
                        arr = customer.FullyQualifiedName.Split(':');

                        string pname = "";
                        int cnt = 0;
                        foreach (var a in arr)
                        {                          
                            cnt++;
                        }
                        for(int i =0;i<cnt-1;i++)
                        {
                            pname += arr[i] + ":";
                        }
                        pname = pname.Remove(pname.Length - 1);
                        ParentRef.Name = pname;
                        customer.ParentRef.Add(ParentRef);
                        customer.DisplayName = arr[cnt - 1];
                    }
                    else
                    {
                        if (!dt.Columns.Contains("DisplayName") || (dr["DisplayName"].ToString() == string.Empty))
                        {
                            customer.DisplayName = customer.FullyQualifiedName;
                        }

                    }                   
                  
                    if (dt.Columns.Contains("Level"))
                    {
                        #region Validations of Level
                        if (dr["Level"].ToString() != string.Empty)
                        {
                            if (dr["Level"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Level (" + dr["Level"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.Level = dr["Level"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.Level = dr["Level"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.Level = dr["Level"].ToString();
                                }
                            }
                            else
                            {
                                customer.Level = dr["Level"].ToString();
                            }
                        }
                        #endregion
                    }

                    OnlineEntities.SalesTermRef SalesTermRef = new OnlineEntities.SalesTermRef();
                    if (dt.Columns.Contains("SalesTermRef"))
                    {
                        #region Validations of SalesTermRef
                        if (dr["SalesTermRef"].ToString() != string.Empty)
                        {
                            if (dr["SalesTermRef"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesTermRef (" + dr["SalesTermRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SalesTermRef.Name = dr["SalesTermRef"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                }
                            }
                            else
                            {
                                SalesTermRef.Name = dr["SalesTermRef"].ToString();
                            }
                        }
                        #endregion
                    }
                    customer.SalesTermRef.Add(SalesTermRef);

                    OnlineEntities.PaymentMethodRef PaymentMethodRef = new OnlineEntities.PaymentMethodRef();
                    if (dt.Columns.Contains("PaymentMethodRef"))
                    {
                        #region Validations of SalesTermRef
                        if (dr["PaymentMethodRef"].ToString() != string.Empty)
                        {
                            if (dr["PaymentMethodRef"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PaymentMethodRef (" + dr["PaymentMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                    }
                                }
                                else
                                {
                                    PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                                }
                            }
                            else
                            {
                                PaymentMethodRef.Name = dr["PaymentMethodRef"].ToString();
                            }
                        }
                        #endregion
                    }
                    customer.PaymentMethodRef.Add(PaymentMethodRef);

                    if (dt.Columns.Contains("OpenBalance"))
                    {
                        #region Validations for OpenBalance
                        if (dr["OpenBalance"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["OpenBalance"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OpenBalance ( " + dr["OpenBalance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.OpenBalance = dr["OpenBalance"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.OpenBalance = dr["OpenBalance"].ToString();
                                    }
                                }
                                else
                                    customer.OpenBalance = dr["OpenBalance"].ToString();
                            }
                            else
                            {
                                customer.OpenBalance = string.Format("{0:00000000.00}", Convert.ToDouble(dr["OpenBalance"].ToString()));
                            }
                        }

                        #endregion

                    }

                    if (dt.Columns.Contains("OpenBalanceDate"))
                    {
                        #region validations of OpenBalanceDate
                        if (dr["OpenBalanceDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["OpenBalanceDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out CustomerDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This OpenBalanceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            customer.OpenBalanceDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            customer.OpenBalanceDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        customer.OpenBalanceDate = datevalue;
                                    }
                                }
                                else
                                {
                                    CustomerDt = dttest;
                                    customer.OpenBalanceDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                CustomerDt = Convert.ToDateTime(datevalue);
                                customer.OpenBalanceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    OnlineEntities.CurrencyRef CustomerRef = new OnlineEntities.CurrencyRef();

                    //Axis-710
                    if (dt.Columns.Contains("CurrencyRef"))
                    {
                        #region Validations of CustomerRefName
                        if (dr["CurrencyRef"].ToString() != string.Empty)
                        {
                            if (dr["CurrencyRef"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        CustomerRef.Name = dr["CurrencyRef"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        CustomerRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            else
                            {
                                CustomerRef.Name = dr["CurrencyRef"].ToString();
                            }
                        }
                        #endregion
                    }
                    //Axis-710 ends
                    customer.CurrencyRef.Add(CustomerRef);  

                    if (dt.Columns.Contains("PreferredDeliveryMethod"))
                    {
                        #region Validations of PreferredDeliveryMethodOf
                        if (dr["PreferredDeliveryMethod"].ToString() != string.Empty)
                        {
                            if (dr["PreferredDeliveryMethod"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PreferredDeliveryMethod (" + dr["PreferredDeliveryMethod"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.PreferredDeliveryMethod = dr["PreferredDeliveryMethod"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.PreferredDeliveryMethod = dr["PreferredDeliveryMethod"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.PreferredDeliveryMethod = dr["PreferredDeliveryMethod"].ToString();
                                }
                            }
                            else
                            {
                                customer.PreferredDeliveryMethod = dr["PreferredDeliveryMethod"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ResaleNum"))
                    {
                        #region Validations of ResaleNum
                        if (dr["ResaleNum"].ToString() != string.Empty)
                        {
                            if (dr["ResaleNum"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ResaleNum (" + dr["ResaleNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        customer.ResaleNum = dr["ResaleNum"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        customer.ResaleNum = dr["ResaleNum"].ToString();
                                    }
                                }
                                else
                                {
                                    customer.ResaleNum = dr["ResaleNum"].ToString();
                                }
                            }
                            else
                            {
                                customer.ResaleNum = dr["ResaleNum"].ToString();
                            }
                        }
                        #endregion
                    }

                    coll.Add(customer);

                }
                else
                {
                    return null;
                }
            }

            #endregion
            
            #endregion

            return coll;

        }

    }
}
