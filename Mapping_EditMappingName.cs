﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class Mapping_EditMappingName : Form
    {
        private List<string> _functionNames;
        private bool closeForm=true;

        public string mappingName
        {
            get
            {
                return this.textBoxFunctionName.Text.TrimStart().TrimEnd().Trim();
            }
        }
        public Mapping_EditMappingName()
        {
            InitializeComponent();
        }


        public Mapping_EditMappingName(List<string> mappingNames , string mappingName)
        {
            _functionNames = mappingNames;
            InitializeComponent();
            LabelWarning.Text = "A map with the name " + mappingName + " already exist do you want to replace it or rename it?";
            HideShowNewMappingDetails(false);

        }
        private void HideShowNewMappingDetails(bool visibility)
        {
            labelEnterMappingName.Visible = visibility;
            textBoxFunctionName.Visible = visibility;
            buttonCancel.Visible = visibility;
            buttonOK.Visible = visibility;
          
            btnClose.Visible = !visibility;
            btnRename.Visible = !visibility;
            btnReplace.Visible = !visibility;
            LabelWarning.Visible = !visibility;
        }


            private void buttonOK_Click(object sender, EventArgs e)
        {
            string functionName = this.textBoxFunctionName.Text.Trim().TrimStart().TrimEnd();
            if (functionName == string.Empty)
            {
                MessageBox.Show("Please enter Mapping name", "Zed Axis");

                textBoxFunctionName.Focus();
                closeForm = false;
            }
            else if(functionName.Contains("<")|| functionName.Contains(">"))
            {
                MessageBox.Show("Enter valid Mapping name, (Avoid keywords as '<' and '>').", "Zed Axis");
                textBoxFunctionName.Clear();
                textBoxFunctionName.Focus();

                closeForm = false;
            }
            else
            {
                if (_functionNames.Contains(functionName))
                {
                    MessageBox.Show("Mapping with this name already exists, Please try with different name", "Zed Axis");
                    textBoxFunctionName.Focus();

                    closeForm = false;
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void Mapping_EditFunctionName_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!closeForm)
                e.Cancel = true;
            closeForm = true;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            HideShowNewMappingDetails(true);

        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }
    }
}
