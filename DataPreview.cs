using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.Xml;
using EDI.Constant;
using System.Xml.Xsl;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

namespace TransactionImporter
{
    public partial class DataPreview : Form
    {
        ////Create an instance of DataPreview
        //private static DataPreview m_DataPreview;
        ////Create an instance of SheetName
        //private string m_SheetName;
        ////Create an instance of testcount
        //private int m_testcount = 0;
        ////Create an instance of newcount.
        //private int m_newcount = 0;
        ////Create an instance of FilePathTi
        //private string m_FilePathTi = string.Empty;
        //
        //private string importTypeName = string.Empty;
        ////Create an instance of Rectangle for Preview Box Operations.
        //private Rectangle dragBoxFromMouseDown;
        ////Create an instance of rowindex from Mouse drag.
        //private int rowIndexFromMouseDown;
        ////Create an instance of column index from Mouse drag
        //private int ColumnIndexFromMouseDown;
        ////Create an instance of column index To Mouse drop
        //private int ColumnIndexOfItemUnderMouseToDrop;
        ////Create an instance of row index to Mouse drop
        //private int rowIndexOfItemUnderMouseToDrop;

        //private DataSet m_xmlDataSet;

        //string ParentTag = string.Empty;
        //Hashtable xmllist = new Hashtable();

        //Dictionary<string, string> headerDictionary = null;
        //DataTable dt = null;


        /// <summary>
        /// This method is used for getting instance.
        /// </summary>
        /// <returns></returns>
        //public static DataPreview GetInstance()
        //{
        //    if (m_DataPreview == null)
        //        m_DataPreview = new DataPreview();
        //    return m_DataPreview;
        //}

        //#region Properties

        ///// <summary>
        ///// Gets or Sets sheetname
        ///// </summary>
        //public string SheetName
        //{
        //    get
        //    {
        //        return this.m_SheetName;
        //    }
        //    set
        //    {
        //        if (this.m_SheetName != value)
        //        {
        //            this.m_SheetName = value;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Gets or Sets FilePathTi
        ///// </summary>
        //public string FilePathTI
        //{
        //    get
        //    {
        //        return this.m_FilePathTi;
        //    }
        //    set
        //    {
        //        this.m_FilePathTi = value;
        //    }
        //}

        ///// <summary>
        ///// Gets or Sets Test count
        ///// </summary>
        //public int TestCount
        //{
        //    get
        //    {
        //        return m_testcount;
        //    }
        //    set
        //    {
        //        m_testcount = value;
        //    }
        //}

        ///// <summary>
        ///// Gets or sets new count.
        ///// </summary>
        //public int NewCount
        //{
        //    get
        //    { return this.m_newcount; }
        //    set
        //    {
        //        this.m_newcount = value;
        //    }
        //}

        //#endregion

        //private DataTable m_ModifiedData;//Create an instance of Modified Data

        ///// <summary>
        ///// Gets or sets the xmldata.
        ///// </summary>
        //public DataSet XmlDataSet
        //{
        //    get { return m_xmlDataSet; }
        //    set { m_xmlDataSet = value; }
        //}

        ///// <summary>
        ///// Gets or Sets Modified Data.
        ///// </summary>
        //public DataTable ModifiedData
        //{
        //    get { return this.m_ModifiedData; }
        //    set
        //    {
        //        this.m_ModifiedData = value;
        //    }
        //}

        //private DataSet m_dsModify;//Create an instance of Modify data.

        ///// <summary>
        ///// Gets or Sets modify data.
        ///// </summary>
        //public DataSet M_dsModify
        //{
        //    get { return this.m_dsModify; }
        //    set
        //    {
        //        this.m_dsModify = value;
        //    }
        //}

        //public DataPreview()
        //{
        //    InitializeComponent();
            

        //}

        //private void buttonOk_Click(object sender, EventArgs e)
        //{
        //    string previewFile = string.Empty;
        //    try
        //    {
        //        if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
        //        {
        //            if (buttonReload.Enabled)
        //            {
        //                //For update changes
        //                this.m_ModifiedData.AcceptChanges();
        //                //Assinging datatabel to property.
        //                this.ModifiedData = this.m_ModifiedData;
        //                this.dataGridViewDataPreview.Update();
        //                this.dataGridViewDataPreview.Refresh();
        //                this.DialogResult = DialogResult.OK;
        //                this.Close();
        //            }
        //            else
        //                this.Close();
        //        }
        //        else
        //        {
        //            #region For New code for replacing xml dataset and creating Finalpreview file
        //            if (buttonReload.Enabled)
        //            {
        //                //newly added for bug fixing
        //                if (!CommonUtilities.GetInstance().SelectedMappingName.ToString().Equals(string.Empty) && !CommonUtilities.GetInstance().SelectedMappingName.ToString().Equals("<Add New Mapping>"))
        //                {

        //                    this.dt.AcceptChanges();

        //                    this.dataGridViewDataPreview.Update();

        //                    this.dataGridViewDataPreview.Refresh();

        //                    string finalPreviewFile = getMappedXmlForPreview(CommonUtilities.GetInstance().SelectedMappingName.ToString(), dt);
                    
        //                    CommonUtilities.GetInstance().FinalPreviewFileName = finalPreviewFile;
        //                    headerDictionary = null;
        //                    dt = null;
    
        //                    this.DialogResult = DialogResult.OK;
    
        //                    this.Close();
        //                }
        //                else
        //                {
        //                    this.Close();
        //                }
        //            }

        //            #endregion

        //            #region Previous commented code for xml dataset
        //            //previewFile = Application.StartupPath;
        //            //previewFile += "\\" + "preview" + ".xml";

        //            //try
        //            //{
        //            //    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            //        previewFile = previewFile.Replace("Program Files", Constants.xpPath);
        //            //    else
        //            //        previewFile = previewFile.Replace("Program Files", "Users\\Public\\Documents");
        //            //}
        //            //catch { }

        //            //if (buttonReload.Enabled)
        //            //{
        //            //    //For update changes
        //            //    this.m_xmlDataSet.AcceptChanges();

        //            //    //Assinging datatabel to property.
        //            //    this.XmlDataSet = this.m_xmlDataSet;

        //            //    this.dataGridViewDataPreview.Update();

        //            //    this.dataGridViewDataPreview.Refresh();

        //            //    try
        //            //    {
        //            //        XmlDataSet.WriteXml(previewFile);
        //            //    }
        //            //    catch { }

        //            //    //newly added for bug fixing
        //            //    if (!CommonUtilities.GetInstance().SelectedMappingName.ToString().Equals(string.Empty) && !CommonUtilities.GetInstance().SelectedMappingName.ToString().Equals("<Add New Mapping>"))
        //            //    {

        //            //        string mappedXmlFile = getMappedXml(CommonUtilities.GetInstance().SelectedMappingName);
        //            //        try
        //            //        {
        //            //            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            //                mappedXmlFile = mappedXmlFile.Replace("Program Files", Constants.xpPath);
        //            //            else
        //            //                mappedXmlFile = mappedXmlFile.Replace("Program Files", "Users\\Public\\Documents");
        //            //        }
        //            //        catch { }


        //            //        XmlDocument xdoc = new XmlDocument();

        //            //        try
        //            //        {
        //            //            xdoc.Load(mappedXmlFile);
        //            //        }
        //            //        catch { }

        //            //        XmlNode root = (XmlNode)xdoc.DocumentElement;
        //            //        XmlNode node = root.ChildNodes[0].ChildNodes[0].ChildNodes[0];


        //            //        XmlDocument xdoc1 = new XmlDocument();
        //            //        try
        //            //        {
        //            //            xdoc1.Load(previewFile);
        //            //        }
        //            //        catch { }

        //            //        XmlNode root1 = (XmlNode)xdoc1.DocumentElement;
        //            //        XmlNode node1 = root1.ChildNodes[0].ChildNodes[0].ChildNodes[0];


        //            //        string importType = xdoc.ChildNodes[2].ChildNodes[0].ChildNodes[0].ChildNodes[0].Name;
        //            //        importTypeName = importType.Substring(0, importType.Length - 3);


        //            //        string finalPreviewFile = Application.StartupPath + "\\" + "finalPreview" + ".xml"; ;

        //            //        try
        //            //        {
        //            //            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            //                finalPreviewFile = finalPreviewFile.Replace("Program Files", Constants.xpPath);
        //            //            else
        //            //                finalPreviewFile = finalPreviewFile.Replace("Program Files", "Users\\Public\\Documents");
        //            //        }
        //            //        catch { }

        //            //        System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();


        //            //        requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
        //            //        requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + "6.0" + "\""));

        //            //        //Create the outer request envelope tag
        //            //        System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
        //            //        requestXmlDoc.AppendChild(outer);

        //            //        //Create the inner request envelope & any needed attributes
        //            //        System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
        //            //        outer.AppendChild(inner);
        //            //        inner.SetAttribute("onError", "stopOnError");

        //            //        //Create JournalEntryAddRq aggregate and fill in field values for it
        //            //        System.Xml.XmlElement InvoiceAddRq = requestXmlDoc.CreateElement(importTypeName + "AddRq");
        //            //        inner.AppendChild(InvoiceAddRq);

        //            //        //Create JournalEntryAdd aggregate and fill in field values for it
        //            //        System.Xml.XmlElement InvoiceAdd = requestXmlDoc.CreateElement(importTypeName + "Add");

        //            //        InvoiceAddRq.AppendChild(InvoiceAdd);

        //            //        requestXmlDoc.Save(finalPreviewFile);

        //            //        getPreviewXmlElements(node, node1, InvoiceAdd, requestXmlDoc, finalPreviewFile);

        //            //        try
        //            //        {
        //            //            //delete mapping xml file
        //            //            File.Delete(mappedXmlFile);
        //            //        }
        //            //        catch { }

        //            //        try
        //            //        {
        //            //            //delete preview file which is generated from dataset
        //            //            File.Delete(previewFile);
        //            //        }
        //            //        catch { }

        //            //        CommonUtilities.GetInstance().FinalPreviewFileName = finalPreviewFile;

        //            //        this.DialogResult = DialogResult.OK;

        //            //        this.Close();
        //            //    }
        //            //    else
        //            //    {
        //            //        try
        //            //        {
        //            //            //delete preview file which is generated from dataset
        //            //            File.Delete(previewFile);
        //            //        }
        //            //        catch { }

        //            //        this.Close();
        //            //    }

        //            //}
        //            #endregion



        //        }
        //    }
        //    catch
        //    {
        //        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG096"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        this.Close();
        //        return;
        //    }
        //}

        /// <summary>
        /// get xml File for preview function
        /// </summary>
        /// <param name="node"></param>
        /// <param name="node1"></param>
        /// <param name="parentnode1"></param>
        /// <param name="requestXmlDoc"></param>

        //private void getPreviewXmlElements(XmlNode node, XmlNode node1, XmlElement parentnode1, XmlDocument requestXmlDoc, string filename)
        //{
        //    for (int i = 0; i < node.ChildNodes.Count; i++)
        //    {
        //        for (int j = 0; j < node1.ChildNodes.Count; j++)
        //        {
        //            if (node.ChildNodes[i].Name.Equals(node1.ChildNodes[j].Name) && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        //            {
        //                System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        //                parentnode1.AppendChild(parentNode);

        //                if (node.ChildNodes[i].ChildNodes.Count > 0 && node1.ChildNodes[j].ChildNodes.Count > 0)
        //                {
        //                    getPreviewXmlElements(node.ChildNodes[i], node1.ChildNodes[j], parentNode, requestXmlDoc, filename);
        //                }

        //            }
        //            else if (node.ChildNodes[i].Name.Equals(node1.ChildNodes[j].Name))
        //            {
        //                System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        //                parentNode.InnerText = node1.ChildNodes[j].InnerText;
        //                parentnode1.AppendChild(parentNode);

        //            }
        //        }
        //    }
        //    requestXmlDoc.Save(filename);
        //}


        /////// <summary>
        /////// This method is used for Updating import data with constant values.
        /////// </summary>
        /////// <param name="dtUpdated"></param>
        /////// <returns></returns>
        //////public DataTable GetPreviewORImportData(DataTable dtUpdated)
        //////{
        //////    try
        //////    {
        //////        if (this.TestCount != 1)
        //////        {
        //////            string mapName = CommonUtilities.GetInstance().SelectedMapping.Name;
        //////            string constantColumnList = string.Empty;
        //////            string m_settingsPath = Application.StartupPath;
        //////            m_settingsPath += @"\Settings.xml";
        //////            try
        //////            {
        //////                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //////                    m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
        //////                else
        //////                    m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents"); 
        //////            }
        //////            catch { }
                   
        //////            System.Xml.XmlDocument xdoc = new XmlDocument();
        //////            xdoc.Load(m_settingsPath);

        //////            XmlNode root = (XmlNode)xdoc.DocumentElement;
        //////            for (int i = 0; i < root.ChildNodes.Count; i++)
        //////            {
        //////                if (root.ChildNodes.Item(i).Name == "ConstantMappings")
        //////                {
        //////                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
        //////                    foreach (XmlNode node in userMappingsXML)
        //////                    {
        //////                        if (node.FirstChild.InnerText == mapName)
        //////                        {

        //////                            foreach (XmlNode childNodes in node.ChildNodes)
        //////                            {

        //////                                if (childNodes.Name == "name")
        //////                                {

        //////                                }
        //////                                else
        //////                                    if (childNodes.Name != "ImportType")
        //////                                    {
        //////                                        if (childNodes.Name != "MappingType")
        //////                                        {
        //////                                            if (childNodes.Name != string.Empty && childNodes.InnerText != string.Empty)
        //////                                            {
        //////                                                constantColumnList += childNodes.Name + "~" + childNodes.InnerText + "#";
        //////                                            }
        //////                                        }
        //////                                    }
        //////                            }
        //////                            break;
        //////                        }

        //////                    }
        //////                    break;
        //////                }
        //////            }
        //////            if (constantColumnList != string.Empty)
        //////            {
        //////                string[] constantValuesArray = constantColumnList.Split('#');
        //////                if (dtUpdated == null)
        //////                {
        //////                    dtUpdated = new DataTable();
        //////                    DataRow dr = dtUpdated.NewRow();
        //////                    for (int count = 0; count < constantValuesArray.Length; count++)
        //////                    {
        //////                        if (constantValuesArray[count] != string.Empty)
        //////                        {
        //////                            string[] splitConstants = constantValuesArray[count].Split('~');
        //////                            DataColumn dtColumnConstant = new DataColumn();
        //////                            dtColumnConstant.ColumnName = splitConstants[0].Replace("[", string.Empty).Replace("]", string.Empty).Trim();
        //////                            try
        //////                            {
        //////                                dtUpdated.Columns.Add(dtColumnConstant);

        //////                            }
        //////                            catch
        //////                            {
        //////                                dtUpdated.Columns.Remove(dtColumnConstant);
        //////                                dtUpdated.AcceptChanges();
        //////                                dtUpdated.Columns.Add(dtColumnConstant);
        //////                            }
                                 
        //////                            dr[dtColumnConstant] = splitConstants[1];
                                   
                                   
        //////                        }
        //////                    }
        //////                    dtUpdated.Rows.Add(dr);
        //////                }
        //////                else
        //////                {
        //////                    for (int count = 0; count < constantValuesArray.Length; count++)
        //////                    {
        //////                        if (constantValuesArray[count] != string.Empty)
        //////                        {
        //////                            string[] splitConstants = constantValuesArray[count].Split('~');
        //////                            DataColumn dtColumnConstant = new DataColumn();
        //////                            dtColumnConstant.ColumnName = splitConstants[0].Replace("[", string.Empty).Replace("]", string.Empty).Trim();
        //////                            try
        //////                            {
                                      
        //////                                dtUpdated.Columns.Add(dtColumnConstant);

        //////                            }
        //////                            catch
        //////                            {
        //////                                dtUpdated.Columns.Remove(dtColumnConstant);
        //////                                dtUpdated.AcceptChanges();
        //////                                dtUpdated.Columns.Add(dtColumnConstant);
        //////                            }
                                   
        //////                            for (int index = 0; index < dtUpdated.Rows.Count; index++)
        //////                            {
        //////                                dtUpdated.Rows[index][dtColumnConstant] = splitConstants[1];
        //////                            }
                                    
        //////                        }
        //////                    }
        //////                }

        //////            }
        //////        }

        //////    }
        //////    catch { }
        //////    return dtUpdated;
        //////}

        /////// <summary>
        /////// This method is used for display data of files.
        /////// </summary>
        /////// <returns></returns>
        //////public bool PopulateDataWithoutPreview()
        //////{
        //////    try
        //////    {
        //////        //Checking Datatabel is null or not.
        //////        if (this.m_ModifiedData == null)
        //////        {
        //////            if (DataProcessingBlocks.CommonUtilities.GetInstance().SelectedMapping != null)//Checking Selected Mapping is null or not.
        //////            {
        //////                DataProcessingBlocks.ExcellUtility.GetInstance().DataNumber = 0;

        //////                //Get data with selected mapping and assign to Datatable.
        //////                this.m_ModifiedData = DataProcessingBlocks.ExcellUtility.GetInstance().GetDataForSelectedMapping(this.m_SheetName);
                        
        //////            }
        //////            else
        //////            {
        //////                DataProcessingBlocks.ExcellUtility.GetInstance().DataNumber = this.TestCount;
        //////                //Get data without selected mapping and assign to Datatable.
        //////                this.m_ModifiedData = DataProcessingBlocks.ExcellUtility.GetInstance().GetDataBySheetName(this.m_SheetName);
                        
        //////            }
        //////        }
        //////        else
        //////        {
        //////            if (DataProcessingBlocks.CommonUtilities.GetInstance().SelectedMapping != null)//checking selected mapping is null or not.
        //////            {
        //////                if (this.NewCount == 0)
        //////                {
        //////                    DataProcessingBlocks.ExcellUtility.GetInstance().DataNumber = 0;
        //////                    //Get data with selected mapping and assign to Datatable.
        //////                    this.m_ModifiedData = DataProcessingBlocks.ExcellUtility.GetInstance().GetDataForSelectedMapping(this.m_SheetName);
        //////                }


        //////            }
        //////            else
        //////            {
        //////                DataProcessingBlocks.ExcellUtility.GetInstance().DataNumber = this.TestCount;
        //////                //Get data without selected mapping and assign to Datatable.
        //////                this.m_ModifiedData = DataProcessingBlocks.ExcellUtility.GetInstance().GetDataBySheetName(this.m_SheetName);
                        
        //////            }

        //////        }
        //////        //Checking file is text file or not
        //////        if (System.IO.Path.GetExtension(this.FilePathTI).ToLower() == ".txt")
        //////        {
        //////            DataProcessingBlocks.ExcellUtility.GetInstance().CurrentSheetName = string.Empty;
        //////            //Assing text file path
        //////            DataProcessingBlocks.ExcellUtility.GetInstance().FilePath = this.FilePathTI;
        //////        }
        //////        this.m_ModifiedData = GetPreviewORImportData(this.m_ModifiedData);
        //////        //Bind the datatabel to datagridview.
        //////        this.bindingSourcePreviewData.DataSource = this.m_ModifiedData;
        //////        //this.bindingSourcePreviewData.DataSource = M_dsModify;
        //////        return true;
        //////    }
        //////    catch (TIException TIex)
        //////    {
        //////        //MessageBox.Show(TIex.Message);
        //////        throw new DataProcessingBlocks.TIException(TIex.ErrorCode);
        //////        //return false;
        //////    }
        //////}

        ///////// <summary>
        ///////// This method is used for display data for preview.
        ///////// </summary>
        ///////// <returns></returns>
        //////public bool PopulateDataToPreview()
        //////{
        //////    try
        //////    {
        //////        //Enabling the datagridtable for the file except the xml.
        //////        dataGridViewDataPreview.ReadOnly = false;
        //////        comboBoxXmlPreviewTable.Visible = false;
        //////        buttonReload.Enabled = true;
        //////        dataGridViewDataPreview.AllowUserToDeleteRows = true;

        //////        //Assinging datatable to null.
        //////        this.m_ModifiedData = null;
        //////        this.m_dsModify = new DataSet();
        //////        if (this.m_ModifiedData == null)//Checking datatabel is null
        //////        {
        //////            if (DataProcessingBlocks.CommonUtilities.GetInstance().SelectedMapping != null)//Checking selectedMapping is null or not.
        //////            {
        //////                if (this.SheetName == null)
        //////                    this.SheetName = DataProcessingBlocks.ExcellUtility.GetInstance().CurrentSheetName;
        //////                DataProcessingBlocks.ExcellUtility.GetInstance().DataNumber = this.TestCount;
        //////                //Gets selectedmapping data and assign to Datatable.
        //////                this.m_ModifiedData = DataProcessingBlocks.ExcellUtility.GetInstance().GetDataForSelectedMapping(this.SheetName);
                       
        //////            }
        //////            else
        //////            {
        //////                if (this.SheetName == null)
        //////                    this.SheetName = DataProcessingBlocks.ExcellUtility.GetInstance().CurrentSheetName;
        //////                //Gets selectedmapping data and assign to Datatable.
        //////                DataProcessingBlocks.ExcellUtility.GetInstance().DataNumber = this.TestCount;
        //////                this.m_ModifiedData = DataProcessingBlocks.ExcellUtility.GetInstance().GetDataBySheetName(this.SheetName);
                        
        //////            }
        //////        }
        //////        //Checking uploaded file is textfile or not.
        //////        if (System.IO.Path.GetExtension(this.FilePathTI).ToLower() == ".txt")
        //////        {
        //////            DataProcessingBlocks.ExcellUtility.GetInstance().CurrentSheetName = string.Empty;
        //////            //Assinging file path 
        //////            DataProcessingBlocks.ExcellUtility.GetInstance().FilePath = this.FilePathTI;
        //////        }
        //////        this.m_ModifiedData = GetPreviewORImportData(this.m_ModifiedData);
        //////        //Assinging Datatabel
        //////        this.bindingSourcePreviewData.DataSource = this.m_ModifiedData;
        //////        try
        //////        {

        //////            this.dataGridViewDataPreview.DataSource = this.bindingSourcePreviewData.DataSource;
        //////        }
        //////        catch
        //////        {

        //////        }
                
        //////        return true;
        //////    }
        //////    catch (TIException TIex)
        //////    {
        //////        //MessageBox.Show(TIex.Message);
        //////        throw new DataProcessingBlocks.TIException(TIex.ErrorCode);
        //////        //return false;
        //////    }
        //////}

        //////private void buttonReload_Click(object sender, EventArgs e)
        //////{
        //////    try
        //////    {
        //////        if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
        //////        {
        //////            //Asssing datatable to null.
        //////            this.m_ModifiedData = null;
        //////            //Calling PopulateDataToPreview method for display data.
        //////            this.PopulateDataToPreview();
        //////        }
        //////        else
        //////        {
        //////            if (!CommonUtilities.GetInstance().SelectedMappingName.ToString().Equals(string.Empty) && !CommonUtilities.GetInstance().SelectedMappingName.ToString().Equals("<Add New Mapping>"))
        //////            {
        //////                #region commented old code for Valid mapping is selected for browse xml file
        //////                //DataSet dsReload = new DataSet();

        //////                //this.m_xmlDataSet = null;

        //////                //string xmlfileName = getMappedXml(CommonUtilities.GetInstance().SelectedMappingName.ToString());

        //////                ////create OSRelated Path
        //////                ////CommonUtilities.GetInstance().osRelatedPath(xmlfileName);
        //////                //try
        //////                //{
        //////                //    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //////                //        xmlfileName = xmlfileName.Replace("Program Files", Constants.xpPath);
        //////                //    else
        //////                //        xmlfileName = xmlfileName.Replace("Program Files", "Users\\Public\\Documents");
        //////                //}
        //////                //catch { }

        //////                //try
        //////                //{
        //////                //    dsReload.ReadXml(xmlfileName);
        //////                //}
        //////                //catch { }

        //////                //DataPreview.GetInstance().XmlDataSet = dsReload;

        //////                //try
        //////                //{
        //////                //    File.Delete(xmlfileName);
        //////                //}
        //////                //catch { }

        //////                //this.PopulateXmlDatatoPreview();

        //////                //string finalPreview = CommonUtilities.GetInstance().FinalPreviewFileName;

        //////                //if (File.Exists(finalPreview))
        //////                //{
        //////                //    File.Delete(finalPreview);
        //////                //}
        //////                #endregion

        //////                #region New code for Valid mapping is selected for browse xml file
        //////                headerDictionary = null;
        //////                dt = null;

        //////                //Assign data table for selected mapping
        //////                this.dataGridViewDataPreview.DataSource = GetDataSourceForXmlPreview();


        //////                string finalPreview = CommonUtilities.GetInstance().FinalPreviewFileName;

        //////                if (File.Exists(finalPreview))
        //////                {
        //////                    File.Delete(finalPreview);
        //////                }

        //////                #endregion


        //////            }
        //////            else if (CommonUtilities.GetInstance().SelectedMappingName.ToString().Equals(string.Empty))
        //////            {
        //////                #region comented code preview without selecting valid mapping for browse xml file
        //////                //DataSet ds = new DataSet();

        //////                //this.m_xmlDataSet = null;

        //////                //try
        //////                //{
        //////                //    ds.ReadXml(CommonUtilities.GetInstance().BrowseFileName);
        //////                //}
        //////                //catch { }

        //////                //DataPreview.GetInstance().XmlDataSet = ds;

        //////                //this.PopulateXmlDatatoPreview();

        //////                //string finalPreview = CommonUtilities.GetInstance().FinalPreviewFileName;

        //////                //if (File.Exists(finalPreview))
        //////                //{
        //////                //    File.Delete(finalPreview);
        //////                //}
        //////                #endregion

        //////                #region preview without selecting valid mapping for browse xml file

        //////                //Assign data table for browse xml file
        //////                this.dataGridViewDataPreview.DataSource = GetDataSourceForBrowseFilePreview();

        //////                string finalPreview = CommonUtilities.GetInstance().FinalPreviewFileName;

        //////                if (File.Exists(finalPreview))
        //////                {
        //////                    File.Delete(finalPreview);
        //////                }

        //////                #endregion
        //////            }
        //////        }

        //////    }
        //////    catch (TIException Tiex)
        //////    {
        //////        MessageBox.Show(Tiex.Message.ToString());

        //////    }
        //////}

        /////// <summary>
        /////// This method is used for user to add rows.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_AllowUserToAddRowsChanged(object sender, EventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        try
        ////        {
        ////            if (this.TestCount == 1)
        ////            {
        ////                //Display message when mapping is not created or selected
        ////                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                this.Close();
        ////                return;
        ////            }
        ////            this.m_ModifiedData.AcceptChanges();
        ////            this.NewCount = 1;
        ////            this.ModifiedData = this.m_ModifiedData;
        ////        }
        ////        catch
        ////        { }
        ////    }
        ////}

        /////// <summary>
        /////// This method is used for user to delete rows.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_AllowUserToDeleteRowsChanged(object sender, EventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        try
        ////        {
        ////            if (this.TestCount == 1)
        ////            {
        ////                //Display message when mapping is not created or selected
        ////                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                this.Close();
        ////                return;
        ////            }
        ////            this.m_ModifiedData.AcceptChanges();
        ////            this.NewCount = 1;
        ////            this.ModifiedData = this.m_ModifiedData;
        ////        }
        ////        catch
        ////        { }
        ////    }
        ////}

        /////// <summary>
        /////// This method is used to add rows.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        try
        ////        {
        ////            if (this.TestCount == 1)
        ////            {
        ////                //Display message when mapping is not created or selected
        ////                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                this.Close();
        ////                return;
        ////            }
        ////            this.m_ModifiedData.AcceptChanges();
        ////            this.NewCount = 1;
        ////            this.ModifiedData = this.m_ModifiedData;
        ////        }
        ////        catch
        ////        { }
        ////    }
        ////}

        /////// <summary>
        /////// This method is called when user made changes in preview window.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_KeyUp(object sender, KeyEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        try
        ////        {
        ////            if (this.TestCount == 1)
        ////            {
        ////                //Display message when mapping is not created or selected
        ////                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                this.Close();
        ////                return;
        ////            }
        ////            this.m_ModifiedData.AcceptChanges();
        ////            this.NewCount = 1;
        ////            this.ModifiedData = this.m_ModifiedData;
        ////        }
        ////        catch
        ////        { }
        ////    }
        ////}

        /////// <summary>
        /////// This method is called when user made changes in preview window.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        try
        ////        {
        ////            if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
        ////            {
        ////                if (this.TestCount == 1)
        ////                {
        ////                    //Display message when mapping is not created or selected
        ////                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                    this.Close();
        ////                    return;
        ////                }
        ////                this.m_ModifiedData.AcceptChanges();
        ////                this.NewCount = 1;
        ////                this.ModifiedData = this.m_ModifiedData;
        ////            }
        ////            else
        ////            {

        ////                if (this.TestCount == 1)
        ////                {
        ////                    //Display message when mapping is not created or selected
        ////                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                    this.Close();
        ////                    return;
        ////                }
        ////                this.m_xmlDataSet.AcceptChanges();
        ////                this.NewCount = 1;
        ////                this.XmlDataSet = this.m_xmlDataSet;
        ////            }
        ////        }
        ////        catch
        ////        { }
        ////    }
        ////}

        /////// <summary>
        /////// This event is used for delete row from preview window.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void deleteRowsToolStripMenuItem_Click(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        //Get the collection of rows.
        ////        DataGridViewSelectedRowCollection dataGridViewSelectedRows = dataGridViewDataPreview.SelectedRows;

        ////        if (dataGridViewDataPreview.ReadOnly == true)
        ////        {
        ////            MessageBox.Show(Constants.NoDeleteMsg,Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////            return;
        ////        }
        ////        if (dataGridViewSelectedRows != null)//Checking selected rows is null or not.
        ////        {
        ////            try
        ////            {
        ////                if (this.TestCount == 1)
        ////                {
        ////                    //Display message when mapping is not created or selected
        ////                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                    this.Close();
        ////                    return;
        ////                }
        ////                //getting row index array
        ////                int[] selectedRowIndexs = new int[dataGridViewSelectedRows.Count];
        ////                int count = 0;

        ////                foreach (DataGridViewRow dgrow in dataGridViewSelectedRows)
        ////                {
        ////                    selectedRowIndexs[count] = dgrow.Index;
        ////                    //removing rows from preview window.
        ////                    this.m_ModifiedData.Rows.RemoveAt(dgrow.Index);
        ////                    count++;

        ////                }
        ////                this.m_ModifiedData.AcceptChanges();
        ////                this.NewCount = 1;
        ////                this.ModifiedData = this.m_ModifiedData;

        ////            }
        ////            catch
        ////            {
        ////                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER006"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                //MessageBox.Show("Please select or generate mapping then make the changes in preview box.");
        ////                return;
        ////            }
        ////        }
        ////    }
        ////    catch
        ////    { }
        ////}

        /////// <summary>
        /////// This method is used for delete rows
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        try
        ////        {
        ////            if (this.TestCount == 1)
        ////            {
        ////                //Display message when mapping is not created or selected
        ////                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                this.Close();
        ////                return;
        ////            }
        ////            this.m_ModifiedData.AcceptChanges();
        ////            this.NewCount = 1;
        ////            this.ModifiedData = this.m_ModifiedData;
        ////        }
        ////        catch
        ////        { }
        ////    }
        ////}
        
        /////// <summary>
        /////// This method is used for display data error.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        ////{
        ////    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER007"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////    return;
        ////}
       
        /////// <summary>
        /////// This method is used for coping row when mouse move.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_MouseMove(object sender, MouseEventArgs e)
        ////{
        ////    try
        ////    {
        ////        if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
        ////        {

        ////            // If the mouse moves outside the rectangle, start the drag.
        ////            if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
        ////            {
        ////                // Proceed with the drag and drop, passing in the list item.                    
        ////                DragDropEffects dropEffect = dataGridViewDataPreview.DoDragDrop(dataGridViewDataPreview.Rows[rowIndexFromMouseDown], DragDropEffects.Copy);
        ////            }
        ////        }
        ////    }
        ////    catch
        ////    { }

        ////}

        /////// <summary>
        /////// This method is used for coping row.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_MouseDown(object sender, MouseEventArgs e)
        ////{
        ////    try
        ////    {

        ////        // Get the index of the item the mouse is below.
        ////        rowIndexFromMouseDown = dataGridViewDataPreview.HitTest(e.X, e.Y).RowIndex;
        ////        ColumnIndexFromMouseDown = dataGridViewDataPreview.HitTest(e.X, e.Y).ColumnIndex;
        ////        if (rowIndexFromMouseDown != -1)
        ////        {
        ////            // Remember the point where the mouse down occurred. 
        ////            // The DragSize indicates the size that the mouse can move 
        ////            // before a drag event should be started.                
        ////            Size dragSize = SystemInformation.DragSize;
        ////            // Create a rectangle using the DragSize, with the mouse position being
        ////            // at the center of the rectangle.
        ////            dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
        ////        }
        ////        else
        ////            // Reset the rectangle if the mouse is not over an item in the ListBox.
        ////            dragBoxFromMouseDown = Rectangle.Empty;
        ////    }
        ////    catch
        ////    { }
        ////}

        /////// <summary>
        /////// This method is used for drag over.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_DragOver(object sender, DragEventArgs e)
        ////{
        ////    e.Effect = DragDropEffects.Copy;

        ////}

        /////// <summary>
        /////// this method is used for draging row or fields and paste it on dropping place.
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////private void dataGridViewDataPreview_DragDrop(object sender, DragEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        try
        ////        {
        ////            if (this.TestCount == 1)
        ////            {
        ////                //Display message when mapping is not created or selected
        ////                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////                this.Close();
        ////                return;
        ////            }
        ////            // The mouse locations are relative to the screen, so they must be
        ////            // converted to client coordinates.
        ////            Point clientPoint = dataGridViewDataPreview.PointToClient(new Point(e.X, e.Y));
        ////            // Get the row index of the item the mouse is below.
        ////            rowIndexOfItemUnderMouseToDrop = dataGridViewDataPreview.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
        ////            // Get the column index of the item the mouse is below.
        ////            ColumnIndexOfItemUnderMouseToDrop = dataGridViewDataPreview.HitTest(clientPoint.X, clientPoint.Y).ColumnIndex;

        ////            // If the drag operation was a move then remove and insert the row.
        ////            if (e.Effect == DragDropEffects.Copy)
        ////            {
        ////                DataGridViewRow rowToMove = new DataGridViewRow();
        ////                rowToMove = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;

        ////                if (ColumnIndexOfItemUnderMouseToDrop != -1)
        ////                {
        ////                    if (ColumnIndexOfItemUnderMouseToDrop == ColumnIndexFromMouseDown)
        ////                    {
        ////                        string strColumnvalue = rowToMove.Cells[ColumnIndexFromMouseDown].Value.ToString();
        ////                        if (strColumnvalue != string.Empty)
        ////                        {
        ////                            int showrowindex = 0;
        ////                            int showcolumnindex = 0;
        ////                            if (rowIndexOfItemUnderMouseToDrop < this.ModifiedData.Rows.Count - 1)
        ////                            {
        ////                                this.ModifiedData.Rows[rowIndexOfItemUnderMouseToDrop][ColumnIndexOfItemUnderMouseToDrop] = strColumnvalue;
        ////                            }
        ////                            else
        ////                            {
        ////                                DataRow drr = ((DataRowView)rowToMove.DataBoundItem).Row;
        ////                                this.ModifiedData.ImportRow(drr);
        ////                                for (int iCounter = 0; iCounter < this.ModifiedData.Columns.Count; iCounter++)
        ////                                {
        ////                                    if (ColumnIndexOfItemUnderMouseToDrop == iCounter)
        ////                                    {
        ////                                        this.ModifiedData.Rows[rowIndexOfItemUnderMouseToDrop][iCounter] = strColumnvalue;
        ////                                        showrowindex = rowIndexOfItemUnderMouseToDrop;
        ////                                        showcolumnindex = iCounter;

        ////                                    }
        ////                                    else
        ////                                    {
        ////                                        try
        ////                                        {
        ////                                            this.ModifiedData.Columns[iCounter].AllowDBNull = true;
        ////                                            this.ModifiedData.Rows[rowIndexOfItemUnderMouseToDrop][iCounter] = null;
        ////                                        }
        ////                                        catch
        ////                                        {

        ////                                        }
        ////                                    }
        ////                                }

        ////                            }
        ////                            dataGridViewDataPreview.DataSource = this.ModifiedData;
        ////                            dataGridViewDataPreview.Refresh();
        ////                            this.dataGridViewDataPreview.Rows[showrowindex].Cells[showcolumnindex].Selected = true;
        ////                            this.m_ModifiedData.AcceptChanges();
        ////                            this.NewCount = 1;

        ////                        }
        ////                    }
        ////                }
        ////                else
        ////                {
        ////                    if (rowToMove.DataBoundItem != null)
        ////                    {
        ////                        DataRow drr = ((DataRowView)rowToMove.DataBoundItem).Row;
        ////                        if (drr != null)
        ////                        {
        ////                            this.ModifiedData.ImportRow(drr);
        ////                        }
        ////                        dataGridViewDataPreview.DataSource = this.ModifiedData;
        ////                        dataGridViewDataPreview.Refresh();
        ////                        this.m_ModifiedData.AcceptChanges();
        ////                        this.NewCount = 1;
        ////                    }
        ////                }


        ////            }

        ////        }
        ////        catch
        ////        {
        ////            //MessageBox.Show(ex.Message.ToString());
        ////        }
        ////    }
        ////}

        ////private void dataGridViewDataPreview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        if (this.TestCount == 1)
        ////        {
        ////            //Display message when mapping is not created or selected
        ////            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////            this.Close();
        ////            return;
        ////        }
        ////    }
        ////}

        ////private void dataGridViewDataPreview_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        if (this.TestCount == 1)
        ////        {
        ////            //Display message when mapping is not created or selected
        ////            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////            this.Close();
        ////            return;
        ////        }
        ////    }
        ////}

        ////private void dataGridViewDataPreview_CellContentClick(object sender, DataGridViewCellEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        if (this.TestCount == 1)
        ////        {
        ////            //Display message when mapping is not created or selected
        ////            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////            this.Close();
        ////            return;
        ////        }
        ////    }
        ////}

        ////private void dataGridViewDataPreview_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        ////{
        ////    if (dataGridViewDataPreview.ReadOnly == false)
        ////    {
        ////        if (this.TestCount == 1)
        ////        {
        ////            //Display message when mapping is not created or selected
        ////            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG038"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        ////            this.Close();
        ////            return;
        ////        }
        ////    }
        ////}

        //////private void DataPreview_FormClosing(object sender, FormClosingEventArgs e)
        //////{
        //////    try
        //////    {
        //////        if (this.TestCount == 1)
        //////        {

        //////            //Asssing datatable to null.
        //////            this.m_ModifiedData = null;
        //////            //Calling PopulateDataToPreview method for display data.
        //////            this.PopulateDataToPreview();
        //////        }
        //////        else
        //////        {
        //////            //For update changes
        //////            this.m_ModifiedData.AcceptChanges();
        //////            //Assinging datatabel to property.
        //////            this.ModifiedData = this.m_ModifiedData;
        //////            this.dataGridViewDataPreview.Update();
        //////            this.dataGridViewDataPreview.Refresh();
        //////        }
        //////    }
        //////    catch
        //////    {
        //////        return;
        //////    }
        //////}
		
        //// private void dataGridViewDataPreview_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        ////{
        ////    if (e.RowIndex >=0 && e.ColumnIndex >=0 && e.Button == MouseButtons.Right)
        ////    {
        ////       dataGridViewDataPreview.Rows[e.RowIndex].Selected = true;
        ////       Rectangle r = dataGridViewDataPreview.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);

        ////       contextMenuStripDeleteRows.Show((Control)sender, r.Left + e.X, r.Top + e.Y);
        ////    }
        ////}

        // /// <summary>
        // ///  This method is used to add the table name from the dataset to the combobox .
        // /// </summary>
        
        // public bool PopulateXmlDatatoPreview()
        // {
        //     if (m_xmlDataSet != null)
        //     {
        //         this.buttonReload.Enabled = true;
        //         comboBoxXmlPreviewTable.Items.Clear();
        //         for (int index = 0; index < m_xmlDataSet.Tables.Count; index++)
        //         {
        //             comboBoxXmlPreviewTable.Items.Add(m_xmlDataSet.Tables[index].TableName);
        //         }
        //         comboBoxXmlPreviewTable.SelectedIndex = 0;

        //         return true;
        //     }
        //     return false;
        // }

        
        ///// <summary>
        ///// This event is used to show the preview of the table as per the selected tablename from the combobox.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void comboBoxXmlPreviewTable_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //        this.bindingSourcePreviewData.DataSource = this.m_xmlDataSet.Tables[comboBoxXmlPreviewTable.SelectedItem.ToString()];

        //        this.dataGridViewDataPreview.DataSource = this.bindingSourcePreviewData.DataSource;

        //        //Enabling the datagridtable for the file except the xml.
        //        dataGridViewDataPreview.ReadOnly = false;
        //        comboBoxXmlPreviewTable.Visible = true;
        //        //buttonReload.Enabled = false;
        //        dataGridViewDataPreview.AllowUserToDeleteRows = false;
         
        //}




        ///// <summary>
        ///// This is used to show the preview.
        ///// </summary>
        ////public void XmlPreview(string fileName)
        ////{
        ////    DataPreview dataPreviewForm = DataPreview.GetInstance();
        ////    DataSet ds = new DataSet();
        ////    DataSet dsXml = new DataSet();
        ////    DataTable dt = new DataTable();

        ////    //To check whether the file is Valid.
        ////    System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
        ////    try
        ////    {
        ////        requestXmlDoc.Load(fileName);
        ////    }
        ////    catch (XmlException)
        ////    {
        ////        MessageBox.Show("Error while loading the file.Please check the xml.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////        return;
        ////    }

        ////    try
        ////    {
        ////        if (requestXmlDoc.ChildNodes[2].Name.ToLower() != "qbxml")
        ////        {
        ////            MessageBox.Show(Constants.InvalidXmlFileMsg, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////            return;
        ////        }
        ////        else if (requestXmlDoc.ChildNodes[2].ChildNodes[0].Name.ToLower() != "qbxmlmsgsrq")
        ////        {
        ////            MessageBox.Show(Constants.InvalidXmlFileMsg, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////            return;
        ////        }
        ////    }
        ////    catch (NullReferenceException)
        ////    {
        ////        MessageBox.Show(Constants.InvalidXmlFileMsg, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////        return;
        ////    }
        ////    try
        ////    {
        ////        //Reads xml schema into the dataset.
        ////        ds.ReadXml(fileName);
        ////    }
        ////    catch (System.Security.SecurityException)
        ////    {
        ////        MessageBox.Show(Constants.SecurityExceptionMsg, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        ////        return;
        ////    }

            
        ////    if(CommonUtilities.GetInstance().SelectedMappingName.Equals(string.Empty))
        ////    {
        ////        #region Commented code If there is no mapping selected for browse xml 
        ////        //dataPreviewForm.XmlDataSet = ds;

        ////        //if (dataPreviewForm.PopulateXmlDatatoPreview())//Show Preview box.
        ////        //{
                    
        ////        //    dataPreviewForm.ShowDialog();
        ////        //}
        ////        #endregion

        ////        #region New code If there is no mapping selected for browse xml file
                
        ////        comboBoxXmlPreviewTable.Visible = false;

        ////        dataGridViewDataPreview.DataSource = GetDataSourceForBrowseFilePreview();

        ////        dataPreviewForm.ShowDialog();
        ////        #endregion



        ////    }
        ////    else if (!CommonUtilities.GetInstance().SelectedMappingName.Equals("<Add New Mappings") && !CommonUtilities.GetInstance().SelectedMappingName.Equals("<Edit Mappings>"))
        ////    {
        ////        #region Newly added code If there is valid mapping is selected for browse xml file.

        ////        //Assign dataset
        ////        comboBoxXmlPreviewTable.Visible = false;

        ////        dataGridViewDataPreview.DataSource = GetDataSourceForXmlPreview();

        ////        dataPreviewForm.ShowDialog();

        ////        #endregion

        ////        #region Commented code If there is valid mapping is selected for browse xml file

        ////        //string xmlfileName = getMappedXml(CommonUtilities.GetInstance().SelectedMappingName.ToString());

        ////        //try
        ////        //{
        ////        //    dsXml.ReadXml(xmlfileName);
        ////        //}
        ////        //catch { }

        ////        //dataPreviewForm.XmlDataSet = dsXml;

        ////        //try
        ////        //{
        ////        //    File.Delete(xmlfileName);
        ////        //}
        ////        //catch { }

        ////        //if (dataPreviewForm.PopulateXmlDatatoPreview())//Show Preview box.
        ////        //{
                    
        ////        //    dataPreviewForm.ShowDialog();
                     
        ////        //}
        ////        #endregion
        ////    }

        ////    //dispose datapreview form object
        ////    dataPreviewForm.Dispose();
            
        ////}

        ///// <summary>
        ///// method returns data table for browse file and no mapping is selected
        ///// </summary>
        ///// <returns></returns>
        ////private DataTable GetDataSourceForBrowseFilePreview()
        ////{
        ////    Dictionary<string, string> htBrowseFilePreview = new Dictionary<string, string>();

        ////    htBrowseFilePreview = GetElementValue();

        ////    dt = new DataTable();

        ////    //Add Headers
        ////    foreach (string columnHeader in htBrowseFilePreview.Keys)
        ////    {
        ////        dt.Columns.Add(columnHeader);
        ////    }

        ////    object[] dataPreview = new object[htBrowseFilePreview.Count];

        ////    int pos = 0;
        ////    foreach (string data in htBrowseFilePreview.Keys)
        ////    {
        ////        dataPreview[pos] = htBrowseFilePreview[data];
        ////        pos++;
        ////    }

        ////    dt.LoadDataRow(dataPreview, true);

        ////    return dt;
        ////}

        ////private DataTable GetDataSourceForXmlPreview()
        ////{
        ////    //get XSL file of selected mappings
        ////    string xslPath = CommonUtilities.GetInstance().getXslFile();


        ////    #region Get OS Related Path
            
        ////    try
        ////    {
        ////        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        ////            xslPath = xslPath.Replace("Program Files", Constants.xpPath);
        ////        else
        ////            xslPath = xslPath.Replace("Program Files", "Users\\Public\\Documents");
        ////    }
        ////    catch { }

        ////    #endregion

        ////    #region Load xsl file
        ////    System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();

        ////    try
        ////    {
        ////        requestXmlDoc.Load(xslPath);
        ////    }
        ////    catch (XmlException)
        ////    {
        ////        MessageBox.Show("Error while loading the file.Please check the xml.");
        ////    }
        ////    #endregion


        ////    #region For getting node List for user and constant mappings
        ////    XmlNode root = (XmlNode)requestXmlDoc.DocumentElement;


        ////    //for User Mappings
        ////    XmlNodeList nodeListUser = root.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes;


        ////    //For Constant Mapping
        ////    XmlNodeList nodeListConstant = root.ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes;
        ////    #endregion


        ////    #region For creating DataTable column header
        ////    //To add only those column for which mapping was set

        ////    //create Dictionary which contain key as User Mapping name and Value as Constant Mapping name

        ////    Dictionary<string, string> htColumn = new Dictionary<string, string>();

        ////    htColumn = getColumHeaderName(nodeListUser, nodeListConstant);

        ////    dt = new DataTable();

        ////    foreach (string columnHeader in htColumn.Keys)
        ////    {
        ////        dt.Columns.Add(columnHeader);
        ////    }
        ////    #endregion

        ////    #region Variable declaration
        ////    int cnt1 = 0;
        ////    int cnt2 = 0;
        ////    string dataUser = string.Empty;
        ////    #endregion


        ////    #region For User Mapping Create array

        ////    //for User Mapping
        ////    foreach (XmlNode node in nodeListUser)
        ////    {
        ////        if (cnt1 > 2)
        ////        {
        ////            dataUser += node.InnerText + "~";
        ////        }
        ////        cnt1++;
        ////    }

        ////    object[] dataUserArray = Regex.Split(dataUser.Substring(0, dataUser.Length - 1), "~");

        ////    //for user Mapping I need to change value to xml data present in xml file

        ////    Dictionary<string, string> ht = new Dictionary<string, string>();
        ////    ht = GetElementValue();

        ////    for (int j = 0; j < dataUserArray.Length; j++)
        ////    {
        ////        if (dataUserArray[j].ToString() != string.Empty)
        ////            dataUserArray[j] = ht[dataUserArray[j].ToString()];
        ////    }
        ////    #endregion

        ////    #region For Constant mapping Create array
        ////    int pos = 0;
        ////    foreach (XmlNode node in nodeListConstant)
        ////    {
        ////        if (cnt2 > 2)
        ////        {
        ////            if (node.InnerText != string.Empty)
        ////                dataUserArray[pos] = node.InnerText;
        ////            pos++;
        ////        }
        ////        cnt2++;
        ////    }
        ////    #endregion

        ////    #region Get count for creating final obhject array
        ////    int limit = 0;
        ////    for (int count = 0; count < dataUserArray.Length; count++)
        ////    {
        ////        if (dataUserArray[count].ToString() != string.Empty)
        ////        {
        ////            limit++;
        ////        }
        ////    }
        ////    #endregion

        ////    #region create Final array of object which contain actual data(user and constant mapped data)
        ////    object[] finalData = new object[limit];

        ////    int counter = 0;
        ////    for (int count = 0; count < dataUserArray.Length; count++)
        ////    {
        ////        if (dataUserArray[count].ToString() != string.Empty)
        ////        {
        ////            finalData[counter] = dataUserArray[count];
        ////            counter++;
        ////        }
        ////    }
        ////    #endregion

        ////    dt.LoadDataRow(finalData, true);

        ////    return dt;


        ////}

        ///// <summary>
        ///// Returns Hash table for mapped fieds
        ///// </summary>
        ///// <returns></returns>
        ////private Dictionary<string,string> GetElementValue()
        ////{
        ////    //get file Name of Browse xml file
        ////    string xmlPath = CommonUtilities.GetInstance().BrowseFileName;


        ////    #region Get OS Related Path

        ////    try
        ////    {
        ////        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        ////            xmlPath = xmlPath.Replace("Program Files", Constants.xpPath);
        ////        else
        ////            xmlPath = xmlPath.Replace("Program Files", "Users\\Public\\Documents");
        ////    }
        ////    catch { }

        ////    #endregion

        ////    Dictionary<string, string> columnNames = new Dictionary<string, string>();

        ////    Hashtable list = new Hashtable();

        ////    try
        ////    {
        ////        XmlDocument xmlDoc = new XmlDocument();
        ////        xmlDoc.Load(xmlPath);

        ////        xmllist = new Hashtable();

        ////        XmlNode root = (XmlNode)xmlDoc.DocumentElement;

        ////        XmlNodeList nodeList = root.ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes;

        ////        string nodeName = string.Empty;

        ////        foreach (XmlNode node in nodeList)
        ////        {
        ////            list.Clear();

        ////            list = getXmlElements(node);
        ////            foreach (string xmlData in list.Keys)
        ////            {
        ////                columnNames.Add(xmlData, list[xmlData].ToString());
        ////            }
        ////        }

        ////    }
        ////    catch (XmlException)
        ////    {
        ////        MessageBox.Show("Error while loading the file.Please check the xml.");
        ////    }
        ////    return columnNames;
        ////}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="node"></param>
        ///// <returns></returns>
        ////private Hashtable getXmlElements(XmlNode node)
        ////{
        ////    if (node.ChildNodes.Count > 0 && !node.FirstChild.Name.ToString().Equals("#text"))
        ////    {

        ////        string nodeName = ParentTag + node.Name;
        ////        for (int i = 0; i < node.ChildNodes.Count; i++)
        ////        {
        ////            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.ToString().Equals("#text"))
        ////            {
        ////                ParentTag = node.Name;
        ////                xmllist = getXmlElements(node.ChildNodes[i]);
        ////            }
        ////            else
        ////            {
        ////                ParentTag = string.Empty;
        ////                xmllist.Add(nodeName + node.ChildNodes[i].Name, node.ChildNodes[i].InnerText);
        ////            }
        ////        }
        ////    }
        ////    else
        ////        xmllist.Add(node.Name, node.InnerText);
        ////    return xmllist;
        ////}

        ///// <summary>
        ///// Return Column headers for data table 
        ///// </summary>
        ///// <param name="nodeListUser"></param>
        ///// <param name="nodeListConstant"></param>
        ///// <returns></returns>
        ////private Dictionary<string, string> getColumHeaderName(XmlNodeList nodeListUser, XmlNodeList nodeListConstant)
        ////{
        ////    int cnt = 0;
        ////    int cnt1 = 0;

        ////    string dataUserMapp = string.Empty;

        ////    string dataConstMapp = string.Empty;

        ////    Dictionary<string, string> htTempDictionary = new Dictionary<string, string>();

        ////    #region for creating array of keys that are used in dictionary
        ////    foreach (XmlNode node in nodeListUser)
        ////    {
        ////        if (cnt > 2)
        ////        {
        ////            dataUserMapp += node.Attributes.GetNamedItem("name").Value + "~";
        ////        }
        ////        cnt++;
        ////    }

        ////    object[] dataUserArray = Regex.Split(dataUserMapp.Substring(0, dataUserMapp.Length - 1), "~");

        ////    #endregion

        ////    #region for creating array of values that are used in dictionary
        ////    foreach (XmlNode node in nodeListConstant)
        ////    {
        ////        if (cnt1 > 2)
        ////        {
        ////            dataConstMapp += node.Attributes.GetNamedItem("name").Value + "~";
        ////        }
        ////        cnt1++;
        ////    }

        ////    object[] dataConstantArray = Regex.Split(dataConstMapp.Substring(0, dataConstMapp.Length - 1), "~");
        ////    #endregion

        ////    #region Add it to temporary Dictionary (User Mapping as Key and Constant Mapping as Values)

        ////    for (int i = 0; i < dataUserArray.Length; i++)
        ////    {
        ////        htTempDictionary.Add(dataUserArray[i].ToString(), dataConstantArray[i].ToString());
        ////    }
        ////    #endregion

        ////    #region for making hashtable(key/value pair) for created mapping
        ////    Hashtable mappedList = new Hashtable();

        ////    cnt = 0;
        ////    cnt1 = 0;
        ////    string key = string.Empty;
        ////    foreach (XmlNode node in nodeListUser)
        ////    {
        ////        if (cnt > 2)
        ////        {
        ////            if (node.InnerText.ToString() != string.Empty)
        ////                mappedList.Add(node.Attributes.GetNamedItem("name").Value, htTempDictionary[node.Attributes.GetNamedItem("name").Value]);
        ////        }
        ////        cnt++;
        ////    }
        ////    foreach (XmlNode node in nodeListConstant)
        ////    {
        ////        if (cnt1 > 2)
        ////        {
        ////            if (node.InnerText.ToString() != string.Empty)
        ////            {
        ////                foreach (string s in htTempDictionary.Keys)
        ////                {
        ////                    if (htTempDictionary[s].Equals(node.Attributes.GetNamedItem("name").Value))
        ////                    {
        ////                        key = s;
        ////                        break;
        ////                    }

        ////                }

        ////                mappedList.Add(key, node.Attributes.GetNamedItem("name").Value);
        ////            }
        ////        }
        ////        cnt1++;
        ////    }
        ////    #endregion

        ////    #region Creating key/value pair in proper order

        ////    headerDictionary = new Dictionary<string, string>();

        ////    foreach (string keyDictionary in htTempDictionary.Keys)
        ////    {
        ////        foreach (string keydata in mappedList.Keys)
        ////        {
        ////            if (keyDictionary.Equals(keydata))
        ////            {
        ////                headerDictionary.Add(keyDictionary, htTempDictionary[keydata]);
        ////                break;
        ////            }

        ////        }
        ////    }
        ////    #endregion

        ////    return headerDictionary;
        ////}


        ///// <summary>
        /////Get new genarated Mapped qbxml file for creating FinalPreview
        ///// </summary>
        ///// <param name="fileName"></param>
        ///// <returns></returns>

        //public string getMappedXmlForPreview(string selectedMapping,DataTable dt)
        //{

        //    Hashtable htFinalMapped = new Hashtable();

        //    for (int i = 0; i < dt.Rows[0].Table.Columns.Count; i++)
        //    {
        //        htFinalMapped.Add(headerDictionary[dt.Rows[0].Table.Columns[i].ToString()], dt.Rows[0].ItemArray[i].ToString());
        //    }

        //    importTypeName = string.Empty;

        //    System.Xml.XmlDocument xdoc = new XmlDocument();

        //    try
        //    {
        //        xdoc.Load(CommonUtilities.GetInstance().BrowseFileName);
        //    }
        //    catch (XmlException) { }

        //    string importType = string.Empty;
        //    try
        //    {
        //        importType = xdoc.ChildNodes[2].ChildNodes[0].ChildNodes[0].ChildNodes[0].Name;
        //        importTypeName = importType.Substring(0, importType.Length - 3);
        //    }
        //    catch { }

            
        //    string fileName = string.Empty;

        //    fileName = Application.StartupPath + "\\" + "finalPreview" + ".xml"; ;

        //    try
        //    {
        //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            fileName = fileName.Replace("Program Files", Constants.xpPath);
        //        else
        //            fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
        //    }
        //    catch { }


        //    //Create an instance of xml document.
        //    System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();

        //    //Loading xml file into Xml document.
        //    //requestXmlDoc.Load(fileName);

        //    requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
        //    requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

        //    //Create the outer request envelope tag
        //    System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
        //    requestXmlDoc.AppendChild(outer);

        //    //Create the inner request envelope & any needed attributes
        //    System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
        //    outer.AppendChild(inner);
        //    inner.SetAttribute("onError", "stopOnError");

        //    //Create JournalEntryAddRq aggregate and fill in field values for it
        //    System.Xml.XmlElement InvoiceAddRq = requestXmlDoc.CreateElement(importTypeName + "AddRq");
        //    inner.AppendChild(InvoiceAddRq);

        //    //Create JournalEntryAdd aggregate and fill in field values for it
        //    System.Xml.XmlElement InvoiceAdd = requestXmlDoc.CreateElement(importTypeName + "Add");

        //    InvoiceAddRq.AppendChild(InvoiceAdd);


        //    XmlNode root = (XmlNode)xdoc.DocumentElement;
        //    XmlNode node = root.ChildNodes[0].ChildNodes[0].ChildNodes[0];


        //    requestXmlDoc.Save(fileName);

        //    List<string> coll = new List<string>();
        //    string startwith = string.Empty;

        //    getXmlElements(node, htFinalMapped, InvoiceAdd, requestXmlDoc, coll, startwith, fileName, importType);

        //    return fileName;

        //}



        ///// <summary>
        /////Get new genarated Mapped qbxml file for import
        ///// </summary>
        ///// <param name="fileName"></param>
        ///// <returns></returns>

        //public string getMappedXml(string selectedMapping)
        //{

        //    //get xsl file 
        //    string xslPath = CommonUtilities.GetInstance().getXslFile();

        //    //get OSRelated path for xsl
        //    //CommonUtilities.GetInstance().osRelatedPath(xslPath);
        //    try
        //    {
        //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            xslPath = xslPath.Replace("Program Files", Constants.xpPath);
        //        else
        //            xslPath = xslPath.Replace("Program Files", "Users\\Public\\Documents");
        //    }
        //    catch { }


        //    //xml filepath
        //    string xmlPath = Application.StartupPath + "\\" + "Settings.xml";

        //    //get OSRelated path for xml
        //    //CommonUtilities.GetInstance().osRelatedPath(xmlPath);
        //    try
        //    {
        //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            xmlPath = xmlPath.Replace("Program Files", Constants.xpPath);
        //        else
        //            xmlPath = xmlPath.Replace("Program Files", "Users\\Public\\Documents");
        //    }
        //    catch { }

        //    //file name for transform xml file
        //    string outputFile = CommonUtilities.GetInstance().XslTransformXmlFileName;
            
        //    //get OSRelated path for output filr
        //    //CommonUtilities.GetInstance().osRelatedPath(outputFile);
        //    try
        //    {
        //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            outputFile = outputFile.Replace("Program Files", Constants.xpPath);
        //        else
        //            outputFile = outputFile.Replace("Program Files", "Users\\Public\\Documents");
        //    }
        //    catch { }

        //    //get XSL Transformed xml file
        //    string transformXml = CommonUtilities.GetInstance().getTransformXml(xmlPath, xslPath, outputFile);

        //    //get OSRealated path for xsl transform file
        //    try
        //    {
        //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            transformXml = transformXml.Replace("Program Files", Constants.xpPath);
        //        else
        //            transformXml = transformXml.Replace("Program Files", "Users\\Public\\Documents");
        //    }
        //    catch { }
            

        //    Hashtable ht = new Hashtable();
        //    ht = getMappedList(transformXml);

        //    importTypeName = string.Empty;

        //    System.Xml.XmlDocument xdoc = new XmlDocument();
        //    try
        //    {
        //        xdoc.Load(CommonUtilities.GetInstance().BrowseFileName);
        //    }
        //    catch (XmlException) { }

        //    string importType = string.Empty;
        //    try
        //    {
        //        importType = xdoc.ChildNodes[2].ChildNodes[0].ChildNodes[0].ChildNodes[0].Name;
        //        importTypeName = importType.Substring(0, importType.Length - 3);
        //    }
        //    catch { }

        //    //importTypeName = importType.Substring(0, importType.Length - 3);



        //    string fileName = string.Empty;

        //    fileName = Application.StartupPath;
        //    fileName += System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";

        //    try
        //    {
        //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
        //            fileName = fileName.Replace("Program Files", Constants.xpPath);
        //        else
        //            fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
        //    }
        //    catch { }


        //        //Create an instance of xml document.
        //        System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();

        //        //Loading xml file into Xml document.
        //        //requestXmlDoc.Load(fileName);

        //        requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
        //        requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

        //        //Create the outer request envelope tag
        //        System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
        //        requestXmlDoc.AppendChild(outer);

        //        //Create the inner request envelope & any needed attributes
        //        System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
        //        outer.AppendChild(inner);
        //        inner.SetAttribute("onError", "stopOnError");

        //        //Create JournalEntryAddRq aggregate and fill in field values for it
        //        System.Xml.XmlElement InvoiceAddRq = requestXmlDoc.CreateElement(importTypeName + "AddRq");
        //        inner.AppendChild(InvoiceAddRq);

        //        //Create JournalEntryAdd aggregate and fill in field values for it
        //        System.Xml.XmlElement InvoiceAdd = requestXmlDoc.CreateElement(importTypeName + "Add");

        //        InvoiceAddRq.AppendChild(InvoiceAdd);


        //        XmlNode root = (XmlNode)xdoc.DocumentElement;
        //        XmlNode node = root.ChildNodes[0].ChildNodes[0].ChildNodes[0];


        //        requestXmlDoc.Save(fileName);

        //        List<string> coll = new List<string>();
        //        string startwith = string.Empty;

        //        getXmlElements(node, ht, InvoiceAdd, requestXmlDoc, coll, startwith, fileName, importType);

        //    return fileName;

        //}

        //#region for commented of xml file for user
        ///// <summary>
        ///// generate xml file for user and constant mapping
        ///// </summary>
        ///// <param name="node"></param>
        ///// <param name="mapList"></param>
        ///// <param name="parentNode1"></param>
        ///// <param name="requestXmlDoc"></param>
        ///// <param name="coll"></param>
        ///// <param name="startwith"></param>
        ////private void getXmlElements(XmlNode node, Hashtable mapList, XmlElement parentNode1, XmlDocument requestXmlDoc, List<string> coll, string startwith, string fileName)
        ////{
        ////    for (int i = 0; i < node.ChildNodes.Count; i++)
        ////    {
        ////        foreach (DictionaryEntry ar in mapList)
        ////        {


        ////            if (ar.Key.ToString().Contains(node.ChildNodes[i].Name))
        ////            {
        ////                if (ar.Key.ToString().StartsWith(node.ChildNodes[i].Name))
        ////                {
        ////                    if (!coll.Contains(node.ChildNodes[i].Name))
        ////                    {

        ////                        coll.Add(node.ChildNodes[i].Name);
        ////                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            parentNode1.AppendChild(parentNode);
        ////                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                            {
        ////                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, node.ChildNodes[i].Name, fileName);
        ////                            }
        ////                        }
        ////                        else
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);

        ////                            if (ar.Value != null)

        ////                                parentNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;

        ////                            else

        ////                                parentNode.InnerText = node.ChildNodes[i].InnerText;


        ////                            parentNode1.AppendChild(parentNode);
        ////                        }
        ////                    }
        ////                }
        ////                else if (ar.Key.ToString().EndsWith(node.ChildNodes[i].Name) && ar.Key.ToString().StartsWith(startwith))
        ////                {
        ////                    System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                    if (ar.Value != null)
        ////                        childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        ////                    else
        ////                        childNode.InnerText = node.ChildNodes[i].InnerText;

        ////                    parentNode1.AppendChild(childNode);
        ////                }
        ////                else if (ar.Key.ToString().Contains(node.ChildNodes[i].Name) && ar.Key.ToString().StartsWith(startwith) && !startwith.Equals(string.Empty))
        ////                {
        ////                    coll.Add(node.ChildNodes[i].Name);

        ////                    System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                    parentNode1.AppendChild(parentNode);

        ////                    if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                    {
        ////                        getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, startwith, fileName);
        ////                    }
        ////                }

        ////            }
        ////        }
        ////    }
        ////    requestXmlDoc.Save(fileName);
        ////}


        ////private void getXmlElements(XmlNode node, Hashtable mapList, XmlElement parentNode1, XmlDocument requestXmlDoc, List<string> coll, string startwith, string fileName,string baseParentTag)
        ////{
        ////    for (int i = 0; i < node.ChildNodes.Count; i++)
        ////    {
        ////        foreach (DictionaryEntry ar in mapList)
        ////        {
        ////            if (ar.Key.ToString().Contains(node.ChildNodes[i].Name))
        ////            {
        ////                if (ar.Key.ToString().StartsWith(node.ChildNodes[i].Name))
        ////                {
        ////                    if (!coll.Contains(node.ChildNodes[i].Name))
        ////                    {

        ////                        coll.Add(node.ChildNodes[i].Name);
        ////                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            parentNode1.AppendChild(parentNode);
        ////                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                            {
        ////                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, node.ChildNodes[i].Name, fileName,baseParentTag);
        ////                            }
        ////                        }
        ////                        else
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);

        ////                            if (ar.Value != null)

        ////                                parentNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;

        ////                            else

        ////                                parentNode.InnerText = node.ChildNodes[i].InnerText;


        ////                            parentNode1.AppendChild(parentNode);
        ////                        }
        ////                    }
        ////                }
        ////                else if (ar.Key.ToString().EndsWith(node.ChildNodes[i].Name) && ar.Key.ToString().Contains(parentNode1.Name) )
        ////                {
        ////                    if (parentNode1.ParentNode.Name.Equals(baseParentTag) )
        ////                    {
        ////                        System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                        if (ar.Value != null)
        ////                            childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        ////                        else
        ////                            childNode.InnerText = node.ChildNodes[i].InnerText;

        ////                        parentNode1.AppendChild(childNode);
        ////                    }
        ////                    else if(ar.Key.ToString().Contains(parentNode1.ParentNode.Name))
        ////                    {
        ////                        if (ar.Key.ToString().Contains(parentNode1.ParentNode.Name))
        ////                        {
        ////                            System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            if (ar.Value != null)
        ////                                childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        ////                            else
        ////                                childNode.InnerText = node.ChildNodes[i].InnerText;

        ////                            parentNode1.AppendChild(childNode);

        ////                        }
        ////                    }
        ////                }
        ////                else if (ar.Key.ToString().Contains(node.ChildNodes[i].Name) && ar.Key.ToString().StartsWith(startwith) && !startwith.Equals(string.Empty))
        ////                {
        ////                    if (!coll.Contains(node.ChildNodes[i].Name))
        ////                    {
        ////                        coll.Add(node.ChildNodes[i].Name);
        ////                        //for emp file

        ////                        System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                        parentNode1.AppendChild(parentNode);

        ////                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                        {
        ////                            getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, node.ChildNodes[i].Name, fileName, baseParentTag);
        ////                        }
        ////                    }
        ////                    
        ////                }

        ////            }
        ////        }
        ////    }
        ////    requestXmlDoc.Save(fileName);
        ////}

        ////latest
        ////private void getXmlElements(XmlNode node, Hashtable mapList, XmlElement parentNode1, XmlDocument requestXmlDoc, List<string> coll, string startwith, string fileName, string baseParentTag)
        ////{
        ////    for (int i = 0; i < node.ChildNodes.Count; i++)
        ////    {
        ////        foreach (DictionaryEntry ar in mapList)
        ////        {
        ////            if (ar.Key.ToString().Contains(node.ChildNodes[i].Name))
        ////            {
        ////                if (ar.Key.ToString().StartsWith(node.ChildNodes[i].Name))
        ////                {
        ////                    if (!coll.Contains(node.ChildNodes[i].Name))
        ////                    {

        ////                        coll.Add(node.ChildNodes[i].Name);
        ////                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            parentNode1.AppendChild(parentNode);
        ////                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                            {
        ////                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, node.ChildNodes[i].Name, fileName, baseParentTag);
        ////                            }
        ////                        }
        ////                        else
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);

        ////                            if (ar.Value != null)

        ////                                parentNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;

        ////                            else

        ////                                parentNode.InnerText = node.ChildNodes[i].InnerText;


        ////                            parentNode1.AppendChild(parentNode);
        ////                        }
        ////                    }
        ////                }
        ////                else if (ar.Key.ToString().EndsWith(node.ChildNodes[i].Name) && ar.Key.ToString().Contains(parentNode1.Name))
        ////                {
        ////                    //for upper level xml tag whose parent node is base parent tag
        ////                    if (parentNode1.ParentNode.Name.Equals(baseParentTag) && ar.Key.ToString().StartsWith(parentNode1.Name))
        ////                    {
        ////                        System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                        if (ar.Value != null)
        ////                            childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        ////                        else
        ////                            childNode.InnerText = node.ChildNodes[i].InnerText;

        ////                        parentNode1.AppendChild(childNode);
        ////                    }
        ////                    else if (ar.Key.ToString().Contains(parentNode1.ParentNode.Name))
        ////                    {
        ////                        //for xml tag which is not upper level(for xml subtags)
        ////                        //if (ar.Key.ToString().Contains(parentNode1.ParentNode.Name))
        ////                        //{
        ////                            System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            if (ar.Value != null)
        ////                                childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        ////                            else
        ////                                childNode.InnerText = node.ChildNodes[i].InnerText;

        ////                            parentNode1.AppendChild(childNode);

        ////                        //}
        ////                    }
        ////                }
        ////                else if (ar.Key.ToString().Contains(node.ChildNodes[i].Name) && ar.Key.ToString().StartsWith(startwith) && !startwith.Equals(string.Empty))
        ////                {
        ////                    if (!coll.Contains(node.ChildNodes[i].Name))
        ////                    {
        ////                        coll.Add(node.ChildNodes[i].Name);
        ////                        //for emp file

        ////                        System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                        parentNode1.AppendChild(parentNode);

        ////                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                        {
        ////                            getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, node.ChildNodes[i].Name, fileName, baseParentTag);
        ////                        }
        ////                    }
        ////                    else if (coll.Contains(node.ChildNodes[i].Name) && !coll.IndexOf(node.ChildNodes[i].Name).Equals(coll.Count - 1))
        ////                    {
        ////                        coll.Add(node.ChildNodes[i].Name);
        ////                        //for emp file

        ////                        System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                        parentNode1.AppendChild(parentNode);

        ////                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                        {
        ////                            getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, node.ChildNodes[i].Name, fileName, baseParentTag);
        ////                        }
        ////                    }
        ////                }

        ////            }
        ////        }
        ////    }
        ////    requestXmlDoc.Save(fileName);
        ////}

        ////private void getXmlElements(XmlNode node, Hashtable mapList, XmlElement parentNode1, XmlDocument requestXmlDoc, List<string> coll, string startwith, string fileName, string baseParentTag)
        ////{
        ////    for (int i = 0; i < node.ChildNodes.Count; i++)
        ////    {
        ////        foreach (DictionaryEntry ar in mapList)
        ////        {
        ////            if (ar.Key.ToString().Contains(node.ChildNodes[i].Name))
        ////            {
        ////                if (ar.Key.ToString().StartsWith(node.ChildNodes[i].Name))
        ////                {
        ////                    if (!coll.Contains(node.ChildNodes[i].Name))
        ////                    {

        ////                        coll.Add(node.ChildNodes[i].Name);
        ////                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            parentNode1.AppendChild(parentNode);
        ////                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                            {
        ////                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll,startwith+node.ChildNodes[i].Name, fileName, baseParentTag);
        ////                            }
        ////                            break;
        ////                        }
        ////                        else
        ////                        {
        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);

        ////                            if (ar.Value != null)

        ////                                parentNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;

        ////                            else

        ////                                parentNode.InnerText = node.ChildNodes[i].InnerText;


        ////                            parentNode1.AppendChild(parentNode);
        ////                        }
        ////                    }
        ////                }
        ////                else if (ar.Key.ToString().EndsWith(node.ChildNodes[i].Name) && ar.Key.ToString().Contains(parentNode1.Name))
        ////                {
        ////                    //for upper level xml tag whose parent node is base parent tag
        ////                    if (parentNode1.ParentNode.Name.Equals(baseParentTag) && ar.Key.ToString().StartsWith(parentNode1.Name))
        ////                    {
        ////                        System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                        if (ar.Value != null)
        ////                            childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        ////                        else
        ////                            childNode.InnerText = node.ChildNodes[i].InnerText;

        ////                        parentNode1.AppendChild(childNode);
        ////                    }
        ////                    else if (ar.Key.ToString().Contains(parentNode1.ParentNode.Name))
        ////                    {
        ////                        //for xml tag which is not upper level(for xml subtags)

        ////                        System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                        if (ar.Value != null)
        ////                            childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        ////                        else
        ////                            childNode.InnerText = node.ChildNodes[i].InnerText;

        ////                        parentNode1.AppendChild(childNode);

        ////                    }
        ////                }
        ////                else if (ar.Key.ToString().Contains(node.ChildNodes[i].Name) && ar.Key.ToString().StartsWith(startwith) && !startwith.Equals(string.Empty))
        ////                {
        ////                    if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        ////                    {
        ////                        if (!coll.Contains(node.ChildNodes[i].Name))
        ////                        {
        ////                            coll.Add(node.ChildNodes[i].Name);
        ////                            //for emp file

        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            parentNode1.AppendChild(parentNode);

        ////                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                            {
        ////                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, startwith + node.ChildNodes[i].Name, fileName, baseParentTag);
        ////                            }
        ////                            break;
        ////                        }
        ////                        else if (coll.Contains(node.ChildNodes[i].Name) && !coll.IndexOf(node.ChildNodes[i].Name).Equals(coll.Count - 1))
        ////                        {
        ////                            coll.Add(node.ChildNodes[i].Name);
        ////                            //for emp file

        ////                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        ////                            parentNode1.AppendChild(parentNode);

        ////                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        ////                            {
        ////                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, startwith + node.ChildNodes[i].Name, fileName, baseParentTag);
        ////                            }
        ////                            break;
        ////                        }
        ////                    }
        ////                }
        ////            }
        ////        }
        ////    }
        ////    requestXmlDoc.Save(fileName);
        ////}

        //#endregion

        ///// <summary>
        ///// new changes recusrsive logic to genarate xml file for selected mapping
        ///// </summary>
        ///// <param name="node"></param>
        ///// <param name="mapList"></param>
        ///// <param name="parentNode1"></param>
        ///// <param name="requestXmlDoc"></param>
        ///// <param name="coll"></param>
        ///// <param name="startwith"></param>
        ///// <param name="fileName"></param>
        ///// <param name="baseParentTag"></param>
        //private void getXmlElements(XmlNode node, Hashtable mapList, XmlElement parentNode1, XmlDocument requestXmlDoc, List<string> coll, string startwith, string fileName, string baseParentTag)
        //{
        //    for (int i = 0; i < node.ChildNodes.Count; i++)
        //    {
        //        foreach (DictionaryEntry ar in mapList)
        //        {
        //            if (ar.Key.ToString().Contains(node.ChildNodes[i].Name))
        //            {
        //                if (ar.Key.ToString().StartsWith(node.ChildNodes[i].Name))
        //                {
        //                    if (!coll.Contains(node.ChildNodes[i].Name))
        //                    {

        //                        coll.Add(node.ChildNodes[i].Name);
        //                        if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        //                        {
        //                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        //                            parentNode1.AppendChild(parentNode);
        //                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        //                            {
        //                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, startwith + node.ChildNodes[i].Name, fileName, baseParentTag);
        //                            }
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);

        //                            if (ar.Value != null)

        //                                parentNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;

        //                            else

        //                                parentNode.InnerText = node.ChildNodes[i].InnerText;


        //                            parentNode1.AppendChild(parentNode);
        //                        }
        //                    }
        //                }
        //                else if (ar.Key.ToString().EndsWith(node.ChildNodes[i].Name) && ar.Key.ToString().Contains(parentNode1.Name))
        //                {
                            
        //                        //for upper level xml tag whose parent node is base parent tag
        //                        if (parentNode1.ParentNode.Name.Equals(baseParentTag) && ar.Key.ToString().StartsWith(parentNode1.Name))
        //                        {
        //                            int length = startwith.Length;
                                    
        //                            string endswith = ar.Key.ToString().Substring(length);

        //                            if (endswith.Equals(node.ChildNodes[i].Name))
        //                            {
        //                                System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        //                                if (ar.Value != null)
        //                                    childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        //                                else
        //                                    childNode.InnerText = node.ChildNodes[i].InnerText;

        //                                parentNode1.AppendChild(childNode);
        //                            }
        //                        }
        //                        else if (ar.Key.ToString().Contains(parentNode1.ParentNode.Name))
        //                        {
        //                            //for xml tag which is not upper level(for xml subtags)

        //                            int length = startwith.Length;
                                    
        //                            string endswith = ar.Key.ToString().Substring(length);
                                    
        //                            if (endswith.Equals(node.ChildNodes[i].Name))
        //                            {
        //                                System.Xml.XmlElement childNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        //                                if (ar.Value != null)
        //                                    childNode.InnerText = ar.Value.ToString();//node.ChildNodes[i].InnerText;
        //                                else
        //                                    childNode.InnerText = node.ChildNodes[i].InnerText;

        //                                parentNode1.AppendChild(childNode);
        //                            }

        //                        }
                            
        //                }
        //                else if (ar.Key.ToString().Contains(node.ChildNodes[i].Name) && ar.Key.ToString().StartsWith(startwith) && !startwith.Equals(string.Empty))
        //                {
        //                    if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.Equals("#text"))
        //                    {
        //                        if (!coll.Contains(node.ChildNodes[i].Name))
        //                        {
        //                            coll.Add(node.ChildNodes[i].Name);
        //                            //for emp file

        //                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        //                            parentNode1.AppendChild(parentNode);

        //                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        //                            {
        //                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, startwith + node.ChildNodes[i].Name, fileName, baseParentTag);
        //                            }
        //                            break;
        //                        }
        //                        else if (coll.Contains(node.ChildNodes[i].Name) && !coll.IndexOf(node.ChildNodes[i].Name).Equals(coll.Count - 1))
        //                        {
        //                            coll.Add(node.ChildNodes[i].Name);
        //                            //for emp file

        //                            System.Xml.XmlElement parentNode = requestXmlDoc.CreateElement(node.ChildNodes[i].Name);
        //                            parentNode1.AppendChild(parentNode);

        //                            if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].Name.Equals("#text"))
        //                            {
        //                                getXmlElements(node.ChildNodes[i], mapList, parentNode, requestXmlDoc, coll, startwith + node.ChildNodes[i].Name, fileName, baseParentTag);
        //                            }
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    requestXmlDoc.Save(fileName);
        //}




        ///// <summary>
        /////Get Mappings From transform xml File 
        ///// </summary>
        ///// <param name="fileName"></param>
        ///// <returns></returns>

        //private Hashtable getMappedList(string fileName)
        //{
        //    Hashtable xmlMappedNodes = new Hashtable();

        //    System.Xml.XmlDocument xdoc = new XmlDocument();
        //    xdoc.Load(fileName);

        //    XmlNode root = (XmlNode)xdoc.DocumentElement;

        //    for (int i = 0; i < root.ChildNodes.Count; i++)
        //    {
        //        if (root.ChildNodes.Item(i).Name == "UserMappings")
        //        {
        //            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

        //            foreach (XmlNode childNodes in userMappingsXML)
        //            {
        //                if (childNodes.Name.Equals("mappingName") || childNodes.Name.Equals("MappingType"))
        //                {
        //                }
        //                else if (childNodes.Name.Equals("ImportType"))
        //                {

        //                }
        //                else
        //                {
        //                    if (childNodes.InnerText.Equals(string.Empty))
        //                    {
        //                    }
        //                    else
        //                    {
        //                        if (!xmlMappedNodes.Contains(childNodes.InnerText))
        //                        xmlMappedNodes.Add(childNodes.InnerText, null);
        //                    }
        //                }
        //            }
        //        }
        //        if (root.ChildNodes.Item(i).Name == "ConstantMappings")
        //        {
        //            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

        //            foreach (XmlNode childNodes in userMappingsXML)
        //            {
        //                if (childNodes.Name.Equals("mappingName") || childNodes.Name.Equals("MappingType") || childNodes.Name.Equals("ImportType"))
        //                {
        //                }
        //                else
        //                {
        //                    if (childNodes.InnerText.Equals(string.Empty))
        //                    {
        //                    }
        //                    else
        //                    {
        //                        xmlMappedNodes.Add(childNodes.Name, childNodes.InnerText);
        //                    }
        //                }
        //            }
        //        }


        //    }
        //    File.Delete(fileName);
        //    return xmlMappedNodes;
        //}






    }
    
}