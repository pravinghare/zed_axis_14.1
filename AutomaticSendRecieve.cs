using System;
using System.Collections.Generic;
using System.Text;
using Tamir.SharpSsh.jsch;
using EDI.Message;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Data;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Xml;
using System.Data.Odbc;
using EDI.Constant;
using System.Windows.Forms;
using TransactionImporter;

namespace DataProcessingBlocks
{
    public class AutomaticSendRecieve
    {
        private int m_totalSend;
        private int m_totalRecieve;
        private string m_connected;
        String showCount;
        Outlook m_outlook;
        //Ebay
        private DateTime eBayUpdatedDate;
        private string eBayToken;
        private string eBaytradingPartnerName;
        private string Service = string.Empty;

        //OsCommerce
        private string osCommerceConnectionString;
        private DateTime osCommerceUpdatedDate;
        private string osCommerceTradingPartnerName;

        private string dbHostName = string.Empty;
        private string dbPort = string.Empty;
        private string dbUserName = string.Empty;
        private string dbPassword = string.Empty;
        private string dbName = string.Empty;
        private string dbPrefix = string.Empty;
        private string sshHostName = string.Empty;
        private string sshPortNo = string.Empty;
        private string sshUserName = string.Empty;
        private string sshPassword = string.Empty;
        private string eBayType = string.Empty;
 
        DefaultAccountSettings m_accountSettings;
        string m_growLabel;
        Session session;
        JSch jsch = new JSch();
        int rPort = -1;
        string host = null;

        public AutomaticSendRecieve(Outlook outlook, string connectedSoftware, DefaultAccountSettings accountSettings, string growLabel)
        {            
            m_totalSend = new MessageController().GetDraftMessageCount();
            this.m_connected = connectedSoftware;
            this.m_growLabel = growLabel;

            if (accountSettings != null)
                this.m_accountSettings = accountSettings.GetDefaultAccountSettings();
           
            m_outlook = outlook;
            backgroundWorkerSend();
        }

        public AutomaticSendRecieve(string ebay_Service, DateTime eBay_updatedDate, string eBay_Token, string eBay_tradingPartnerName, string eBayType)
        {         
            Service = ebay_Service;
            eBayUpdatedDate = eBay_updatedDate;
            eBayToken = eBay_Token;
            eBaytradingPartnerName = eBay_tradingPartnerName;
            if (string.IsNullOrEmpty(eBayType))
                this.eBayType = "SandBox";
            else
                this.eBayType = eBayType;
            backgroundWorkerSend();
        }

        public AutomaticSendRecieve(string osCommerce_service, DateTime commerce_UpdatedDate, string commerce_oscommerceServer, string os_userName, string os_password, string os_dbName, string os_tradingPartnerName)
        {           
            Service = osCommerce_service;
            osCommerceUpdatedDate = commerce_UpdatedDate;
            dbHostName = commerce_oscommerceServer;
            dbUserName = os_userName;
            dbPassword = os_password;
            dbName = os_dbName;
            osCommerceTradingPartnerName = os_tradingPartnerName;
            backgroundWorkerSend();
        }

        private void backgroundWorkerSend()
        {
            if (CommonUtilities.GetInstance().IsOutboundFlag)
            {
                int index = 1;
                try
                {
                    DataSet dataSetDraftMails = new MessageController().GetDraftMails();
                    if (dataSetDraftMails != null && dataSetDraftMails.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dataRow in dataSetDraftMails.Tables[0].Rows)
                        {

                            m_outlook.Send(dataRow);
                            index++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //CommonUtilities.WriteErrorLog(ex.Message);
                    //CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
            }
            try
            {
                m_totalRecieve = m_outlook.GetMessageCount();
            }
            catch
            { }
            if (Service.Equals("eBay"))
            {
                backgroundWorkerEbayService();
            }
            else if (Service.Equals("OsCommerce"))
            {
                backgroundWorkerOsCommerceService();
            }
            else
            {
                backgroundWorkerRecieve();
            }
        }
     
        private void backgroundWorkerRecieve()
        {
            try
            {
                Pop3MailClient pop3MailClient = null;
                try
                {
                    pop3MailClient = m_outlook.Connect();
                }
                catch { }
                for (int index = 1; index <= m_totalRecieve; index++)
                {

                    //Invoke the save function to download and save mail messages.
                    m_outlook.Save(pop3MailClient, index);

                }
                try
                {
                    m_outlook.Disconnect(pop3MailClient);
                }
                catch { }
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            System.Threading.Thread.Sleep(500);
        }
      
        private void backgroundWorkerEbayService()
        {
            string message = string.Empty;

            string headerLine = string.Empty;

            string[] middleLine = null;

            StringBuilder sb = new StringBuilder();

            //For getting Seller Transactions
            //string xmlText = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?><GetSellerTransactionsRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><eBayAuthToken>" + eBayToken + "</eBayAuthToken></RequesterCredentials><IncludeContainingOrder>1</IncludeContainingOrder><ModTimeFrom>" + updatedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeFrom><ModTimeTo>" + DateTime.Now.AddMinutes(-5).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeTo><TransactionArray><Transaction><ContainingOrder><OrderStatus>Completed</OrderStatus></ContainingOrder></Transaction></TransactionArray></GetSellerTransactionsRequest>";
        
            //  string xmlText = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?><GetSellerTransactionsRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><eBayAuthToken>" + eBayToken + "</eBayAuthToken></RequesterCredentials><IncludeContainingOrder>1</IncludeContainingOrder><ModTimeFrom>" + eBayUpdatedDate.AddMinutes(-5).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeFrom><ModTimeTo>" + DateTime.Now.AddMinutes(5).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeTo></GetSellerTransactionsRequest>";

            string xmlText = @"<?xml version=""1.0"" encoding=""iso88591_to_utf8""?><GetSellerTransactionsRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><eBayAuthToken>" + eBayToken + "</eBayAuthToken></RequesterCredentials><IncludeContainingOrder>1</IncludeContainingOrder><ModTimeFrom>" + eBayUpdatedDate.AddMinutes(-5).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeFrom><ModTimeTo>" + DateTime.Now.AddMinutes(5).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeTo></GetSellerTransactionsRequest>";

            //Put the data into a UTF8 encoded  byte array
            UTF8Encoding encoding = new UTF8Encoding();
            int dataLen = encoding.GetByteCount(xmlText);
            byte[] utf8Bytes = new byte[dataLen];
            Encoding.UTF8.GetBytes(xmlText, 0, xmlText.Length, utf8Bytes, 0);

            #region Setup The Request inc. HTTP Headers
            //Create a new HttpWebRequest object for the SandBox ServerUrl
            HttpWebRequest request = null;

            if (eBayType.Equals("SandBox"))
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.ebay.com/ws/api.dll");
            }

            //Set Request Method (POST) and Content Type (text/xml)
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.ContentLength = utf8Bytes.Length;

            if (eBayType.Equals("SandBox"))
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-897e-4a18-a573-07714198a5d0");
                request.Headers.Add("X-EBAY-API-CERT-NAME: fb608b80-f4d3-411f-b7a4-eb1f703de66d");
            }
            else
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-1857-4ef5-80db-d9d837e3db06");
                request.Headers.Add("X-EBAY-API-CERT-NAME: ce11b74b-d701-45bd-b097-5b1cc9a42dcb");
            }
            //Add Compatability Level to HTTP Headers
            //Regulates versioning of the XML interface for the API
            request.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL: 647");

            //Add function name, SiteID and Detail Level to HTTP Headers
            request.Headers.Add("X-EBAY-API-CALL-NAME: GetSellerTransactions");
            request.Headers.Add("X-EBAY-API-SITEID: 0");

            //Time out = 15 seconds,  set to -1 for no timeout.
            //If times-out - throws a WebException with the
            //Status property set to WebExceptionStatus.Timeout.
            request.Timeout = 15000;

            #endregion

            #region Send The Request
            Stream str = null;
            try
            {
                //Set the request Stream
                str = request.GetRequestStream();
                //Write the equest to the Request Steam
                str.Write(utf8Bytes, 0, utf8Bytes.Length);
                str.Close();
                //Get response into stream
                WebResponse resp = request.GetResponse();
                str = resp.GetResponseStream();
            }
            catch (WebException wEx)
            {
                //CommonUtilities.WriteErrorLog(wEx.Message.ToString());
                //CommonUtilities.WriteErrorLog(wEx.StackTrace.ToString());
                message = wEx.Message;
                CommonUtilities.WriteErrorLog(message);
                return;
                //return message;
            }
            #endregion

            #region Process Response
            // Get Response into String
            StreamReader sr = new StreamReader(str);
            XmlDocument xmlDoc = new XmlDocument();
            System.Data.DataTable dataTable = new System.Data.DataTable();

            xmlDoc.LoadXml(sr.ReadToEnd());
            sr.Close();
            str.Close();

            //get the root node, for ease of use
            XmlNode root = xmlDoc["GetSellerTransactionsResponse"];

            string sessionID = string.Empty;
            //There have been Errors
            if (root["Errors"] != null)
            {
                string errorCode = root["Errors"]["ErrorCode"].InnerText;
                string errorShort = root["Errors"]["ShortMessage"].InnerText;
                string errorLong = root["Errors"]["LongMessage"].InnerText;

                CommonUtilities.WriteErrorLog(errorCode + "\n " + errorShort);
                CommonUtilities.WriteErrorLog(errorCode + "\n " + errorLong);
                CommonUtilities.WriteErrorLog(errorCode + "\n " + errorShort + "\n\n" + errorCode + "\n " + errorLong);
                //return errorLong;

            }
            else
            {
                #region Add Orders to database
                try
                {
                    #region Datatable Columns

                    dataTable.Columns.Add("orderID");
                    dataTable.Columns.Add("amountPaid");
                    dataTable.Columns.Add("buyerUserID");
                    dataTable.Columns.Add("createdTime");
                    dataTable.Columns.Add("shippedTime");
                    dataTable.Columns.Add("shippingAddressName");
                    dataTable.Columns.Add("shippingAddressPhone");
                    dataTable.Columns.Add("shippingAddressStreet1");
                    dataTable.Columns.Add("shippingAddressStreet2");
                    dataTable.Columns.Add("shippingAddressCityName");
                    dataTable.Columns.Add("shippingAddressStateOrProvince");
                    dataTable.Columns.Add("shippingAddressPostalCode");
                    dataTable.Columns.Add("shippingAddressCountryName");
                    dataTable.Columns.Add("shippingDetailsInsuranceFee");

                    dataTable.Columns.Add("shippingDetailsServiceOptionShippingServiceCost");
                    dataTable.Columns.Add("shippingDetailsServiceOptionShippingInsuranceCost");
                    dataTable.Columns.Add("shippingDetailsServiceOptionShippingServiceAdditionalCost");
                    dataTable.Columns.Add("shippingAdjustmentAmount");
                    dataTable.Columns.Add("sellingManagerSalesRecordId");
                    dataTable.Columns.Add("itemId");
                    dataTable.Columns.Add("sku");
                    dataTable.Columns.Add("title");
                    dataTable.Columns.Add("quantityPurchased");
                    dataTable.Columns.Add("transactionPrice");

                    #endregion
                    int index = 0;
                    int indexex = 0;
                    foreach (System.Xml.XmlNode mainNode in xmlDoc.ChildNodes)
                    {
                        foreach (System.Xml.XmlNode oNode in mainNode.ChildNodes)
                        {
                            if (oNode.Name.Equals("TransactionArray"))
                            {

                                foreach (XmlNode orderArrayNodes in oNode.ChildNodes)
                                {
                                    string orderID = string.Empty;
                                    string amountPaid = string.Empty;
                                    string buyerUserID = string.Empty;
                                    string createdTime = string.Empty;
                                    string shippedTime = string.Empty;
                                    string shippingAddressName = string.Empty;
                                    string shippingAddressPhone = string.Empty;
                                    string shippingAddressStreet1 = string.Empty;
                                    string shippingAddressStreet2 = string.Empty;
                                    string shippingAddressCityName = string.Empty;
                                    string shippingAddressStateOrProvince = string.Empty;
                                    string shippingAddressPostalCode = string.Empty;
                                    string shippingAddressCountryName = string.Empty;
                                    string shippingDetailsInsuranceFee = string.Empty;
                                    string shippingDetailsInternationalServiceOptionShippingServiceCost = string.Empty;
                                    string shippingDetailsInternationalServiceOptionShippingInsuranceCost = string.Empty;
                                    string shippingDetailsInternationalServiceOptionShippingServiceAdditionalCost = string.Empty;
                                    string shippingDetailsServiceOptionShippingServiceCost = string.Empty;
                                    string shippingDetailsServiceOptionShippingInsuranceCost = string.Empty;
                                    string shippingDetailsServiceOptionShippingServiceAdditionalCost = string.Empty;
                                    string shippingAdjustmentAmount = string.Empty;
                                    string sellingManagerSalesRecordId = string.Empty;
                                    bool isOrder = false;
                                    object[] objArray = new object[24];


                                    indexex = index++;


                                    //Checking TxnID
                                    #region Transaction Nodes
                                    if (orderArrayNodes.Name.Equals("Transaction"))
                                    {

                                        middleLine = new string[orderArrayNodes.ChildNodes.Count];
                                        int count = 0;
                                        string itemId = string.Empty;
                                        string sku = string.Empty;
                                        string title = string.Empty;
                                        string quantityPurchased = string.Empty;
                                        string transactionPrice = string.Empty;

                                        foreach (XmlNode orderNodes in orderArrayNodes.ChildNodes)
                                        {
                                            if (orderNodes.Name.Equals("AdjustmentAmount"))
                                            {
                                                shippingAdjustmentAmount = orderNodes.InnerText;
                                            }
                                            if (orderNodes.Name.Equals("Buyer"))
                                            {
                                                foreach (XmlNode ordNode in orderNodes.ChildNodes)
                                                {
                                                    if (ordNode.Name.Equals("UserID"))
                                                    {
                                                        buyerUserID = ordNode.InnerText;
                                                    }
                                                    if (ordNode.Name.Equals("BuyerInfo"))
                                                    {
                                                        foreach (XmlNode shipNode in ordNode.ChildNodes)
                                                        {
                                                            if (shipNode.Name.Equals("ShippingAddress"))
                                                            {
                                                                foreach (XmlNode shippingNode in shipNode.ChildNodes)
                                                                {
                                                                    if (shippingNode.Name.Equals("Name"))
                                                                    {
                                                                        shippingAddressName = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("Phone"))
                                                                    {
                                                                        shippingAddressPhone = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("Street1"))
                                                                    {
                                                                        shippingAddressStreet1 = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("Street2"))
                                                                    {
                                                                        shippingAddressStreet2 = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("CityName"))
                                                                    {
                                                                        shippingAddressCityName = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("StateOrProvince"))
                                                                    {
                                                                        shippingAddressStateOrProvince = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("PostalCode"))
                                                                    {
                                                                        shippingAddressPostalCode = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("CountryName"))
                                                                    {
                                                                        shippingAddressCountryName = shippingNode.InnerText;
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            if (orderNodes.Name.Equals("AmountPaid"))
                                            {

                                                amountPaid = orderNodes.InnerText;
                                            }

                                            if (orderNodes.Name.Equals("CreatedDate"))
                                            {
                                                createdTime = orderNodes.InnerText;
                                            }
                                            if (orderNodes.Name.Equals("ShippedTime"))
                                            {
                                                shippedTime = orderNodes.InnerText;
                                            }

                                            if (orderNodes.Name.Equals("ShippingDetails"))
                                            {
                                                foreach (XmlNode shippingDetailsNode in orderNodes.ChildNodes)
                                                {
                                                    if (shippingDetailsNode.Name.Equals("InsuranceFee"))
                                                    {
                                                        shippingDetailsInsuranceFee = shippingDetailsNode.InnerText;
                                                    }
                                                    if (shippingDetailsNode.Name.Equals("SellingManagerSalesRecordNumber"))
                                                    {
                                                        sellingManagerSalesRecordId = shippingDetailsNode.InnerText;
                                                    }

                                                }
                                            }
                                            if (orderNodes.Name.Equals("ContainingOrder"))
                                            {
                                                foreach (XmlNode ordNode in orderNodes.ChildNodes)
                                                {
                                                    if (ordNode.Name.Equals("ShippingDetails"))
                                                    {
                                                        foreach (XmlNode shippingDetailsNode in ordNode.ChildNodes)
                                                        {
                                                            if (shippingDetailsNode.Name.Equals("SellingManagerSalesRecordNumber"))
                                                            {
                                                                sellingManagerSalesRecordId = shippingDetailsNode.InnerText;
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                            if (orderNodes.Name.Equals("Item"))
                                            {

                                                foreach (XmlNode itemListNode in orderNodes.ChildNodes)
                                                {
                                                    if (itemListNode.Name.Equals("ItemID"))
                                                    {
                                                        itemId = itemListNode.InnerText;
                                                    }
                                                    if (itemListNode.Name.Equals("SKU"))
                                                    {
                                                        sku = itemListNode.InnerText;
                                                    }
                                                    if (itemListNode.Name.Equals("Title"))
                                                    {
                                                        title = itemListNode.InnerText;
                                                    }
                                                }
                                            }

                                            if (orderNodes.Name.Equals("QuantityPurchased"))
                                            {
                                                quantityPurchased = orderNodes.InnerText;

                                            }
                                            if (orderNodes.Name.Equals("TransactionPrice"))
                                            {
                                                transactionPrice = orderNodes.InnerText;
                                            }

                                            if (orderNodes.Name.Equals("ShippingServiceSelected"))
                                            {
                                                foreach (XmlNode shippingServiceNode in orderNodes.ChildNodes)
                                                {
                                                    if (shippingServiceNode.Name.Equals("ShippingServiceCost"))
                                                    {
                                                        shippingDetailsServiceOptionShippingServiceCost = shippingServiceNode.InnerText;
                                                    }
                                                    if (shippingServiceNode.Name.Equals("ShippingInsuranceCost"))
                                                    {
                                                        shippingDetailsServiceOptionShippingInsuranceCost = shippingServiceNode.InnerText;
                                                    }
                                                    if (shippingServiceNode.Name.Equals("ShippingServiceAdditionalCost"))
                                                    {
                                                        shippingDetailsServiceOptionShippingServiceAdditionalCost = shippingServiceNode.InnerText;
                                                    }
                                                }

                                            }
                                            if (orderNodes.Name.Equals("ContainingOrder"))
                                            {
                                                foreach (XmlNode ordNode in orderNodes.ChildNodes)
                                                {
                                                    if (ordNode.Name.Equals("OrderID"))
                                                    {
                                                        orderID = ordNode.InnerText;
                                                        isOrder = true;
                                                        objArray[0] = orderID;
                                                        objArray[1] = amountPaid;

                                                        objArray[2] = buyerUserID;
                                                        objArray[3] = createdTime;
                                                        objArray[4] = shippedTime;
                                                        objArray[5] = shippingAddressName;
                                                        objArray[6] = shippingAddressPhone;
                                                        objArray[7] = shippingAddressStreet1;
                                                        objArray[8] = shippingAddressStreet2;
                                                        objArray[9] = shippingAddressCityName;
                                                        objArray[10] = shippingAddressStateOrProvince;
                                                        objArray[11] = shippingAddressPostalCode;
                                                        objArray[12] = shippingAddressCountryName;

                                                        objArray[13] = shippingDetailsInsuranceFee;

                                                        objArray[14] = shippingDetailsServiceOptionShippingServiceCost;
                                                        objArray[15] = shippingDetailsServiceOptionShippingInsuranceCost;
                                                        objArray[16] = shippingDetailsServiceOptionShippingServiceAdditionalCost;
                                                        objArray[17] = shippingAdjustmentAmount;
                                                        objArray[18] = sellingManagerSalesRecordId;
                                                        objArray[19] = itemId;
                                                        objArray[20] = sku;
                                                        objArray[21] = title;
                                                        objArray[22] = quantityPurchased;
                                                        objArray[23] = transactionPrice;

                                                        dataTable.LoadDataRow(objArray, false);

                                                    }
                                                }
                                            }
                                        }

                                        if (orderID == string.Empty)
                                        {
                                            if (itemId != string.Empty)
                                            {

                                                middleLine[count] += "eBay2|" + sellingManagerSalesRecordId + "|" + itemId + "|" + sku + "|";
                                                middleLine[count] += "" + title + "|" + quantityPurchased + "|" + transactionPrice + "\n";
                                                count++;

                                            }

                                            #region Insert data into database
                                            headerLine += "eBay|" + eBaytradingPartnerName + "|" + sellingManagerSalesRecordId + "\n";
                                            headerLine += "eBay1|" + amountPaid + "|" + buyerUserID + "|" + sellingManagerSalesRecordId + "|" + createdTime + "|" + shippedTime + "|";
                                            headerLine += "" + shippingAddressName + "|" + shippingAddressPhone + "|" + shippingAddressStreet1 + "|";
                                            headerLine += "" + shippingAddressStreet2 + "|" + shippingAddressCityName + "|" + shippingAddressStateOrProvince + "|";
                                            headerLine += "" + shippingAddressPostalCode + "|" + shippingAddressCountryName + "|" + shippingDetailsInsuranceFee + "|";
                                            headerLine += "" + shippingDetailsInternationalServiceOptionShippingServiceCost + "|" + shippingDetailsInternationalServiceOptionShippingInsuranceCost + "|" + shippingDetailsInternationalServiceOptionShippingServiceAdditionalCost + "|";
                                            headerLine += "" + shippingDetailsServiceOptionShippingServiceCost + "|" + shippingDetailsServiceOptionShippingInsuranceCost + "|" + shippingDetailsServiceOptionShippingServiceAdditionalCost + "|" + shippingAdjustmentAmount + "\n";

                                            sb.AppendLine(headerLine);


                                            foreach (string item in middleLine)
                                            {
                                                if (item != null)
                                                {
                                                    if (item != string.Empty)
                                                    {
                                                        sb.AppendLine(item);
                                                    }
                                                }
                                            }


                                            SaveOrder(sb, sellingManagerSalesRecordId, eBaytradingPartnerName);

                                            headerLine = string.Empty;
                                            sb = new StringBuilder();
                                            #endregion
                                        }

                                    }
                                    else
                                    {
                                        FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                        StreamWriter wrtLog = new StreamWriter(fs);
                                        wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                                        wrtLog.WriteLine(" There are no orders available on EBay Server \n");
                                        wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                                        wrtLog.WriteLine("\n------------------------------------------------------------\n");
                                        wrtLog.Close();
                                    }
                                    #endregion

                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                    //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                }
                #endregion

                //GetMultipleOrderID(dataTable, tradingPartnerName);
                GetMultipleSku(dataTable, eBaytradingPartnerName);
            }
            #endregion
        }
     
        /// <summary>
        /// This method is used for Getting Order into Proper format
        /// and Store that order into Axis v5.0 database.
        /// </summary>
        /// <param name="respData">Object of String Buider which will be stored in database.</param>
        private void SaveOrder(StringBuilder sb, string orderId, string tradingPartnerName)
        {
            OdbcConnection con = new OdbcConnection();
            con.ConnectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");

            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = con;
            //Getting Trading partner id.
            cmd.CommandText = "SELECT TradingPartnerID FROM trading_partner_schema WHERE TPName='" + tradingPartnerName + "'";
            con.Open();
            OdbcDataReader dr = cmd.ExecuteReader();

            string tradingPartnerID = string.Empty;

            while (dr.Read())
            {
                tradingPartnerID = dr[0].ToString();
            }

            dr.Close();
            cmd.Dispose();
            con.Close();
            MemoryStream stream = null;
            string Attachname = "eBay  " + tradingPartnerName + " " + orderId;

            try
            {
                con.Open();
                string myString = sb.ToString();
                byte[] myByteArray = System.Text.Encoding.ASCII.GetBytes(myString);
                stream = new MemoryStream(myByteArray);

                BinaryReader mmsfileReader = new BinaryReader((Stream)stream);
                string MessageDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //Insert Message data into database.
                OdbcParameter paramFile = new OdbcParameter("parameterName", mmsfileReader.ReadBytes((int)stream.Length));
                string serviceText = string.Empty;
                string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag,TradingPartnerID,MailProtocolID) VALUES ('" + MessageDate + "','3','" + tradingPartnerName + "','Order " + orderId + "',0," + tradingPartnerID + ",'4')";
                cmd.Connection = con;
                cmd.CommandText = commandText;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                //For getting Message id from message schema.
                OdbcCommand cmdObj = new OdbcCommand();
                con.Open();
                cmdObj.Connection = con;
                cmdObj.CommandText = "SELECT MAX(MessageID) FROM message_schema";
                object messageID = cmdObj.ExecuteScalar();
                cmdObj.Dispose();
                con.Close();
                //For adding eBay message into Database.
                string insertMessageCommand = string.Empty;
                if (messageID != null)
                    insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'3')";
                OdbcCommand cmdFile = new OdbcCommand();
                con.Open();
                cmdFile.Connection = con;
                cmdFile.CommandText = insertMessageCommand;
                cmdFile.Parameters.Add(paramFile);
                cmdFile.ExecuteNonQuery();
                con.Close();
                mmsfileReader.Close();
                stream.Close();

                //For update LastUpdated ebay date.
                string commandDateText = " UPDATE trading_partner_schema SET LastUpdatedDate = '" + MessageDate + "' WHERE TradingPartnerID =" + tradingPartnerID + "";
                OdbcCommand commandDate = new OdbcCommand();
                commandDate.Connection = con;
                con.Open();
                commandDate.CommandText = commandDateText;
                commandDate.ExecuteNonQuery();
                commandDate.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }

        }

        private void GetMultipleSku(System.Data.DataTable dataTable, string tradingPartnerName)
        {
            DataView view = dataTable.DefaultView;
            decimal totalSum = 0;
            view.Sort = "orderID";
            dataTable = view.ToTable();

            #region Format Orders

            int count = 0;
            string headerLine = string.Empty;
            StringBuilder sb = new StringBuilder();
            string[] middleLine = new string[dataTable.Rows.Count];

            #region For Single orderId & SKU.

            if (dataTable.Rows.Count == 1)
            {
                if (dataTable.Rows[0][19].ToString() != string.Empty)
                {
                    middleLine[0] += "eBay2|" + dataTable.Rows[0][18].ToString() + "|" + dataTable.Rows[0][19].ToString() + "|" + dataTable.Rows[0][20].ToString() + "|";
                    middleLine[0] += "" + dataTable.Rows[0][21].ToString() + "|" + dataTable.Rows[0][22].ToString() + "|" + dataTable.Rows[0][23].ToString() + "\n";
                }

                #region Insert data into database


                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[0][18].ToString() + "\n";
                headerLine += "eBay1|" + dataTable.Rows[0][1].ToString() + "|" + dataTable.Rows[0][2].ToString() + "|" + dataTable.Rows[0][18].ToString() + "|" + dataTable.Rows[0][3].ToString() + "|" + dataTable.Rows[0][4].ToString() + "|";
                headerLine += "" + dataTable.Rows[0][5].ToString() + "|" + dataTable.Rows[0][6].ToString() + "|" + dataTable.Rows[0][7].ToString() + "|";
                headerLine += "" + dataTable.Rows[0][8].ToString() + "|" + dataTable.Rows[0][9].ToString() + "|" + dataTable.Rows[0][10].ToString() + "|";
                headerLine += "" + dataTable.Rows[0][11].ToString() + "|" + dataTable.Rows[0][12].ToString() + "|" + dataTable.Rows[0][13].ToString() + "|";
                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                headerLine += "" + dataTable.Rows[0][14].ToString() + "|" + dataTable.Rows[0][15].ToString() + "|" + dataTable.Rows[0][16].ToString() + "|" + dataTable.Rows[0][17].ToString() + "\n";

                sb.AppendLine(headerLine);


                foreach (string item in middleLine)
                {
                    if (item != null)
                    {
                        if (item != string.Empty)
                        {
                            sb.AppendLine(item);
                        }
                    }
                }


                SaveOrder(sb, dataTable.Rows[0][18].ToString(), tradingPartnerName);
                headerLine = string.Empty;
                sb = new StringBuilder();
                count = 0;
                middleLine = new string[dataTable.Rows.Count];
                #endregion

                return;

            }

            #endregion

            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
            {

                if (rowIndex == dataTable.Rows.Count - 1)//If Last Row
                {
                    if (dataTable.Rows[rowIndex][0].ToString() != dataTable.Rows[rowIndex - 1][0].ToString())//if (count > 0)
                    {
                        #region Insert data into database

                        headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "\n";
                        headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex - 1][2].ToString() + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "|" + dataTable.Rows[rowIndex - 1][3].ToString() + "|" + dataTable.Rows[rowIndex - 1][4].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][5].ToString() + "|" + dataTable.Rows[rowIndex - 1][6].ToString() + "|" + dataTable.Rows[rowIndex - 1][7].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][8].ToString() + "|" + dataTable.Rows[rowIndex - 1][9].ToString() + "|" + dataTable.Rows[rowIndex - 1][10].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][11].ToString() + "|" + dataTable.Rows[rowIndex - 1][12].ToString() + "|" + dataTable.Rows[rowIndex - 1][13].ToString() + "|";
                        headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][14].ToString() + "|" + dataTable.Rows[rowIndex - 1][15].ToString() + "|" + dataTable.Rows[rowIndex - 1][16].ToString() + "|" + dataTable.Rows[rowIndex - 1][17].ToString() + "\n";

                        sb.AppendLine(headerLine);

                        foreach (string item in middleLine)
                        {
                            if (item != null)
                            {
                                if (item != string.Empty)
                                {
                                    sb.AppendLine(item);
                                }
                            }
                        }


                        SaveOrder(sb, dataTable.Rows[rowIndex - 1][18].ToString(), tradingPartnerName);
                        headerLine = string.Empty;
                        sb = new StringBuilder();
                        count = 0;
                        totalSum = 0;
                        middleLine = new string[dataTable.Rows.Count];
                        #endregion

                        //For the last current row to insert***********************************
                        if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                        {
                            middleLine[0] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                            middleLine[0] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";
                        }

                        #region Insert data into database


                        headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                        headerLine += "eBay1|" + dataTable.Rows[rowIndex][1].ToString() + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                        headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                        sb.AppendLine(headerLine);


                        foreach (string item in middleLine)
                        {
                            if (item != null)
                            {
                                if (item != string.Empty)
                                {
                                    sb.AppendLine(item);
                                }
                            }
                        }


                        SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                        headerLine = string.Empty;
                        sb = new StringBuilder();
                        count = 0;
                        totalSum = 0;
                        middleLine = new string[dataTable.Rows.Count];
                        #endregion
                    }
                    else
                    {

                        //To Check whether current row orderid == previous row orderId.
                        if (dataTable.Rows[rowIndex][0].ToString() == dataTable.Rows[rowIndex - 1][0].ToString())
                        {

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                                middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";
                            }

                            //totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex - 1][0].ToString());

                            //For the Current row.
                            #region Insert data into database
                            headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                            headerLine += "eBay1|" + totalSum + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                            headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                            sb.AppendLine(headerLine);


                            foreach (string item in middleLine)
                            {
                                if (item != null)
                                {
                                    if (item != string.Empty)
                                    {
                                        sb.AppendLine(item);
                                    }
                                }
                            }


                            SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                            headerLine = string.Empty;
                            sb = new StringBuilder();
                            count = 0;
                            totalSum = 0;
                            middleLine = new string[dataTable.Rows.Count];
                            #endregion
                        }

                        else
                        {
                            //For the Previous row. 
                            #region Insert data into database

                            headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "\n";
                            headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex - 1][2].ToString() + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "|" + dataTable.Rows[rowIndex - 1][3].ToString() + "|" + dataTable.Rows[rowIndex - 1][4].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][5].ToString() + "|" + dataTable.Rows[rowIndex - 1][6].ToString() + "|" + dataTable.Rows[rowIndex - 1][7].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][8].ToString() + "|" + dataTable.Rows[rowIndex - 1][9].ToString() + "|" + dataTable.Rows[rowIndex - 1][10].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][11].ToString() + "|" + dataTable.Rows[rowIndex - 1][12].ToString() + "|" + dataTable.Rows[rowIndex - 1][13].ToString() + "|";
                            headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][14].ToString() + "|" + dataTable.Rows[rowIndex - 1][15].ToString() + "|" + dataTable.Rows[rowIndex - 1][16].ToString() + "|" + dataTable.Rows[rowIndex - 1][17].ToString() + "\n";

                            sb.AppendLine(headerLine);

                            foreach (string item in middleLine)
                            {
                                if (item != null)
                                {
                                    if (item != string.Empty)
                                    {
                                        sb.AppendLine(item);
                                    }
                                }
                            }


                            SaveOrder(sb, dataTable.Rows[rowIndex - 1][18].ToString(), tradingPartnerName);
                            headerLine = string.Empty;
                            sb = new StringBuilder();
                            count = 0;
                            totalSum = 0;
                            middleLine = new string[dataTable.Rows.Count];
                            #endregion

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());

                                middleLine[0] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[0] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";

                            }

                            //For the Current row.
                            #region Insert data into database

                            headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                            headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                            headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                            sb.AppendLine(headerLine);


                            foreach (string item in middleLine)
                            {
                                if (item != null)
                                {
                                    if (item != string.Empty)
                                    {
                                        sb.AppendLine(item);
                                    }
                                }
                            }


                            SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                            headerLine = string.Empty;
                            sb = new StringBuilder();
                            count = 0;
                            totalSum = 0;
                            middleLine = new string[dataTable.Rows.Count];
                            #endregion
                        }
                    }
                }
                else
                {
                    if (rowIndex == 0)
                    {
                        if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                        {
                            totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                            middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                            middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";

                        }

                    }
                    else
                    {
                        if (dataTable.Rows[rowIndex][0].ToString() == dataTable.Rows[rowIndex - 1][0].ToString())
                        {

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                                middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";
                                count++;
                            }
                            if (rowIndex == dataTable.Rows.Count - 1)
                            {
                                #region Insert data into database


                                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                                headerLine += "eBay1|" + totalSum + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                                sb.AppendLine(headerLine);


                                foreach (string item in middleLine)
                                {
                                    if (item != null)
                                    {
                                        if (item != string.Empty)
                                        {
                                            sb.AppendLine(item);
                                        }
                                    }
                                }


                                SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                                headerLine = string.Empty;
                                sb = new StringBuilder();
                                count = 0;
                                totalSum = 0;
                                middleLine = new string[dataTable.Rows.Count];
                                #endregion
                            }

                        }
                        else
                        {

                            if (dataTable.Rows.Count > 1)
                            {
                                #region Insert data into database

                                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "\n";
                                headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex - 1][2].ToString() + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "|" + dataTable.Rows[rowIndex - 1][3].ToString() + "|" + dataTable.Rows[rowIndex - 1][4].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][5].ToString() + "|" + dataTable.Rows[rowIndex - 1][6].ToString() + "|" + dataTable.Rows[rowIndex - 1][7].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][8].ToString() + "|" + dataTable.Rows[rowIndex - 1][9].ToString() + "|" + dataTable.Rows[rowIndex - 1][10].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][11].ToString() + "|" + dataTable.Rows[rowIndex - 1][12].ToString() + "|" + dataTable.Rows[rowIndex - 1][13].ToString() + "|";
                                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][14].ToString() + "|" + dataTable.Rows[rowIndex - 1][15].ToString() + "|" + dataTable.Rows[rowIndex - 1][16].ToString() + "|" + dataTable.Rows[rowIndex - 1][17].ToString() + "\n";

                                sb.AppendLine(headerLine);


                                foreach (string item in middleLine)
                                {
                                    if (item != null)
                                    {
                                        if (item != string.Empty)
                                        {
                                            sb.AppendLine(item);
                                        }
                                    }
                                }


                                SaveOrder(sb, dataTable.Rows[rowIndex - 1][0].ToString(), tradingPartnerName);
                                headerLine = string.Empty;
                                sb = new StringBuilder();
                                count = 0;
                                totalSum = 0;
                                middleLine = new string[dataTable.Rows.Count];
                                #endregion
                            }

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                                middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";

                            }
                            if (rowIndex == dataTable.Rows.Count - 1)
                            {
                                #region Insert data into database


                                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                                headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                                sb.AppendLine(headerLine);


                                foreach (string item in middleLine)
                                {
                                    if (item != null)
                                    {
                                        if (item != string.Empty)
                                        {
                                            sb.AppendLine(item);
                                        }
                                    }
                                }


                                SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                                headerLine = string.Empty;
                                sb = new StringBuilder();
                                count = 0;
                                totalSum = 0;
                                middleLine = new string[dataTable.Rows.Count];
                                #endregion
                            }
                            //}

                        }
                    }
                }
            }
            #endregion
        }

        //OsCommerce

        public string GetTotal(string connectionString, int version, string orderID)
        {
            string shipDetails = string.Empty;

            if (!string.IsNullOrEmpty(sshHostName))
            {
                //int port = 22;

                try
                {

                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a ODBC Connection.
                            MySqlConnection conn = new MySqlConnection();
                            MySqlCommand cmmd = new MySqlCommand();
                            MySqlDataAdapter da;

                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout =30;";
                            conn.Open();
                            cmmd = new MySqlCommand();
                            cmmd.Connection = conn;

                            string Query = string.Empty;
                            //if (version == 2)
                            //{
                            //Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldTotal, orderID);
                            Query = "SELECT value FROM " + dbPrefix.Trim() + "orders_total WHERE orders_id =" + orderID + " AND class='ot_total'";

                            //}
                            //else
                            //{
                            //Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringTotal, orderID);
                            //}


                            cmmd.CommandText = Query;
                            da = new MySqlDataAdapter(cmmd);
                            DataSet orderDataSet = new DataSet();
                            da.Fill(orderDataSet);
                            if (orderDataSet == null)
                            {
                                return null;
                            }
                            else
                                if (orderDataSet.Tables[0].Rows.Count == 0)
                                {
                                    return null;
                                }

                            shipDetails = orderDataSet.Tables[0].Rows[0][0].ToString();
                            cmmd.Dispose();
                            conn.Close();

                        }
                        catch (SocketException ex)
                        {
                            CommonUtilities.WriteErrorLog("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message);
                            session.disconnect();

                        }
                        catch { }
                    }
                }
                catch { }
            }
            else
            {
                MySqlConnection con = new MySqlConnection();

                if (dbPort == string.Empty || dbPort == "3306")
                    connectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                else
                    connectionString = "Server=" + dbHostName + ";Port=" + dbPort + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                con.ConnectionString = connectionString;
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                string Query = string.Empty;
                //if (version == 2)
                //{
                //Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldTotal, orderID);
                Query = "SELECT value FROM " + dbPrefix.Trim() + "orders_total WHERE orders_id =" + orderID + " AND class='ot_total'";

                //}
                //else
                //{
                // Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringTotal, orderID);
                //}

                cmd.CommandText = Query;
                con.Open();
                MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmd);
                DataSet orderDataSet = new DataSet();
                orderAdapter.Fill(orderDataSet);
                cmd.Dispose();
                con.Close();

                if (orderDataSet == null)
                {
                    return string.Empty;
                }
                else
                    if (orderDataSet.Tables[0].Rows.Count == 0)
                    {
                        return string.Empty;
                    }

                shipDetails = orderDataSet.Tables[0].Rows[0][0].ToString();
            }
            return shipDetails;
        }

        public bool CheckOrderV2(string oscServer, string userName, string password, string databaseName)
        {
            try
            {
                MySqlConnection con = new MySqlConnection();
                MySqlCommand cmd = new MySqlCommand();

                #region Whether SSH

                if (!string.IsNullOrEmpty(sshHostName))
                {
                    int port = 22;

                    try
                    {
                        session = jsch.getSession(sshUserName, sshHostName, port);
                        session.setHost(sshHostName);
                        session.setPassword(sshPassword);
                        UserInfo ui = new MyUserInfo();
                        session.setUserInfo(ui);

                        session.connect();

                        //Set port forwarding on the opened session.
                        session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));
                        if (session.isConnected())
                        {
                            try
                            {
                                //Create a ODBC Connection.
                                MySqlConnection conn = new MySqlConnection();
                                MySqlCommand cmmd = new MySqlCommand();
                                MySqlDataReader dr;

                                conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                                if (conn.State != ConnectionState.Open)
                                    conn.Open();
                                cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                                cmmd.Connection = conn;
                                this.osCommerceConnectionString = conn.ConnectionString;

                                //cmmd.CommandText = "SHOW FULL TABLES FROM " + dbName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + dbName + "='Orders'";
                                cmmd.CommandText = "SHOW TABLES IN " + dbName;
                                dr = cmmd.ExecuteReader();
                                bool flag = true;
                                while (dr.Read())
                                {
                                    if (dr[0].ToString().ToLower() == dbPrefix.Trim() + "orders")
                                    {
                                        flag = true;
                                        break;
                                    }
                                    else
                                        flag = false;
                                }
                                dr.Close();
                                cmmd.Dispose();
                                if (conn.State != ConnectionState.Closed)
                                    conn.Close();
                                return flag;
                            }
                            catch (SocketException)
                            {
                                CommonUtilities.WriteErrorLog("Connection timeout for the OSCommerce.Please try again.");
                                session.disconnect();
                                return false;
                            }
                            catch { }
                        }
                    }
                    catch (SocketException ex)
                    {
                        //CommonUtilities.WriteErrorLog(ex.Message + ex.StackTrace);
                        session.disconnect();
                        return false;

                    }
                    catch (Exception ex)
                    {
                        //CommonUtilities.WriteErrorLog(ex.Message + ex.StackTrace);
                        session.disconnect();
                        return false;
                    }
                }
                #endregion
                else
                {
                    con = new MySqlConnection();
                    MySqlDataReader dr;
                    string connectString = string.Empty;
                    if (dbPort == string.Empty || dbPort == "3306")
                        connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    else
                        connectString = "Server=" + dbHostName + ";Port=" + dbPort + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    con.ConnectionString = connectString;
                    cmd = new MySqlCommand();
                    cmd.Connection = con;
                    this.osCommerceConnectionString = connectString;


                    //cmd.CommandText = "SHOW FULL TABLES FROM " + databaseName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + databaseName + "='Orders'";
                    cmd.CommandText = "SHOW TABLES IN " + databaseName;
                    con.Open();
                    dr = cmd.ExecuteReader();
                    bool flag = true;
                    while (dr.Read())
                    {
                        if (dr[0].ToString().ToLower() == dbPrefix.Trim() + "orders")
                        {
                            flag = true;
                            break;
                        }
                        else
                            flag = false;
                    }
                    dr.Close();
                    cmd.Dispose();
                    con.Close();
                    return flag;
                }
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }
            return false;
        }

        public bool CheckOrderV3(string oscServer, string userName, string password, string databaseName)
        {

            try
            {

                MySqlCommand cmd = new MySqlCommand();
                MySqlConnection con = new MySqlConnection();
                MySqlDataReader dr;

                if (!string.IsNullOrEmpty(sshHostName))
                {

                    MySqlConnection conn = new MySqlConnection();
                    MySqlCommand cmmd = new MySqlCommand();
                    MySqlDataReader reader;
                    //Set port forwarding on the opened session.
                    //session.disconnect();
                    //session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));

                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a ODBC Connection.
                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                            if (conn.State != ConnectionState.Open)
                                conn.Open();
                            cmmd = new MySqlCommand();
                            cmmd.Connection = conn;
                            this.osCommerceConnectionString = conn.ConnectionString;

                            //cmmd.CommandText = "SHOW FULL TABLES FROM " + dbName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + dbName + "='osc_Orders'";
                            cmd.CommandText = "SHOW TABLES IN " + dbName;
                            //conn.Open();
                            reader = cmmd.ExecuteReader();
                            bool flag = true;
                            while (reader.Read())
                            {
                                if (reader[0].ToString().ToLower() == dbPrefix.Trim() + "osc_orders")
                                {
                                    flag = true;
                                    break;
                                }
                                else
                                    flag = false;
                            }
                            reader.Close();
                            cmmd.Dispose();
                            if (conn.State != ConnectionState.Closed)
                                conn.Close();
                            return flag;
                        }
                        catch (SocketException ex)
                        {
                            CommonUtilities.WriteErrorLog("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message);
                            session.disconnect();
                            return false;
                        }
                        catch { }
                    }
                }

                else
                {
                    con = new MySqlConnection();
                    string connectString = string.Empty;
                    if (dbPort == string.Empty || dbPort == "3306")
                        connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    else
                        connectString = "Server=" + dbHostName + ";Port=" + dbPort + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                    con.ConnectionString = connectString;
                    cmd = new MySqlCommand();
                    cmd.Connection = con;
                    this.osCommerceConnectionString = connectString;


                    //cmd.CommandText = "SHOW FULL TABLES FROM " + dbName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + dbName + "='osc_Orders'";
                    cmd.CommandText = "SHOW TABLES IN " + dbName;
                    con.Open();
                    dr = cmd.ExecuteReader();
                    bool flag = true;
                    while (dr.Read())
                    {
                        if (dr[0].ToString().ToLower() == dbPrefix.Trim() + "osc_orders")
                        {
                            flag = true;
                            break;
                        }
                        else
                            flag = false;
                    }
                    dr.Close();
                    cmd.Dispose();
                    con.Close();
                    return flag;
                }
            }
            catch (SocketException)
            {
                CommonUtilities.WriteErrorLog("Test Connection Failed.Please enter the correct details & try again.");
                session.disconnect();
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog("Test Connection Failed.Please enter the correct details & try again.");
                if (session != null)
                {
                    session.disconnect();
                }
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }
            return false;
        }

        public string[] GetShippingDetails(string connectionString, int version, string orderID)
        {
            string[] shipDetails = new string[2];

            if (!string.IsNullOrEmpty(sshHostName))
            {
                try
                {

                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a MySqlConnection Connection.
                            MySqlConnection conn = new MySqlConnection();
                            MySqlCommand cmmd = new MySqlCommand();
                            MySqlDataAdapter da;

                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                            if (conn.State != ConnectionState.Open)
                                conn.Open();
                            cmmd = new MySqlCommand();
                            cmmd.Connection = conn;

                            string Query = string.Empty;
                            //if (version == 2)
                            //{
                            Query = "SELECT title,value FROM " + dbPrefix.Trim() + "orders_total WHERE orders_id =" + orderID + " AND class='ot_shipping'";
                            //}
                            //else
                            //{
                            //Query = "SELECT title,value FROM " + dbPrefix.Trim() + "osc_orders_total WHERE orders_id =" + orderID + " AND class='ot_shipping'";
                            //}

                            cmmd.CommandText = Query;
                            da = new MySqlDataAdapter(cmmd);
                            DataSet orderDataSet = new DataSet();
                            da.Fill(orderDataSet);
                            if (orderDataSet == null)
                            {
                                return null;
                            }
                            else
                                if (orderDataSet.Tables[0].Rows.Count == 0)
                                {
                                    return null;
                                }

                            shipDetails[0] = orderDataSet.Tables[0].Rows[0][0].ToString();
                            shipDetails[1] = orderDataSet.Tables[0].Rows[0][1].ToString();
                            cmmd.Dispose();
                            if (conn.State != ConnectionState.Closed)
                                conn.Close();

                        }
                        catch (SocketException ex)
                        {
                            CommonUtilities.WriteErrorLog("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message);
                            session.disconnect();

                        }
                        catch { }
                    }
                }
                catch { }
            }
            else
            {
                MySqlConnection con = new MySqlConnection();
                con.ConnectionString = connectionString;
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                string Query = string.Empty;
                //if (version == 2)
                //{
                //Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldShipping, orderID);
                Query = "SELECT title,value FROM " + dbPrefix.Trim() + "orders_total WHERE orders_id =" + orderID + " AND class='ot_shipping'";

                cmd.CommandText = Query;
                con.Open();
                MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmd);
                DataSet orderDataSet = new DataSet();
                orderAdapter.Fill(orderDataSet);
                cmd.Dispose();
                con.Close();

                if (orderDataSet == null)
                {
                    return null;
                }
                else
                    if (orderDataSet.Tables[0].Rows.Count == 0)
                    {
                        return null;
                    }

                shipDetails[0] = orderDataSet.Tables[0].Rows[0][0].ToString();
                shipDetails[1] = orderDataSet.Tables[0].Rows[0][1].ToString();
            }

            return shipDetails;
        }

        /// <summary>
        /// This method is used for Getting Order into Proper format
        /// and Store that order into Axis v5.0 database.
        /// </summary>
        /// <param name="respData">Object of String Buider which will be stored in database.</param>
        private void Save(StringBuilder respData, string orderId, string tradingPartnerName)
        {
            OdbcConnection con = new OdbcConnection();
            con.ConnectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");

            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = con;
            //Getting Trading partner id.
            cmd.CommandText = string.Format(Constants.GetTPIDByTPName, tradingPartnerName);
            con.Open();
            OdbcDataReader dr = cmd.ExecuteReader();

            string tradingPartnerID = string.Empty;

            while (dr.Read())
            {
                tradingPartnerID = dr[0].ToString();
            }

            dr.Close();
            cmd.Dispose();
            con.Close();

            MemoryStream stream = null;
            string Attachname = "OSCommerce  " + tradingPartnerName + " " + orderId;

            try
            {
                con.Open();
                string myString = respData.ToString();
                byte[] myByteArray = System.Text.Encoding.ASCII.GetBytes(myString);
                stream = new MemoryStream(myByteArray);

                BinaryReader mmsfileReader = new BinaryReader((Stream)stream);
                string MessageDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //Insert Message data into database.
                OdbcParameter paramFile = new OdbcParameter("parameterName", mmsfileReader.ReadBytes((int)stream.Length));
                string serviceText = string.Empty;
                string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag,TradingPartnerID,MailProtocolID) VALUES ('" + MessageDate + "','3','" + tradingPartnerName + "','Order " + orderId + "',0," + tradingPartnerID + ",'5')";
                cmd.Connection = con;
                cmd.CommandText = commandText;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                //For getting Message id from message schema.
                OdbcCommand cmdObj = new OdbcCommand();
                con.Open();
                cmdObj.Connection = con;
                cmdObj.CommandText = "SELECT MAX(MessageID) FROM message_schema";
                object messageID = cmdObj.ExecuteScalar();
                cmdObj.Dispose();
                con.Close();
                //For adding eBay message into Database.
                string insertMessageCommand = string.Empty;
                if (messageID != null)
                    insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'3')";
                OdbcCommand cmdFile = new OdbcCommand();
                con.Open();
                cmdFile.Connection = con;
                cmdFile.CommandText = insertMessageCommand;
                cmdFile.Parameters.Add(paramFile);
                cmdFile.ExecuteNonQuery();
                con.Close();
                mmsfileReader.Close();
                stream.Close();

                //For update LastUpdated ebay date.
                string commandDateText = " UPDATE trading_partner_schema SET LastUpdatedDate = '" + MessageDate + "' WHERE TradingPartnerID =" + tradingPartnerID + "";
                OdbcCommand commandDate = new OdbcCommand();
                commandDate.Connection = con;
                con.Open();
                commandDate.CommandText = commandDateText;
                commandDate.ExecuteNonQuery();
                commandDate.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }

        }

        private void backgroundWorkerOsCommerceService()
        {
            #region Checking Which Version is used

            OdbcCommand cmd = new OdbcCommand();
            OdbcConnection con = new OdbcConnection();
            MySqlConnection conn = new MySqlConnection();
            MySqlCommand cmmd = new MySqlCommand();
            DataSet orderDataSets = new DataSet();
            DataSet ds = null;
            DataSet dataset = null;

            con = new OdbcConnection();

            con.ConnectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");

            cmd = new OdbcCommand();
            cmd.CommandText = string.Format(EDI.Constant.Constants.GetDetailsByTPName, osCommerceTradingPartnerName);
            dataset = new DataSet();

            OdbcDataAdapter adapter = new OdbcDataAdapter(cmd.CommandText, con.ConnectionString);
            adapter.Fill(dataset, "trading_partner_schema");

            if (dataset != null)
            {
                dbHostName = dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                dbUserName = dataset.Tables[0].Rows[0]["UserName"].ToString();
                dbPassword = dataset.Tables[0].Rows[0]["Password"].ToString();
                if (dbPassword.Contains(";"))
                    dbPassword = "'" + dbPassword + "'";

                dbPort = dataset.Tables[0].Rows[0]["eBayAuthToken"].ToString();
                dbPrefix = dataset.Tables[0].Rows[0]["XSDLocation"].ToString();
                dbName = dataset.Tables[0].Rows[0]["OSCDatabase"].ToString();


                if (!string.IsNullOrEmpty(dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString()))
                {
                    sshHostName = dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString();
                    sshPortNo = dataset.Tables[0].Rows[0]["eBayDevId"].ToString();
                    sshUserName = dataset.Tables[0].Rows[0]["eBayCertID"].ToString();
                    sshPassword = dataset.Tables[0].Rows[0]["eBayAppId"].ToString();
                    if (sshPassword.Contains(";"))
                        sshPassword = "'" + sshPassword + "'";

                }
            }

            cmd.Dispose();
            con.Close();
            session = null;

            int version = 2;
            if (CheckOrderV2(dbHostName, dbUserName, dbPassword, dbName))
                version = 2;
            //else
            //    if (CheckOrderV3(dbHostName, dbUserName, dbPassword, dbName))
            //        version = 3;
            else
                return;

            #endregion

            StringBuilder sb = new StringBuilder();

            string orderQuery = string.Empty;

            orderQuery = "SELECT orders_id from " + dbPrefix.Trim() + "orders WHERE last_modified >= '" + osCommerceUpdatedDate.ToString("yyyy-MM-dd HH:mm:ss") + "' AND last_modified <= '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";

            try
            {

                if (!string.IsNullOrEmpty(sshHostName))
                {

                    conn = new MySqlConnection();
                    cmmd = new MySqlCommand();
                    //Set port forwarding on the opened session.
                    //session.disconnect();
                    //session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));


                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a ODBC Connection.

                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                            this.osCommerceConnectionString = conn.ConnectionString;
                            cmmd = new MySqlCommand();
                            cmmd.Connection = conn;
                            cmmd.CommandText = orderQuery;
                            conn.Open();


                            MySqlDataAdapter ordAdapter = new MySqlDataAdapter(cmmd);
                            orderDataSets = new DataSet();
                            ordAdapter.Fill(orderDataSets);
                            cmmd.Dispose();
                            conn.Close();

                        }
                        catch (SocketException ex)
                        {
                            CommonUtilities.WriteErrorLog("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message);
                            session.disconnect();
                            return;
                        }
                        catch { }
                    }
                }
                else
                {
                    conn = new MySqlConnection();
                    string connectString = string.Empty;
                    if (dbPort == string.Empty || dbPort == "3306")
                        connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    else
                        connectString = "Server=" + dbHostName + ";Port=" + dbPort + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                    conn.ConnectionString = connectString;
                    this.osCommerceConnectionString = conn.ConnectionString;
                    cmmd = new MySqlCommand();
                    cmmd.Connection = conn;
                    cmmd.CommandText = orderQuery;
                    conn.Open();


                    MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmmd);
                    orderDataSets = new DataSet();
                    orderAdapter.Fill(orderDataSets);
                    cmmd.Dispose();
                    conn.Close();
                }

                if (orderDataSets == null)
                {
                    //return string.Empty;
                }
                else
                {
                    if (orderDataSets.Tables[0].Rows.Count == 0)
                    {
                        //return string.Empty;

                    }
                }

                DataView orderDataSet = orderDataSets.Tables[0].DefaultView;
                orderDataSet.Sort = "orders_id";

                for (int count = 0; count < orderDataSet.Table.Rows.Count; count++)
                {

                    #region Process Order

                    string Query = string.Empty;
                    //if (version == 2)
                    //{
                    //Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldMain, orderDataSet.Table.Rows[count][0].ToString());
                    //Query = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.final_price,op.products_tax,op.products_quantity,op.products_model FROM " + dbPrefix.Trim() + "orders ordr LEFT OUTER join " + dbPrefix.Trim() + "orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join " + dbPrefix.Trim() + "products_description pd on op.products_id = pd.products_id LEFT OUTER join " + dbPrefix.Trim() + "orders_status_history osh on ordr.orders_id = osh.orders_id  WHERE pd.language_id=1 and osh.orders_status_id = 1 AND ordr.orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + "";
                    //Query = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.final_price,op.products_tax,op.products_quantity,op.products_model FROM " + dbPrefix.Trim() + "orders ordr LEFT OUTER join " + dbPrefix.Trim() + "orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join " + dbPrefix.Trim() + "products_description pd on op.products_id = pd.products_id LEFT OUTER join " + dbPrefix.Trim() + "orders_status_history osh on ordr.orders_id = osh.orders_id  WHERE pd.language_id=1 and osh.customer_notified= (SELECT MAX(customer_notified) FROM " + dbPrefix.Trim() + "orders_status_history where orders_status_id = ( select max(orders_status_id) from " + dbPrefix.Trim() + "orders_status_history where orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + ")) AND ordr.orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + "";
                    Query = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.final_price,op.products_tax,op.products_quantity,op.products_model FROM " + dbPrefix.Trim() + "orders ordr LEFT OUTER join " + dbPrefix.Trim() + "orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join " + dbPrefix.Trim() + "products_description pd on op.products_id = pd.products_id LEFT OUTER join " + dbPrefix.Trim() + "orders_status_history osh on ordr.orders_id = osh.orders_id  WHERE pd.language_id=1 and osh.orders_status_history_id = (SELECT MAX(orders_status_history_id) FROM " + dbPrefix.Trim() + "orders_status_history where orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + ") AND ordr.orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + "";

                    //}
                    //else
                    //{
                    //Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringMain, orderDataSet.Table.Rows[count][0].ToString());
                    //}
                    try
                    {
                        if (!string.IsNullOrEmpty(sshHostName))
                        {

                            conn = new MySqlConnection();
                            cmmd = new MySqlCommand();

                            //Set port forwarding on the opened session.
                            //session.disconnect();
                            //session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));

                            if (session.isConnected())
                            {
                                try
                                {
                                    //Create a ODBC Connection.

                                    conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                                    this.osCommerceConnectionString = conn.ConnectionString;
                                    cmmd = new MySqlCommand();
                                    cmmd.Connection = conn;
                                    cmmd.CommandText = Query;
                                    conn.Open();

                                    MySqlDataAdapter dataAdapter = new MySqlDataAdapter(cmmd);
                                    ds = new DataSet();
                                    dataAdapter.Fill(ds);
                                    cmmd.Dispose();
                                    conn.Close();
                                    //session.disconnect();
                                }
                                catch (SocketException ex)
                                {
                                    CommonUtilities.WriteErrorLog("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message);
                                    session.disconnect();
                                    return;
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            MySqlConnection orderCon = new MySqlConnection();
                            string connectString = string.Empty;
                            if (dbPort == string.Empty || dbPort == "3306")
                                connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                            else
                                connectString = "Server=" + dbHostName + ";Port=" + dbPort + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                            orderCon.ConnectionString = connectString;
                            this.osCommerceConnectionString = connectString;
                            MySqlCommand orderCmd = new MySqlCommand();
                            orderCmd.Connection = orderCon;
                            orderCmd.CommandText = Query;
                            orderCon.Open();

                            MySqlDataAdapter dtAdapter = new MySqlDataAdapter(orderCmd);
                            ds = new DataSet();
                            dtAdapter.Fill(ds);
                            orderCmd.Dispose();
                            orderCon.Close();
                        }
                        if (ds == null)
                        {
                            continue;
                        }
                        else
                        {
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                continue;
                            }
                            else
                            {
                                #region Store OSCommerce Order

                                string headerLine = string.Empty;
                                string[] middleLine = new string[ds.Tables[0].Rows.Count];
                                for (int temp = 0; temp < ds.Tables[0].Rows.Count; temp++)
                                {
                                    string[] shipInfo = new string[2];
                                    if (ds.Tables[0].Rows[temp][0].ToString() != string.Empty)
                                    {
                                        shipInfo = GetShippingDetails(this.osCommerceConnectionString, version, ds.Tables[0].Rows[temp][0].ToString());
                                        string total = GetTotal(this.osCommerceConnectionString, version, ds.Tables[0].Rows[temp][0].ToString());

                                        if (temp == ds.Tables[0].Rows.Count - 1)
                                        {
                                            #region Insert data into database

                                            //if (version == 2)
                                            //{
                                            middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                            middleLine[temp] += "" + ds.Tables[0].Rows[temp][16].ToString() + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}
                                            //else
                                            //{
                                            //   middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                            //   middleLine[temp] += "" + string.Empty + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}


                                            headerLine += "OSCommerce|" + osCommerceTradingPartnerName + "|" + ds.Tables[0].Rows[temp][0].ToString() + "\n";
                                            headerLine += "OSCommerce1|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][1].ToString() + "|" + ds.Tables[0].Rows[temp][2].ToString() + "|" + ds.Tables[0].Rows[temp][3].ToString() + "|" + ds.Tables[0].Rows[temp][4].ToString() + "|";
                                            headerLine += "" + ds.Tables[0].Rows[temp][5].ToString() + "|" + ds.Tables[0].Rows[temp][6].ToString() + "|" + ds.Tables[0].Rows[temp][7].ToString() + "|";
                                            headerLine += "" + ds.Tables[0].Rows[temp][8].ToString() + "|" + ds.Tables[0].Rows[temp][9].ToString() + "|" + ds.Tables[0].Rows[temp][10].ToString() + "|";
                                            if (shipInfo != null)
                                            {
                                                if (shipInfo[0] == null && shipInfo[1] == null)
                                                    headerLine += "" + string.Empty + "|" + string.Empty + "|" + ds.Tables[0].Rows[temp][11].ToString() + "|";
                                                else
                                                    headerLine += "" + (shipInfo == null ? string.Empty : shipInfo[1].ToString()) + "|" + (shipInfo == null ? string.Empty : shipInfo[0].ToString()) + "|" + ds.Tables[0].Rows[temp][11].ToString() + "|";
                                            }
                                            headerLine += "" + ds.Tables[0].Rows[temp][12].ToString() + "|" + ds.Tables[0].Rows[temp][13].ToString() + "\n";

                                            sb.AppendLine(headerLine);


                                            foreach (string item in middleLine)
                                            {
                                                if (item != null)
                                                {
                                                    if (item != string.Empty)
                                                    {
                                                        sb.AppendLine(item);
                                                    }
                                                }
                                            }

                                            Save(sb, ds.Tables[0].Rows[temp][0].ToString(), osCommerceTradingPartnerName);
                                            headerLine = string.Empty;
                                            sb = new StringBuilder();

                                            middleLine = new string[ds.Tables[0].Rows.Count];
                                            #endregion
                                        }
                                        else
                                        {
                                            //if (version == 2)
                                            //{
                                            middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                            middleLine[temp] += "" + ds.Tables[0].Rows[temp][16].ToString() + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}
                                            //else
                                            //{
                                            //   middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                            //  middleLine[temp] += "" + string.Empty + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}

                                        }
                                    }
                                }

                                #endregion
                            }
                        }
                        ds = null;

                    }
                    catch (Exception ex)
                    {
                        //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                        //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

                        CommonUtilities.WriteErrorLog(ex.Message);
                    }

                    #endregion

                }

                //return string.Empty;

            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                //return string.Empty;
            }
            finally
            {
                if (session != null)
                {
                    session.disconnect();
                }
            }

        }

        public class MyUserInfo : UserInfo
        {
            /// &lt;summary&gt;
            /// Holds the user password
            /// &lt;/summary&gt;
            private String passwd;

            /// &lt;summary&gt;
            /// Returns the user password
            /// &lt;/summary&gt;
            public String getPassword() { return passwd; }

            /// &lt;summary&gt;
            /// Prompt the user for a Yes/No input
            /// &lt;/summary&gt;
            public bool promptYesNo(String str)
            {
                return true;
            }

            /// &lt;summary&gt;
            /// Returns the user passphrase (passwd for the private key file)
            /// &lt;/summary&gt;
            public String getPassphrase() { return null; }

            /// &lt;summary&gt;
            /// Prompt the user for a passphrase (passwd for the private key file)
            /// &lt;/summary&gt;
            public bool promptPassphrase(String message) { return true; }

            /// &lt;summary&gt;
            /// Prompt the user for a password
            /// &lt;/summary&gt;
            public bool promptPassword(String message) { return true; }

            /// &lt;summary&gt;
            /// Shows a message to the user
            /// &lt;/summary&gt;
            public void showMessage(String message) { }

        }

    }
}

