﻿namespace DataProcessingBlocks
{
    partial class PointOfSaleConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PointOfSaleConnection));
            this.groupBoxCreateItems = new System.Windows.Forms.GroupBox();
            this.txtQBPOSVersion = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.txtComputer = new System.Windows.Forms.TextBox();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelCompany = new System.Windows.Forms.Label();
            this.labelComputer = new System.Windows.Forms.Label();
            this.checkBoxIsPractice = new System.Windows.Forms.CheckBox();
            this.LabelIsPractice = new System.Windows.Forms.Label();
            this.groupBoxCreateItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxCreateItems
            // 
            this.groupBoxCreateItems.Controls.Add(this.LabelIsPractice);
            this.groupBoxCreateItems.Controls.Add(this.checkBoxIsPractice);
            this.groupBoxCreateItems.Controls.Add(this.txtQBPOSVersion);
            this.groupBoxCreateItems.Controls.Add(this.buttonOK);
            this.groupBoxCreateItems.Controls.Add(this.txtCompany);
            this.groupBoxCreateItems.Controls.Add(this.txtComputer);
            this.groupBoxCreateItems.Controls.Add(this.labelVersion);
            this.groupBoxCreateItems.Controls.Add(this.labelCompany);
            this.groupBoxCreateItems.Controls.Add(this.labelComputer);
            this.groupBoxCreateItems.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxCreateItems.ForeColor = System.Drawing.Color.Black;
            this.groupBoxCreateItems.Location = new System.Drawing.Point(16, 14);
            this.groupBoxCreateItems.Name = "groupBoxCreateItems";
            this.groupBoxCreateItems.Size = new System.Drawing.Size(371, 212);
            this.groupBoxCreateItems.TabIndex = 1;
            this.groupBoxCreateItems.TabStop = false;
            this.groupBoxCreateItems.Text = "Connection String";
            // 
            // txtQBPOSVersion
            // 
            this.txtQBPOSVersion.Location = new System.Drawing.Point(127, 130);
            this.txtQBPOSVersion.Name = "txtQBPOSVersion";
            this.txtQBPOSVersion.Size = new System.Drawing.Size(75, 21);
            this.txtQBPOSVersion.TabIndex = 14;
            // 
            // buttonOK
            // 
            this.buttonOK.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.Location = new System.Drawing.Point(127, 158);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 13;
            this.buttonOK.Text = "Connect";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(127, 69);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(190, 21);
            this.txtCompany.TabIndex = 12;
            this.txtCompany.TextChanged += new System.EventHandler(this.txtCompany_TextChanged);
            // 
            // txtComputer
            // 
            this.txtComputer.Location = new System.Drawing.Point(127, 31);
            this.txtComputer.Name = "txtComputer";
            this.txtComputer.Size = new System.Drawing.Size(190, 21);
            this.txtComputer.TabIndex = 11;
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(11, 130);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(104, 13);
            this.labelVersion.TabIndex = 2;
            this.labelVersion.Text = "QBPOS Version :";
            // 
            // labelCompany
            // 
            this.labelCompany.AutoSize = true;
            this.labelCompany.Location = new System.Drawing.Point(11, 72);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(116, 13);
            this.labelCompany.TabIndex = 1;
            this.labelCompany.Text = "QBPOS Company :";
            // 
            // labelComputer
            // 
            this.labelComputer.AutoSize = true;
            this.labelComputer.Location = new System.Drawing.Point(11, 34);
            this.labelComputer.Name = "labelComputer";
            this.labelComputer.Size = new System.Drawing.Size(110, 13);
            this.labelComputer.TabIndex = 0;
            this.labelComputer.Text = "Computer Name :";
            // 
            // checkBoxIsPractice
            // 
            this.checkBoxIsPractice.AutoSize = true;
            this.checkBoxIsPractice.Location = new System.Drawing.Point(127, 100);
            this.checkBoxIsPractice.Name = "checkBoxIsPractice";
            this.checkBoxIsPractice.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsPractice.TabIndex = 15;
            this.checkBoxIsPractice.UseVisualStyleBackColor = true;
            this.checkBoxIsPractice.CheckedChanged += new System.EventHandler(this.checkBoxIsPractice_CheckedChanged);
            // 
            // LabelIsPractice
            // 
            this.LabelIsPractice.AutoSize = true;
            this.LabelIsPractice.Location = new System.Drawing.Point(11, 100);
            this.LabelIsPractice.Name = "LabelIsPractice";
            this.LabelIsPractice.Size = new System.Drawing.Size(76, 13);
            this.LabelIsPractice.TabIndex = 16;
            this.LabelIsPractice.Text = "Is Practice :";
            // 
            // PointOfSaleConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(402, 240);
            this.Controls.Add(this.groupBoxCreateItems);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PointOfSaleConnection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Point Of Sale Connection";
            this.Load += new System.EventHandler(this.PointOfSaleConnection_Load);
            this.groupBoxCreateItems.ResumeLayout(false);
            this.groupBoxCreateItems.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxCreateItems;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.Label labelComputer;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.TextBox txtComputer;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.TextBox txtQBPOSVersion;
        private System.Windows.Forms.Label LabelIsPractice;
        private System.Windows.Forms.CheckBox checkBoxIsPractice;
    }
}