using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataProcessingBlocks;
using EDI.Constant;
using System.Collections.Specialized;

namespace DataProcessingBlocks
{
    public partial class QuickBookAccountSettings : Form
    {

        #region Private Members
        private static QuickBookAccountSettings m_options;
        private bool m_optionFlag;
        private static string m_QBFileName;
        QBTaxCodeListCollection QBTaxCodeListColl;
        QBAccountQueryListCollection QBIncomeAccountQueryListColl;
        QBAccountQueryListCollection QBCOGSAccountQueryListColl;
        QBAccountQueryListCollection QBAssetAccountQueryListColl;
      
        public bool test;
		// Axis 10.0
		QBAccountQueryListCollection QBExpenseAccountQueryListColl;
        #endregion

        #region Properties

      


        #endregion

        #region Static Methods

        /// <summary>
        /// This method is used to get static instance.
        /// </summary>
        /// <returns></returns>
        public static QuickBookAccountSettings GetInstance()
        {
            if (m_options == null)
                m_options = new QuickBookAccountSettings();
            return m_options;
        }
        #endregion
       
        #region Constructor
        public QuickBookAccountSettings()
        {
            InitializeComponent();            
        }
        #endregion

        #region Properties
        public bool OptionFlag
        {
            get { return this.m_optionFlag; }
            set { this.m_optionFlag = value; }
        }

        /// <summary>
        /// This property is set to get QuickBook FileName.
        /// </summary>
        public static string QBFileName
        {
            get { return m_QBFileName; }
            set { m_QBFileName = value; }
        }
        #endregion

        #region Private Methods
        private void PopulateForm()
        {
            DefaultAccountSettings m_DefaultSetting = new DefaultAccountSettings();
            m_DefaultSetting = m_DefaultSetting.GetDefaultAccountSettings();

            //Populate TaxCode DDL
            QBTaxCodeListColl = new QBTaxCodeListCollection();
            //Required try catch block for first method to check whether QuickBook is busy.
            try
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    QBTaxCodeListColl.PopulateTaxCodeList(m_QBFileName);
                }
                if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
                {
                    QBTaxCodeListColl.PopulateTaxCodeListOnline();
                }

                    comboBoxTaxCode.DataSource = QBTaxCodeListColl;
                    comboBoxTaxCode.DisplayMember = "Name";
                    if (m_DefaultSetting != null)
                    {
                        //Assining selected tax code.
                        if (m_DefaultSetting.TaxCode != null)
                        {
                            if (m_DefaultSetting.TaxCode != string.Empty)
                            {
                                try
                                {
                                    int indexCount = 0;
                                    for (int temp = 0; temp < comboBoxTaxCode.Items.Count; temp++)
                                    {
                                        if (m_DefaultSetting.TaxCode == QBTaxCodeListColl[temp].Name.ToString())
                                        {
                                            indexCount = temp;
                                            break;
                                        }
                                    }
                                    comboBoxTaxCode.SelectedIndex = indexCount;
                                }
                                catch { }
                            }

                        }
                    }

                    //for types of item (Service, Inventory , NonInventory) 

                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                    {
                        comboBoxType.DataSource = Enum.GetValues(typeof(DataProcessingBlocks.AccountType));
                    }
                    if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
                    {
                        comboBoxType.DataSource = Enum.GetValues(typeof(DataProcessingBlocks.OnlineAccountType));  
                    }                    
                   
                    //Assining selected Income Account.
                    if (m_DefaultSetting != null)
                    {
                        if (m_DefaultSetting.Type != null)
                        {
                            if (m_DefaultSetting.Type != string.Empty)
                            {
                                try
                                {
                                    if (m_DefaultSetting.Type.ToString() == "InventoryPart")
                                    {
                                        comboBoxAssetAccount.Enabled = true;
                                        comboBoxCOGSAccount.Enabled = true;

                                    }
                                    int indexCount = 0;
                                    for (int temp = 0; temp < comboBoxType.Items.Count; temp++)
                                    {
                                        if (m_DefaultSetting.Type == comboBoxType.Items[temp].ToString())
                                        {
                                            indexCount = temp;
                                            break;
                                        }
                                    }
                                    comboBoxType.SelectedIndex = indexCount;
                                }
                                catch { }
                                //m_DefaultSetting = null;
                            }
                        }
                    }              
               
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message, "Zed Axis");
                this.Close();
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Zed Axis");
                this.Close();
                return;
            }
            //Populate Income Account DDL
            QBIncomeAccountQueryListColl = new QBAccountQueryListCollection();
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                QBIncomeAccountQueryListColl.PopulateIncomeAccountQuery(m_QBFileName);
            }
            if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
            {
                QBIncomeAccountQueryListColl.PopulateIncomeAccountOnline();
            }
           
            comboBoxIncomeAccount.DataSource = QBIncomeAccountQueryListColl;
            comboBoxIncomeAccount.DisplayMember = "FullName";
            if (m_DefaultSetting != null)
            {
                //Assining selected Income Account.
                if (m_DefaultSetting.IncomeAccount != null)
                {
                    if (m_DefaultSetting.IncomeAccount != string.Empty)
                    {
                        try
                        {
                            int indexCount = 0;
                            for (int temp = 0; temp < comboBoxIncomeAccount.Items.Count; temp++)
                            {
                                if (m_DefaultSetting.IncomeAccount == QBIncomeAccountQueryListColl[temp].FullName.ToString())
                                {
                                    indexCount = temp;
                                    break;
                                }
                            }
                            comboBoxIncomeAccount.SelectedIndex = indexCount;
                        }
                        catch
                        { }
                    }
                }
            }
            //Populate COGS Account DDL
            QBCOGSAccountQueryListColl = new QBAccountQueryListCollection();
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                QBCOGSAccountQueryListColl.PopulateCOGSAccountQuery(m_QBFileName);
            }

            if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
            {
                QBCOGSAccountQueryListColl.PopulateCOGSAccountOnline();
            }

                
                foreach (object obj in QBCOGSAccountQueryListColl)
                {
                    comboBoxCOGSAccount.Items.Add(obj);
                }
                // Axis 10.0 changes end
                comboBoxCOGSAccount.DisplayMember = "FullName";

                if (m_DefaultSetting != null)
                {
                    //Assining selected COGS Account.
                    if (m_DefaultSetting.COGSAccount != null)
                    {
                        if (m_DefaultSetting.COGSAccount != string.Empty)
                        {
                            try
                            {
                                int indexCount = 0;
                                for (int temp = 0; temp < comboBoxCOGSAccount.Items.Count; temp++)
                                {
                                    if (m_DefaultSetting.COGSAccount == QBCOGSAccountQueryListColl[temp].FullName.ToString())
                                    {
                                        indexCount = temp;
                                        break;
                                    }
                                }
                                comboBoxCOGSAccount.SelectedIndex = indexCount;
                            }
                            catch { }
                        }
                    }
                }
            

                //Populate Expense Account DDL
                QBExpenseAccountQueryListColl = new QBAccountQueryListCollection();
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    QBExpenseAccountQueryListColl.PopulateExpenseAccountQuery(m_QBFileName);
                }
                if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
                {
                    QBExpenseAccountQueryListColl.PopulateExpenseAccountOnline();
                }

                

                //Populate Asset Account DDl
                QBAssetAccountQueryListColl = new QBAccountQueryListCollection();
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    QBAssetAccountQueryListColl.PopulateAssetAccountQuery(m_QBFileName);
                }
                if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
                { 
                    QBAssetAccountQueryListColl.PopulateAssetAccountOnline();
                }
                comboBoxAssetAccount.DataSource = QBAssetAccountQueryListColl;
                comboBoxAssetAccount.DisplayMember = "FullName";

                if (m_DefaultSetting != null)
                {
                    //Assining selected Asset Account.
                    if (m_DefaultSetting.AssetAccount != null)
                    {
                        if (m_DefaultSetting.AssetAccount != string.Empty)
                        {
                            try
                            {
                                int indexCount = 0;
                                for (int temp = 0; temp < comboBoxAssetAccount.Items.Count; temp++)
                                {
                                    if (m_DefaultSetting.AssetAccount == QBAssetAccountQueryListColl[temp].FullName.ToString())
                                    {
                                        indexCount = temp;
                                        break;
                                    }
                                }
                                comboBoxAssetAccount.SelectedIndex = indexCount;
                            }
                            catch { }
                        }
                    }
                }

                //Populate Freight fields (Service and Other charge items)
                QBAssetAccountQueryListColl = new QBAccountQueryListCollection();
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {                 
                    QBAssetAccountQueryListColl.PopulateServiceAndOtherChargeItemType(m_QBFileName);
                }
                if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
                {
                    QBAssetAccountQueryListColl.PopulateFrieghtOnline();
                }
                comboBoxFreight.DataSource = QBAssetAccountQueryListColl;
                comboBoxFreight.DisplayMember = "FullName";
                comboBoxFreight.SelectedIndex = -1;

                if (m_DefaultSetting != null)
                {
                    //Assining selected Asset Account.
                    if (m_DefaultSetting.Frieght != null)
                    {
                        if (m_DefaultSetting.Frieght != string.Empty)
                        {
                            try
                            {
                                int indexCount = 0;
                                for (int temp = 0; temp < comboBoxFreight.Items.Count; temp++)
                                {
                                    if (m_DefaultSetting.Frieght == QBAssetAccountQueryListColl[temp].FullName.ToString())
                                    {
                                        indexCount = temp;
                                        break;
                                    }
                                }
                                comboBoxFreight.SelectedIndex = indexCount;
                            }
                            catch { }
                        }
                    }
                }

                //Populate Insurance fields (Service and Other charge items)
                QBAssetAccountQueryListColl = new QBAccountQueryListCollection();
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    QBAssetAccountQueryListColl.PopulateServiceAndOtherChargeItemType(m_QBFileName);
                }
                if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
                {
                    QBAssetAccountQueryListColl.PopulateFrieghtOnline();
                }
                comboBoxInsurance.DataSource = QBAssetAccountQueryListColl;
                comboBoxInsurance.DisplayMember = "FullName";
                comboBoxInsurance.SelectedIndex = -1;

                if (m_DefaultSetting != null)
                {
                    //Assining selected Asset Account.
                    if (m_DefaultSetting.Insurance != null)
                    {
                        if (m_DefaultSetting.Insurance != string.Empty)
                        {
                            try
                            {
                                int indexCount = 0;
                                for (int temp = 0; temp < comboBoxInsurance.Items.Count; temp++)
                                {
                                    if (m_DefaultSetting.Insurance == QBAssetAccountQueryListColl[temp].FullName.ToString())
                                    {
                                        indexCount = temp;
                                        break;
                                    }
                                }
                                comboBoxInsurance.SelectedIndex = indexCount;
                            }
                            catch { }
                        }
                    }
                }

                //Populate Discount fields
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    QBAssetAccountQueryListColl = new QBAccountQueryListCollection();
                    QBAssetAccountQueryListColl.PopulateDiscountItemType(QBFileName);
                    comboBoxDiscount.DataSource = QBAssetAccountQueryListColl;
                    comboBoxDiscount.DisplayMember = "FullName";
                    comboBoxDiscount.SelectedIndex = -1;

                    if (m_DefaultSetting != null)
                    {
                        //Assining selected Asset Account.
                        if (m_DefaultSetting.Discount != null)
                        {
                            if (m_DefaultSetting.Discount != string.Empty)
                            {
                                try
                                {
                                    int indexCount = 0;
                                    for (int temp = 0; temp < comboBoxDiscount.Items.Count; temp++)
                                    {
                                        if (m_DefaultSetting.Discount == QBAssetAccountQueryListColl[temp].FullName.ToString())
                                        {
                                            indexCount = temp;
                                            break;
                                        }
                                    }
                                    comboBoxDiscount.SelectedIndex = indexCount;
                                }
                                catch { }
                            }
                        }
                    }
                }

                //Populate SalesTax fields-bug no. 410
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    QBAssetAccountQueryListColl = new QBAccountQueryListCollection();
                    QBAssetAccountQueryListColl.PopulateSalesTaxItemType(QBFileName);
                    comboBoxSalesTax.DataSource = QBAssetAccountQueryListColl;
                    comboBoxSalesTax.DisplayMember = "FullName";
                    comboBoxSalesTax.SelectedIndex = -1;

                    if (m_DefaultSetting != null)
                    {
                        //Assining selected Asset Account.
                        if (m_DefaultSetting.SalesTax != null)
                        {
                            if (m_DefaultSetting.SalesTax != string.Empty)
                            {
                                try
                                {
                                    int indexCount = 0;
                                    for (int temp = 0; temp < comboBoxSalesTax.Items.Count; temp++)
                                    {
                                        if (m_DefaultSetting.SalesTax == QBAssetAccountQueryListColl[temp].FullName.ToString())
                                        {
                                            indexCount = temp;
                                            break;
                                        }
                                    }
                                    comboBoxSalesTax.SelectedIndex = indexCount;
                                }
                                catch { }
                            }
                        }
                    }
                }
            

            m_DefaultSetting = null;

        }
                
        #endregion

        #region Event Handlers
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //Bring comboBox to its initial index.
            if (comboBoxType.HasChildren == true)
            {
                comboBoxType.SelectedIndex = 0;
            }
            m_options = null;
                       
            this.Close();
        }
          
        private void Options_Load(object sender, EventArgs e)
        {
            DataProcessingBlocks.UserMappings importOptions = new UserMappings();
            NameValueCollection attributes = importOptions.getImportOption();
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                comboBoxType.DataSource = Enum.GetValues(typeof(DataProcessingBlocks.AccountType));
                comboBoxDiscount.Enabled = true;
                comboBoxSalesTax.Enabled = true;

                //486
                checkBoxSKUlookUp.Enabled = false;
            }
            if(TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
            {
                comboBoxType.DataSource = Enum.GetValues(typeof(DataProcessingBlocks.OnlineAccountType));
                comboBoxDiscount.Enabled = false;
                comboBoxSalesTax.Enabled = false;

                //486
                checkBoxSKUlookUp.Enabled = true;

            }

           
            if (attributes.Count == 7)//Import options tag contains 7 attributes
            {
                #region Axis 11.0 -125 Get import options
                for (int i = 4; i < attributes.Count; i++)
                {
                    string key = attributes.GetKey(i);
                    string value = attributes.Get(i);
                    switch (key)
                    {
                        case "GrossNet":
                            if (value == "0")
                            {
                                checkBoxGrossToNet.Checked = false;
                            }
                            else
                            {
                                checkBoxGrossToNet.Checked = true;
                            }
                            break;
                       
                        case "VendorLookup":
                            if (value == "0")
                            {
                                checkBoxPartNumberLookup.Checked = false;
                            }
                            else
                            {
                                checkBoxPartNumberLookup.Checked = true;
                            }
                            break;
                        case "ALULookup":
                            if (value == "0")
                            {
                                checkBoxALULookup.Checked = false;
                            }
                            else
                            {
                                checkBoxALULookup.Checked = true;
                            }
                            break;
                    }
                }
                #endregion
            }
            else if (!this.OptionFlag)
            {
               
                DefaultAccountSettings m_DefaultSetting = new DefaultAccountSettings();
                m_DefaultSetting = m_DefaultSetting.GetDefaultAccountSettings();
                //Assining selected Income Account.
                if (m_DefaultSetting != null)
                {
                    if (m_DefaultSetting.Type != null)
                    {
                        if (m_DefaultSetting.Type != string.Empty)
                        {
                            try
                            {
                                int indexCount = 0;
                                for (int temp = 0; temp < comboBoxType.Items.Count; temp++)
                                {
                                    if (m_DefaultSetting.Type == comboBoxType.Items[temp].ToString())
                                    {
                                        indexCount = temp;
                                        break;
                                    }
                                }
                                comboBoxType.SelectedIndex = indexCount;
                            }
                            catch { }
                            //m_DefaultSetting = null;
                        }
                    }
                }

                if (m_DefaultSetting != null)
                {
                    if (m_DefaultSetting.GrossToNet != null)
                    {
                        if (m_DefaultSetting.GrossToNet != string.Empty)
                        {
                            if (m_DefaultSetting.GrossToNet == "1")
                            {
                                checkBoxGrossToNet.Checked = true;
                            }
                            else
                            {
                                checkBoxGrossToNet.Checked = false;
                            }
                           // m_DefaultSetting = null;
                        }
                    }
                }
                if (m_DefaultSetting != null)
                {
                    if (m_DefaultSetting.VendorLookup != null)
                    {
                        if (m_DefaultSetting.VendorLookup != string.Empty)
                        {
                            if (m_DefaultSetting.VendorLookup == "1")
                            {
                                checkBoxPartNumberLookup.Checked = true;
                            }
                            else
                            {
                                checkBoxPartNumberLookup.Checked = false;
                            }
                            m_DefaultSetting = null;
                        }
                    }
                }
                if (m_DefaultSetting != null)
                {
                    if (m_DefaultSetting.ALULookup != null)
                    {
                        if (m_DefaultSetting.ALULookup != string.Empty)
                        {
                            if (m_DefaultSetting.ALULookup == "1")
                            {
                                checkBoxALULookup.Checked = true;
                            }
                            else
                            {
                                checkBoxALULookup.Checked = false;
                            }
                            m_DefaultSetting = null;
                        }
                    }

                }
                //Axis-565
                if (m_DefaultSetting != null)
                {
                    if (m_DefaultSetting.UPCLookup != null)
                    {
                        if (m_DefaultSetting.UPCLookup != string.Empty)
                        {
                            if (m_DefaultSetting.UPCLookup == "1")
                            {
                                checkBoxUPCLookup.Checked = true;
                            }
                            else
                            {
                                checkBoxUPCLookup.Checked = false;
                            }
                            m_DefaultSetting = null;
                        }
                    }
                }
            }

            //Axis 8.0
            #region Retain Vendor Part Number Lookup

            if (DBConnection.MySqlDataAcess.GetVendorPartNumberLookupStatus(Constants.QBstring))
            {
                checkBoxPartNumberLookup.Checked = true;
            }
            //416
            if (DBConnection.MySqlDataAcess.GetALULookupStatus(Constants.QBPOSstring))
            {
                checkBoxALULookup.Checked = true;
            }

            //Axis-565
            if (DBConnection.MySqlDataAcess.GetUPCLookupStatus(Constants.QBPOSstring))
            {
                checkBoxUPCLookup.Checked = true;
            }
            //Axis-565 end

            //Axis 12.0 486
            if (CommonUtilities.GetInstance().ConnectedSoftware == Constants.QBOnlinestring)
            {
                if (DBConnection.MySqlDataAcess.GetSKULookupStatus(Constants.QBstring))
                {
                    checkBoxSKUlookUp.Checked = true;
                }
                if (DBConnection.MySqlDataAcess.GetGrossToNetStatus(Constants.QBstring))
                {
                    checkBoxGrossToNet.Checked = true;
                }
            }


            #endregion
  
            PopulateForm();
            if (CommonUtilities.GetInstance().ConnectedSoftware == Constants.QBPOSstring)
            {
                comboBoxType.Enabled = false;
                comboBoxTaxCode.Enabled = false;
                comboBoxIncomeAccount.Enabled = false;
                comboBoxCOGSAccount.Enabled = false;
                comboBoxAssetAccount.Enabled = false;
                comboBoxFreight.Enabled = false;
                comboBoxInsurance.Enabled = false;
                comboBoxDiscount.Enabled = false;
                comboBoxSalesTax.Enabled = false;
                checkBoxGrossToNet.Enabled = false;
                checkBoxPartNumberLookup.Enabled = false;
                checkBoxALULookup.Enabled = true;

                //486 axis 12.0
                checkBoxSKUlookUp.Enabled = false;

            }
            else
            {
                comboBoxType.Enabled = true;
                comboBoxTaxCode.Enabled = true;
                comboBoxIncomeAccount.Enabled = true;
				//bug 491 axis 12.0
                if (CommonUtilities.GetInstance().ConnectedSoftware == Constants.QBstring)
                {
                    if (comboBoxType.SelectedValue.ToString() == "InventoryPart")
                    {
                        comboBoxCOGSAccount.Enabled = true;
                        comboBoxAssetAccount.Enabled = true;
                    }
                    if (comboBoxType.SelectedValue.ToString() == "NonInventoryPart")
                    {
                        comboBoxCOGSAccount.Enabled = true;
                        comboBoxAssetAccount.Enabled = false;

                    }
                    if (comboBoxType.SelectedValue.ToString() == "Service")
                    {
                        comboBoxCOGSAccount.Enabled = false;
                        comboBoxAssetAccount.Enabled = false;
                    }
                }

                if (CommonUtilities.GetInstance().ConnectedSoftware == Constants.QBOnlinestring)
                {
                    if (comboBoxType.SelectedValue.ToString() == "NonInventory")
                    {
                        comboBoxCOGSAccount.Enabled = true;
                        comboBoxAssetAccount.Enabled = false;

                    }
                    if (comboBoxType.SelectedValue.ToString() == "Inventory")
                    {
                        comboBoxCOGSAccount.Enabled = true;
                        comboBoxAssetAccount.Enabled = true;

                    }
                    if (comboBoxType.SelectedValue.ToString() == "Service")
                    {
                        comboBoxCOGSAccount.Enabled = true;
                        comboBoxAssetAccount.Enabled = false;
                    }
                }
				
                comboBoxFreight.Enabled = true;
                comboBoxInsurance.Enabled = true;
                comboBoxDiscount.Enabled = true;
                comboBoxSalesTax.Enabled = true;
                checkBoxGrossToNet.Enabled = true;
                checkBoxPartNumberLookup.Enabled = true;
                checkBoxALULookup.Enabled = false;
            }

 

        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().ItemType = comboBoxType.SelectedItem.ToString();
            if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
            {
                if (comboBoxType.SelectedItem.ToString() == "Inventory")
                {
                    QBIncomeAccountQueryListColl = new QBAccountQueryListCollection();
                    QBIncomeAccountQueryListColl.PopulateIncomeAccountOnline();
                    comboBoxCOGSAccount.Enabled = true;
                    comboBoxAssetAccount.Enabled = true;
                }
                if (comboBoxType.SelectedItem.ToString() == "NonInventory")
                {
                    QBAssetAccountQueryListColl = new QBAccountQueryListCollection();
                    QBAssetAccountQueryListColl.PopulateFrieghtOnline();
                    //bug 491
                    comboBoxCOGSAccount.Enabled = true;
                    comboBoxAssetAccount.Enabled = false;
                }

                if (comboBoxType.SelectedItem.ToString() == "Service")
                {
                    comboBoxCOGSAccount.Enabled = false;
                    comboBoxAssetAccount.Enabled = false;
                }
            }
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                if (comboBoxType.SelectedItem.ToString() == EDI.Constant.Constants.Inventory)
                {
                    comboBoxCOGSAccount.Enabled = true;
                    comboBoxAssetAccount.Enabled = true;

                }
                if (comboBoxType.SelectedItem.ToString() == EDI.Constant.Constants.NonInventory)
                {
                    comboBoxCOGSAccount.Enabled = true;
                    comboBoxAssetAccount.Enabled = false;
                }
                if (comboBoxType.SelectedItem.ToString() == EDI.Constant.Constants.Service)
                {
                    comboBoxTaxCode.Enabled = true;
                    comboBoxIncomeAccount.Enabled = true;

                    comboBoxCOGSAccount.Enabled = false;
                    comboBoxAssetAccount.Enabled = false;
                }
            }
        }
                        
        private void QuickBookAccountSettings_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_options = null;
        }
        
        private void buttonOK_Click(object sender, EventArgs e)
        {
            DefaultAccountSettings newSettings = new DefaultAccountSettings();
            NameValueCollection attribute = new NameValueCollection();
          
			bool flag = false;
            bool ALUflag = false;
            bool SKUflag = false;
            bool grossflag = false;
            //Axis-565
            bool UPCflag = false;

            //Set filename
            newSettings.File = QBFileName;

            if (comboBoxType.SelectedIndex != -1)
            {
                newSettings.Type = comboBoxType.SelectedItem.ToString();
            }

          
            if (TransactionImporter.TransactionImporter.rdbQBOnlinebutton == true)
            {
                if (comboBoxType.SelectedItem.ToString() == "NonInventory")
                {
                    //QBAssetAccountQueryListColl = new QBAccountQueryListCollection();
                    //QBAssetAccountQueryListColl.PopulateFrieghtOnline();

                    //bug 491
                    comboBoxCOGSAccount.Enabled = true;
                }
            }


            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                if (comboBoxType.SelectedItem.ToString() == EDI.Constant.Constants.Inventory)
                {
                    comboBoxCOGSAccount.Enabled = true;
                    comboBoxAssetAccount.Enabled = true;

                }
                else
                {
                    comboBoxCOGSAccount.Enabled = false;
                    comboBoxAssetAccount.Enabled = false;
                }
            }

            if (comboBoxTaxCode.SelectedIndex != -1)
            {
                newSettings.TaxCode = ((DataProcessingBlocks.QBTaxCodeList)(comboBoxTaxCode.SelectedItem)).Name;
            }
            if (comboBoxIncomeAccount.SelectedIndex != -1)
            {
                newSettings.IncomeAccount = ((DataProcessingBlocks.QBAccountQueryList)(comboBoxIncomeAccount.SelectedItem)).FullName;
            }
            //Check whether comboboxCOGSAccount is Enabled.
            if (comboBoxCOGSAccount.Enabled == true)
            {
                if (comboBoxCOGSAccount.SelectedIndex != -1)
                {
                    newSettings.COGSAccount = ((DataProcessingBlocks.QBAccountQueryList)(comboBoxCOGSAccount.SelectedItem)).FullName;
                }
            }
            if (comboBoxAssetAccount.Enabled == true)
            {
                if (comboBoxAssetAccount.SelectedIndex != -1)
                {
                    newSettings.AssetAccount = ((DataProcessingBlocks.QBAccountQueryList)(comboBoxAssetAccount.SelectedItem)).FullName;
                }
            }
            //if (checkBoxGrossToNet.Checked)
            //{
            //    newSettings.GrossToNet = "1";
            //    attribute.Add("GrossNet", "1");
            //}
            //else
            //{
            //    newSettings.GrossToNet = "0";
            //    attribute.Add("GrossNet", "0");
            //}
            //416
            if (checkBoxALULookup.Checked)
            {
                ALUflag = true;
                UPCflag = false;
                newSettings.ALULookup = "1";
                attribute.Add("ALULookup", "1");
            }
            else
            {
                newSettings.ALULookup = "0";
                attribute.Add("ALULookup", "0");
            }

            //Axis-565
            if (checkBoxUPCLookup.Checked)
            {
                UPCflag = true;
                ALUflag = false;
                newSettings.UPCLookup = "1";
                attribute.Add("UPCLookup", "1");
            }
            else
            {
                newSettings.UPCLookup = "0";
                attribute.Add("UPCLookup", "0");
            }
            //Axis-565 end

            //bug 486 axis 12.0
             if (checkBoxSKUlookUp.Checked)
            {
                 SKUflag = true;
                newSettings.SKULookup = "1";
                attribute.Add("SKULookup", "1");
            }
            else
            {
                newSettings.SKULookup = "0";
                attribute.Add("SKULookup", "0");
            }


             //bug 286 axis 12.0
             if (checkBoxGrossToNet.Checked)
             {
                 grossflag = true;
                 newSettings.GrossToNet = "1";
                 attribute.Add("GrossToNet", "1");
             }
             else
             {
                 newSettings.GrossToNet = "0";
                 attribute.Add("GrossToNet", "0");
             }

            //Save fright, Insurance, Discount
            if (comboBoxFreight.SelectedIndex != -1)
            {
                newSettings.Frieght = ((DataProcessingBlocks.QBAccountQueryList)(comboBoxFreight.SelectedItem)).FullName;
            }
            if (comboBoxInsurance.SelectedIndex != -1)
            {
                newSettings.Insurance = ((DataProcessingBlocks.QBAccountQueryList)(comboBoxInsurance.SelectedItem)).FullName;             
            }
            if (comboBoxDiscount.SelectedIndex != -1)
            {
                newSettings.Discount = ((DataProcessingBlocks.QBAccountQueryList)(comboBoxDiscount.SelectedItem)).FullName;                         
            }
            //Save Sales Tax- bug no. 410
            if (comboBoxSalesTax.SelectedIndex != -1)
            {
                newSettings.SalesTax=((DataProcessingBlocks.QBAccountQueryList)(comboBoxSalesTax.SelectedItem)).FullName;    
            }
            //newSettings.save();            

            //#region Axis 8.0 Vendor Part Number Lookup

            if (checkBoxPartNumberLookup.Checked)
            {
                flag = true;
                newSettings.VendorLookup = "1";
                attribute.Add("VendorLookup", "1");
            }
            else
            {
                newSettings.VendorLookup = "0";
                attribute.Add("VendorLookup", "0");
            }
            
            newSettings.save();            

            #region Axis 8.0 Vendor Part Number Lookup

           
            DBConnection.MySqlDataAcess.SetVendorPartNumberLookupStatus(Constants.QBstring, flag);
            //416 change conn str
            DBConnection.MySqlDataAcess.SetALULookupStatus(Constants.QBPOSstring, ALUflag);
            //Axis-565
            DBConnection.MySqlDataAcess.SetUPCLookupStatus(Constants.QBPOSstring, UPCflag);
            //Axis-565 end
            //bug 286
            DBConnection.MySqlDataAcess.SetGrossToNetStatus(Constants.QBOnlinestring, grossflag);

            DBConnection.MySqlDataAcess.SetSKULookupStatus(Constants.QBPOSstring, SKUflag);


            #endregion

            #region Axis 11.0 -125 Save More options to selected mapping
            //Save in commomUtilities
            CommonUtilities.GetInstance().GrossNet = checkBoxGrossToNet.Checked;
            CommonUtilities.GetInstance().VendorLookup = checkBoxPartNumberLookup.Checked;

            CommonUtilities.GetInstance().ALULookup = checkBoxALULookup.Checked;

            //Axis-565
            CommonUtilities.GetInstance().UPCLookup = checkBoxUPCLookup.Checked;
            //Axis-565 end
            CommonUtilities.GetInstance().SKULookup = checkBoxSKUlookUp.Checked;


            CommonUtilities.GetInstance().GrossNet = checkBoxGrossToNet.Checked;

            DataProcessingBlocks.UserMappings importOptions = new UserMappings();
            importOptions.setImportOption(attribute);
            #endregion       

            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG097"),DataProcessingBlocks.MessageCodes.GetValue("ProductName"),MessageBoxButtons.OK,MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
       
        private void chkListValidate_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        #endregion

        private void checkBoxGrossToNet_CheckedChanged(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().GrossNet = checkBoxGrossToNet.Checked;
        }

        private void checkBoxPartNumberLookup_CheckedChanged(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().VendorLookup = checkBoxPartNumberLookup.Checked;
        }

        private void checkBoxALULookup_CheckedChanged(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().ALULookup = checkBoxALULookup.Checked;
        }

        private void checkBoxSKUlookUp_CheckedChanged(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().SKULookup = checkBoxSKUlookUp.Checked;
        }

        //Axis-565
        private void checkBoxUPCLookup_CheckedChanged(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().UPCLookup = checkBoxUPCLookup.Checked;
        }

        private void checkBoxUPCLookup_Click(object sender, EventArgs e)
        {
            checkBoxALULookup.Checked = false;
        }

        private void checkBoxALULookup_Click(object sender, EventArgs e)
        {
            checkBoxUPCLookup.Checked = false;
        }
        //Axis-565 end
    }
}