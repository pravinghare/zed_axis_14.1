using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using EDI.Constant;
using EDI.Message;
using DataProcessingBlocks;

namespace MailConfigurations
{
    public partial class MailConfigurationSettings : Form
    {
        private static MailConfigurationSettings m_MailConfigurationSettings;
        private string ProductCaption = EDI.Constant.Constants.ProductName;
        public static MailConfigurationSettings GetInstance()
        {
            if (m_MailConfigurationSettings == null)
                m_MailConfigurationSettings = new MailConfigurationSettings();
            return m_MailConfigurationSettings;
        }
        public MailConfigurationSettings()
        {
            InitializeComponent();
            this.comboBoxMailProtocol.DataSource = MailSettings.TradingPartner.GetMailProtocol();
            this.comboBoxMailProtocol.DisplayMember = DataProcessingBlocks.MailProtocolColumns.MailProtocol.ToString();
            this.comboBoxMailProtocol.ValueMember = DataProcessingBlocks.MailProtocolColumns.MailProtocol.ToString();
            populateControls();
        }

        private void populateControls()
        {
            using (DataSet dataSet = MailSettings.MailSetup.GetMailSetupDetails())
            {
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {

                    int protocolID = Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.MailProtocolID.ToString()]);
                    string mailProtocol = MailSettings.TradingPartner.GetMailProtocol(protocolID);
                    for (int i = 0; i < comboBoxMailProtocol.Items.Count; i++)
                    {
                        if (mailProtocol.Equals(((DataRowView)comboBoxMailProtocol.Items[i]).Row[MailProtocolColumns.MailProtocol.ToString()].ToString()))
                            comboBoxMailProtocol.SelectedIndex = i;
                    }
                    textBoxInMailServer.Text = dataSet.Tables[0].Rows[0][MailSetupColumns.IncommingServer.ToString()].ToString();
                    textBoxOutServer.Text = dataSet.Tables[0].Rows[0][MailSetupColumns.OutgoingServer.ToString()].ToString();
                    textBoxInPortNo.Text = dataSet.Tables[0].Rows[0][MailSetupColumns.InPortNo.ToString()].ToString();
                    textBoxOutPortNo.Text = dataSet.Tables[0].Rows[0][MailSetupColumns.OutPortNo.ToString()].ToString();
                    Int16 ssl = Convert.ToInt16(dataSet.Tables[0].Rows[0][MailSetupColumns.IsSSL.ToString()]);
                    if (ssl.Equals(0))
                        checkBoxUseSSL.Checked = false;
                    else
                        checkBoxUseSSL.Checked = true;
                    textBoxUserName.Text = dataSet.Tables[0].Rows[0][MailSetupColumns.UserName.ToString()].ToString();
                    textBoxEmailAddress.Text = dataSet.Tables[0].Rows[0][MailSetupColumns.EmailID.ToString()].ToString();
                    string pass = dataSet.Tables[0].Rows[0][9].ToString();
                    if (string.IsNullOrEmpty(pass))
                    {
                        textBoxPassword.Text = "";
                        checkBoxRememberPassword.Checked = false;
                    }
                    else
                    {
                        textBoxPassword.Text = pass;
                        checkBoxRememberPassword.Checked = true;
                    }
                    //Advanced settings
                    int auto=0;
                    if ((object)dataSet.Tables[0].Rows[0][MailSetupColumns.AutomaticFlag.ToString()] != null)

                        auto = Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.AutomaticFlag.ToString()].ToString() == string.Empty ? 0 : Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.AutomaticFlag.ToString()]));
                    if (auto.Equals(0))
                    {
                        checkBoxSendReceive.Checked = false;
                        textBoxSendReceive.Enabled = false;
                        checkBoxInboundMessages.Checked = false;
                        checkBoxOutboundMessages.Checked = false;
                        checkBoxInboundMessages.Enabled = false;
                        checkBoxOutboundMessages.Enabled = false;
                    }
                    else
                    {
                        checkBoxSendReceive.Checked = true;
                        textBoxSendReceive.Enabled = true;
                        checkBoxInboundMessages.Checked = true;
                        checkBoxOutboundMessages.Checked = true;
                        checkBoxInboundMessages.Enabled = true;
                        checkBoxOutboundMessages.Enabled = true;
                    }

                    int send = 0;
                    if ((object)dataSet.Tables[0].Rows[0][MailSetupColumns.OutboundFlag.ToString()] != null)
                        send = Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.OutboundFlag.ToString()].ToString() == string.Empty ? 0 : Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.OutboundFlag.ToString()].ToString()));

                    if (send.Equals(0))
                    {
                        checkBoxOutboundMessages.Checked = false;
                        //checkBoxOutboundMessages.Enabled = false;
                    }
                    else
                    {
                        checkBoxOutboundMessages.Checked = true;
                        //checkBoxOutboundMessages.Enabled = true;
                    }


                    int receive = 0;
                    if ((object)dataSet.Tables[0].Rows[0][MailSetupColumns.InboundFlag.ToString()] != null)
                        receive = Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.InboundFlag.ToString()].ToString() == string.Empty ? 0 : Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.InboundFlag.ToString()].ToString()));

                    if (receive.Equals(0))
                    {
                        checkBoxInboundMessages.Checked = false;
                        //checkBoxInboundMessages.Enabled = false;
                    }
                    else
                    {
                        checkBoxInboundMessages.Checked = true;
                        //checkBoxInboundMessages.Enabled = true;                    
                    }


                    int value = 0;
                    if ((object)dataSet.Tables[0].Rows[0][MailSetupColumns.ScheduleInMin.ToString()] != null)
                        value = Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.ScheduleInMin.ToString()].ToString() == string.Empty ? 0 : Convert.ToInt32(dataSet.Tables[0].Rows[0][MailSetupColumns.ScheduleInMin.ToString()].ToString()));
                    if (value != 0)
                        textBoxSendReceive.Text = Convert.ToString(value);
                    else
                        textBoxSendReceive.Text = string.Empty;
                }
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxInMailServer.Text.Trim()))
            {
                MessageBox.Show("Please enter incomming mail server name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInMailServer.Focus();
                return;
            }

            if (string.IsNullOrEmpty(textBoxOutServer.Text.Trim()))
            {
                MessageBox.Show("Please enter outgoing mail server name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxOutServer.Focus();
                return;
            }

            if (string.IsNullOrEmpty(textBoxInPortNo.Text.Trim()))
            {
                MessageBox.Show("Please enter incomming port number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInPortNo.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxOutPortNo.Text.Trim()))
            {
                MessageBox.Show("Please enter outgoing port number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxOutPortNo.Focus();
                return;
            }

            if (string.IsNullOrEmpty(textBoxUserName.Text.Trim()))
            {
                MessageBox.Show("Please enter user name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxUserName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxEmailAddress.Text.Trim()))
            {
                MessageBox.Show("Please enter email address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxEmailAddress.Focus();
                return;
            }
            if (checkBoxRememberPassword.Checked && string.IsNullOrEmpty(textBoxPassword.Text.Trim()))
            {
                MessageBox.Show("Please enter password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxPassword.Focus();
                return;
            }
                        
            if (!Regex.IsMatch(textBoxInPortNo.Text, EDI.Constant.Constants.Numeric_Pattern))

            {
                MessageBox.Show("Please enter numeric value in incomming port number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInPortNo.Focus();
                return;
            }

            if (Convert.ToInt32(textBoxInPortNo.Text).Equals(0))
            {
                MessageBox.Show("Incomming port number should be greater than zero.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInPortNo.Focus();
                return;
            }
            if (!Regex.IsMatch(textBoxOutPortNo.Text, EDI.Constant.Constants.Numeric_Pattern))
            {
                MessageBox.Show("Please enter numeric value in outgoing port number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxOutPortNo.Focus();
                return;
            }
           
            if (Convert.ToInt32(textBoxOutPortNo.Text).Equals(0))
            {
                MessageBox.Show("Outgoing port number should be greater than zero.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxOutPortNo.Focus();
                return;
            }
            if (!DataProcessingBlocks.CommonUtilities.IsEmail(textBoxEmailAddress.Text.Trim()))
            {
                MessageBox.Show("Please enter valid email as abc@1yz.com.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxEmailAddress.Focus();
                return;
            }
            if (checkBoxSendReceive.Checked && textBoxSendReceive.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter the time for automatic send/recieve.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxSendReceive.Focus();
                return;
            }
            if (checkBoxSendReceive.Checked && textBoxSendReceive.Text.Trim() != string.Empty && Convert.ToInt16(textBoxSendReceive.Text.Trim()) == 0)
            {
                MessageBox.Show("Please enter valid time.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxSendReceive.Focus();
                return;
            }

            #region Advanced Settings

            bool autoFlag = false;
            bool sendFlag = false;
            bool receiveFlag = false;

            if(checkBoxSendReceive.Checked)
                autoFlag = true;
            if(checkBoxInboundMessages.Checked)
                receiveFlag = true;
            if (checkBoxOutboundMessages.Checked)
                sendFlag = true;
                
            #endregion

            bool isSSL = false;
            if (checkBoxUseSSL.Checked)
                isSSL = true;

            int scheduleValue = 0;
            if (!string.IsNullOrEmpty(textBoxSendReceive.Text.Trim()) && (Convert.ToInt16(textBoxSendReceive.Text.Trim()) != 0))
            {
                scheduleValue = Convert.ToInt16(textBoxSendReceive.Text.Trim());       
            }
            if (!textBoxSendReceive.Enabled)
            {
                scheduleValue = 0;
            }

            MailSettings.MailSetup mailSetup = new MailSettings.MailSetup(comboBoxMailProtocol.SelectedValue.ToString(), textBoxInMailServer.Text.Trim(), textBoxOutServer.Text.Trim(), textBoxInPortNo.Text.Trim(), textBoxOutPortNo.Text.Trim(), isSSL, textBoxUserName.Text.Trim(), textBoxEmailAddress.Text.Trim(), textBoxPassword.Text.Trim(), autoFlag, sendFlag, receiveFlag, scheduleValue);
            if (checkBoxRememberPassword.Checked)
                mailSetup.IsPasswordRemembered = true;
            else
                mailSetup.IsPasswordRemembered = false;
            if (mailSetup.SaveMailSetupDetails())
            {
                MessageBox.Show("Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.Cancel;
                this.Close();
                m_MailConfigurationSettings = null;

                //Update the status of Advanced settings.
                if (checkBoxSendReceive.Checked)
                    CommonUtilities.GetInstance().IsAutoFlag = true;
                else
                    CommonUtilities.GetInstance().IsAutoFlag = false;
                if (checkBoxInboundMessages.Checked)
                    CommonUtilities.GetInstance().IsInboundFlag = true;
                else
                    CommonUtilities.GetInstance().IsInboundFlag = false;
                if (checkBoxOutboundMessages.Checked)
                    CommonUtilities.GetInstance().IsOutboundFlag = true;
                else
                    CommonUtilities.GetInstance().IsOutboundFlag = false;

                if (!string.IsNullOrEmpty(textBoxSendReceive.Text) && textBoxSendReceive.Enabled)
                {
                    if (MessageBox.Show("Please restart the application to take effects for the Automatic SendReceive if scheduled time is updated.", DataProcessingBlocks.MessageCodes.GetValue("ProductName"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        CommonUtilities.GetInstance().IsRestartAfterAutomaticSettings = true;
                        Application.Exit();
                    }
                }
            }
            else
                MessageBox.Show("Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information); 
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
          
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            m_MailConfigurationSettings = null;
        }

        private void buttonTestConnection_Click(object sender, EventArgs e)
        {
           
            if (string.IsNullOrEmpty(textBoxInMailServer.Text.Trim()))
            {
                MessageBox.Show("Please enter incomming mail server name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInMailServer.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxInPortNo.Text.Trim()))
            {
                MessageBox.Show("Please enter incomming port number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInPortNo.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxUserName.Text.Trim()))
            {
                MessageBox.Show("Please enter user name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxUserName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxEmailAddress.Text.Trim()))
            {
                MessageBox.Show("Please enter email address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxEmailAddress.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxPassword.Text.Trim()))
            {
                MessageBox.Show("Please enter password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxPassword.Focus();
                return;
            }
            //for bug 12
            if (!Regex.IsMatch(textBoxInPortNo.Text, EDI.Constant.Constants.Numeric_Pattern))
            {
                MessageBox.Show("Please enter numeric value in incomming port number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInPortNo.Focus();
                return;
            }
            //for bug 12
            if (Convert.ToInt32(textBoxInPortNo.Text).Equals(0))
            {
                MessageBox.Show("Incomming port number should be greater than zero.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxInPortNo.Focus();
                return;
            }
            if (!DataProcessingBlocks.CommonUtilities.IsEmail(textBoxEmailAddress.Text.Trim()))
            {
                MessageBox.Show("Please enter valid email as abc@xyz.com.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxEmailAddress.Focus();
                return;
            }

            //for bug number 37
            Pop3MailClient pop3 = new Pop3MailClient(textBoxInMailServer.Text, Convert.ToInt32(textBoxInPortNo.Text), checkBoxUseSSL.Checked, textBoxUserName.Text, textBoxPassword.Text);
            try
            {
                pop3.Connect();

                if (pop3.Pop3ConnectionState == Pop3ConnectionStateEnum.Connected)
                    MessageBox.Show("Incoming mail server test connection was successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Connect failed, check settings", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                pop3.Disconnect();
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                MessageBox.Show(ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //if (string.IsNullOrEmpty(textBoxOutPortNo.Text.Trim()))
            //{
            //    MessageBox.Show("Please enter outgoing port number", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxOutPortNo.Focus();
            //    return;
            //}
            //if (string.IsNullOrEmpty(textBoxOutServer.Text.Trim()))
            //{
            //    MessageBox.Show("Please enter outgoing server name", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxOutServer.Focus();
            //    return;
            //}
            //if (!Regex.IsMatch(textBoxOutPortNo.Text, EDI.Constant.Constants.Numeric_Pattern))
            //{
            //    MessageBox.Show("Please enter numeric value", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxOutPortNo.Focus();
            //    return;
            //}
            //if (!Regex.IsMatch(textBoxOutPortNo.Text, EDI.Constant.Constants.Numeric_Pattern))
            //{
            //    MessageBox.Show("Please enter numeric value in outgoing port number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxOutPortNo.Focus();
            //    return;
            //}
            //if (Convert.ToInt32(textBoxOutPortNo.Text).Equals(0))
            //{
            //    MessageBox.Show("Outgoing port number should be greater than zero.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxOutPortNo.Focus();
            //    return;
            //}

        }

        private void comboBoxMailProtocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!comboBoxMailProtocol.SelectedIndex.Equals(0))
            {
                MessageBox.Show("Other mail protocol is not supported in this version of Zed Axis.",ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxMailProtocol.SelectedIndex = 0;
                return;
            }
        }

        private void textBoxSendReceive_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = Constants.validnumber;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void textBoxInPortNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = Constants.validnumber;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void textBoxOutPortNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = Constants.validnumber;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void checkBoxSendReceive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSendReceive.Checked)
            {
                textBoxSendReceive.Enabled = true;
                checkBoxInboundMessages.Enabled = true;
                checkBoxOutboundMessages.Enabled = true;
            }
            else
            {
                textBoxSendReceive.Enabled = false;
                textBoxSendReceive.Text = string.Empty;
                checkBoxInboundMessages.Checked = false;
                checkBoxOutboundMessages.Checked = false;
                checkBoxInboundMessages.Enabled = false;
                checkBoxOutboundMessages.Enabled = false;
            }
        }    
    }
}