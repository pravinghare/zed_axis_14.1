using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DataProcessingBlocks;
using Streams;
using System.IO;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class is used to Export data to filepath selected by user.
    /// </summary>
    public partial class ExportData : Form
    {
        #region Private Members

        private string m_filePath;
        private DataTable m_exportData;

        #endregion

        #region Constructor
        public ExportData()
        {
            InitializeComponent();
        }               
        
        #endregion

        #region Public Properties

        /// <summary>
        /// This property is used for getting and setting dataTable from collection class
        /// </summary>
        public DataTable ExportDataTable
        {
            get { return m_exportData; }
            set { m_exportData = value; }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This method is used for Exporting data to Tab Delimited Text File.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="FilePath"></param>
        private void ExportTextFile(DataTable dt, string FilePath)
        {
            int i = 0;
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(FilePath, false);
                //Writng the columns Name.
                for (i = 0; i < dt.Columns.Count - 1; i++)
                {
                    sw.Write(dt.Columns[i].ColumnName + " \t");
                }
                sw.Write(dt.Columns[i].ColumnName);
                sw.WriteLine();
              
                //Writing the Data. 
                foreach (DataRow row in dt.Rows)
                {
                    //Creating an Object Array.
                    object[] array = row.ItemArray;

                    for (i = 0; i < array.Length - 1; i++)
                    {
                        sw.Write(array[i].ToString() + " \t");
                    }
                    sw.Write(array[i].ToString());
                    sw.WriteLine();
                }

                sw.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Invalid Operation : \n" + ex.ToString(),  "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
           
        }        

        #endregion

        #region Event Handlers

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            saveFileDialogExportFilePath.FileName = string.Empty;
            //Select the filter type for SaveDialogFilePath.
            saveFileDialogExportFilePath.Filter = "Text File |*.txt; | Excel File | *.xls; | Excel 2007 | *.xlsx";
            if (saveFileDialogExportFilePath.ShowDialog() == DialogResult.OK)
            {
                textBoxExportDataFilePath.Text = saveFileDialogExportFilePath.FileName;
                m_filePath = textBoxExportDataFilePath.Text;
            }
        }
       
        /// <summary>
        /// This button event click is used for exporting data to Excel and text File.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExport_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            //Check whether user has browse a file Path to Export Data.
            if (m_filePath == null)
            {
                MessageBox.Show("Please Browse a File Path to Export Data.","Zed Axis",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;             
            }
            if (m_exportData.Rows.Count == 0)
            {
                MessageBox.Show("There is no item present in Current Transaction Type.","Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                string extension = string.Empty;
                ExcellUtility excel = new ExcellUtility();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();                
                dt = m_exportData;
                //Converting an dataTable to dataset.
                if (ds != null)
                {
                    ds.Tables.Add(dt);
                }
                
                //Getting an Extension of File Path.
                extension =  Path.GetExtension(m_filePath);
               
                if (extension == ".xls")
                {
                    //Exporting Data to Excel Sheet(.xls).
                    excel.ExportToExcel(ds, m_filePath);                    
                }
                else if (extension == ".txt")
                {
                    //Exporting Data to Text File.
                    ExportTextFile(m_exportData, m_filePath);
                }
                else if(extension == ".xlsx")
                {                   
                    //Exporting Data to Excel Sheet(.xlsx).
                    excel.ExportToExcel2007(ds, m_filePath);                    
                }
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Data Successfully Exported.","Zed Axis",MessageBoxButtons.OK);
                this.Close();
                textBoxExportDataFilePath.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        #endregion      
    }
}