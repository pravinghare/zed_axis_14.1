using System;
using System.Collections.Specialized;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using EDI.Constant;

namespace DataProcessingBlocks
{
    /// <summary>
    /// Summary description for Settings.
    /// </summary>
    public class MessageCodes
    {
        #region Private Static Methods
        private static NameValueCollection m_settings;
        private static string m_settingsPath; 
        #endregion

        #region Constructor
        /// <summary>
        /// Static constructor
        /// </summary>
        static MessageCodes()
        {
            m_settingsPath = Application.StartupPath;
            m_settingsPath += @"\Messages.xml";
            try
            {
                //if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //    m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                //else
                //    m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    //Axis 10.1 changes 
                    //if (m_settingsPath.Contains("Program Files (x86)"))
                    //{
                    //    m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                    //}
                    //else
                    //{
                    //    m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                    //}
                    //
                    //End region
                    if (m_settingsPath.Contains("Program Files (x86)"))
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (m_settingsPath.Contains("Program Files (x86)"))
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                    }

                }
            }
            catch { }

            if (!File.Exists(m_settingsPath))
                throw new FileNotFoundException(m_settingsPath + " could not be found.");

            System.Xml.XmlDocument xdoc = new XmlDocument();
            xdoc.Load(m_settingsPath);
            XmlElement root = xdoc.DocumentElement;
            System.Xml.XmlNodeList nodeList = root.ChildNodes.Item(0).ChildNodes;

            // Add settings to the NameValueCollection.
            m_settings = new NameValueCollection();


            foreach (XmlNode node in nodeList)
            {
                m_settings.Add(node.Attributes["key"].Value, node.Attributes["value"].Value);
            }
           
        } 
        #endregion

        #region Static Methods
        
        /// <summary>
        /// Gets the Setting values from Settings File.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string GetValue(string code)
        {
            return m_settings.Get(code);
        } 
        #endregion
       
    }
}

