using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using EDI.Constant;
using System.Xml;
using Telerik.WinControls.UI;
using Streams;
//Axis 10.0
using System.Globalization;
using TransactionImporter;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    public partial class CodingTemplatePreview : Form
    {
        #region Private members
        TransactionImporter.TransactionImporter objTransactionImporter;
        private static CodingTemplatePreview m_CodingTemplatePreview = null;
        private string[] m_codingTemplateList = new string[100];

        // Telerik Control Declarations
        private GridViewRowInfo tDr;
        private GridViewCheckBoxColumn tCheckColumn;
        private GridViewComboBoxColumn tComboColumn;
        private GridViewTextBoxColumn tTextColumn;
        private GridViewHyperlinkColumn tHyperlinkColumn;

        string importedcnt = "";

        private GridViewMultiComboBoxColumn tMultiComboColumn;
        private bool isColumnAdded;

        private DataGridViewRow dr;
        private DataGridViewCheckBoxColumn checkColumn;
        private DataGridViewComboBoxColumn comboColumn;
        private DataGridViewTextBoxColumn textColumn;

        private DataGridViewCheckBoxCell checkCell;
        private DataGridViewComboBoxCell comboCell;
        private DataGridViewTextBoxCell textCell;
        private DataGridViewTextBoxCell textCellMemo;
        private bool selectionFlag = true;

        List<string> custjobList = null;
        List<string> empList = null;
        List<string> vendList = null;
        List<string> othrList = null;

        //463 assign null 
        QBCustomerListCollection customerList = null;
        QBEmployeeListCollection employeeList = null;
        QBVendorListCollection vendorList = null;
        QBOtherListCollection otherList = null;



        List<string> custList = null;
        List<string> radCustList = null;
        List<string> radEmpList = null;
        List<string> radVendList = null;
        List<string> clasList = null;
        List<string> radOthrList = null;
        List<string> radExpenseAccList = null;

        //463 assign null 
        QBCustomerListCollection radCustomerList = null;
        QBEmployeeListCollection radEmployeeList = null;
        QBVendorListCollection radVendorList = null;
        QBClassListCollection classList = null;
        QBOtherListCollection radOtherList = null;
        QBAccountQueryListCollection radExpenseAccountList = null;

        private DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
        private GridViewCellStyle radCellStyle = new GridViewCellStyle();
        private GridViewCellStyle validateCellStyle = new GridViewCellStyle();

        private string m_ConnectedSoft;

        bool flagQBO = false;

        int startIndex = 0;
        int endIndex = 0;
        string data = string.Empty;
        string subStr = string.Empty;
        string[] val = new string[500];
        public static string tmpTxnId1;
        public static string tmpTxnId;
        string creditAccount = "CreditCard Account";
        string bankAccount = "Bank Account";
        public string selectedAccountType = CommonUtilities.GetInstance().ComboBoxImportMappingName;
        public string accountType = CommonUtilities.GetInstance().ComboBoxImportMapping.ToString();
        bool DateFlag = false;
        public List<string> allcust = new List<string>();

        List<string> accountItemList = CommonUtilities.GetInstance().AccountItemList;


        //Axis 10.0
        public TransactionImporter.TransactionImporter trs = new TransactionImporter.TransactionImporter();

        //466 add null
        QBSalesReceiptListCollection QBSalesReceiptColl = null;
        QBDepositListCollection QBDepositColl = null;



        public List<string> salesReceiptData = new List<string>();
        private string xmlFileContent = string.Empty;
        DataTable salesReceptInfo = new DataTable();
        DataTable depositInfo = new DataTable();
        DataTable dtSalesReceipt = new DataTable();
        DataTable dtDeposit = new DataTable();
        DataTable dtTransactionInfo = new DataTable();

        //463
        QBCheckListCollection QBCheckColl = null;

        public List<string> checkData = new List<string>();
        DataTable checkInfo = new DataTable();
        DataTable dtCheck = new DataTable();
        private string xmlFileContent1 = string.Empty;

        //463
        QBCreditCardChargeListCollection QBCreditCardChargeColl = null;


        public List<string> creditCardChargeData = new List<string>();
        DataTable creditCardChargeInfo = new DataTable();
        DataTable dtCreditCardCharge = new DataTable();
        private string xmlFileContent2 = string.Empty;

        //463
        QBCreditCardCreditListCollection QBCreditCardCreditColl = null;

        public List<string> creditCardCreditData = new List<string>();
        DataTable creditCardCreditInfo = new DataTable();
        DataTable dtCreditCardCredit = new DataTable();
        private string xmlFileContent3 = string.Empty;



        //466
        DefaultAccountSettings m_DefaultSetting = null;


        DataTable dt = null;
        DataTable dt1 = null;
        int showCounter = 1;
        ImportSplashScreen iss = ImportSplashScreen.GetInstance();
        private StringBuilder m_sb;
        private int m_Failure = 0;
        private int m_Success = 0;
        private int m_skip = 0;
        private bool m_IsBusy = true;
        string statusMsg = string.Empty;
        private string txnId = string.Empty;
        string nameImport = string.Empty;
        private string tranType;
        double matchedAmount = 0.0;
        string accType = string.Empty;
        //string selectedAccountType = string.Empty;

        #endregion

        public static string coddingTemplateConnectedSoftware;
        // DefaultAccountSettings m_DefaultSetting;
        private string strConnectedSoftware = string.Empty;
        ImportSplashScreen objImportSplashScreen = new ImportSplashScreen();

        #region Public properties

        public static CodingTemplatePreview GetInstance()
        {
            m_CodingTemplatePreview = new CodingTemplatePreview();
            return m_CodingTemplatePreview;
        }

        public string[] CodingTemplateList
        {
            get
            { return m_codingTemplateList; }
            set
            { m_codingTemplateList = value; }
        }


        /// <summary>
        /// This property is used for set and get Connected Accounting Software value.
        /// </summary>
        public string ConnectedSoft
        {
            get
            { return m_ConnectedSoft; }
            set
            {
                m_ConnectedSoft = value;
            }
        }
        //Axis 10.0
        public bool IsBusy
        {
            get { return m_IsBusy; }
            set { m_IsBusy = value; }
        }
        /// <summary>
        /// Get or set message from the quickbook or MYOB.
        /// </summary>
        public StringBuilder StrBuilder
        {
            get { return m_sb; }
            set { m_sb = value; }
        }

        /// <summary>
        /// Get or set the failure count.
        /// </summary>
        public int Failures
        {
            get { return m_Failure; }
            set { m_Failure = value; }
        }

        /// <summary>
        /// Get or set the Success count.
        /// </summary>
        public int Successes
        {
            get { return m_Success; }
            set { m_Success = value; }
        }

        /// <summary>
        /// Get or set the Skip count.
        /// </summary>
        public int Skip
        {
            get { return m_skip; }
            set { m_skip = value; }
        }

        #endregion

        public CodingTemplatePreview()
        {
            InitializeComponent();
        }



        /// <summary>
        /// Ok button functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPreviewOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            radExpenseAccountList = null;

            cellStyle = null;
            radCellStyle = null;
            validateCellStyle = null;
            accountItemList = null;
            QBSalesReceiptColl = null;
            QBDepositColl = null;
            salesReceiptData = null;
            QBCreditCardCreditColl = null;
            creditCardCreditInfo = null;
            dtCreditCardCredit = null;




            tComboColumn = null;
            CommonUtilities.GetInstance().OtherListWithType = null;
            CommonUtilities.GetInstance().VendorListWithType = null;
            CommonUtilities.GetInstance().EmployeeListWithType = null;
            CommonUtilities.GetInstance().CustomerListWithType = null;
            CommonUtilities.GetInstance().AccountItemList = null;
            CommonUtilities.GetInstance().depositeaccountTO = null;
            CommonUtilities.GetInstance().CodingTemplateList = null;
            CommonUtilities.GetInstance().ExpenseAccountList = null;
            CommonUtilities.GetInstance().allcust = null;
            //CommonUtilities.GetInstance().sb = null;
            CommonUtilities.GetInstance().CombineCustomerList = null;

            if (radCustList != null)
            {
                radCustList = null;
            }

            if (radCustomerList != null)
            {
                radCustomerList = null;
            }
            if (radEmployeeList != null)
            {
                radEmployeeList = null;
            }
            if (radVendorList != null)
            {
                radVendorList = null;
            }
            if (radOtherList != null)
            {
                radOtherList = null;
            }
            if (classList != null)
            {
                classList = null;
            }
            if (otherList != null)
            {
                otherList.Clear();
            }
            if (custjobList != null)
            {
                custjobList.Clear();
            }
            if (custList != null)
            {
                custList.Clear();
            }
            if (vendList != null)
            {
                vendList.Clear();
            }
            if (clasList != null)
            {
                clasList.Clear();
            }

            CommonUtilities.GetInstance().StatementDataTable = null;
            this.dataGridViewPreviewCodingTemplate.DataSource = null;
            this.radGridCodingTemplate.DataSource = null;
            //bug 463
            radGridCodingTemplate.Dispose();
            //this.DialogResult = DialogResult.OK;

            //update datagridview
            //this.dataGridViewPreviewCodingTemplate.Update();
            //this.dataGridViewPreviewCodingTemplate.Refresh();


            //this.radGridCodingTemplate.Update();
            //this.radGridCodingTemplate.Refresh();


            //CommonUtilities.GetInstance().StatementDataTable = GetDataTable(this.dataGridViewPreviewCodingTemplate);
            //CommonUtilities.GetInstance().StatementDataTable = GetDataTable(this.radGridCodingTemplate);  

            //if (this.comboBoxPreviewCodingTemplate.SelectedIndex!=-1)
            //    CommonUtilities.GetInstance().SelectedCodingTemplate = this.comboBoxPreviewCodingTemplate.SelectedItem.ToString();

            #region commented for new changes Update Coding.xml 
            //CommonUtilities.GetInstance().UpdateCodingTemplate(CommonUtilities.GetInstance().StatementDataTable);
            #endregion

            m_CodingTemplatePreview = null;
            //bug 463
            //  m_CodingTemplatePreview.Dispose();

            try
            {
                //TransactionImporter.TransactionImporter frmImporter = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                //ComboBox cbTemplates = (ComboBox)frmImporter.comboBoxCodingTemplate;
                //cbTemplates.Items.Clear();
                //cbTemplates.Items.Add("<Add New Coding Template>");
                //List<string> templateList = new List<string>();
                //templateList = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();



                //foreach (string codingTemplateName in templateList)
                //{
                //    if (!string.IsNullOrEmpty(codingTemplateName))
                //        cbTemplates.Items.Add(codingTemplateName);
                //}
                //cbTemplates.SelectedItem = this.comboBoxPreviewCodingTemplate.SelectedItem.ToString();
                CommonUtilities.GetInstance().StatementDataTable = null;
            }
            catch (Exception exps)
            {
                MessageBox.Show("Error:" + exps.Message);
            }
            CommonUtilities.GetInstance().is_statement_preview = false;
            Dispose(true);
            this.Close();
        }



        public DataTable GetSalesReceiptData()
        {
            QBSalesReceiptColl = new QBSalesReceiptListCollection();
            QBSalesReceiptColl.PopulateSalesReceiptList(CommonUtilities.GetInstance().CompanyFile);
            xmlFileContent = QBSalesReceiptColl.GetXmlFileData();

            if (QBSalesReceiptColl.Count > 0)
            {

                dtSalesReceipt = GenerateExportData.GetInstance().CreateSalesReceiptDataTable(QBSalesReceiptColl);
            }
            return dtSalesReceipt;

        }

        public DataTable GetDepositData()
        {
            QBDepositColl = new QBDepositListCollection();
            QBDepositColl.PopulateDepositList(CommonUtilities.GetInstance().CompanyFile);
            xmlFileContent = QBDepositColl.GetXmlFileData();

            if (QBDepositColl.Count > 0)
            {

                dtDeposit = GenerateExportData.GetInstance().CreateDepositDataTable(QBDepositColl);
            }
            return dtDeposit;

        }

        public DataTable GetChecktData()
        {
            QBCheckColl = new QBCheckListCollection();
            QBCheckColl.PopulateCheckList(CommonUtilities.GetInstance().CompanyFile);

            xmlFileContent1 = QBCheckColl.GetXmlFileData();

            if (QBCheckColl.Count > 0)
            {

                dtCheck = GenerateExportData.GetInstance().CreateCheckDataTable(QBCheckColl);
            }
            return dtCheck;

        }


        public DataTable GetCreditCardChargeData()
        {
            QBCreditCardChargeColl = new QBCreditCardChargeListCollection();


            QBCreditCardChargeColl.PopulateCreditCardChargeList(CommonUtilities.GetInstance().CompanyFile);

            xmlFileContent2 = QBCreditCardChargeColl.GetXmlFileData();

            if (QBCreditCardChargeColl.Count > 0)
            {

                dtCreditCardCharge = GenerateExportData.GetInstance().CreateCreditCardChargeDataTable(QBCreditCardChargeColl);
            }
            return dtCreditCardCharge;

        }


        //public Hashtable GetCreditCardCreditData()
        //{
        //    QBCreditCardChargeColl = new QBCreditCardChargeListCollection();
        //    Hashtable testData = QBCreditCardChargeColl.PopulateCreditCardCreditList(CommonUtilities.GetInstance().CompanyFile);

        //    return testData;

        //}



        public DataTable GetCreditCardCreditData()
        {
            QBCreditCardCreditColl = new QBCreditCardCreditListCollection();


            QBCreditCardCreditColl.PopulateCreditCardCreditAllDataList(CommonUtilities.GetInstance().CompanyFile);

            xmlFileContent3 = QBCreditCardCreditColl.GetXmlFileData();

            if (QBCreditCardCreditColl.Count > 0)
            {

                dtCreditCardCredit = GenerateExportData.GetInstance().CreateCreditCardCreditDataTable(QBCreditCardCreditColl);
            }
            return dtCreditCardCredit;

        }

        /// <summary>
        /// Create dataTable using dataGridview of CodingTemplatePreview form
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <returns></returns>
        private DataTable GetDataTable(DataGridView dataGridView)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dataGridView.RowCount > 0)
                {
                    for (int col = 0; col < dataGridView.Columns.Count; col++)
                    {
                        dt.Columns.Add(dataGridView.Columns[col].HeaderText);
                    }
                    for (int row = 0; row < dataGridView.RowCount; row++)
                    {
                        DataRow datarw = dt.NewRow();
                        for (int col = 1; col < dataGridView.ColumnCount; col++)
                        {
                            if (dataGridView.Rows[row].Cells[col].FormattedValue.Equals("<NULL>"))
                                datarw[col] = dataGridView.Rows[row].Cells[col].FormattedValue;
                            else
                                datarw[col] = string.Empty;
                        }
                        dt.Rows.Add(datarw);
                    }
                }
                else
                    dt = null;
                return dt;
            }
            catch
            {
                return null;
            }
        }




        //Axis 10.0
        /// <summary>
        /// Create dataTable using RADGridView of CodingTemplatePreview form
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <returns></returns>
        private DataTable GetDataTable(RadGridView radGridView)
        {
            try
            {
                DataTable dt = new DataTable();
                if (radGridView.RowCount > 0)
                {
                    for (int col = 0; col < radGridView.Columns.Count - 1; col++)
                    {
                        dt.Columns.Add(radGridView.Columns[col].HeaderText);
                    }
                    for (int row = 0; row <= radGridView.RowCount - 1; row++)
                    {
                        DataRow datarw = dt.NewRow();
                        for (int col = 0; col < radGridView.ColumnCount - 1; col++)
                        {
                            if (radGridView.Rows[row].Cells[col].Value != null)
                            {
                                if (!radGridView.Rows[row].Cells[col].Value.ToString().Equals("<None>"))
                                {
                                    datarw[col] = radGridView.Rows[row].Cells[col].Value;
                                    if (col == 5 || col == 8 || col == 9)
                                    {
                                        string customerName = radGridView.Rows[row].Cells[col].Value.ToString();
                                        string[] custName = Regex.Split(customerName, @"\s{2,}");

                                        datarw[col] = custName[0];
                                    }

                                }
                                else
                                    datarw[col] = string.Empty;
                            }

                        }
                        dt.Rows.Add(datarw);
                    }
                }
                else
                    dt = null;
                return dt;
            }
            catch (Exception exs)
            {
                MessageBox.Show("Error : " + exs.Message);
                return null;
            }

        }




        /// <summary>
        /// Show Edit Template
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPreviewEdit_Click(object sender, EventArgs e)
        {
            EditTemplate editTemplate = new EditTemplate();

            if (this.ConnectedSoft == Constants.QBstring)
            {
                editTemplate.ConnectedSoft = Constants.QBstring;
            }
            // axis11 pos
            else if (this.ConnectedSoft == Constants.QBPOSstring)
            {
                editTemplate.ConnectedSoft = Constants.QBPOSstring;
            }
            else if (this.ConnectedSoft == Constants.QBOnlinestring)
            {
                editTemplate.ConnectedSoft = Constants.QBOnlinestring;
            }

            DataGridView dgvEdit = new DataGridView();
            DataGridView dgvEdit1 = new DataGridView();

            bool selectFlag = false;

            //Axis 10.0

            for (int i = 0; i < this.radGridCodingTemplate.RowCount; i++)
            {
                try
                {
                    if (this.radGridCodingTemplate.Rows[i].Cells[0].Value.Equals(true))
                    {
                        selectFlag = true;
                        break;
                    }
                    else
                        selectFlag = false;
                }
                catch
                {
                }
            }



            for (int i = 0; i < this.dataGridViewPreviewCodingTemplate.RowCount; i++)
            {
                if (this.dataGridViewPreviewCodingTemplate.Rows[i].Cells[0].FormattedValue.Equals(true))
                {
                    selectFlag = true;
                    break;
                }
                else
                    selectFlag = false;

            }

            if (selectFlag.Equals(false))
            {
                //show apropriate message
                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0136"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (this.comboBoxPreviewCodingTemplate.SelectedIndex != -1)
            {
                CommonUtilities.GetInstance().SelectedCodingTemplate = this.comboBoxPreviewCodingTemplate.SelectedItem.ToString();
            }

            //Save total number of row in CommonUtilities
            CommonUtilities.GetInstance().RowCountOfCodingTemplateGridView = this.dataGridViewPreviewCodingTemplate.RowCount.ToString();


            //Save total number of row in CommonUtilities for Axis 10.0
            CommonUtilities.GetInstance().RowCountOfCodingTemplateGridView = this.radGridCodingTemplate.RowCount.ToString();


            //create dataGridview for EditTemplate
            // dgvEdit = editTemplate.CreateDatasetForEditTemplatePreview(this.dataGridViewPreviewCodingTemplate,false);

            //***************************************************************************MAJOR ISSUE*******************************************************
            //create dataGridview for EditTemplate for Axis 10.0

            //dgvEdit = editTemplate.CreateDatasetForEditTemplatePreview(this.dataGridViewPreviewCodingTemplate, false);

            dgvEdit = editTemplate.CreateDatasetForEditTemplatePreview(this.radGridCodingTemplate, false);

            DialogResult key = editTemplate.ShowDialog();

            if (key == DialogResult.Cancel)
            {
                Hashtable getEditData = CommonUtilities.GetInstance().EditCodingTemplate;



                try
                {
                    #region Not Required Commented code Update appropriate data row (As updation done on keyword matches not on row number)

                    //List<string[]> editGridData=CommonUtilities.GetInstance().EditGridviewData;

                    #endregion

                    #region Update matching Keyword row in datagridview

                    //get keyword information from Coding.xml file
                    List<string[]> matchedData = CommonUtilities.GetInstance().KeywordList;


                    for (int row = 0; row < radGridCodingTemplate.Rows.Count; row++)
                    {
                        #region Modify datagridview as per matched Data
                        for (int j = 0; j < matchedData.Count; j++)
                        {
                            if (matchedData[j][7].ToString().ToLower().Equals("contains"))
                            {
                                if (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
                                    || matchedData[j][0].ToLower().Contains(radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower()))
                                {
                                    #region Contains found ... update datagridview

                                    radGridCodingTemplate.Rows[row].Cells[0].Value = true;
                                    radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][6];
                                    radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
                                    radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
                                    radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
                                    radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
                                    radGridCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];

                                    if (creditAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                            tranType = "CreditCardCharge";
                                        else
                                            tranType = "CreditCardCredit";

                                    }

                                    if (bankAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                            tranType = "Check";
                                        else
                                            tranType = "SalesReceipt";

                                    }

                                    string tmpMemo = radGridCodingTemplate.Rows[row].Cells[4].Value.ToString();

                                    string tmpCustomerName = radGridCodingTemplate.Rows[row].Cells[5].Value.ToString();

                                    string tmpItemName = radGridCodingTemplate.Rows[row].Cells[6].Value.ToString();

                                    string tmpTax = radGridCodingTemplate.Rows[row].Cells[7].Value.ToString();

                                    string tmpPayment = string.Empty;

                                    if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                        tmpPayment = radGridCodingTemplate.Rows[row].Cells[11].Value.ToString();
                                    else
                                        tmpPayment = radGridCodingTemplate.Rows[row].Cells[11].Value.ToString();

                                    string tmpBillable = string.Empty;

                                    if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == true)
                                        tmpBillable = "Billable";
                                    if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == false)
                                        tmpBillable = "NotBillable";



                                    string tmpRefNo = radGridCodingTemplate.Rows[row].Cells[2].Value.ToString();

                                    string curDate = radGridCodingTemplate.Rows[row].Cells[1].Value.ToString();
                                    DateTime currentDate = Convert.ToDateTime(curDate);


                                    DateTime fromDate = currentDate.AddDays(-5);
                                    DateTime toDate = currentDate.AddDays(5);

                                    string matchString = string.Empty;

                                    matchString = objImportSplashScreen.GetTransactionInformation(tranType, tmpRefNo, fromDate, toDate, accountType, selectedAccountType, tmpPayment);
                                    string[] match = new string[4];

                                    if (matchString != null)
                                    {
                                        match = matchString.Split(':');
                                        matchedAmount = Convert.ToDouble(match[1].ToString());
                                        accType = match[2].ToString();
                                    }

                                    if (tranType == "SalesReceipt")
                                    {


                                        foreach (DataRow Dr in salesReceptInfo.Rows)
                                        {
                                            //string date;
                                            //date = radGridCodingTemplate.Rows[row].Cells[1].Value.ToString();
                                            //DateTime tmpDate = DateTime.Parse(date);
                                            //date = tmpDate.ToString("d");

                                            //DateTime qbDate = DateTime.Parse(Dr[5].ToString());
                                            //string quickBookdate = qbDate.ToString("d");


                                            //string tmpMemo = radGridCodingTemplate.Rows[row].Cells[4].Value.ToString();

                                            //string tmpCustomerName = radGridCodingTemplate.Rows[row].Cells[5].Value.ToString();

                                            //string tmpItemName = radGridCodingTemplate.Rows[row].Cells[6].Value.ToString();

                                            //string tmpTax = radGridCodingTemplate.Rows[row].Cells[7].Value.ToString();

                                            //string tmpPayment = radGridCodingTemplate.Rows[row].Cells[10].Value.ToString();

                                            //&& tmpDate.ToString().Equals(quickBookdate.ToString())

                                            if (tmpMemo.ToLower().Equals(Dr[34].ToString().ToLower()) &&
                                                 tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                                 tmpItemName.ToLower().Equals(Dr[42].ToString().ToLower()) &&
                                                 tmpTax.ToLower().Equals(Dr[52].ToString().ToLower()) &&
                                                 tmpPayment.ToLower().Equals(Dr[49].ToString().ToLower())

                                                )
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                                radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_S";
                                                tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();


                                            }






                                        }

                                        if (selectedAccountType.Equals(accType) &&
                                                 matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                                )
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_S";

                                        }


                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value.ToString() == "")
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            //  radGridCodingTemplate.Rows[row].Cells[14].Value = "";
                                        }


                                    }

                                    if (tranType == "Check")
                                    {


                                        foreach (DataRow Dr in checkInfo.Rows)
                                        {

                                            //string date;
                                            //date = radGridCodingTemplate.Rows[row].Cells[1].Value.ToString();
                                            //DateTime tmpDate = DateTime.Parse(date);
                                            //date = tmpDate.ToString("d");

                                            //DateTime qbDate = DateTime.Parse(Dr[5].ToString());
                                            //string quickBookdate = qbDate.ToString("d");


                                            //string tmpMemo = radGridCodingTemplate.Rows[row].Cells[4].Value.ToString();

                                            //string tmpCustomerName = radGridCodingTemplate.Rows[row].Cells[5].Value.ToString();

                                            //string tmpItemName = radGridCodingTemplate.Rows[row].Cells[6].Value.ToString();

                                            //string tmpTax = radGridCodingTemplate.Rows[row].Cells[7].Value.ToString();

                                            //string tmpPayment = radGridCodingTemplate.Rows[row].Cells[10].Value.ToString();

                                            //&& tmpDate.ToString().Equals(quickBookdate.ToString())

                                            if (tmpMemo.ToLower().Equals(Dr[10].ToString().ToLower()) &&
                                                 tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                                 tmpItemName.ToLower().Equals(Dr[24].ToString().ToLower()) &&
                                                 tmpPayment.ToLower().Equals(Dr[6].ToString().ToLower()) &&
                                                 tmpBillable.ToLower().Equals(Dr[26].ToString().ToLower()))
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                                radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_C";
                                                tmpTxnId = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();

                                            }





                                        }
                                        if (selectedAccountType.Equals(accType) &&
                                               matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                               )
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_C";

                                        }



                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value.ToString() == "")
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            // radGridCodingTemplate.Rows[row].Cells[14].Value = "";
                                        }

                                    }



                                    if (tranType == "CreditCardCredit")
                                    {


                                        foreach (DataRow Dr in creditCardCreditInfo.Rows)
                                        {
                                            DateTime tmpDate = new DateTime();
                                            tmpDate = Convert.ToDateTime(Dr[3].ToString());
                                            int dateResult = DateTime.Compare(currentDate, tmpDate);

                                            if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
                                                 tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                                 tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
                                                 tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
                                                 tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
                                                 dateResult == 0

                                                )
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                                radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_CR";
                                                tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();


                                            }



                                        }

                                        if (selectedAccountType.Equals(accType) &&
                                                 matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                                )
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_CR";

                                        }




                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value.ToString() == "")
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            //  radGridCodingTemplate.Rows[row].Cells[14].Value = "";
                                        }


                                    }



                                    if (tranType == "CreditCardCharge")
                                    {


                                        foreach (DataRow Dr in creditCardChargeInfo.Rows)
                                        {
                                            DateTime tmpDate = new DateTime();
                                            tmpDate = Convert.ToDateTime(Dr[3].ToString());
                                            int dateResult = DateTime.Compare(currentDate, tmpDate);

                                            if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
                                                 tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                                 tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
                                                 tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
                                                 tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
                                                 dateResult == 0


                                                )
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                                radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_CH";
                                                tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();


                                            }

                                        }

                                        if (selectedAccountType.Equals(accType) &&
                                             matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                            )
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_CH";

                                        }




                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value.ToString() == "")
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            // radGridCodingTemplate.Rows[row].Cells[14].Value = "";
                                        }


                                    }



                                    #endregion
                                }
                            }
                            else if (matchedData[j][7].ToString().ToLower().Equals("matches"))
                            {
                                if (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Equals(matchedData[j][0].ToLower()))
                                {
                                    #region Match found ... update datagridview
                                    radGridCodingTemplate.Rows[row].Cells[0].Value = true;
                                    radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][6];
                                    radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
                                    radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
                                    radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
                                    radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
                                    radGridCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];
                                    radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                    #endregion
                                }
                            }
                        }

                        #endregion
                    }

                    #region cmt

                    //for (int row = 0; row < dataGridViewPreviewCodingTemplate.Rows.Count; row++)
                    //{
                    //    for (int j = 0; j < matchedData.Count; j++)
                    //    {
                    //        if (matchedData[j][6].ToString().ToLower().Equals("contains"))
                    //        {
                    //            if (dataGridViewPreviewCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
                    //                || matchedData[j][0].ToLower().Contains(dataGridViewPreviewCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower()))
                    //            {
                    //                #region Contains found ... update datagridview
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[0].Value = true;
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][1];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][2];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][3];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][4];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][5];
                    //                #endregion
                    //            }
                    //        }
                    //        else if (matchedData[j][6].ToString().ToLower().Equals("matches"))
                    //        {
                    //            if (dataGridViewPreviewCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Equals(matchedData[j][0].ToLower()))
                    //            {
                    //                #region Match found ... update datagridview
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[0].Value = true;
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][1];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][2];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][3];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][4];
                    //                dataGridViewPreviewCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][5];
                    //                #endregion
                    //            }
                    //        }
                    //    }
                    //}


                    //Axis 10.0

                    //for (int row = 0; row < radGridCodingTemplate.Rows.Count; row++)
                    //{
                    //    for (int j = 0; j < matchedData.Count; j++)
                    //    {
                    //        if (matchedData[j][6].ToString().ToLower().Equals("contains"))
                    //        {
                    //            if (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
                    //                || matchedData[j][0].ToLower().Contains(radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower()))
                    //            {
                    //                #region Contains found ... update datagridview
                    //                radGridCodingTemplate.Rows[row].Cells[0].Value = true;
                    //                radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][1];
                    //                radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][2];
                    //                radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][3];
                    //                radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][4];
                    //                radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][5];
                    //                #endregion
                    //            }
                    //        }
                    //        else if (matchedData[j][6].ToString().ToLower().Equals("matches"))
                    //        {
                    //            if (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Equals(matchedData[j][0].ToLower()))
                    //            {
                    //                #region Match found ... update datagridview
                    //                radGridCodingTemplate.Rows[row].Cells[0].Value = true;
                    //                radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][1];
                    //                radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][2];
                    //                radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][3];
                    //                radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][4];
                    //                radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][5];
                    //                #endregion
                    //            }
                    //        }
                    //    }
                    //}


                    #endregion cmt

                    #endregion

                    #region Not Required Commented Add new row to datagridview as it is added newly in Edit Template

                    //ArrayList newRows = GetNewlyAddedData(CommonUtilities.GetInstance().NewRowEditTemplate);

                    //Reset it to string.empty
                    //CommonUtilities.GetInstance().NewRowEditTemplate = string.Empty;

                    //foreach (List<string> dataRow in newRows)
                    //{
                    //    AddNewDataRow(dataRow);
                    //}


                    #endregion
                }
                catch
                { }
            }
            else
            {
            }

            //adding code for refrashing
            if (selectionFlag)
            {
                DataGridView dgv = new DataGridView();
                //Axis 10.0
                RadGridView radDgv = new RadGridView();


                string codingTemplateName = string.Empty;

                codingTemplateName = comboBoxPreviewCodingTemplate.SelectedItem.ToString();

                CommonUtilities.GetInstance().SelectedCodingTemplate = codingTemplateName;

                dataGridViewPreviewCodingTemplate.Rows.Clear();

                dgv = CreateDatasetForCodingTemplatePreview(CommonUtilities.GetInstance().BrowseFileName, codingTemplateName, true);

                //Axis 10.0

                while (this.radGridCodingTemplate.Rows.Count > 0)
                {
                    this.radGridCodingTemplate.Rows.RemoveAt(this.radGridCodingTemplate.Rows.Count - 1);
                }
                radDgv = CreateRADDatasetForCodingTemplatePreview(CommonUtilities.GetInstance().BrowseFileName, codingTemplateName, true);



                if (!radDgv.Rows.Count.Equals(0))
                {

                    radGridCodingTemplate.DataSource = radDgv.DataSource;
                    this.buttonPreviewEdit.Enabled = true;
                    radGridCodingTemplate.Refresh();
                    CommonUtilities.GetInstance().StatementDataTable = null;
                }
                else
                {
                    this.buttonPreviewEdit.Enabled = false;
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0135"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radGridCodingTemplate.DataSource = radDgv.DataSource;
                    radGridCodingTemplate.Refresh();
                }
                selectionFlag = true;
            }

            // comboBoxPreviewCodingTemplate_SelectedIndexChanged(sender,e);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataRow"></param>
        private void AddNewDataRow(List<string> dataRow)
        {
            #region Fill newly created row with data from file

            try
            {
                dr = new DataGridViewRow();

                //Axis 10.0 

                //tDr= new GridViewRowInfo(this.radGridCodingTemplate.MasterView);

                for (int j = 0; j < 10; j++)
                {
                    switch (j)
                    {

                        case 0:
                            tCheckColumn = new GridViewCheckBoxColumn();

                            //tCheckColumn.SetValue = true;
                            dr.Cells.Add(checkCell);
                            break;

                        case 1:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;

                        case 2:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;

                        case 3:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;

                        case 4:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = custList;
                            comboCell.Value = dataRow[0];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;


                        case 5:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = CommonUtilities.GetInstance().ItemList;
                            comboCell.Value = dataRow[1];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;


                        case 6:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                            comboCell.Value = dataRow[2];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;

                        case 7:
                            checkCell = new DataGridViewCheckBoxCell();
                            checkCell.Value = dataRow[4];
                            dr.Cells.Add(checkCell);
                            break;


                        case 8:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = custList;
                            comboCell.Value = dataRow[3];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;

                        case 9:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);

                            break;
                    }
                }













                for (int j = 0; j < 10; j++)
                {
                    switch (j)
                    {

                        case 0:
                            checkCell = new DataGridViewCheckBoxCell();
                            checkCell.Value = true;
                            dr.Cells.Add(checkCell);
                            break;

                        case 1:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;

                        case 2:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;

                        case 3:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;

                        case 4:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = custList;
                            comboCell.Value = dataRow[0];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;


                        case 5:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = CommonUtilities.GetInstance().ItemList;
                            comboCell.Value = dataRow[1];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;


                        case 6:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                            comboCell.Value = dataRow[2];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;

                        case 7:
                            checkCell = new DataGridViewCheckBoxCell();
                            checkCell.Value = dataRow[4];
                            dr.Cells.Add(checkCell);
                            break;


                        case 8:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = custList;
                            comboCell.Value = dataRow[3];
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;

                        case 9:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);

                            break;
                    }
                }
                dataGridViewPreviewCodingTemplate.Rows.Add(dr);
            }
            catch
            { }

            #endregion
        }


        /// <summary>
        /// Return ArrayList of newly added data row in Edit Template
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private ArrayList GetNewlyAddedData(string newdata)
        {
            try
            {
                string[] numRecords = Regex.Split(newdata, "\r\n");

                List<string> lData = null;
                ArrayList dataList = new ArrayList();

                foreach (string data in numRecords)
                {
                    lData = new List<string>();
                    string[] fetchdata = Regex.Split(data, "~");

                    foreach (string fetchedData in fetchdata)
                    {
                        lData.Add(fetchedData);
                    }

                    dataList.Add(lData);

                }

                return dataList;
            }
            catch
            {
                return null;
            }
        }

       
        public string[] csvheader(string filename)
        {

            string[] arr = new string[0];
            if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("csv"))
            {
                string[] csvdata = File.ReadAllLines(filename);

                foreach (string data in csvdata)
                {
                    string headers = data.ToString();
                    string[] array = headers.Split(',');
                    arr = new string[array.Length];

                    for (int i = 0; i < array.Length; i++)
                    {
                        if (array[i].Contains("Date"))
                        {
                            arr = array;
                            return arr;

                        }

                    }

                }

            }
            return arr = null;
        }
        /// <summary>
        /// Return ArrayList of QIF file data
        /// </summary>
        /// <param name="fileName">Browse Statement file</param>
        /// <returns></returns>

        //Axis 11.0 Bug No.132
        private ArrayList GettxtFileData(string fileName)
        {
            ArrayList dataQIFList = new ArrayList();

            try
            {
                int cnt = 0;
                string[] qifData = File.ReadAllLines(fileName);
                if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("xls") || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("xlsx"))
                {
                    cnt = 1;
                    ComboBox comboBoxWorkSheet = new ComboBox();
                    comboBoxWorkSheet.DataSource = DataProcessingBlocks.ExcellUtility.GetInstance().GetExcelSheetNames();
                    string value = "Data";
                    DataTable dt = DataProcessingBlocks.ExcellUtility.GetInstance().GetDataBySheetName(value);
                    int colcnt = dt.Columns.Count - 1;

                    string[] a = new string[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int j = 0; j < colcnt; j++)
                        {

                            string dtval = dt.Rows[i].ItemArray[j].ToString();
                            // Axis 411
                            string[] dtvalseperator = dtval.Split(' ', '\t');
                            if (j == 1)
                            {
                                string val = dtvalseperator[0];
                                dtval = val;//.Substring(0, 10);
                                if (val.Length > 10)
                                {
                                    dtval = dtval.Substring(0, 10);
                                }
                            }
                            a[i] += dtval + "\t";
                        }
                    }
                    qifData = a;

                }
                else
                {
                    if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("csv"))
                    {
                        cnt = 0;

                    }
                    qifData = File.ReadAllLines(fileName);
                }

                List<string> dataQIF = null;

                bool flag = true;


                foreach (string data in qifData)
                {
                    if (cnt == 0)
                    {
                        cnt++;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(data.Trim()))
                        {
                            if (!data.Trim().StartsWith("!"))
                            {
                                if (!data.Trim().StartsWith("^"))
                                {
                                    #region Actual Data

                                    //for first line of record create new List object
                                    if (flag.Equals(true))
                                        dataQIF = new List<string>();

                                    flag = false;
                                    string dataOfList = string.Empty;

                                    if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("csv"))
                                    {
                                        dataOfList = data.Replace(',', '~');
                                        dataOfList = dataOfList.Replace('\"', ' ');
                                    }
                                    else
                                    {
                                        dataOfList = data.Replace('\t', '~');
                                    }

                                    dataQIFList.Add(dataOfList);


                                    #endregion
                                }
                                else
                                {
                                    #region Completion of Record indicated by "^"
                                    flag = true;
                                    dataQIFList.Add(dataQIF);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region for Line i.e. !Type:Bank
                                #endregion
                            }
                        }
                    }
                }

                return dataQIFList;
            }
            catch
            {
                dataQIFList = null;
                return dataQIFList;
            }
        }
        //Axis 11.0 Bug No.132

     
        public RadGridView CreateRADDatasetForCodingTemplatePreview(string fileName, string codingTemplateName, bool flag)
        {
            selectionFlag = flag;
            GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(this.radGridCodingTemplate.MasterView);
            //get data for header row
            string[] headerRow = CommonUtilities.GetInstance().HeaderForCodingTemplate;

            //QIF file data consist of special keyword which gives meaning to data in file
            string[] typeOfDataForQIF = CommonUtilities.GetInstance().TypeOfDataForQIF;

            //OFX file data consist of special keyword which gives meaning to data in file
            string[] typeOfDataForOFX = CommonUtilities.GetInstance().TypeOfDataForOFX;


            string[] typeOfDataForTXT = CommonUtilities.GetInstance().TypeOfDataForTXT;

            string[] typeOfDataForcsv = CommonUtilities.GetInstance().TypeOfDataForcsv;

            //Axis 10.0 Get Expense Account List
            radExpenseAccountList = new QBAccountQueryListCollection();

            object tmpExpAccList = new object();
            radExpenseAccountList = new QBAccountQueryListCollection();
            radExpenseAccountList.PopulateExpenseAccountQuery(CommonUtilities.GetInstance().CompanyFile);

            if (this.ConnectedSoft == Constants.QBstring)
            {
                //463 assign null 
                radCustomerList = null;
                radEmployeeList = null;
                radVendorList = null;
                radOtherList = null;
                classList = null;
                radCustomerList = new QBCustomerListCollection();
                radEmployeeList = new QBEmployeeListCollection();
                radVendorList = new QBVendorListCollection();
                radOtherList = new QBOtherListCollection();
                classList = new QBClassListCollection();
                List<string> combinedCustomerList = new List<string>();

                //check whether CustomerList is null or not
                if (radCustList == null)
                {
                    #region Create customer list

                    List<string> radallCustList = radCustomerList.GetAllCustomerList(CommonUtilities.GetInstance().CompanyFile);
                    custjobList = new List<string>();
                    //463
                    //custjobList = radCustomerList.GetAllCustomerList(CommonUtilities.GetInstance().CompanyFile);
                    custjobList = radallCustList;
                    radCustList = new List<string>();
                    if (radEmpList == null)
                        radEmpList = radEmployeeList.GetAllEmployeeList(CommonUtilities.GetInstance().CompanyFile);
                    if (radVendList == null)
                        radVendList = radVendorList.GetAllVendorList(CommonUtilities.GetInstance().CompanyFile);
                    if (radOthrList == null)
                        radOthrList = radOtherList.GetAllOtherNameList(CommonUtilities.GetInstance().CompanyFile);
                    if (clasList == null)
                        clasList = classList.GetAllClassList(CommonUtilities.GetInstance().CompanyFile);

                    // commented for Axis 324        combinedCustomerList.AddRange(CommonUtilities.GetInstance().CustomerListWithType);
                    combinedCustomerList.AddRange(CommonUtilities.GetInstance().EmployeeListWithType);
                    combinedCustomerList.AddRange(CommonUtilities.GetInstance().VendorListWithType);
                    combinedCustomerList.AddRange(CommonUtilities.GetInstance().OtherListWithType);
                    combinedCustomerList.Insert(0, "<None>");

                    //  CommonUtilities.GetInstance().CombineCustomerList1 = combinedCustomerList;
                    CommonUtilities.GetInstance().CombineCustomerList = combinedCustomerList;

                    //CommonUtilities.GetInstance().AllCustomerList = combinedCustomerList;
                    //CommonUtilities.GetInstance().AllCustomerList.AddRange(radallCustList);
                    allcust.AddRange(radallCustList);
                    allcust.AddRange(CommonUtilities.GetInstance().EmployeeListWithType);
                    allcust.AddRange(CommonUtilities.GetInstance().VendorListWithType);
                    allcust.AddRange(CommonUtilities.GetInstance().OtherListWithType);
                    allcust.Insert(0, "<None>");

                    foreach (string empName in radEmpList)
                    {
                        if (!radCustList.Contains(empName))
                        {
                            radCustList.Add(empName);
                        }
                    }
                    foreach (string vendName in radVendList)
                    {
                        if (!radCustList.Contains(vendName))
                        {
                            radCustList.Add(vendName);
                        }
                    }
                    foreach (string custName in radallCustList)
                    {
                        radCustList.Add(custName);
                    }
                    radCustList.Insert(0, "<None>");

                    #endregion
                }
            }
            CommonUtilities.GetInstance().allcust = allcust;
            //if (!flag)
            if (this.radGridCodingTemplate.ColumnCount < 12)
            {
                FillRADHeaderData(headerRow, custjobList);
            }

            //Set comboBoxPreviewCodingTemplate selected item to codingTemplateName get from TI form
            comboBoxPreviewCodingTemplate.SelectedItem = codingTemplateName;

            if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("ofx")
               || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qfx"))
            {
                #region Parsing logic for OFX AND QFX

                //set flagQBO to false
                flagQBO = false;

                #region 1]get all List of data from browse ofx or qfx file

                //create object of ArrayList to store all Lists of data from browse OFX or QFX
                ArrayList fileData = new ArrayList();

                fileData = GetListsOfFileData(fileName);

                #endregion



                #region 3]For All data create row in datagridview

                if (!fileData.Count.Equals(0))
                {
                    Hashtable hashTableInfo;
                    int rowCount = 0;

                    foreach (List<string> dataList in fileData)
                    {

                        #region Create hashtable from fileData

                        hashTableInfo = new Hashtable();
                        hashTableInfo = GetDataInKeyValueFormat(dataList, typeOfDataForOFX);

                        #endregion

                        #region Fill Datarow of datagridview

                        //check in Codings.xml file
                        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                        {
                            if (this.ConnectedSoft == Constants.QBstring)
                            {
                                //fill dataGridview with file data
                                FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                            }
                            //axis 11 pos 
                            else if (this.ConnectedSoft == Constants.QBPOSstring)
                            {
                                FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                            }
                            else if (this.ConnectedSoft == Constants.QBOnlinestring)
                            {
                                FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                            }

                        }
                        else
                        {
                            if (!CommonUtilities.GetInstance().IsImportProcess)
                            {
                                #region If no data found in Coding.xml
                                if (this.ConnectedSoft == Constants.QBstring)
                                {
                                    //fill dataGridview with file data
                                    FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                                }
                                //axis 11 pos
                                else if (this.ConnectedSoft == Constants.QBPOSstring)
                                {
                                    FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                                }

                                else if (this.ConnectedSoft == Constants.QBOnlinestring)
                                {
                                    FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                                }

                                #endregion
                            }
                        }

                        #endregion

                        //increment rowCount
                        rowCount++;
                    }
                }
                //fileData = null;

                #endregion

                #endregion
            }
            else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qbo"))
            {

                #region Parsing logic for New QBO

                //set flagQBO to false
                flagQBO = false;

                #region 1]get all List of data from browse QBO

                //create object of ArrayList to store all Lists of data from browse OFX or QFX
                ArrayList fileData = new ArrayList();

                //fileData = GetListsOfFileData(fileName);
                fileData = GetListsOfQBOFileData(fileName);

                #endregion

                #region Not required commented code 2]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

                //consist of coding template matched name list
                //ArrayList matchList = new ArrayList();

                //matchList = GetMatchListOfData(fileData, codingTemplateName);


                #endregion

                #region 3]For All data create row in datagridview

                if (!fileData.Count.Equals(0))
                {

                    Hashtable hashTableInfo;

                    int rowCount = 0;

                    foreach (List<string> dataList in fileData)
                    {

                        #region Create hashtable from fileData

                        hashTableInfo = new Hashtable();
                        hashTableInfo = GetDataInKeyValueFormat(dataList, typeOfDataForOFX);

                        #endregion

                        #region Fill Datarow of datagridview

                        //check in Codings.xml file
                        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                        {
                            if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBOnlinestring)
                            {
                                //fill dataGridview with file data
                                FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                            }
                            //axis 11 pos 
                            else if (this.ConnectedSoft == Constants.QBPOSstring)
                            {
                                //fill dataGridview with file data
                                FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                            }

                            #region Not Required Commented if data found in Coding.xml
                            //if (rowCount.Equals(fileData.Count - 1))
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
                            //}
                            //else
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
                            //}

                            #endregion

                        }
                        else
                        {
                            if (!CommonUtilities.GetInstance().IsImportProcess)
                            {
                                #region If no data found in Coding.xml
                                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBOnlinestring)
                                {
                                    //fill dataGridview with file data
                                    FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                                }
                                //axis11 pos
                                else if (this.ConnectedSoft == Constants.QBPOSstring)
                                {
                                    //fill dataGridview with file data
                                    FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                                }

                                #endregion
                            }
                        }

                        #endregion

                        //increment rowCount
                        rowCount++;
                    }
                }


                #endregion

                #endregion
                //466
                //  fileData = null;

            }

            else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qif"))
            {
                #region Parsing Logic for QIF file

                //set flagQBO to false
                flagQBO = false;

                #region 1]get all List of data from browse QIF file

                ArrayList qifFileData = new ArrayList();
                ArrayList qifFileData1 = new ArrayList();
                List<string> strlist = new List<string>();
                qifFileData = GetQifFileData(fileName);

                #endregion

                #region BugNo:321


                for (int i = 0; i < qifFileData.Count; i++)
                {
                    if (qifFileData[i] != null)
                    {
                        List<string> obj = (List<string>)qifFileData[i];
                        qifFileData1 = new ArrayList();
                        for (int j = 0; j < 1; j++)
                        {
                            qifFileData1.Add((object)obj[j]);
                        }
                        string qdate = qifFileData1[0].ToString();
                        //string s = qdate.IndexOf(

                        if (qdate.Contains("-"))
                        {
                            string[] array_date = qdate.ToString().Split('-');
                            if (Convert.ToInt32(array_date[0].Remove(0, 2)) > 12 && array_date[2].Length != 4)
                            {
                                DateFlag = true;
                            }
                        }
                        else if (qdate.Contains("/"))
                        {
                            string[] array_date = qdate.ToString().Split('/');
                            //string arr1 = array_date[0].Remove(0, 2);
                            //var len = arr1.Length; int first = Convert.ToInt32(arr1); int c = 12;
                            if (Convert.ToInt32(array_date[0].Remove(0, 2)) > 12 && array_date[2].Length != 4)
                            {
                                DateFlag = true;
                            }
                        }
                    }
                }
                #endregion

                #region 3]For All data create row in datagridview

                //if file contains data
                if (!qifFileData.Count.Equals(0))
                {
                    Hashtable hashTableInfo;
                    int rowCount = 0;

                    foreach (List<string> mdataList in qifFileData)
                    {
                        #region Create hashtable from qifFileData

                        hashTableInfo = new Hashtable();
                        hashTableInfo = GetDataInKeyValueFormat(mdataList, typeOfDataForQIF);

                        #endregion

                        #region Fill newly created row with data from file

                        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                        {
                            if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBOnlinestring)
                            {
                                //fill dataGridview with file data
                                FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);

                            }
                            //axis 11 pos 
                            else if (this.ConnectedSoft == Constants.QBPOSstring)
                            {
                                //fill dataGridview with file data
                                FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);

                            }

                            #region Not required Commented if data found in Coding.xml
                            //if (rowCount.Equals(qifFileData.Count - 1))
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
                            //}
                            //else
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
                            //}

                            #endregion

                        }
                        else
                        {
                            if (!CommonUtilities.GetInstance().IsImportProcess)
                            {
                                #region If no data found in Coding.xml

                                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBOnlinestring)
                                {
                                    //fill dataGridview with file data
                                    if (hashTableInfo != null)
                                    {
                                        FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                                    }
                                }
                                //axis 11 pos
                                else if (this.ConnectedSoft == Constants.QBstring)
                                {
                                    //fill dataGridview with file data
                                    FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList, DateFlag);
                                }

                                #endregion
                            }
                        }
                        #endregion

                        //increment rowCount
                        rowCount++;

                    }
                }

                #endregion
                #endregion
                //463
                //qifFileData = null;
                //qifFileData1 = null;


            }

            else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("txt") || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("xls") || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("xlsx") || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("csv"))
            {
                #region Parsing Logic for QIF file
                Hashtable hashTableInfo = new Hashtable();
                //set flagQBO to false
                flagQBO = false;

                #region 1]get all List of data from browse QIF file

                ArrayList txtFileData = new ArrayList();

                txtFileData = GettxtFileData(fileName);

                #endregion



                #region 3]For All data create row in datagridview
                int rowCount = 0;
                //if file contains data
                if (!txtFileData.Count.Equals(0))
                {
                    //  Hashtable hashTableInfo=new Hashtable();



                    foreach (string s in txtFileData)
                    {
                        #region Create hashtable from qifFileData

                        hashTableInfo = new Hashtable();
                        if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("csv"))
                        {
                            typeOfDataForcsv = csvheader(fileName);
                            if (typeOfDataForcsv == null)
                            {
                                typeOfDataForcsv = CommonUtilities.GetInstance().TypeOfDataForcsv;
                            }
                            // typeOfDataForcsv = "";
                            hashTableInfo = GetDataInKeyValueFormattxt(s, typeOfDataForcsv);
                        }
                        else
                        {

                            hashTableInfo = GetDataInKeyValueFormattxt(s, typeOfDataForTXT);
                        }

                        #endregion

                        #region Fill newly created row with data from file

                        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                        {
                            //axis 11 pos
                            if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                            {
                                //fill dataGridview with file data
                                FillRADGridRowWithFileData_TXT(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);

                            }

                        }
                        else
                        {
                            if (!CommonUtilities.GetInstance().IsImportProcess)
                            {
                                #region If no data found in Coding.xml
                                //axis11 pos
                                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                                {
                                    //fill dataGridview with file data
                                    FillRADGridRowWithFileData_TXT(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);
                                }

                                #endregion
                            }
                        }
                        #endregion

                        //increment rowCount
                        rowCount++;

                    }
                }
                rowCount = 0;

                #endregion
                #endregion

                //463
                //  txtFileData = null;
                ////  hashTableInfo = null;
                //  typeOfDataForcsv = null;

            }



            selectionFlag = true;
            matchData(codingTemplateName, false, CommonUtilities.GetInstance().BrowseFileExtension);

            return radGridCodingTemplate;


        }

        public RadGridView matchData(string codingTemplateName, bool flag, string extn)
        {
            #region for mathing button
            // selectionFlag = false;

            //get keyword information from Coding.xml file based on matching codingTemplateName 
            //CommonUtilities.GetInstance().ConnectedSoftware = this.ConnectedSoft;
            List<string[]> matchedData = new List<string[]>();
            if (codingTemplateName != null)
            {
                matchedData = CommonUtilities.GetInstance().GetKeywordDataForCodingTemplate(codingTemplateName);
            }
            else
            {

                for (int i = 0; i < radGridCodingTemplate.RowCount; i++)
                {
                    if ((radGridCodingTemplate.Rows[i].Cells[4].Value != null && radGridCodingTemplate.Rows[i].Cells[4].Value.ToString() != "") || (radGridCodingTemplate.Rows[i].Cells[5].Value != null && radGridCodingTemplate.Rows[i].Cells[5].Value.ToString() != "") || (radGridCodingTemplate.Rows[i].Cells[6].Value != null && radGridCodingTemplate.Rows[i].Cells[6].Value.ToString() != "") || (radGridCodingTemplate.Rows[i].Cells[7].Value != null && radGridCodingTemplate.Rows[i].Cells[7].Value.ToString() != "") || (radGridCodingTemplate.Rows[i].Cells[8].Value != null && radGridCodingTemplate.Rows[i].Cells[8].Value.ToString() != "") || (radGridCodingTemplate.Rows[i].Cells[9].Value != null && radGridCodingTemplate.Rows[i].Cells[9].Value.ToString() != ""))
                    {
                        string[] data = new string[10];
                        data[0] = "";
                        if (radGridCodingTemplate.Rows[i].Cells[5].Value != null)
                        {
                            data[1] = radGridCodingTemplate.Rows[i].Cells[5].Value.ToString();
                        }
                        else
                        {
                            data[1] = "";
                        }
                        if (radGridCodingTemplate.Rows[i].Cells[6].Value != null)
                        {
                            data[2] = radGridCodingTemplate.Rows[i].Cells[6].Value.ToString();
                        }
                        else
                        {
                            data[2] = "";
                        }

                        if (radGridCodingTemplate.Rows[i].Cells[7].Value != null)
                        {
                            data[3] = radGridCodingTemplate.Rows[i].Cells[7].Value.ToString();
                        }
                        else
                        {
                            data[3] = "";
                        }
                        if (radGridCodingTemplate.Rows[i].Cells[8].Value != null)
                        {
                            data[4] = radGridCodingTemplate.Rows[i].Cells[8].Value.ToString();
                        }
                        else
                        {
                            data[4] = "";
                        }
                        if (radGridCodingTemplate.Rows[i].Cells[9].Value != null && radGridCodingTemplate.Rows[i].Cells[9].Value != "")
                        {
                            data[5] = radGridCodingTemplate.Rows[i].Cells[9].Value.ToString();
                        }
                        else
                        {
                            data[5] = "";
                        }
                        if (radGridCodingTemplate.Rows[i].Cells[10].Value != null)
                        {
                            data[6] = radGridCodingTemplate.Rows[i].Cells[10].Value.ToString();
                        }
                        else
                        {
                            data[6] = "";
                        }
                        if (extn == "xlsx")
                        {
                            if (radGridCodingTemplate.Rows[i].Cells[3].Value != null)
                            {
                                data[7] = radGridCodingTemplate.Rows[i].Cells[3].Value.ToString();
                            }
                            else
                            {
                                data[7] = "";
                            }
                        }
                        else
                        {
                            if (radGridCodingTemplate.Rows[i].Cells[4].Value != null)
                            {
                                data[7] = radGridCodingTemplate.Rows[i].Cells[4].Value.ToString();
                            }
                            else
                            {
                                data[7] = "";
                            }
                        }
                        data[8] = "";
                        data[9] = "";
                        if (extn == "xlsx")
                        {
                            radGridCodingTemplate.Rows[i].Cells[4].Value = "";
                        }
                        else
                        {
                            radGridCodingTemplate.Rows[i].Cells[3].Value = "";
                        }

                        matchedData.Add(data);

                    }
                }
            }


            //463
            //depositInfo = GetDepositData();

            //salesReceptInfo = GetSalesReceiptData();
            //checkInfo = GetChecktData();
            //creditCardChargeInfo = GetCreditCardChargeData();
            //creditCardCreditInfo = GetCreditCardCreditData();          



            try
            {


                for (int j = 0; j < matchedData.Count; j++)
                {

                    #region Modify datagridview as per matched Data

                    for (int row = 0; row < radGridCodingTemplate.Rows.Count; row++)
                    {
                        //794
                        if (matchedData[j][8].ToString().ToLower().Equals("contains"))
                        {

                            if ((radGridCodingTemplate.Rows[row].Cells[3].Value.ToString() != "" && radGridCodingTemplate.Rows[row].Cells[3].Value.ToString() != string.Empty)
                                && (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
                                || matchedData[j][0].ToLower().Contains(radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower()))
                                )
                            {
                                #region Contains found ... update datagridview

                                radGridCodingTemplate.Rows[row].Cells[0].Value = true;
                                //radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][4];
                                radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
                                radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
                                radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
                                radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
                                if (extn != "xlsx")
                                {
                                    radGridCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];

                                    radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][7];

                                }
                                else
                                {
                                    var ma = matchedData[j][5];
                                    decimal a;
                                    if (decimal.TryParse(ma, out a))
                                    {
                                        a = 1;
                                    }
                                    else
                                    {
                                        a = 2;
                                    }
                                    if (a == 2)
                                    {
                                    }
                                    else
                                    {

                                        radGridCodingTemplate.Rows[j].Cells[11].Value = matchedData[j][5];
                                    }
                                    radGridCodingTemplate.Rows[row].Cells[9].Value = "";

                                }

                                radGridCodingTemplate.Rows[row].Cells[10].Value = matchedData[j][6];
                                //   radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][7];
                                radGridCodingTemplate.Rows[row].Cells[12].Value = matchedData[j][8];

                                if (creditAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
                                {
                                    if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                        tranType = "CreditCardCharge";
                                    else
                                        tranType = "CreditCardCredit";

                                }

                                if (bankAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
                                {
                                    if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                        tranType = "Check";
                                    else
                                        tranType = "Deposit";

                                }

                                string tmpMemo = "";
                                if (extn == "xlsx")
                                {
                                    tmpMemo = radGridCodingTemplate.Rows[row].Cells[3].Value.ToString();
                                }
                                else
                                {
                                    tmpMemo = radGridCodingTemplate.Rows[row].Cells[4].Value.ToString();
                                }

                                string tmpCustomerName = radGridCodingTemplate.Rows[row].Cells[5].Value.ToString();

                                string tmpItemName = radGridCodingTemplate.Rows[row].Cells[6].Value.ToString();

                                string tmpTax = radGridCodingTemplate.Rows[row].Cells[7].Value.ToString();

                                string depositeaccountTo = CommonUtilities.GetInstance().depositeaccountTO;


                                string tmpPayment = ""; ;

                                if (extn != "xlsx")
                                {

                                    if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                        tmpPayment = radGridCodingTemplate.Rows[row].Cells[11].Value.ToString();
                                    else
                                        tmpPayment = radGridCodingTemplate.Rows[row].Cells[11].Value.ToString();
                                }

                                string tmpBillable = string.Empty;

                                if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[10].Value.ToString()) == true)
                                    tmpBillable = "Billable";
                                if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[10].Value.ToString()) == false)
                                    tmpBillable = "NotBillable";



                                string tmpRefNo = radGridCodingTemplate.Rows[row].Cells[2].Value.ToString();

                                string curDate = radGridCodingTemplate.Rows[row].Cells[1].Value.ToString();

                                DateTime currentDate = DateTime.ParseExact(curDate, "MM-dd-yyyy", null);
                                String MyString_new = currentDate.ToString("dd-MM-yyyy");
                                currentDate = DateTime.ParseExact(MyString_new, "dd-MM-yyyy", null);


                                DateTime fromDate = currentDate.AddDays(-5);
                                DateTime toDate = currentDate.AddDays(5);


                                string matchString = string.Empty;
                                matchString = objImportSplashScreen.GetTransactionInformation(tranType, tmpRefNo, fromDate, toDate, accountType, selectedAccountType, tmpPayment);

                                string[] match = new string[4];

                                if (matchString != null)
                                {
                                    match = matchString.Split(':');

                                    matchedAmount = Convert.ToDouble(match[1].ToString());
                                    accType = match[2].ToString();

                                }
                                if (tranType == "Deposit")
                                {
                                    foreach (DataRow Dr in depositInfo.Rows)
                                    {
                                        string tmp = Dr[4].ToString();
                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(tmp);

                                        int dateResult = DateTime.Compare(currentDate, tmpDate);

                                        if (tmpPayment.ToLower().Equals(Dr[21].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[18].ToString().ToLower()) &&
                                             depositeaccountTo.ToLower().Equals(Dr[5].ToString().ToLower()) &&
                                             dateResult == 0

                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_D";
                                            tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();


                                        }
                                        #region Axis 10 commented code
                                        //tmpMemo.ToLower().Equals(Dr[34].ToString().ToLower()) &&
                                        //tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                        //tmpItemName.ToLower().Equals(Dr[42].ToString().ToLower()) &&
                                        //tmpTax.ToLower().Equals(Dr[52].ToString().ToLower()) &&
                                        //dateResult == 0
                                        #endregion

                                    }

                                    if (selectedAccountType.Equals(accType) &&
                                            matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                           )
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                        }
                                        //for 366
                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_S";

                                    }
                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }

                                        }
                                    }
                                    catch
                                    {
                                    }


                                }

                                if (tranType == "Check")
                                {


                                    foreach (DataRow Dr in checkInfo.Rows)
                                    {

                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(Dr[4].ToString());
                                        int dateResult = DateTime.Compare(currentDate, tmpDate);


                                        if (tmpMemo.ToLower().Equals(Dr[10].ToString().ToLower()) &&
                                             tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[27].ToString().ToLower()) &&
                                             tmpPayment.ToLower().Equals(Dr[6].ToString().ToLower()) &&
                                             tmpBillable.ToLower().Equals(Dr[26].ToString().ToLower()) &&
                                             dateResult == 0

                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_C";
                                            tmpTxnId = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();

                                        }



                                    }

                                    if (selectedAccountType.Equals(accType) && matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment))
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                        }

                                        //for 366
                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_C";

                                    }


                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }

                                        }
                                    }
                                    catch
                                    {
                                    }

                                }




                                if (tranType == "CreditCardCredit")
                                {

                                    foreach (DataRow Dr in creditCardCreditInfo.Rows)
                                    {
                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(Dr[3].ToString());
                                        int dateResult = DateTime.Compare(currentDate, tmpDate);

                                        if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
                                             tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
                                             tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
                                             tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
                                             dateResult == 0

                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_CR";
                                            tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();


                                        }




                                    }
                                    if (selectedAccountType.Equals(accType) &&
                                            matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                           )
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                        }
                                        //for 366
                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_CR";

                                    }


                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }
                                            // radGridCodingTemplate.Rows[row].Cells[14].Value = "";
                                        }
                                    }
                                    catch
                                    {

                                    }


                                }

                                if (tranType == "CreditCardCharge")
                                {
                                    foreach (DataRow Dr in creditCardChargeInfo.Rows)
                                    {
                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(Dr[3].ToString());
                                        int dateResult = DateTime.Compare(currentDate, tmpDate);

                                        if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
                                             tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
                                             tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
                                             tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
                                             dateResult == 0


                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_CH";
                                            tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();

                                        }

                                    }

                                    if (selectedAccountType.Equals(accType) &&
                                            matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                           )
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";

                                        }
                                        //for 366
                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_CH";

                                    }


                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }

                                        }
                                    }
                                    catch
                                    {
                                    }
                                }

                                #endregion
                            }

                        }
                        else if (matchedData[j][7].ToString().ToLower().Equals("matches"))
                        {
                            if ((radGridCodingTemplate.Rows[row].Cells[3].Value.ToString() != "" && radGridCodingTemplate.Rows[row].Cells[3].Value.ToString() != string.Empty)
                                && (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
                                     || matchedData[j][0].ToLower().Contains(radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower())))
                            {
                                #region Match found ... update datagridview
                                radGridCodingTemplate.Rows[row].Cells[0].Value = true;
                                // radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][6];
                                radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
                                radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
                                radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
                                radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
                                radGridCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];
                                // radGridCodingTemplate.Rows[row].Cells[10].Value = "";
                                if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("csv"))
                                {
                                    radGridCodingTemplate.Rows[row].Cells[2].Value = "";

                                }
                                if (creditAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
                                {
                                    if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                        tranType = "CreditCardCharge";
                                    else
                                        tranType = "CreditCardCredit";

                                }

                                if (bankAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
                                {
                                    if (radGridCodingTemplate.Rows[row].Cells[11].Style.ForeColor == Color.Red)
                                        tranType = "Check";
                                    else
                                        tranType = "Deposit";

                                }

                                string tmpMemo = radGridCodingTemplate.Rows[row].Cells[4].Value.ToString();

                                string tmpCustomerName = radGridCodingTemplate.Rows[row].Cells[5].Value.ToString();

                                string tmpItemName = radGridCodingTemplate.Rows[row].Cells[6].Value.ToString();

                                string tmpTax = radGridCodingTemplate.Rows[row].Cells[7].Value.ToString();

                                string tmpPayment = radGridCodingTemplate.Rows[row].Cells[11].Value.ToString();
                                string tmpBillable = string.Empty;

                                if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == true)
                                    tmpBillable = "Billable";
                                if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == false)
                                    tmpBillable = "NotBillable";
                                string tmpRefNo = "";

                                if (radGridCodingTemplate.Rows[row].Cells[2].Value.ToString() != null)
                                {
                                    tmpRefNo = radGridCodingTemplate.Rows[row].Cells[2].Value.ToString();
                                }
                                else
                                {
                                    tmpRefNo = "";
                                }

                                string curDate = radGridCodingTemplate.Rows[row].Cells[1].Value.ToString();
                                DateTime currentDate;
                                try
                                {
                                    string day = curDate.Substring(3, 2);
                                    string month = curDate.Substring(0, 2);
                                    string year = curDate.Substring(6, 4);
                                    string dt = day + "/" + month + "/" + year;
                                    currentDate = Convert.ToDateTime(dt);

                                }
                                catch (Exception ex)
                                {
                                    curDate = curDate + " 12:00:00 AM";
                                    currentDate = Convert.ToDateTime(curDate);

                                }

                                DateTime fromDate = currentDate.AddDays(-5);
                                DateTime toDate = currentDate.AddDays(5);



                                string matchString = string.Empty;
                                matchString = objImportSplashScreen.GetTransactionInformation(tranType, tmpRefNo, fromDate, toDate, accountType, selectedAccountType, tmpPayment);
                                string[] match = new string[4];
                                if (matchString != null)
                                {
                                    match = matchString.Split(':');
                                    matchedAmount = Convert.ToDouble(match[1].ToString());
                                    accType = match[2].ToString();

                                }
                                if (tranType == "Deposit")
                                {


                                    foreach (DataRow Dr in depositInfo.Rows)
                                    {
                                        string tmp = Dr[4].ToString();
                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(tmp);

                                        int dateResult = DateTime.Compare(currentDate, tmpDate);

                                        if (tmpMemo.ToLower().Equals(Dr[34].ToString().ToLower()) &&
                                             tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[42].ToString().ToLower()) &&
                                             tmpTax.ToLower().Equals(Dr[52].ToString().ToLower()) &&
                                             tmpPayment.ToLower().Equals(Dr[49].ToString().ToLower()) &&
                                             dateResult == 0

                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_S";
                                            tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();


                                        }


                                    }

                                    if (selectedAccountType.Equals(accType) &&
                                             matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                            )
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                        }
                                        //for 366
                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_S";

                                    }


                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }

                                        }
                                    }
                                    catch
                                    {
                                    }

                                }

                                if (tranType == "Check")
                                {


                                    foreach (DataRow Dr in checkInfo.Rows)
                                    {

                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(Dr[4].ToString());
                                        int dateResult = DateTime.Compare(currentDate, tmpDate);


                                        if (tmpMemo.ToLower().Equals(Dr[10].ToString().ToLower()) &&
                                             tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[27].ToString().ToLower()) &&
                                             tmpPayment.ToLower().Equals(Dr[6].ToString().ToLower()) &&
                                             tmpBillable.ToLower().Equals(Dr[26].ToString().ToLower()) &&
                                             dateResult == 0

                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_C";
                                            tmpTxnId = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();

                                        }


                                    }
                                    if (selectedAccountType.Equals(accType) &&
                                             matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                            )
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {

                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                        }
                                        //for 366

                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_C";

                                    }

                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }

                                        }
                                    }
                                    catch
                                    {
                                    }
                                }

                                if (tranType == "CreditCardCredit")
                                {


                                    foreach (DataRow Dr in creditCardCreditInfo.Rows)
                                    {
                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(Dr[3].ToString());
                                        int dateResult = DateTime.Compare(currentDate, tmpDate);

                                        if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
                                             tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
                                             tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
                                             tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
                                             dateResult == 0

                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_CR";
                                            tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();


                                        }


                                    }

                                    if (selectedAccountType.Equals(accType) &&
                                            matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                           )
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                        }
                                        //for 366
                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_CR";
                                    }

                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }

                                        }
                                    }
                                    catch
                                    {
                                    }
                                }

                                if (tranType == "CreditCardCharge")
                                {
                                    foreach (DataRow Dr in creditCardChargeInfo.Rows)
                                    {
                                        DateTime tmpDate = new DateTime();
                                        tmpDate = Convert.ToDateTime(Dr[3].ToString());
                                        int dateResult = DateTime.Compare(currentDate, tmpDate);

                                        if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
                                             tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
                                             tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
                                             tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
                                             tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
                                             dateResult == 0


                                            )
                                        {
                                            //for 366
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Imported";
                                            radGridCodingTemplate.Rows[row].Cells[14].Value = Dr[0].ToString() + "_CH";
                                            tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[14].Value.ToString();
                                        }

                                    }


                                    if (selectedAccountType.Equals(accType) &&
                                         matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
                                        )
                                    {
                                        if (flag == true)
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "Matched";
                                        }
                                        else
                                        {
                                            radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                        }
                                        //for 366
                                        radGridCodingTemplate.Rows[row].Cells[14].Value = match[0] + "_CH";

                                    }


                                    try
                                    {
                                        if (radGridCodingTemplate.Rows[row].Cells[13].Value == "" || radGridCodingTemplate.Rows[row].Cells[13].Value == null)
                                        {
                                            if (flag == true)
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "Unmatched";
                                            }
                                            else
                                            {
                                                radGridCodingTemplate.Rows[row].Cells[13].Value = "";
                                            }

                                        }
                                    }
                                    catch
                                    {
                                    }

                                }

                                #endregion
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error :" + exp.Message);
            }

            return radGridCodingTemplate;
            #endregion
        }

        /// <summary>
        /// This function parse Statement File
        /// Create DataGridview consist of parsed data from statement file
        /// </summary>
        /// <param name="fileName">Browse Statement File</param>
        /// <param name="codingTemplateName">Selected Coding Template Name</param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public DataGridView CreateDatasetForCodingTemplatePreview(string fileName, string codingTemplateName, bool flag)
        {

            selectionFlag = flag;

            //get data for header row
            string[] headerRow = CommonUtilities.GetInstance().HeaderForCodingTemplate;

            //QIF file data consist of special keyword which gives meaning to data in file
            string[] typeOfDataForQIF = CommonUtilities.GetInstance().TypeOfDataForQIF;

            //OFX file data consist of special keyword which gives meaning to data in file
            string[] typeOfDataForOFX = CommonUtilities.GetInstance().TypeOfDataForOFX;

            #region Set DataGridViewStyle
            cellStyle.Font = new Font(new FontFamily("Verdana"), 8.25F);
            cellStyle.ForeColor = Color.Black;
            #endregion

            //This method is used to Create Header for datagridview
            if (!flag)
                FillDataGridViewHeaderData(headerRow);

            if (this.ConnectedSoft == Constants.QBstring)
            {

                customerList = new QBCustomerListCollection();
                employeeList = new QBEmployeeListCollection();
                vendorList = new QBVendorListCollection();

                //check whether CustomerList is null or not
                if (custList == null)
                {
                    #region Create customer list

                    custList = customerList.GetAllCustomerList(CommonUtilities.GetInstance().CompanyFile);
                    if (empList == null)
                        empList = employeeList.GetAllEmployeeList(CommonUtilities.GetInstance().CompanyFile);
                    if (vendList == null)
                        vendList = vendorList.GetAllVendorList(CommonUtilities.GetInstance().CompanyFile);
                    foreach (string empName in empList)
                    {
                        if (!custList.Contains(empName))
                        {
                            custList.Add(empName);
                        }
                    }
                    foreach (string vendName in vendList)
                    {
                        if (!custList.Contains(vendName))
                        {
                            custList.Add(vendName);
                        }
                    }
                    custList.Insert(0, "<None>");

                    #endregion
                }
            }

            //Set comboBoxPreviewCodingTemplate selected item to codingTemplateName get from TI form
            comboBoxPreviewCodingTemplate.SelectedItem = codingTemplateName;

            if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("ofx")
                || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qfx"))
            {
                #region Parsing logic for OFX AND QFX

                //set flagQBO to false
                flagQBO = false;

                #region 1]get all List of data from browse ofx or qfx file

                //create object of ArrayList to store all Lists of data from browse OFX or QFX
                ArrayList fileData = new ArrayList();

                fileData = GetListsOfFileData(fileName);

                #endregion

                #region Not required commented code 2]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

                //consist of coding template matched name list
                //ArrayList matchList = new ArrayList();

                //matchList = GetMatchListOfData(fileData, codingTemplateName);


                #endregion

                #region 3]For All data create row in datagridview

                if (!fileData.Count.Equals(0))
                {

                    Hashtable hashTableInfo;

                    int rowCount = 0;

                    foreach (List<string> dataList in fileData)
                    {

                        #region Create hashtable from fileData

                        hashTableInfo = new Hashtable();
                        hashTableInfo = GetDataInKeyValueFormat(dataList, typeOfDataForOFX);

                        #endregion

                        #region Fill Datarow of datagridview

                        //check in Codings.xml file
                        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                        {
                            //axis11 pos
                            if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                            {
                                //fill dataGridview with file data
                                FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                            }

                            #region Not Required Commented if data found in Coding.xml
                            //if (rowCount.Equals(fileData.Count - 1))
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
                            //}
                            //else
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
                            //}

                            #endregion

                        }
                        else
                        {
                            if (!CommonUtilities.GetInstance().IsImportProcess)
                            {
                                #region If no data found in Coding.xml
                                //axis 11 pos
                                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                                {
                                    //fill dataGridview with file data
                                    FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                                }

                                #endregion
                            }
                        }

                        #endregion

                        //increment rowCount
                        rowCount++;
                    }
                }


                #endregion

                #endregion
            }
            else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qbo"))
            {
                #region Parsing logic for QBO file (use of xml file)

                //#region 1]Copy <OFX> data to new created xml file

                //string xmlFile = CreateXMLForQBO(fileName);

                //#endregion



                //try
                //{
                //    System.Xml.XmlDocument xdoc = new XmlDocument();

                //    //load created xml file
                //    xdoc.Load(xmlFile);

                //    flagQBO = false;

                //    //Get NodeList which consist of STMTTRN node
                //    XmlNodeList dataList = xdoc.GetElementsByTagName("STMTTRN");

                //    #region 2]Get ArrayList for qbo file data

                //    ArrayList qboDataList = new ArrayList();
                //    qboDataList = GetQBOFileData(dataList);

                //    #endregion

                //    #region Not required commented 3]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

                //    //consist of coding template matched name list
                //    //ArrayList matchList = new ArrayList();
                //    //matchList = GetQBOFileMatchData(qboDataList, codingTemplateName);

                //    #endregion

                //    #region 4]For All data create row in datagridview

                //    //if file contains data
                //    if (!qboDataList.Count.Equals(0))
                //    {
                //        Hashtable hashTableInfo;

                //        int rowCount = 0;

                //        foreach (List<string> mdataList in qboDataList)
                //        {
                //            #region Create hashtable from fileData

                //            hashTableInfo = new Hashtable();
                //            hashTableInfo = GetDataInKeyValueFormat(mdataList, typeOfDataForOFX);

                //            #endregion

                //            #region Fill newly created row with data from file

                //            if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                //            {
                //                if (this.ConnectedSoft == Constants.QBstring)
                //                {
                //                    //fill dataGridview with file data
                //                    FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                //                }
                //                #region Not required commented code if data found in Coding.xml
                //                //if (rowCount.Equals(qboDataList.Count - 1))
                //                //{
                //                //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
                //                //}
                //                //else
                //                //{
                //                //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, false);
                //                //}

                //                #endregion

                //            }
                //            else
                //            {
                //                if (!CommonUtilities.GetInstance().IsImportProcess)
                //                {
                //                    #region If no data found in Coding.xml
                //                    if (this.ConnectedSoft == Constants.QBstring)
                //                    {
                //                        //fill dataGridview with file data
                //                        FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                //                    }

                //                    #endregion
                //                }
                //            }

                //            #endregion

                //            //increment rowCount
                //            rowCount++;

                //        }
                //    }

                //    #endregion

                //    #region Delete xml file
                //    try
                //    {
                //       //// File.Delete(xmlFile);
                //    }
                //    catch
                //    {
                //    }
                //    #endregion

                //}
                //catch (FileNotFoundException ex)
                //{
                //    //set flagQBO flag true
                //    flagQBO = true;

                //    #region 1]get all List of data from browse ofx or qfx file

                //    //create object of ArrayList to store all Lists of data from browse OFX or QFX
                //    ArrayList fileData = new ArrayList();

                //    fileData = GetListsOfFileData(fileName);

                //    #endregion

                //    #region Not required commented 2]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

                //    //consist of coding template matched name list
                //    //ArrayList matchList = new ArrayList();

                //    //matchList = GetMatchListOfData(fileData, codingTemplateName);


                //    #endregion

                //    #region 3]For All data create row in datagridview

                //    if (!fileData.Count.Equals(0))
                //    {

                //        Hashtable hashTableInfo;

                //        int rowCount = 0;

                //        foreach (List<string> dataList in fileData)
                //        {

                //            #region Create hashtable for fileData

                //            hashTableInfo = new Hashtable();
                //            hashTableInfo = GetDataInKeyValueFormat(dataList, typeOfDataForOFX);

                //            #endregion

                //            #region Fill Datarow of datagridview

                //            if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                //            {
                //                if (this.ConnectedSoft == Constants.QBstring)
                //                {
                //                    //fill dataGridview with file data
                //                    FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                //                }

                //                #region Not required Commented code if data found in Coding.xml
                //                //if (rowCount.Equals(fileData.Count - 1))
                //                //{
                //                //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
                //                //}
                //                //else
                //                //{
                //                //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, false);
                //                //}

                //                #endregion

                //            }
                //            else
                //            {
                //                if (!CommonUtilities.GetInstance().IsImportProcess)
                //                {
                //                    #region If no data found in Coding.xml

                //                    if (this.ConnectedSoft == Constants.QBstring)
                //                    {
                //                        //fill dataGridview with file data
                //                        FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                //                    }

                //                    #endregion
                //                }
                //            }

                //            #endregion

                //            //increment rowCount
                //            rowCount++;

                //        }
                //    }


                //    #endregion
                //}
                //catch
                //{

                //}
                #endregion


                #region Parsing logic for New QBO

                //set flagQBO to false
                flagQBO = false;

                #region 1]get all List of data from browse QBO

                //create object of ArrayList to store all Lists of data from browse OFX or QFX
                ArrayList fileData = new ArrayList();

                //fileData = GetListsOfFileData(fileName);
                fileData = GetListsOfQBOFileData(fileName);

                #endregion

                #region Not required commented code 2]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

                //consist of coding template matched name list
                //ArrayList matchList = new ArrayList();

                //matchList = GetMatchListOfData(fileData, codingTemplateName);


                #endregion

                #region 3]For All data create row in datagridview

                if (!fileData.Count.Equals(0))
                {

                    Hashtable hashTableInfo;

                    int rowCount = 0;

                    foreach (List<string> dataList in fileData)
                    {

                        #region Create hashtable from fileData

                        hashTableInfo = new Hashtable();
                        hashTableInfo = GetDataInKeyValueFormat(dataList, typeOfDataForOFX);

                        #endregion

                        #region Fill Datarow of datagridview

                        //check in Codings.xml file
                        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                        {
                            //axis11 pos
                            if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                            {
                                //fill dataGridview with file data
                                FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                            }

                            #region Not Required Commented if data found in Coding.xml
                            //if (rowCount.Equals(fileData.Count - 1))
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
                            //}
                            //else
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
                            //}

                            #endregion

                        }
                        else
                        {
                            if (!CommonUtilities.GetInstance().IsImportProcess)
                            {
                                #region If no data found in Coding.xml
                                //axis 11 pos
                                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                                {
                                    //fill dataGridview with file data
                                    FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                                }

                                #endregion
                            }
                        }

                        #endregion

                        //increment rowCount
                        rowCount++;
                    }
                }


                #endregion

                #endregion


            }
            else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qif"))
            {
                #region Parsing Logic for QIF file

                //set flagQBO to false
                flagQBO = false;

                #region 1]get all List of data from browse QIF file

                ArrayList qifFileData = new ArrayList();

                qifFileData = GetQifFileData(fileName);

                #endregion

                #region Not required commented 2]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

                //consist of coding template matched name list
                //ArrayList matchList = new ArrayList();

                //matchList = GetQIFFileMatchData(qifFileData,codingTemplateName);

                #endregion

                #region 3]For All data create row in datagridview

                //if file contains data
                if (!qifFileData.Count.Equals(0))
                {
                    Hashtable hashTableInfo;
                    int rowCount = 0;

                    foreach (List<string> mdataList in qifFileData)
                    {
                        #region Create hashtable from qifFileData

                        hashTableInfo = new Hashtable();
                        hashTableInfo = GetDataInKeyValueFormat(mdataList, typeOfDataForQIF);

                        #endregion

                        #region Fill newly created row with data from file

                        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
                        {
                            //axis 11 pos
                            if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                            {
                                //fill dataGridview with file data
                                FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                            }

                            #region Not required Commented if data found in Coding.xml
                            //if (rowCount.Equals(qifFileData.Count - 1))
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
                            //}
                            //else
                            //{
                            //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
                            //}

                            #endregion

                        }
                        else
                        {
                            if (!CommonUtilities.GetInstance().IsImportProcess)
                            {
                                #region If no data found in Coding.xml
                                //axis 11 pos
                                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                                {
                                    //fill dataGridview with file data
                                    FillDataGridViewRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension);
                                }

                                #endregion
                            }
                        }
                        #endregion

                        //increment rowCount
                        rowCount++;

                    }
                }

                #endregion
                #endregion                

            }

            //get keyword information from Coding.xml file based on matching codingTemplateName 
            CommonUtilities.GetInstance().ConnectedSoftware = this.ConnectedSoft;
            List<string[]> matchedData = CommonUtilities.GetInstance().GetKeywordDataForCodingTemplate(codingTemplateName);

            for (int row = 0; row < dataGridViewPreviewCodingTemplate.Rows.Count; row++)
            {
                #region Modify datagridview as per matched Data
                for (int j = 0; j < matchedData.Count; j++)
                {
                    if (matchedData[j][7].ToString().ToLower().Equals("contains"))
                    {
                        if (dataGridViewPreviewCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
                            || matchedData[j][0].ToLower().Contains(dataGridViewPreviewCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower()))
                        {
                            #region Contains found ... update datagridview
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[0].Value = true;
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][6];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];

                            #endregion
                        }
                    }
                    else if (matchedData[j][7].ToString().ToLower().Equals("matches"))
                    {
                        if (dataGridViewPreviewCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Equals(matchedData[j][0].ToLower()))
                        {
                            #region Match found ... update datagridview
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[0].Value = true;
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][6];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
                            dataGridViewPreviewCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];
                            #endregion
                        }
                    }
                }

                #endregion
            }

            return dataGridViewPreviewCodingTemplate;
        }


        /// <summary>
        /// Generate DataGridView using Coding.xml file
        /// </summary>
        /// <param name="hashTableInfo"></param>
        /// <param name="p"></param>
        private void FillDataGridViewRowWithXMLFileData(Hashtable hashTableInfo, string fileExtension, string codingTemplateName, int rowCount, bool lastRow)
        {
            List<string[]> xmlData = CommonUtilities.GetInstance().GetXMLCodingData(codingTemplateName);

            try
            {
                bool flag = false;

                for (int i = 0; i < xmlData.Count; i++)
                {
                    if (rowCount.ToString().Equals(xmlData[i][2]))
                    {
                        #region RowNumber Tag from Coding.xml file found match
                        flag = true;

                        dr = new DataGridViewRow();
                        object result = null;

                        for (int j = 0; j < 11; j++)
                        {
                            #region Create column for dataRow
                            switch (j)
                            {

                                case 0:
                                    checkCell = new DataGridViewCheckBoxCell();
                                    checkCell.Value = true;
                                    dr.Cells.Add(checkCell);
                                    break;

                                case 1:
                                    if (!fileExtension.Equals("qif"))
                                    {
                                        if (hashTableInfo["DTUSER"].ToString().Length > 14)
                                        {
                                            result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["DTUSER"].ToString());
                                        }
                                        else
                                            result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["DTUSER"].ToString(), DateFlag);
                                    }
                                    else
                                    {
                                        if (hashTableInfo["D"].ToString().Length > 14)
                                        {
                                            result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["D"].ToString());
                                        }
                                        else
                                            result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["D"].ToString(), DateFlag);
                                    }
                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                            textCell.Value = result;
                                        else
                                            textCell.Value = string.Empty;

                                        dr.Cells.Add(textCell);
                                    }

                                    break;

                                case 2:
                                    if (fileExtension.Equals("qbo"))
                                    {
                                        result = hashTableInfo["FITID"];
                                    }
                                    else if (!fileExtension.Equals("qif"))
                                    {
                                        result = hashTableInfo["FITID"];
                                    }
                                    else
                                    {
                                        result = hashTableInfo["N"];
                                    }

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                            textCell.Value = result;
                                        else
                                            textCell.Value = string.Empty;

                                        dr.Cells.Add(textCell);
                                    }

                                    break;

                                case 3:
                                    if (!fileExtension.Equals("qif"))
                                    {
                                        result = hashTableInfo["NAME"];
                                    }
                                    else
                                    {
                                        result = hashTableInfo["P"];
                                    }

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                            textCell.Value = result;
                                        else
                                            textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);
                                    }

                                    break;
                                case 4:
                                    if (!fileExtension.Equals("qif"))
                                    {
                                        result = hashTableInfo["MEMO"];
                                    }
                                    else
                                    {
                                        result = hashTableInfo["M"];
                                    }

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                            textCell.Value = result;
                                        else
                                            textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);
                                    }

                                    break;

                                case 5:
                                    comboCell = new DataGridViewComboBoxCell();
                                    comboCell.DataSource = custList;
                                    //Set Name from Coding.xml
                                    comboCell.Value = xmlData[i][3];
                                    comboCell.Style = cellStyle;
                                    dr.Cells.Add(comboCell);
                                    break;


                                case 6:
                                    comboCell = new DataGridViewComboBoxCell();
                                    comboCell.DataSource = CommonUtilities.GetInstance().ItemList;
                                    //Set Account/Item from Coding.xml
                                    comboCell.Value = xmlData[i][4];
                                    comboCell.Style = cellStyle;
                                    dr.Cells.Add(comboCell);
                                    break;


                                case 7:
                                    comboCell = new DataGridViewComboBoxCell();
                                    comboCell.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                                    //Set tax from Coding.xml
                                    comboCell.Value = xmlData[i][5];
                                    comboCell.Style = cellStyle;
                                    dr.Cells.Add(comboCell);
                                    break;

                                case 8:
                                    comboCell = new DataGridViewComboBoxCell();
                                    comboCell.DataSource = custList;
                                    comboCell.Value = xmlData[i][6];
                                    comboCell.Style = cellStyle;
                                    dr.Cells.Add(comboCell);
                                    break;
                                case 9:

                                    if (!fileExtension.Equals("qif"))
                                    {
                                        if (CommonUtilities.GetInstance().IsImportProcess)
                                        {
                                            result = hashTableInfo["TRNAMT"];
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(hashTableInfo["TRNAMT"].ToString()))
                                            {
                                                if (hashTableInfo["TRNAMT"].ToString().Contains("-"))
                                                {
                                                    result = hashTableInfo["TRNAMT"].ToString().Replace("-", "");
                                                }
                                                else
                                                    result = hashTableInfo["TRNAMT"];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().IsImportProcess)
                                        {
                                            result = hashTableInfo["T"];
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(hashTableInfo["T"].ToString()))
                                            {
                                                if (hashTableInfo["T"].ToString().Contains("-"))
                                                {
                                                    result = hashTableInfo["T"].ToString().Replace("-", "");
                                                }
                                                else
                                                    result = hashTableInfo["T"];
                                            }
                                        }
                                    }

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                            textCell.Value = result;
                                        else
                                            textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);
                                    }

                                    break;
                                case 10:
                                    checkCell = new DataGridViewCheckBoxCell();
                                    //Set Billable status
                                    if (xmlData[i][7].Equals("Billable"))
                                        checkCell.Value = true;
                                    else
                                        checkCell.Value = false;

                                    dr.Cells.Add(checkCell);
                                    break;




                            }
                            #endregion
                        }
                        dataGridViewPreviewCodingTemplate.Rows.Add(dr);
                        #endregion

                    }
                }

                if (!flag)
                {
                    #region Rest of data from browse file which dont have match with Coding.xml file data

                    dr = new DataGridViewRow();
                    object result = null;

                    for (int j = 0; j < 11; j++)
                    {
                        #region Create column for dataRow
                        switch (j)
                        {

                            case 0:
                                checkCell = new DataGridViewCheckBoxCell();
                                checkCell.Value = false;
                                dr.Cells.Add(checkCell);
                                break;

                            case 1:
                                if (!fileExtension.Equals("qif"))
                                {
                                    if (hashTableInfo["DTUSER"].ToString().Length > 14)
                                    {
                                        result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["DTUSER"].ToString());
                                    }
                                    else
                                        result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["DTUSER"].ToString(), DateFlag);
                                }
                                else
                                {
                                    if (hashTableInfo["D"].ToString().Length > 14)
                                    {
                                        result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["D"].ToString());
                                    }
                                    else
                                        result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["D"].ToString(), DateFlag);
                                }
                                textCell = new DataGridViewTextBoxCell();
                                textCell.Style = cellStyle;
                                if (result != null)
                                {
                                    if (!result.Equals(string.Empty))
                                        textCell.Value = result;
                                    else
                                        textCell.Value = string.Empty;

                                    dr.Cells.Add(textCell);
                                }

                                break;

                            case 2:
                                if (fileExtension.Equals("qbo"))
                                {
                                    result = hashTableInfo["FITID"];
                                }
                                else if (!fileExtension.Equals("qif"))
                                {
                                    result = hashTableInfo["FITID"];
                                }
                                else
                                {
                                    result = hashTableInfo["N"];
                                }

                                textCell = new DataGridViewTextBoxCell();
                                textCell.Style = cellStyle;
                                if (result != null)
                                {
                                    if (!result.Equals(string.Empty))
                                        textCell.Value = result;
                                    else
                                        textCell.Value = string.Empty;

                                    dr.Cells.Add(textCell);
                                }

                                break;

                            case 3:
                                if (!fileExtension.Equals("qif"))
                                {
                                    result = hashTableInfo["NAME"];
                                }
                                else
                                {
                                    result = hashTableInfo["P"];
                                }

                                textCell = new DataGridViewTextBoxCell();
                                textCell.Style = cellStyle;
                                if (result != null)
                                {
                                    if (!result.Equals(string.Empty))
                                        textCell.Value = result;
                                    else
                                        textCell.Value = string.Empty;
                                    dr.Cells.Add(textCell);
                                }

                                break;
                            case 4:
                                if (!fileExtension.Equals("qif"))
                                {
                                    result = hashTableInfo["MEMO"];
                                }
                                else
                                {
                                    result = hashTableInfo["M"];
                                }

                                textCell = new DataGridViewTextBoxCell();
                                textCell.Style = cellStyle;
                                if (result != null)
                                {
                                    if (!result.Equals(string.Empty))
                                        textCell.Value = result;
                                    else
                                        textCell.Value = string.Empty;
                                    dr.Cells.Add(textCell);
                                }

                                break;

                            case 5:
                                comboCell = new DataGridViewComboBoxCell();
                                comboCell.DataSource = custList;
                                comboCell.Style = cellStyle;
                                dr.Cells.Add(comboCell);
                                break;


                            case 6:
                                comboCell = new DataGridViewComboBoxCell();
                                comboCell.DataSource = CommonUtilities.GetInstance().ItemList;
                                comboCell.Style = cellStyle;
                                dr.Cells.Add(comboCell);
                                break;


                            case 7:
                                comboCell = new DataGridViewComboBoxCell();
                                comboCell.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                                comboCell.Style = cellStyle;
                                dr.Cells.Add(comboCell);
                                break;

                            case 8:


                                comboCell = new DataGridViewComboBoxCell();
                                comboCell.DataSource = custList;
                                comboCell.Style = cellStyle;
                                dr.Cells.Add(comboCell);
                                break;
                            case 9:

                                if (!fileExtension.Equals("qif"))
                                {
                                    if (CommonUtilities.GetInstance().IsImportProcess)
                                    {
                                        result = hashTableInfo["TRNAMT"];
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(hashTableInfo["TRNAMT"].ToString()))
                                        {
                                            if (hashTableInfo["TRNAMT"].ToString().Contains("-"))
                                            {
                                                result = hashTableInfo["TRNAMT"].ToString().Replace("-", "");
                                            }
                                            else
                                                result = hashTableInfo["TRNAMT"];
                                        }
                                    }
                                }
                                else
                                {
                                    if (CommonUtilities.GetInstance().IsImportProcess)
                                    {
                                        result = hashTableInfo["T"];
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(hashTableInfo["T"].ToString()))
                                        {
                                            if (hashTableInfo["T"].ToString().Contains("-"))
                                            {
                                                result = hashTableInfo["T"].ToString().Replace("-", "");
                                            }
                                            else
                                                result = hashTableInfo["T"];
                                        }
                                    }
                                }

                                textCell = new DataGridViewTextBoxCell();
                                textCell.Style = cellStyle;
                                if (result != null)
                                {
                                    if (!result.Equals(string.Empty))
                                        textCell.Value = result;
                                    else
                                        textCell.Value = string.Empty;
                                    dr.Cells.Add(textCell);
                                }

                                break;
                            case 10:
                                checkCell = new DataGridViewCheckBoxCell();
                                dr.Cells.Add(checkCell);
                                break;


                        }
                        #endregion
                    }

                    dataGridViewPreviewCodingTemplate.Rows.Add(dr);

                    #endregion
                }

                if (lastRow)
                {
                    for (int i = 0; i < xmlData.Count; i++)
                    {
                        if (Convert.ToInt32(xmlData[i][2]) > rowCount)
                        {
                            #region RowNumber Tag from Coding.xml file found match
                            flag = true;

                            dr = new DataGridViewRow();

                            for (int j = 0; j < 11; j++)
                            {
                                #region Create column for dataRow
                                switch (j)
                                {

                                    case 0:
                                        checkCell = new DataGridViewCheckBoxCell();
                                        checkCell.Value = true;
                                        dr.Cells.Add(checkCell);
                                        break;

                                    case 1:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);
                                        break;

                                    case 2:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);

                                        break;

                                    case 3:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);
                                        break;

                                    case 4:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);
                                        break;

                                    case 5:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = custList;
                                        //Set Name from Coding.xml
                                        comboCell.Value = xmlData[i][3];
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;


                                    case 6:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = CommonUtilities.GetInstance().ItemList;
                                        //Set Account/Item from Coding.xml
                                        comboCell.Value = xmlData[i][4];
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;


                                    case 7:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                                        //Set tax from Coding.xml
                                        comboCell.Value = xmlData[i][5];
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;

                                    case 8:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = custList;
                                        comboCell.Value = xmlData[i][6];
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;
                                    case 9:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = string.Empty;
                                        dr.Cells.Add(textCell);

                                        break;




                                    case 10:
                                        checkCell = new DataGridViewCheckBoxCell();
                                        //Set Billable status
                                        if (xmlData[i][7].Equals("Billable"))
                                            checkCell.Value = true;
                                        else
                                            checkCell.Value = false;

                                        dr.Cells.Add(checkCell);
                                        break;

                                }
                                #endregion
                            }
                            dataGridViewPreviewCodingTemplate.Rows.Add(dr);
                            #endregion

                        }
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Return ArrayList of match data from file (comparision done with coding template name)
        /// </summary>
        /// <param name="qifFileData"></param>
        /// <param name="codingTemplateName"></param>
        /// <returns></returns>
        private ArrayList GetQIFFileMatchData(ArrayList qifFileData, string codingTemplateName)
        {
            ArrayList matchList = new ArrayList();

            try
            {
                foreach (List<string> listData in qifFileData)
                {
                    foreach (string data in listData)
                    {
                        if (data.ToLower().StartsWith("p"))
                        {
                            string[] name = Regex.Split(data, "~");
                            if (Convert.ToString(name[1]).ToLower().Contains(codingTemplateName.ToLower()))
                                matchList.Add(listData);
                        }
                    }
                }
            }
            catch
            {
                //error 
            }

            return matchList;
        }


        private void ValidateStyle(GridViewCellInfo cell)
        {
            cell.Style.Font = new Font(new FontFamily("Verdana"), 8.25F);
            cell.Style.CustomizeFill = true;
            cell.Style.BackColor = Color.Empty;
        }


        private void RADCellStyle(GridViewCellInfo cell)
        {
            cell.Style.Font = new Font(new FontFamily("Verdana"), 8.25F);
            cell.Style.ForeColor = Color.Black;
        }


        private void CellStyleRed(GridViewCellInfo cell)
        {
            cell.Style.Font = new Font(new FontFamily("Verdana"), 8.25F);
            cell.Style.ForeColor = Color.Red;
        }
        private void CellStyleYellow(GridViewCellInfo cell)
        {
            cell.Style.Font = new Font(new FontFamily("Verdana"), 8.25F);
            cell.Style.ForeColor = Color.DeepPink;
        }

        private void FillRADGridRowWithFileData(Hashtable hashTableInfo, string fileExtension, string codingTemplateName, List<string> custList, bool DateFlag)
        {
            string culture = CommonUtilities.GetInstance().CountryVersion;




            #region Fill newly created row with data from file

            // List<string[]> matchedData = CommonUtilities.GetInstance().GetKeywordDataForCodingTemplate(codingTemplateName);

            try
            {
                string[] data = new string[17];
                List<CommonUtilities.QIFDatasource> dataList = new List<CommonUtilities.QIFDatasource>();
                GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(this.radGridCodingTemplate.MasterView);


                object result = null;
                object payee = null;
                decimal t = 0;
                if (fileExtension.Equals("qif"))

                    payee = hashTableInfo["P"];

                #region If Coding Template is null.

                if (codingTemplateName == null)
                {
                    for (int j = 0; j < 13; j++)
                    {
                        switch (j)
                        {

                            case 0:
                                if (fileExtension.Equals("qif"))
                                {
                                    string chkValue = "true";
                                    data[0] = chkValue;

                                    rowInfo.Cells[0].Value = chkValue;
                                }

                                break;

                            case 1:
                                if (!fileExtension.Equals("qif") && !fileExtension.Equals("txt"))
                                {
                                    if (hashTableInfo["DTPOSTED"].ToString().Length > 14)
                                    {
                                        result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["DTPOSTED"].ToString());
                                    }
                                    else
                                        result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["DTPOSTED"].ToString(), DateFlag);

                                    rowInfo.Cells[1].Value = result;
                                }
                                else if (fileExtension.Equals("txt"))
                                {
                                    //{ "Bank Account", "Date", "Narrative", "Debit Amount", "Credit Amount", "Categories", "Serial" };
                                    if (hashTableInfo["Date"].ToString().Length > 14)
                                    {
                                        result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["Date"].ToString());
                                    }
                                    else
                                        result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["Date"].ToString(), DateFlag);

                                    rowInfo.Cells[1].Value = result;
                                }

                                else
                                {

                                    if (hashTableInfo["D"].ToString().Length > 14)
                                    {
                                        result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["D"].ToString());
                                        data[1] = result.ToString();

                                        rowInfo.Cells[1].Value = result;
                                    }
                                    else
                                    {
                                        result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["D"].ToString(), DateFlag);

                                        data[1] = result.ToString();

                                        rowInfo.Cells[1].Value = result;

                                    }
                                }

                                break;

                            case 2:
                                if (fileExtension.Equals("qbo"))
                                {
                                    result = hashTableInfo["FITID"];

                                    rowInfo.Cells[2].Value = result;

                                }
                                else if (!fileExtension.Equals("qif"))
                                {
                                    result = hashTableInfo["FITID"];

                                    rowInfo.Cells[2].Value = result;

                                }
                                else
                                {
                                    result = hashTableInfo["N"];
                                    data[5] = result.ToString();

                                    rowInfo.Cells[2].Value = result;

                                }

                                break;

                            case 3:
                                if (!fileExtension.Equals("qif"))
                                {
                                    result = hashTableInfo["NAME"];

                                    rowInfo.Cells[3].Value = result;

                                }
                                else
                                {
                                    result = hashTableInfo["P"];
                                    data[6] = result.ToString();

                                    rowInfo.Cells[3].Value = result;


                                }

                                break;

                            case 4:
                                if (!fileExtension.Equals("qif"))
                                {
                                    result = hashTableInfo["MEMO"];

                                    rowInfo.Cells[4].Value = result;

                                }
                                else
                                {
                                    result = hashTableInfo["M"];
                                    data[3] = result.ToString();

                                    rowInfo.Cells[4].Value = result;

                                }


                                break;


                            case 5:
                                GridViewCellInfo cell = rowInfo.Cells[5];

                                if (cell.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[5].Value = "";
                                }

                                break;


                            case 6:
                                GridViewCellInfo cell1 = rowInfo.Cells[6];

                                if (cell1.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[6].Value = "";
                                }

                                break;


                            case 7:
                                GridViewCellInfo cell2 = rowInfo.Cells[7];

                                if (cell2.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[7].Value = "";
                                }

                                break;

                            case 8:
                                GridViewCellInfo cell3 = rowInfo.Cells[8];

                                if (cell3.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[8].Value = "";
                                }

                                break;
                            case 9:

                                //reflectin in class colomun amout

                                //if (!fileExtension.Equals("qif"))
                                //{
                                //    if (CommonUtilities.GetInstance().IsImportProcess)
                                //    {
                                //        result = hashTableInfo["TRNAMT"];

                                //        rowInfo.Cells[9].Value = result;

                                //    }
                                //    else
                                //    {
                                //        if (!string.IsNullOrEmpty(hashTableInfo["TRNAMT"].ToString()))
                                //        {
                                //            if (hashTableInfo["TRNAMT"].ToString().Contains("-"))
                                //            {
                                //                result = hashTableInfo["TRNAMT"].ToString().Replace("-", "");
                                //                //   this.CellStyleRed(rowInfo.Cells[10]);


                                //                rowInfo.Cells[9].Value = result;

                                //            }
                                //            else
                                //            {
                                //                result = hashTableInfo["TRNAMT"];



                                //                rowInfo.Cells[9].Value = result;
                                //            }


                                //        }
                                //    }

                                //}
                                //else
                                //{
                                //    if (CommonUtilities.GetInstance().IsImportProcess)
                                //    {
                                //        result = hashTableInfo["T"];


                                //        rowInfo.Cells[9].Value = result;

                                //    }
                                //    else
                                //    {
                                //        if (!string.IsNullOrEmpty(hashTableInfo["T"].ToString()))
                                //        {
                                //            if (hashTableInfo["T"].ToString().Contains("-"))
                                //            {
                                //                result = hashTableInfo["T"].ToString().Replace("-", "");
                                //                data[2] = result.ToString();

                                //                //if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                //                this.CellStyleRed(rowInfo.Cells[9]);

                                //                //  rowInfo.Cells[9].Value = result;

                                //            }
                                //            else
                                //            {
                                //                result = hashTableInfo["T"];
                                //                data[2] = result.ToString();

                                //                //  rowInfo.Cells[9].Value = result;

                                //            }
                                //        }
                                //    }
                                //}

                                break;
                            case 10:
                                rowInfo.Cells[10].Value = false;

                                break;
                            case 11:
                                if (!fileExtension.Equals("qif"))
                                {
                                    result = hashTableInfo["TRNAMT"];
                                }
                                else
                                {
                                    result = hashTableInfo["T"];
                                }
                                result = result.ToString().Trim();
                                t = Convert.ToDecimal(result);

                                if (result != null)
                                {
                                    if (t < 0)
                                    {

                                        this.CellStyleRed(rowInfo.Cells[11]);
                                    }
                                }
                                rowInfo.Cells[11].Value = result;

                                break;

                            case 12:
                                result = "";
                                rowInfo.Cells[12].Value = result.ToString();
                                break;

                            case 13:
                                result = "";
                                rowInfo.Cells[13].Value = result.ToString();
                                break;
                        }
                    }
                }
                #endregion

                #region  If Coding Template is not null.
                else
                {
                    List<string[]> matchedData = CommonUtilities.GetInstance().GetKeywordDataForCodingTemplate(codingTemplateName);
                    for (int i = 0; i < matchedData.Count; i++)
                    {
                        for (int j = 0; j < 13; j++)
                        {
                            switch (j)
                            {

                                case 0:

                                    string chkValue = "true";
                                    data[0] = chkValue;

                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                    {
                                        try
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[0]);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            //MessageBox.Show(ex.Message);
                                        }
                                    }
                                    if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                        this.RADCellStyle(rowInfo.Cells[0]);

                                    rowInfo.Cells[0].Value = chkValue;


                                    break;

                                case 1:
                                    if (!fileExtension.Equals("qif"))
                                    {
                                        if (hashTableInfo["DTPOSTED"].ToString().Length > 14)
                                        {
                                            result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["DTPOSTED"].ToString());
                                        }
                                        else
                                            result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["DTPOSTED"].ToString(), DateFlag);

                                        rowInfo.Cells[1].Value = result;
                                    }
                                    else
                                    {
                                        if (hashTableInfo["D"].ToString().Length > 14)
                                        {

                                            result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["D"].ToString());
                                            data[1] = result.ToString();

                                            if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                            {
                                                if (payee != null)
                                                {
                                                    if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                        this.ValidateStyle(rowInfo.Cells[1]);
                                                }
                                            }
                                            if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                                this.RADCellStyle(rowInfo.Cells[1]);

                                            rowInfo.Cells[1].Value = result;

                                        }
                                        else
                                        {
                                            result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["D"].ToString(), DateFlag);

                                            data[1] = result.ToString();
                                            if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                            {
                                                if (payee != null)
                                                {
                                                    if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                        this.ValidateStyle(rowInfo.Cells[1]);
                                                }
                                            }
                                            if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                                this.RADCellStyle(rowInfo.Cells[1]);

                                            rowInfo.Cells[1].Value = result;

                                        }
                                    }

                                    break;

                                case 2:
                                    if (fileExtension.Equals("qbo"))
                                    {
                                        result = hashTableInfo["FITID"];
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[2]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[2]);

                                        rowInfo.Cells[2].Value = result;

                                    }
                                    else if (!fileExtension.Equals("qif"))
                                    {
                                        result = hashTableInfo["FITID"];
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[2]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[2]);

                                        rowInfo.Cells[2].Value = result;

                                    }
                                    else
                                    {
                                        result = hashTableInfo["N"];
                                        data[5] = result.ToString();
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[2]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[2]);

                                        rowInfo.Cells[2].Value = result;

                                    }

                                    break;

                                case 3:
                                    if (!fileExtension.Equals("qif"))
                                    {
                                        result = hashTableInfo["NAME"];
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[3]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[3]);

                                        rowInfo.Cells[3].Value = result;

                                    }
                                    else
                                    {
                                        result = hashTableInfo["P"];
                                        data[6] = result.ToString();
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[3]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[3]);

                                        rowInfo.Cells[3].Value = result;


                                    }

                                    break;

                                case 4:
                                    if (!fileExtension.Equals("qif"))
                                    {
                                        result = hashTableInfo["MEMO"];
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[4]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[4]);

                                        rowInfo.Cells[4].Value = result;

                                    }
                                    else
                                    {
                                        result = hashTableInfo["M"];
                                        data[3] = result.ToString();
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee != null)
                                            {
                                                if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    this.ValidateStyle(rowInfo.Cells[4]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[4]);

                                        rowInfo.Cells[4].Value = result;

                                    }


                                    break;


                                case 5:
                                    GridViewCellInfo cell = rowInfo.Cells[5];

                                    if (cell.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[5].Value = "";
                                    }
                                    // this.radGridCodingTemplate.Columns["Name"].Width=111;
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = custList;
                                    //rowInfo.Cells[5].Value = custList;
                                    //radGridCodingTemplate.MasterTemplate.Columns.Add(tComboColumn);

                                    break;


                                case 6:
                                    GridViewCellInfo cell1 = rowInfo.Cells[6];

                                    if (cell1.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[6].Value = "";
                                    }
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = CommonUtilities.GetInstance().ItemList;
                                    // rowInfo.Cells[6].Value = CommonUtilities.GetInstance().ItemList;

                                    break;


                                case 7:
                                    GridViewCellInfo cell2 = rowInfo.Cells[7];

                                    if (cell2.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[7].Value = "";
                                    }
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                                    //rowInfo.Cells[7].Value = CommonUtilities.GetInstance().TaxCodeList;
                                    break;

                                case 8:
                                    GridViewCellInfo cell3 = rowInfo.Cells[8];

                                    if (cell3.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[8].Value = "";
                                    }
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = custList;
                                    //rowInfo.Cells[8].Value = custList;
                                    break;
                                case 9:

                                    if (!fileExtension.Equals("qif"))
                                    {
                                        if (CommonUtilities.GetInstance().IsImportProcess)
                                        {
                                            result = hashTableInfo["TRNAMT"];
                                            if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                            {
                                                if (payee != null)
                                                {
                                                    if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    {
                                                        this.ValidateStyle(rowInfo.Cells[5]);
                                                        this.ValidateStyle(rowInfo.Cells[6]);
                                                        this.ValidateStyle(rowInfo.Cells[7]);
                                                        this.ValidateStyle(rowInfo.Cells[8]);
                                                        this.ValidateStyle(rowInfo.Cells[9]);
                                                        this.ValidateStyle(rowInfo.Cells[10]);
                                                    }
                                                }
                                            }
                                            if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            {
                                                this.RADCellStyle(rowInfo.Cells[5]);
                                                this.RADCellStyle(rowInfo.Cells[6]);
                                                this.RADCellStyle(rowInfo.Cells[7]);
                                                this.RADCellStyle(rowInfo.Cells[8]);
                                                this.RADCellStyle(rowInfo.Cells[9]);
                                                this.RADCellStyle(rowInfo.Cells[10]);
                                            }
                                            this.CellStyleRed(rowInfo.Cells[9]);
                                            //   rowInfo.Cells[9].Value = result;

                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(hashTableInfo["TRNAMT"].ToString()))
                                            {
                                                if (hashTableInfo["TRNAMT"].ToString().Contains("-"))
                                                {
                                                    result = hashTableInfo["TRNAMT"].ToString().Replace("-", "");

                                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                                    {
                                                        if (payee != null)
                                                        {
                                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                            {
                                                                this.ValidateStyle(rowInfo.Cells[5]);
                                                                this.ValidateStyle(rowInfo.Cells[6]);
                                                                this.ValidateStyle(rowInfo.Cells[7]);
                                                                this.ValidateStyle(rowInfo.Cells[8]);
                                                                this.ValidateStyle(rowInfo.Cells[9]);
                                                                this.ValidateStyle(rowInfo.Cells[10]);
                                                            }
                                                        }
                                                    }
                                                    if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                                    {
                                                        this.RADCellStyle(rowInfo.Cells[5]);
                                                        this.RADCellStyle(rowInfo.Cells[6]);
                                                        this.RADCellStyle(rowInfo.Cells[7]);
                                                        this.RADCellStyle(rowInfo.Cells[8]);
                                                        this.RADCellStyle(rowInfo.Cells[9]);
                                                        this.CellStyleRed(rowInfo.Cells[10]);
                                                    }
                                                    //   this.CellStyleRed(rowInfo.Cells[9]);
                                                    // rowInfo.Cells[9].Value = result;

                                                }
                                                else
                                                {
                                                    result = hashTableInfo["TRNAMT"];


                                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                                    {
                                                        if (payee != null)
                                                        {
                                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                            {
                                                                this.ValidateStyle(rowInfo.Cells[5]);
                                                                this.ValidateStyle(rowInfo.Cells[6]);
                                                                this.ValidateStyle(rowInfo.Cells[7]);
                                                                this.ValidateStyle(rowInfo.Cells[8]);
                                                                this.ValidateStyle(rowInfo.Cells[9]);
                                                                this.ValidateStyle(rowInfo.Cells[10]);
                                                            }
                                                        }
                                                    }
                                                    if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                                    {
                                                        this.RADCellStyle(rowInfo.Cells[5]);
                                                        this.RADCellStyle(rowInfo.Cells[6]);
                                                        this.RADCellStyle(rowInfo.Cells[7]);
                                                        this.RADCellStyle(rowInfo.Cells[8]);
                                                        this.RADCellStyle(rowInfo.Cells[9]);
                                                        this.CellStyleRed(rowInfo.Cells[10]);
                                                    }
                                                    //    this.CellStyleRed(rowInfo.Cells[9]);
                                                    // rowInfo.Cells[9].Value = result;
                                                }

                                            }
                                        }

                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().IsImportProcess)
                                        {
                                            result = hashTableInfo["T"];

                                            if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                            {
                                                if (payee != null)
                                                {
                                                    if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                    {
                                                        this.ValidateStyle(rowInfo.Cells[5]);
                                                        this.ValidateStyle(rowInfo.Cells[6]);
                                                        this.ValidateStyle(rowInfo.Cells[7]);
                                                        this.ValidateStyle(rowInfo.Cells[8]);
                                                        this.ValidateStyle(rowInfo.Cells[9]);
                                                        this.ValidateStyle(rowInfo.Cells[10]);
                                                    }
                                                }
                                            }
                                            if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            {
                                                this.RADCellStyle(rowInfo.Cells[5]);
                                                this.RADCellStyle(rowInfo.Cells[6]);
                                                this.RADCellStyle(rowInfo.Cells[7]);
                                                this.RADCellStyle(rowInfo.Cells[8]);
                                                this.RADCellStyle(rowInfo.Cells[9]);
                                                this.RADCellStyle(rowInfo.Cells[10]);
                                            }
                                            //  this.CellStyleRed(rowInfo.Cells[9]);
                                            //   rowInfo.Cells[9].Value = result;

                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(hashTableInfo["T"].ToString()))
                                            {
                                                if (hashTableInfo["T"].ToString().Contains("-"))
                                                {
                                                    result = hashTableInfo["T"].ToString().Replace("-", "");
                                                    data[2] = result.ToString();
                                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                                    {
                                                        if (payee != null)
                                                        {
                                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                            {
                                                                this.ValidateStyle(rowInfo.Cells[5]);
                                                                this.ValidateStyle(rowInfo.Cells[6]);
                                                                this.ValidateStyle(rowInfo.Cells[7]);
                                                                this.ValidateStyle(rowInfo.Cells[8]);
                                                                this.ValidateStyle(rowInfo.Cells[9]);
                                                                this.ValidateStyle(rowInfo.Cells[10]);
                                                                this.CellStyleRed(rowInfo.Cells[10]);

                                                            }
                                                        }
                                                    }
                                                    //if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                                    //  this.CellStyleRed(rowInfo.Cells[9]);
                                                    // this.CellStyleRed(rowInfo.Cells[9]);
                                                    // rowInfo.Cells[9].Value = result;

                                                }
                                                else
                                                {
                                                    result = hashTableInfo["T"];
                                                    data[2] = result.ToString();
                                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                                    {
                                                        if (payee != null)
                                                        {
                                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                                this.ValidateStyle(rowInfo.Cells[10]);
                                                        }
                                                    }
                                                    //if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                                    //{
                                                    this.RADCellStyle(rowInfo.Cells[5]);
                                                    this.RADCellStyle(rowInfo.Cells[6]);
                                                    this.RADCellStyle(rowInfo.Cells[7]);
                                                    this.RADCellStyle(rowInfo.Cells[8]);
                                                    this.RADCellStyle(rowInfo.Cells[9]);
                                                    this.RADCellStyle(rowInfo.Cells[10]);
                                                    //}
                                                    //   this.CellStyleRed(rowInfo.Cells[9]);   rowInfo.Cells[9].Value = result;
                                                    //    rowInfo.Cells[9].Value = matchedData[i][5];

                                                }
                                            }
                                        }
                                    }

                                    break;
                                case 10:
                                    rowInfo.Cells[10].Value = false;
                                    // tCheckColumn = new GridViewCheckBoxColumn();
                                    //rowInfo.Cells[9].Value = "";
                                    break;
                                case 11:
                                    if (!fileExtension.Equals("qif"))
                                    {
                                        result = hashTableInfo["TRNAMT"];
                                    }
                                    else
                                    {
                                        result = hashTableInfo["T"];
                                    }

                                    result = result.ToString().Trim();
                                    t = Convert.ToDecimal(result);

                                    if (result != null)
                                    {
                                        if (t < 0)
                                        {

                                            this.CellStyleRed(rowInfo.Cells[11]);
                                        }
                                    }


                                    rowInfo.Cells[11].Value = result;

                                    break;
                                case 12:
                                    result = "";
                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                    {
                                        if (payee != null)
                                        {
                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                this.ValidateStyle(rowInfo.Cells[12]);
                                        }
                                    }
                                    if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                    {
                                        this.RADCellStyle(rowInfo.Cells[12]);
                                    }
                                    //    this.CellStyleRed(rowInfo.Cells[12]);
                                    rowInfo.Cells[12].Value = result.ToString();
                                    break;

                                case 13:
                                    result = "";
                                    rowInfo.Cells[13].Value = result.ToString();
                                    break;
                            }
                        }
                    }
                    matchedData = null;

                    //}
                    #endregion
                    //change
                }

                radGridCodingTemplate.Rows.Add(rowInfo);

                data = null;
                // rowInfo.Dispose();

                result = null;
                payee = null;
                GC.WaitForPendingFinalizers();
                GC.Collect();

            }
            catch (Exception e)
            {
                MessageBox.Show("Exception:" + e.Message);
            }

            #endregion
        }

        //Axis 11.0 Bug No.132
        private void FillRADGridRowWithFileData_TXT(Hashtable hashTableInfo, string fileExtension, string codingTemplateName, List<string> custList)
        {
            string culture = CommonUtilities.GetInstance().CountryVersion;

            #region Fill newly created row with data from file

            List<string[]> matchedData = CommonUtilities.GetInstance().GetKeywordDataForCodingTemplate(codingTemplateName);

            try
            {
                string[] data = new string[17];
                List<CommonUtilities.QIFDatasource> dataList = new List<CommonUtilities.QIFDatasource>();
                GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(this.radGridCodingTemplate.MasterView);


                object result = null;
                object payee = null;
                if (fileExtension.Equals("txt"))
                    payee = hashTableInfo["P"];

                #region If Coding Template is null.

                if (codingTemplateName == null)
                {
                    for (int j = 0; j < 13; j++)
                    {
                        switch (j)
                        {

                            case 0:
                                if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx") || fileExtension.Equals("csv"))
                                {
                                    string chkValue = "false";
                                    data[0] = chkValue;

                                    rowInfo.Cells[0].Value = chkValue;
                                }

                                break;

                            case 1:
                                if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx") || fileExtension.Equals("csv"))
                                {
                                    if (hashTableInfo["Date"].ToString().Length > 14)
                                    {
                                        result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["Date"].ToString());
                                    }
                                    else
                                        // result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["Date"].ToString());
                                        result = hashTableInfo["Date"];

                                    rowInfo.Cells[1].Value = result;
                                }




                                break;

                            case 2:
                                if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx") || fileExtension.Equals("csv"))
                                {
                                    result = hashTableInfo["Bank Account"];

                                    rowInfo.Cells[2].Value = result;

                                }

                                break;

                            case 3:
                                if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx") || fileExtension.Equals("csv"))
                                {


                                }


                                break;

                            case 4:
                                if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx"))
                                {
                                    result = hashTableInfo["Narrative"];

                                    rowInfo.Cells[3].Value = result;

                                }
                                else if (fileExtension.Equals("csv"))
                                {
                                    result = hashTableInfo["Description"];

                                    rowInfo.Cells[4].Value = result;
                                }


                                break;


                            case 5:
                                GridViewCellInfo cell = rowInfo.Cells[5];

                                if (cell.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[5].Value = "";
                                }

                                break;


                            case 6:
                                GridViewCellInfo cell1 = rowInfo.Cells[6];

                                if (cell1.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[6].Value = "";
                                }

                                break;


                            case 7:
                                GridViewCellInfo cell2 = rowInfo.Cells[7];

                                if (cell2.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[7].Value = "";
                                }

                                break;

                            case 8:
                                GridViewCellInfo cell3 = rowInfo.Cells[8];

                                if (cell3.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                {
                                    rowInfo.Cells[8].Value = "";
                                }

                                break;


                            case 9:
                                if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx"))
                                {
                                    result = hashTableInfo["Debit Amount"];
                                    rowInfo.Cells[9].Value = result;
                                }
                                else if (fileExtension.Equals("csv"))
                                {
                                    result = hashTableInfo["Balance"];

                                    rowInfo.Cells[9].Value = result;
                                }




                                break;
                            case 10:
                                rowInfo.Cells[10].Value = false;

                                break;
                            case 11:
                                result = "";
                                rowInfo.Cells[11].Value = result;

                                break;

                            case 12:
                                result = "";
                                rowInfo.Cells[12].Value = result.ToString();
                                break;

                            case 13:
                                result = "";
                                rowInfo.Cells[13].Value = result.ToString();
                                break;
                        }
                    }
                }
                #endregion


                else
                {


                    for (int i = 0; i < matchedData.Count; i++)
                    {
                        for (int j = 0; j < 13; j++)
                        {
                            switch (j)
                            {

                                case 0:

                                    string chkValue = "true";
                                    data[0] = chkValue;

                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                    {
                                        if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                            this.ValidateStyle(rowInfo.Cells[0]);
                                    }
                                    if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                        this.RADCellStyle(rowInfo.Cells[0]);

                                    rowInfo.Cells[0].Value = chkValue;


                                    break;

                                case 1:
                                    if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx") || fileExtension.Equals("csv"))
                                    {
                                        if (hashTableInfo["Date"].ToString().Length > 14)
                                        {
                                            result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["Date"].ToString());
                                        }
                                        else
                                            //  result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["Date"].ToString());
                                            result = hashTableInfo["Date"];

                                        rowInfo.Cells[1].Value = result;
                                    }

                                    break;

                                case 2:
                                    if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx") || fileExtension.Equals("csv"))
                                    {
                                        result = hashTableInfo["Bank Account"];
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                this.ValidateStyle(rowInfo.Cells[2]);
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[2]);

                                        rowInfo.Cells[2].Value = result;

                                    }

                                    break;

                                case 3:
                                    if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx") || fileExtension.Equals("csv"))
                                    {
                                        result = "";
                                        rowInfo.Cells[3].Value = result;

                                    }

                                    break;

                                case 4:
                                    if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx"))
                                    {
                                        result = hashTableInfo["Narrative"];
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                this.ValidateStyle(rowInfo.Cells[4]);
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[4]);

                                        rowInfo.Cells[3].Value = result;

                                    }
                                    else if (fileExtension.Equals("csv"))
                                    {
                                        result = hashTableInfo["Description"];
                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                                this.ValidateStyle(rowInfo.Cells[4]);
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                            this.RADCellStyle(rowInfo.Cells[4]);

                                        rowInfo.Cells[4].Value = result;

                                    }



                                    break;


                                case 5:
                                    GridViewCellInfo cell = rowInfo.Cells[5];

                                    if (cell.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[5].Value = "";
                                    }
                                    // this.radGridCodingTemplate.Columns["Name"].Width=111;
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = custList;
                                    //rowInfo.Cells[5].Value = custList;
                                    //radGridCodingTemplate.MasterTemplate.Columns.Add(tComboColumn);

                                    break;


                                case 6:
                                    GridViewCellInfo cell1 = rowInfo.Cells[6];

                                    if (cell1.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[6].Value = "";
                                    }
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = CommonUtilities.GetInstance().ItemList;
                                    // rowInfo.Cells[6].Value = CommonUtilities.GetInstance().ItemList;

                                    break;


                                case 7:
                                    GridViewCellInfo cell2 = rowInfo.Cells[7];

                                    if (cell2.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[7].Value = "";
                                    }
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                                    //rowInfo.Cells[7].Value = CommonUtilities.GetInstance().TaxCodeList;
                                    break;

                                case 8:
                                    GridViewCellInfo cell3 = rowInfo.Cells[8];

                                    if (cell3.ColumnInfo.GetType() == typeof(GridViewComboBoxColumn))
                                    {
                                        rowInfo.Cells[8].Value = "";
                                    }
                                    //tComboColumn = new GridViewComboBoxColumn();
                                    //tComboColumn.DataSource = custList;
                                    //rowInfo.Cells[8].Value = custList;
                                    break;
                                case 9:
                                    if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx"))
                                    {
                                        result = hashTableInfo["Debit Amount"];
                                    }
                                    else if (fileExtension.Equals("csv"))
                                    {
                                        result = hashTableInfo["Balance"];
                                    }
                                    if (CommonUtilities.GetInstance().IsImportProcess)
                                    {


                                        if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                        {
                                            if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                            {
                                                this.ValidateStyle(rowInfo.Cells[5]);
                                                this.ValidateStyle(rowInfo.Cells[6]);
                                                this.ValidateStyle(rowInfo.Cells[7]);
                                                this.ValidateStyle(rowInfo.Cells[8]);
                                                this.ValidateStyle(rowInfo.Cells[9]);
                                                this.ValidateStyle(rowInfo.Cells[10]);
                                            }
                                        }
                                        if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                        {
                                            this.RADCellStyle(rowInfo.Cells[5]);
                                            this.RADCellStyle(rowInfo.Cells[6]);
                                            this.RADCellStyle(rowInfo.Cells[7]);
                                            this.RADCellStyle(rowInfo.Cells[8]);
                                            this.RADCellStyle(rowInfo.Cells[9]);
                                            this.RADCellStyle(rowInfo.Cells[10]);
                                        }



                                    }
                                    this.CellStyleRed(rowInfo.Cells[9]);
                                    if (fileExtension.Equals("txt") || fileExtension.Equals("xls") || fileExtension.Equals("xlsx"))
                                    {
                                        rowInfo.Cells[9].Value = "";
                                    }
                                    else
                                    {
                                        rowInfo.Cells[9].Value = result;
                                    }

                                    break;
                                case 10:
                                    rowInfo.Cells[10].Value = false;
                                    // tCheckColumn = new GridViewCheckBoxColumn();
                                    //rowInfo.Cells[9].Value = "";
                                    break;
                                case 11:

                                    //  result = "";

                                    rowInfo.Cells[11].Value = result;

                                    break;
                                case 12:
                                    result = "";
                                    if (matchedData[i][1] == "" || matchedData[i][2] == "" || (matchedData[i][5] == "true" && matchedData[i][4] == ""))
                                    {
                                        if (payee.ToString().ToLower().Contains(matchedData[i][0].ToLower()) || matchedData[i][0].ToLower().Contains(payee.ToString().ToLower()))
                                            this.ValidateStyle(rowInfo.Cells[12]);
                                    }
                                    if (matchedData[i][1] != "" && matchedData[i][2] != "")
                                    {
                                        this.RADCellStyle(rowInfo.Cells[12]);
                                    }
                                    rowInfo.Cells[12].Value = result.ToString();
                                    break;

                                case 13:
                                    result = "";
                                    rowInfo.Cells[13].Value = result.ToString();
                                    break;
                            }
                        }
                    }

                }
                radGridCodingTemplate.Rows.Add(rowInfo);
                //dataList.Add(new CommonUtilities.QIFDatasource(data));
                // radGridCodingTemplate.Rows.Add(data);


            }
            catch (Exception e)
            {
                MessageBox.Show("Exception:" + e.Message);
            }

            #endregion
        }


        /// <summary>
        /// Fill DataGridView with file data
        /// </summary>
        /// <param name="hashTableInfo">Hashtable consist of file data</param>
        /// <param name="fileExtension">Type of extension for browse Statement Import file</param>
        private void FillDataGridViewRowWithFileData(Hashtable hashTableInfo, string fileExtension)
        {
            #region Fill newly created row with data from file

            try
            {
                string[] data = new string[16];


                dr = new DataGridViewRow();
                object result = null;

                for (int j = 0; j < 12; j++)
                {
                    switch (j)
                    {

                        case 0:
                            checkCell = new DataGridViewCheckBoxCell();
                            checkCell.Value = true;
                            dr.Cells.Add(checkCell);
                            break;

                        case 1:
                            if (!fileExtension.Equals("qif"))
                            {
                                //if (hashTableInfo["DTUSER"].ToString().Length > 14)
                                //{
                                //    result =CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["DTUSER"].ToString());
                                //}
                                //else
                                //    result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["DTUSER"].ToString());

                                if (hashTableInfo["DTPOSTED"].ToString().Length > 14)
                                {
                                    result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["DTPOSTED"].ToString());
                                }
                                else
                                    result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["DTPOSTED"].ToString(), DateFlag);
                            }
                            else
                            {
                                if (hashTableInfo["D"].ToString().Length > 14)
                                {
                                    result = CommonUtilities.GetInstance().GetDateFormat(hashTableInfo["D"].ToString());
                                    data[0] = result.ToString();

                                }
                                else
                                    result = CommonUtilities.GetInstance().GetDateFormatforRest(hashTableInfo["D"].ToString(), DateFlag);
                                data[0] = result.ToString();
                            }
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            if (result != null)
                            {
                                if (!result.Equals(string.Empty))
                                    textCell.Value = result;
                                else
                                    textCell.Value = string.Empty;

                                dr.Cells.Add(textCell);
                            }

                            break;

                        case 2:
                            if (fileExtension.Equals("qbo"))
                            {
                                result = hashTableInfo["FITID"];
                            }
                            else if (!fileExtension.Equals("qif"))
                            {
                                result = hashTableInfo["FITID"];
                            }
                            else
                            {
                                result = hashTableInfo["N"];
                                data[4] = result.ToString();
                            }

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            if (result != null)
                            {
                                if (!result.Equals(string.Empty))
                                    textCell.Value = result;
                                else
                                    textCell.Value = string.Empty;

                                dr.Cells.Add(textCell);
                            }

                            break;

                        case 3:
                            if (!fileExtension.Equals("qif"))
                            {
                                result = hashTableInfo["NAME"];
                            }
                            else
                            {
                                result = hashTableInfo["P"];
                                data[5] = result.ToString();
                            }

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            if (result != null)
                            {
                                if (!result.Equals(string.Empty))
                                    textCell.Value = result;
                                else
                                    textCell.Value = string.Empty;
                                dr.Cells.Add(textCell);
                            }

                            break;
                        case 4:
                            if (!fileExtension.Equals("qif"))
                            {
                                result = hashTableInfo["MEMO"];
                            }
                            else
                            {
                                result = hashTableInfo["M"];
                                data[2] = result.ToString();
                            }

                            textCellMemo = new DataGridViewTextBoxCell();
                            textCellMemo.Style = cellStyle;
                            if (result != null)
                            {
                                if (!result.Equals(string.Empty))
                                    textCellMemo.Value = result;
                                else
                                    textCellMemo.Value = string.Empty;
                                dr.Cells.Add(textCellMemo);
                            }

                            break;


                        case 5:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = custList;
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;


                        case 6:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = CommonUtilities.GetInstance().ItemList;
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;


                        case 7:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;

                        case 8:
                            comboCell = new DataGridViewComboBoxCell();
                            comboCell.DataSource = custList;
                            comboCell.Style = cellStyle;
                            dr.Cells.Add(comboCell);
                            break;
                        case 9:
                            checkCell = new DataGridViewCheckBoxCell();
                            dr.Cells.Add(checkCell);
                            break;

                        case 10:
                            if (!fileExtension.Equals("qif"))
                            {
                                if (CommonUtilities.GetInstance().IsImportProcess)
                                {
                                    result = hashTableInfo["TRNAMT"];
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(hashTableInfo["TRNAMT"].ToString()))
                                    {
                                        if (hashTableInfo["TRNAMT"].ToString().Contains("-"))
                                        {
                                            result = hashTableInfo["TRNAMT"].ToString().Replace("-", "");
                                        }
                                        else
                                            result = hashTableInfo["TRNAMT"];
                                    }
                                }
                            }
                            else
                            {
                                if (CommonUtilities.GetInstance().IsImportProcess)
                                {
                                    result = hashTableInfo["T"];
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(hashTableInfo["T"].ToString()))
                                    {
                                        if (hashTableInfo["T"].ToString().Contains("-"))
                                        {
                                            result = hashTableInfo["T"].ToString().Replace("-", "");
                                            data[1] = result.ToString();
                                        }
                                        else
                                        {
                                            result = hashTableInfo["T"];
                                            data[1] = result.ToString();
                                        }
                                    }
                                }
                            }

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            if (result != null)
                            {
                                if (!result.Equals(string.Empty))
                                    textCell.Value = result;
                                else
                                    textCell.Value = string.Empty;
                                dr.Cells.Add(textCell);
                            }

                            break;
                        case 11:
                            if (!fileExtension.Equals("qif"))
                            {
                                result = hashTableInfo["TRNAMT"];
                            }
                            else
                            {
                                result = hashTableInfo["T"];
                            }
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            if (result != null)
                            {
                                if (!result.Equals(string.Empty))
                                    textCell.Value = result;
                                else
                                    textCell.Value = string.Empty;
                                dr.Cells.Add(textCell);
                            }
                            break;
                    }
                }
                //   dataGridViewPreviewCodingTemplate.Rows.Clear();

                dataGridViewPreviewCodingTemplate.Rows.Add(dr);


                // dataList.Add(new CommonUtilities.QIFDatasource(data));
                //radGridCodingTemplate.Rows.Add(data);


            }
            catch
            {
            }

            #endregion
        }


        /// <summary>
        /// Return QBOFile matching List(compare qbofile data with coding template name)
        /// </summary>
        /// <param name="qboDataList"></param>
        /// <param name="codingTemplateName"></param>
        /// <returns></returns>
        private ArrayList GetQBOFileMatchData(ArrayList qboDataList, string codingTemplateName)
        {
            try
            {
                ArrayList matchList = new ArrayList();

                foreach (List<string> listData in qboDataList)
                {
                    foreach (string data in listData)
                    {
                        if (data.StartsWith("NAME"))
                        {
                            string[] name = Regex.Split(data, "~");
                            if (Convert.ToString(name[1]).ToLower().Contains(codingTemplateName.ToLower()))
                                matchList.Add(listData);
                        }
                    }
                }

                return matchList;
            }
            catch
            {
                return null;
            }


        }



        public class CustomCheckBoxColumn : GridViewCheckBoxColumn
        {
            public override Type GetCellType(GridViewRowInfo row)
            {
                if (row is GridViewTableHeaderRowInfo)
                {
                    return typeof(CheckBoxInHeader.CheckBoxHeaderCell);
                }
                return base.GetCellType(row);
            }
        }

        
        private void FillRADHeaderData(string[] headerRow, List<string> custList)
        {
            radExpenseAccList = new List<string>();
            //   radExpenseAccList = CommonUtilities.GetInstance().CombinedItemList;
            radExpenseAccList.AddRange(CommonUtilities.GetInstance().ExpenseAccountList);

            try
            {
                this.radGridCodingTemplate.AutoGenerateColumns = false;

                for (int i = 0; i < headerRow.Length; i++)
                {
                    switch (i)
                    {

                        case 0:
                            //tCheckColumn= new GridViewCheckBoxColumn();
                            //tCheckColumn.HeaderText=headerRow[i].ToString();
                            //tCheckColumn.Width = 50;
                            //radGridCodingTemplate.Columns.Add(tCheckColumn);

                            CustomCheckBoxColumn chkBox = new CustomCheckBoxColumn();
                            chkBox.Width = 50;
                            chkBox.Name = "Select";
                            this.radGridCodingTemplate.ViewCellFormatting += new CellFormattingEventHandler(radGridCodingTemplate_ViewCellFormatting);
                            this.radGridCodingTemplate.Columns.Add(chkBox);
                            break;

                        case 1:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 80;
                            radGridCodingTemplate.Columns.Add(tTextColumn);
                            break;

                        case 2:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 30;
                            radGridCodingTemplate.Columns.Add(tTextColumn);


                            //FilterDescriptor filter1 = new FilterDescriptor();
                            //filter1.Operator = FilterOperator.Contains;
                            //filter1.Value = "Qu";
                            //filter1.IsFilterEditor = true;
                            //this.radGridCodingTemplate.Columns["ProductName"].FilterDescriptor = filter1;
                            break;

                        case 3:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 100;
                            radGridCodingTemplate.Columns.Add(tTextColumn);
                            break;

                        case 4:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 80;
                            radGridCodingTemplate.Columns.Add(tTextColumn);
                            break;

                        case 5:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 110;
                            radGridCodingTemplate.Columns.Add(tComboColumn);

                            tComboColumn.DataSource = allcust;
                            break;

                        case 6:
                            //tMultiComboColumn = new GridViewMultiComboBoxColumn();
                            //tMultiComboColumn.HeaderText = headerRow[i].ToString();
                            //tMultiComboColumn.DisplayMember = "Quantity";
                            //tMultiComboColumn.ValueMember = "OrderID";
                            //tMultiComboColumn.FieldName = "OrderID";
                            //radGridCodingTemplate.Columns.Add(tMultiComboColumn);
                            //tMultiComboColumn.DataSource = CommonUtilities.GetInstance().CombinedItemListArray;
                            //this.radGridCodingTemplate.CellBeginEdit += new GridViewCellCancelEventHandler(radGridCodingTemplate_CellBeginEdit);


                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 110;
                            radGridCodingTemplate.Columns.Add(tComboColumn);
                            //tComboColumn.DataSource = CommonUtilities.GetInstance().ItemList;
                            tComboColumn.DataSource = radExpenseAccList;
                            break;

                        case 7:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 80;
                            radGridCodingTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                            break;

                        case 8:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 110;
                            radGridCodingTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = custList;
                            break;

                        case 9:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 110;
                            radGridCodingTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = clasList;
                            break;
                        case 10:
                            tCheckColumn = new GridViewCheckBoxColumn();
                            tCheckColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 110;
                            radGridCodingTemplate.Columns.Add(tCheckColumn);
                            break;
                        case 11:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 70;
                            radGridCodingTemplate.Columns.Add(tTextColumn);
                            break;

                        case 12:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.IsVisible = false;
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 80;
                            radGridCodingTemplate.Columns.Add(tTextColumn);
                            break;




                        case 13:
                            tHyperlinkColumn = new GridViewHyperlinkColumn();
                            tHyperlinkColumn.IsVisible = true;
                            tHyperlinkColumn.HeaderText = headerRow[i].ToString();
                            tHyperlinkColumn.Width = 80;

                            radGridCodingTemplate.Columns.Add(tHyperlinkColumn);

                            break;



                        case 14:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.IsVisible = false;
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 50;
                            radGridCodingTemplate.Columns.Add(tTextColumn);
                            break;



                    }


                }
                // Axis 323 for filter
                foreach (GridViewColumn column in radGridCodingTemplate.Columns)
                {
                    GridViewCheckBoxColumn col = column as GridViewCheckBoxColumn;

                    GridViewTextBoxColumn col_text = column as GridViewTextBoxColumn;
                    GridViewComboBoxColumn col_cmb = column as GridViewComboBoxColumn;
                    if (column is GridViewCheckBoxColumn)
                    {
                        col.AllowFiltering = false;
                    }


                    else if (column.HeaderText == "Status")
                    {
                        if (col_text != null)
                        {
                            col_text.AllowFiltering = true;
                        }
                    }
                    else
                    {
                        if (col_text != null)
                        {
                            col_text.AllowFiltering = false;
                        }
                        else if (col_cmb != null)
                        {
                            col_cmb.AllowFiltering = false;
                        }
                    }



                }


                //for (int i = 0; i < headerRow.Length; i++)
                //{
                //    if (!(radGridCodingTemplate.Columns[i] is GridViewCheckBoxColumn))
                //        radGridCodingTemplate.Columns[i].AllowFiltering = true;
                //}


            }

            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }








        /// <summary>
        /// Create Header row for DataGridView
        /// </summary>
        /// <param name="headerRow"></param>
        private void FillDataGridViewHeaderData(string[] headerRow)
        {
            #region Create Header for DataGridView of CodingTemplateForm

            try
            {

                for (int i = 0; i < headerRow.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            checkColumn = new DataGridViewCheckBoxColumn();
                            checkColumn.HeaderText = headerRow[i].ToString();
                            checkColumn.FlatStyle = FlatStyle.Standard;
                            dataGridViewPreviewCodingTemplate.Columns.Add(checkColumn);
                            break;

                        case 1:
                            textColumn = new DataGridViewTextBoxColumn();
                            textColumn.HeaderText = headerRow[i].ToString();
                            //textColumn.ReadOnly = true;
                            dataGridViewPreviewCodingTemplate.Columns.Add(textColumn);
                            break;
                        case 2:
                            textColumn = new DataGridViewTextBoxColumn();
                            textColumn.HeaderText = headerRow[i].ToString();
                            //textColumn.ReadOnly = true;
                            dataGridViewPreviewCodingTemplate.Columns.Add(textColumn);
                            break;
                        case 3:
                            textColumn = new DataGridViewTextBoxColumn();
                            textColumn.HeaderText = headerRow[i].ToString();
                            //textColumn.ReadOnly = true;
                            dataGridViewPreviewCodingTemplate.Columns.Add(textColumn);
                            break;
                        case 4:
                            textColumn = new DataGridViewTextBoxColumn();
                            textColumn.HeaderText = headerRow[i].ToString();
                            //textColumn.ReadOnly = true;
                            dataGridViewPreviewCodingTemplate.Columns.Add(textColumn);
                            break;
                        case 5:
                            comboColumn = new DataGridViewComboBoxColumn();
                            comboColumn.HeaderText = headerRow[i].ToString();
                            comboColumn.DropDownWidth = 160;
                            comboColumn.Width = 90;
                            comboColumn.FlatStyle = FlatStyle.Standard;
                            dataGridViewPreviewCodingTemplate.Columns.Add(comboColumn);
                            break;

                        case 6:
                            comboColumn = new DataGridViewComboBoxColumn();
                            comboColumn.HeaderText = headerRow[i].ToString();
                            comboColumn.DropDownWidth = 160;
                            comboColumn.Width = 90;
                            comboColumn.FlatStyle = FlatStyle.Standard;
                            dataGridViewPreviewCodingTemplate.Columns.Add(comboColumn);
                            break;

                        case 7:
                            comboColumn = new DataGridViewComboBoxColumn();
                            comboColumn.HeaderText = headerRow[i].ToString();
                            comboColumn.DropDownWidth = 160;
                            comboColumn.Width = 90;
                            comboColumn.FlatStyle = FlatStyle.Standard;
                            dataGridViewPreviewCodingTemplate.Columns.Add(comboColumn);
                            break;

                        case 8:
                            comboColumn = new DataGridViewComboBoxColumn();
                            comboColumn.HeaderText = headerRow[i].ToString();
                            comboColumn.DropDownWidth = 160;
                            comboColumn.Width = 90;
                            comboColumn.FlatStyle = FlatStyle.Standard;
                            dataGridViewPreviewCodingTemplate.Columns.Add(comboColumn);
                            break;

                        case 9:

                            textColumn = new DataGridViewTextBoxColumn();
                            textColumn.HeaderText = headerRow[i].ToString();
                            //textColumn.ReadOnly = true;
                            dataGridViewPreviewCodingTemplate.Columns.Add(textColumn);
                            break;
                        case 10:
                            checkColumn = new DataGridViewCheckBoxColumn();
                            checkColumn.HeaderText = headerRow[i].ToString();
                            checkColumn.FlatStyle = FlatStyle.Standard;
                            dataGridViewPreviewCodingTemplate.Columns.Add(checkColumn);
                            break;

                        case 11:
                            textColumn = new DataGridViewTextBoxColumn();
                            textColumn.HeaderText = headerRow[i].ToString();
                            textColumn.ReadOnly = true;
                            textColumn.Visible = false;
                            dataGridViewPreviewCodingTemplate.Columns.Add(textColumn);
                            break;
                    }
                }
                for (int i = 0; i < headerRow.Length; i++)
                {
                    dataGridViewPreviewCodingTemplate.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }

            }
            catch
            {

            }
            #endregion
        }



        /// <summary>
        /// Get MatchList of Data by comparing it with CodingTemplate name
        /// </summary>
        /// <param name="fileData"></param>
        /// <returns></returns>
        private ArrayList GetMatchListOfData(ArrayList fileData, string codingTemplateName)
        {
            ArrayList matchList = new ArrayList();

            try
            {
                foreach (List<string> listData in fileData)
                {
                    foreach (string data in listData)
                    {
                        if (data.Contains("<NAME>"))
                        {
                            string[] name = Regex.Split(data, ">");
                            if (Convert.ToString(name[1]).ToLower().Contains(codingTemplateName.ToLower()))
                                matchList.Add(listData);
                        }
                    }
                }

                return matchList;
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Returns hashtable of File data
        /// </summary>
        /// <param name="dataList"></param>
        /// <param name="GetDataInKeyValueFormat"></param>
        /// <returns></returns>
        private Hashtable GetDataInKeyValueFormat(List<string> dataList, string[] typeOfDataForFile)
        {
            try
            {
                Hashtable hashTableInfo = new Hashtable();

                if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("ofx")
                    || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qfx")
                    || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qbo")
                    || flagQBO.Equals(true))
                {
                    #region Get Key/Value data from OFX/QFX File
                    foreach (string data in dataList)
                    {
                        if (!string.IsNullOrEmpty(data))
                        {
                            string validData = data.Replace("<", "").Trim();

                            string[] keyValue = Regex.Split(validData, ">");
                            //if (keyValue[1].Contains("/"))
                            //{
                            //    string[] keyvalue = Regex.Split(keyValue[1], "/");
                            //}

                            if (!keyValue[0].Contains("STMTTRN"))
                            {
                                if (keyValue[1].Contains("/"))
                                {
                                    string[] keyvalue = Regex.Split(keyValue[1], "/");
                                    hashTableInfo.Add(keyValue[0], keyvalue[0]);
                                }
                                else
                                {
                                    hashTableInfo.Add(keyValue[0], keyValue[1]);
                                }
                            }
                        }


                    }

                    foreach (string data in typeOfDataForFile)
                    {
                        if (!hashTableInfo.Contains(data))
                        {
                            hashTableInfo.Add(data, string.Empty);
                        }
                    }
                    #endregion
                }
                else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qbo"))
                {
                    #region Get Key/Value data from QBO File
                    //foreach (string data in dataList)
                    //{
                    //    string[] keyValue = Regex.Split(data, "~");

                    //    if (!keyValue[0].Contains("STMTTRN"))
                    //        hashTableInfo.Add(keyValue[0], keyValue[1]);

                    //}

                    //foreach (string data in typeOfDataForFile)
                    //{
                    //    if (!hashTableInfo.Contains(data))
                    //    {
                    //        hashTableInfo.Add(data, string.Empty);
                    //    }
                    //}
                    #endregion
                }
                else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qif"))
                {
                    #region Get Key/Value data from QIF file
                    foreach (string data in dataList)
                    {
                        string[] keyValue = Regex.Split(data, "~");
                        hashTableInfo.Add(keyValue[0], keyValue[1]);
                    }

                    foreach (string data in typeOfDataForFile)
                    {
                        if (!hashTableInfo.Contains(data))
                        {
                            hashTableInfo.Add(data, string.Empty);
                        }
                    }
                    #endregion
                }
                else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("txt"))
                {
                    #region Get Key/Value data from QIF file
                    foreach (string data in dataList)
                    {
                        string[] keyValue = Regex.Split(data, "/");
                        hashTableInfo.Add(keyValue[0], keyValue[1]);
                    }

                    foreach (string data in typeOfDataForFile)
                    {
                        if (!hashTableInfo.Contains(data))
                        {
                            hashTableInfo.Add(data, string.Empty);
                        }
                    }
                    #endregion
                }

                return hashTableInfo;
            }
            catch
            {
                return null;
            }

        }


        private Hashtable GetDataInKeyValueFormattxt(string dataList, string[] typeOfDataForFile)
        {
            Hashtable hashTableInfo = new Hashtable();
            if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("txt") || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("xls") || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("xlsx") || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("csv"))
            {
                int i = 0;
                #region Get Key/Value data from QIF file
                string[] array = (string[])dataList.Split('~');
                foreach (string data in array)
                {
                    // string[] keyValue = Regex.Split(data, "/");
                    string[] keyValue = Regex.Split(data, "~");
                    if (hashTableInfo.Contains(keyValue[0]))
                    {
                    }
                    else
                    {
                        if (typeOfDataForFile.Length > i)
                        {
                            hashTableInfo.Add(typeOfDataForFile[i], keyValue[0]);
                        }
                        i++;
                    }
                }

                ////foreach (string data in typeOfDataForFile)
                ////{
                ////    if (!hashTableInfo.Contains(data))
                ////    {
                ////        hashTableInfo.Add(data, string.Empty);
                ////    }
                ////}
                #endregion
            }

            return hashTableInfo;

        }

        /// <summary>
        /// Return ArrayList of QIF file data
        /// </summary>
        /// <param name="fileName">Browse Statement file</param>
        /// <returns></returns>
        private ArrayList GetQifFileData(string fileName)
        {
            ArrayList dataQIFList = new ArrayList();

            try
            {
                string[] qifData = File.ReadAllLines(fileName);

                List<string> dataQIF = null;

                bool flag = true;
                if (qifData != null)
                {
                    foreach (string data in qifData)
                    {
                        if (!string.IsNullOrEmpty(data.Trim()))
                        {
                            if (!data.Trim().StartsWith("!"))
                            {
                                if (!data.Trim().StartsWith("^"))
                                {
                                    #region Actual Data

                                    //for first line of record create new List object
                                    if (flag.Equals(true))
                                        dataQIF = new List<string>();

                                    flag = false;
                                    string dataOfList = string.Empty;

                                    dataOfList = data.Substring(0, 1) + "~" + data.Substring(1);

                                    dataQIF.Add(dataOfList);


                                    #endregion
                                }
                                else
                                {
                                    #region Completion of Record indicated by "^"
                                    flag = true;
                                    dataQIFList.Add(dataQIF);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region for Line i.e. !Type:Bank
                                #endregion
                            }
                        }
                    }
                }

                return dataQIFList;
            }
            catch
            {
                dataQIFList = null;
                return dataQIFList;
            }
        }




        /// <summary>
        /// Return ArrayList of qbo file data(data like node.Name~node.InnerText)
        /// </summary>
        /// <param name="dataList">NodeList of STMTTRN node</param>
        /// <returns></returns>
        private ArrayList GetQBOFileData(XmlNodeList dataList)
        {
            ArrayList qboData = new ArrayList();

            try
            {
                List<string> record = null;

                foreach (XmlNode data in dataList)
                {
                    record = new List<string>();

                    for (int i = 0; i < data.ChildNodes.Count; i++)
                    {
                        record.Add(data.ChildNodes[i].Name + "~" + data.ChildNodes[i].InnerText);
                    }

                    //add list into arraylist
                    qboData.Add(record);
                }

                return qboData;
            }
            catch
            {
                qboData = null;
                return qboData;
            }
        }

        /// <summary>
        /// Create XML file and copy OFX TAG DATA into it and return file
        /// </summary>
        /// <param name="fileName">Browse Statement File</param>
        /// <returns></returns>
        private string CreateXMLForQBO(string fileName)
        {
            #region Get data of <OFX></OFX>

            string[] data = File.ReadAllLines(fileName);

            string qboFileData = string.Empty;

            try
            {
                foreach (string ofxData in data)
                {
                    if (ofxData.StartsWith("<OFX>"))
                        qboFileData = ofxData;
                }
            }
            catch
            {
            }

            #endregion


            #region Create xml file and assign data to it

            string xmlFile = string.Empty;

            //create file name 
            xmlFile = Application.StartupPath + "\\" + DateTime.Now.Ticks.ToString() + ".xml";

            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (xmlFile.Contains("Program Files (x86)"))
                    {
                        xmlFile = xmlFile.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        xmlFile = xmlFile.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (xmlFile.Contains("Program Files (x86)"))
                    {
                        xmlFile = xmlFile.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        xmlFile = xmlFile.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }

            try
            {
                System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();

                requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));

                System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBO");
                requestXmlDoc.AppendChild(outer);
                outer.InnerXml = qboFileData;

                requestXmlDoc.Save(xmlFile);
            }
            catch
            {
                //error regarding xml loading
            }
            #endregion

            return xmlFile;
        }

        /// <summary>
        /// Returns ArrayList (Collection of List)
        /// Get data from file and return as ArrayList
        /// </summary>
        /// <param name="fileName">Browse Statement File</param>
        /// <returns></returns>
        private ArrayList GetListsOfFileData(string fileName)
        {
            //create ArrayList to store all List of data
            ArrayList dataLists = new ArrayList();

            try
            {
                string[] fileData = File.ReadAllLines(fileName);

                bool flag = false;

                List<string> record = null;


                for (int i = 0; i < fileData.Length; i++)
                {
                    if (!string.IsNullOrEmpty(fileData[i]))
                    {
                        if ((fileData[i].Trim().Equals("<STMTTRN>") || fileData[i].Trim().Equals("<CCSTMTRS>") || flag.Equals(true)) && !fileData[i].Trim().Equals("</STMTTRN>"))
                        {
                            if (flag.Equals(false))
                                record = new List<string>();

                            flag = true;
                            record.Add(Convert.ToString(fileData[i]).Trim());
                        }
                        else if (fileData[i].Trim().Equals("</STMTTRN>") || fileData[i].Trim().Equals("</CCSTMTRS>"))
                        {
                            flag = false;
                            record.Add(Convert.ToString(fileData[i]).Trim());
                            dataLists.Add(record);
                        }
                    }
                }
                return dataLists;
            }
            catch
            {
                dataLists = null;
                return dataLists;
            }
        }


        private ArrayList GetListsOfQBOFileData(string fileName)
        {
            ArrayList dataLists = new ArrayList();

            try
            {
                string[] fileData = File.ReadAllLines(fileName);

                bool flag = false;

                //List<string> record = null;

                List<string> record = new List<string>();

                for (int i = 0; i < fileData.Length; i++)
                {
                    if (!string.IsNullOrEmpty(fileData[i]))
                    {
                        #region commented code
                        //if ((fileData[i].Trim().Equals("<STMTTRN>") || fileData[i].Trim().Equals("<CCSTMTRS>") || flag.Equals(true)) && !fileData[i].Trim().Equals("</STMTTRN>"))
                        //{
                        //    if (flag.Equals(false))
                        //        record = new List<string>();

                        //    flag = true;
                        //    record.Add(Convert.ToString(fileData[i]).Trim());
                        //}
                        //else if (fileData[i].Trim().Equals("</STMTTRN>") || fileData[i].Trim().Equals("</CCSTMTRS>"))
                        //{
                        //    flag = false;
                        //    record.Add(Convert.ToString(fileData[i]).Trim());
                        //    dataLists.Add(record);
                        //}
                        #endregion

                        data = fileData[i].ToString();

                        if (data.Contains("<OFX>"))
                        {
                            if (data.Contains("<STMTTRN>"))
                            {
                                startIndex = data.IndexOf("<STMTTRN>");
                                endIndex = data.IndexOf("</STMTTRN>");

                                if (startIndex > 0)
                                {
                                    subStr = data.Substring(startIndex, (endIndex - startIndex) + 10);
                                    // break;
                                    //    data = data.Substring(startIndex+10, (endIndex-startIndex)+10);

                                    bool flag1 = true;

                                    while (flag1)
                                    {
                                        record = new List<string>();
                                        val = subStr.Split('<');
                                        for (int j = 1; j < val.Length; j++)
                                            record.Add("<" + val[j].Trim().ToString());

                                        dataLists.Add(record);
                                        data = data.Remove(startIndex, (endIndex - startIndex) + 10);
                                        startIndex = data.IndexOf("<STMTTRN>");
                                        endIndex = data.IndexOf("</STMTTRN>");
                                        if (startIndex > -1)
                                        {
                                            subStr = data.Substring(startIndex, (endIndex - startIndex) + 10);
                                            flag1 = true;
                                        }
                                        else
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                if ((fileData[i].Trim().Equals("<STMTTRN>") || fileData[i].Trim().Equals("<CCSTMTRS>") || flag.Equals(true)) && !fileData[i].Trim().Equals("</STMTTRN>"))
                                {
                                    if (flag.Equals(false))
                                        record = new List<string>();

                                    flag = true;
                                    record.Add(Convert.ToString(fileData[i]).Trim());
                                }
                                else if (fileData[i].Trim().Equals("</STMTTRN>") || fileData[i].Trim().Equals("</CCSTMTRS>"))
                                {
                                    flag = false;
                                    record.Add(Convert.ToString(fileData[i]).Trim());
                                    dataLists.Add(record);
                                }
                            }
                        }
                        else
                        {
                            if ((fileData[i].Trim().Equals("<STMTTRN>") || fileData[i].Trim().Equals("<CCSTMTRS>") || flag.Equals(true)) && !fileData[i].Trim().Equals("</STMTTRN>"))
                            {
                                if (flag.Equals(false))
                                    record = new List<string>();

                                flag = true;
                                record.Add(Convert.ToString(fileData[i]).Trim());
                            }
                            else if (fileData[i].Trim().Equals("</STMTTRN>") || fileData[i].Trim().Equals("</CCSTMTRS>"))
                            {
                                flag = false;
                                record.Add(Convert.ToString(fileData[i]).Trim());
                                dataLists.Add(record);
                            }
                        }

                        #region commented code
                        //startIndex = data.IndexOf("<STMTTRN>");
                        //endIndex = data.IndexOf("</STMTTRN>");

                        //if (startIndex > 0)
                        //{
                        //    subStr = data.Substring(startIndex, (endIndex - startIndex) + 10);
                        //    break;
                        ////    data = data.Substring(startIndex+10, (endIndex-startIndex)+10);
                        //}
                        #endregion
                    }
                }
                //bool flag1 = true;

                //while (flag1)
                //{
                //    record = new List<string>();
                //    val = subStr.Split('<');
                //    for(int i=1;i<val.Length;i++)
                //        record.Add("<"+val[i].Trim().ToString());

                //    dataLists.Add(record);
                //    data = data.Remove(startIndex, (endIndex - startIndex) + 10);
                //    startIndex = data.IndexOf("<STMTTRN>");
                //    endIndex = data.IndexOf("</STMTTRN>");
                //    if (startIndex > -1)
                //    {
                //        subStr = data.Substring(startIndex, (endIndex - startIndex) + 10);
                //        flag1 = true;
                //    }
                //    else
                //        break;
                //}

                return dataLists;
            }
            catch
            {
                dataLists = null;
                return dataLists;
            }
        }

        /// <summary>
        /// Show data in datagridview depending upon coding template name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxPreviewCodingTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //check whether valid index is selected by user
                if (comboBoxPreviewCodingTemplate.SelectedIndex > -1)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    CommonUtilities common = new CommonUtilities();
                    if (this.ConnectedSoft == Constants.QBstring)
                    {
                        common.ConnectedSoftware = Constants.QBstring;
                    }
                    //axis 11 pos
                    else if (this.ConnectedSoft == Constants.QBPOSstring)
                    {
                        common.ConnectedSoftware = Constants.QBPOSstring;
                    }
                    else if (this.ConnectedSoft == Constants.QBOnlinestring)
                    {
                        common.ConnectedSoftware = Constants.QBOnlinestring;
                    }
                    else if (this.ConnectedSoft == Constants.Xerostring)
                    {
                        common.ConnectedSoftware = Constants.Xerostring;
                    }

                    bool validate = common.ValidateCodingTemplate(this.comboBoxPreviewCodingTemplate.SelectedItem.ToString());
                    if (validate)
                    {
                        CommonUtilities.GetInstance().CodingTemplateName = this.comboBoxPreviewCodingTemplate.SelectedItem.ToString();
                    }
                    else
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0138"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.comboBoxPreviewCodingTemplate.SelectedIndex = -1;
                        this.dataGridViewPreviewCodingTemplate.Rows.Clear();
                        return;
                    }
                    //for first time
                    if (selectionFlag)
                    {
                        DataGridView dgv = new DataGridView();
                        //Axis 10.0
                        RadGridView radDgv = new RadGridView();


                        string codingTemplateName = string.Empty;

                        codingTemplateName = comboBoxPreviewCodingTemplate.SelectedItem.ToString();

                        CommonUtilities.GetInstance().SelectedCodingTemplate = codingTemplateName;

                        dataGridViewPreviewCodingTemplate.Rows.Clear();
                        string[] headerRow = CommonUtilities.GetInstance().HeaderForCodingTemplate;
                        FillDataGridViewHeaderData(headerRow);
                        dgv = CreateDatasetForCodingTemplatePreview(CommonUtilities.GetInstance().BrowseFileName, codingTemplateName, true);

                        //Axis 10.0

                        while (this.radGridCodingTemplate.Rows.Count > 0)
                        {
                            this.radGridCodingTemplate.Rows.RemoveAt(this.radGridCodingTemplate.Rows.Count - 1);
                        }
                        radDgv = CreateRADDatasetForCodingTemplatePreview(CommonUtilities.GetInstance().BrowseFileName, codingTemplateName, true);



                        if (!radDgv.Rows.Count.Equals(0))
                        {

                            radGridCodingTemplate.DataSource = radDgv.DataSource;
                            this.buttonPreviewEdit.Enabled = true;
                            radGridCodingTemplate.Refresh();
                            CommonUtilities.GetInstance().StatementDataTable = null;
                        }
                        else
                        {
                            this.buttonPreviewEdit.Enabled = false;
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0135"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            radGridCodingTemplate.DataSource = radDgv.DataSource;
                            radGridCodingTemplate.Refresh();
                        }
                        selectionFlag = true;
                    }

                }
            }
            catch
            { }
        }


        /// <summary>
        /// Create DataGridView using already existing dataTable
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="codingTemplateName"></param>
        public void CreateDataForExistingDataTable(DataTable dt, string codingTemplateName)
        {
            try
            {
                string[] headerRow = CommonUtilities.GetInstance().HeaderForCodingTemplate;
                #region Set DataGridViewStyle
                cellStyle.Font = new Font(new FontFamily("Verdana"), 8.25F);
                cellStyle.ForeColor = Color.Black;
                #endregion
                FillDataGridViewHeaderData(headerRow);
                comboBoxPreviewCodingTemplate.SelectedItem = codingTemplateName;
                dataGridViewPreviewCodingTemplate.Rows.Clear();
                //axis 11  pos 
                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                {
                    #region Fill row of datagridview as per data table for QuickBooks
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        try
                        {
                            dr = new DataGridViewRow();
                            for (int j = 0; j < 11; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        checkCell = new DataGridViewCheckBoxCell();
                                        if (dt.Rows[i].ItemArray[j].Equals("True"))
                                        {
                                            checkCell.Value = true;
                                            dr.Cells.Add(checkCell);
                                        }
                                        else
                                        {
                                            checkCell.Value = false;
                                            dr.Cells.Add(checkCell);
                                        }
                                        break;
                                    case 1:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = dt.Rows[i].ItemArray[j];
                                        dr.Cells.Add(textCell);
                                        break;
                                    case 2:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = dt.Rows[i].ItemArray[j];
                                        dr.Cells.Add(textCell);
                                        break;
                                    case 3:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = dt.Rows[i].ItemArray[j];
                                        dr.Cells.Add(textCell);
                                        break;
                                    case 4:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = dt.Rows[i].ItemArray[j];
                                        dr.Cells.Add(textCell);
                                        break;
                                    case 5:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = custList;
                                        if (!string.IsNullOrEmpty(dt.Rows[i].ItemArray[j].ToString()))
                                            comboCell.Value = dt.Rows[i].ItemArray[j].ToString();
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;
                                    case 6:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = CommonUtilities.GetInstance().ItemList;
                                        if (!string.IsNullOrEmpty(dt.Rows[i].ItemArray[j].ToString()))
                                            comboCell.Value = dt.Rows[i].ItemArray[j].ToString();
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;
                                    case 7:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = CommonUtilities.GetInstance().TaxCodeList;
                                        if (!string.IsNullOrEmpty(dt.Rows[i].ItemArray[j].ToString()))
                                            comboCell.Value = dt.Rows[i].ItemArray[j].ToString();
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;
                                    case 8:
                                        comboCell = new DataGridViewComboBoxCell();
                                        comboCell.DataSource = custList;
                                        if (!string.IsNullOrEmpty(dt.Rows[i].ItemArray[j].ToString()))
                                            comboCell.Value = dt.Rows[i].ItemArray[j].ToString();
                                        comboCell.Style = cellStyle;
                                        dr.Cells.Add(comboCell);
                                        break;
                                    case 9:

                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = dt.Rows[i].ItemArray[j];
                                        dr.Cells.Add(textCell);
                                        break;

                                    case 10:
                                        checkCell = new DataGridViewCheckBoxCell();
                                        if (dt.Rows[i].ItemArray[j].Equals("True"))
                                        {
                                            checkCell.Value = true;
                                            dr.Cells.Add(checkCell);
                                        }
                                        else
                                        {
                                            checkCell.Value = false;
                                            dr.Cells.Add(checkCell);
                                        }
                                        break;


                                    case 11:
                                        textCell = new DataGridViewTextBoxCell();
                                        textCell.Style = cellStyle;
                                        textCell.Value = dt.Rows[i].ItemArray[j];
                                        dr.Cells.Add(textCell);
                                        break;
                                }
                            }
                            dataGridViewPreviewCodingTemplate.Rows.Add(dr);
                        }
                        catch
                        { }
                    }
                    #endregion
                }

            }
            catch
            {
            }

        }

        /// <summary>
        /// Condition for CustomerJob and Billable status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPreviewCodingTemplate_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    //axis11 pos
                    if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                    {
                        if (!string.IsNullOrEmpty(dataGridViewPreviewCodingTemplate.Rows[e.RowIndex].Cells[7].FormattedValue.ToString()))
                        {
                            if (dataGridViewPreviewCodingTemplate.Rows[e.RowIndex].Cells[7].FormattedValue.ToString().Equals("<None>"))
                            {
                                dataGridViewPreviewCodingTemplate.Rows[e.RowIndex].Cells[8].Value = false;
                                dataGridViewPreviewCodingTemplate.Rows[e.RowIndex].Cells[8].ReadOnly = true;
                                dataGridViewPreviewCodingTemplate.Refresh();
                            }
                            else
                            {
                                dataGridViewPreviewCodingTemplate.Rows[e.RowIndex].Cells[8].ReadOnly = false;
                                dataGridViewPreviewCodingTemplate.Refresh();
                            }
                        }
                        else
                        {
                            dataGridViewPreviewCodingTemplate.Rows[e.RowIndex].Cells[8].Value = false;
                            dataGridViewPreviewCodingTemplate.Rows[e.RowIndex].Cells[8].ReadOnly = true;
                            dataGridViewPreviewCodingTemplate.Refresh();
                        }
                    }

                }
            }
            catch
            {
            }
        }

        private void CodingTemplatePreview_FormClosed(object sender, FormClosedEventArgs e)
        {

            //463
            tComboColumn = null;
            CommonUtilities.GetInstance().OtherListWithType = null;
            CommonUtilities.GetInstance().VendorListWithType = null;
            CommonUtilities.GetInstance().EmployeeListWithType = null;
            CommonUtilities.GetInstance().CustomerListWithType = null;
            CommonUtilities.GetInstance().AccountItemList = null;
            CommonUtilities.GetInstance().depositeaccountTO = null;
            CommonUtilities.GetInstance().CodingTemplateList = null;
            CommonUtilities.GetInstance().ExpenseAccountList = null;
            CommonUtilities.GetInstance().allcust = null;
            // CommonUtilities.GetInstance().sb = null;
            CommonUtilities.GetInstance().CombineCustomerList = null;

            if (radCustList != null)
            {
                radCustList = null;
            }


            if (radCustomerList != null)
            {
                radCustomerList = null;
            }
            if (radEmployeeList != null)
            {
                radEmployeeList = null;
            }
            if (radVendorList != null)
            {
                radVendorList = null;
            }
            if (radOtherList != null)
            {
                radOtherList = null;
            }
            if (classList != null)
            {
                classList = null;
            }
            if (otherList != null)
            {
                otherList.Clear();
            }
            if (custjobList != null)
            {
                custjobList.Clear();
            }
            if (custList != null)
            {
                custList.Clear();
            }
            if (vendList != null)
            {
                vendList.Clear();
            }

            this.DialogResult = DialogResult.OK;



            //update datagridview
            //463
            //this.dataGridViewPreviewCodingTemplate.Update();
            //this.dataGridViewPreviewCodingTemplate.Refresh();

            // CommonUtilities.GetInstance().StatementDataTable = GetDataTable(this.dataGridViewPreviewCodingTemplate);

            //463
            ////  CommonUtilities.GetInstance().StatementDataTable = GetDataTable(this.radGridCodingTemplate);

            CommonUtilities.GetInstance().StatementDataTable = null;

            if (this.radGridCodingTemplate != null)
            {
                this.radGridCodingTemplate.Dispose();
            }
            //463
            if (this.comboBoxPreviewCodingTemplate.SelectedIndex != -1)
                CommonUtilities.GetInstance().SelectedCodingTemplate = null;

            // CommonUtilities.GetInstance().SelectedCodingTemplate = this.comboBoxPreviewCodingTemplate.SelectedItem.ToString();


            if (m_CodingTemplatePreview != null)
            {
                m_CodingTemplatePreview.Dispose();
            }
            try
            {
                //463
                //TransactionImporter.TransactionImporter frmImporter = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                //ComboBox cbTemplates = (ComboBox)frmImporter.comboBoxCodingTemplate;
                //cbTemplates.Items.Clear();
                //cbTemplates.Items.Add("<Add New Coding Template>");
                //List<string> templateList = new List<string>();
                //templateList = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();



                //foreach (string codingTemplateName in templateList)
                //{
                //    if (!string.IsNullOrEmpty(codingTemplateName))
                //        cbTemplates.Items.Add(codingTemplateName);
                //}
                //cbTemplates.SelectedItem = this.comboBoxPreviewCodingTemplate.SelectedItem.ToString();
                CommonUtilities.GetInstance().StatementDataTable = null;

            }
            catch (Exception exp)
            {
                //MessageBox.Show("Error:" +exp.Message);
            }
            // if (objTransactionImporter.buttonImport.Visible == false)
            //   objTransactionImporter.buttonImport.Visible = true;

            this.Close();

        }

        private void dataGridViewPreviewCodingTemplate_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            return;
        }

        private void radGridCodingTemplate_CellFormatting(object sender, CellFormattingEventArgs e)
        {

        }

        private void radGridCodingTemplate_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            //if (this.radGridCodingTemplate.CurrentColumn is GridViewMultiComboBoxColumn)
            //{
            //    if (!isColumnAdded)
            //    {
            //        isColumnAdded = true;
            //        RadMultiColumnComboBoxElement editor = (RadMultiColumnComboBoxElement)this.radGridCodingTemplate.ActiveEditor;
            //        editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
            //        editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("OrderID"));
            //        editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Quantity"));
            //        editor.AutoSizeDropDownToBestFit = true;
            //    }
            //}
        }

        private void buttonAddNewCodingTemplate_Click(object sender, EventArgs e)
        {
            #region Used for creating new Coding Template


            CommonUtilities.GetInstance().CodingTemplateName = string.Empty;

            EditTemplate editTemplate = EditTemplate.GetInstance();

            if (ConnectedSoft == Constants.QBstring)
            {
                editTemplate.ConnectedSoft = Constants.QBstring;
            }
            //axis 11 pos
            else if (ConnectedSoft == Constants.QBPOSstring)
            {
                editTemplate.ConnectedSoft = Constants.QBPOSstring;
            }

            else if (ConnectedSoft == Constants.QBOnlinestring)
            {
                editTemplate.ConnectedSoft = Constants.QBOnlinestring;
            }

            List<string> editTemplateList = new List<string>();

            editTemplateList = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();
            CommonUtilities.GetInstance().CodingTemplateList = editTemplateList;

            foreach (string codingTemplateName in editTemplateList)
            {
                if (!string.IsNullOrEmpty(codingTemplateName))
                    editTemplate.comboBoxTemplate.Items.Add(codingTemplateName);
            }
            DataGridView dgvEdit = new DataGridView();

            dgvEdit = editTemplate.CreateDatasetForEditTemplatePreview(null, true);

            DialogResult key = editTemplate.ShowDialog();

            //  this.radGridCodingTemplate = new RadGridView();

            if (key == DialogResult.Cancel)
            {
                while (this.radGridCodingTemplate.Rows.Count > 0)
                {
                    this.radGridCodingTemplate.Rows.RemoveAt(this.radGridCodingTemplate.Rows.Count - 1);
                }


                //CodingTemplatePreview codingTemplate = CodingTemplatePreview.GetInstance();

                if (ConnectedSoft == Constants.QBstring)
                {
                    this.ConnectedSoft = Constants.QBstring;
                    //codingTemplate.ConnectedSoft = Constants.QBstring;
                }
                //axis 11 pos
                else if (ConnectedSoft == Constants.QBPOSstring)
                {
                    this.ConnectedSoft = Constants.QBPOSstring;
                }
                else if (ConnectedSoft == Constants.QBOnlinestring)
                {
                    this.ConnectedSoft = Constants.QBOnlinestring;
                }

                List<string> codingTemplateList = new List<string>();
                codingTemplateList = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();

                comboBoxPreviewCodingTemplate.Items.Clear();

                CommonUtilities.GetInstance().CodingTemplateList = codingTemplateList;
                foreach (string codingTemplateName in codingTemplateList)
                {
                    if (!string.IsNullOrEmpty(codingTemplateName))
                        this.comboBoxPreviewCodingTemplate.Items.Add(codingTemplateName);
                }
                //DataGridView dsCt = new DataGridView();
                //RadGridView radDs = new RadGridView();

                // dsCt = this.CreateDatasetForCodingTemplatePreview(CommonUtilities.GetInstance().BrowseFileName, CommonUtilities.GetInstance().NewTemplateName, false);
                //radDs = this.CreateRADDatasetForCodingTemplatePreview(CommonUtilities.GetInstance().BrowseFileName, CommonUtilities.GetInstance().NewTemplateName, false);

                // this.dataGridViewPreviewCodingTemplate.DataSource= dsCt.DataSource;
                //this.radGridCodingTemplate.DataSource = radDs.DataSource;

                // this.radGridCodingTemplate.DataSource = radDs.DataSource;


                this.radGridCodingTemplate = this.CreateRADDatasetForCodingTemplatePreview(CommonUtilities.GetInstance().BrowseFileName, CommonUtilities.GetInstance().NewTemplateName, false);

                this.radGridCodingTemplate.Visible = true;


                if (this.radGridCodingTemplate.Rows.Count.Equals(0))
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0135"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                    //this.Visible = false;
                    //this.Close();
                    //DialogResult keyresult = this.ShowDialog();
                    //if (keyresult == DialogResult.OK)
                    //{
                    //this.Dispose();
                    //this.Close();

                    //}
                }

            }

            #endregion

        }

        private void radGridCodingTemplate_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement is GridFilterCellElement && e.CellElement.ColumnInfo.Name == "Select")
            {
                e.CellElement.Children.Clear();
                e.CellElement.Alignment = ContentAlignment.MiddleLeft;
            }
        }

       
        private void buttonImportCodingTemplate_Click(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().is_statement_preview = true;
            //   CommonUtilities.GetInstance().ValidateRowCount = String.Empty;
            CommonUtilities.GetInstance().BtnPreview = true;
            CommonUtilities.GetInstance().StatementDataTable = this.GetDataTable(this.radGridCodingTemplate);
            strConnectedSoftware = CommonUtilities.GetInstance().ConnectedSoftware;

            if (strConnectedSoftware == Constants.QBstring)
            {
                Mappings.GetInstance().ConnectedSoft = strConnectedSoftware;
                //Display Message if Default settings is not saved.
                m_DefaultSetting = new DefaultAccountSettings();
                //  m_DefaultSetting = m_DefaultSetting.GetDefaultAccountSettings();

                if (m_DefaultSetting == null)
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    QuickBookAccountSettings settingsform = new QuickBookAccountSettings();
                    settingsform.Show();
                    return;
                }
            }


            objTransactionImporter = new TransactionImporter.TransactionImporter();

            // TransactionImporter.TransactionImporter.is_statement_preview_selected = true;   //bug 501
            objTransactionImporter.buttonImport_Click(sender, e);

            CommonUtilities.GetInstance().IsImportProcess = false;
        }




        /// <summary>
        /// This method is used to truncate RefNumber if length is greater than 11
        /// </summary>
        /// <returns></returns>
        private DataTable CreateFormatedRefNumberDataTable(DataTable dt)
        {
            try
            {
                DataTable dtNew = new DataTable();
                for (int col = 0; col < dt.Columns.Count; col++)
                {
                    dtNew.Columns.Add(dt.Columns[col].ColumnName);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dtNew.NewRow();
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j == 2)
                        {
                            if (!dt.Rows[i][j].ToString().Equals(string.Empty))
                            {
                                if (dt.Rows[i][j].ToString().Length > 11)
                                {
                                    dr[j] = dt.Rows[i][j].ToString().Substring(dt.Rows[i][j].ToString().Length - 11);
                                }
                                else
                                    dr[j] = dt.Rows[i][j].ToString();
                            }
                            else
                                dr[j] = dt.Rows[i][j].ToString();
                        }
                        else
                            dr[j] = dt.Rows[i][j].ToString();
                    }
                    dtNew.Rows.Add(dr);
                }
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    DataRow dr = dtNew.NewRow();
                //    for (int j = 0; j < dt.Columns.Count; j++)
                //    {
                //        if (j == 2)
                //        {
                //            if (!dt.Rows[i][j].ToString().Equals(string.Empty))
                //            {
                //                if (dt.Rows[i][j].ToString().Length > 11)
                //                {
                //                    dr[j] = dt.Rows[i][j].ToString().Substring(dt.Rows[i][j].ToString().Length - 11);
                //                }
                //                else
                //                    dr[j] = dt.Rows[i][j].ToString();
                //            }
                //            else
                //                dr[j] = dt.Rows[i][j].ToString();
                //        }
                //        else
                //            dr[j] = dt.Rows[i][j].ToString();
                //    }
                //    dtNew.Rows.Add(dr);
                //}
                return dtNew;
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// Return  modified  data table 
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        private DataTable GetModifiedTable(string entityName)
        {
            DataTable dt = new DataTable();
            bool itemFlag = false;
            bool accountFlag = false;

            #region Check whether datarow contain Item or Account

            if (!entityName.Equals(string.Empty))
                itemFlag = CommonUtilities.GetInstance().FindInItemList(entityName);

            if (!itemFlag && !entityName.Equals(string.Empty))
                accountFlag = CommonUtilities.GetInstance().FindInAccountList(entityName);

            if (itemFlag)
            {
                #region Create data table as per ItemLine

                for (int i = 0; i < 10; i++)
                {
                    dt.Columns.Add(CommonUtilities.GetInstance().ImportingItemDataTable[i].ToString());
                }

                #endregion
            }
            else if (accountFlag)
            {
                #region Create data table as per ExpenseLine

                for (int i = 0; i < 12; i++)
                {
                    if (i == 0 || i == 11)
                    {
                    }
                    else
                        dt.Columns.Add(CommonUtilities.GetInstance().ImportingExpenseDataTable[i].ToString());
                }

                #endregion
            }
            else
            {
                #region Create data table as above both condition is false
                for (int i = 0; i < 12; i++)
                {
                    if (i == 0 || i == 11)
                    {
                    }
                    else
                        dt.Columns.Add(CommonUtilities.GetInstance().ImportingExpenseDataTable[i].ToString());
                }
                #endregion
            }

            #endregion

            return dt;
        }


        /// Generate final data table
        /// </summary>
        /// <param name="modifiedTable"></param>
        /// <returns></returns>
        private DataTable GenerateFinalTable(DataTable modifiedTable)
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr = dt.NewRow();

                for (int i = 0; i < modifiedTable.Columns.Count; i++)
                {
                    if (i == 10 || i == 11)
                    {
                        DataColumn col = new DataColumn(modifiedTable.Columns[i].ColumnName);
                        dt.Columns.Add(col);
                        dr[modifiedTable.Columns[i].ColumnName] = modifiedTable.Rows[0].ItemArray[i];
                    }
                    else
                        if (!string.IsNullOrEmpty(Convert.ToString(modifiedTable.Rows[0].ItemArray[i])))
                    {
                        DataColumn col = new DataColumn(modifiedTable.Columns[i].ColumnName);
                        dt.Columns.Add(col);
                        dr[modifiedTable.Columns[i].ColumnName] = modifiedTable.Rows[0].ItemArray[i];
                    }
                }
                dt.Rows.Add(dr);

                return dt;
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Create data table for SalesReceipt transaction
        /// </summary>
        /// <returns></returns>
        private DataTable GetSalesReceiptTable()
        {
            try
            {
                DataTable dt = new DataTable();

                for (int i = 0; i < 8; i++)
                {
                    dt.Columns.Add(CommonUtilities.GetInstance().ImportingSalesReceiptDataTable[i].ToString());
                }

                return dt;
            }
            catch
            {
                return null;
            }

        }

        //463 null valuee error  .tostring t value
        private void radGridCodingTemplate_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (radGridCodingTemplate.Rows[e.RowIndex].Cells[13].Value != null || radGridCodingTemplate.Rows[e.RowIndex].Cells[13].Value != "-1")
                {

                    if (e.ColumnIndex == 13 && e.RowIndex >= 0 && radGridCodingTemplate.Rows[e.RowIndex].Cells[13].Value != "" && radGridCodingTemplate.Rows[e.RowIndex].Cells[13].Value != null)
                    {
                        try
                        {
                            string testString = radGridCodingTemplate.Rows[e.RowIndex].Cells[14].Value.ToString();
                            string[] parts = testString.Split('_');

                            //CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                            //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                            if (radGridCodingTemplate.Rows[e.RowIndex].Cells[14].Value != null && parts[1] == "S")
                            {
                                string txnId = parts[0];

                                ImportSplashScreen.GetTransactionViewRequest("Deposit", txnId, "Txn");
                            }
                            else if (radGridCodingTemplate.Rows[e.RowIndex].Cells[14].Value != null && parts[1] == "C")
                            {
                                string txnId = parts[0];

                                ImportSplashScreen.GetTransactionViewRequest("Check", txnId, "Txn");
                            }

                            else if (radGridCodingTemplate.Rows[e.RowIndex].Cells[14].Value != null && parts[1] == "CH")
                            {
                                string txnId = parts[0];

                                ImportSplashScreen.GetTransactionViewRequest("CreditCardCharge", txnId, "Txn");
                            }
                            else if (radGridCodingTemplate.Rows[e.RowIndex].Cells[14].Value != null && parts[1] == "CR")
                            {
                                string txnId = parts[0];

                                ImportSplashScreen.GetTransactionViewRequest("CreditCardCredit", txnId, "Txn");
                            }
                            else
                                MessageBox.Show("No matching transaction found", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        catch
                        {

                        }

                        //ImportSplashScreen.GetTransaction(txnId, "Txn");

                    }
                    else if (e.ColumnIndex == 12 && e.RowIndex >= 0 && radGridCodingTemplate.Rows[e.RowIndex].Cells[13].Value.ToString() == "")
                    {
                        MessageBox.Show("No matching transaction found", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

     
        private void button1_Click_1(object sender, EventArgs e)
        {
            bool flag = true;
            string codingTemplateName = string.Empty;
            if (comboBoxPreviewCodingTemplate.SelectedItem != null)
            {
                codingTemplateName = comboBoxPreviewCodingTemplate.SelectedItem.ToString();

                CommonUtilities.GetInstance().SelectedCodingTemplate = codingTemplateName;

                matchData(codingTemplateName, flag, "");
            }
            else
            {
                matchData(null, flag, "");
                // MessageBox.Show("Please select template", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }


        
        private void CodingTemplatePreview_Load(object sender, EventArgs e)
        {
            CommonUtilities.GetInstance().radgrd = radGridCodingTemplate;
        }
    }
}


