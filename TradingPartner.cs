using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Xml;
using System.IO;


namespace TradingPartnerDetails
{
    /// <summary>
    /// Trading Partner class used for save the trading partner details in xml format
    /// </summary>
    class TradingPartner
    {
        string m_TradingPartnetID;
        string m_Name;
        string m_DocumentExchange;
        string m_EmailAddress;
        string m_ClientID;
        string m_SchemaLocation;
        NameValueCollection m_Mapping;
        static string m_FilePath = Application.StartupPath + @"\TradingPartnerDB.xml";
        
        /// <summary>
        /// Internal constructor is used to initialise the member variable
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="name"></param>
        /// <param name="docExchange"></param>
        /// <param name="email"></param>
        /// <param name="clientID"></param>
        /// <param name="schemaLoacation"></param>
        internal TradingPartner(string Id, string name, string docExchange,string email, string clientID,string schemaLoacation)
        {
           this.m_TradingPartnetID = Id;
           this.m_Name = name;
           this.m_DocumentExchange = docExchange;
           this.m_EmailAddress = email;
           this.m_ClientID = clientID;
           this.m_SchemaLocation = schemaLoacation;
           this.m_Mapping = new NameValueCollection();
           this.InitialisingMapping();
            
        }
        /// <summary>
        /// Get the Mapping for xml file
        /// </summary>
        internal NameValueCollection Mapping
        {
            get { return this.m_Mapping; }
            
        }
        /// <summary>
        /// Get the file path for xml file
        /// </summary>
        internal static string FilePath
        {
            get { return m_FilePath; }
           
        }
       
        /// <summary>
        /// Saving the trading partner details
        /// </summary>
        /// <returns></returns>
        internal bool SaveTradingPartnerDetail()
        {
            if (!File.Exists(m_FilePath))
            {
                this.CreateNew();
                return true;
            }
            else if (!TradingPartnerCollection.IsExist(m_TradingPartnetID)) 
            {
                this.Append();
                return true;
            }
            else
              return false;    

        }
        /// <summary>
        /// Assigning the value for mapping fields
        /// </summary>
        private void InitialisingMapping()
        {
            this.m_Mapping.Add("TradingPartnerID", m_TradingPartnetID);
            this.m_Mapping.Add("Name", m_Name);
            this.m_Mapping.Add("DocumentExchange", m_DocumentExchange);
            this.m_Mapping.Add("EmailAddress", m_EmailAddress);
            this.m_Mapping.Add("ClientID",m_ClientID);
            this.m_Mapping.Add("SchemaLocation", m_SchemaLocation);
        }
        /// <summary>
        /// This method is called first time when xml file is does not exist
        /// </summary>
        private void CreateNew()
        {
                    
                XmlTextWriter objXmlWriter = new XmlTextWriter(FilePath, null);
                objXmlWriter.Formatting = Formatting.Indented;
                objXmlWriter.WriteStartDocument();
                objXmlWriter.WriteStartElement("TradingPartnerDetails");  //root element name
                for (int i = 0; i < this.m_Mapping.Count; i++)
                {
                    objXmlWriter.WriteStartElement(this.Mapping.GetKey(i));
                    objXmlWriter.WriteString(this.Mapping.Get(i));
                    objXmlWriter.WriteEndElement();
                }
                objXmlWriter.WriteEndElement();
                objXmlWriter.WriteEndDocument();
                objXmlWriter.Close();
          
                        
        }

        /// <summary>
        /// Appending file element and its value in existing xml file.
        /// </summary>
        private void Append()
        {
              XmlDocument xdoc = new XmlDocument();
              xdoc.Load(m_FilePath);
              XmlNode root = (XmlNode)xdoc.DocumentElement;

              for (int i = 0; i < this.m_Mapping.Count; i++)
                {
                    XmlElement newElement = xdoc.CreateElement(this.Mapping.GetKey(i));
                    newElement.InnerText = this.Mapping.Get(i);
                    root.AppendChild(newElement);
                }
               xdoc.Save(m_FilePath);
            
        }
    }

    class TradingPartnerCollection : Collection<TradingPartnerAddressBook>
    {

        private TradingPartnerCollection()
        {
        }
        /// <summary>
        /// This method checks whether the trading partner ID is exist in xml file or not
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static bool IsExist(string id)
        {
       
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(TradingPartner.FilePath);
            XmlNode root = (XmlNode)xdoc.DocumentElement;
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "TradingPartnerID" && root.ChildNodes.Item(i).InnerText==id)
                {
                    return true;
                   
                }
            }
            return false;

        }
    }
}
